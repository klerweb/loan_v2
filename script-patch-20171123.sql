
ALTER TABLE public.land ADD land_benefit text NULL ;
ALTER TABLE public.loan_guarantee_land ALTER COLUMN loan_guarantee_land_benefit TYPE varchar(50) USING loan_guarantee_land_benefit::varchar ;
ALTER TABLE public.loan_analysis ADD loan_analysis_cycle int4 NULL ;
