            

$(".txtDate").datepicker({ 
	dateFormat: "dd/mm/yy", 
	isBuddhist: true,
	dayNames: ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"],
    dayNamesMin: ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
    monthNames: ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],
    monthNamesShort: ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
    yearRange: "-100:+0",
    maxDate: "0",
    changeMonth: true,  
    changeYear: true ,  
    beforeShow:function(){  
    	if($(this).val()!="")
        {  
            var arrayDate = $(this).val().split("/");       
            arrayDate[2] = parseInt(arrayDate[2])-543;  
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
        }
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);
    },  
    onChangeMonthYear: function(){  
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);        
    },  
    onClose:function(dateText, inst){         
    	var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2])+543;  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
    },  
    onSelect: function(dateText, inst){   
        var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2]);  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
    }  
});


$(".txtDateLongYear").datepicker({ 
    dateFormat: "dd/mm/yy", 
    isBuddhist: true,
    dayNames: ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"],
    dayNamesMin: ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
    monthNames: ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],
    monthNamesShort: ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
    yearRange: "-10:+100", 
    changeMonth: true,  
    changeYear: true ,  
    beforeShow:function(){  
        if($(this).val()!="")
        {  
            var arrayDate = $(this).val().split("/");       
            arrayDate[2] = parseInt(arrayDate[2])-543;  
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
        }
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);
    },  
    onChangeMonthYear: function(){  
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);        
    },  
    onClose:function(dateText, inst){         
        var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2])+543;  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
    },  
    onSelect: function(dateText, inst){   
        var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2]);  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
    }  
});

 $(".txtDateStartDate").datepicker({ 
    dateFormat: "dd/mm/yy", 
    isBuddhist: true,
    dayNames: ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"],
    dayNamesMin: ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
    monthNames: ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],
    monthNamesShort: ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
    yearRange: "-10:+10",  
     
    changeMonth: true,  
    changeYear: true ,  
    beforeShow:function(){  
        if($(this).val()!="")
        {  
            var arrayDate = $(this).val().split("/");       
            arrayDate[2] = parseInt(arrayDate[2])-543;  
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
        }
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);
    },  
    onChangeMonthYear: function(){  
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);        
    },  
    onClose:function(dateText, inst){         
        var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2])+543;  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
    },  
    onSelect: function(dateText, inst){   
        var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2]);  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
        
             var length  =  $('#ddlLeasing_length').val();
            var periods  =  $('#txtLeasing_periods').val()*1;
            var startdate = $('#txtLeasing_from_date').val();
            var pay_period = 0; 
            var amount = $('#txtLeasing_amount').val().replace(/,/g, "")*1;
            var first_pay  = $('#txtLeasing_pay_amount').val().replace(/,/g, "")*1;
            var  principal = (amount-first_pay).toFixed(2);
 
            $('#txtLeasing_pay_amount_principal').val(numberWithCommas((amount-first_pay).toFixed(2)));           // cal principal - first recent  
         
                if(principal !="0.00" &&  principal !='0' && periods !='0'){
                          pay_period = (principal/periods).toFixed(2);        
                        $('#txtLeasing_payment_period').val(numberWithCommas(pay_period));       
                  }
          
            // set end date 
          if (startdate != undefined && startdate != '' && periods!=0 && periods !="" ) {
            arr_startdate = startdate.split("/");
            startdate = (parseInt(arr_startdate[2])) + '-' + arr_startdate[1] + '-' + arr_startdate[0];
             
            var datestart = new Date(startdate);  
             periods = periods-1;
            if(length == '1'){
                datestart.setMonth(datestart.getMonth() + periods)- datestart.setDate(datestart.getDate() - 1);      // month      
            }else if(length == '6'){
                datestart.setMonth(datestart.getMonth() + (periods*6))- datestart.setDate(datestart.getDate() - 1);      // 6 month     
            }else if(length == '12'){   //year
                datestart.setMonth(datestart.getMonth() + (periods*12))- datestart.setDate(datestart.getDate() - 1);        // year     
            }
            if(!isValidDate(setDate(datestart))) {
               var datestart2 = new Date(datestart.setDate(datestart.getDate() + 1)); 
               var date1 =  datestart2.getDate();
                var month1 =  (datestart2.getMonth()+1);
                      if(date1<10){ date1 =  ("0" + date1 ) ; }
                      if(month1<10){ month1 =  ("0" + month1 ) ; }
       
               $('#txtLeasing_to_date').val(date1+"/"+month1+"/"+(datestart2.getFullYear()+543));             
            }else{
                $('#txtLeasing_to_date').val(setDate(datestart));   
            } 
            
                   
        }   
    }  
}); 

$(".iconDate").click(function(){ 
    $("#"+$(this).attr('data-ref')).datepicker("show"); 
}); 
$(".iconDateLongYear").click(function(){ 
    $("#"+$(this).attr('data-ref')).datepicker("show"); 
}); 

$(".ddlLandType, #ddlDirector,#ddlLeasing_length, #ddlAttorney, #ddlPosition,#ddLeasing_registrar_title_id, #ddlProvince, #ddlAmphur, #ddlDistrict, #ddlCreditorTitle, #ddlCreditorProvince, #ddlCreditorAmphur, #ddlCreditorDistrict, #ddlObjective, #ddlLandType, #ddlCase,#ddlLeasing_interest").select2({
	minimumResultsForSearch: -1
});

 

String.prototype.replaceAt = function(index, char){
    return this.substr(0, index) + "<span style=\'color:red;\'>" + char + "</span>";
}

$.ui.autocomplete.prototype._renderItem = function(ul, item){
	this.term = this.term.toLowerCase();
	var resultStr = item.label.toLowerCase();
	var t = "";
	
	while (resultStr.indexOf(this.term) != -1)
	{
		var index = resultStr.indexOf(this.term);
		t = t + item.label.replaceAt(index, item.label.slice(index, index + this.term.length));
		resultStr = resultStr.substr(index + this.term.length);
		item.label = item.label.substr(index + this.term.length);
	}
	
	return $("<li></li>").data("item.autocomplete", item).append("<a>" + t + item.label + "</a>").appendTo(ul);
};	

$("#ddlProvince").change(function(){
	$("#divAmphur").load(base_url+"leasing/amphur/"+$("#ddlProvince").val());
	$("#divDistrict").load(base_url+"leasing/district");
});

$("#ddlAmphur").change(function(){
	$("#divDistrict").load(base_url+"leasing/district/"+$("#ddlAmphur").val());
});

$("#ddlCreditorProvince").change(function(){
	$("#divCreditorAmphur").load(base_url+"leasing/amphur_creditor/"+$("#ddlCreditorProvince").val());
	$("#divCreditorDistrict").load(base_url+"leasing/district_creditor");
});

$("#ddlCreditorAmphur").change(function(){
	$("#divCreditorDistrict").load(base_url+"leasing/district_creditor/"+$(this).val());
});

 

$("#frmLeasing").validate({
	rules: {
		txtRai: {
			min:0,
			digits: true,
			required: true
		},
		txtNgan: {
			min:0,
			max:3,
			digits: true,
			required: true
		},
		txtWah: {
			min:0,
			max:99,
			number: true,
			required: true
		},txtLeasing_area_amount: {
                number:true,     
                required: true
         },   txtLeasing_payment_period: {
                number:true,    
                required: true
          },txtLeasing_amount: {
                number:true,    
                required: true
          },txtLeasing_pay_amount: {
                number:true,    
                required: true
          },txtLeasing_pay_amount_principal: {
                number:true,       
                required: true
          },txtLeasing_pay_amount_interest: {
                number:true,    
                required: true
          },txtLeasing_interest: {
                number:true,      
                required: true
          },txtLeasing_rate: {
                number:true,     
                required: true
          }    
	},
	highlight: function(element) {
		$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
	},
	success: function(element) {
		$(element).closest(".form-group").removeClass("has-error");
	}
});      
                                                       
$("#btnSaveLeasingLand").click(function(){ 
    if ($("#frmAddLeasingLand").valid())
    {
        
 
         var data_send = $('#frmAddLeasingLand').serializeArray();       
         $.post(base_url+"leasing/save_leasingLand",    data_send   , function(data) {
         $('#area_landClone tr:last').after('<tr>'+data+'</tr>');   
          countRowLand();
          calculateLand();
          clearInputModal();  
        })    
        
          
        
        $("#modalLand").modal("toggle");  
    }
}); 

$('.editrowLand').click(function(){             
    var formData=[];
     $(this).parent().parent().find("td input,td select").each(function() {    
            textVal = this.value;
            inputName = $(this).attr("name");    
            formData[inputName]=  textVal ;
             
        });                                  
          var row = $(this).closest('tr').index();
                                                                                                                                 
          $('#frmAddLeasingLand #land_id').val(formData['edit_land_id']);    
          $('#frmAddLeasingLand #ddlLandType').select2("val", formData['edit_land_type_id']);    
          $('#frmAddLeasingLand #txtLand_no').val(formData['edit_land_no']);
          $('#frmAddLeasingLand #txtLand_addr_no').val(formData['edit_land_addr_no']);
          $('#frmAddLeasingLand #txtLand_survey_page').val(formData['edit_land_survey_page']);
          $('#frmAddLeasingLand #txtLand_addr_moo').val(formData['edit_land_addr_moo']);     
          $('#frmAddLeasingLand #ddlCreditorProvince').select2("val", formData['edit_province']);
          $('#frmAddLeasingLand #ddlCreditorAmphur').select2("val", formData['edit_amphur']);      
          $('#frmAddLeasingLand #ddlCreditorDistrict').select2("val", formData['edit_district']);   
          $('#frmAddLeasingLand #txtLeasing_area_rai').val(formData['edit_land_area_rai']);
          $('#frmAddLeasingLand #txtLeasing_area_rai').trigger('change');
          $('#frmAddLeasingLand #txtLeasing_area_ngan').val(formData['edit_land_area_ngan']);
          $('#frmAddLeasingLand #txtLeasing_area_ngan').trigger('change');
          $('#frmAddLeasingLand #txtLeasing_area_wah').val(formData['txtLand_area_wah']);
          $('#frmAddLeasingLand #txtLeasing_area_wah').trigger('change');
          $('#frmAddLeasingLand #row_id').val(row);
          
          $('#btn_edit').show();
          $('#btn_add').hide();
          $('#modalLand').modal('show');                                          
           
});  
$('.modalLand').click(function(){
    
    clearInputModal(); 
    $('#modalLand #btn_edit').hide();
    $('#modalLand #btn_add').show();
    $('#modalLand').modal('show');
    
    
});


$("#btnEditLeasingLand").click(function(){ 
    if ($("#frmAddLeasingLand").valid())
    {
         $("#modalLand").modal("toggle");        
         calculateLand();
         var data_send = $('#frmAddLeasingLand').serializeArray();       
         $.post(base_url+"leasing/save_leasingLand",    data_send   , function(data) {
         $('#area_landClone tr:eq('+$('#row_id').val()+')').html(data);   
          countRowLand();
          calculateLand(); 
          clearInputModal();  
        })  
    }
}); 

function clearInputModal(){
    $('#frmAddLeasingLand input').val(''); 
    $('#frmAddLeasingLand #ddlLandType').select2("val", "");
    
    $('#frmAddLeasingLand #ddlCreditorProvince').select2("val", "");
    $('#frmAddLeasingLand #ddlCreditorAmphur').select2("val", "");
    $('#frmAddLeasingLand #ddlCreditorDistrict').select2("val", "");
}

function countRowLand(){
    $( ".row_land" ).each(function( index ) {
        $(this).html('1.'+(index+1));          
    });
}

function delRowLand(rows){
 
      if($('table#area_landClone tr:eq('+rows+')').find('td input[name="edit_land_id"]')){
                var val = $('table#area_landClone tr:eq('+rows+')').find('td input[name="edit_land_id"]').val();
                $('#delete_land').val($('#delete_land').val()+','+val);
           }                                                                                                                               
       $('table#area_landClone tr:eq('+rows+')').remove();
       countRowLand();
       calculateLand();
}

function editRowLand(rows){
      var formData=[];
    $('table#area_landClone tr:eq('+rows+')').find("td input,td select").each(function() {    
            textVal = this.value;
            inputName = $(this).attr("name");    
            formData[inputName]=  textVal ;     
        });                                  
          $('#frmAddLeasingLand #ddlLandType').select2("val", formData['ddlLand_type_id[]']);    
          $('#frmAddLeasingLand #txtLand_no').val(formData['txtLand_no[]']);
          $('#frmAddLeasingLand #txtLand_addr_no').val(formData['txtLand_addr_no[]']);
          $('#frmAddLeasingLand #txtLand_survey_page').val(formData['txtLand_survey_page[]']);
          $('#frmAddLeasingLand #txtLand_addr_moo').val(formData['txtLand_addr_moo[]']);     
          $('#frmAddLeasingLand #ddlCreditorProvince').select2("val", formData['landProvince[]']);
          $('#frmAddLeasingLand #ddlCreditorAmphur').select2("val", formData['landAmphur[]']);      
          $('#frmAddLeasingLand #ddlCreditorDistrict').select2("val", formData['landDistrict[]']);   
          $('#frmAddLeasingLand #txtLeasing_area_rai').val(formData['leasing_area_rai[]']);   
          $('#frmAddLeasingLand #txtLeasing_area_ngan').val(formData['txtLeasing_area_ngan[]']);   
          $('#frmAddLeasingLand #txtLeasing_area_wah').val(formData['txtLeasing_area_wah[]']);       
          $('#frmAddLeasingLand #row_id').val(rows);
          
          $('#btn_edit').show();
          $('#btn_add').hide();
          $('#modalLand').modal('show');  
}

function calculateLand(){
      var total_area = 0 ;
      
    $( ".land_rai" ).each(function(  ) {
          
        total_area +=$(this).val()*400;          
         
    });
    $(".land_ngan").each(function(){
         
        total_area +=$(this).val()*100;
         
    });
    $(".land_wah").each(function(){
         
        total_area +=$(this).val()*1;
          
    });
    var total = 0;
    var rai =0;  
    var ngan =0;  
    var wah =0;  
     rai = parseInt((total_area)/400);
     ngan = parseInt((total_area % 400)/100);
     wah = ((total_area % 400)%100).toFixed(1); 
      if(wah<10){
           wah =  ("0" + wah ) ;       
       }
     
               
    $('#txtLeasing_area_amount').val($( ".row_land" ).length);
    $('#txtRai').val(rai)  ;
    $('#txtNgan').val(ngan)  ;
    $('#txtWah').val(wah)  ;
    
}

function setDate(dates){
        var vdate = new Date(dates);
        var vYear = vdate.getFullYear() + 543;
        var vMonth = ("0" + (vdate.getMonth() + 1)).slice(-2);
        var vDay = ("0" + (vdate.getDate() + 1)).slice(-2); 

       return vDay + '/' + vMonth + '/' + vYear ;
}

function calulateAmount(){ 
      
    var length  =  $('#ddlLeasing_length').val();
    var periods  =  $('#txtLeasing_periods').val()*1;
    var startdate = $('#txtLeasing_from_date').val();
    var pay_period = 0; 
    var amount = $('#txtLeasing_amount').val().replace(/,/g, "")*1;
    var first_pay  = $('#txtLeasing_pay_amount').val().replace(/,/g, "")*1;
    var principal = (amount-first_pay).toFixed(2);
    
    $('#txtLeasing_pay_amount_principal').val(numberWithCommas((amount-first_pay).toFixed(2)));           // cal principal - first recent 
        
        if(principal !="0.00" &&  principal !='0' && periods !='0'){
                  pay_period = (principal/periods).toFixed(2);
                  $('#txtLeasing_payment_period').val(numberWithCommas(pay_period));    
          }
          
            // set end date 
          if (startdate != undefined && startdate != '' && periods!=0 && periods !="" ) {
            arr_startdate = startdate.split("/");
            startdate = (parseInt(arr_startdate[2]-543)) + '-' + arr_startdate[1] + '-' + arr_startdate[0];
             
            var datestart = new Date(startdate);  
            periods = periods-1; 
            if(length == '1'){
                datestart.setMonth(datestart.getMonth() + periods) - datestart.setDate(datestart.getDate() - 1);      // month
            }else if(length == '6'){
                datestart.setMonth(datestart.getMonth() + (periods*6)) - datestart.setDate(datestart.getDate() - 1);     // 6 month 
            }else if(length == '12'){   //year
                 datestart.setMonth(datestart.getMonth() + (periods*12)) - datestart.setDate(datestart.getDate() - 1);     // year        
            }
           if(!isValidDate(setDate(datestart))) {
               var datestart2 = new Date(datestart.setDate(datestart.getDate() + 1)); 
               var date1 =  datestart2.getDate();
                var month1 =  (datestart2.getMonth()+1);
                      if(date1<10){ date1 =  ("0" + date1 ) ; }
                      if(month1<10){ month1 =  ("0" + month1 ) ; }
       
               $('#txtLeasing_to_date').val(date1+"/"+month1+"/"+(datestart2.getFullYear()+543));             
            }else{
                $('#txtLeasing_to_date').val(setDate(datestart));   
            } 
                             
        }     
} 


 function isValidDate(s) {
  var bits = s.split('/');
  var d = new Date(bits[2], bits[1] - 1, bits[0]);
  return d && (d.getMonth() + 1) == bits[1];
}
 
$('#ddlLeasing_length').change(function(){
  calulateAmount();   
});

$('#txtLeasing_periods').keyup(function(){
      calulateAmount();
});

$('#txtLeasing_amount').keyup(function(){
      calulateAmount();
});

$('#txtLeasing_pay_amount').keyup(function(){
      calulateAmount();
});
                                
calculateLand(); 
calulateAmount();

/* ------------------------------ cal Leasing  ---------------------------- */

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}  

function cal_time()
{
    var time = parseInt($("#txtCalTime").val());

    sum = 0;
    for(i = 1; i <= time; i++)
    {
        end = parseInt($("#txtCalEnd_"+i).val());
        start = parseInt($("#txtCalStart_"+i).val());

        amount = parseFloat($("#txtCalAmount_"+i).val());        
        round = (end - start) + 1;    

        sum += round * amount;
    }

    return sum;
}

function cal_analysis()
{
 
    $.post(base_url+"leasing/cal", {   income: $("#txtLeasing_amount").val(), amount: $("#txtLeasing_pay_amount_principal").val(), interest: $("#ddlLeasing_interest").val(), type: $("#ddlLeasing_length").val(), year: $("#txtLeasing_periods").val(), pay: $("#txtLeasing_from_date").val(), start: $("#txtDate").val()  }, function(data) {
        $("#div_leasingCal").html(data);
    });
          
}

$("#btnLeasingCal").click(function(){ 
 
    if($('#ddlLeasing_length').val() !="" && ($('#txtLeasing_amount').val() !='0.00' || $('#txtLeasing_amount').val() !='0' )  && $('#txtLeasing_periods').val() !=0 && $('#txtLeasing_from_date').val() !="" && $('#txtDate').val() !=""){
        $.post(base_url+"leasing/cal_leasing", { id: $("#txtId").val(), amount: $("#txtLeasing_pay_amount_principal").val(),round:$('#txtLeasing_periods').val() }, function(data) {
            $("#divCalLeasing").html(data);
            $("#modalLeasingCal").modal("show");
        });  
        
        $("#modalLeasingCal").modal("show"); 
    }else{
        alert('โปรดกรอกข้อมูลให้ครบถ้วนก่อนคำนวณ');
    }
     
});   

$("#btnLeasingCalSave").click(function(){ 
    var use = cal_time();
    var amount = parseFloat($("#txtLeasing_pay_amount_principal").val().replace(/,/g,""));
     
    if(use==amount)
    {
        if( $('#txtCalEnd_'+$("#txtCalTime").val()).val() != $('#txtLeasing_periods').val()){
           alert("จำนวนงวดไม่ตรงตามสัญญา");  
        }else{
          $("#modalLeasingCal").modal("toggle");
            $.post(base_url+"leasing/save_cal", $("#frmLeasingCal").serialize(), function(data){
                cal_analysis();   
             });  
        }   
    }
    else
    {
        alert("เงินต้นไม่พอดี");
    }
});  

cal_analysis();
 