// In the following example, markers appear when the user clicks on the map.
      // The markers are stored in an array.
      // The user can then click an option to hide, show or delete the markers.
      var map=null;
      var markers = [];
     
  
      $('a[href="#land_location"]').on('click', function () {
        if (map == null) {
            initMap();       
             var lat = $('[name*=txt_lat]').val();
            var lng = $('[name*=txt_long]').val();
            
            if(lat!=''&&lng!=''){
                var myLatlng = new google.maps.LatLng(lat, lng);
                addMarker(myLatlng);
                map.panTo(myLatlng);
            }  
        }
    });

      function initMap() {
        var thailand = {lat: 13.736717, lng:100.523186};

        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: thailand
          //mapTypeId: 'terrain'
        });

        // This event listener will call addMarker() when the map is clicked.
        map.addListener('click', function(event) {
           clearMarkers();
           var lat =event.latLng.lat();
            var lng = event.latLng.lng();
            var id = $('[name*=land_id]').val();
            var latlng = lat + ',' + lng;
            //alert(lat + ',' + lng);
           $.ajax({
                method: "POST",
                url: base_url +'land/save_latlong',
                data: { land_id: id, latlng: latlng }
              })
                .done(function( msg ) {
                  //alert( "Data Saved: " + msg );
                  if(msg){
                       $('[name*=txt_lat]').val(lat);
                      $('[name*=txt_long]').val(lng);
                  }
                });
          addMarker(event.latLng);
        });

        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);
       
      }

      // Adds a marker to the map and push to the array.
      function addMarker(location) {
        var marker = new google.maps.Marker({
          position: location,
          map: map
        });
        markers.push(marker);
      }

      // Sets the map on all markers in the array.
      function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
      }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
      }
      
      $('#btn_setmap').click(function () { 
          if(map==null)
              initMap() 
          
            clearMarkers();
            var lat = $('[name*=txt_lat]').val();
            var lng = $('[name*=txt_long]').val();
            
            if(lat!=''&&lng!=''){
                var myLatlng = new google.maps.LatLng(lat, lng);
                addMarker(myLatlng);
                map.panTo(myLatlng);
            }  
             
            
        });
$(document).ready(function(){
     initMap();
});