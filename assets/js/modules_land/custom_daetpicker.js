
$.fn.CustomDatePicker = function(){
	
	$(this).datepicker({
			dateFormat: "dd/mm/yy",
			isBuddhist: true,
			dayNames: datename,
			dayNamesMin: datenamemin,
			monthNames: monthname,
			monthNamesShort: monthnamemin,
			changeMonth: true,
			changeYear: true,
			beforeShow: function () {
				if ($(this).val() != "")
				{
					var arrayDate = $(this).val().split("/");
					arrayDate[2] = parseInt(arrayDate[2]) - 543;
					$(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
				}
				setTimeout(function () {
					$.each($(".ui-datepicker-year option"), function (j, k) {
						var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
						$(".ui-datepicker-year option").eq(j).text(textYear);
					});
				}, 50);
			},
			onChangeMonthYear: function () {
				setTimeout(function () {
					$.each($(".ui-datepicker-year option"), function (j, k) {
						var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
						$(".ui-datepicker-year option").eq(j).text(textYear);
					});
				}, 50);
			},
			onClose: function () {
				if ($(this).val() != "" && $(this).val() == dateBefore) {
					var arrayDate = dateBefore.split("/");
					arrayDate[2] = parseInt(arrayDate[2]) + 543;
					$(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
				}
			},
			onSelect: function (dateText, inst) {
				dateBefore = $(this).val();
				arr_birdate = dateBefore.split("/");
				
				var arrayDate = dateText.split("/");
				arrayDate[2] = parseInt(arrayDate[2]) + 543;
				$(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

			
			}
		});
}


