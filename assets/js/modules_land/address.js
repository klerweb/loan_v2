$("#ddl_province").change(function () {
    $("#div_amphur").load(base_url + "land/amphur/" + $("#ddl_province").val());
    $("#div_district").load(base_url + "land/district");
});

//owner
loadOwnerAddress();

$("#ddl_owner_province").change(function () {
    address_load($('#div_owner_amphur'));
    $("#div_owner_amphur").load(base_url + "land/amphur_owner/" + $("#ddl_owner_province").val() + '/' + $('#hidden_owner_amphur').val());
});

$("#div_owner_amphur").change(function () {
    address_load($('#div_owner_district'));
    $("#div_owner_district").load(base_url + "land/district_owner/" + $('#ddl_amphur_owner').val() + '/' + $('#hidden_owner_district').val());


});

$("#div_owner_district").change(function () {
    $('#txt_owner_zipcode').val('');
    $.get(base_url + 'land/zipcode/' + $('#ddl_owner_province').val() + '/' + $('#ddl_amphur_owner').val() + '/' + $('#ddl_district_owner').val(), function (data) {

        $('#txt_owner_zipcode').val(data);
    });


});

//loan
loadPersonLoadAddress();
$("#ddl_province").change(function () {
    address_load($('#div_amphur'));
    $("#div_amphur").load(base_url + "land/amphur/" + $("#ddl_province").val() + '/' + $('#hidden_amphur').val());
});

$("#div_amphur").change(function () {
    address_load($('#div_district'));
    $("#div_district").load(base_url + "land/district/" + $('#ddl_amphur').val() + '/' + $('#hidden_district').val());
});

function loadOwnerAddress() {

    if ($("#ddl_owner_province").val() != null && $("#ddl_owner_province").val() > 0) {
        $('[name*=btn_save_owner]').attr('disabled','disabled');
        var load = $("#div_owner_amphur").load(base_url + "land/amphur_owner/" + $("#ddl_owner_province").val() + '/' + $('#hidden_owner_amphur').val(), function () {

            address_load($('#div_owner_district'));
            $("#div_owner_district").load(base_url + "land/district_owner/" + $('#ddl_amphur_owner').val() + '/' + $('#hidden_owner_district').val(),function(){
                
               $.get(base_url + 'land/zipcode/' + $("#ddl_owner_province").val() + '/' + $('#ddl_amphur_owner').val() + '/' + $('#ddl_district_owner').val(),function(data,status){
                    $('#txt_owner_zipcode').val(data);
                    $('[name*=btn_save_owner]').removeAttr('disabled');
                });
            });
        });
    }
}

function loadPersonLoadAddress() {
    if ($("#ddl_province").val() != null && $("#ddl_province").val() > 0) {
        $('[name*=btn_land_save]').attr('disabled','disabled');
        var load = $("#div_amphur").load(base_url + "land/amphur/" + $("#ddl_province").val() + '/' + $('#hidden_amphur').val(), function () {

            address_load($('#div_district'));
            $("#div_district").load(base_url + "land/district/" + $('#ddl_amphur').val() + '/' + $('#hidden_district').val(),function(){
                 $('[name*=btn_land_save]').removeAttr('disabled');
            });

        });
    }
}

function address_load(obj) {
    $(obj).html('<ul class="fa-ul"><li><i class="fa-li fa fa-spinner fa-spin"></i> Loading..</li></ul>');
}
//end address