﻿/*****initial control******/

	
	var dayNames = ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"];
	var dayNamesMin = ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."];
	var monthNames = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
	var monthNamesShort =["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."];
	
	//set default all html tag select
	$('select[name^="ddl"]').select2({
		minimumResultsForSearch: -1
	});
	
	
	
	var province = $("#ddl_province").val();	
	if(province != ''){
		var amphur = $('#hidden_amphur').val();		
		$("#div_amphur").load(base_url+"land/amphur/"+province+"/"+amphur);

		if(amphur != ''){		
			var district = $('#hidden_district').val(); 
			$("#div_district").load(base_url+"land/district/"+amphur+"/"+district);
			
		}		
	}
	
	//ajax dropdown
	$("#ddl_province").change(function(){
		$("#div_amphur").load(base_url+"land/amphur/"+$("#ddl_province").val());
		$("#div_district").load(base_url+"land/district");
	});

	var ddl_owner_province = $("#ddl_owner_province").val();	
	if(ddl_owner_province != ''){
		var amphur = $('#hidden_owner_amphur').val();
		$("#div_owner_amphur").load(base_url+"land/amphur_owner/"+$("#ddl_owner_province").val()+"/"+amphur);
		if(amphur != ''){		
			var district = $('#hidden_owner_district').val(); 
			$("#div_owner_district").load(base_url+"land/district_owner/"+amphur+"/"+district);			
			
			$('#txt_owner_zipcode').load(base_url+'land/zipcode/'+ddl_owner_province+'/'+amphur+'/'+district);
		}
		
	}
	
	$("#ddl_owner_province").change(function(){
		$("#div_owner_amphur").load(base_url+"land/amphur_owner/"+$("#ddl_owner_province").val());
		$("#div_owner_district").load(base_url+"land/district_owner");
	});

	//search owner
	$('#btn_search_owner').click(function(){
		jQuery.get(base_url+"land/search_person",{name:$('#txt_search_owner').val()}, function( data ) {
			$.each(data, function(i,row){
			$("#list_owner_person.table > tbody").append("<tr><td>"+row['person_fname']+' '+row['person_lname']+"</td><td>"
			+ '<input class="btn btn-sm" onclick="func_add_owner('+row['person_id']+',\''+row['person_fname']+'\',\''+row['person_lname']+'\');" type="button" value="เลือก">'
			+ "</td></tr>");
			});
		}, "json" );
	});
	
	//birthdday
	$('#txtDate').datepicker({
		dateFormat: "dd/mm/yy", 
		isBuddhist: true,
		dayNames: dayNames,
		dayNamesMin: dayNamesMin,
		monthNames: monthNames,
		monthNamesShort: monthNamesShort,
		changeMonth: true,  
		changeYear: true ,  
		beforeShow:function(){  
			if($(this).val()!="")
			{  
				var arrayDate = $(this).val().split("/");       
				arrayDate[2] = parseInt(arrayDate[2])-543;  
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
			}  
			setTimeout(function(){  
				$.each($(".ui-datepicker-year option"),function(j,k){  
					var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
					$(".ui-datepicker-year option").eq(j).text(textYear);  
				});               
			},50);
		},  
		onChangeMonthYear: function(){  
			setTimeout(function(){  
				$.each($(".ui-datepicker-year option"),function(j,k){  
					var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
					$(".ui-datepicker-year option").eq(j).text(textYear);  
				});               
			},50);        
		},  
		onClose:function(){  
			if($(this).val()!="" && $(this).val()==dateBefore){           
				var arrayDate = dateBefore.split("/");  
				arrayDate[2] = parseInt(arrayDate[2])+543;  
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);      
			}         
		},  
		onSelect: function(dateText, inst){  
			
			dateBefore = $(this).val();  
			var arrayDate = dateText.split("/");  
			arrayDate[2] = parseInt(arrayDate[2])+543;  
			$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
			
			
		} 
	});
	
	$('#txt_map_check_date').datepicker({
		dateFormat: "dd/mm/yy", 
		isBuddhist: true,
		dayNames: dayNames,
		dayNamesMin: dayNamesMin,
		monthNames: monthNames,
		monthNamesShort: monthNamesShort,
		changeMonth: true,  
		changeYear: true ,  
		ignoreReadonly: true,
		beforeShow:function(){  
			if($(this).val()!="")
			{  
				var arrayDate = $(this).val().split("/");       
				arrayDate[2] = parseInt(arrayDate[2])-543;  
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
			}  
			setTimeout(function(){  
				$.each($(".ui-datepicker-year option"),function(j,k){  
					var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
					$(".ui-datepicker-year option").eq(j).text(textYear);  
				});               
			},50);
			
		},  
		onChangeMonthYear: function(){  
			setTimeout(function(){  
				$.each($(".ui-datepicker-year option"),function(j,k){  
					var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
					$(".ui-datepicker-year option").eq(j).text(textYear);  
				});               
			},50);        
		},  
		onClose:function(){  
			if($(this).val()!="" && $(this).val()==dateBefore){           
				var arrayDate = dateBefore.split("/");  
				arrayDate[2] = parseInt(arrayDate[2])+543;  
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);      
			}         
		},  
		onSelect: function(dateText, inst){  
			
			dateBefore = $(this).val();  
			var arrayDate = dateText.split("/");  
			arrayDate[2] = parseInt(arrayDate[2])+543;  
			$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
			
			
		} 
	});
	
	$('#txt_owner_birthday').datepicker({ 
		dateFormat: "dd/mm/yy", 
		isBuddhist: true,
		dayNames: dayNames,
		dayNamesMin: dayNamesMin,
		monthNames: monthNames,
		monthNamesShort: monthNamesShort,
		changeMonth: true,  
		changeYear: true ,  
		beforeShow:function(){  
			if($(this).val()!="")
			{  
				var arrayDate = $(this).val().split("/");       
				arrayDate[2] = parseInt(arrayDate[2])-543;  
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
			}  
			setTimeout(function(){  
				$.each($(".ui-datepicker-year option"),function(j,k){  
					var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
					$(".ui-datepicker-year option").eq(j).text(textYear);  
				});               
			},50);
		},  
		onChangeMonthYear: function(){  
			setTimeout(function(){  
				$.each($(".ui-datepicker-year option"),function(j,k){  
					var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
					$(".ui-datepicker-year option").eq(j).text(textYear);  
				});               
			},50);        
		},  
		onClose:function(){  
			if($(this).val()!="" && $(this).val()==dateBefore){           
				var arrayDate = dateBefore.split("/");  
				arrayDate[2] = parseInt(arrayDate[2])+543;  
				$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);      
			}         
		},  
		onSelect: function(dateText, inst){  
			
			dateBefore = $(this).val();  
			var arrayDate = dateText.split("/");  
			arrayDate[2] = parseInt(arrayDate[2])+543;  
			$(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
			
			$.get(base_url+'land/calculate_age',{'dob': dateText },function(data){
				$('#txt_owner_age').val(data);
			});
		}  
	});
	
	$.validator.addMethod('filesize', function(value, element, param) {
    // param = size (in bytes) 
    // element = element to validate (<input>)
    // value = value of the element (file name)
		return this.optional(element) || (element.files[0].size <= param) 
	});
	
	
	$("#frm_owner").validate({
	rules: {				
		txt_owner_name : {
			required :true
		},
		txt_owner_last_name : {
			required :true
		},
		ddl_title : {
			required : true
		},
		ddl_owner_province : {
			required : true
		},
		ddl_amphur_owner : {
			required : true
		},
		ddl_district_owner : {
			required : true
		}	
		
	},
	highlight: function(element) {
		$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
	},
	success: function(element) {
		$(element).closest(".form-group").removeClass("has-error");
	},errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
    }
	});
	
	$("#frm_land").validate({
		rules: {	
			
		},
		highlight: function(element) {
			$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
		},
		success: function(element) {
			$(element).closest(".form-group").removeClass("has-error");
		},
		errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
		}
	});
	
	jQuery.validator.addClassRules({
		  css_digit: {
			min: 0,
			digits: true
		  },
		  css_decimal : {
			min: 0,
			number: true
		  }
		  
		});
		
		
	$("input[name=rdo_mortagage]").change(function(){
		var val = $(this).val();
		if(val == "1"){
			$('#txt_land_mortagage').attr('disabled','disabled');
			$('#txt_land_mortagage').val('');
		}else{
			$('#txt_land_mortagage').removeAttr('disabled');
		}
		
	});
	
	$('#land_condition_price_drop13').click(function(){
		if($(this).prop('checked')){
			$('#txt_condition_price_drop13').removeAttr('disabled');
		}else{
			$('#txt_condition_price_drop13').val();
			$('#txt_condition_price_drop13').attr('disabled','disabled');
		}
	});
	
	$('input[name=rdo_reserve]').click(function(){
		func_land_reservation($(this));		
	});
	
	$('input[name=rdo_map_check]').click(function(){
		$('#txt_map_check_date').prop('readonly',true);
		if($(this).val() == 2 ){
			$('#txt_map_check_date').prop('readonly',false);
			
		}
	});
	
	//add rows pricing
	$('#btn-add-price').click(function(){
		var stop = true;
		$('#table_price > tbody > tr').each(function(){
			if(stop){
				if($(this).attr('class') == 'hide'){
					$(this).removeClass('hide');
					stop = false;					
				}
			}
		});
	});
	
	$('input[name^=txt_land_assess_price_pre_max],input[name^=txt_land_assess_price_pre_min],input[name^=land_assess_price]').keyup(function(){
		
		calculate_price();
	});
	
	calculate_total_area();
	$('input[name^=txt_land_assess_rai],input[name^=txt_land_assess_ngan],input[name^=txt_land_assess_wa]').keyup(function(){
		calculate_total_area();
	});
	
	$.each($('input[name^=land_assess_inspect_price]'),function(){
		inspect_price($(this));
	});
	//inspect_price($('input[name^=land_assess_inspect_price]'));
	$('input[name^=land_assess_inspect_price]').keyup(function(){
		inspect_price($(this));
	});
	
	$('#btn_inspect_copy').click(function(){
		$('#tag_target').val('table_org_price');
		
	});
	
	$('#btn_land_own').click(function(){
		var name = $('#m_name').val();
		var lastname = $('#m_last_name').val();
		var birthday = $('#m_birthday').val();
		
		$('#pnl_owners_1').text(name);

		//hidden tr
		var $row = $('#tb-land-owner >tbody').find('tr');
		var has = true;
		$.each($row,function(){
			if($(this).hasClass('hidden') && has)
			{
				has = false;
				$(this).removeClass('hidden');
			}
		});

		
		$('#modal_add_person').modal('hide');
	});
	$( "#txt_owner_name" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
           url : base_url + "land/search_owner_person",
          dataType: "json",
          data: {
            name: request.term
          },
          success: function( data ) {
            response($.map(data, function(item){
					
					var label = item.person_fname;
					return {
						label:label,
						person_id: item.person_id,
						title_id: item.title_id,							
						person_fname: item.person_fname,
						person_lname: item.person_lname,
						person_addr_card_no: item.person_addr_card_no,
						person_addr_card_moo: item.person_addr_card_moo,
						person_addr_card_road: item.person_addr_card_road,
						person_addr_card_district_id: item.person_addr_card_district_id,
						person_addr_card_amphur_id: item.person_addr_card_amphur_id,
						person_addr_card_province_id: item.person_addr_card_province_id
					};
				}));
          }
        });
      },
      minLength: 2,
	  select: function(event, ui){
		event.preventDefault();
		//$("#hid_person_id").val(ui.item.person_id);
		$("#ddl_title").select2("val", ui.item.title_id);
		$("#txt_owner_name").val(ui.item.person_fname);
		$("#txt_owner_last_name").val(ui.item.person_lname);
		$("#txt_owner_addr_no").val(ui.item.person_addr_card_no);
		$("#txt_owner_addr_moo").val(ui.item.person_addr_card_moo);
		$("#txt_owner_addr_road").val(ui.item.person_addr_card_road);
		$("#ddlProvince").select2("val", ui.item.person_addr_card_province_id);
		
		$("#div_owner_amphur").load(base_url+"land/amphur_owner/"+ui.item.person_addr_card_province_id, function(response, status, xhr ){
			$("#ddl_owner_amphur").select2("val", ui.item.person_addr_card_amphur_id);
		});
		
		$("#div_owner_district").load(base_url+"land/district_owner/"+ui.item.person_addr_card_amphur_id, function(response, status, xhr ){
			$("#ddl_owner_distric").select2("val", ui.item.person_addr_card_district_id);
		});
	  }
	  
    });
	
	
String.prototype.replaceAt = function(index, char){
    return this.substr(0, index) + "<span style=\'color:red;\'>" + char + "</span>";
}

$.ui.autocomplete.prototype._renderItem = function(ul, item){
	this.term = this.term.toLowerCase();
	var resultStr = item.label.toLowerCase();
	var t = "";
	
	while (resultStr.indexOf(this.term) != -1)
	{
		var index = resultStr.indexOf(this.term);
		t = t + item.label.replaceAt(index, item.label.slice(index, index + this.term.length));
		resultStr = resultStr.substr(index + this.term.length);
		item.label = item.label.substr(index + this.term.length);
	}
	
	return $("<li></li>").data("item.autocomplete", item).append("<a>" + t + item.label + "</a>").appendTo(ul);
};	


/********** form land **********/
function func_choose_mortagage(){	
	$('#land_mortagage_price').prop('disabled',true);
	if($('#land_mortagage_2').prop('checked')){
		$('#land_mortagage_price').prop('disabled',false);
	}
}

//position land
function func_land_reservation(obj){
	
	$('#txt_unable_reserve').prop('disabled',true);//ไม่สามารถตรวจสอบได้ว่าอยู่ในเขตป่าสงวนหรือไม่
	$('#txt_unable_reserve').val('');
	$('#txt_reserve_location').prop('disabled',true);//อยู่ในเขต ระบุ
	$('#txt_reserve_location').val('');
	
	//คาบเกียว
	$('#txt_reserve_zone').prop('disabled',true);	
	$('#txt_reserve_zone').val('');
	
	$('input[name=rdo_map_check]').prop('disabled',true);
	$('input[name=rdo_map_check]').prop('checked',false);	
	$('#txt_map_check_date').prop('readonly',true);
	$('#txt_map_check_date').val('');
	
	var val = $(obj).val();
	if(val == 0){
		$('#txt_unable_reserve').prop('disabled',false);
	}else
	if(val == 1){
		$('#txt_reserve_location').prop('disabled',false);
	}		
	else if(val == 3){
		$('#txt_reserve_zone').prop('disabled',false);	
		$('input[name=rdo_map_check]').prop('disabled',false);	
	}	
	
}

function func_add_owner(id,fname,lname){	
	
	$('#txtPersonId').val(id);
	$('#m_name').val(fname);
	$('#m_last_name').val(lname);	
	
	//close popup
	$('#txt_search_owner').val('');
	$('#list_owner_person > tbody >tr').remove();
	$('#modal_search_person').modal('hide');
	
	$('#modal_add_person').modal('show');
}

function func_add_owner(){
	$('#action').val('add_new');
	$('#frm_land').submit();
}

function func_delete_owner(id){
	if(confirm('คุณต้องการลบข้อมูลใช่หรือไม่')){
		$('#action').val('del_owner');
		$('#del_owner').val(id);
		$('#frm_land').submit();
	}
	
}

function copy_area(){
	var copy_rai = $('#txt_land_area_mortgage_rai').val();
	var copy_ngan = $('#txt_land_area_mortgage_ngan').val();
	var copy_wah = $('#txt_land_area_mortgage_wah').val();
	
	$('#txt_land_assess_org_rai,#txt_land_assess_emp_rai').val(copy_rai);
	$('#txt_land_assess_org_ngan,#txt_land_assess_emp_ngan').val(copy_ngan);
	$('#txt_land_assess_org_wah,#txt_land_assess_emp_wah').val(copy_wah);

}

function inspect_price(obj){
	//sum price
	var price = 0.00;
	var tr = $(obj).closest("tr");
	if(tr != undefined){
		var rai = $(tr).find('input[name^=txt_land_assess_inspect_rai]').val();
		var ngan = $(tr).find('input[name^=txt_land_assess_inspect_ngan]').val();
		var wa = $(tr).find('input[name^=txt_land_assess_inspect_wa]').val();
		
		var area_rai = calculate_rai(rai,ngan,wa);
		var input = $(obj).val();
		price = parseFloat(input) * area_rai;
	
		
		$(tr).find('input[name^=result_inspect_basic]').val(price);
		$(tr).find('span.result_inspect_price').text(numberWithCommas(price));
	}
	
	
	
	var price_total = 0.00;	
	$('input[name^=result_inspect_basic]').each(function(){
		if($(this).val() != ''){
			price_total = price_total + parseFloat($(this).val());
		}
	});
	
	$('.price_org_toal').text(numberWithCommas(price_total));
	$('#price_org_toal').val(price_total);
	
	//$('input[name^=txt_sum_inspect_price],input[name^=txt_sum_staff_price]').val(numberWithCommas(price_total));
	
}

function calculate_total_area(){
	var total_rai = 0.00;
	var total_gnan = 0.00;
	var total_wa = 0.00;
	
	var arr_rai = [];
	var arr_ngan = [];
	var arr_wa = [];
	
	var row_index = 0;
	
	$('input[name^=txt_land_assess_rai]').each(function(){
		var ele = $(this);

		var ele_ngan = $('input[name^=txt_land_assess_ngan]')[row_index];
		var ele_wa = $('input[name^=txt_land_assess_wa]')[row_index];
		
		if(ele != undefined){
			var val = ele.val();
			arr_rai[row_index] = 0.00;
			arr_ngan[row_index] = 0.00;
			if(val != '' && parseFloat(val)){
				total_rai = total_rai + parseFloat(val);				
				arr_rai[row_index] = parseFloat(val);
			}
			
			if(ele_ngan != undefined){
				var val_ngan = $(ele_ngan).val();
				if(val_ngan != '' && parseFloat(val_ngan)){
					total_gnan = total_gnan+parseFloat(val_ngan);
					arr_ngan[row_index] = parseFloat(val_ngan);
				}
			}
			
			if(ele_wa != undefined){
				var val_wa = $(ele_wa).val();
				if(val_wa != '' && parseFloat(val_wa)){
					total_wa = total_wa+parseFloat(val_wa);
					arr_wa[row_index] = parseFloat(val_wa);
				}
			}
			
			row_index++;
		}
	});
	
	
	
	//calculate total area
	var wa = (parseInt(total_rai) * 400) + (parseInt(total_gnan)*100) + (parseInt(total_wa));
	var unit_rai = Math.floor(wa/400);
	var unit_ngan = Math.floor((wa%400)/100);
	var unit_wa = Math.floor(wa%100);
	
	
	
	//console.log('wa:'+total_wa);
	
	$('#result_area_total').val(wa);
	$('span.result_area_total').text(intWithCommas(unit_rai)+'  ไร่ '+intWithCommas(unit_ngan)+' งาน '+intWithCommas(unit_wa)+' ตารางวา');
	
}

function calculate_price(){
	var total_max = 0.00;
	var total_min = 0.00;
	var total_price = 0.00;	
	
	var arr_price_max = [];
	var arr_price_min = [];
	var arr_price = [];
	var row_index = 0;
	
	
	//calculate total max
	$('input[name^=txt_land_assess_price_pre_max]').each(function(){
		var ele = $(this);
		if(ele != undefined){
			var val = ele.val();
			arr_price_max[row_index] = 0.00;
			if(val != '' && parseFloat(val)){
				total_max = total_max + parseFloat(val);				
				arr_price_max[row_index] = parseFloat(val);
			}
			row_index++;
		}
	});	
	//$('span.result_price_max').text(numberWithCommas(total_max));
	
	row_index = 0;
	//calculate total min
	$('input[name^=txt_land_assess_price_pre_min]').each(function(){
		var ele = $(this);
		if(ele != undefined){
			var val = ele.val();
			arr_price_min[row_index] = 0.00;
			if(val != '' && parseFloat(val)){
				total_min = total_min + parseFloat(val);				
				arr_price_min[row_index] = parseFloat(val);
			}
			row_index++;
		}
	});
	//$('span.result_price_min').text(numberWithCommas(total_min));
	
	row_index = 0;
	//calculate total
	//console.log($('input[name^=land_assess_price]').length);
	$('input[name^=land_assess_price]').each(function(){
		var ele = $(this);
		if(ele != undefined){
			var val = ele.val();
			arr_price[row_index] = 0.00;
			if(val != '' && parseFloat(val)){
				total_price = total_price + parseFloat(val);				
				arr_price[row_index] = parseFloat(val);
			}
			row_index++;
		}
	});
	//$('span.result_price_total').text(numberWithCommas(total_price));
	row_index = 0;
	var arr_rai = [];
	var arr_ngan = [];
	var arr_wa = [];
	$('input[name^=txt_land_assess_rai]').each(function(){
		var ngan = $('input[name^=txt_land_assess_ngan]')[row_index];
		var wa = $('input[name^=txt_land_assess_wa]')[row_index];
		
		arr_rai[row_index] = $(this).val();
		arr_ngan[row_index] = $(ngan).val();
		arr_wa[row_index] = $(wa).val();
		row_index++;
	});
	
	
	var row_total_max = 0.00;
	var row_total_min = 0.00;
	var row_price_total = 0.00;
	
	$arr_result = $('span.result_price');
	$arr_hidden_res = $('input.result_price');
	
	for(i=0;i<arr_price_max.length-1;i++){
		if(arr_price_max[i] != 0.00 && arr_price_min[i] != 0.00 && arr_price[i] != 0.00){
			res = (((arr_price_max[i] +  arr_price_min[i]) / 2.00 ) + arr_price[i]) / 2.00;
			$($arr_result[i]).text(numberWithCommas(res));
			$($arr_hidden_res[i]).val(res.toFixed(2));
			
			//price per rai
			
		}
	}
	
}


function calculate_rai(rai,ngan,wa){
	var total_wah = calculatewWa(rai,ngan,wa);
	return numberWithCommas(parseFloat(total_wah) / 400.00);
}

function calculatewWa(rai,ngan,wa){
	if(isNaN(rai)||rai=='') rai =0;
	if(isNaN(ngan)||ngan=='') ngan =0;
	if(isNaN(wa)||wa=='') wa =0;

	return (parseInt(rai) * 400) + (parseInt(ngan)*100) + (parseInt(wa));
}

function intWithCommas(x) {
	if(isNaN(x)||x=='') x=0.00;
	
	x = x.toFixed(0);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberWithCommas(x) {
	if(isNaN(x)||x=='') x=0.00;
	x = x.toFixed(2);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

