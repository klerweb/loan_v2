var datename = ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"];
var datenamemin = ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."];
var monthname = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
var monthnamemin = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];

//set dropdown
$('select[name^="ddl"]').select2({
    minimumResultsForSearch: -1
});

//edit and update area on land
$('#land_area_rai').keyup(function () {
    var newVal = $(this).val();
    var setObject = $('#txt_land_area_mortgage_rai');
    change_area(setObject,newVal);
    //$('#txt_land_area_mortgage_rai').val($(this).val());
});
$('#txt_land_area_mortgage_rai').keyup(function () {
    var newVal = $(this).val();
    var setObject = $('#land_area_rai');
    change_area(setObject,newVal);
      
});

$('#land_area_ngan').keyup(function () {
    var newVal = $(this).val();
    var setObject = $('#txt_land_area_mortgage_ngan');
    change_area(setObject,newVal);
    //$('#txt_land_area_mortgage_rai').val($(this).val());
});
$('#txt_land_area_mortgage_ngan').keyup(function () {
    var newVal = $(this).val();
    var setObject = $('#land_area_ngan');
    change_area(setObject,newVal);
    //$('#land_area_ngan').val($(this).val());
});

$('#land_area_wah').keyup(function () {
    var newVal = $(this).val();
    var setObject = $('#txt_land_area_mortgage_wah');
    change_area(setObject,newVal);
    //$('#txt_land_area_mortgage_wah').val($(this).val());
});
$('#txt_land_area_mortgage_wah').keyup(function () {
    var newVal = $(this).val();
    var setObject = $('#land_area_wah');
    change_area(setObject,newVal);
    //$('#land_area_wah').val($(this).val());
});

function change_area(obj,newval){
    var cond = $('#ddl_land_area_mortgage').val();
    if(cond=='1')
        $(obj).val(newval);
}
//$(obj).html('<ul class="fa-ul"><li><i class="fa-li fa fa-spinner fa-spin"></i> Loading..</li></ul>');
$("#txt_card_id").autocomplete({
    source: function (request, response) {
         $('.idload').removeClass('hide');
        //setTimeout(function(){
        
        $.ajax({
            url: base_url + 'land/person_service',
            dataType: "json",
            type: 'POST',
            data: {
                thaiid: request.term
            },            
            success: function (data) {
                $('.idload').addClass('hide');
                response($.map(data, function (item) {                   
                    return {
                        label: item.person_thaiid + ' ' + item.person_fname + " " + item.person_lname,
                        value: item.person_thaiid,
                        title: item.title_id,
                        fname: item.person_fname,
                        lname: item.person_lname,
                        birthdate: item.person_birthdate,
                        addr_no: item.person_addr_card_no,
                        addr_moo: item.person_addr_card_moo,
                        addr_road: item.person_addr_card_road,
                        addr_province: item.person_addr_card_province_id,
                        addr_amphur: item.person_addr_card_amphur_id,
                        addr_district: item.person_addr_card_district_id,
                        addr_zipcode: item.person_addr_card_zipcode,
                    }
                }));
            }
        });
        
        //},5000);//timeout
    },
    minLength: 2,
    select: function (event, ui) {
        
        $('#ddl_title').val(ui.item.title).trigger('change');
        $('#txt_owner_name').val(ui.item.fname);
        $('#txt_owner_last_name').val(ui.item.lname);
        $('#txt_owner_addr_no').val(ui.item.addr_no);
        $('#txt_owner_addr_moo').val(ui.item.addr_moo);
        $('#txt_owner_addr_road').val(ui.item.addr_road);
        $('#txt_post').val(ui.item.addr_zipcode);
        $('#hidden_owner_amphur').val(ui.item.addr_amphur);
        $('#hidden_owner_district').val(ui.item.addr_district);
        $('#ddl_owner_province').val(ui.item.addr_province).trigger('change');
        
        loadOwnerAddress();

        var birdate = new Date(ui.item.birthdate);
        var birYear = birdate.getFullYear() + 543;
        var birMonth = birdate.getMonth() + 1;
        var birDay = birdate.getDate();

        $('#txt_owner_birthday').val(birDay + '/' + birMonth + '/' + birYear);
        CaculateAge($('#txt_owner_birthday'), $('#txt_owner_age'));

    },    
    create: function () {       
       
        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
            return $('<li>')
                    .append('<a>' + hightlight(item.label, this.term) + '</a>')
                    .appendTo(ul);
        };
    }
});

birthDatePicker($('#txt_owner_birthday'), $('#txt_owner_age'));

CaculateAge($('#txt_owner_birthday'), $('#txt_owner_age'));


$('#txtDate').CustomDatePicker();

$('[name*=rdo_mortagage]').click(function () {
    var val = $(this).val();
    var ele = $('#txt_land_mortagage');
    if (val == 2) {
        $(ele).removeAttr('readonly');
    } else {
        $(ele).val('').attr('readonly', true);
    }
});

$('[name*=land_condition_price_drop]').click(function () {
    var val = $(this).val();
    var check = $(this).prop("checked");
    var ele = $('#txt_condition_price_drop13');

    if (val == '13' && check) {
        $(ele).removeAttr('readonly');
    } else if (val == '13') {
        $(ele).attr('readonly', true);
    }
});

$('[name*=rdo_reserve]').click(function () {
    var val = $(this).val();

    //value equal 0
    var txt_unable_reserve = $('#txt_unable_reserve');
    $(txt_unable_reserve).val('');
    $(txt_unable_reserve).attr('readonly', true);

    //value equal 1
    var txt_reserve_location = $('#txt_reserve_location');
    $(txt_reserve_location).val('');
    $(txt_reserve_location).attr('readonly', true);

    //value equal 3
    var txt_reserve_zone = $('#txt_reserve_zone');
    $(txt_reserve_zone).val('');
    $(txt_reserve_zone).attr('readonly', true);

    var rdo_map_check = $('[name*=rdo_map_check]');
    $(rdo_map_check).attr('checked', false);	//remove check
    $(rdo_map_check).attr('disabled', true);	//disable

    var txt_map_check_date = $('#txt_map_check_date');
    $(txt_map_check_date).val('');
    $(txt_map_check_date).attr('readonly', true);
    
    
     var land_reservation_other = $('#land_reservation_other');
    $(land_reservation_other).val('');
    $(land_reservation_other).attr('readonly', false);

    if (val == '0') {
        $(txt_unable_reserve).removeAttr('readonly');
    } else if (val == '1') {
        $(txt_reserve_location).removeAttr('readonly');
    } else if (val == '3') {
        $(rdo_map_check).removeAttr('disabled');	//disable
        $(txt_reserve_zone).removeAttr('readonly');
        //$(land_reservation_other).removeAttr('readonly');
    }
});

$('#txt_map_check_date').CustomDatePicker();

$('[name*=rdo_map_check]').click(function () {
    $val = $(this).val();
    var txt_map_check_date = $('#txt_map_check_date');

    $(txt_map_check_date).datepicker("destroy");
    $(txt_map_check_date).val('');
    $(txt_map_check_date).attr('readonly', true);

    if ($val == '2') {
        //console.log('sssssssssss');
        $(txt_map_check_date).removeAttr('readonly');
        $(txt_map_check_date).CustomDatePicker();
    }
});

//dynamic add field
$(".add-more").toggle(function (e) {
    e.preventDefault();
    len = $('[id*=loc_row]').length + 1;

    //set last add btn
    count = len;
    for (i = 1; i < len; i++) {
        id = '#loc_row' + i;
        if ($(id).hasClass('hide'))
            count--;
    }
    if (count < len) {
        tag_i = $(this).children('i');
        $(tag_i).removeClass('fa-plus');
        $(tag_i).addClass('fa-minus');
    }

    has = true;
    for (i = 1; i < len; i++) {
        id = '#loc_row' + i;
        hasclass = $(id).hasClass('hide');
        if (has && hasclass) {
            $(id).removeClass('hide');
            $(this).removeClass('add-more');
            $(this).addClass('remove-more');
            has = false;
            $(".remove-more").on()
        }

    }
}, function (e) {
    e.preventDefault();
    rel = $(this).attr('rel');
    id = '#loc_row' + rel;
    console.log(id);
    $(id).addClass('hide');

    tag_i = $(this).children('i');
    $(tag_i).addClass('fa-plus');
    $(tag_i).removeClass('fa-minus');
});

$('[id*=btn-add-price]').click(function () {
    var set = true;
    $.each($('[id*=table_price] tbody tr'), function () {
        if ($(this).hasClass('hide') && set) {
            $(this).removeClass('hide');
            set = false;
        }
    });
});

$('[id*=btn_inspect_copy]').click(function () {
    $('[name*=tabs]').val(4);
});

calculateAreaTotal();
$('[name*=txt_land_assess_rai],[name*=txt_land_assess_ngan],[name*=txt_land_assess_wa]').keyup(function () {
    calculateAreaTotal();
});

$('[class*=inspect_price_]').keyup(function () {
    var cls = $(this).attr('rel');//get class
    var arr = [0, 0, 0];
    i = 0;
    $.each($('[class*=inspect_price_' + cls + ']'), function () {
        arr[i] = isNaN(parseFloat($(this).val())) ? 0.00 : parseFloat($(this).val());
        i++;
    });

    var price = (((arr[0] + arr[1]) / 2.00) + arr[2]) / 2;

    $('[class*=result_price_' + cls + ']').text(numberWithCommas(price));
    $('[id*=result_price_' + cls + ']').val(price.toFixed(2));


});

$('[class*=row_staff_]').keyup(function () {
    staff_inspect();
});

$('[class*=row_assign_insp_]').keyup(function () {
    assign_inspect();
});

//set tabs
$('#tabs_land li').click(function () {
    //console.log($(this).attr('tabindex'));
    $('[name*=tabs]').val($(this).attr('tabindex'));
});

/* comment ให้เลือกตำแหน่งได้อิสระ */
/*$('#ddl_inspector_1').change(function () {
    var val = $(this).val();
    $.get(base_url + 'land/emp_position/' + val, function (data) {

        $("#ddl_insp_position").select2("destroy");
        $('#ddl_insp_position option[value=' + data + ']').prop('selected', 'selected');


        $("#ddl_insp_position").select2();

    });
});

$('#ddl_inspector_2').change(function () {
    var val = $(this).val();
    $.get(base_url + 'land/emp_position/' + val, function (data) {

        $("#ddl_insp_position_2").select2("destroy");
        $('#ddl_insp_position_2 option[value=' + data + ']').prop('selected', 'selected');


        $("#ddl_insp_position_2").select2();

    });
});*/

function fnDelRow(row) {
    var tr = $('tr[class*=' + row + ']');
    var input = $(tr).find('input');
    $.each(input, function () {
        $(this).val('');
    });
    $(tr).addClass('hide');
    $(tr).focus();

}

function CaculateAge(birthdate, setAge) {
    //
    if (birthdate != undefined && $(birthdate).val() != '') {
        var age = 0;
        var dateBefore = $(birthdate).val();

        if (dateBefore != undefined && dateBefore != '') {
            arr_birdate = dateBefore.split("/");
            dateBefore = (parseInt(arr_birdate[2]) - 543) + '-' + arr_birdate[1] + '-' + arr_birdate[0];
            dob = new Date(dateBefore);
            var today = new Date();
            age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        }
        if (age > 0) {
            jQuery(setAge).val(age);
        } else {
            jQuery(setAge).val(0);
        }
    }
}

function birthDatePicker(obj, setAge) {

    $(obj).datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: datename,
        dayNamesMin: datenamemin,
        monthNames: monthname,
        monthNamesShort: monthnamemin,
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "" && $(this).val() == dateBefore) {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            arr_birdate = dateBefore.split("/");
            dateBefore = arr_birdate[2] + '-' + arr_birdate[1] + '-' + arr_birdate[0];

            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

            //age
            dob = new Date(dateBefore);
            var today = new Date();
            var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

            if (age > 0) {
                jQuery(setAge).val(age);
            } else {
                jQuery(setAge).val(0);
            }
        }
    });
}

function hightlight(str, term) {
    var index = str.indexOf(term);
    var tmp = str.split(term);
    //var inserttmp = 
    var c = '';
    if (index > 0) {
        if (tmp.length > 2) {
            for (i = 0; i < tmp.length; i++) {
                if (tmp[i] == '')
                    c += '<span style="color:red">' + term + '</span>';
                else
                    c += tmp[i] + '<span style="color:red">' + term + '</span>';
            }
        } else {
            c = tmp[0] + '<span style="color:red">' + term + '</span>' + tmp[1];
        }
    } else {
        c = '<span style="color:red">' + term + '</span>' + tmp[1];
    }
    return c;
}

function numberWithCommas(x) {
    if (isNaN(x) || x == '')
        x = 0.00;
    x = x.toFixed(2);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function calcurateAreaRai(rai, ngan, ) {

}

function calculateAreaTotal() {
    var ele_rai = 0;
    var ele_ngan = 0;
    var ele_wah = 0;
    var total = 0;

    $.each($('[name*=txt_land_assess_rai]'), function () {
        ele_rai += isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
    });

    $.each($('[name*=txt_land_assess_ngan]'), function () {
        ele_ngan += isNaN(parseInt($(this).val())) ? 0 : parseInt($(this).val());
    });

    $.each($('[name*=txt_land_assess_wa]'), function () {
        ele_wah += isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val());
    });

    sum_wa = ele_wah % 100;
    sum_ngan = (ele_ngan + parseInt(ele_wah / 100)) % 4;
    sum_rai = ele_rai + parseInt((ele_ngan + parseInt(ele_wah / 100)) / 4);
     
    var ele_rai1 = parseInt($('#land_area_rai').val()*400);
    var ele_ngan1 = parseInt($('#land_area_ngan').val()*100);
    var ele_wah1 = parseFloat($('#land_area_wah').val());
    
    var ele_rai2 = parseInt(sum_rai*400);
    var ele_ngan2 = parseInt(sum_ngan*100);
    var ele_wah2 = parseFloat(sum_wa.toFixed(2));
    
    var sum_area1 = ele_rai1+ele_ngan1+ele_wah1;
    var sum_area2 = ele_rai2+ele_ngan2+ele_wah2;
    
    if(sum_area2 > sum_area1){
    	$('.result_area_total').text('เนื้อที่จำนองไม่ถูกต้อง');
    }else{
    	$('.result_area_total').text(sum_rai + ' ไร่ ' + sum_ngan + ' งาน ' + sum_wa.toFixed(2) + '  ตารางวา  ');
    }
}

function spitThousand(num) {
    if(num> 1000){
        var sp = parseInt(num % 1000);
        return parseInt(num - sp);
    }else{
        return num;
    }
}


var init_insp = $('[name*=txt_sum_inspect_price]').val();
if(init_insp !== undefined )
    init_insp = init_insp.replace(',','');

//if(!isNaN(init_insp)){
    if((init_insp) < 1){
        staff_inspect();
        //console.log('set1');
    }
//}
staff_inspect();
function staff_inspect() {
    var sumtotal = 0.00;
    $.each($('[class*=row_staff_total_]'), function () {
        var row = $(this).attr('rel');
        if (row != undefined) {
            var cls = '[class*=row_staff_' + row + ']';
            var disp = '[class*=disp_row_staff_total_' + row + ']';
            var val = '[class*=row_staff_total_' + row + ']';
            var arr = [];
            $.each($(cls), function () {
                //console.log($(this).val());
                arr.push($(this).val());
            });
            if (arr.length == 4) {
                var area = (parseFloat(arr[0]) * 400) + (parseFloat(arr[1] * 100)) + (parseFloat(arr[2]));
                var price = parseFloat(arr[3]);
                var total = parseFloat(area / 400.00) * price;
                $(disp).text(numberWithCommas(total));
                $(val).val(total.toFixed(2));
               // console.log('area='+area+'price='+price+'parseFloat='+parseFloat(area / 400.00)+'total='+total);
                sumtotal += total;
            }
        }
    });

    //summary total
    $('[name*=price_org_toal]').val(sumtotal.toFixed(2));
    $('[class*=price_org_toal]').text(numberWithCommas(sumtotal));
    $('[name*=txt_sum_inspect_price]').val(spitThousand(sumtotal.toFixed(2)));
}

var init_staff = $('[name*=txt_sum_staff_price]').val();
if(init_staff !== undefined)
    init_staff = init_staff.replace(',','');

//if(!isNaN(init_staff)){
    if((init_staff) < 1){
        assign_inspect();
        console.log(init_staff);
    }
//}
assign_inspect();
function assign_inspect() {
    var sumtotal = 0.00;

    $.each($('[class*=total_assign_row_inpc_]'), function () {
        var row = $(this).attr('rel');
        if (row != undefined) {
            var cls = '[class*=row_assign_insp_' + row + ']';
            var disp = '[class*=result_assign_price_' + row + ']';
            var hidden_val = '[class*=total_assign_row_inpc_' + row + ']';
            var arr = [];
            $.each($(cls), function () {
                //console.log($(this).val());
                arr.push($(this).val());
            });

            if (arr.length == 4) {
                var area = (parseFloat(arr[0]) * 400) + (parseFloat(arr[1] * 100)) + (parseFloat(arr[2]));
                var price = parseFloat(arr[3]);
                var total = parseFloat(area / 400.00) * price;
                $(disp).text(numberWithCommas(total));

                $(hidden_val).val(total.toFixed(2));

                sumtotal += total;
            }
        }
    });
    

    //summary total
    $('[name*=price_org_assign_toal]').val(sumtotal.toFixed(2));
    $('[class*=price_org_assign_toal]').text(numberWithCommas(sumtotal));
    $('[name*=txt_sum_staff_price]').val(spitThousand(sumtotal.toFixed(2)));
}

