 $.validator.addMethod(
        "thaiid", 
        function (value, element){
            var val = value.replace(/-/g,'');
            console.log(val);
            if(val.length != 13)
                return false;
            
            if (/^[1-9][0-9]{12}$/g.test(val))
            {
                return true;
            }else{
                return false;
            }
        },
        jQuery.validator.format("รหัสประชาชนไม่ถูกต้อง")
    );
    
    $.validator.addMethod(
        "numberWithComma", 
        function (value, element){
            var value = value.replace('-','');
           
            
            if ( /^(\d+|\d+,\d{1,2})$/g.test(value))
            {
                return true;
            }else{
                return false;
            }
        },
        jQuery.validator.format("Please enter a value greater than or equal to 0.")
    );
    
    $.validator.addMethod('Decimal', function(value, element) {
        return this.optional(element) || /^[0-9,]+(\.\d{0,2})?$/.test(value); 
    }, "You must include two decimal places");

$("#frm_land_owner").validate({
     ignore: [],
    rules: {
        ddl_title: {
            required: true
        },
        ddl_owner_province: {
            required: true
        },
        ddl_amphur_owner: {
            required: true
        },
        ddl_district_owner: {
            required: true
        },
        ddl_title_father: {
            required: true
        },
        ddl_title_mather: {
            required: true
        },
        ddl_relation: {
            required: true
        },
        txt_card_id : {
                required: true,
                thaiid : true                
            }        
    },

    highlight: function (element) {
        $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
    },
    success: function (element) {
        $(element).closest(".form-group").removeClass("has-error");
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$("#frm_land").validate({
    rules: {
        ddl_land_type: {
            required: true
        },
        ddl_insp_position: {
            required: true
        },
        ddl_insp_position_2: {
            required: true
        },
        ddl_owner_province: {
            required: true
        },
        ddl_amphur_owner: {
            required: true
        },
        ddl_district_owner: {
            required: true
        }

    },

    highlight: function (element) {
        $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
    },
    success: function (element) {
        $(element).closest(".form-group").removeClass("has-error");
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});


jQuery.validator.addClassRules({
    css_digit: {
        
        digits: true
    },
    css_decimal: {        
        number: true       
    },
    css_req: {
        required: true
    },
    max4: {
        max: 4,
        digits: true
    },
    max100: {
        max: 100,
        digits: true
    },
    max100w: {
        max: 100,
        number:true,
		Decimal: true,
		required: true
    },
    
});

$('#txt_card_id').keypress(function(){
    //autoTab(this)
});

function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
        var pattern=new String("_-____-_____-_-__"); // กำหนดรูปแบบในนี้
        var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){           
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);           
        }
}