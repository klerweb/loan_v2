jQuery(document).ready(function () {
    
    $("#loan_doc_date").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

            //age
            dob = new Date(dateBefore);
            var today = new Date();
            var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

            if (age > 0) {
                jQuery('#person_age').val(age);
            } else {
                jQuery('#person_age').val(0);
            }
        }
    });
    
    // validation form_doc
    jQuery("#form_doc").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
});

