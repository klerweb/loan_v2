jQuery(document).ready(function () {
    /*province, amphur, district for land_addr*/    
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id&zipcode_id=land_addr_zipcode';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';    
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;
    
    // validation form_land_occupy
    jQuery("#form_land_occupy").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
    
    
    
    /*province, amphur, district for land_addr*/
    $("#land_addr_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#land_addr_province_id").change(function () {
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + param_url_pad_land_addr);
    });

    $("#land_addr_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#land_addr_amphur_id").change(function () {
        $("#div_land_addr_district_id").load(base_url + "loan/district/" + $("#land_addr_amphur_id").val() + param_url_pad_land_addr);
    });

    $("#land_addr_district_id").select2({
        minimumResultsForSearch: -1
    });
});

