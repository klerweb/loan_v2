jQuery(document).ready(function () {
    
    //loan_activity_income1
    jQuery('#income1_same').change(function(){
        if ($('#income1_same').is(':checked')) {            
            $('#loan_activity_income1').val(jQuery('#loan_activity_income2').val());
            $('#loan_activity_income1').prop('readonly', true);
        }else{
            $('#loan_activity_income1').prop('readonly', false);
        }
    });
    
    //loan_activity_income3
    jQuery('#income3_same').change(function(){
        if ($('#income3_same').is(':checked')) {            
            $('#loan_activity_income3').val(jQuery('#loan_activity_income1').val());
            $('#loan_activity_income3').prop('readonly', true);
        }else{
            $('#loan_activity_income3').prop('readonly', false);
        }
    });
    
    //loan_activity_expenditure1
    jQuery('#expenditure1_same').change(function(){
        if ($('#expenditure1_same').is(':checked')) {            
            $('#loan_activity_expenditure1').val(jQuery('#loan_activity_expenditure2').val());
            $('#loan_activity_expenditure1').prop('readonly', true);
        }else{
            $('#loan_activity_expenditure1').prop('readonly', false);
        }
    });
    
    //loan_activity_expenditure3
    jQuery('#expenditure3_same').change(function(){
        if ($('#expenditure3_same').is(':checked')) {            
            $('#loan_activity_expenditure3').val(jQuery('#loan_activity_expenditure1').val());
            $('#loan_activity_expenditure3').prop('readonly', true);
        }else{
            $('#loan_activity_expenditure3').prop('readonly', false);
        }
    });


    // validation form_income
    jQuery("#form_income").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
});
