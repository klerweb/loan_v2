jQuery(document).ready(function () {
    
    // validation form_loan_asset
    jQuery("#form_loan_asset").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
});

