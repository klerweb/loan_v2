jQuery(document).ready(function () {
    /*province, amphur, district addr_card for person*/
    var person_pad_id_addr_card_list = 'province_id=person_addr_card_province_id&amphur_id=person_addr_card_amphur_id&district_id=person_addr_card_district_id&zipcode_id=person_addr_card_zipcode';
    var person_pad_name_addr_card_list = 'province_name=person[person_addr_card_province_id]&amphur_name=person[person_addr_card_amphur_id]&district_name=person[person_addr_card_district_id]';
    var param_url_person_pad_addr_card = "/?" + person_pad_id_addr_card_list + "&" + person_pad_name_addr_card_list;

    /*province, amphur, district addr_card for person*/
    var person_pad_id_addr_pre_list = 'province_id=person_addr_pre_province_id&amphur_id=person_addr_pre_amphur_id&district_id=person_addr_pre_district_id&zipcode_id=person_addr_pre_zipcode';
    var person_pad_name_addr_pre_list = 'province_name=person[person_addr_pre_province_id]&amphur_name=person[person_addr_pre_amphur_id]&district_name=person[person_addr_pre_district_id]';
    var person_pad_class_addr_pre_list = 'province_class=person_addr_pre&amphur_class=person_addr_pre&district_class=person_addr_pre';
    var param_url_person_pad_addr_pre = "/?" + person_pad_id_addr_pre_list + "&" + person_pad_name_addr_pre_list + "&" + person_pad_class_addr_pre_list;
    
    /*dateToday*/
    //var dateToday = new Date();

    //person_birthdate    
    $("#person_birthdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,        
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

            //age            
            var arrayDateAge = dateText.split("/");
            var d1 = new Date(arrayDateAge[2] + "-" + arrayDateAge[1] + "-" + arrayDateAge[0]);
            var d2 = new Date();
            var diff = d2.getTime() - d1.getTime();
            var age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));

            if (age > 0) {
                jQuery('#person_age').val(age);
            } else {
                jQuery('#person_age').val(0);
            }
        }
    });

    //person_card_startdate
    $("#person_card_startdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function (selectedDate) {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            
            //new mindate
            var arrayDate = selectedDate.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) - 543;
            selectedDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
            $("#person_card_expiredate").datepicker("option","minDate", selectedDate);
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //person_card_expiredate
    $("#person_card_expiredate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        //minDate: new Date($('#person_card_startdate').val()),
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //btn_person_search
    jQuery('#person_thaiid').focus(function () {
        $("#thaiid_error").css("display", "none");
        $("#person_thaiid_form .control-label").css("color", "#65707D");
        $("#person_thaiid_form input[type=text]").css("border", "1px solid #CCCCCC");
        $("#person_thaiid_form input[type=text]").css("color", "#65707D");
    });
    
    jQuery('#person_thaiid').blur(function () {
         if (checkID(jQuery('#person_thaiid').val()) === false) {
            //alert('กรอกเลขที่บัตรประจำตัวประชาชนไม่ถูกต้อง');            
            $("#thaiid_error").css("display", "initial");
            $("#person_thaiid_form .control-label").css("color", "#A94442");
            $("#person_thaiid_form input[type=text]").css("border", "1px solid #A94442");
            $("#person_thaiid_form input[type=text]").css("color", "#A94442");

            jQuery('#person_thaiid').val('');
            return false;
         } else if ($("#person_thaiid").val() == $("#loan_person_thaiid").val()) {
             $("#thaiid_error2").css("display", "initial");
             $("#person_thaiid_form .control-label").css("color", "#A94442");
             $("#person_thaiid_form input[type=text]").css("border", "1px solid #A94442");
             $("#person_thaiid_form input[type=text]").css("color", "#A94442");

             jQuery('#person_thaiid').val('');
             return false;
         }
         else if (($("#person_thaiid").val() == $("#loan_spouse_thaiid").val()) && ($("#loan_spouse_thaiid").val() != '')) {
             $("#thaiid_error2").css("display", "initial");
             $("#person_thaiid_form .control-label").css("color", "#A94442");
             $("#person_thaiid_form input[type=text]").css("border", "1px solid #A94442");
             $("#person_thaiid_form input[type=text]").css("color", "#A94442");

             jQuery('#person_thaiid').val('');
             return false;
         }
         
        
        $.get("loan/search_person", {person_thaiid: jQuery('#person_thaiid').val()}).done(function (data) {
            var arr = [];
            json = JSON.stringify(eval('(' + data + ')')); //convert to json string
            arr = $.parseJSON(json); //convert to javascript array

            if (arr['person_id'] == null) {
                jQuery('#res_success_person_search').css("display", "none");
                jQuery('#res_fail_person_search').css("display", "inline-block");
            } else {
                jQuery('#res_success_person_search').css("display", "inline-block");
                jQuery('#res_fail_person_search').css("display", "none");
                
                //jQuery('#person_id').val(arr['person_id']);
                jQuery('#title_id').val(arr['title_id']);
                jQuery('#person_fname').val(arr['person_fname']);
                jQuery('#person_lname').val(arr['person_lname']);
                jQuery('#person_birthdate').val(arr['person_birthdate']);
                jQuery("#sex_id_" + arr['sex_id']).attr('checked', 'checked');
                jQuery('#race_id').val(arr['race_id']);
                jQuery('#nationality_id').val(arr['nationality_id']);
                jQuery('#religion_id').val(arr['religion_id']);
                jQuery('#person_thaiid').val(arr['person_thaiid']);
                jQuery('#person_card_startdate').val(arr['person_card_startdate']);
                jQuery('#person_card_expiredate').val(arr['person_card_expiredate']);
                jQuery('#person_addr_card_no').val(arr['person_addr_card_no']);
                jQuery('#person_addr_card_moo').val(arr['person_addr_card_moo']);
                jQuery('#person_addr_card_road').val(arr['person_addr_card_road']);
    //            jQuery('#person_addr_card_district_id').val(arr['person_addr_card_district_id']);
    //            jQuery('#person_addr_card_amphur_id').val(arr['person_addr_card_amphur_id']);
    //            jQuery('#person_addr_card_province_id').val(arr['person_addr_card_province_id']);
                jQuery('#person_addr_pre_no').val(arr['person_addr_pre_no']);
                jQuery('#person_addr_pre_moo').val(arr['person_addr_pre_moo']);
                jQuery('#person_addr_pre_road').val(arr['person_addr_pre_road']);
    //            jQuery('#person_addr_pre_district_id').val(arr['person_addr_pre_district_id']);
    //            jQuery('#person_addr_pre_amphur_id').val(arr['person_addr_pre_amphur_id']);
    //            jQuery('#person_addr_pre_province_id').val(arr['person_addr_pre_province_id']);
                jQuery('#person_phone').val(arr['person_phone']);
                jQuery('#person_mobile').val(arr['person_mobile']);
                jQuery('#person_email').val(arr['person_email']);
                jQuery('#career_id').val(arr['career_id']);
                jQuery('#person_career_other').val(arr['person_career_other']);
                jQuery("#status_married_id_" + arr['status_married_id']).attr('checked', 'checked');
                jQuery('#person_income_per_month').val(arr['person_income_per_month']);
                jQuery('#person_expenditure_per_month').val(arr['person_expenditure_per_month']);
                jQuery('#person_addr_card_zipcode').val(arr['person_addr_card_zipcode']);
                jQuery('#person_addr_pre_zipcode').val(arr['person_addr_pre_zipcode']);

                //cal age
                var person_birthdate = jQuery('#person_birthdate').val();
                if (person_birthdate) {
                    res_arr = person_birthdate.split("/");
                    person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];
                    
                    dob = new Date(person_birthdate);
                    var today = new Date();
                    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                    if (age > 0) {
                        jQuery('#person_age').val(age);
                    } else {
                        jQuery('#person_age').val(0);
                    }
                }

                //addr_card for person
                    if (arr['person_addr_card_province_id'] != null) {
                        $("#div_person_addr_card_province_id").load(base_url + "loan/province/" + arr['person_addr_card_province_id'] + param_url_person_pad_addr_card);
                        $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_card_province_id'] + "/0" + param_url_person_pad_addr_card);
                    }
                    if (arr['person_addr_card_province_id'] != null && arr['person_addr_card_amphur_id'] != null) {
                        $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_card_province_id'] + "/" + arr['person_addr_card_amphur_id'] + param_url_person_pad_addr_card);
                        $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/0" + param_url_person_pad_addr_card);
                    }
                    if (arr['person_addr_card_amphur_id'] != null && arr['person_addr_card_district_id'] != null) {
                        $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/" + arr['person_addr_card_district_id'] + param_url_person_pad_addr_card);
                    }

                    //addr_pre for person
                    if (arr['person_addr_pre_province_id'] != null) {
                        $("#div_person_addr_pre_province_id").load(base_url + "loan/province/" + arr['person_addr_pre_province_id'] + param_url_person_pad_addr_pre);
                        $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_pre_province_id'] + "/0" + param_url_person_pad_addr_pre);
                    }
                    if (arr['person_addr_pre_province_id'] != null && arr['person_addr_pre_amphur_id'] != null) {
                        $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_pre_province_id'] + "/" + arr['person_addr_pre_amphur_id'] + param_url_person_pad_addr_pre);
                        $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/0" + param_url_person_pad_addr_pre);
                    }
                    if (arr['person_addr_pre_amphur_id'] != null && arr['person_addr_pre_district_id'] != null) {
                        $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/" + arr['person_addr_pre_district_id'] + param_url_person_pad_addr_pre);
                    }
            }                
        });
    });

    //cal age
    var person_birthdate = jQuery('#person_birthdate').val();
    if (person_birthdate) {
        res_arr = person_birthdate.split("/");
        person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];
        
        dob = new Date(person_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#person_age').val(age);
        } else {
            jQuery('#person_age').val(0);
        }
    }

    // validation form_loan_co
    jQuery("#form_loan_co").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });


    /*province, amphur, district addr_card for person*/
    $("#person_addr_card_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#person_addr_card_province_id").change(function () {
        $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_card_province_id").val() + param_url_person_pad_addr_card);
        $("#div_person_addr_card_district_id").load(base_url + "loan/district" + param_url_person_pad_addr_card);
    });

    $("#person_addr_card_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#person_addr_card_amphur_id").change(function () {
        $("#div_person_addr_card_district_id").load(base_url + "loan/district/" + $("#person_addr_card_amphur_id").val() + param_url_person_pad_addr_card);
    });

    $("#person_addr_card_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district addr_pre for person*/
    $("#person_addr_pre_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#person_addr_pre_province_id").change(function () {
        $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_pre_province_id").val() + param_url_person_pad_addr_pre);
        $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + param_url_person_pad_addr_pre);
    });

    $("#person_addr_pre_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#person_addr_pre_amphur_id").change(function () {
        $("#div_person_addr_pre_district_id").load(base_url + "loan/district/" + $("#person_addr_pre_amphur_id").val() + param_url_person_pad_addr_pre);
    });

    $("#person_addr_pre_district_id").select2({
        minimumResultsForSearch: -1
    });


    $('#person_addr_same').change(function () {
        if ($(this).is(":checked")) {
            $('.person_addr_pre').val('');
            $('.person_addr_pre').attr('readonly', true);
            $('.person_addr_pre').prop('required', false);

            $("#person_addr_pre_province_id").select2("val", "");
            $("#person_addr_pre_amphur_id").select2("val", "");
            $("#person_addr_pre_district_id").select2("val", "");
        } else {
            $('.person_addr_pre').attr('readonly', false);
            $('.person_addr_pre').prop('required', true);
        }
    });
    
    /*default sex_id by title_id*/
    $('#title_id').change(function () {
        var title_id = jQuery('#title_id').val();
        if (title_id == 1) {
            $("#sex_id_1").attr('checked', 'checked');
        } else if (title_id == '' || title_id == 0) {
            //$(".sex_id").checked = false; 
        } else {
            $("#sex_id_2").attr('checked', 'checked');
        }
    });
});

function checkID(id){
    if (id.length != 13)
        return false;
    for (i = 0, sum = 0; i < 12; i++)
        sum += parseFloat(id.charAt(i)) * (13 - i);
    if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12)))
        return false;
    return true;
}    