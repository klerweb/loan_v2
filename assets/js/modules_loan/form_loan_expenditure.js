jQuery(document).ready(function () {
    
    //loan_expenditure_amount2
    jQuery('#expenditure1_same').change(function(){
        if ($('#expenditure1_same').is(':checked')) {            
            $('#loan_expenditure_amount1').val(jQuery('#loan_expenditure_amount2').val());
            $('#loan_expenditure_amount1').prop('readonly', true);
        }else{
            $('#loan_expenditure_amount1').prop('readonly', false);
        }
    });
    
    //loan_expenditure_amount3
    jQuery('#expenditure3_same').change(function(){
        if ($('#expenditure3_same').is(':checked')) {            
            $('#loan_expenditure_amount3').val(jQuery('#loan_expenditure_amount1').val());
            $('#loan_expenditure_amount3').prop('readonly', true);
        }else{
            $('#loan_expenditure_amount3').prop('readonly', false);
        }
    });
    
    // validation form_expenditure
    jQuery("#form_expenditure").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
    
    /*default loan_expenditure_name by loan_expenditure_type*/
    $('.loan_expenditure_type').click(function () {
        var loan_expenditure_type_id = $(this).val();
        
        if (loan_expenditure_type_id == 1) {
            $('#loan_expenditure_name').val('ครัวเรือน');
            $('#loan_expenditure_name').prop('readonly', true);
        }else {            
            $('#loan_expenditure_name').prop('readonly', false);
        }
    });
});
