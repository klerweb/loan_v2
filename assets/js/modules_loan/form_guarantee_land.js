jQuery(document).ready(function () {
    /*province, amphur, district for land_addr*/    
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';    
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;
    
    /*province, amphur, district for building*/    
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';    
    var param_url_pad_building= "/?" + pad_id_building_list + "&" + pad_name_building_list;
    
    // validation form_guarantee_land
    jQuery("#form_guarantee_land").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
    
    //loan_guarantee_land_relief_enddate
//    jQuery('#loan_guarantee_land_relief_enddate').datepicker({dateFormat: 'yy-mm-dd'});
    $("#loan_guarantee_land_relief_enddate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });
    
    /*province, amphur, district for land_addr*/
    $("#land_addr_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#land_addr_province_id").change(function () {
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + param_url_pad_land_addr);
    });

    $("#land_addr_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#land_addr_amphur_id").change(function () {
        $("#div_land_addr_district_id").load(base_url + "loan/district/" + $("#land_addr_amphur_id").val() + param_url_pad_land_addr);
    });

    $("#land_addr_district_id").select2({
        minimumResultsForSearch: -1
    });
    
    /*province, amphur, district for building*/
    $("#building_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#building_province_id").change(function () {
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + param_url_pad_building);
    });

    $("#building_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#building_amphur_id").change(function () {
        $("#div_building_district_id").load(base_url + "loan/district/" + $("#building_amphur_id").val() + param_url_pad_building);
    });

    $("#building_district_id").select2({
        minimumResultsForSearch: -1
    });

    
});