jQuery(document).ready(function () {
    
    // validation form_loan_objective
    jQuery("#form_loan_objective").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });
});
