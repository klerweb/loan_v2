jQuery(document).ready(function () {
	//$("#person_thaiid").mask("9-9999-99999-99-9");

    /*province, amphur, district addr_card for person*/
    var person_pad_id_addr_card_list = 'province_id=person_addr_card_province_id&amphur_id=person_addr_card_amphur_id&district_id=person_addr_card_district_id&zipcode_id=person_addr_card_zipcode';
    var person_pad_name_addr_card_list = 'province_name=person[person_addr_card_province_id]&amphur_name=person[person_addr_card_amphur_id]&district_name=person[person_addr_card_district_id]';
    var person_pad_class_addr_card_list = 'province_class=person_addr_card&amphur_class=person_addr_card&district_class=person_addr_card';
    var param_url_person_pad_addr_card = "/?" + person_pad_id_addr_card_list + "&" + person_pad_name_addr_card_list + "&" + person_pad_class_addr_card_list;

    /*province, amphur, district addr_card for person*/
    var person_pad_id_addr_pre_list = 'province_id=person_addr_pre_province_id&amphur_id=person_addr_pre_amphur_id&district_id=person_addr_pre_district_id&zipcode_id=person_addr_pre_zipcode';
    var person_pad_name_addr_pre_list = 'province_name=person[person_addr_pre_province_id]&amphur_name=person[person_addr_pre_amphur_id]&district_name=person[person_addr_pre_district_id]';
    var person_pad_class_addr_pre_list = 'province_class=person_addr_pre&amphur_class=person_addr_pre&district_class=person_addr_pre';
    var param_url_person_pad_addr_pre = "/?" + person_pad_id_addr_pre_list + "&" + person_pad_name_addr_pre_list + "&" + person_pad_class_addr_pre_list;

    /*province, amphur, district addr_pre for spouse*/
    var spouse_pad_id_addr_pre_list = 'province_id=spouse_addr_pre_province_id&amphur_id=spouse_addr_pre_amphur_id&district_id=spouse_addr_pre_district_id&zipcode_id=spouse_addr_pre_zipcode';
    var spouse_pad_name_addr_pre_list = 'province_name=spouse[person_addr_pre_province_id]&amphur_name=spouse[person_addr_pre_amphur_id]&district_name=spouse[person_addr_pre_district_id]';
    var spouse_pad_class_addr_pre_list = 'province_class=spouse_addr_pre&amphur_class=spouse_addr_pre&district_class=spouse_addr_pre';
    var param_url_spouse_pad_addr_pre = "/?" + spouse_pad_id_addr_pre_list + "&" + spouse_pad_name_addr_pre_list + "&" + spouse_pad_class_addr_pre_list;

    /*province, amphur, district person_land_addr for person*/
    var person_pad_id_person_land_addr_list = 'province_id=person_land_addr_province_id&amphur_id=person_land_addr_amphur_id&district_id=person_land_addr_district_id&zipcode_id=person_land_addr_addr_zipcode';
    var person_pad_name_person_land_addr_list = 'province_name=person[person_land_addr_province_id]&amphur_name=person[person_land_addr_amphur_id]&district_name=person[person_land_addr_district_id]';
    var person_pad_class_person_land_addr_list = 'province_class=person_land_addr&amphur_class=person_land_addr&district_class=person_land_addr';
    var param_url_person_pad_person_land_addr = "/?" + person_pad_id_person_land_addr_list + "&" + person_pad_name_person_land_addr_list + "&" + person_pad_class_person_land_addr_list;

    $("#loan_year").change(function(){
    	$("#divProject").load(base_url+"loan/form_person/project/"+$("#loan_year").val());
    });

    //person_birthdate
    $("#person_birthdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(),
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

            //age
            var arrayDateAge = dateText.split("/");
            var d1 = new Date(arrayDateAge[2] + "-" + arrayDateAge[1] + "-" + arrayDateAge[0]);
            var d2 = new Date();
            var diff = d2.getTime() - d1.getTime();
            var age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));

            if (age > 0) {
                jQuery('#person_age').val(age);
            } else {
                jQuery('#person_age').val(0);
            }

            if (age < 18) {
                alert('อายุต่ำกว่า 18 ปี ห้ามทำรายการ');
                $(this).val('');
                jQuery('#person_age').val('');
            }

            if (age > 65) {
                alert('อายุ 65 ปีขึ้นไปต้องมีผู้กู้ร่วม');
            }

        }
    });

    //person_card_startdate
    $("#person_card_startdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function (selectedDate) {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }

            //new mindate
            var arrayDate = selectedDate.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) - 543;
            selectedDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
            $("#person_card_expiredate").datepicker("option", "minDate", selectedDate);
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //person_card_expiredate
    $("#person_card_expiredate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //loan_date
//    jQuery('#loan_date').datepicker({dateFormat: 'yy-mm-dd'});
    $("#loan_date").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //spouse_birthdate
    $("#spouse_birthdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        maxDate: new Date(),
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

            //age
            var arrayDateAge = dateText.split("/");
            var d1 = new Date(arrayDateAge[2] + "-" + arrayDateAge[1] + "-" + arrayDateAge[0]);
            var d2 = new Date();
            var diff = d2.getTime() - d1.getTime();
            var age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));

            if (age > 0) {
                jQuery('#spouse_age').val(age);
            } else {
                jQuery('#spouse_age').val(0);
            }
        }
    });

    //spouse_card_startdate
    $("#spouse_card_startdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function (selectedDate) {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }

            //new mindate
            var arrayDate = selectedDate.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) - 543;
            selectedDate = arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2];
            $("#spouse_card_expiredate").datepicker("option", "minDate", selectedDate);
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //spouse_card_expiredate
    $("#spouse_card_expiredate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //person_thaiid search
    jQuery('#person_thaiid').focus(function () {
        $("#thaiid_error").css("display", "none");
        $("#thaiid_error2").css("display", "none");
        $("#person_thaiid_form .control-label").css("color", "#65707D");
        $("#person_thaiid_form input[type=text]").css("border", "1px solid #CCCCCC");
        $("#person_thaiid_form input[type=text]").css("color", "#65707D");
    });

    jQuery('#person_thaiid').blur(function () {
        if (checkID(jQuery('#person_thaiid').val()) === false) {
            //alert('กรอกเลขที่บัตรประจำตัวประชาชนไม่ถูกต้อง');
            $("#thaiid_error").css("display", "initial");
            $("#person_thaiid_form .control-label").css("color", "#A94442");
            $("#person_thaiid_form input[type=text]").css("border", "1px solid #A94442");
            $("#person_thaiid_form input[type=text]").css("color", "#A94442");

            jQuery('#person_thaiid').val('');
            return false;
        } else if ($("#person_thaiid").val() == $("#spouse_thaiid").val()) {
            $("#thaiid_error2").css("display", "initial");
            $("#person_thaiid_form .control-label").css("color", "#A94442");
            $("#person_thaiid_form input[type=text]").css("border", "1px solid #A94442");
            $("#person_thaiid_form input[type=text]").css("color", "#A94442");

            jQuery('#person_thaiid').val('');
            return false;
        }

        $.get("loan/search_person", {person_thaiid: jQuery('#person_thaiid').val()}).done(function (data) {
            var arr = [];
            json = JSON.stringify(eval('(' + data + ')')); //convert to json string
            arr = $.parseJSON(json); //convert to javascript array

            if (arr['person_id'] == null) {
                jQuery('#res_success_person_search').css("display", "none");
                jQuery('#res_fail_person_search').css("display", "inline-block");
            } else {
                jQuery('#res_success_person_search').css("display", "inline-block");
                jQuery('#res_fail_person_search').css("display", "none");

                //set value
                //jQuery('#person_id').val(arr['person_id']);
                jQuery('#title_id').val(arr['title_id']);
                jQuery('#person_fname').val(arr['person_fname']);
                jQuery('#person_lname').val(arr['person_lname']);
                jQuery('#person_birthdate').val(arr['person_birthdate']);
                jQuery("#sex_id_" + arr['sex_id']).attr('checked', 'checked');
                jQuery('#race_id').val(arr['race_id']);
                jQuery('#nationality_id').val(arr['nationality_id']);
                jQuery('#religion_id').val(arr['religion_id']);
                jQuery('#person_thaiid').val(arr['person_thaiid']);
                jQuery('#person_card_startdate').val(arr['person_card_startdate']);
                jQuery('#person_card_expiredate').val(arr['person_card_expiredate']);
                jQuery('#person_addr_card_no').val(arr['person_addr_card_no']);
                jQuery('#person_addr_card_moo').val(arr['person_addr_card_moo']);
                jQuery('#person_addr_card_road').val(arr['person_addr_card_road']);
//                jQuery('#person_addr_card_district_id').val(arr['person_addr_card_district_id']);
//                jQuery('#person_addr_card_amphur_id').val(arr['person_addr_card_amphur_id']);
//                jQuery('#person_addr_card_province_id').val(arr['person_addr_card_province_id']);
                jQuery('#person_addr_pre_no').val(arr['person_addr_pre_no']);
                jQuery('#person_addr_pre_moo').val(arr['person_addr_pre_moo']);
                jQuery('#person_addr_pre_road').val(arr['person_addr_pre_road']);
//                jQuery('#person_addr_pre_district_id').val(arr['person_addr_pre_district_id']);
//                jQuery('#person_addr_pre_amphur_id').val(arr['person_addr_pre_amphur_id']);
//                jQuery('#person_addr_pre_province_id').val(arr['person_addr_pre_province_id']);
                jQuery('#person_phone').val(arr['person_phone']);
                jQuery('#person_mobile').val(arr['person_mobile']);
                jQuery('#person_email').val(arr['person_email']);
                jQuery('#career_id').val(arr['career_id']);
                jQuery('#person_career_other').val(arr['person_career_other']);
                jQuery("#status_married_id_" + arr['status_married_id']).attr('checked', 'checked');

                //cal age
                var person_birthdate = jQuery('#person_birthdate').val();
                if (person_birthdate) {
                    res_arr = person_birthdate.split("/");
                    person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];

                    dob = new Date(person_birthdate);
                    var today = new Date();
                    var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                    if (age > 0) {
                        jQuery('#person_age').val(age);
                    } else {
                        jQuery('#person_age').val(0);
                    }
                }

                //addr_card for person
                // if (arr['person_addr_card_province_id'] != null) {
                //     $("#div_person_addr_card_province_id").load(base_url + "loan/province/" + arr['person_addr_card_province_id'] + param_url_person_pad_addr_card);
                //     $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_card_province_id'] + "/0" + param_url_person_pad_addr_card);
                // }
                // if (arr['person_addr_card_province_id'] != null && arr['person_addr_card_amphur_id'] != null) {
                //     $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_card_province_id'] + "/" + arr['person_addr_card_amphur_id'] + param_url_person_pad_addr_card);
                //     $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/0" + param_url_person_pad_addr_card);
                // }
                // if (arr['person_addr_card_amphur_id'] != null && arr['person_addr_card_district_id'] != null) {
                //     $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/" + arr['person_addr_card_district_id'] + param_url_person_pad_addr_card);
                // }

                //addr_pre for person
                if (arr['person_addr_pre_province_id'] != null) {
                    $("#div_person_addr_pre_province_id").load(base_url + "loan/province/" + arr['person_addr_pre_province_id'] + param_url_person_pad_addr_pre);
                    $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_pre_province_id'] + "/0" + param_url_person_pad_addr_pre);
                }
                if (arr['person_addr_pre_province_id'] != null && arr['person_addr_pre_amphur_id'] != null) {
                    $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + arr['person_addr_pre_province_id'] + "/" + arr['person_addr_pre_amphur_id'] + param_url_person_pad_addr_pre);
                    $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/0" + param_url_person_pad_addr_pre);
                }
                if (arr['person_addr_pre_amphur_id'] != null && arr['person_addr_pre_district_id'] != null) {
                    $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/" + arr['person_addr_pre_district_id'] + param_url_person_pad_addr_pre);
                }

                //person_address & person_land
                if (arr['person_address_type'] != null) {
                    if (arr['person_address_type'] == 1) {
                        $(".display_person_address_type_1").show();
                        $("#person_address_type_" + arr['person_address_type']).attr('checked', 'checked')
                        $('#person_land_type_id').val(arr['person_land_type_id']);
                        $('#person_land_no').val(arr['person_land_no']);
                        $('#person_land_addr_no').val(arr['person_land_addr_no']);
                        $('#person_land_area_rai').val(arr['person_land_area_rai']);
                        $('#person_land_area_ngan').val(arr['person_land_area_ngan']);
                        $('#person_land_area_wah').val(arr['person_land_area_wah']);

                        if (arr['person_land_addr_province_id'] != null) {
                            $("#div_person_land_addr_province_id").load(base_url + "loan/province/" + arr['person_land_addr_province_id'] + param_url_person_pad_person_land_addr);
                            $("#div_person_land_addr_amphur_id").load(base_url + "loan/amphur/" + arr['person_land_addr_province_id'] + "/0" + param_url_person_pad_person_land_addr);
                        }
                        if (arr['person_land_addr_province_id'] != null && arr['person_land_addr_amphur_id'] != null) {
                            $("#div_person_land_addr_amphur_id").load(base_url + "loan/amphur/" + arr['person_land_addr_province_id'] + "/" + arr['person_land_addr_amphur_id'] + param_url_person_pad_person_land_addr);
                            $("#div_person_land_addr_district_id").load(base_url + "loan/district" + "/" + arr['person_land_addr_amphur_id'] + "/0" + param_url_person_pad_person_land_addr);
                        }
                        if (arr['person_land_addr_district_id'] != null && arr['person_land_addr_district_id'] != null) {
                            $("#div_person_land_addr_district_id").load(base_url + "loan/district" + "/" + arr['person_land_addr_amphur_id'] + "/" + arr['person_land_addr_district_id'] + param_url_person_pad_person_land_addr);
                        }
                    } else if (arr['person_address_type'] == 3) {
                        $(".display_person_address_type_1_3").show();
                        $('#person_address_type_other').val(arr['person_address_type_other']);
                    } else {
                        $(".display_person_address_type_1").hide();
                        $(".display_person_address_type_1_3").hide();
                    }
                }
            }
        });
    });


    //spouse_thaiid check
    jQuery('#spouse_thaiid').focus(function () {
        $("#spouse_thaiid_error").css("display", "none");
        $("#spouse_thaiid_error2").css("display", "none");
        $("#spouse_thaiid_form .control-label").css("color", "#65707D");
        $("#spouse_thaiid_form input[type=text]").css("border", "1px solid #CCCCCC");
        $("#spouse_thaiid_form input[type=text]").css("color", "#65707D");
    });

    jQuery('#spouse_thaiid').blur(function () {
    	if(jQuery('#spouse_thaiid').val()=='')
    	{
    		 return true;
    	} else if (checkID(jQuery('#spouse_thaiid').val()) === false) {
            //alert('กรอกเลขที่บัตรประจำตัวประชาชนไม่ถูกต้อง');
            $("#spouse_thaiid_error").css("display", "initial");
            $("#spouse_thaiid_form .control-label").css("color", "#A94442");
            $("#spouse_thaiid_form input[type=text]").css("border", "1px solid #A94442");
            $("#spouse_thaiid_form input[type=text]").css("color", "#A94442");

            jQuery('#spouse_thaiid').val('');
            return false;
        } else if ($("#person_thaiid").val() == $("#spouse_thaiid").val()) {
        	$("#spouse_thaiid_error2").css("display", "initial");
            $("#spouse_thaiid_form .control-label").css("color", "#A94442");
            $("#spouse_thaiid_form input[type=text]").css("border", "1px solid #A94442");
            $("#spouse_thaiid_form input[type=text]").css("color", "#A94442");

            jQuery('#spouse_thaiid').val('');
            return false;
        }

    });

    //cal age
    var person_birthdate = jQuery('#person_birthdate').val();
    if (person_birthdate) {
        res_arr = person_birthdate.split("/");
        person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];

        dob = new Date(person_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#person_age').val(age);
        } else {
            jQuery('#person_age').val(0);
        }

//        if(age < 18){
//            alert('ห้ามทำรายการ');
//        }
//
//        if(age > 65){
//            alert('ต้องมีผู้กู้ร่วม');
//        }
    }

    //cal age
    var spouse_birthdate = jQuery('#spouse_birthdate').val();
    if (spouse_birthdate) {
        res_arr = spouse_birthdate.split("/");
        spouse_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];

        dob = new Date(spouse_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#spouse_age').val(age);
        } else {
            jQuery('#spouse_age').val(0);
        }
    }

  /*province, amphur, district addr_card for person*/
    // $("#person_addr_card_province_id").select2({
    //     minimumResultsForSearch: -1
    // });
    // $("#person_addr_card_province_id").change(function () {
    //     $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_card_province_id").val() + param_url_person_pad_addr_card);
    //     $("#div_person_addr_card_district_id").load(base_url + "loan/district" + param_url_person_pad_addr_card);
    // });
    //
    // $("#person_addr_card_amphur_id").select2({
    //     minimumResultsForSearch: -1
    // });
    // $("#person_addr_card_amphur_id").change(function () {
    //     $("#div_person_addr_card_district_id").load(base_url + "loan/district/" + $("#person_addr_card_amphur_id").val() + param_url_person_pad_addr_card);
    // });

    $("#loan_year, #project_id").select2({
        minimumResultsForSearch: -1
    });

    // $("#person_addr_card_district_id").select2({
    //     minimumResultsForSearch: -1
    // });

    /*province, amphur, district addr_pre for person*/
    // $("#person_addr_pre_province_id").select2({
    //     minimumResultsForSearch: -1
    // });
    // $("#person_addr_pre_province_id").change(function () {
    //     $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_pre_province_id").val() + param_url_person_pad_addr_pre);
    //     $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + param_url_person_pad_addr_pre);
    // });
    //
    // $("#person_addr_pre_amphur_id").select2({
    //     minimumResultsForSearch: -1
    // });
    // $("#person_addr_pre_amphur_id").change(function () {
    //     $("#div_person_addr_pre_district_id").load(base_url + "loan/district/" + $("#person_addr_pre_amphur_id").val() + param_url_person_pad_addr_pre);
    // });
    //
    // $("#person_addr_pre_district_id").select2({
    //     minimumResultsForSearch: -1
    // });

    /*province, amphur, district addr_pre for spouse*/
    $("#spouse_addr_pre_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#spouse_addr_pre_province_id").change(function () {
        $("#div_spouse_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#spouse_addr_pre_province_id").val() + param_url_spouse_pad_addr_pre);
        $("#div_spouse_addr_pre_district_id").load(base_url + "loan/district" + param_url_spouse_pad_addr_pre);
    });

    $("#spouse_addr_pre_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#spouse_addr_pre_amphur_id").change(function () {
        $("#div_spouse_addr_pre_district_id").load(base_url + "loan/district/" + $("#spouse_addr_pre_amphur_id").val() + param_url_spouse_pad_addr_pre);
    });

    $("#spouse_addr_pre_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district person_land_addr for person*/
    $("#person_land_addr_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#person_land_addr_province_id").change(function () {
        $("#div_person_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#person_land_addr_province_id").val() + param_url_person_pad_person_land_addr);
        $("#div_person_land_addr_district_id").load(base_url + "loan/district" + param_url_person_pad_person_land_addr);
    });

    $("#person_land_addr_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#person_land_addr_amphur_id").change(function () {
        $("#div_person_land_addr_district_id").load(base_url + "loan/district/" + $("#person_land_addr_amphur_id").val() + param_url_person_pad_person_land_addr);
    });

    $("#person_land_addr_district_id").select2({
        minimumResultsForSearch: -1
    });


    /*person_addr_same*/
    $('#person_addr_same').change(function () {
        if ($(this).is(":checked")) {
            var readonly_list = '&province_readonly=readonly&amphur_readonly=readonly&district_readonly=readonly';
            var required_list = '&province_required=&amphur_required=&district_required=';
            var same_addr = readonly_list + required_list;

            //addr_pre for person
//            $("#div_person_addr_pre_province_id").load(base_url + "loan/province/0" + param_url_person_pad_addr_pre + same_addr);
//            $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/0/0" + param_url_person_pad_addr_pre + same_addr);
//            $("#div_person_addr_pre_district_id").load(base_url + "loan/district/0/0" + param_url_person_pad_addr_pre + same_addr);

//            $('.person_addr_pre').val('');
            $('.person_addr_pre').attr('readonly', true);
            $('.person_addr_pre').prop('required', false);
						$(".div_addr_same").hide();
        } else {
            $('.person_addr_pre').attr('readonly', false);
            $('.person_addr_pre').prop('required', true);
						$(".div_addr_same").show();
        }
    });

    /*spouse_addr_same*/
    $('#spouse_addr_same').change(function () {
        if ($(this).is(":checked")) {
            var readonly_list = '&province_readonly=readonly&amphur_readonly=readonly&district_readonly=readonly';
            var same_addr = readonly_list;

            //addr_pre for person
//            $("#div_spouse_addr_pre_province_id").load(base_url + "loan/province/0" + param_url_person_pad_person_land_addr + same_addr);
//            $("#div_spouse_addr_pre_amphur_id").load(base_url + "loan/amphur/0/0" + param_url_person_pad_person_land_addr + same_addr);
//            $("#div_spouse_addr_pre_district_id").load(base_url + "loan/district/0/0" + param_url_person_pad_person_land_addr + same_addr);

            //$('.spouse_addr_pre').val('');
            $('.spouse_addr_pre').attr('readonly', true);
						$(".div_spouse_addr_same").hide();
        } else {
						$(".div_spouse_addr_same").show();
            $('.spouse_addr_pre').attr('readonly', false);
        }
    });

    /*default sex_id by title_id*/
    $('#title_id').change(function () {
        var title_id = jQuery('#title_id').val();
        if (title_id == 1) {
            $("#sex_id_1").attr('checked', 'checked');
        } else if (title_id == '' || title_id == 0) {
            //$(".sex_id").checked = false;
        } else {
            $("#sex_id_2").attr('checked', 'checked');
        }
    });

    /*display_person_address_type*/
    $(".person_address_type").change(function () {
        var address_type_id = $(this).val();
        if (address_type_id == 1) {
            $(".display_person_address_type_1_3").show();
            $(".display_person_address_type_1").show();
        } else if (address_type_id == 3) {
            $(".display_person_address_type_1").hide();
            $(".display_person_address_type_1_3").show();
        } else {
            $(".display_person_address_type_1_3").hide();
            $(".display_person_address_type_1").hide();
        }
    });


});


function checkID(id) {
    if (id.length != 13)
        return false;
    for (i = 0, sum = 0; i < 12; i++)
        sum += parseFloat(id.charAt(i)) * (13 - i);
    if ((11 - sum % 11) % 10 != parseFloat(id.charAt(12)))
        return false;
    return true;
}



$("#frm_person").validate({
	rules: {
		person_land_addr_no: {
			digits: true
		}
	},
	highlight: function(element) {
		$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
	},
	success: function(element) {
		$(element).closest(".form-group").removeClass("has-error");
	}
});

$("#person_card_expire").change(function() {
    if($(this).is(":checked"))
    {
        $("#divExpire").hide();
    }
    else
    {
    	 $("#divExpire").show();
    }
});

$("#spouse_card_expire").change(function() {
    if($(this).is(":checked"))
    {
        $("#divExpireSpouse").hide();
    }
    else
    {
    	 $("#divExpireSpouse").show();
    }
});
