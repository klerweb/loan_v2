jQuery(document).ready(function () {
    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id&zipcode_id=land_addr_zipcode';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

    /*province, amphur, district for building*/
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id&zipcode_id=building_zipcode';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';
    var pad_required_building_list = 'province_required=&amphur_required=&district_required=';
    var param_url_pad_building = "/?" + pad_id_building_list + "&" + pad_name_building_list + "&" + pad_required_building_list;

    /*province, amphur, district addr_card for redeem_owner*/
    var redeem_owner_pad_id_addr_card_list = 'province_id=redeem_owner_person_addr_card_province_id&amphur_id=redeem_owner_person_addr_card_amphur_id&district_id=redeem_owner_person_addr_card_district_id&zipcode_id=redeem_owner_person_addr_card_zipcode';
    var redeem_owner_pad_name_addr_card_list = 'province_name=redeem_owner[person_addr_card_province_id]&amphur_name=redeem_owner[person_addr_card_amphur_id]&district_name=redeem_owner[person_addr_card_district_id]';
    var param_url_redeem_owner_pad_addr_card = "/?" + redeem_owner_pad_id_addr_card_list + "&" + redeem_owner_pad_name_addr_card_list;

    /*province, amphur, district addr_pre for redeem_owner*/
    var redeem_owner_pad_id_addr_pre_list = 'province_id=redeem_owner_person_addr_pre_province_id&amphur_id=redeem_owner_person_addr_pre_amphur_id&district_id=redeem_owner_person_addr_pre_district_id&zipcode_id=redeem_owner_person_addr_pre_zipcode';
    var redeem_owner_pad_name_addr_pre_list = 'province_name=redeem_owner[person_addr_pre_province_id]&amphur_name=redeem_owner[person_addr_pre_amphur_id]&district_name=redeem_owner[person_addr_pre_district_id]';
    var redeem_owner_pad_class_addr_pre_list = 'province_class=redeem_owner_person_addr_pre&amphur_class=redeem_owner_person_addr_pre&district_class=redeem_owner_person_addr_pre';
    var param_url_redeem_owner_pad_addr_pre = "/?" + redeem_owner_pad_id_addr_pre_list + "&" + redeem_owner_pad_name_addr_pre_list + "&" + redeem_owner_pad_class_addr_pre_list;

    /*province, amphur, district addr_card for creditor*/
    var creditor_pad_id_addr_card_list = 'province_id=creditor_person_addr_card_province_id&amphur_id=creditor_person_addr_card_amphur_id&district_id=creditor_person_addr_card_district_id&zipcode_id=creditor_person_addr_card_zipcode';
    var creditor_pad_name_addr_card_list = 'province_name=creditor[person_addr_card_province_id]&amphur_name=creditor[person_addr_card_amphur_id]&district_name=creditor[person_addr_card_district_id]';
    var param_url_creditor_pad_addr_card = "/?" + creditor_pad_id_addr_card_list + "&" + creditor_pad_name_addr_card_list;

    /*province, amphur, district addr_pre for creditor*/
    var creditor_pad_id_addr_pre_list = 'province_id=creditor_person_addr_pre_province_id&amphur_id=creditor_person_addr_pre_amphur_id&district_id=creditor_person_addr_pre_district_id&zipcode_id=creditor_person_addr_pre_zipcode';
    var creditor_pad_name_addr_pre_list = 'province_name=creditor[person_addr_pre_province_id]&amphur_name=creditor[person_addr_pre_amphur_id]&district_name=creditor[person_addr_pre_district_id]';
    var creditor_pad_class_addr_pre_list = 'province_class=creditor_person_addr_pre&amphur_class=creditor_person_addr_pre&district_class=creditor_person_addr_pre';
    var param_url_creditor_pad_addr_pre = "/?" + creditor_pad_id_addr_pre_list + "&" + creditor_pad_name_addr_pre_list + "&" + creditor_pad_class_addr_pre_list;

    /*province, amphur, district addr_card for borrowers*/
    var borrowers_pad_id_addr_card_list = 'province_id=borrowers_person_addr_card_province_id&amphur_id=borrowers_person_addr_card_amphur_id&district_id=borrowers_person_addr_card_district_id&zipcode_id=borrowers_person_addr_card_zipcode';
    var borrowers_pad_name_addr_card_list = 'province_name=borrowers[person_addr_card_province_id]&amphur_name=borrowers[person_addr_card_amphur_id]&district_name=borrowers[person_addr_card_district_id]';
    var param_url_borrowers_pad_addr_card = "/?" + borrowers_pad_id_addr_card_list + "&" + borrowers_pad_name_addr_card_list;

    /*province, amphur, district addr_pre for borrowers*/
    var borrowers_pad_id_addr_pre_list = 'province_id=borrowers_person_addr_pre_province_id&amphur_id=borrowers_person_addr_pre_amphur_id&district_id=borrowers_person_addr_pre_district_id&zipcode_id=borrowers_person_addr_pre_zipcode';
    var borrowers_pad_name_addr_pre_list = 'province_name=borrowers[person_addr_pre_province_id]&amphur_name=borrowers[person_addr_pre_amphur_id]&district_name=borrowers[person_addr_pre_district_id]';
    var borrowers_pad_class_addr_pre_list = 'province_class=borrowers_person_addr_pre&amphur_class=borrowers_person_addr_pre&district_class=borrowers_person_addr_pre';
    var param_url_borrowers_pad_addr_pre = "/?" + borrowers_pad_id_addr_pre_list + "&" + borrowers_pad_name_addr_pre_list + "&" + borrowers_pad_class_addr_pre_list;


    jQuery('#loan_objective_id').on('change', function () {
        if (jQuery(this).val() > 0) {
            var url = jQuery('#curr_url').val() + '/land_' + jQuery(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
        } else {
            var url = jQuery('#curr_url').val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
        }

        return false;
    });

    /*loan_type_contract_date*/
//    jQuery('#loan_type_contract_date').datepicker({dateFormat: 'yy-mm-dd'});
    $("#loan_type_contract_date").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    /*loan_type_case_date*/
//    jQuery('#loan_type_case_date').datepicker({dateFormat: 'yy-mm-dd'});
    $("#loan_type_case_date").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    /*loan_type_case_date*/
//    jQuery('#loan_type_sale_date').datepicker({dateFormat: 'yy-mm-dd'});
    $("#loan_type_sale_date").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });


    /*redeem_owner*/
    //person_birthdate
//    jQuery('#redeem_owner_person_birthdate').datepicker({dateFormat: 'yy-mm-dd',maxDate: '0'});
    $("#redeem_owner_person_birthdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);

            //age
            dob = new Date(dateBefore);
            var today = new Date();
            var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

            if (age > 0) {
                jQuery('#redeem_owner_person_age').val(age);
            } else {
                jQuery('#redeem_owner_person_age').val(0);
            }
            
            
//            dateBefore = $(this).val();
//            var arrayDate = dateText.split("/");
//            arrayDate[2] = parseInt(arrayDate[2]) + 543;
//            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
//
//            //age
//            res_arr = dateBefore.split("/");
//            redeem_owner_person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];
//            alert(redeem_owner_person_birthdate);
//            dob = new Date(redeem_owner_person_birthdate);
//            var today = new Date();
//            var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
//
//            if (age > 0) {
//                jQuery('#redeem_owner_person_age').val(age);
//            } else {
//                jQuery('#redeem_owner_person_age').val(0);
//            }
        }
    });

    //person_card_startdate
//    jQuery('#redeem_owner_person_card_startdate').datepicker({dateFormat: 'yy-mm-dd'});
    $("#redeem_owner_person_card_startdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //person_card_expiredate
//    jQuery('#redeem_owner_person_card_expiredate').datepicker({dateFormat: 'yy-mm-dd'});
    $("#redeem_owner_person_card_expiredate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //btn_person_search
    jQuery('#redeem_owner_btn_person_search').click(function () {
        $.get("../../loan/search_person", {person_thaiid: jQuery('#redeem_owner_person_search').val()}).done(function (data) {
            var arr = [];
            json = JSON.stringify(eval('(' + data + ')')); //convert to json string
            arr = $.parseJSON(json); //convert to javascript array

            if (arr['person_id'] == null) {
                jQuery('#redeem_owner_res_success_person_search').css("display", "none");
                jQuery('#redeem_owner_res_fail_person_search').css("display", "inline-block");
            } else {
                jQuery('#redeem_owner_res_success_person_search').css("display", "inline-block");
                jQuery('#redeem_owner_res_fail_person_search').css("display", "none");
            }

            jQuery('#redeem_owner_title_id').val(arr['title_id']);
            jQuery('#redeem_owner_person_fname').val(arr['person_fname']);
            jQuery('#redeem_owner_person_lname').val(arr['person_lname']);
            jQuery('#redeem_owner_person_birthdate').val(arr['person_birthdate']);
            jQuery("#redeem_owner_sex_id_" + arr['sex_id']).attr('checked', 'checked');
            jQuery('#redeem_owner_race_id').val(arr['race_id']);
            jQuery('#redeem_owner_nationality_id').val(arr['nationality_id']);
            jQuery('#redeem_owner_religion_id').val(arr['religion_id']);
            jQuery('#redeem_owner_person_thaiid').val(arr['person_thaiid']);
            jQuery('#redeem_owner_person_card_startdate').val(arr['person_card_startdate']);
            jQuery('#redeem_owner_person_card_expiredate').val(arr['person_card_expiredate']);
            jQuery('#redeem_owner_person_addr_card_no').val(arr['person_addr_card_no']);
            jQuery('#redeem_owner_person_addr_card_moo').val(arr['person_addr_card_moo']);
            jQuery('#redeem_owner_person_addr_card_road').val(arr['person_addr_card_road']);
            jQuery('#redeem_owner_person_addr_card_district_id').val(arr['person_addr_card_district_id']);
            jQuery('#redeem_owner_person_addr_card_amphur_id').val(arr['person_addr_card_amphur_id']);
            jQuery('#redeem_owner_person_addr_card_province_id').val(arr['person_addr_card_province_id']);
            jQuery('#redeem_owner_person_addr_pre_no').val(arr['person_addr_pre_no']);
            jQuery('#redeem_owner_person_addr_pre_moo').val(arr['person_addr_pre_moo']);
            jQuery('#redeem_owner_person_addr_pre_road').val(arr['person_addr_pre_road']);
            jQuery('#redeem_owner_person_addr_pre_district_id').val(arr['person_addr_pre_district_id']);
            jQuery('#redeem_owner_person_addr_pre_amphur_id').val(arr['person_addr_pre_amphur_id']);
            jQuery('#redeem_owner_person_addr_pre_province_id').val(arr['person_addr_pre_province_id']);
            jQuery('#redeem_owner_person_phone').val(arr['person_phone']);
            jQuery('#redeem_owner_person_mobile').val(arr['person_mobile']);
            jQuery('#redeem_owner_person_email').val(arr['person_email']);
            jQuery('#career_id').val(arr['career_id']);
            jQuery('#redeem_owner_person_career_other').val(arr['person_career_other']);
            jQuery("#status_married_id_" + arr['status_married_id']).attr('checked', 'checked');

            //cal age
            var redeem_owner_person_birthdate = arr['person_birthdate'];
            if (redeem_owner_person_birthdate) {
                res_arr = redeem_owner_person_birthdate.split("/");
                redeem_owner_person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];

                dob = new Date(redeem_owner_person_birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                if (age > 0) {
                    jQuery('#redeem_owner_person_age').val(age);
                } else {
                    jQuery('#redeem_owner_person_age').val(0);
                }
            }

            jQuery('#redeem_owner_person_birthdate').change(function () {
                redeem_owner_person_birthdate = jQuery('#redeem_owner_person_birthdate').val();

                dob = new Date(redeem_owner_person_birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                if (age > 0) {
                    jQuery('#redeem_owner_person_age').val(age);
                } else {
                    jQuery('#redeem_owner_person_age').val(0);
                }
            });


            //addr_card for redeem_owner
            $("#div_redeem_owner_person_addr_card_province_id").load(base_url + "loan/province/" + arr['person_addr_card_province_id'] + param_url_redeem_owner_pad_addr_card);
            $("#div_redeem_owner_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#redeem_owner_person_addr_card_province_id").val() + "/" + arr['person_addr_card_amphur_id'] + param_url_redeem_owner_pad_addr_card);
            $("#div_redeem_owner_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/" + arr['person_addr_card_district_id'] + param_url_redeem_owner_pad_addr_card);

            //addr_pre for redeem_owner
            $("#div_redeem_owner_person_addr_pre_province_id").load(base_url + "loan/province/" + arr['person_addr_pre_province_id'] + param_url_redeem_owner_pad_addr_pre);
            $("#div_redeem_owner_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#redeem_owner_person_addr_pre_province_id").val() + "/" + arr['person_addr_pre_amphur_id'] + param_url_redeem_owner_pad_addr_pre);
            $("#div_redeem_owner_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/" + arr['person_addr_pre_district_id'] + param_url_redeem_owner_pad_addr_pre);
        });
    });

    //cal age
    var redeem_owner_person_birthdate = jQuery('#redeem_owner_person_birthdate').val();
    if (redeem_owner_person_birthdate) {
        res_arr = redeem_owner_person_birthdate.split("/");
        redeem_owner_person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];

        dob = new Date(redeem_owner_person_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#redeem_owner_person_age').val(age);
        } else {
            jQuery('#redeem_owner_person_age').val(0);
        }
    }

//    jQuery('#redeem_owner_person_birthdate').change(function () {
//        res_arr = redeem_owner_person_birthdate.split("/");
//        redeem_owner_person_birthdate = (res_arr[2] - 543) + '-' + res_arr[1] + '-' + res_arr[0];
//
//        dob = new Date(person_birthdate);
//        var today = new Date();
//        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
//        
//        if(age > 0){
//            jQuery('#redeem_owner_person_age').val(age);
//        }else{
//            jQuery('#redeem_owner_person_age').val(0);
//        }
//    });


    /*creditor*/
    //person_birthdate
//    jQuery('#creditor_person_birthdate').datepicker({dateFormat: 'yy-mm-dd', maxDate: '0'});
    $("#creditor_person_birthdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            
            //age
            dob = new Date(dateBefore);
            var today = new Date();
            var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

            if (age > 0) {
                jQuery('#creditor_person_age').val(age);
            } else {
                jQuery('#creditor_person_age').val(0);
            }
        }
    });

    //person_card_startdate
//    jQuery('#creditor_person_card_startdate').datepicker({dateFormat: 'yy-mm-dd'});
    $("#creditor_person_card_startdate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //person_card_expiredate
//    jQuery('#creditor_person_card_expiredate').datepicker({dateFormat: 'yy-mm-dd'});
    $("#creditor_person_card_expiredate").datepicker({
        dateFormat: "dd/mm/yy",
        isBuddhist: true,
        dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
        dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
        monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
        monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
        changeMonth: true,
        changeYear: true,
        beforeShow: function () {
            if ($(this).val() != "")
            {
                var arrayDate = $(this).val().split("/");
                arrayDate[2] = parseInt(arrayDate[2]) - 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onChangeMonthYear: function () {
            setTimeout(function () {
                $.each($(".ui-datepicker-year option"), function (j, k) {
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                    $(".ui-datepicker-year option").eq(j).text(textYear);
                });
            }, 50);
        },
        onClose: function () {
            if ($(this).val() != "") {
                var arrayDate = dateBefore.split("/");
                arrayDate[2] = parseInt(arrayDate[2]) + 543;
                $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
            }
        },
        onSelect: function (dateText, inst) {
            dateBefore = $(this).val();
            var arrayDate = dateText.split("/");
            arrayDate[2] = parseInt(arrayDate[2]) + 543;
            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
        }
    });

    //btn_person_search
    jQuery('#creditor_btn_person_search').click(function () {
        $.get("../../loan/search_person", {person_thaiid: jQuery('#creditor_person_search').val()}).done(function (data) {
            var arr = [];
            json = JSON.stringify(eval('(' + data + ')')); //convert to json string
            arr = $.parseJSON(json); //convert to javascript array

            if (arr['person_id'] == null) {
                jQuery('#creditor_res_success_person_search').css("display", "none");
                jQuery('#creditor_res_fail_person_search').css("display", "inline-block");
            } else {
                jQuery('#creditor_res_success_person_search').css("display", "inline-block");
                jQuery('#creditor_res_fail_person_search').css("display", "none");
            }

            jQuery('#creditor_person_id').val(arr['person_id']);
            jQuery('#creditor_title_id').val(arr['title_id']);
            jQuery('#creditor_person_fname').val(arr['person_fname']);
            jQuery('#creditor_person_lname').val(arr['person_lname']);
            jQuery('#creditor_person_birthdate').val(arr['person_birthdate']);
            jQuery("#creditor_sex_id_" + arr['sex_id']).attr('checked', 'checked');
            jQuery('#creditor_race_id').val(arr['race_id']);
            jQuery('#creditor_nationality_id').val(arr['nationality_id']);
            jQuery('#creditor_religion_id').val(arr['religion_id']);
            jQuery('#creditor_person_thaiid').val(arr['person_thaiid']);
            jQuery('#creditor_person_card_startdate').val(arr['person_card_startdate']);
            jQuery('#creditor_person_card_expiredate').val(arr['person_card_expiredate']);
            jQuery('#creditor_person_addr_card_no').val(arr['person_addr_card_no']);
            jQuery('#creditor_person_addr_card_moo').val(arr['person_addr_card_moo']);
            jQuery('#creditor_person_addr_card_road').val(arr['person_addr_card_road']);
            jQuery('#creditor_person_addr_card_district_id').val(arr['person_addr_card_district_id']);
            jQuery('#creditor_person_addr_card_amphur_id').val(arr['person_addr_card_amphur_id']);
            jQuery('#creditor_person_addr_card_province_id').val(arr['person_addr_card_province_id']);
            jQuery('#creditor_person_addr_pre_no').val(arr['person_addr_pre_no']);
            jQuery('#creditor_person_addr_pre_moo').val(arr['person_addr_pre_moo']);
            jQuery('#creditor_person_addr_pre_road').val(arr['person_addr_pre_road']);
            jQuery('#creditor_person_addr_pre_district_id').val(arr['person_addr_pre_district_id']);
            jQuery('#creditor_person_addr_pre_amphur_id').val(arr['person_addr_pre_amphur_id']);
            jQuery('#creditor_person_addr_pre_province_id').val(arr['person_addr_pre_province_id']);
            jQuery('#creditor_person_phone').val(arr['person_phone']);
            jQuery('#creditor_person_mobile').val(arr['person_mobile']);
            jQuery('#creditor_person_email').val(arr['person_email']);
            jQuery('#creditor_career_id').val(arr['career_id']);
            jQuery('#creditor_person_career_other').val(arr['person_career_other']);

            //cal age
            var creditor_person_birthdate = arr['person_birthdate'];
            if (creditor_person_birthdate) {
                dob = new Date(creditor_person_birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                if (age > 0) {
                    jQuery('#creditor_person_age').val(age);
                } else {
                    jQuery('#creditor_person_age').val(0);
                }
            }

            jQuery('#creditor_person_birthdate').change(function () {
                creditor_person_birthdate = jQuery('#creditor_person_birthdate').val();

                dob = new Date(creditor_person_birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                if (age > 0) {
                    jQuery('#creditor_person_age').val(age);
                } else {
                    jQuery('#creditor_person_age').val(0);
                }
            });


            //addr_card for creditor
            $("#div_creditor_person_addr_card_province_id").load(base_url + "loan/province/" + arr['person_addr_card_province_id'] + param_url_creditor_pad_addr_card);
            $("#div_creditor_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#creditor_person_addr_card_province_id").val() + "/" + arr['person_addr_card_amphur_id'] + param_url_creditor_pad_addr_card);
            $("#div_creditor_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/" + arr['person_addr_card_district_id'] + param_url_creditor_pad_addr_card);

            //addr_pre for creditor
            $("#div_creditor_person_addr_pre_province_id").load(base_url + "loan/province/" + arr['person_addr_pre_province_id'] + param_url_creditor_pad_addr_pre);
            $("#div_creditor_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#creditor_person_addr_pre_province_id").val() + "/" + arr['person_addr_pre_amphur_id'] + param_url_creditor_pad_addr_pre);
            $("#div_creditor_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/" + arr['person_addr_pre_district_id'] + param_url_creditor_pad_addr_pre);
        });
    });

    //cal age
    var creditor_person_birthdate = jQuery('#creditor_person_birthdate').val();
    if (creditor_person_birthdate) {
        dob = new Date(creditor_person_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#creditor_person_age').val(age);
        } else {
            jQuery('#creditor_person_age').val(0);
        }
    }


    /*borrowers*/
    //person_birthdate
    jQuery('#borrowers_person_birthdate').datepicker({dateFormat: 'yy-mm-dd', maxDate: '0'});

    //person_card_startdate
    jQuery('#borrowers_person_card_startdate').datepicker({dateFormat: 'yy-mm-dd'});

    //person_card_expiredate
    jQuery('#borrowers_person_card_expiredate').datepicker({dateFormat: 'yy-mm-dd'});

    //btn_person_search
    jQuery('#borrowers_btn_person_search').click(function () {
        $.get("../../loan/search_person", {person_thaiid: jQuery('#borrowers_person_search').val()}).done(function (data) {
            var arr = [];
            json = JSON.stringify(eval('(' + data + ')')); //convert to json string
            arr = $.parseJSON(json); //convert to javascript array

            if (arr['person_id'] == null) {
                jQuery('#borrowers_res_success_person_search').css("display", "none");
                jQuery('#borrowers_res_fail_person_search').css("display", "inline-block");
            } else {
                jQuery('#borrowers_res_success_person_search').css("display", "inline-block");
                jQuery('#borrowers_res_fail_person_search').css("display", "none");
            }

            jQuery('#borrowers_person_id').val(arr['person_id']);
            jQuery('#borrowers_title_id').val(arr['title_id']);
            jQuery('#borrowers_person_fname').val(arr['person_fname']);
            jQuery('#borrowers_person_lname').val(arr['person_lname']);
            jQuery('#borrowers_person_birthdate').val(arr['person_birthdate']);
            jQuery("#borrowers_sex_id_" + arr['sex_id']).attr('checked', 'checked');
            jQuery('#borrowers_race_id').val(arr['race_id']);
            jQuery('#borrowers_nationality_id').val(arr['nationality_id']);
            jQuery('#borrowers_religion_id').val(arr['religion_id']);
            jQuery('#borrowers_person_thaiid').val(arr['person_thaiid']);
            jQuery('#borrowers_person_card_startdate').val(arr['person_card_startdate']);
            jQuery('#borrowers_person_card_expiredate').val(arr['person_card_expiredate']);
            jQuery('#borrowers_person_addr_card_no').val(arr['person_addr_card_no']);
            jQuery('#borrowers_person_addr_card_moo').val(arr['person_addr_card_moo']);
            jQuery('#borrowers_person_addr_card_road').val(arr['person_addr_card_road']);
            jQuery('#borrowers_person_addr_card_district_id').val(arr['person_addr_card_district_id']);
            jQuery('#borrowers_person_addr_card_amphur_id').val(arr['person_addr_card_amphur_id']);
            jQuery('#borrowers_person_addr_card_province_id').val(arr['person_addr_card_province_id']);
            jQuery('#borrowers_person_addr_pre_no').val(arr['person_addr_pre_no']);
            jQuery('#borrowers_person_addr_pre_moo').val(arr['person_addr_pre_moo']);
            jQuery('#borrowers_person_addr_pre_road').val(arr['person_addr_pre_road']);
            jQuery('#borrowers_person_addr_pre_district_id').val(arr['person_addr_pre_district_id']);
            jQuery('#borrowers_person_addr_pre_amphur_id').val(arr['person_addr_pre_amphur_id']);
            jQuery('#borrowers_person_addr_pre_province_id').val(arr['person_addr_pre_province_id']);
            jQuery('#borrowers_person_phone').val(arr['person_phone']);
            jQuery('#borrowers_person_mobile').val(arr['person_mobile']);
            jQuery('#borrowers_person_email').val(arr['person_email']);
            jQuery('#borrowers_career_id').val(arr['career_id']);
            jQuery('#borrowers_person_career_other').val(arr['person_career_other']);

            //cal age
            var borrowers_person_birthdate = arr['person_birthdate'];
            if (borrowers_person_birthdate) {
                dob = new Date(borrowers_person_birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                if (age > 0) {
                    jQuery('#borrowers_person_age').val(age);
                } else {
                    jQuery('#borrowers_person_age').val(0);
                }
            }

            jQuery('#borrowers_person_birthdate').change(function () {
                borrowers_person_birthdate = jQuery('#borrowers_person_birthdate').val();

                dob = new Date(borrowers_person_birthdate);
                var today = new Date();
                var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

                if (age > 0) {
                    jQuery('#borrowers_person_age').val(age);
                } else {
                    jQuery('#borrowers_person_age').val(0);
                }
            });


            //addr_card for borrowers
            $("#div_borrowers_person_addr_card_province_id").load(base_url + "loan/province/" + arr['person_addr_card_province_id'] + param_url_borrowers_pad_addr_card);
            $("#div_borrowers_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#borrowers_person_addr_card_province_id").val() + "/" + arr['person_addr_card_amphur_id'] + param_url_borrowers_pad_addr_card);
            $("#div_borrowers_person_addr_card_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_card_amphur_id'] + "/" + arr['person_addr_card_district_id'] + param_url_borrowers_pad_addr_card);

            //addr_pre for borrowers
            $("#div_borrowers_person_addr_pre_province_id").load(base_url + "loan/province/" + arr['person_addr_pre_province_id'] + param_url_borrowers_pad_addr_pre);
            $("#div_borrowers_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#borrowers_person_addr_pre_province_id").val() + "/" + arr['person_addr_pre_amphur_id'] + param_url_borrowers_pad_addr_pre);
            $("#div_borrowers_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + arr['person_addr_pre_amphur_id'] + "/" + arr['person_addr_pre_district_id'] + param_url_borrowers_pad_addr_pre);


        });
    });

    //cal age
    var borrowers_person_birthdate = jQuery('#borrowers_person_birthdate').val();
    if (borrowers_person_birthdate) {
        dob = new Date(borrowers_person_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#borrowers_person_age').val(age);
        } else {
            jQuery('#borrowers_person_age').val(0);
        }
    }

    jQuery('#borrowers_person_birthdate').change(function () {
        borrowers_person_birthdate = jQuery('#borrowers_person_birthdate').val();

        dob = new Date(borrowers_person_birthdate);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));

        if (age > 0) {
            jQuery('#borrowers_person_age').val(age);
        } else {
            jQuery('#borrowers_person_age').val(0);
        }
    });

    // validation form_loan
    jQuery("#form_loan").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });

    // validation form_building
    jQuery("#form_building").validate({
        highlight: function (element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        },
        success: function (element) {
            $(element).closest(".form-group").removeClass("has-error");
        }
    });



    /*province, amphur, district for land_addr*/
    $("#land_addr_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#land_addr_province_id").change(function () {
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + param_url_pad_land_addr);
    });

    $("#land_addr_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#land_addr_amphur_id").change(function () {
        $("#div_land_addr_district_id").load(base_url + "loan/district/" + $("#land_addr_amphur_id").val() + param_url_pad_land_addr);
    });

    $("#land_addr_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district for building*/
    $("#building_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#building_province_id").change(function () {
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + param_url_pad_building);
    });

    $("#building_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#building_amphur_id").change(function () {        
        $("#div_building_district_id").load(base_url + "loan/district/" + $("#building_amphur_id").val() + param_url_pad_building);
    });

    $("#building_district_id").select2({
        minimumResultsForSearch: -1
    });


    /*province, amphur, district addr_card for redeem_owner*/
    $("#redeem_owner_person_addr_card_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#redeem_owner_person_addr_card_province_id").change(function () {
        $("#div_redeem_owner_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#redeem_owner_person_addr_card_province_id").val() + param_url_redeem_owner_pad_addr_card);
        $("#div_redeem_owner_person_addr_card_district_id").load(base_url + "loan/district" + param_url_redeem_owner_pad_addr_card);
    });

    $("#redeem_owner_person_addr_card_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#redeem_owner_person_addr_card_amphur_id").change(function () {
        $("#redeem_owner_person_addr_card_district_id").load(base_url + "loan/district/" + $("#redeem_owner_person_addr_card_amphur_id").val() + param_url_redeem_owner_pad_addr_card);
    });

    $("#redeem_owner_person_addr_card_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district addr_pre for redeem_owner*/
    $("#redeem_owner_person_addr_pre_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#redeem_owner_person_addr_pre_province_id").change(function () {
        $("#div_redeem_owner_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#redeem_owner_person_addr_pre_province_id").val() + param_url_redeem_owner_pad_addr_pre);
        $("#die_redeem_owner_person_addr_pre_district_id").load(base_url + "loan/district" + param_url_redeem_owner_pad_addr_pre);
    });

    $("#redeem_owner_person_addr_pre_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#redeem_owner_person_addr_pre_amphur_id").change(function () {
        $("#die_redeem_owner_person_addr_pre_district_id").load(base_url + "loan/district/" + $("#redeem_owner_person_addr_pre_amphur_id").val() + param_url_redeem_owner_pad_addr_pre);
    });

    $("#redeem_owner_person_addr_pre_district_id").select2({
        minimumResultsForSearch: -1
    });


    /*province, amphur, district addr_card for creditor*/
    $("#creditor_person_addr_card_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#creditor_person_addr_card_province_id").change(function () {
        $("#div_creditor_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#creditor_person_addr_card_province_id").val() + param_url_creditor_pad_addr_card);
        $("#div_creditor_person_addr_card_district_id").load(base_url + "loan/district" + param_url_creditor_pad_addr_card);
    });

    $("#creditor_person_addr_card_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#creditor_person_addr_card_amphur_id").change(function () {
        $("#creditor_person_addr_card_district_id").load(base_url + "loan/district/" + $("#creditor_person_addr_card_amphur_id").val() + param_url_creditor_pad_addr_card);
    });

    $("#creditor_person_addr_card_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district addr_pre for creditor*/
    $("#creditor_person_addr_pre_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#creditor_person_addr_pre_province_id").change(function () {
        $("#div_creditor_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#creditor_person_addr_pre_province_id").val() + param_url_creditor_pad_addr_pre);
        $("#div_creditor_person_addr_pre_district_id").load(base_url + "loan/district" + param_url_creditor_pad_addr_pre);
    });

    $("#creditor_person_addr_pre_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#creditor_person_addr_pre_amphur_id").change(function () {
        $("#div_creditor_person_addr_pre_district_id").load(base_url + "loan/district/" + $("#creditor_person_addr_pre_amphur_id").val() + param_url_creditor_pad_addr_pre);
    });

    $("#creditor_person_addr_pre_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district addr_card for borrowers*/
    $("#borrowers_person_addr_card_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#borrowers_person_addr_card_province_id").change(function () {
        $("#div_borrowers_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#borrowers_person_addr_card_province_id").val() + param_url_borrowers_pad_addr_card);
        $("#div_borrowers_person_addr_card_district_id").load(base_url + "loan/district" + param_url_borrowers_pad_addr_card);
    });

    $("#borrowers_person_addr_card_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#borrowers_person_addr_card_amphur_id").change(function () {
        $("#div_borrowers_person_addr_card_district_id").load(base_url + "loan/district/" + $("#borrowers_person_addr_card_amphur_id").val() + param_url_borrowers_pad_addr_card);
    });

    $("#borrowers_person_addr_card_district_id").select2({
        minimumResultsForSearch: -1
    });

    /*province, amphur, district addr_pre for borrowers*/
    $("#borrowers_person_addr_pre_province_id").select2({
        minimumResultsForSearch: -1
    });
    $("#borrowers_person_addr_pre_province_id").change(function () {
        $("#div_borrowers_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#borrowers_person_addr_pre_province_id").val() + param_url_borrowers_pad_addr_pre);
        $("#div_borrowers_person_addr_pre_district_id").load(base_url + "loan/district" + param_url_borrowers_pad_addr_pre);
    });

    $("#borrowers_person_addr_pre_amphur_id").select2({
        minimumResultsForSearch: -1
    });
    $("#borrowers_person_addr_pre_amphur_id").change(function () {
        $("#div_borrowers_person_addr_pre_district_id").load(base_url + "loan/district/" + $("#borrowers_person_addr_pre_amphur_id").val() + param_url_borrowers_pad_addr_pre);
    });

    $("#borrowers_person_addr_pre_district_id").select2({
        minimumResultsForSearch: -1
    });


    $('#redeem_owner_person_addr_same').change(function () {
        if ($(this).is(":checked")) {
            $('.redeem_owner_person_addr_pre').val('');
            $('.redeem_owner_person_addr_pre').attr('readonly', true);
            $('.redeem_owner_person_addr_pre').prop('required', false);

            $("#redeem_owner_person_addr_pre_province_id").select2("val", "");
            $("#redeem_owner_person_addr_pre_amphur_id").select2("val", "");
            $("#redeem_owner_person_addr_pre_district_id").select2("val", "");
        } else {
            $('.redeem_owner_person_addr_pre').attr('readonly', false);
            $('.redeem_owner_person_addr_pre').prop('required', true);
        }
    });

    $('#creditor_person_addr_same').change(function () {
        if ($(this).is(":checked")) {
            $('.creditor_person_addr_pre').val('');
            $('.creditor_person_addr_pre').attr('readonly', true);
            $('.creditor_person_addr_pre').prop('required', false);

            $("#creditor_person_addr_pre_province_id").select2("val", "");
            $("#creditor_person_addr_pre_amphur_id").select2("val", "");
            $("#creditor_person_addr_pre_district_id").select2("val", "");
        } else {
            $('.creditor_person_addr_pre').attr('readonly', false);
            $('.creditor_person_addr_pre').prop('required', true);
        }
    });

    $('#borrowers_person_addr_same').change(function () {
        if ($(this).is(":checked")) {
            $('.borrowers_person_addr_pre').val('');
            $('.borrowers_person_addr_pre').attr('readonly', true);
            $('.borrowers_person_addr_pre').prop('required', false);

            $("#borrowers_person_addr_pre_province_id").select2("val", "");
            $("#borrowers_person_addr_pre_amphur_id").select2("val", "");
            $("#borrowers_person_addr_pre_district_id").select2("val", "");
        } else {
            $('.borrowers_person_addr_pre').attr('readonly', false);
            $('.borrowers_person_addr_pre').prop('required', true);
        }
    });

});

