$("#ddlProjectYear").select2({
	minimumResultsForSearch: -1
});

$("#frmProject").validate({
	highlight: function(element) {
		$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
	},
	success: function(element) {
		$(element).closest(".form-group").removeClass("has-error");
	}
});