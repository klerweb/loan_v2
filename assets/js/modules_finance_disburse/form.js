$("#txtDate").datepicker({ 
	dateFormat: "dd/mm/yy", 
	isBuddhist: true,
	dayNames: ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"],
    dayNamesMin: ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
    monthNames: ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],
    monthNamesShort: ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
    changeMonth: true,  
    changeYear: true ,  
    beforeShow:function(){  
        if($(this).val()!="")
        {  
            var arrayDate = $(this).val().split("/");       
            arrayDate[2] = parseInt(arrayDate[2])-543;  
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
        }  
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);
    },  
    onChangeMonthYear: function(){  
        setTimeout(function(){  
            $.each($(".ui-datepicker-year option"),function(j,k){  
                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                $(".ui-datepicker-year option").eq(j).text(textYear);  
            });               
        },50);        
    },  
    onClose:function(){  
        if($(this).val()!="" && $(this).val()==dateBefore){           
            var arrayDate = dateBefore.split("/");  
            arrayDate[2] = parseInt(arrayDate[2])+543;  
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);      
        }         
    },  
    onSelect: function(dateText, inst){   
        dateBefore = $(this).val();  
        var arrayDate = dateText.split("/");  
        arrayDate[2] = parseInt(arrayDate[2])+543;  
        $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
    }  
});

$("#iconDate").click(function(){ 
	$("#txtDate").datepicker("show"); 
}); 

$("#ddlStatus, #ddlTitle, #ddlProvince, #ddlAmphur, #ddlDistrict, #ddlCreditorTitle, #ddlCreditorProvince, #ddlCreditorAmphur, #ddlCreditorDistrict, #ddlObjective, #ddlLandType, #ddlCase").select2({
	minimumResultsForSearch: -1
});

$("#txtThaiid").autocomplete({
	minLength: 2,
	maxHeight: 13,
	source: function (request, response){
		$.ajax({
			url: base_url+"non_loan/person_search",
			data: { q: request.term },
			dataType: "json",
			success:  function(data){
				response($.map(data, function(item){
					var label = "<p>"+ item.person_thaiid + " :: " + item.person_fname + " " + item.person_lname + "</p>";
					return {
						label:label,
						person_id: item.person_id,
						title_id: item.title_id,							
						person_fname: item.person_fname,
						person_lname: item.person_lname,
						person_thaiid: item.person_thaiid,
						person_addr_card_no: item.person_addr_card_no,
						person_addr_card_moo: item.person_addr_card_moo,
						person_addr_card_road: item.person_addr_card_road,
						person_phone: item.person_phone,
						person_addr_card_district_id: item.person_addr_card_district_id,
						person_addr_card_amphur_id: item.person_addr_card_amphur_id,
						person_addr_card_province_id: item.person_addr_card_province_id
					};
				}));
			},
			error: function(){
				response([]);
			}
		})
	},
	select: function(event, ui){
		event.preventDefault();
		
		$("#txtPersonId").val(ui.item.person_id);
		$("#ddlTitle").select2("val", ui.item.title_id);
		$("#txtFname").val(ui.item.person_fname);
		$("#txtLname").val(ui.item.person_lname);
		$("#txtThaiid").val(ui.item.person_thaiid);
		$("#txtAddrCardNo").val(ui.item.person_addr_card_no);
		$("#txtAddrCardMoo").val(ui.item.person_addr_card_moo);
		$("#txtAddrCardRoad").val(ui.item.person_addr_card_road);
		$("#txtPhone").val(ui.item.person_phone);
		$("#ddlProvince").select2("val", ui.item.person_addr_card_province_id);
		
		$("#divAmphur").load(base_url+"non_loan/amphur/"+ui.item.person_addr_card_province_id, function(response, status, xhr ){
			$("#ddlAmphur").select2("val", ui.item.person_addr_card_amphur_id);
		});
		
		$("#divDistrict").load(base_url+"non_loan/district/"+ui.item.person_addr_card_amphur_id, function(response, status, xhr ){
			$("#ddlDistrict").select2("val", ui.item.person_addr_card_district_id);
		});
	},
	change: function(event, ui){
		event.preventDefault();
		
		if(ui.item == null && typeof(ui.item) !== "undefined")
		{
			//not found
		}
	}
});

$("#txtCreditorThaiid").autocomplete({
	minLength: 2,
	maxHeight: 13,
	source: function (request, response){
		$.ajax({
			url: base_url+"non_loan/person_search",
			data: { q: request.term },
			dataType: "json",
			success:  function(data){
				response($.map(data, function(item){
					var label = "<p>"+ item.person_thaiid + " :: " + item.person_fname + " " + item.person_lname + "</p>";
					return {
						label: label,
						person_id: item.person_id,
						title_id: item.title_id,							
						person_fname: item.person_fname,
						person_lname: item.person_lname,
						person_thaiid: item.person_thaiid,
						person_addr_card_no: item.person_addr_card_no,
						person_addr_card_moo: item.person_addr_card_moo,
						person_addr_card_road: item.person_addr_card_road,
						person_phone: item.person_phone,
						person_addr_card_district_id: item.person_addr_card_district_id,
						person_addr_card_amphur_id: item.person_addr_card_amphur_id,
						person_addr_card_province_id: item.person_addr_card_province_id
					};
				}));
			},
			error: function(){
				response([]);
			}
		})
	},
	select: function(event, ui){
		event.preventDefault();
		
		$("#txtCreditorId").val(ui.item.person_id);
		$("#ddlCreditorTitle").select2("val", ui.item.title_id);
		$("#txtCreditorFname").val(ui.item.person_fname);
		$("#txtCreditorLname").val(ui.item.person_lname);
		$("#txtCreditorThaiid").val(ui.item.person_thaiid);
		$("#txtCreditorAddrCardNo").val(ui.item.person_addr_card_no);
		$("#txtCreditorAddrCardMoo").val(ui.item.person_addr_card_moo);
		$("#txtCreditorAddrCardRoad").val(ui.item.person_addr_card_road);
		$("#txtCreditorAddrCardPhone").val(ui.item.person_phone);
		$("#ddlCreditorProvince").select2("val", ui.item.person_addr_card_province_id);
		
		$("#divCreditorAmphur").load(base_url+"non_loan/amphur_creditor/"+ui.item.person_addr_card_province_id, function(response, status, xhr ){
			$("#ddlCreditorAmphur").select2("val", ui.item.person_addr_card_amphur_id);
		});
		
		$("#divCreditorDistrict").load(base_url+"non_loan/district_creditor/"+ui.item.person_addr_card_amphur_id, function(response, status, xhr ){
			$("#ddlCreditorDistrict").select2("val", ui.item.person_addr_card_district_id);
		});
	},
	change: function(event, ui){
		event.preventDefault();
		
		if(ui.item == null && typeof(ui.item) !== "undefined")
		{
			//not found
		}
	}
});

String.prototype.replaceAt = function(index, char){
    return this.substr(0, index) + "<span style=\'color:red;\'>" + char + "</span>";
}

$.ui.autocomplete.prototype._renderItem = function(ul, item){
	this.term = this.term.toLowerCase();
	var resultStr = item.label.toLowerCase();
	var t = "";
	
	while (resultStr.indexOf(this.term) != -1)
	{
		var index = resultStr.indexOf(this.term);
		t = t + item.label.replaceAt(index, item.label.slice(index, index + this.term.length));
		resultStr = resultStr.substr(index + this.term.length);
		item.label = item.label.substr(index + this.term.length);
	}
	
	return $("<li></li>").data("item.autocomplete", item).append("<a>" + t + item.label + "</a>").appendTo(ul);
};	

$("#ddlProvince").change(function(){
	$("#divAmphur").load(base_url+"non_loan/amphur/"+$("#ddlProvince").val());
	$("#divDistrict").load(base_url+"non_loan/district");
});

$("#ddlAmphur").change(function(){
	$("#divDistrict").load(base_url+"non_loan/district/"+$("#ddlAmphur").val());
});

$("#ddlCreditorProvince").change(function(){
	$("#divCreditorAmphur").load(base_url+"non_loan/amphur_creditor/"+$("#ddlCreditorProvince").val());
	$("#divCreditorDistrict").load(base_url+"non_loan/district_creditor");
});

$("#ddlCreditorAmphur").change(function(){
	$("#divCreditorDistrict").load(base_url+"non_loan/district_creditor/"+$("#ddlCreditorAmphur").val());
});

$("#ddlCase").change(function(){
	if($("#ddlCase").val()=="0")
	{
		$("#divOther").show();
	}
	else
	{
		$("#divOther").hide();
	}
});

$("#frmNonLoan").validate({
	rules: {
		txtRai: {
			min:0,
			digits: true,
			required: true
		},
		txtNgan: {
			min:0,
			max:3,
			digits: true,
			required: true
		},
		txtWah: {
			min:0,
			max:99,
			digits: true,
			required: true
		},
		txtThaiid: {
			minlength: 13,
			digits: true,
			required: true
		},
		txtAddrCardMoo: {
			digits: true,
			required: true
		},
		txtCreditorThaiid: {
			minlength: 13,
			digits: true
		},
		txtCreditorAddrCardMoo: {
			digits: true
		}
	},
	highlight: function(element) {
		$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
	},
	success: function(element) {
		$(element).closest(".form-group").removeClass("has-error");
	}
});

$.validator.addMethod("thaiid", function(value, element) {
	if(value.length != 13)
	{
		return false;
	}
	else
	{
		for(i=0, sum=0; i < 12; i++) sum += parseFloat(id.charAt(i))*(13-i); 
		
		return (11-sum%11)%10!=parseFloat(id.charAt(12))? false : true;
	}
},"Please enter format not correctly.");