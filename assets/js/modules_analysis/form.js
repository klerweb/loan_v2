function expenditure() {
  $.post(base_url + "analysis/expenditure/" + $("#txtLoan").val() + "/" + $("#txtId").val(), function(data) {
    $("#divExpenditure").html(data);
  });
}

function edit_expenditure(id, name, year, amount) {
  $("#txtIdExpenditure").val(id);
  $("#txtNameExpenditure").val(name);
  $("#txtYearExpenditure").val(year);
  $("#txtExpenditure").val(amount);
  $("#modalExpenditure").modal("show");
}

function cal_analysis() {
  $.post(base_url + "analysis/cal", {
    income: $("#txtYear" + $("#ddlCalIncomeYear").val() + "Sum").val(),
    amount: $("#txtCalAmount").val(),
    interest: $("#ddlCalInterest").val(),
    type: $("#ddlCalType").val(),
    year: $("#txtCalYear").val(),
    pay: $("#txtCalPay").val(),
    start: $("#txtCalStart").val()
  }, function(data) {
    $("#divCal").html(data);
  });
}

function edit_other(round, amount) {
  $("#txtRound").val(round);
  $("#txtOther").val(amount);
  $("#modalOther").modal("show");
}

function edit_principle_other(round, principle_amount) {
  $("#txtRound").val(round);
  $("#txtPrincipleOther").val(principle_amount);
  $("#modalPrincipleOther").modal("show");
}

function edit_interest_other(round, interest_amount) {
  $("#txtRound").val(round);
  $("#txtInterestOther").val(interest_amount);
  $("#modalInterestOther").modal("show");
}

$("#chkEgov").change(function() {
  if (this.checked) {
    $("#divEgov").show();
  } else {
    $("#divEgov").hide();
  }
});

$("#chkCooperative").change(function() {
  if (this.checked) {
    $("#divCooperative").show();
  } else {
    $("#divCooperative").hide();
  }
});

$("#chkBank").change(function() {
  if (this.checked) {
    $("#divBank").show();
  } else {
    $("#divBank").hide();
  }
});

$("#chkBankOther").change(function() {
  if (this.checked) {
    $("#divBankOther").show();
  } else {
    $("#divBankOther").hide();
  }
});

$("#rdApproveStatus1").click(function() {
  $("#divDisapprove").hide();
  $("#divApprove").show();
});

$("#rdApproveStatus0").click(function() {
  $("#divApprove").hide();
  $("#divDisapprove").show();
});

$("#rdApproveSectorStatus1").click(function() {
  $("#divDisapproveSector").hide();
  $("#divApproveSector").show();
});

$("#rdApproveSectorStatus0").click(function() {
  $("#divApproveSector").hide();
  $("#divDisapproveSector").show();
});

$("#ddlCalInterest, #ddlCalType, #ddlCalIncomeYear").select2({
  minimumResultsForSearch: -1
});

$("#txtWithin").datepicker({
  dateFormat: "dd/mm/yy",
  isBuddhist: true,
  dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
  dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
  monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
  monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
  yearRange: "-100:+0",
  maxDate: "0",
  changeMonth: true,
  changeYear: true,
  beforeShow: function() {
    if ($(this).val() != "") {
      var arrayDate = $(this).val().split("/");
      arrayDate[2] = parseInt(arrayDate[2]) - 543;
      $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
    }
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onChangeMonthYear: function() {
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onClose: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]) + 543;
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  },
  onSelect: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]);
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  }
});

$("#txtDeadline").datepicker({
  dateFormat: "dd/mm/yy",
  isBuddhist: true,
  dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
  dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
  monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
  monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
  yearRange: "-100:+0",
  maxDate: "0",
  changeMonth: true,
  changeYear: true,
  beforeShow: function() {
    if ($(this).val() != "") {
      var arrayDate = $(this).val().split("/");
      arrayDate[2] = parseInt(arrayDate[2]) - 543;
      $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
    }
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onChangeMonthYear: function() {
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onClose: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]) + 543;
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  },
  onSelect: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]);
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  }
});

$("#txtCalStart").datepicker({
  dateFormat: "dd/mm/yy",
  isBuddhist: true,
  dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
  dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
  monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
  monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
  yearRange: "-100:+1",
  maxDate: "1Y",
  changeMonth: true,
  changeYear: true,
  beforeShow: function() {
    if ($(this).val() != "") {
      var arrayDate = $(this).val().split("/");
      arrayDate[2] = parseInt(arrayDate[2]) - 543;
      $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
    }
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onChangeMonthYear: function() {
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onClose: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]) + 543;
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  },
  onSelect: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]);
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  }
});

$("#txtCalPay").datepicker({
  dateFormat: "dd/mm/yy",
  isBuddhist: true,
  dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
  dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
  monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
  monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
  yearRange: "-100:+1",
  maxDate: "1Y",
  changeMonth: true,
  changeYear: true,
  beforeShow: function() {
    if ($(this).val() != "") {
      var arrayDate = $(this).val().split("/");
      arrayDate[2] = parseInt(arrayDate[2]) - 543;
      $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
    }
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onChangeMonthYear: function() {
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onClose: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]) + 543;
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  },
  onSelect: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]);
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  }
});

$("#txtApproveDate").datepicker({
  dateFormat: "dd/mm/yy",
  isBuddhist: true,
  dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
  dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
  monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
  monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
  yearRange: "-100:+0",
  maxDate: "0",
  changeMonth: true,
  changeYear: true,
  beforeShow: function() {
    if ($(this).val() != "") {
      var arrayDate = $(this).val().split("/");
      arrayDate[2] = parseInt(arrayDate[2]) - 543;
      $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
    }
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onChangeMonthYear: function() {
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onClose: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]) + 543;
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  },
  onSelect: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]);
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  }
});

$("#txtApproveSectorDate").datepicker({
  dateFormat: "dd/mm/yy",
  isBuddhist: true,
  dayNames: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"],
  dayNamesMin: ["อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส."],
  monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
  monthNamesShort: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
  yearRange: "-100:+0",
  maxDate: "0",
  changeMonth: true,
  changeYear: true,
  beforeShow: function() {
    if ($(this).val() != "") {
      var arrayDate = $(this).val().split("/");
      arrayDate[2] = parseInt(arrayDate[2]) - 543;
      $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
    }
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onChangeMonthYear: function() {
    setTimeout(function() {
      $.each($(".ui-datepicker-year option"), function(j, k) {
        var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
        $(".ui-datepicker-year option").eq(j).text(textYear);
      });
    }, 50);
  },
  onClose: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]) + 543;
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  },
  onSelect: function(dateText, inst) {
    var arrayDate = dateText.split("/");
    arrayDate[2] = parseInt(arrayDate[2]);
    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
  }
});

$("#iconWithin").click(function() {
  $("#txtWithin").datepicker("show");
});

$("#iconDeadline").click(function() {
  $("#txtDeadline").datepicker("show");
});

$("#iconStart").click(function() {
  $("#txtCalStart").datepicker("show");
});

$("#iconPay").click(function() {
  $("#txtCalPay").datepicker("show");
});

$("#iconApprove").click(function() {
  $("#txtApproveDate").datepicker("show");
});

$("#iconApproveSector").click(function() {
  $("#txtApproveDateSector").datepicker("show");
});

$("#btnExpenditure").click(function() {
  if ($("#frmExpenditure").valid()) {
    $("#modalExpenditure").modal("toggle");

    $.post(base_url + "analysis/save_expenditure", {
      loan: $("#txtLoan").val(),
      id: $("#txtIdExpenditure").val(),
      name: $("#txtNameExpenditure").val(),
      year: $("#txtYearExpenditure").val(),
      amount: $("#txtExpenditure").val()
    }, function(data) {
      expenditure();
      cal_analysis();
    });
  }
});

$("#btnCal").click(function() {
  $("#txtAnaCalYear").val($("#txtCalYear").val());
  $("#txtAnaCalType").val($("#ddlCalType").val());
  $("#txtAnaCalAmount").val($("#txtCalAmount").val());

  //$.post(base_url+"analysis/cal_ana", { year: $("#txtAnaCalYear").val(), type: $("#txtAnaCalType").val(), amount: $("#txtAnaCalAmount").val() }, function(data) {
  //	$("#divCalAna").html(data);
  //	$("#modalAnaCal").modal("show");
  //});

  $.post(base_url + "analysis/cal_ana2", {
    id: $("#txtId").val(),
    cycle: $("#txtCycle").val(),
    // year: $("#txtAnaCalYear").val(),
    // type: $("#txtAnaCalType").val(),
    amount: $("#txtAnaCalAmount").val()
  }, function(data) {
    $("#divCalAna2").html(data);
    $("#modalAnaCal2").modal("show");
  });
});

$("#ddlCalType").change(function() {
  var cycle = 0;
  var year = parseInt($("#txtCalYear").val());
  var type = parseInt($("#ddlCalType").val());
  cycle = year * 12 / type;

  $("#txtCycle").val(cycle);
  cal_analysis();
});

$("#btnAnaCal").click(function() {
  if ($("#frmAnaCal").valid()) {
    $("#modalAnaCal").modal("toggle");

    $.post(base_url + "analysis/save_cal", $("#frmAnaCal").serialize(), function(data) {
      cal_analysis();
    });
  }
});

$("#btnAnaCal2").click(function() {
  var use = cal_time();
  var amount = parseFloat($("#txtAnaCalAmount").val().replace(/,/g, ""));

  if (use == amount) {
    $("#modalAnaCal2").modal("toggle");

    $.post(base_url + "analysis/save_cal2", $("#frmAnaCal2").serialize(), function(data) {
      cal_analysis();
    });
  } else {
    alert("เงินต้นไม่พอดี");
  }
});

function cal_time() {
  var time = parseInt($("#txtCalTime").val());

  sum = 0;
  for (i = 1; i <= time; i++) {
    end = parseInt($("#txtCalEnd_" + i).val());
    start = parseInt($("#txtCalStart_" + i).val());

    amount = parseFloat($("#txtCalAmount_" + i).val());
    round = (end - start) + 1;

    sum += round * amount;
  }

  return sum;
}

$("#btnOther").click(function() {
  if ($("#frmOther").valid()) {
    $("#modalOther").modal("toggle");

    $.post(base_url + "analysis/save_other", {
      round: $("#txtRound").val(),
      other: $("#txtOther").val()
    }, function(data) {
      cal_analysis();
    });
  }
});

$("#btnPrincipleOther").click(function() {
  if ($("#frmPrincipleOther").valid()) {
    $("#modalPrincipleOther").modal("toggle");

    $.post(base_url + "analysis/save_principle_other", {
      round: $("#txtRound").val(),
      principle_other: $("#txtPrincipleOther").val()
    }, function(data) {
      cal_analysis();
    });
  }
});

$("#btnInterestOther").click(function() {
  if ($("#frmInterestOther").valid()) {
    $("#modalInterestOther").modal("toggle");

    $.post(base_url + "analysis/save_interest_other", {
      round: $("#txtRound").val(),
      interest_other: $("#txtInterestOther").val()
    }, function(data) {
      cal_analysis();
    });
  }
});

$("#frmAnalysis").validate({
  rules: {
    txtCalYear: {
      min: 1,
      digits: true,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
  },
  success: function(element) {
    $(element).closest(".form-group").removeClass("has-error");
  }
});

$("#frmAnaCal").validate({
  rules: {
    "txtCalAmount[]": {
      min: 0,
      sum: true,
      number: true,
      required: true
    },
    "txtCalOther[]": {
      min: 0,
      digits: true,
      required: true
    },
    "txtCalPrinciple[]": {
      min: 0,
      digits: true,
      required: true
    },
    "txtCalInterest[]": {
      min: 0,
      digits: true,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
  },
  success: function(element) {
    $(element).closest(".form-group").removeClass("has-error");
  }
});

$.validator.addMethod("sum", function(value, element) {
  var sum = 0;

  $("input[name='txtCalAmount[]']").each(function() {
    sum += parseFloat($(this).val());
  });

  return parseFloat($("#txtAnaCalAmount").val().replace(/,/g, "")) == sum;

}, "Please enter a value less than to amount.");

$("#frmExpenditure").validate({
  rules: {
    txtExpenditure: {
      min: 0,
      digits: true,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
  },
  success: function(element) {
    $(element).closest(".form-group").removeClass("has-error");
  }
});

$("#frmOther").validate({
  rules: {
    txtOther: {
      min: 0,
      digits: true,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
  },
  success: function(element) {
    $(element).closest(".form-group").removeClass("has-error");
  }
});

$("#frmPrincipleOther").validate({
  rules: {
    txtPrincipleOther: {
      min: 0,
      digits: true,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
  },
  success: function(element) {
    $(element).closest(".form-group").removeClass("has-error");
  }
});

$("#frmInterestOther").validate({
  rules: {
    txtInterestOther: {
      min: 0,
      digits: true,
      required: true
    }
  },
  highlight: function(element) {
    $(element).closest(".form-group").removeClass("has-success").addClass("has-error");
  },
  success: function(element) {
    $(element).closest(".form-group").removeClass("has-error");
  }
});


      expenditure();
      setTimeout(function() {
        cal_analysis();
      }, 5000);

      // var cycle = 0;
      // var year = parseInt($("#txtCalYear").val());
      // var type = parseInt($("#ddlCalType").val());
      // cycle = year * 12 / type;
      //
      // $("#txtCycle").val(cycle);
