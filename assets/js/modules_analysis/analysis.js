function edit(loan_id, id)
{
	if(confirm("คุณต้องการแก้ไขแบบพิจารณาใช่หรือไม่?"))
	{
		window.location = "analysis/form/edit/"+loan_id+"/"+id;
    }
}

function del(id)
{
	if(confirm("คุณต้องการยกเลิกแบบพิจารณาใช่หรือไม่?"))
	{
		window.location = "analysis/del/"+id;
    }
}