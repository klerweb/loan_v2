function edit(id)
{
	if(confirm("คุณต้องการแก้ไขแบบประเมินใช่หรือไม่?"))
	{
		window.location = "summary/form/edit/"+id;
    }
}

function del(id)
{
	if(confirm("คุณต้องการยกเลิกแบบประเมินใช่หรือไม่?"))
	{
		window.location = "summary/del/"+id;
    }
}