<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	public function __construct() {
		parent::__construct();
		
		if(get_uid_login()){
			$session_x = $this->session->userdata('session_x');
			$this->session->set_userdata('session_x', $session_x);	
		}else{
			$this->session->unset_userdata('session_x');
			redirect_url(base_url(),'กรุณาล๊อกอินเข้าสู่ระบบ');
		}
	}	
}