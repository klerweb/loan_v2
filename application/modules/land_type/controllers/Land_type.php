<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Land_type extends MY_Controller
{
	private $title_page = 'ประเภทที่ดิน';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('land_type_model', 'land_type');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->land_type->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_land_type/land_type.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('land_type', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('land_type')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มประเภทที่ดิน';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('land_type_model', 'land_type');
			$data_content['data'] = $this->land_type->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มประเภทที่ดิน';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขประเภทที่ดิน';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_land_type/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('land_type_model', 'land_type');
		
		$array['land_type_name'] = $this->input->post('txtLandTypeName');
		$array['land_type_status'] = '1';
		
		$id = $this->land_type->save($array, $this->input->post('txtLandTypeId'));
		
		if($this->input->post('txtLandTypeId')=='')
		{
			$data = array(
					'alert' => 'บันทึกประเภทที่ดินเรียบร้อยแล้ว',
					'link' => site_url('land_type'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขประเภทที่ดินเรียบร้อยแล้ว',
					'link' => site_url('land_type'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('land_type_model', 'land_type');
		$this->land_type->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกประเภทที่ดินเรียบร้อยแล้ว',
				'link' => site_url('land_type'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['land_type_name'] = '';
		
		return $data;
	}
}