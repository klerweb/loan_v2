<?php
class Bank_payment extends MY_Controller{
	private $title_page = 'ธนาคารรับชำระหนี้เงินกู้';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		
		$this->load->model('bank_payment_model', 'bank_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'setting'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		$date=explode("-",date("Y-m-d"));
		$data_content = array(
				'show_bank_payment' => $this->bank_payment_m->load_show_bank_payment()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('show_bank_payment', $data_content, TRUE)
		);
		$this->parser->parse('main', $data);
	}
	public function edit_form_bank($com_code)
	{
		$this->load->model('bank_payment_model', 'bank_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'setting'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#','ธนาคารรับชำระหนี้เงินกู้'=>site_url('bank_payment'));
		$data_content = array(
				'show_bank_edit' => $this->bank_payment_m->edit_form_bank($com_code),
				'bank_name'=> $this->bank_payment_m->load_bank_name()
		);
		$title_page="แก้ไขข้อมูลธนาคารรับชำระหนี้เงินกู้";
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form_edit_bank_payment', $data_content, TRUE)
		);
		$this->parser->parse('main', $data);
	}
	public function save_edit_form_bank()
	{
		$this->load->model('bank_payment_model', 'bank_payment_m');
		$data_edit_bank=$this->bank_payment_m->edit_f_bank();
		echo $data_edit_bank;
	}
	public function load_bank_form()
	{
		$this->load->model('bank_payment_model', 'bank_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'setting'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#','ธนาคารรับชำระหนี้เงินกู้'=>site_url('bank_payment'));
		$data_content = array(
				'bank_name'=> $this->bank_payment_m->load_bank_name_active()
		);
		$title_page="เพิ่มข้อมูลธนาคารรับชำระหนี้เงินกู้";
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form_bank_payment', $data_content, TRUE)
		);
		$this->parser->parse('main', $data);
	}
	public function save_form_bank()
	{
		$this->load->model('bank_payment_model', 'bank_payment_m');
		$data_save_bank=$this->bank_payment_m->save_f_bank();
		echo $data_save_bank;
	}
	
}
?>