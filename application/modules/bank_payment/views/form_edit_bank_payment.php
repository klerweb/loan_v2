<script type="text/javascript">
$(document).ready(function(){
	$("#edit_data").click(function(){
		
		var b=true;
		if ($("#txtaccount_name").val()=="")
		{
			alert("กรุณาใส่ชื่อบัญชี");
			b=false;
		}
		else if ($("#ddlStatus").val()==-1)
		{
			alert("กรุณาเลือกประเภทบัญชี");
			b=false;
		}
		else if ($("#ddlObjective").val()==-1)
		{
			alert("กรุณาเลือกธนาคาร");
			b=false;
		}
		else if ($("#txtbranch_name").val()=="")
		{
			alert("กรุณาใส่สาขาธนาคาร");
			b=false;
		}
		else if ($("#ddlTitle").val()==-1)
		{
			alert("กรุณาเลือกสถานะของบัญชี");
			b=false;
		}
		
		if (b)
		{
			
			var loan_id=$('#loan_id').val();
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "bank_payment/save_edit_form_bank",
			data: {com_code:$("#txtcomcode").val(),account_name:$("#txtaccount_name").val(),type_bank:$("#ddlStatus").val(),bank_id:$("#ddlObjective").val(), branch_name:$("#txtbranch_name").val(),status_bank:$("#ddlTitle").val()}})
			.success(function(result2) {
				
				if(result2=="success")
				{
					
					window.location.href='<?php echo site_url()?>bank_payment';
				}
				else
				{
					alert(result2);
				}
			});
		}
	});
		});


//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
//------------id_card
function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
        var pattern=new String("___-______-_"); // กำหนดรูปแบบในนี้
        var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){           
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);           
        }
}
//--------------end id card-----------
</script>
<?php
$split_data=explode(",",$show_bank_edit);
?>
<form>
	<input type="hidden" id="txtBankId" name="txtBankId" value="" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-bank">แก้ไขข้อมูลธนาคารรับชำระหนี้เงินกู้</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมายเลขบัญชีธนาคาร <span class="asterisk">*</span></label>
								<input type="text" name="txtcomcode" id="txtcomcode" class="form-control" value="<?php echo $split_data[0];?>" maxlength="12" disabled="disabled" onkeyup="autoTab(this)"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ชื่อบัญชีธนาคาร<span class="asterisk">*</span></label>
								<input type="text" name="txtaccount_name" id="txtaccount_name" class="form-control" value="<?php echo $split_data[1];?>" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ประเภทบัญชี<span class="asterisk">*</span></label>
								<select name="ddlStatus" id="ddlStatus" data-placeholder="กรุณาเลือกบัญชีธนาคาร" class="width100p" required>
									<option value="-1">กรุณาเลือกประเภทบัญชี</option>
									<option value="ออมทรัพย์" <?php if($split_data[2]=="ออมทรัพย์") { echo ' SELECTED'; }?>>ออมทรัพย์</option>
									<option value="ฝากประจำ" <?php if($split_data[2]=="ฝากประจำ") { echo ' SELECTED'; }?>>ฝากประจำ</option>
									<option value="กระแสรายวัน" <?php if($split_data[2]=="กระแสรายวัน") { echo ' SELECTED'; }?>>กระแสรายวัน</option>
							</select> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">กรุณาเลือกธนาคาร<span class="asterisk">*</span></label>
								<select name="ddlObjective" id="ddlObjective" data-placeholder="กรุณาเลือกธนาคาร" class="width100p" required>
									<option value="-1">กรุณาเลือกธนาคาร</option>
									<?php
									$split_bank_name=explode("\\",$bank_name);
									for ($array_bank=0;$array_bank<count($split_bank_name)-1;$array_bank++)
									{ 
										$split_bank_name2=explode(",",$split_bank_name[$array_bank]);
								?>
								<option value=<?php echo $split_bank_name2[0]; if($split_bank_name2[0]==$split_data[4]) { echo ' SELECTED'; }?>>  <?php echo $split_bank_name2[1];?> </option>
									<?php } ?>
							</select> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">สาขา<span class="asterisk">*</span></label>
								<input type="text" name="txtbranch_name" id="txtbranch_name" class="form-control" value="<?php echo $split_data[3];?>" maxlength="100" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">สถานะบัญชี<span class="asterisk">*</span></label>
								<select name="ddlTitle" id="ddlTitle" data-placeholder="สถานะบัญชี" class="width100p" required>
									<option value=-1>สถานะบัญชี*</option>
									<?php
			$split_person_payable=explode(",",$split_data_person[$i_person]); ?>
		<option value=1 <?php if($split_data[5]==1) { echo ' SELECTED'; }?>>เปิด </option>
		<option value=0 <?php if($split_data[5]==0) { echo ' SELECTED'; }?>>ปิด </option>

							</select> 
							</div><!-- form-group -->
							
						</div><!-- col-sm-3 -->
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button type="button" class="btn btn-primary" id="edit_data" name="edit_data">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>
