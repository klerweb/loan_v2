<script type="text/javascript">
$(document).ready(function(){
	
});
function exportToExcel(tableID){
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6' style='height: 75px; text-align: center; width: 250px'>";
    var textRange; var j=0;
    tab = document.getElementById(tableID); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {

        tab_text=tab_text;

        tab_text=tab_text+tab.rows[j].innerHTML.toUpperCase()+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text= tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); //remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer

    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write( 'sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"sudhir123.txt");
    }

    else {
       sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
    
    return (sa);
}
function fncSubmit()
{
	if ($("#txtDate").val()=="")
	 {
		 alert("กรุณาเลือกวันที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}

</script>
<?php

function cal_date($fdate,$ldate)
{
	//-------------------cal date
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);                //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$u_y=date("Y", $mage)-1970;
	$u_m=date("m",$mage)-1;
	$u_d=date("d",$mage)-1;
	$show_date="";
	if($u_y==0){$u_y="";} else {$u_y=$u_y." ปี";}
	if($u_m==0){$u_m="";} else {$u_m=$u_m." เดือน";}
	if($u_d==0){$u_d="";} else {$u_d=$u_d." วัน";}
 return "  $u_y $u_m $u_d";
 
//---------------end caldate	
}
function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return floor($result);	
}
$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
$month1 = array("ม.ค.", "ก.พ.", "มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
?>
<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url('bank_payment/load_bank_form'); ?>'">เพิ่มธนาคารรับชำระเงินกู้</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
						<th>หมายเลขบัญชีธนาคาร</th>
						<th>ชื่อบัญชี</th>
						<th>ประเภทของบัญชี</th>
						<th>ธนาคาร</th>
						<th>สาขาธนาคาร</th>
						<th>สถานะของบัญชี</th>
						<th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				if($show_bank_payment!="no data")
				{
					$i=1;
					$split_array_bank=explode("\\",$show_bank_payment);
					for($j=0;$j<count($split_array_bank)-1;$j++)
					{
						$split_data_bank=explode(",",$split_array_bank[$j]);
						echo "<tr>";
						echo "<td>".$i."</td>";
						echo "<td>".$split_data_bank[0]."</td>";
						echo "<td>".$split_data_bank[1]."</td>";
						echo "<td>".$split_data_bank[2]."</td>";
						echo "<td>".$split_data_bank[4]."</td>";
						echo "<td>".$split_data_bank[3]."</td>";
						echo "<td>";
						if ($split_data_bank[5]==1){ echo "ยังมีการใช้งานอยู่";} else { echo "ยกเลิกการใช้งาน"; }
						echo "</td>";
						echo "<td>"?><button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="
							if(confirm('คุณต้องการแก้ไขข้อมูลธนาคารใช่หรือไม่?'))
	{ location.href='<?php echo site_url()?>Bank_payment/edit_form_bank/<?php echo $split_data_bank[0];?>';
	}"><span class="fa fa-edit">
						<?php echo "</td>";
						echo "</tr>";
						$i++;

					}
				}
?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>