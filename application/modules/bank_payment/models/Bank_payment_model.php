<?php
class Bank_payment_model extends CI_Model{

	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	function load_show_bank_payment()
	{
		$data_bank_payment="";
		$query_data_bank_loan_payment=$this->db->get('bank_loan_payment');
		if($query_data_bank_loan_payment->num_rows()>0)
		{
			foreach($query_data_bank_loan_payment->result() as $data_bank_loan_payment)
			{
				$this->db->where('bank_id',$data_bank_loan_payment->bank_id);
				$query_bank_name=$this->db->get('config_bank');
				$data_bank_name=$query_bank_name->result();
				$data_bank_payment=$data_bank_payment.$data_bank_loan_payment->com_code.",".$data_bank_loan_payment->account_name.",".$data_bank_loan_payment->bank_type_name.",".$data_bank_loan_payment->bank_branch_name.",".$data_bank_name[0]->bank_name.",".$data_bank_loan_payment->status_com_code."\\";
			}
			return $data_bank_payment;
		}
		else
		{
			return "no data";
		}

	}
	function edit_form_bank($com_code)
	{
		$data_bank_payment="";
		$this->db->where('com_code',$com_code);
		$query_data_bank_loan_payment=$this->db->get('bank_loan_payment');
		if($query_data_bank_loan_payment->num_rows()>0)
		{
			foreach($query_data_bank_loan_payment->result() as $data_bank_loan_payment)
			{
				$this->db->where('bank_id',$data_bank_loan_payment->bank_id);
				$query_bank_name=$this->db->get('config_bank');
				$data_bank_name=$query_bank_name->result();
				$data_bank_payment=$data_bank_payment.$data_bank_loan_payment->com_code.",".$data_bank_loan_payment->account_name.",".$data_bank_loan_payment->bank_type_name.",".$data_bank_loan_payment->bank_branch_name.",".$data_bank_loan_payment->bank_id.",".$data_bank_loan_payment->status_com_code;
			}
			return $data_bank_payment;
		}
		else
		{
			return "no data";
	}
	}
	function load_bank_name()
	{
		$query_data_bank_name=$this->db->get('config_bank');
		$bank_name="";
		if($query_data_bank_name->num_rows()>0)
		{
			foreach($query_data_bank_name->result() as $data_bank_name)
			{
				$bank_name=$bank_name.$data_bank_name->bank_id.",".$data_bank_name->bank_name."\\";
			}
			return $bank_name;
		}
		else
		{
			return "no data";
		}
	}
	function edit_f_bank()
	{
		$update_bank_payment=array(
			'account_name'=>$_POST['account_name'],
			'status_com_code'=>$_POST['status_bank'],
			'bank_id'=>$_POST['bank_id'],
			'bank_type_name'=>$_POST['type_bank'],
			'bank_branch_name'=>$_POST['branch_name'],
			'updatedate'=>date("Y-m-d")
	);
		$this->db->where('com_code',$_POST['com_code']);
		if($this->db->update('bank_loan_payment',$update_bank_payment))
		{
			return "success";
		}
		else
		{
			return "ไม่สามารถบันทึกได้";
		}
	}
	function load_bank_name_active()
	{
		$this->db->where('bank_status','1');
		$query_data_bank_name=$this->db->get('config_bank');
		$bank_name="";
		if($query_data_bank_name->num_rows()>0)
		{
			foreach($query_data_bank_name->result() as $data_bank_name)
			{
				$bank_name=$bank_name.$data_bank_name->bank_id.",".$data_bank_name->bank_name."\\";
			}
			return $bank_name;
		}
		else
		{
			return "no data";
		}
	}
	function save_f_bank()
	{
		$insert_bank_payment=array(
			'com_code'=>$_POST['com_code'],
			'account_name'=>$_POST['account_name'],
			'status_com_code'=>$_POST['status_bank'],
			'bank_id'=>$_POST['bank_id'],
			'bank_type_name'=>$_POST['type_bank'],
			'bank_branch_name'=>$_POST['branch_name'],
			'createddate'=>date("Y-m-d"),
			'updatedate'=>date("Y-m-d")
	);
		if($this->db->insert('bank_loan_payment',$insert_bank_payment))
		{
			return "success";
		}
		else
		{
			return "ไม่สามารถบันทึกได้";
		}
	}

}
?>