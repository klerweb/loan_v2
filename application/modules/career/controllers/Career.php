<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends MY_Controller
{
	private $title_page = 'อาชีพ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('career_model', 'career');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->career->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_career/career.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('career', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('career')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มอาชีพ';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('career_model', 'career');
			$data_content['data'] = $this->career->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มอาชีพ';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขอาชีพ';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_career/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('career_model', 'career');
		
		$array['career_name'] = $this->input->post('txtCareerName');
		$array['career_status'] = '1';
		
		$id = $this->career->save($array, $this->input->post('txtCareerId'));
		
		if($this->input->post('txtCareerId')=='')
		{
			$data = array(
					'alert' => 'บันทึกอาชีพเรียบร้อยแล้ว',
					'link' => site_url('career'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขอาชีพเรียบร้อยแล้ว',
					'link' => site_url('career'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('career_model', 'career');
		$this->career->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกอาชีพเรียบร้อยแล้ว',
				'link' => site_url('career'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['career_name'] = '';
		
		return $data;
	}
}