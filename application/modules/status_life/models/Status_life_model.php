<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_life_model extends CI_Model
{
	private $table = 'master_status_life';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("status_life_status", "1");
		$this->db->order_by("status_life_id", "asc");
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>