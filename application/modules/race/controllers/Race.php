<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Race extends MY_Controller
{
	private $title_page = 'เชื้อชาติ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('race_model', 'race');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->race->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_race/race.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('race', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('race')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มเชื้อชาติ';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('race_model', 'race');
			$data_content['data'] = $this->race->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มเชื้อชาติ';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขเชื้อชาติ';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_race/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('race_model', 'race');
		
		$array['race_name'] = $this->input->post('txtRaceName');
		$array['race_status'] = '1';
		
		$id = $this->race->save($array, $this->input->post('txtRaceId'));
		
		if($this->input->post('txtRaceId')=='')
		{
			$data = array(
					'alert' => 'บันทึกเชื้อชาติเรียบร้อยแล้ว',
					'link' => site_url('race'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขเชื้อชาติเรียบร้อยแล้ว',
					'link' => site_url('race'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('race_model', 'race');
		$this->race->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกเชื้อชาติเรียบร้อยแล้ว',
				'link' => site_url('race'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['race_name'] = '';
		
		return $data;
	}
}