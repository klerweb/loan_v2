<form id="frmRace" name="frmRace" action="<?php echo base_url().'race/save'; ?>" method="post">
	<input type="hidden" id="txtRaceId" name="txtRaceId" value="<?php echo $id; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-race">เชื้อชาติ</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">เชื้อชาติ <span class="asterisk">*</span></label>
								<input type="text" name="txtRaceName" id="txtRaceName" class="form-control" value="<?php echo $data['race_name']; ?>" maxlength="100" required />
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>