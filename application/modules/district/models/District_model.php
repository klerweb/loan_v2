<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District_model extends CI_Model
{
	private $table = 'master_district';

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function getByAmphur($amphur)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("amphur_id", $amphur);
		$this->db->order_by("district_name", "asc");
		$query = $this->db->get();

		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}

	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("district_id", $id);
		$query = $this->db->get();

		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}

		return $result;
	}

	public function get_by_id($id){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("district_id",$id);
		$query = $this->db->get();

		$result = $query->row_array();

		return $result;
	}
	public function getZip($id){
			$this->db->select("*");
			$this->db->from('master_zipcode');
			$this->db->where("district_id",$id);
			$query = $this->db->get();
			if($query->num_rows()!=0)
			{
				$result = $query->row_array();
			}else
			{
				$result = null;
			}

			return $result;
	}

}
?>
