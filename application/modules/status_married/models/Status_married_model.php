<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status_married_model extends CI_Model
{
	private $table = 'master_status_married';

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("status_married_status", "1");
		$this->db->order_by("status_married_id", "asc");
		$query = $this->db->get();

		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}

	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("status_married_id", $id);
		$this->db->where("status_married_status", "1");
		$query = $this->db->get();

		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}

		return $result;
	}
}
?>
