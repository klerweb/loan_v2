<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends MY_Controller
{
	private $title_page = 'โครงการ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('project_model', 'project');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->project->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_project/project.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('project', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('project')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มโครงการ';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('project_model', 'project');
			$data_content['data'] = $this->project->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มโครงการ';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขโครงการ';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_project/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('project_model', 'project');
		
		$array['project_name'] = $this->input->post('txtProjectName');
		$array['project_year'] = $this->input->post('ddlProjectYear');
		$array['project_status'] = '1';
		
		$id = $this->project->save($array, $this->input->post('txtProjectId'));
		
		if($this->input->post('txtProjectId')=='')
		{
			$data = array(
					'alert' => 'บันทึกโครงการเรียบร้อยแล้ว',
					'link' => site_url('project'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขโครงการเรียบร้อยแล้ว',
					'link' => site_url('project'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('project_model', 'project');
		$this->project->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกโครงการเรียบร้อยแล้ว',
				'link' => site_url('project'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['project_name'] = '';
		$data['project_year'] = intval(date('Y'))+543;
		
		return $data;
	}
}