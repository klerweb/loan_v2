<form id="frmProject" name="frmProject" action="<?php echo base_url().'project/save'; ?>" method="post">
	<input type="hidden" id="txtProjectId" name="txtProjectId" value="<?php echo $id; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-project">โครงการ</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">โครงการ <span class="asterisk">*</span></label>
								<input type="text" name="txtProjectName" id="txtProjectName" class="form-control" value="<?php echo $data['project_name']; ?>" maxlength="100" required />
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">ปีงบประมาณ <span class="asterisk">*</span></label>
								<select name="ddlProjectYear" id="ddlProjectYear" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php 
										for ($y = 2017; $y <= intval(date('Y')); $y++)
										{
											$y_thai = $y + 543;
									?>
											<option value="<?php echo $y_thai; ?>"<?php if($data['project_year']==$y_thai) echo ' SELECTED'; ?>><?php echo $y_thai; ?></option>
									<?php
										}
									?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>