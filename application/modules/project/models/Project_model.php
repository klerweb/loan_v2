<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_model extends CI_Model
{
	private $table = 'config_project';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("project_status", "1");
		$this->db->order_by("project_name", "asc");
		$query = $this->db->get();
		
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("project_id", $id);
		$this->db->where("project_status", "1");
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function getByYear($year)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("project_status", "1");
		$this->db->where("project_year", $year);
		$this->db->order_by("project_name", "asc");
		$query = $this->db->get();
	
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function save($array, $id='')
	{
		if($id=='')
		{
			$this->db->set($array);
			$this->db->set("project_createdate", "NOW()", FALSE);
			$this->db->set("project_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
			
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("project_updatedate", "NOW()", FALSE);
			$this->db->where("project_id", $id);
			$this->db->update($this->table, $array);
		}
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("project_status" => "0");
	
		$this->db->set($array);
		$this->db->where("project_id", $id);
		$this->db->update($this->table, $array);
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>