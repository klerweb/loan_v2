<table class="table table-bordered">
				<thead>
				<tr>
					<th>ลำดับ</th>
					<th>ประเภทสิ่งปลูกสร้าง</th>
					<th>เนื้อที่ (ตร.ม.)</th>
					<th>ราคาต่อ ตร.ม.</th>
					<th>ราคาประเมิน</th>
					<th>ราคาค่าก่อสร้างตาม แบบแปลน/สัญญา*</th>
				</tr>
				</thead>
				<tbody >			
											
<?php 
							$sum_all_price = 0;
							$sum_all_cost = 0;
							
							
							foreach ($result as $key => $list) {
								$sum_p = $list['building_assess_area'] && $list['building_assess_price_per_area'] ? $list['building_assess_area'] * $list['building_assess_price_per_area'] : 0;
								$sum_all_price += $sum_p;
								$sum_all_cost += ($list['building_assess_cost'] ? $list['building_assess_cost'] : 0);
								echo '<tr>
										<td>'.($key+1).'</td>
										<td>'.$list['building_part_desc'].','.$list['building_part_sub_desc'].'</td>
										<td><div class="form-group"><input type="text" name="building_assess_area['.$list['building_assess_id'].']" class="form-control area" value="'.($list['building_assess_area'] ? $list['building_assess_area'] : 0).'" required ></div></td>
										<td><div class="form-group"><input type="text" name="building_assess_price_per_area['.$list['building_assess_id'].']" class="form-control price" value="'.($list['building_assess_price_per_area'] ? $list['building_assess_price_per_area'] : 0).'" required ></div></td>
										<td><div class="form-group"><input type="text" name="sum_p['.$list['building_assess_id'].']" class="form-control sum_p" value="'.$sum_p.'" readonly ></div></td>
										<td><div class="form-group"><input type="text" name="building_assess_cost['.$list['building_assess_id'].']" class="form-control cost" value="'.($list['building_assess_cost'] ? $list['building_assess_cost'] : 0).'" required ></div></td>
									</tr>';
							}
							
							$depreciate_price = 0;
							$depreciate_cost = 0;
							
							$sum_all_price2 = 0;
							$sum_all_cost2 = 0;
							
							$sum_percent = $building_record_data['building_record_depreciate_sum_percent'] ? $building_record_data['building_record_depreciate_sum_percent'] : 0;
							
							$depreciate_price = ($sum_all_price * $sum_percent ) / 100;
							$depreciate_cost = ($sum_all_cost * $sum_percent ) / 100;
							
							$sum_all_price2 = $sum_all_price - $depreciate_price;
							$sum_all_cost2 = $sum_all_cost - $depreciate_cost;							
							
							?>	
						<tr style="background-color: #f9f9f9;">
							<td colspan="4">รวมราคาประเมินสิ่งปลูกสร้างก่อนหักค่าเสื่อม</td>
							<td><div class="form-group"><input type="text" name="sum_all_price" id="sum_all_price" class="form-control" value="<?php echo $sum_all_price;?>" readonly ></div></td>
							<td><div class="form-group"><input type="text" name="sum_all_cost" id="sum_all_cost" class="form-control" value="<?php echo $sum_all_cost;?>" readonly ></div></td>
						</tr>
						<tr style="background-color: #f9f9f9;">
							<td colspan="4">
										<div class="row">
									       <div class="col-sm-3 form-group">
										     <div class="input-group">
							                 	<input type="text" name="building_record_depreciate_year" id="building_record_depreciate_year" class="form-control" placeholder="หักค่าเสื่อมราคา" value="<?php echo $building_record_data['building_record_depreciate_year'] ? $building_record_data['building_record_depreciate_year'] : 0;?>" required>
							                  	<span class="input-group-addon">ปี</span>
							                 </div>
										   </div>
										   <div class="col-sm-3 form-group">
										     <div class="input-group">
							                 	<input type="text" name="building_record_depreciate_year_percent" id="building_record_depreciate_year_percent" class="form-control" placeholder="ปีละ" value="<?php echo $building_record_data['building_record_depreciate_year_percent'] ? $building_record_data['building_record_depreciate_year_percent'] : 0;?>" required>
							                  	<span class="input-group-addon">%</span>
							                 </div>
										   </div>
										   <div class="col-sm-3 form-group">
										     <div class="input-group">
							                 	<input type="text" name="building_record_depreciate_sum_percent" id="building_record_depreciate_sum_percent" class="form-control" placeholder="จะเป็น" value="<?php echo $sum_percent;?>" readonly>
							                  	<span class="input-group-addon">%</span>
							                 </div>
										   </div>
									    </div>
							 </td>
							 <td><div class="form-group"><input type="text" name="depreciate_price" id="depreciate_price" class="form-control" value="<?php echo $depreciate_price;?>" readonly ></div></td>
							 <td><div class="form-group"><input type="text" name="depreciate_cost" id="depreciate_cost" class="form-control" value="<?php echo $depreciate_cost;?>" readonly ></div></td>
						</tr>
						<tr style="background-color: #f9f9f9;">
							<td colspan="4">ราคาประเมินสิ่งปลูกสร้างหลังหักค่าเสื่อม</td>
							<td><div class="form-group"><input type="text" name="sum_all_price2" id="sum_all_price2" class="form-control" value="<?php echo $sum_all_price2;?>" readonly ></div></td>
							<td><div class="form-group"><input type="text" name="sum_all_cost2" id="sum_all_cost2" class="form-control" value="<?php echo $sum_all_cost2;?>" readonly ></div></td>
						</tr>
						<tr style="background-color: #f9f9f9;">
							<td colspan="4">ราคาค่าเสื่อม 30%</td>
							<td><div class="form-group"><input type="text" name="building_record_price1" id="building_record_price1" class="form-control" value="" ></div></td>
							<td><div class="form-group"><input type="text" name="building_record_price2" id="building_record_price2" class="form-control" value="" ></div></td>
						</tr>
						</tbody>
			</table>
<script>
jQuery(document).ready(function () {

	 $("#t_building").on('keyup', '.area', function(){
		    $(this).val($(this).val().replace(/[^0-9\s]/, ''));
			var area  = $(this).parents("tr").find(".area").val();
			var price  = $(this).parents("tr").find(".price").val();
			if(area != '' && price != ''){
				var sum_p = area * price;
				$(this).parents("tr").find(".sum_p").val(sum_p);
				sum_price();
				depreciate();
			}			
	 });

	 $("#t_building").on('keyup', '.price', function(){
		   $(this).val($(this).val().replace(/[^0-9\s]/, ''));
			var area  = $(this).parents("tr").find(".area").val();
			var price  = $(this).parents("tr").find(".price").val();
			if(area != '' && price != ''){
				var sum_p = area * price;
				$(this).parents("tr").find(".sum_p").val(sum_p);
				sum_price();
				depreciate();
			}			
	 });

	 $("#t_building").on('keyup', '.cost', function(){
		 sum_cost();
		 depreciate();
	 });

	 $("#building_record_depreciate_year").on('keyup',function(){
		 $(this).val($(this).val().replace(/[^0-9\s]/, ''));
		 depreciate();
	 });

	 $("#building_record_depreciate_year_percent").on('keyup',function(){
		 $(this).val($(this).val().replace(/[^0-9\s]/, '')); 
		 depreciate();
	 });

	 function sum_price()
	 {
		var sum_all_price = 0;
		$( ".sum_p" ).each(function( index ) {					
			var sum = parseInt($(this).val());
			if(sum > 0){
				sum_all_price += sum;
			}
			$('#sum_all_price').val(sum_all_price);
		});
	 }

	 function sum_cost()
	 {
		var sum_all_cost = 0;
		$( ".cost" ).each(function( index ) {					
			var cost = parseInt($(this).val());
			if(cost > 0){
				sum_all_cost += cost;
			}
			$('#sum_all_cost').val(sum_all_cost);
		});
	 }

	 function depreciate()
	 {
		var year = $('#building_record_depreciate_year').val();
		var percent = $('#building_record_depreciate_year_percent').val();

		if(year != '' && percent != ''){
			var sum_percent = year * percent;
			$('#building_record_depreciate_sum_percent').val(sum_percent);

			var sum_all_price = $('#sum_all_price').val();
			var depreciate_price = (sum_all_price*sum_percent)/100;
			$('#depreciate_price').val(depreciate_price);
			$('#sum_all_price2').val(sum_all_price-depreciate_price);

			var sum_all_cost = $('#sum_all_cost').val();
			var depreciate_cost = (sum_all_cost*sum_percent)/100;
			$('#depreciate_cost').val(depreciate_cost);
			$('#sum_all_cost2').val(sum_all_cost-depreciate_cost);			
		}
	 }
});
</script>