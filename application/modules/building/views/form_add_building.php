<form id="frm1" name="frm1" method="post" action="<?php echo base_url().'building/save_building_record'?>" enctype="multipart/form-data">
		<input type="hidden" name="building_id"  id="building_id" value="<?php echo $building_id;?>"></input>
		<input type="hidden" name="person_id"  id="person_id" value="<?php echo $person_id;?>"></input>
		<input type="hidden" name="building_record_id"  id="building_record_id" value="<?php echo $building_record_data['building_record_id']?>"></input>
<!--  form add -->
	<div class="row">
		<div class="col-sm-12">
			<h4>แบบบันทึกการตรวจสอบประเมินราคาสิ่งปลูกสร้าง</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">ชื่อ-สกุลของผู้ขอสินเชื่อ</label>
	        <input type="text" name="person_name" class="form-control" readonly="readonly" value="<?php echo $person_data['person_fname'].' '.$person_data['person_lname'];?>">
		</div>                                             
	</div><!-- row -->		
	<div class="row">
		<div class="col-sm-12">
			<h5>ที่ตั้งของสิ่งปลูกสร้าง </h5>
		</div>
	</div>
	<div class="row">
	            <div class="col-sm-6 form-group">
	                     <label class="control-label">เป็น</label>
	                     <div class="row">
	                     <?php if(!empty($land_type_list)){	                     		
	                     		foreach ($land_type_list as $val) {
	                     			echo '<div class="col-sm-3">';
	                     			echo '<input type="radio" id="loan_type_id" name="loan_type_id" value="'.$val['land_type_id'].'" '.($land_data['land_type_id'] == $val['land_type_id']?'checked="checked"':'').' disabled="disabled">
		                                  <label>'.$val['land_type_name'].' </label>';
	                     			echo '</div>';
	                     		}	                     		                        
                         }?>
		             	</div>
	             </div>
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">เลขที่</label>
	                     <input type="text" name="land_no" class="form-control" value="<?php echo $land_data['land_no'];?>" disabled="disabled">
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">เนื้อที่</label>
	                     <div class="input-group">
		                 	<input type="text" name="land_area_rai" class="form-control" value="<?php echo $land_data['land_area_rai'].'-'.$land_data['land_area_ngan'].'-'.$land_data['land_area_wah'];?>" disabled="disabled">
		                  	<span class="input-group-addon">ไร่-งาน-ตารางวา</span>
		                 </div>
	             </div>       
	</div><!-- row -->	
	<div class="row">
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">จังหวัด</label>
	                      <select class="form-control" id="land_addr_province_id" name="land_addr_province_id" disabled="disabled">
                           <option value="">กรุณาเลือก</option>
                            <?php if(!empty($province_list)){
	                     		foreach ($province_list as $val) {
	                     			echo '<option value="'.$val['province_id'].'" '.($land_data['land_addr_province_id'] == $val['province_id']?'selected="selected"':'').'>'.$val['province_name'].'</option>';
	                     		}                      
                         	}?>
                         </select>
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">อำเภอ/เขต</label>
	                      <select class="form-control" id="land_addr_amphur_id" name="land_addr_amphur_id" disabled="disabled">
                            <option value="">กรุณาเลือก</option>
                             <?php if(!empty($amphur_list)){
	                     		foreach ($amphur_list as $val) {
	                     			echo '<option value="'.$val['amphur_id'].'" '.($land_data['land_addr_amphur_id'] == $val['amphur_id']?'selected="selected"':'').'>'.$val['amphur_name'].'</option>';
	                     		}                      
                         	}?>
                         </select>
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ตำบล/แขวง </label>
	                     <select class="form-control" id="land_addr_district_id" name="land_addr_district_id" disabled="disabled">	                     
                            <option value="">กรุณาเลือก</option>
                            <?php if(!empty($district_list)){
	                     		foreach ($district_list as $val) {
	                     			echo '<option value="'.$val['district_id'].'" '.($land_data['land_addr_district_id'] == $val['district_id']?'selected="selected"':'').'>'.$val['district_name'].'</option>';
	                     		}                      
                         	}?>
                         </select>
	             </div>                                      
	</div><!-- row -->
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
	    <div class="col-sm-6 form-group">
	         <label class="control-label">เขียนที่ <span class="asterisk">*</span></label>
	         <input type="text" name="building_record_location" id="building_record_location" class="form-control" value="<?php echo $building_record_data['building_record_location'] ? $building_record_data['building_record_location'] : 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)' ?>" required>
	    </div>
	    <div class="col-sm-3 form-group">
	         <label class="control-label">วันที่ <span class="asterisk">*</span></label>
	         <div class="input-group">
	         <?php 
	         if($building_record_data['building_record_date']){
				list($y, $m, $d) = explode('-', $building_record_data['building_record_date']);
				$building_record_data['building_record_date'] = $d.'/'.$m.'/'.(intval($y)+543);	
			}else{
				$building_record_data['building_record_date'] = date('d/m/').(date('Y')+543);
			}
	         ?>
                <input type="text" name="building_record_date" id="building_record_date" class="form-control" value="<?php echo $building_record_data['building_record_date']?>" readonly="readonly" />
				<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
             </div>
	    </div> 
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h5>ผู้ถือกรรมสิทธิ์สิ่งปลูกสร้าง</h5>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">คำนำหน้า <span class="asterisk">*</span></label>
	        <select class="form-control" id="building_owner_title_id" name="building_owner_title_id" required>	                     
            <option value="">คำนำหน้า</option>            
             <?php if(!empty($title_list)){
	              	foreach ($title_list as $val) {
	              	echo '<option value="'.$val['title_id'].'" '.($building_data['building_owner_title_id'] == $val['title_id']?'selected="selected"':'').'>'.$val['title_name'].'</option>';
	         	}                      
             }?>
            </select>
	    </div>
		<div class="col-sm-3 form-group">
	    	<label class="control-label">ชื่อ<span class="asterisk">*</span></label>
	        <input type="text" name="building_owner_fname" id="building_owner_fname" class="form-control" value="<?php echo $building_data['building_owner_fname'];?>" required>
		</div>  
		<div class="col-sm-3 form-group">
	    	<label class="control-label">นามสกุล<span class="asterisk">*</span></label>
	        <input type="text" name="building_owner_lname" id="building_owner_lname" class="form-control" value="<?php echo $building_data['building_owner_lname'];?>" required>
		</div>                                             
	</div><!-- row -->
	<div class="row">
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">สิ่งปลูกสร้างเลขที่ </label>
	                     <input type="text" name="building_no" id="building_no" class="form-control" value="<?php echo $building_data['building_no'];?>" disabled="disabled">
	             </div>
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">หมู่ที่</label>
	                     <input type="text" name="building_moo" id="building_moo" class="form-control" value="<?php echo $building_data['building_moo'];?>" disabled="disabled">
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ตรอก/ซอย</label>
	                     <input type="text" name="building_soi" id="building_soi" class="form-control" value="<?php echo $building_data['building_soi'];?>" disabled="disabled">
	             </div>       
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">ถนน</label>
	                     <input type="text" name="building_road" id="building_road" class="form-control" value="<?php echo $building_data['building_road'];?>" disabled="disabled">
	            </div>
	 </div>
	 <div class="row">
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">จังหวัด</label>
	                      <select name="ddlProvince" id="ddlProvince" data-placeholder="กรุณาเลือก" class="width100p" disabled="disabled">
                           <option value="">กรุณาเลือก</option>
                            <?php if(!empty($province_list)){
	                     		foreach ($province_list as $val) {
	                     			echo '<option value="'.$val['province_id'].'" '.($building_data['building_province_id'] == $val['province_id']?'selected="selected"':'').'>'.$val['province_name'].'</option>';
	                     		}                      
                         	}?>
                         </select>
	             </div>
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">อำเภอ/เขต</label>
	                      <div id="divAmphur">
									<select name="ddlAmphur" id="ddlAmphur" data-placeholder="กรุณาเลือก" class="width100p" disabled="disabled">
										<option value="">กรุณาเลือก</option>
										<?php if(!empty($amphur_list)){
				                     		foreach ($amphur_list as $val) {
				                     			echo '<option value="'.$val['amphur_id'].'" '.($building_data['building_amphur_id'] == $val['amphur_id']?'selected="selected"':'').'>'.$val['amphur_name'].'</option>';
				                     		}                      
			                         	}?>
									</select>
						</div>
	             </div>
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">ตำบล/แขวง </label>
	                     <div id="divDistrict">
									<select name="ddlDistrict" id="ddlDistrict" data-placeholder="กรุณาเลือก" class="width100p" disabled="disabled">
										<option value="">กรุณาเลือก</option>
										<?php if(!empty($district_list)){
				                     		foreach ($district_list as $val) {
				                     			echo '<option value="'.$val['district_id'].'" '.($building_data['building_district_id'] == $val['district_id']?'selected="selected"':'').'>'.$val['district_name'].'</option>';
				                     		}                      
			                         	}?>
									</select>
						</div>
	             </div>                                       
	</div><!-- row -->
	<div class="row">
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ประเภท/ลักษณะสิ่งปลูกสร้าง</label>
	                      <select class="form-control" id="building_type_id" name="building_type_id" disabled="disabled">
                            <option value="">กรุณาเลือก</option>
                             <?php if(!empty($building_type_list)){
	                     		$building_type_id_name = $building_data['building_type_id']===0 ? 'อื่นๆ ('.$building_data['building_type_other'].')' : '';
	                     		foreach ($building_type_list as $val) {	 
	                     			if($building_data['building_type_id'] == $val['building_type_id']){
	                     				$building_type_id_name = $val['building_type_name'];
	                     			}	                     			
	                     			echo '<option value="'.$val['building_type_id'].'" '.($building_data['building_type_id'] == $val['building_type_id']?'selected':'').'>'.$val['building_type_name'].'</option>';
	                     		}                      
                         	}?>
                            <option value="0"<?php echo $building_data['building_type_id']===0?'selected':'';?>>อื่นๆ</option>
                         </select>
                         <div class="form-group" id="dv_building_type_other" <?php echo $building_data['building_type_id']==='0'?'':'style="display:none;"'?>>
                         	<label class="control-label">ระบุ</label>
	                     	<input type="text" name="building_type_other" id="building_type_other" class="form-control" value="<?php echo $building_data['building_type_other'];?>" disabled="disabled">
	                     </div>
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ชั้นบ้าน<span class="asterisk">*</span></label>
	                     <input type="text" name="building_layer" id="building_layer" class="form-control" value="<?php echo $building_data['building_layer'];?>" required>
	             </div>      
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">จำนวน<span class="asterisk">*</span> </label>
	                     <div class="input-group">
		                 	<input type="text" name="building_amount" id="building_amount" class="form-control" value="<?php echo $building_data['building_amount'];?>" required>
		                  	<span class="input-group-addon">อาคาร</span>
		                 </div>
	             </div>                                   
	 </div><!-- row -->
	 <div class="row">
	     <div class="col-sm-12">
	       <h5>ประโยชน์ใช้สอย ภายในสิ่งปลูกสร้าง (ระบุจำนวน)</h5>
	     </div>
	 </div>
	 <div class="row">
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">ห้องนอน<span class="asterisk">*</span> </label>
	                     <input type="text" name="building_bedroom" id="building_bedroom" class="form-control" value="<?php echo $building_data['building_bedroom'];?>" required>
	             </div>
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ห้องน้ำ<span class="asterisk">*</span></label>
	                     <input type="text" name="building_bathroom" id="building_bathroom" class="form-control" value="<?php echo $building_data['building_bathroom'];?>" required>
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ห้องครัว<span class="asterisk">*</span></label>
	                     <input type="text" name="building_kitchen" id="building_kitchen" class="form-control" value="<?php echo $building_data['building_kitchen'];?>" required>
	             </div>       
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">อื่นๆ<span class="asterisk">*</span></label>
	                     <input type="text" name="building_other" id="building_other" class="form-control" value="<?php echo $building_data['building_other'];?>" required>
	             </div>
	  </div>
	  <div class="row">
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">ประเภทการใช้งาน<span class="asterisk">*</span></label>
	                     <input type="text" name="building_usage" id="building_usage" class="form-control" value="<?php echo $building_data['building_usage'];?>" required>
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">พื้นที่ใช้สอยสิ่งปลูกสร้าง<span class="asterisk">*</span></label>
	                     <div class="input-group">
		                 	<input type="text" name="building_area" id="building_area" class="form-control" value="<?php echo $building_data['building_area'];?>" required>
		                  	<span class="input-group-addon">ตารางเมตร</span>
		                 </div>
	             </div>      
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">อายุของสิ่งปลูกสร้าง ก่อสร้างมาประมาณ <span class="asterisk">*</span></label>
	                     <div class="input-group">
		                 	<input type="text" name="building_age" id="building_age" class="form-control" value="<?php echo $building_data['building_age'];?>"required>
		                  	<span class="input-group-addon">ปี</span>
		                 </div>
	             </div>                                   
	 </div><!-- row -->
	 <div class="row">
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">สภาพสิ่งปลูกสร้าง <span class="asterisk">*</span></label>
	                     <select class="form-control" id="building_state" name="building_state" required>
                            <option value="">กรุณาเลือก</option>
                            <option value="1" <?php echo $building_data['building_state']==1?'selected':'';?>>ใหม่ </option>
                            <option value="2" <?php echo $building_data['building_state']==2?'selected':'';?>>ปานกลาง </option>
                            <option value="3" <?php echo $building_data['building_state']==3?'selected':'';?>>เก่า </option>
                            <option value="4" <?php echo $building_data['building_state']==4?'selected':'';?>>มีการตกแต่งบำรุงรักษา </option>
                            <option value="0" <?php echo $building_data['building_state']==0?'selected':'';?>>อื่นๆ </option>
                         </select>	                     
	                     <div class="form-group" id="dv_building_state_other" <?php echo $building_data['building_state']==='0'?'':'style="display:none;"'?>>
                         	<label class="control-label">ระบุ<span class="asterisk">*</span></label>
	                     	<input type="text" name="building_state_other" id="building_state_other" class="form-control" value="<?php echo $building_data['building_state_other'];?>" required>
	                     </div>
	             </div>
	             
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">คุณภาพวัสดุก่อสร้าง<span class="asterisk">*</span></label>
	                     <select class="form-control" id="building_quality" name="building_quality" required>
                            <option value="">กรุณาเลือก</option>
                            <option value="1" <?php echo $building_data['building_quality']==1?'selected':'';?>>ดี </option>
                            <option value="2" <?php echo $building_data['building_quality']==2?'selected':'';?>>ปานกลาง </option>
                            <option value="3" <?php echo $building_data['building_quality']==3?'selected':'';?>>พอใช้ </option>
                         </select>
	             </div> 
	             <div class="col-sm-3 form-group">
	                     <label class="control-label">รูปแบบสิ่งปลูกสร้าง<span class="asterisk">*</span></label>
	                     <select class="form-control" id="building_style" name="building_style" required>
                            <option value="">กรุณาเลือก</option>
                            <option value="1" <?php echo $building_data['building_style']==1?'selected':'';?>>ดี </option>
                            <option value="2" <?php echo $building_data['building_style']==2?'selected':'';?>>ปานกลาง </option>
                            <option value="3" <?php echo $building_data['building_style']==3?'selected':'';?>>พอใช้ </option>
                         </select>
	             </div>       
	            <div class="col-sm-3 form-group">
	                     <label class="control-label">ฝีมือการก่อสร้าง<span class="asterisk">*</span></label>
	                     <select class="form-control" id="building_skill" name="building_skill" required>
                           <option value="">กรุณาเลือก</option>
                            <option value="1" <?php echo $building_data['building_skill']==1?'selected':'';?>>ดี </option>
                            <option value="2" <?php echo $building_data['building_skill']==2?'selected':'';?>>ปานกลาง </option>
                            <option value="3" <?php echo $building_data['building_skill']==3?'selected':'';?>>พอใช้ </option>
                         </select>
	             </div>
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h4>รายงานประกอบแบบก่อสร้างพร้อมแบบแปลน <span class="asterisk">*</span></h4>
			<h5>ส่วนต่างๆ ของสิ่งปลูกสร้าง</h5>
		</div>
	</div>
	<?php 				
				if(!empty($building_part_data)){					
					foreach ($building_part_data as $part_id => $list) {
							echo '<div class="row">
											<div class="col-sm-2 form-group">
							       				<label class="control-label">'.$list['building_part_desc'].'</label>
						          			</div>
											<div class="col-sm-10 form-group">
					       						<div class="row">';
							foreach ($list['sub_list'] as $part_sub_id => $desc) {
								$list = explode('|',$building_data['building_part']);
								$check = in_array($part_sub_id,$list)==true ? 'checked' : '';
								echo '<div class="col-sm-2">
													<input type="checkbox" value="'.$part_sub_id.'" name="building_part_sub_id['.$part_sub_id.']" id="buildingPartSubId_'.$part_id.'_'.$part_sub_id.'" '.$check.' >
													<label>'.$desc.'</label>
									 			</div>';
							}					       						
					        echo '		</div>
					       					</div>
					       				</div>';	
					}
				}
				?>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 form-group">
			<label>ข้อมูลเพิ่มเติม<span class="asterisk">*</span></label>
			<textarea id="building_desc" name="building_desc" class="form-control" rows="3" required><?php echo $building_data['building_desc']?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h4>รูปแผนผังสิ่งปลูกสร้างโดยสังเขป &nbsp;<button id="btnAddImages" name="btnAddImages" type="button" class="btn btn-default btn-xs">+</button></h4>
		</div>
	</div>
	<div class="row" id="t_building_images">
	</div>	
	<div class="row" id="dv_building_images">
	      <div class="col-md-12 form-group"> 
	       			<input type="file" name="building_images[]" class="form-control">
	      </div>
	</div>	
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h4>การประเมินราคาสิ่งปลูกสร้าง</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<div class="table-responsive">			
			<?php 			
			$building_type_id = $building_data['building_type_id'];	
			
			if (!empty($building_assess_data)){
				
				$building_assess_id = $building_assess_data[0]['building_assess_id'];
				$building_assess_type_id = $building_assess_data[0]['building_type_id'];
				
				if($building_assess_type_id != $building_type_id){
					
					$building_assess_area = $building_data['building_area'] ? $building_data['building_area'] : 0;
					$building_assess_price_per_area =  0;
					$building_assess_price = $building_assess_area*$building_assess_price_per_area;
					$building_assess_cost = 0;
					
					$sum_all_price = $building_assess_price;
					$sum_all_cost = $building_assess_cost;
								
					$building_record_depreciate_year = $building_data['building_age'] ? $building_data['building_age'] : 0;						
					$building_record_depreciate_sum_percent = 0;
					
					$depreciate_price = 0;
					$depreciate_cost = 0;
					
					$sum_all_price2 = $sum_all_price-$depreciate_price;
					$sum_all_cost2 = $sum_all_cost-$depreciate_cost;
					
					$building_record_price1 = ($sum_all_price2*30)/100;
					$building_record_price2 = ($sum_all_cost2*30)/100;	
					
				}else{
					
					$building_assess_area = $building_assess_data[0]['building_assess_area'];
					$building_assess_price_per_area =  $building_assess_data[0]['building_assess_price_per_area'];
					$building_assess_price = $building_assess_data[0]['building_assess_price'];
					$building_assess_cost = $building_assess_data[0]['building_assess_cost'];
					
					$sum_all_price = $building_assess_price;
					$sum_all_cost = $building_assess_cost;
								
					$building_record_depreciate_year = @$building_record_data['building_record_depreciate_year'] ? $building_record_data['building_record_depreciate_year'] : $building_data['building_area'];						
					$building_record_depreciate_sum_percent = @$building_record_data['building_record_depreciate_sum_percent'];
					
					$depreciate_price = ($sum_all_price*$building_record_depreciate_sum_percent)/100;
					$depreciate_cost = ($sum_all_cost*$building_record_depreciate_sum_percent)/100;
					
					$sum_all_price2 = @$building_record_data['sum_all_price2'];//$sum_all_price-$depreciate_price;
					$sum_all_cost2 = $sum_all_cost-$depreciate_cost;
					
					$building_record_price1 = @$building_record_data['building_record_price1'];
					$building_record_price2 = @$building_record_data['building_record_price2'];
				}
				
			}else{
				$building_assess_id = '';
				$building_assess_area = $building_data['building_area'] ? $building_data['building_area'] : 0;
				$building_assess_price_per_area = 0;
				$building_assess_price = $building_assess_area*$building_assess_price_per_area;
				$building_assess_cost = 0;
				
				$sum_all_price = $building_assess_price;
				$sum_all_cost = $building_assess_cost;
				
				$building_record_depreciate_year = $building_data['building_age'] ? $building_data['building_age'] : 0;	
				$building_record_depreciate_sum_percent = 0;
				
				$depreciate_price = 0;
				$depreciate_cost = 0;
				
				$sum_all_price2 = $sum_all_price-$depreciate_price;
				$sum_all_cost2 = $sum_all_cost-$depreciate_cost;
				
				$building_record_price1 = ($sum_all_price2*30)/100;
				$building_record_price2 = ($sum_all_cost2*30)/100;	
				
			}			
			?>
			<input type="hidden" name="building_assess_id" value="<?php echo $building_assess_id;?>"></input>
			<input type="hidden" name="building_assess_type_id" value="<?php echo $building_type_id;?>"></input>
			<table class="table table-bordered">
				<thead>
				<tr>
					<th>ลำดับ</th>
					<th>ประเภทสิ่งปลูกสร้าง</th>
					<th>เนื้อที่ (ตร.ม.)</th>
					<th>ราคาต่อ ตร.ม.</th>
					<th>ราคาประเมิน</th>
					<th>ราคาค่าก่อสร้างตาม แบบแปลน/สัญญา*</th>
				</tr>
				</thead>
				<tbody >			
				<tr>
					<td align="center">1</td>
					<td><?php echo $building_type_id_name;?></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="building_assess_area" id="building_assess_area" class="form-control" value="<?php echo $building_assess_area;?>" readonly ></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="building_assess_price_per_area" id="building_assess_price_per_area" class="form-control right" value="<?php echo number_format($building_assess_price_per_area,2);?>" required ></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="building_assess_price" id="building_assess_price" class="form-control right" value="<?php echo number_format($building_assess_price,2);?>" readonly ></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="building_assess_cost" id="building_assess_cost" class="form-control right" value="<?php echo number_format($building_assess_cost,2);?>" required ></div></td>
				</tr>
				<tr style="background-color: #f9f9f9;">
					<td colspan="4">รวมราคาประเมินสิ่งปลูกสร้างก่อนหักค่าเสื่อม</td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="sum_all_price" id="sum_all_price" class="form-control right" value="<?php echo number_format($sum_all_price,2)?>" readonly ></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="sum_all_cost" id="sum_all_cost" class="form-control right" value="<?php echo number_format($sum_all_cost,2)?>" readonly ></div></td>
				</tr>
				<tr style="background-color: #f9f9f9;">
					<td colspan="4">
						<div class="row">
							<div class="col-sm-3 form-group" style="margin-bottom: 0px !important;">
								<label class="control-label">ค่าเสื่อม<span class="asterisk">*</span></label>
							</div> 
							<div class="col-sm-3 form-group" style="margin-bottom: 0px !important;">
							    <select class="form-control" id="building_record_cal_type" name="building_record_cal_type" required>
						        	<option value="">กรุณาเลือก</option>
						         	<option value="1" <?php echo @$building_record_data['building_record_cal_type']===1?'selected':'';?>>ประเภทตึก </option>
						        	<option value="2" <?php echo @$building_record_data['building_record_cal_type']===2?'selected':'';?>>ประเภทครึ่งไม้ครึ่งตึก </option>
						         	<option value="3" <?php echo @$building_record_data['building_record_cal_type']===3?'selected':'';?>>ประเภทไม้ </option>
						   		</select>
							</div> 
							<div class="col-sm-3 form-group" style="margin-bottom: 0px !important;">
								<div class="input-group">
							    	<input type="text" name="building_record_depreciate_year" id="building_record_depreciate_year" class="form-control" placeholder="หักค่าเสื่อมราคา" value="<?php echo $building_record_depreciate_year?>" readonly>
							     	<span class="input-group-addon">ปี</span>
							    </div>
							</div>
							<div class="col-sm-3 form-group" style="margin-bottom: 0px !important;">
								<div class="input-group">
							    	<input type="text" name="building_record_depreciate_sum_percent" id="building_record_depreciate_sum_percent" class="form-control" placeholder="จะเป็น" value="<?php echo $building_record_depreciate_sum_percent?>" readonly>
							     	<span class="input-group-addon">%</span>
								</div>
							</div>
						</div>
					</td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="depreciate_price" id="depreciate_price" class="form-control right" value="<?php echo number_format($depreciate_price,2)?>" readonly ></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="depreciate_cost" id="depreciate_cost" class="form-control right" value="<?php echo number_format($depreciate_cost,2)?>" readonly ></div></td>
				</tr>
				<tr style="background-color: #f9f9f9;">
					<td colspan="4">ราคาประเมินสิ่งปลูกสร้างหลังหักค่าเสื่อม</td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="sum_all_price2" id="sum_all_price2" class="form-control right" value="<?php echo number_format($sum_all_price2,2)?>" readonly ></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="sum_all_cost2" id="sum_all_cost2" class="form-control right" value="<?php echo number_format($sum_all_cost2,2)?>" readonly ></div></td>
				</tr>
				<tr style="background-color: #f9f9f9;">
					<td colspan="4">ราคาค่าเสื่อม 30%</td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="building_record_price1" id="building_record_price1" class="form-control right" value="<?php echo number_format($building_record_price1,2)?>" required></div></td>
					<td><div class="form-group" style="margin-bottom: 0px !important;"><input type="text" name="building_record_price2" id="building_record_price2" class="form-control right" value="<?php echo number_format($building_record_price2,2)?>" required></div></td>
				</tr>
				</tbody>
			</table>
		</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h4>สรุปผลการตรวจสอบประเมินราคาของผู้ตรวจสอบสิ่งปลูกสร้าง</h4>
			<h5>จากการวิเคราะห์และประเมินราคาสิ่งปลูกสร้าง รวมทั้งการพิจารณาข้อมูล และรายละเอียดต่างๆ ดังกล่าวมาข้างต้น ควรประเมินมูลค่าสิ่งปลูกสร้างสิ่งปลูกสร้าง</h5>
		</div>
	</div>
	<div class="row">
	    <div class="col-sm-3 form-group">
	         <label class="control-label">เป็นเงินจำนวน <span class="asterisk">*</span></label>
	         <div class="input-group">
				<input type="text" name="building_record_inspector_assess_amount" id="building_record_inspector_assess_amount" class="form-control right" value="<?php echo @number_format($building_record_data['building_record_inspector_assess_amount'],2)?>" required>
				<span class="input-group-addon">บาท</span>
			</div>
	    </div>
	</div>
	<div class="row">
		<div class="col-sm-12 form-group">
			<label>ความเห็นของพนักงานผู้ตรวจสอบสิ่งปลูกสร้าง<span class="asterisk">*</span></label>
			<textarea name="building_record_inspector_assess_reason" id="building_record_inspector_assess_reason" class="form-control" rows="3" required><?php echo @$building_record_data['building_record_inspector_assess_reason']?></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h4>บันทึกของผู้ได้รับมอบอำนาจ</h4>
			<h5>ให้จำนองสิ่งปลูกสร้างดังกล่าวข้างต้น</h5>
		</div>
	</div>
	<div class="row">
	    <div class="col-sm-3 form-group">
	         <label class="control-label">ภายในวงเงิน<span class="asterisk">*</span></label>
	         <div class="input-group">
				<input type="text" name="building_record_staff_assess_amount" id="building_record_staff_assess_amount" class="form-control right" value="<?php echo @number_format($building_record_data['building_record_staff_assess_amount'],2)?>" required>
				<span class="input-group-addon">บาท</span>
			</div>
	    </div>
	</div>
	<div class="row">
		<div class="col-sm-12 form-group">
			<label>ความเห็นเพิ่มเติม<span class="asterisk">*</span></label>
			<textarea name="building_record_staff_assess_reason" id="building_record_staff_assess_reason" class="form-control" rows="3" required><?php echo @$building_record_data['building_record_staff_assess_reason']?></textarea>
		</div>
	</div>
</form>
<script>
jQuery(document).ready(function () {
	
    $("#building_record_date").datepicker({ 
    	dateFormat: "dd/mm/yy", 
    	isBuddhist: true,
    	dayNames: ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"],
        dayNamesMin: ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
        monthNames: ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],
        monthNamesShort: ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
        changeMonth: true,  
        changeYear: true ,  
        beforeShow:function(){  
            if($(this).val()!="")
            {  
                var arrayDate = $(this).val().split("/");       
                arrayDate[2] = parseInt(arrayDate[2])-543;  
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
            }  
            setTimeout(function(){  
                $.each($(".ui-datepicker-year option"),function(j,k){  
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                    $(".ui-datepicker-year option").eq(j).text(textYear);  
                });               
            },50);
        },  
        onChangeMonthYear: function(){  
            setTimeout(function(){  
                $.each($(".ui-datepicker-year option"),function(j,k){  
                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
                    $(".ui-datepicker-year option").eq(j).text(textYear);  
                });               
            },50);        
        },  
        onClose:function(){  
            if($(this).val()!="" && $(this).val()==dateBefore){           
                var arrayDate = dateBefore.split("/");  
                arrayDate[2] = parseInt(arrayDate[2])+543;  
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);      
            }         
        },  
        onSelect: function(dateText, inst){   
            dateBefore = $(this).val();  
            var arrayDate = dateText.split("/");  
            arrayDate[2] = parseInt(arrayDate[2])+543;  
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
        }  
    });

    $("#iconDate").click(function(){ 
    	$("#building_record_date").datepicker("show"); 
    }); 

    $.validator.addMethod('Decimal', function(value, element) {
        return this.optional(element) || /^[0-9,]+(\.\d{0,2})?$/.test(value); 
    }, "You must include two decimal places");

    $("#frm1").validate({
		rules: {
			building_layer: {
				min:0,
				digits: true,
				required: true
			},
			building_amount: {
				min:0,
				digits: true,
				required: true
			},
			building_bedroom: {
				min:0,
				digits: true,
				required: true
			},
			building_bathroom: {
				min:0,
				digits: true,
				required: true
			},
			building_kitchen: {
				min:0,
				digits: true,
				required: true
			},
			building_age: {
				min:0,
				digits: true,
				required: true
			},
			building_area: {
				number:true,
				Decimal: true,
				required: true
			},
			 "building_images[]": { 
				accept: "image/jpg,image/jpeg,image/png",
	            required: false
	        },
	        "building_part_sub_id[]":{
	        	required: true
	        },
	        building_record_inspector_assess_amount: {
				number:true,
				Decimal: true,
				required: true
			},
			building_record_staff_assess_amount: {
				number:true,
				Decimal: true,
				required: true
			},
			building_record_price1: {
				number:true,
				Decimal: true,
				required: true
			},
			building_record_price2: {
				number:true,
				Decimal: true,
				required: true
			},
			building_assess_price_per_area: {
				number:true,
				Decimal: true,
				required: true
			},
			building_assess_cost: {
				number:true,
				Decimal: true,
				required: true
			},
		},
		highlight: function(element) {
			$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
		},
		unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
		errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },
		success: function(element) {
			$(element).closest(".form-group").removeClass("has-error");
		}		
	});

    jQuery('#btnSubmit').click(function (e) {
    	$('#frm1').submit();
    });

    jQuery('#t_building_images').load("<?php echo base_url()?>building/gen_building_image/<?php echo $building_id?>");
    
    jQuery('#building_type_id').live('change',function(){
    	if($(this).val() == 0 && $(this).val() != ''){
    		$('#dv_building_type_other').show();
    	}else{
    		$('#dv_building_type_other').hide();
    	}
    });
    
    jQuery('#building_state').live('change',function(){
    	if($(this).val() == 0 && $(this).val() != ''){
    		$('#dv_building_state_other').show();
    	}else{
    		$('#dv_building_state_other').hide();
    	}
    });
    
    jQuery('#btnAddImages').live('click',function (e) {
    	$('#dv_building_images').append('<div class="col-md-12 form-group"><input type="file" name="building_images[]" class="form-control"></div>');
    });  

    $("#ddlProvince").select2({
    	minimumResultsForSearch: -1
    });

    $("#ddlAmphur").select2({
    	minimumResultsForSearch: -1
    });

    $("#ddlDistrict").select2({
    	minimumResultsForSearch: -1
    });

    jQuery('#building_area').live('keyup',function(){
    	$('#building_assess_area').val($(this).val());
    	sum_price();
    	sum_cost();
    	depreciate();
    });
    
    jQuery('#building_age').live('keyup',function(){
    	$('#building_record_depreciate_year').val($(this).val());
    	sum_price();
    	sum_cost();
    	depreciate();
    });

    jQuery('#building_assess_price_per_area').live('keyup',function(){
    	sum_price();
    	sum_cost();
    	depreciate();		
    });

	jQuery('#building_assess_cost').live('keyup',function(){
		sum_price();
    	sum_cost();
    	depreciate();
    });

	jQuery('#building_record_cal_type').live('change',function(){
		sum_price();
    	sum_cost();
    	depreciate();
    });

	function sum_price()
	 {		
		var building_assess_area = removeCommas($('#building_assess_area').val());
		var building_assess_price_per_area = removeCommas($('#building_assess_price_per_area').val());
		
		var building_assess_price = (building_assess_area*building_assess_price_per_area).toFixed(2);
		var sum_all_price = building_assess_price;			

		$('#building_assess_price').val(addCommas(building_assess_price));		
		$('#sum_all_price').val(addCommas(sum_all_price));	
	 }

	 function sum_cost()
	 {			
		    var building_assess_cost = removeCommas($('#building_assess_cost').val());
			var sum_all_cost = (building_assess_cost-0).toFixed(2);

			$('#sum_all_cost').val(addCommas(sum_all_cost));
	 }

	 function depreciate()
	 {
		 var type = $('#building_record_cal_type').val();
		 var year = $('#building_record_depreciate_year').val();

		 percent = 0;
		 if(type == 1){
			 if(year <= 10){
				 percent = year;
			 }else if(year >= 11 && year <= 42){
				 year = year - 10;
				 percent = 10 + (year * 2);
			 }else if(year >= 43){
				 percent = 76;
			 }
		 }else if(type == 2){
			 if(year <= 5){
				 percent = year * 2;
			 }else if(year >= 6 && year <= 15){
				 year = year - 5;
				 percent = (5 * 2) + (year * 4);
			 }else if(year >= 16 && year <= 21){
				 year = year - 15;
				 percent = (5 * 2) + (10 * 4) + (year * 5);
			 }else if(year >= 22){
				 percent = 85;
			 }
		 }else if(type == 3){
			 if(year <= 5){
				 percent = year * 3;
			 }else if(year >= 6 && year <= 15){
				 year = year - 5;
				 percent = (5 * 3) + (year * 5);
			 }else if(year >= 16 && year <= 18){
				 year = year - 15;
				 percent = (5 * 3) + (10 * 5) + (year * 7);
			 }else if(year >= 19){
				 percent = 93;
			 }
		 }
		$('#building_record_depreciate_sum_percent').val(percent);

		var sum_all_price = removeCommas($('#sum_all_price').val());
		var depreciate_price = ((sum_all_price*percent)/100).toFixed(2);	
		var sum_all_price2 = (sum_all_price - depreciate_price).toFixed(2);	
		var building_record_price1 = ((sum_all_price2*30)/100).toFixed(2);	

		$('#depreciate_price').val(addCommas(depreciate_price));
		$('#sum_all_price2').val(addCommas(sum_all_price2));
		$('#building_record_price1').val(addCommas(building_record_price1));

		var sum_all_cost= removeCommas($('#sum_all_cost').val());
		var depreciate_cost = ((sum_all_cost*percent)/100).toFixed(2);
		var sum_all_cost2 = (sum_all_cost - depreciate_cost).toFixed(2);
		var building_record_price2 = ((sum_all_cost2*30)/100).toFixed(2);	
		
		$('#depreciate_cost').val(addCommas(depreciate_cost));
		$('#sum_all_cost2').val(addCommas(sum_all_cost2));
		$('#building_record_price2').val(addCommas(building_record_price2));
		
	 }

	 function addCommas(nStr)
	 {
	 	nStr += "";
	 	x = nStr.split(".");
	 	x1 = x[0];
	 	x2 = x.length > 1 ? "." + x[1] : "";
	 	
	 	var rgx = /(\d+)(\d{3})/;
	 	
	 	while (rgx.test(x1))
	 	{
	 		x1 = x1.replace(rgx, "$1" + "," + "$2");
	 	}
	 	return x1 + x2;
	 }

	 function removeCommas(nStr)
	 {
		var str = nStr.replace(/,/g , "");
		return str;
	 }
    
});
</script>