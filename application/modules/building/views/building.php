<p class="nomargin" style="margin-bottom:20px; text-align:right;">
	<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo base_url()?>building/form'">เพิ่มแบบบันทึก</button>
</p>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-btns">
			<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
		</div><!-- panel-btns -->
		<h4 class="panel-title">ค้นหาแบบบันทึกการตรวจสอบประเมินราคาสิ่งปลูกสร้าง</h4>
	</div>
	<div class="panel-body">
		<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo base_url()?>building">
			<div class="form-group">
				<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
				<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="<?php echo $thaiid; ?>" />
			</div><!-- form-group -->
			<div class="form-group">
				<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
				<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
			</div><!-- form-group -->
			<button type="submit" class="btn btn-default btn-sm">ค้นหา</button>
		</form>
	</div><!-- panel-body -->
</div><!-- panel -->
<div class="table-responsive">
	<table class="table table-striped mb30">
		<thead>
			<tr>
            	<th>#</th>
            	<th>เลขที่ขอสินเชื่อ</th>
                <th>ชื่อ-สกุลผู้ขอสินเชื่อ</th>
                <th>เลขที่โฉนด</th>
                <th>ชื่อ-สกุลผู้ถือกรรมสิทธิ์สิ่งปลูกสร้าง</th>
                <th>ประเภทสิ่งปลูกสร้าง</th>
                <th width="30%">ที่ตั้งสิ่งปลูกสร้าง</th>
                <th>จัดการ</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if(!empty($result))
			{
				$i = 1;
				foreach ($result as $row)
				{
					if($row['building_record_status'] == '1'){
						$row['building_record_status'] = 'แบบร่าง';
						$edit_menu = '<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="window.location = \''.base_url().'building/form/'.$row['building_id'].'/'.$row['person_id'].'\'"><span class="fa fa-edit"></span></button>';
					}else{
						$row['building_record_status'] = 'บันทึกเรียบร้อย';
						$edit_menu = '';
					}
		?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $row['loan_code']; ?></td>
				<td><?php echo $row['person_fname'].' '.$row['person_lname']; ?></td>
				<td><?php echo $row['land_no']; ?></td>
				<td><?php echo $row['building_owner_fname'].' '.$row['building_owner_lname']; ?></td>
				<td><?php echo $row['building_type_name']; ?></td>
           		<td><?php echo show_address('building',$row); ?></td>
           		<td>
		          <button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="พิมพ์" data-original-title="พิมพ์" onclick="window.open('<?php echo site_url('building/word/'.$row['building_id'].'/'.$row['person_id']); ?>', '_blank')"><span class="fa fa-print"></span></button>
		          <?php echo $edit_menu;?>	
		        </td>
           	</tr>
		<?php
				}	
			} 
		?>
		</tbody>
	</table>
</div><!-- table-responsive -->