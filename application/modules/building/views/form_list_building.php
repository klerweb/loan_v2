<!-- form search -->
	<div class="row">
		<div class="col-sm-12">
			<h4>สิ่งปลูกสร้าง</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
             <div class="table-responsive">
                 <table class="table mb30">
                 <thead>
                    <tr>
                    	<th></th>
                    	<th>#</th>
                    	<th>ชื่อ-สกุลผู้ขอสินเชื่อ</th>
                    	<th>เลขที่โฉนด</th>
                    	<th>ประเภทสิ่งปลูกสร้าง</th>
                     	<th>ที่ตั้งสิ่งปลูกสร้าง</th>                     	
                    </tr>
                  </thead>
                  <tbody>
                  	<?php 
							$i = 1;
							foreach ($result as $row)
							{
					?>
						<tr>
							<td class="table-action">
								<input type="radio" value="<?php echo $row['building_id'].'|'.$row['person_id']?>" name="rdRecord">
							</td>
							<td><?php echo $i++; ?></td>
							<td><?php echo $row['person_fname'].' '.$row['person_lname']; ?></td>
							<td><?php echo $row['land_no']; ?></td>
							<td><?php echo $row['building_type_name']; ?></td>
			           		<td><?php echo show_address('building',$row); ?></td>
			           	</tr>
					<?php
							}	
					?>
                   </tbody>
                   </table>
            </div><!-- table-responsive -->
    	</div>
    </div>
