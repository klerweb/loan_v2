<?php if ($building_image_data){ ?>
	      <div class="col-md-12 form-group"> 
	       	
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ไฟล์เอกสาร</th>                            
                            <th>รายละเอียด / ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($building_image_data as $key => $row): ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $row['building_image_filename']; ?></td>                                
                                <td>
                                    <a href="#" class=""><span class="glyphicon glyphicon-zoom-in"></span></a>
                                    &nbsp;&nbsp;
                                    <a href="javascript:void(0)" data="<?php echo $row['building_id'].'_'.$row['building_image_id']; ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach;
                        ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->
	      </div>
	      <?php } ?>	      
<script>
jQuery(document).ready(function () {
	$('.btn_confirm_del').click(function () {
	    var c = confirm("ยืนยันลบรายการ ?");
	    if(c){
		    var r = $(this).attr('data').split('_');
		    var building_id  = r[0];
	    	var building_image_id  = r[1];
	    	$.ajax({type: "POST",
	            url: "<?php echo base_url()?>building/gen_building_image/"+building_id+"/"+building_image_id,
	            success:function(result){
	    			$('#t_building_images').html(result);
			     }
	   	 	});
	    }
	});
});
</script>