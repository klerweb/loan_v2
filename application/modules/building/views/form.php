<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">เพิ่มแบบบันทึกการตรวจสอบประเมินราคาสิ่งปลูกสร้าง</h4>
	</div>
	<div class="panel-body">
	<?php if ($status == '200'){ ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message; ?>
            </div>
    <?php } ?>
	
	<!-- form search -->
	<div class="row">
		<div class="col-sm-12">
			<h4>ค้นหาสิ่งปลูกสร้าง</h4>
		</div>
	</div>
	<div class="row">
			<div class="col-sm-3 form-group">
				<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
				<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="" />
			</div><!-- form-group -->
			<div class="col-sm-3 form-group">
				<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
				<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="" />
			</div><!-- form-group -->
			<button type="button" id="btnSearch" class="btn btn-default btn-sm">ค้นหา</button>
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
    
    <div id="form_building">
	</div>  
	
	</div><!-- panel-body -->
	<div class="panel-footer right">
         <button id="btnSubmit" style="display:none" class="btn btn-primary">บันทึก</button>
	</div><!-- panel-footer -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>

<script>
jQuery(document).ready(function () {
	jQuery('#btnSearch').click(function (e) {
    	$('.alert').hide();
    	$('#btnSubmit').hide();
    	//reset form
    	$.ajax({type: "POST",
            url: "<?php echo base_url()?>building/search_building",
            data: 'txtThaiid='+$('#txtThaiid').val()+'&txtFullname='+$('#txtFullname').val(),            
            success:function(result){
		       $("#form_building").html(result);
		     }
   	 	});
    	
    });
	jQuery('input[name="rdRecord"]').live('change', function() {
	   	 var radioValue = $('input[name="rdRecord"]:checked').val();  
	   	 var r = radioValue.split('|');
	    	 var building_id  = r[0];
	    	 var person_id  = r[1];   	    	
	     	 $.ajax({type: "POST",
	           url: "<?php echo base_url()?>building/form_building/"+building_id+"/"+person_id,
	           success:function(result){
			       $("#form_building").html(result);
			       $('#btnSubmit').show();
			     }
	  	 	});
	     	
	   });
	   
	var building_id = '<?php echo $building_id?>';
	if(building_id){
		$.ajax({type: "POST",
	           url: "<?php echo base_url()?>building/form_building/<?php echo $building_id?>/<?php echo $person_id?>",
	           success:function(result){
			       $("#form_building").html(result);
			       $('#btnSubmit').show();
			     }
	  	 	});
	}
});
</script>
