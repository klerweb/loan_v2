<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building extends MY_Controller
{
	private $title = 'รายการแบบบันทึกการตรวจสอบประเมินราคาสิ่งปลูกสร้าง';
	private $message;

	function __construct()
	{
		parent::__construct();

		$this->load->model('building_model');
		$this->load->model('loan/mst_model');
		$this->message = array('200'=> 'บันทึกข้อมูลเรียบร้อย');
	}

	public function index()
	{
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');

		$data_content = array(
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->building_model->search_building_record($thaiid, $fullname)
		);

		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('building', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	public function form($building_id=NULL,$person_id=NULL,$status=NULL)
	{
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title => site_url('building')
			);

		$data_content = array(
			'status'=>$status,
			'message'=>!empty($this->message[$status]) ? $this->message[$status] : '-',
			'building_id' => $building_id > 0 ? $building_id : '',
			'person_id' => $person_id > 0 ? $person_id :''
		);

		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	public function search_building()
	{
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_content = array(
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->building_model->search_building($thaiid, $fullname)
		);
		$this->parser->parse('form_list_building', $data_content);
	}

	public function form_building($building_id,$person_id)
	{
		$building_data = $this->building_model->get_table_by_id('building',$building_id);
		$building_record_data = $this->building_model->get_table_by_wid('building_record','building_id',$building_id);

		$data_content = array(
				'building_id' => $building_id,
				'person_id' => $person_id,
				'person_data' => $this->building_model->get_table_by_id('person',$person_id),
		        'land_data' => $this->building_model->get_table_by_id('land',$building_data['land_id']),
				'land_type_list' => $this->mst_model->get_all('land_type'),
				'title_list' => $this->mst_model->get_all('title'),
				'district_list' => $this->mst_model->get_table_location_all('district'),
				'amphur_list' => $this->mst_model->get_table_location_all('amphur'),
				'province_list' => $this->mst_model->get_table_location_all('province'),
				'building_type_list' => $this->mst_model->get_all('building_type'),
				'building_data' => $building_data,
				'building_record_data' => $building_record_data[0],
				'building_part_data' => $this->gen_building_part($building_id),
				'building_assess_data' => $this->building_model->get_building_assess($building_id)
		);

		$this->parser->parse('form_add_building', $data_content);
	}

	public function gen_building_part($building_id)
	{
		$list = array();
		$building_part = $this->building_model->get_building_part();
		if(!empty($building_part)){
			foreach ($building_part as $key => $part) {
				//$check  = $this->building_model->check_building_part($building_id,$part['building_part_id'],$part['building_part_sub_id']);
				$list[$part['building_part_id']]['building_part_desc'] = $part['building_part_desc'];
				$list[$part['building_part_id']]['sub_list'][$part['building_part_sub_id']] = $part['building_part_sub_desc'];
			}
		}
		return $list;
	}

	public function gen_building_image($building_id,$building_image_id=null)
	{
		if($building_image_id){
			$building_image = $this->building_model->get_table_by_id('building_image',$building_image_id);
			unlink($building_image['building_image_filename']);
			$this->building_model->delete('building_image',$building_image_id);
		}
		$data_content['building_image_data'] = $this->building_model->get_table_by_wid('building_image','building_id',$building_id);
		$this->parser->parse('form_image_building', $data_content);
	}

	public function save_building_record()
	{
		if(!empty($_POST)){
			$building_data = array(
				'building_owner_title_id'=> $_POST['building_owner_title_id'],
				'building_owner_fname' => $_POST['building_owner_fname'],
				'building_owner_lname'=> $_POST['building_owner_lname'],
				//'building_no'=> $_POST['building_no'],
				//'building_moo'=> $_POST['building_moo'],
				//'building_soi'=> $_POST['building_soi'],
				//'building_road'=> $_POST['building_road'],
				//'building_district_id'=> $_POST['ddlDistrict'],
				//'building_amphur_id'=> $_POST['ddlAmphur'],
				//'building_province_id'=> $_POST['ddlProvince'],
				//'building_type_id'=> $_POST['building_type_id'],
				//'building_type_other'=> $_POST['building_type_other'],
				'building_layer'=> $_POST['building_layer'],
				'building_amount'=> $_POST['building_amount'],
				'building_bedroom'=> $_POST['building_bedroom'],
				'building_bathroom'=> $_POST['building_bathroom'],
				'building_kitchen'=> $_POST['building_kitchen'],
				'building_other'=> $_POST['building_other'],
				'building_usage'=> $_POST['building_usage'],
				'building_area'=> $_POST['building_area'],
				'building_age'=> $_POST['building_age'],
				'building_state'=> $_POST['building_state'],
				'building_state_other'=> $_POST['building_state_other'],
				'building_quality'=> $_POST['building_quality'],
				'building_style'=> $_POST['building_style'],
				'building_skill'=> $_POST['building_skill'],
				'building_desc'=> $_POST['building_desc'],
				'building_part'=> implode('|',$_POST['building_part_sub_id']),
				'building_status'=> '2'
			);
			$this->building_model->save('building',$building_data,$_POST['building_id']);

			if(!empty($_FILES['building_images'])){
				foreach ($_FILES['building_images']['tmp_name'] as $key => $file) {
					if($_FILES['building_images']['size'][$key] > 0){
						$extension_file = explode('.', $_FILES['building_images']['name'][$key]);
		                $file_name = $this->config->item('uploads_building_path').date('YmdHis') . rand(00000,99999) . '.' . $extension_file[1];
		                $file_tmp = $_FILES['building_images']['tmp_name'][$key];
		                if(move_uploaded_file($file_tmp,$file_name)){
		                	$building_image_data = array(
								'building_id'=> $_POST['building_id'],
								'building_image_filename' => $file_name,
								'building_image_status'=> '1',
							 );
			                 $this->building_model->save('building_image',$building_image_data);
		                }
					}
				}
			}

		   if(!empty($_POST['building_assess_type_id']) || !empty($_POST['building_assess_id']) ){
		   		$building_assess_id = @$_POST['building_assess_id'];
				$building_assess_data = array(
					'building_id' => $_POST['building_id'],
					'building_type_id' => $_POST['building_assess_type_id'],
					'building_assess_area' => $_POST['building_assess_area'],
					'building_assess_price_per_area' => str_replace(',','',$_POST['building_assess_price_per_area']),
					'building_assess_price' => str_replace(',','',$_POST['building_assess_price']),
					'building_assess_cost' => str_replace(',','',$_POST['building_assess_cost']),
					'building_assess_status'=> '1',
					);
				$this->building_model->save('building_assess',$building_assess_data,$building_assess_id);
			}

			list($d, $m, $y) = explode('/', $_POST['building_record_date']);
			$_POST['building_record_date'] = (intval($y)-543).'-'.$m.'-'.$d;

			$building_record_data = array(
				'building_id'=> $_POST['building_id'],
				'building_record_location' => $_POST['building_record_location'],
				'building_record_date'=> $_POST['building_record_date'],
				'building_record_depreciate_year'=> $_POST['building_record_depreciate_year'],
				//'building_record_depreciate_year_percent'=> $_POST['building_record_depreciate_year_percent'],
				'building_record_depreciate_sum_percent'=> $_POST['building_record_depreciate_sum_percent'],
				'building_record_inspector_assess_amount'=> str_replace(',','',$_POST['building_record_inspector_assess_amount']),
				'building_record_inspector_assess_reason'=> $_POST['building_record_inspector_assess_reason'],
				'building_record_staff_assess_amount'=> str_replace(',','',$_POST['building_record_staff_assess_amount']),
				'building_record_staff_assess_reason'=> $_POST['building_record_staff_assess_reason'],
				'building_record_price1'=> str_replace(',','',$_POST['building_record_price1']),
				'building_record_price2'=> str_replace(',','',$_POST['building_record_price2']),
				'building_record_cal_type'=> $_POST['building_record_cal_type'],
				'sum_all_price2'=> str_replace(',','',$_POST['sum_all_price2']),
				'building_record_status'=> '1',
			);
			$this->building_model->save('building_record',$building_record_data,$_POST['building_record_id']);

			redirect(base_url().'building/form/0/0/200');
		}

	}

	public function pdf($building_id,$person_id)
	{
		ob_start();
		$this->load->library('tcpdf');
		$this->load->helper('currency_helper');

		$filename = 'building_'.date('Ymd');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
		$pdf->SetTitle($filename);//  กำหนด Title
		$pdf->SetSubject('Export receipt'); // กำหนด Subject
		$pdf->SetKeywords($filename); // กำหนด Keyword

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->SetMargins(20, 15, 20, true);
		$pdf->AddPage();

		$building = $this->building_model->get_table_by_id('building',$building_id);
		$owner_title_list = $this->mst_model->get_by_id('title',$building['building_owner_title_id']);
		$building['building_owner_title_name'] = $owner_title_list['title_name'];
		$building_district = $this->mst_model->get_table_location_by_id('district',$building['building_district_id']);
		$building['building_district_name'] = $building_district['district_name'];
		$building_amphur = $this->mst_model->get_table_location_by_id('amphur',$building['building_amphur_id']);
		$building['building_amphur_name'] = $building_amphur['amphur_name'];
		$building_province = $this->mst_model->get_table_location_by_id('province',$building['building_province_id']);
		$building['building_province_name'] = $building_province['province_name'];
		$building_part = $this->gen_building_part($building_id);
		$building_image = $this->building_model->get_table_by_wid('building_image','building_id',$building_id);
		$building_assess = $this->building_model->get_building_assess($building_id);
		$building_type_list = $this->mst_model->get_all('building_type');

		$building_record = $this->building_model->get_table_by_wid('building_record','building_id',$building_id);

		$person = $this->building_model->get_table_by_id('person',$person_id);
		$person_title_list = $this->mst_model->get_by_id('title',$person['title_id']);
		$person['title_name'] = $person_title_list['title_name'];

		$land = $this->building_model->get_table_by_id('land',$building['land_id']);
		$land_addr_district = $this->mst_model->get_table_location_by_id('district',$land['land_addr_district_id']);
		$land['land_addr_district_name'] = $land_addr_district['district_name'];
		$land_addr_amphur = $this->mst_model->get_table_location_by_id('amphur',$land['land_addr_amphur_id']);
		$land['land_addr_amphur_name'] = $land_addr_amphur['amphur_name'];
		$land_addr_province = $this->mst_model->get_table_location_by_id('province',$land['land_addr_province_id']);
		$land['land_addr_province_name'] = $land_addr_province['province_name'];
		$land_type_list = $this->mst_model->get_all('land_type');

		$html_land_type = '';
		if (!empty($land_type_list)){
			$html_land_type .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$html_land_type .= '<tr>';
			foreach ($land_type_list as $list) {
				$html_land_type .= '<td>';
				if($list['land_type_id'] == $land['land_type_id']){
					$land['land_type_name'] = $list['land_type_name'];
					$html_land_type .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_land_type .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_land_type .= '&nbsp;'.num2Thai($list['land_type_name']).'&nbsp;';
				$html_land_type .= '</td>';

			}
			$html_land_type .= '</tr>';
			$html_land_type .= '</table>';
		}

		$html_building_type = '';
		if (!empty($building_type_list)){
			$html_building_type .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$building_type_list[] = array('building_type_id'=>'0','building_type_name'=>'อื่นๆ');
			foreach ($building_type_list as $key => $list) {
				$html_building_type .= ($key%3==0) ? '<tr>' : '';
				$html_building_type .= '<td>';
				if($list['building_type_id'] == $building['building_type_id']){
					$building['building_type_name'] = $list['building_type_name'];
					$html_building_type .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_building_type .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_building_type .= '&nbsp;'.$list['building_type_name'].'&nbsp;';
				if($list['building_type_id'] == '0' && $building['building_type_id'] == '0' && $building['building_type_other']){
					$building['building_type_name'] .= '&nbsp;('.$building['building_type_other'].')';
					$html_building_type .= $building['building_type_other'];
				}
				$html_building_type .= '</td>';
				$html_building_type .= ($key%3==2) || $key ==(count($building_type_list)-1)  ? '</tr>' : '';
			}
			$html_building_type .= '</table>';
		}
		$building_state_list = array('1'=>'ใหม่','2'=>'ปานกลาง','3'=>'เก่า','4'=>'มีการตบแต่งบำรุงรักษา','0'=>'อื่นๆ');
		$html_building_state = '';
		if (!empty($building_state_list)){
			$html_building_state .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$i = 0;
			foreach ($building_state_list as $key => $value) {
				$html_building_state .= ($i%3==0) ? '<tr>' : '';
				$html_building_state .= '<td>';
				if($key == $building['building_state']){
					$html_building_state .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_building_state .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_building_state .= '&nbsp;'.$value.'&nbsp;';
				if($key == '0' && $building['building_state'] == '0' && $building['building_state_other']){
					$html_building_state .= $building['building_state_other'];
				}
				$html_building_state .= '</td>';
				$html_building_state .= ($i%3==2) || $i == (count($building_state_list)-1)  ? '</tr>' : '';
				$i++;
			}
			$html_building_state .= '</table>';
		}
		$array = array('1'=>'ดี','2'=>'ปานกลาง','3'=>'พอใช้');
		$html_building_quality = '';
		if (!empty($array)){
			$html_building_quality .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$html_building_quality .= '<tr>';
			foreach ($array as $key => $value) {
				$html_building_quality .= '<td>';
				if($key == $building['building_quality']){
					$html_building_quality .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_building_quality .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_building_quality .= '&nbsp;'.$value.'&nbsp;';
				$html_building_quality .= '</td>';
			}
			$html_building_quality .= '</tr>';
			$html_building_quality .= '</table>';
		}
		$html_building_style = '';
		if (!empty($array)){
			$html_building_style .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$html_building_style .= '<tr>';
			foreach ($array as $key => $value) {
				$html_building_style .= '<td>';
				if($key == $building['building_style']){
					$html_building_style .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_building_style .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_building_style .= '&nbsp;'.$value.'&nbsp;';
				$html_building_style .= '</td>';
			}
			$html_building_style .= '</tr>';
			$html_building_style .= '</table>';
		}
		$html_building_skill = '';
		if (!empty($array)){
			$html_building_skill .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$html_building_skill .= '<tr>';
			foreach ($array as $key => $value) {
				$html_building_skill .= '<td>';
				if($key == $building['building_skill']){
					$html_building_skill .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_building_skill .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_building_skill .= '&nbsp;'.$value.'&nbsp;';
				$html_building_skill .= '</td>';
			}
			$html_building_skill .= '</tr>';
			$html_building_skill .= '</table>';
		}
		$html_part = '';
		if(!empty($building_part)){
			$html_part .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			foreach ($building_part as $part_id => $list) {
				$html_part .= '<tr>';
				$html_part .= '<td width="30%" valign="top">'.$list['building_part_desc'].'</td>';
				$html_part .= '<td width="70%"><table width="100%" border="0" cellpadding="0" cellspacing="0">';
				$i = 0;
				foreach ($list['sub_list'] as $part_sub_id => $desc) {
					$html_part .= ($i%2==0) ? '<tr>' : '';
					$html_part .= '<td width="50%">';
					$listx = explode('|',$building['building_part']);
					if(in_array($part_sub_id,$listx)==true){
						$html_part .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
					}else{
						$html_part .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
					}
					$html_part .= '&nbsp;'.$desc.'&nbsp;';
					$html_part .= '</td>';
					$html_part .= ($i%2==1) || $i == (count($list['sub_list'])-1)  ? '</tr>' : '';
					$i++;
				}
				$html_part .= '</table></td>';
				$html_part .= '</tr>';
			}
			$html_part .= '</table>';
		}

		$html_image = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					   <tr><td><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></td></tr>
					   </table>';
		if(!empty($building_image)){
			$html_image = '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			foreach ($building_image as $list) {
				$html_image .= '<tr>';
				$html_image .= '<td align="center"><img src="'.base_url().$list['building_image_filename'].'" style="max-width: 500px;max-height: 500px;"><br/><br/></td>';
				$html_image .= '</tr>';
			}
			$html_image .= '</table>';
		}

		$htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="'.base_url().'assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๔</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบบันทึกการตรวจสอบประเมินราคาสิ่งปลูกสร้าง
				</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">เขียนที่&nbsp;&nbsp;'.$building_record[0]['building_record_location'].'</td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่&nbsp;&nbsp;'.num2Thai(thai_display_date($building_record[0]['building_record_date'])).'</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td  colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ชื่อ-สกุลของผู้ขอสินเชื่อ &nbsp; '.$person['title_name']. $person['person_fname'].' '.$person['person_lname'].'</td>
			</tr>
			<tr>
				<td colspan="4">ชื่อ-สกุลของผู้ถือกรรมสิทธิ์สิ่งปลูกสร้าง&nbsp;'.$building['building_owner_title_name'].$building['building_owner_fname'].' '.$building['building_owner_lname'].'</td>
			</tr>
			<tr>
				<td width="25%" >ที่ตั้งของสิ่งปลูกสร้างเป็น</td>
				<td width="75%" colspan="3">'.$html_land_type.'</td>
			</tr>
			<tr>
				<td width="25%" >เลขที่ &nbsp;'.num2Thai($land['land_no']).'</td>
				<td width="75%"  colspan="3">'.num2Thai('เนื้อที่ &nbsp;'.$land['land_area_rai'].' &nbsp;ไร่ &nbsp;'.$land['land_area_ngan'].' &nbsp;งาน &nbsp;'.$land['land_area_wah'].' &nbsp;ตารางวา').'</td>
			</tr>
			<tr>
				<td width="25%" >ตำบล/แขวง&nbsp;'.$land['land_addr_district_name'].' </td>
				<td width="25%" >อำเภอ/เขต&nbsp;'.$land['land_addr_amphur_name'].' </td>
				<td width="25%" >จังหวัด&nbsp; '.$land['land_addr_province_name'].'</td>
				<td width="25%" ></td>
			</tr>
			<tr>
				<td width="25%" >สิ่งปลูกสร้างเลขที่ '.num2Thai($building['building_no']).'  </td>
				<td width="25%" >หมู่ที่ '.num2Thai($building['building_moo']).'</td>
				<td width="25%" >ตรอก/ซอย '.num2Thai($building['building_soi']).'</td>
				<td width="25%" >ถนน '.num2Thai($building['building_road']).'</td>
			</tr>
			<tr>
				<td width="25%" >ตำบล/แขวง '.$building['building_district_name'].' </td>
				<td width="25%" >อำเภอ/เขต '.$building['building_amphur_name'].'</td>
				<td width="25%" >จังหวัด '.$building['building_province_name'].'</td>
				<td width="25%" ></td>
			</tr>
			<tr>
				<td width="100%" colspan="4">ประเภท/ลักษณะสิ่งปลูกสร้าง&nbsp;'.num2Thai($building['building_layer'].'&nbsp;ชั้นบ้าน&nbsp;&nbsp;จำนวน&nbsp;'.$building['building_amount']).'&nbsp;อาคาร</td>
			</tr>
			<tr>
				<td width="10%" ></td>
				<td width="90%" colspan="3">'.$html_building_type.'</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="50%" colspan="2">ประโยชน์ใช้สอย ภายในสิ่งปลูกสร้าง (ระบุจำนวน)</td>
				<td width="25%" >&nbsp;'.num2Thai($building['building_bedroom']).'&nbsp;ห้องนอน</td>
				<td width="25%" >&nbsp;'.num2Thai($building['building_bathroom']).'&nbsp;ห้องน้ำ</td>
			</tr>
			<tr>
				<td width="25%" >&nbsp;'.num2Thai($building['building_kitchen']).'&nbsp;ห้องครัว </td>
				<td width="25%" >&nbsp;อื่นๆ &nbsp;'.num2Thai($building['building_other']).'&nbsp;</td>
				<td width="50%" colspan="2">ประเภทการใช้งาน &nbsp;'.$building['building_usage'].'</td>
			</tr>
			<tr>
				<td width="50%" colspan="2">พื้นที่ใช้สอยสิ่งปลูกสร้าง  &nbsp;'.num2Thai($building['building_area']).'&nbsp;ตารางเมตร</td>
				<td width="50%" colspan="2">อายุของสิ่งปลูกสร้าง ก่อสร้างมาประมาณ  &nbsp;'.num2Thai($building['building_age']).'&nbsp;ปี</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="25%">สภาพสิ่งปลูกสร้าง มีสภาพ</td>
				<td width="75%">'.$html_building_state.'</td>
			</tr>
			<tr>
				<td>คุณภาพวัสดุก่อสร้าง</td>
				<td>'.$html_building_quality.'</td>
			</tr>
			<tr>
				<td>รูปแบบสิ่งปลูกสร้าง</td>
				<td>'.$html_building_style.'</td>
			</tr>
			<tr>
				<td>ฝีมือการก่อสร้าง</td>
				<td>'.$html_building_skill.'</td>
			</tr>
			<tr>
				<td colspan="2"><br/></td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="text-align:center;font-weight: bold;">รายงานประกอบแบบก่อสร้างพร้อมแบบแปลน</td>
			</tr>
			<tr>
				<td>ส่วนต่างๆ ของสิ่งปลูกสร้าง</td>
			</tr>
			<tr>
				<td>'.$html_part.'</td>
			</tr>
			<tr>
				<td>ข้อมูลเพิ่มเติม &nbsp;'.$building['building_desc'].'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ให้แนบแบบแปลนโดยสังเขปหรือสำเนาพิมพ์เขียวของสิ่งปลูกสร้าง (ถ้ามี) และภาพถ่ายด้านหน้า ด้านข้าง รวมทั้งพื้นที่ใช้สอยในทุกชั้นประกอบการประเมิน</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			</table>
			<br pagebreak="true"/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="4" style="text-align:center;font-weight: bold;">รูปแผนผังสิ่งปลูกสร้างโดยสังเขป</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			<tr>
				<td colspan="4" style="text-align:center;">'.$html_image.'</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าพเจ้าขอรับรองว่า รูปแผนผังดังกล่าวนี้ถูกต้องและตั้งอยู่บนที่ดินประเภท&nbsp;'.$land['land_type_name'].'</td>
			</tr>
			<tr>
				<td width="25%" >เลขที่ &nbsp;'.num2Thai($land['land_no']).'</td>
				<td width="25%" >ตำบล/แขวง&nbsp;'.$land['land_addr_district_name'].'</td>
				<td width="25%" >อำเภอ/เขต&nbsp;'.$land['land_addr_amphur_name'].'</td>
				<td width="25%" >จังหวัด&nbsp;'.$land['land_addr_province_name'].'</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ซึ่งนำมาจำนองเป็นประกันกับ บจธ. โดยไม่มีภาระผูกพันใดๆ</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%"><br/><br/></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ขอสินเชื่อ</td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... เจ้าของสิ่งปลูกสร้างผู้ขอจำนอง</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่.............................</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่.............................</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ตรวจสอบสิ่งปลูกสร้าง</td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ตรวจสอบสิ่งปลูกสร้างและเขียนแผนผัง</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่.............................</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตำแหน่ง .................................</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่.............................</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/></td>
				</tr>
			</table>
			<br pagebreak="true"/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="font-weight: bold;text-decoration: underline;">คำแนะนำ</font></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๑.ในแผนผังให้แสดงเส้นทางคมนาคมและระยะทางอย่างต่อเนื่องจากสถานที่สำคัญที่เป็นถาวรวัตถุใกล้เคียง (ระบุชื่อ) เช่น สถานที่ราชการ วัด โรงเรียน ชุมชน เป็นต้น จนถึงที่ตั้งของสิ่งปลูกสร้างหรือใกล้เคียงสิ่งปลูกสร้างที่สุด พร้อมทั้ง ระบุชื่อเส้นทางคมนาคมและระบุว่าเส้นทางนี้เชื่อมโยงระหว่างหมู่บ้านหรือชุมชนอะไร กรณีมีสิ่งปลูกสร้างหลายประเภท หรือหลายอาคารหรือหลายหลัง ให้เขียนรูปสิ่งปลูกสร้างดังกล่าวด้วย</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๒.ให้เจ้าของสิ่งปลูกสร้างและผู้ขอสินเชื่อลงลายมือชื่อด้วย</td>
				</tr>
				<tr>
					<td><br/><br/></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="text-align:center;font-weight: bold;">การประเมินราคาสิ่งปลูกสร้าง</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #000;">
			<tr>
				<td width="7%" style="border: 1px solid #000;text-align:center;">ลำดับ</td>
				<td width="20%" style="border: 1px solid #000;text-align:center;">ประเภทสิ่งปลูกสร้าง</td>
				<td width="15%" style="border: 1px solid #000;text-align:center;">เนื้อที่ (ตร.ม.)</td>
				<td width="20%" style="border: 1px solid #000;text-align:center;">ราคาต่อ ตร.ม.</td>
				<td width="20%" style="border: 1px solid #000;text-align:center;">ราคาประเมิน</td>
				<td width="18%" style="border: 1px solid #000;text-align:center;">ราคาค่าก่อสร้างตามแบบแปลน/สัญญา*</td>
			</tr>
			<tr>
				<td width="7%" style="border: 1px solid #000;text-align:center;">1</td>
				<td width="20%" style="border: 1px solid #000;">&nbsp;'.$building['building_type_name'].'</td>
				<td width="15%" style="border: 1px solid #000;">&nbsp;'.num2Thai($building_assess[0]['building_assess_area']).'</td>
				<td width="20%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_price_per_area'],2)).'&nbsp;&nbsp;</td>
				<td width="20%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_price'],2)).'&nbsp;&nbsp;</td>
				<td width="18%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_cost'],2)).'&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td width="62%" style="border: 1px solid #000;" colspan="4">&nbsp;รวมราคาประเมินสิ่งปลูกสร้างก่อนหักค่าเสื่อม</td>
				<td width="20%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_price'],2)).'&nbsp;&nbsp;</td>
				<td width="18%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_cost'],2)).'&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td width="62%" style="border: 1px solid #000;" colspan="4">&nbsp;หักค่าเสื่อมราคา &nbsp;'.num2Thai($building_record[0]['building_record_depreciate_year']).'&nbsp; ปี จะเป็น &nbsp;'.num2Thai($building_record[0]['building_record_depreciate_sum_percent']).'&nbsp; %</td>
				<td width="20%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format((($building_assess[0]['building_assess_price']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2)).'&nbsp;&nbsp;</td>
				<td width="18%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format((($building_assess[0]['building_assess_cost']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2)).'&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td width="62%" style="border: 1px solid #000;" colspan="4">&nbsp;ราคาประเมินสิ่งปลูกสร้างหลังหักค่าเสื่อม </td>
				<td width="20%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_price']-(($building_assess[0]['building_assess_price']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2)).'&nbsp;&nbsp;</td>
				<td width="18%" style="border: 1px solid #000;text-align:right;">'.num2Thai(number_format($building_assess[0]['building_assess_cost']-(($building_assess[0]['building_assess_cost']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2)).'&nbsp;&nbsp;</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><br/></td>
			</tr>
			<tr>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าพเจ้าขอรับรองว่า สิ่งปลูกสร้างดังกล่าวข้างต้นเป็นกรรมสิทธิ์ของข้าพเจ้าจริง และยินยอมให้ใช้ เป็นหลักประกันหนี้เงินกู้ของ&nbsp; '.$person['title_name']. $person['person_fname'].' '.$person['person_lname'].'&nbsp;หากไม่เป็นความจริงให้ใช้คำรับรองนี้ เป็นหลักฐานในการดำเนินการตามกฎหมายกับข้าพเจ้าได้</td>
			</tr>
			<tr>
				<td><br/><br/></td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="40%" style="text-align:left;"></td>
					<td width="60%" style="text-align:left;">ลงชื่อ ......................................... เจ้าของสิ่งปลูกสร้างผู้ขอจำนอง</td>
				</tr>
				<tr>
					<td width="40%" style="text-align:left;"></td>
					<td width="60%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(............................................)</td>
				</tr>
				<tr>
					<td width="40%" style="text-align:left;"></td>
					<td width="60%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่ .................................</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
				<tr>
					<td width="40%" style="text-align:left;"></td>
					<td width="60%" style="text-align:left;">ลงชื่อ ......................................... เจ้าของสิ่งปลูกสร้างผู้ขอจำนอง</td>
				</tr>
				<tr>
					<td width="40%" style="text-align:left;"></td>
					<td width="60%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(............................................)</td>
				</tr>
				<tr>
					<td width="40%" style="text-align:left;"></td>
					<td width="60%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่ .................................</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
			</table>
			<br pagebreak="true"/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" >* บันทึกกรณีประเมินค่าก่อสร้างตามแบบแปลน</td>
					<td width="50%" ></td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="font-weight: bold;">สรุปผลการตรวจสอบประเมินราคาของผู้ตรวจสอบสิ่งปลูกสร้าง</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จากการวิเคราะห์และประเมินราคาสิ่งปลูกสร้าง รวมทั้งการพิจารณาข้อมูล และรายละเอียดต่างๆ ดังกล่าวมาข้างต้น ควรประเมินมูลค่าสิ่งปลูกสร้างสิ่งปลูกสร้างเป็นเงินจำนวน &nbsp;'.num2Thai(number_format($building_record[0]['building_record_inspector_assess_amount'],2)).'&nbsp;บาท
('.num2wordsThai($building_record[0]['building_record_inspector_assess_amount']).')</td>
			</tr>
			<tr>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ความเห็นของพนักงานผู้ตรวจสอบสิ่งปลูกสร้าง &nbsp;'.$building_record[0]['building_record_inspector_assess_reason'].'</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" style="text-align:left;">ลงชื่อ ........................................ ผู้ตรวจสอบสิ่งปลูกสร้าง</td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ตรวจสอบสิ่งปลูกสร้าง</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตำแหน่ง .................................</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตำแหน่ง .................................</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่.............................</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่.............................</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td style="text-align:center;font-weight: bold;">บันทึกของผู้ได้รับมอบอำนาจ</td>
			</tr>
			<tr>
				<td><br/></td>
			</tr>
			<tr>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ให้จำนองสิ่งปลูกสร้างดังกล่าวข้างต้น ภายในวงเงิน  &nbsp;'.num2Thai(number_format($building_record[0]['building_record_staff_assess_amount'],2)).'&nbsp;บาท
('.num2wordsThai($building_record[0]['building_record_staff_assess_amount']).')</td>
			</tr>
			<tr>
				<td >ความเห็นเพิ่มเติม &nbsp;'.$building_record[0]['building_record_staff_assess_reason'].'</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(............................................)</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตำแหน่ง ..............................................</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่ .................................</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
			</table>';
		//echo $htmlcontent;
		$pdf->SetFont('thsarabun', '', 16);
		$pdf->writeHTML($htmlcontent, true, 0, true, true);

		$pdf->Output($filename.'.pdf', 'I');
	}

	public function word($building_id,$person_id)
	{
		include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$building = $this->building_model->get_table_by_id('building',$building_id);
		$owner_title_list = $this->mst_model->get_by_id('title',$building['building_owner_title_id']);
		$building['building_owner_title_name'] = $owner_title_list['title_name'];
		$building_district = $this->mst_model->get_table_location_by_id('district',$building['building_district_id']);
		$building['building_district_name'] = $building_district['district_name'];
		$building_amphur = $this->mst_model->get_table_location_by_id('amphur',$building['building_amphur_id']);
		$building['building_amphur_name'] = $building_amphur['amphur_name'];
		$building_province = $this->mst_model->get_table_location_by_id('province',$building['building_province_id']);
		$building['building_province_name'] = $building_province['province_name'];
		$building_part = $this->gen_building_part($building_id);
		$building_image = $this->building_model->get_table_by_wid('building_image','building_id',$building_id);
		$building_assess = $this->building_model->get_building_assess($building_id);
		$building_type_list = $this->mst_model->get_all('building_type');

		$building_record = $this->building_model->get_table_by_wid('building_record','building_id',$building_id);

		$person = $this->building_model->get_table_by_id('person',$person_id);
		$person_title_list = $this->mst_model->get_by_id('title',$person['title_id']);
		$person['title_name'] = $person_title_list['title_name'];

		$land = $this->building_model->get_table_by_id('land',$building['land_id']);
		$land_addr_district = $this->mst_model->get_table_location_by_id('district',$land['land_addr_district_id']);
		$land['land_addr_district_name'] = $land_addr_district['district_name'];
		$land_addr_amphur = $this->mst_model->get_table_location_by_id('amphur',$land['land_addr_amphur_id']);
		$land['land_addr_amphur_name'] = $land_addr_amphur['amphur_name'];
		$land_addr_province = $this->mst_model->get_table_location_by_id('province',$land['land_addr_province_id']);
		$land['land_addr_province_name'] = $land_addr_province['province_name'];
		$land_type_list = $this->mst_model->get_all('land_type');

		$html_land_type = ' ';
		if (!empty($land_type_list)){
			foreach ($land_type_list as $list) {
				if($list['land_type_id'] == $land['land_type_id']){
					$land['land_type_name'] = $list['land_type_name'];
					$html_land_type .= '( √ )';
				}else{
					$html_land_type .= '( )';
				}
				$html_land_type .= ' '.num2Thai($list['land_type_name']).' ';
			}
		}

		$html_building_type = ' ';
		if (!empty($building_type_list)){
			$building_type_list[] = array('building_type_id'=>'0','building_type_name'=>'อื่นๆ');
			foreach ($building_type_list as $key => $list) {
				if($list['building_type_id'] == $building['building_type_id']){
					$building['building_type_name'] = $list['building_type_name'];
					$html_building_type .= '( √ )';
				}else{
					$html_building_type .= '(  )';
				}
				$html_building_type .= ' '.$list['building_type_name'].' ';
				if($list['building_type_id'] == '0' && $building['building_type_id'] == '0' && $building['building_type_other']){
					$building['building_type_name'] .= ' ('.$building['building_type_other'].')';
					$html_building_type .= $building['building_type_other'];
				}
			}
		}

		$building_state_list = array('1'=>'ใหม่','2'=>'ปานกลาง','3'=>'เก่า','4'=>'มีการตบแต่งบำรุงรักษา','0'=>'อื่นๆ');
		$html_building_state = '';
		if (!empty($building_state_list)){
			foreach ($building_state_list as $key => $value) {
				if($key == $building['building_state']){
					$html_building_state .= '( √ )';
				}else{
					$html_building_state .= '(  )';
				}
				$html_building_state .= ' '.$value.' ';
				if($key == '0' && $building['building_state'] == '0' && $building['building_state_other']){
					$html_building_state .= $building['building_state_other'];
				}
			}
		}

		$array = array('1'=>'ดี','2'=>'ปานกลาง','3'=>'พอใช้');
		$html_building_quality = '';
		if (!empty($array)){
			foreach ($array as $key => $value) {
				if($key == $building['building_quality']){
					$html_building_quality .= '( √ )';
				}else{
					$html_building_quality .= '(  )';
				}
				$html_building_quality .= ' '.$value.' ';
			}
		}
		$html_building_style = '';
		if (!empty($array)){
			foreach ($array as $key => $value) {
				if($key == $building['building_style']){
					$html_building_style .= '( √ )';
				}else{
					$html_building_style .= '(  )';
				}
				$html_building_style .= ' '.$value.' ';
			}
		}
		$html_building_skill = '';
		if (!empty($array)){
			foreach ($array as $key => $value) {
				if($key == $building['building_skill']){
					$html_building_skill .= '( √ )';
				}else{
					$html_building_skill .= '(  )';
				}
				$html_building_skill .= ' '.$value.' ';
			}
		}

		$GLOBALS['building_record_location'] = $building_record[0]['building_record_location'] ? $building_record[0]['building_record_location'] : '-';
		$GLOBALS['building_record_date'] = $building_record[0]['building_record_date'] ? num2Thai(thai_display_date($building_record[0]['building_record_date'])) : '-';
		$GLOBALS['person_fullname'] = $person['title_name']. $person['person_fname'].' '.$person['person_lname'];
		$GLOBALS['building_owner_fullname'] = $building['building_owner_title_name'].$building['building_owner_fname'].' '.$building['building_owner_lname'];
		$GLOBALS['land_type_list'] = $html_land_type;
		$GLOBALS['land_no'] = $land['land_no'] ? num2Thai($land['land_no']) : '-';
		$GLOBALS['land_area_rai'] = $land['land_area_rai'] ? num2Thai($land['land_area_rai']) : '-';
		$GLOBALS['land_area_ngan'] = $land['land_area_ngan'] ? num2Thai($land['land_area_ngan']) : '-';
		$GLOBALS['land_area_wah'] = $land['land_area_wah'] ? num2Thai($land['land_area_wah']) : '-';
		$GLOBALS['land_addr_district_name'] = $land['land_addr_district_name'] ? $land['land_addr_district_name'] : '-';
		$GLOBALS['land_addr_amphur_name'] = $land['land_addr_amphur_name'] ? $land['land_addr_amphur_name'] : '-';
		$GLOBALS['land_addr_province_name'] = $land['land_addr_province_name'] ? $land['land_addr_province_name'] : '-';
		$GLOBALS['building_no'] = $building['building_no'] ? num2Thai($building['building_no']) : '-';
		$GLOBALS['building_moo'] = $building['building_moo'] ? num2Thai($building['building_moo']) : '-';
		$GLOBALS['building_soi'] = $building['building_soi'] ? num2Thai($building['building_soi']) : '-';
		$GLOBALS['building_road'] = $building['building_road'] ? num2Thai($building['building_road']) : '-';
		$GLOBALS['building_district_name'] = $building['building_district_name'] ? $building['building_district_name'] : '-';
		$GLOBALS['building_amphur_name'] = $building['building_amphur_name'] ? $building['building_amphur_name'] : '-';
		$GLOBALS['building_province_name'] = $building['building_province_name'] ? $building['building_province_name'] : '-';
		$GLOBALS['building_layer'] = $building['building_layer'] ? num2Thai($building['building_layer']) :'-';
		$GLOBALS['building_type_list'] = $html_building_type;
		$GLOBALS['building_amount'] = $building['building_amount'] ? num2Thai($building['building_amount']) :'-';
		$GLOBALS['building_bedroom'] = $building['building_bedroom'] ? num2Thai($building['building_bedroom']) :'-';
		$GLOBALS['building_bathroom'] = $building['building_bathroom'] ? num2Thai($building['building_bathroom']) :'-';
		$GLOBALS['building_kitchen'] = $building['building_kitchen'] ? num2Thai($building['building_kitchen']) :'-';
		$GLOBALS['building_other'] = $building['building_other'] ? num2Thai($building['building_other']) :'-';
		$GLOBALS['building_usage'] = $building['building_usage'] ? $building['building_usage'] : '-';
		$GLOBALS['building_area'] = $building['building_area'] ? num2Thai($building['building_area']) :'-';
		$GLOBALS['building_age'] = $building['building_age'] ? num2Thai($building['building_age']) :'-';
		$GLOBALS['building_state_list'] = $html_building_state;
		$GLOBALS['building_quality_list'] = $html_building_quality;
		$GLOBALS['building_style_list'] = $html_building_style;
		$GLOBALS['building_skill_list'] = $html_building_skill;
		$GLOBALS['building_desc'] = $building['building_desc'] ? $building['building_desc'] : '-';
		$GLOBALS['land_type_name'] = $land['land_type_name'] ? $land['land_type_name'] : '-';
		$GLOBALS['building_assess_price'] = $building_assess[0]['building_assess_price'] ? num2Thai(number_format($building_assess[0]['building_assess_price'],2)) : '-';
		$GLOBALS['building_assess_cost'] = $building_assess[0]['building_assess_cost'] ? num2Thai(number_format($building_assess[0]['building_assess_cost'],2)) : '-';
		$GLOBALS['building_record_depreciate_year'] = $building_record[0]['building_record_depreciate_year'] ? num2Thai($building_record[0]['building_record_depreciate_year']) : '-';
		$GLOBALS['building_record_depreciate_sum_percent'] = $building_record[0]['building_record_depreciate_sum_percent'] ? num2Thai($building_record[0]['building_record_depreciate_sum_percent']) : '-';
		$GLOBALS['building_assess_depreciate_price'] = num2Thai(number_format((($building_assess[0]['building_assess_price']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2));
		$GLOBALS['building_assess_depreciate_cost'] = num2Thai(number_format((($building_assess[0]['building_assess_cost']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2));
		$GLOBALS['building_assess_sum_price'] = num2Thai(number_format($building_assess[0]['building_assess_price']-(($building_assess[0]['building_assess_price']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2));
		$GLOBALS['building_assess_sum_cost'] = num2Thai(number_format($building_assess[0]['building_assess_cost']-(($building_assess[0]['building_assess_cost']*$building_record[0]['building_record_depreciate_sum_percent'])/100),2));
		$GLOBALS['building_record_inspector_assess_amount'] = $building_record[0]['building_record_inspector_assess_amount'] ? num2Thai(number_format($building_record[0]['building_record_inspector_assess_amount'],2)) : '-';
		$GLOBALS['building_record_inspector_assess_amount_text'] = $building_record[0]['building_record_inspector_assess_amount'] ? num2wordsThai($building_record[0]['building_record_inspector_assess_amount']) : '-';
		$GLOBALS['building_record_inspector_assess_reason'] = $building_record[0]['building_record_inspector_assess_reason'] ? $building_record[0]['building_record_inspector_assess_reason'] : '-';
		$GLOBALS['building_record_staff_assess_amount'] = $building_record[0]['building_record_staff_assess_amount'] ? num2Thai(number_format($building_record[0]['building_record_staff_assess_amount'],2)) : '-';
		$GLOBALS['building_record_staff_assess_amount_text'] = $building_record[0]['building_record_staff_assess_amount'] ? num2wordsThai($building_record[0]['building_record_staff_assess_amount']) : '-';
		$GLOBALS['building_record_staff_assess_reason'] = $building_record[0]['building_record_staff_assess_reason'] ? $building_record[0]['building_record_staff_assess_reason'] : '-';

		$template = 'word/template/4_building.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		$TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#building_image#', (!empty($building_image[0]['building_image_filename']) ? $building_image[0]['building_image_filename'] : 'word/template/map.png'), array('unique' => 1));

		$datax = array();
		if(!empty($building_part)){
			foreach ($building_part as $part_id => $list) {
				$html_part = '';
				foreach ($list['sub_list'] as $part_sub_id => $desc) {
					$listx = explode('|',$building['building_part']);
					if(in_array($part_sub_id,$listx)==true){
						$html_part .= '( √ )';
					}else{
						$html_part .= '(  )';
					}
					$html_part .= ' '.$desc.' ';
				}
				$datax[] = array('name'=>$list['building_part_desc'], 'sub_name_list'=>$html_part);
			}
		}
		$TBS->MergeBlock('building_part', $datax);

		$datax = array();
		$datax[] = array('no'=>'๑',
					'building_type_name'=>($building['building_type_name'] ? $building['building_type_name'] : '-'),
					'building_assess_area'=>($building_assess[0]['building_assess_area'] ? num2Thai($building_assess[0]['building_assess_area'])  : '-'),
					'building_assess_price_per_area'=>($building_assess[0]['building_assess_price_per_area'] ? num2Thai(number_format($building_assess[0]['building_assess_price_per_area'],2)) : '-'),
					'building_assess_price'=>($building_assess[0]['building_assess_price'] ? num2Thai(number_format($building_assess[0]['building_assess_price'],2)) : '-'),
					'building_assess_cost'=>($building_assess[0]['building_assess_cost'] ? num2Thai(number_format($building_assess[0]['building_assess_cost'],2)) : '-'));
		$TBS->MergeBlock('building_assess', $datax);

		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'building_'.date('Y-m-d').'.docx';
		$temp_file = tempnam(sys_get_temp_dir(), 'Docx');

	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);
	}


	public function send_download($temp_file,$file) {
	      $basename = basename($file);
	      $length   = sprintf("%u", filesize($temp_file));

	      header('Content-Description: File Transfer');
	      header('Content-Type: application/octet-stream');
	      header('Content-Disposition: attachment; filename="' . $basename . '"');
	      header('Content-Transfer-Encoding: binary');
	      header('Connection: Keep-Alive');
	      header('Expires: 0');
	      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	      header('Pragma: public');
	      header('Content-Length: ' . $length);
	      ob_clean();
	      flush();
	      set_time_limit(0);
	      readfile($temp_file);
	      exit();
	  }

}
