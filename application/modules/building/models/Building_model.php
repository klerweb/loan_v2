<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_model extends CI_Model
{
	private $table = 'building';
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function search_building_record($thaiid='', $fullname='')
	{
		$where = '';
		if($thaiid!='') $where .= "OR person.person_thaiid = '".$thaiid."' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);

		$this->db->select("loan.loan_code,loan.loan_status,building_record.building_record_status,building_record.building_record_id,building_record.building_id,person.person_id,person.person_fname,person.person_lname,land.land_no,building.*,
		(select province_name as building_province_name from master_province where province_id=building.building_province_id),
		(select amphur_name as building_amphur_name from master_amphur where amphur_id=building.building_amphur_id),
		(select district_name as building_district_name from master_district where district_id=building.building_district_id),
		(select building_type_name from config_building_type where building_type_id=building.building_type_id)");
		$this->db->from("building_record");
		$this->db->join("building", "building_record.building_id = building.building_id", "LEFT");
		$this->db->join("land", "building.land_id = land.land_id", "LEFT");
		$this->db->join("loan_guarantee_land", "land.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->join("loan", "loan_guarantee_land.loan_id = loan.loan_id", "LEFT");
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		//$this->db->where("building_record.building_record_status", "1");
		//$this->db->where("building.building_status", "2");//สิ่งปลูกสร้างที่บันทึกแบบบันทึกแล้ว
		//$this->db->where("land.land_status", "1");
		//$this->db->where("loan_guarantee_land.loan_guarantee_land_status", "1");
		//$this->db->where("loan.loan_status", "1");//
		//$this->db->where("person.person_status", "1");

		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);

		if(check_permission_isowner('loan')){
	   		$this->db->where("building_record.building_record_createby", get_uid_login());
	    }

		$query = $this->db->get();
	    //echo $this->db->last_query();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}

	public function search_building($thaiid='', $fullname='')
	{
		$where = '';
		if($thaiid!='') $where .= "OR person.person_thaiid = '".$thaiid."' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);

		$this->db->select("person.person_id,person.person_fname,person.person_lname,land.land_no,building.*,
		(select province_name as building_province_name from master_province where province_id=building.building_province_id),
		(select amphur_name as building_amphur_name from master_amphur where amphur_id=building.building_amphur_id),
		(select district_name as building_district_name from master_district where district_id=building.building_district_id),
		(select building_type_name from config_building_type where building_type_id=building.building_type_id)");
		$this->db->from('building');
		$this->db->join("land", "building.land_id = land.land_id", "LEFT");
		$this->db->join("loan_guarantee_land", "land.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->join("loan", "loan_guarantee_land.loan_id = loan.loan_id", "LEFT");
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->where("building.building_status", "1");
		//$this->db->where("land.land_status", "1");
		//$this->db->where("loan_guarantee_land.loan_guarantee_land_status", "1");
		$this->db->where("loan.loan_status", "1");
		//$this->db->where("person.person_status", "1");

		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);

	    if(check_permission_isowner('loan')){
	   		$this->db->where("building.building_createby", get_uid_login());
	    }

		$query = $this->db->get();
	   // echo $this->db->last_query();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}

	public function get_table_by_id($table,$id)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($table."_id", $id);
		$query = $this->db->get();

		$result = $query->num_rows()!=0? $query->row_array() : null;
		return $result;
	}

	public function get_table_by_wid($table,$field,$id)
	{
		$this->db->select("*");
		$this->db->from($table);
		$this->db->where($field, $id);
		$query = $this->db->get();

		$result = $query->num_rows()!=0? $query->result_array() : null;
		return $result;
	}

	public function save($table,$array, $id='')
	{
		$field_id = $table.'_id';

		if($id=='')
		{
			$array[$table.'_createdate'] = date($this->config->item('log_date_format'));
			$array[$table.'_createby'] = get_uid_login();

			$this->db->set($array);
			$this->db->insert($table);
			$id = $this->db->insert_id();
		}
		else
		{
			$array[$table.'_updatedate'] = date($this->config->item('log_date_format'));
			$array[$table.'_updateby'] = get_uid_login();

			$this->db->where($field_id, $id);
			$this->db->update($table, $array);
		}
		echo $this->db->last_query();
		$num_row = $this->db->affected_rows();

		return array('id' => $id, 'rows' => $num_row);
	}

	public function delete($table,$id)
	{
		$this->db->delete($table,array($table.'_id'=>$id));
	}

	/*public function delete_building_assess($building_id,$building_part_id,$building_part_sub_id) {
        $this->db->delete('building_assess', array('building_id' => $building_id,'building_part_id'=>$building_part_id,'building_part_sub_id'=>$building_part_sub_id));
    }*/

	public function get_building_assess($building_id)
	{
		$this->db->select("building_assess.*");
		$this->db->from('building_assess');
		//$this->db->where("building_assess.building_assess_status", "1");
		$this->db->where("building_assess.building_id", $building_id);
		$query = $this->db->get();
	    //echo $this->db->last_query();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}

/*	public function check_building_part($building_id,$building_part_id,$building_part_sub_id){
		$this->db->select("building_assess_id");
		$this->db->from("building_assess");
		$this->db->where("building_id", $building_id);
		$this->db->where("building_part_id", $building_part_id);
		$this->db->where("building_part_sub_id", $building_part_sub_id);
		$this->db->where("building_assess_status", "1");
		$query = $this->db->get();

		$result = $query->num_rows()!=0? 1 : 0;
		return $result;
	}*/

	public function get_building_part()
	{
		$this->db->select("config_building_part.building_part_id,config_building_part.building_part_desc,config_building_part_sub.building_part_sub_id,config_building_part_sub.building_part_sub_desc");
		$this->db->from('config_building_part');
		$this->db->join("config_building_part_sub", "config_building_part.building_part_id = config_building_part_sub.building_part_id", "LEFT");
		$this->db->where("config_building_part.building_part_status", "1");
		$this->db->where("config_building_part_sub.building_part_sub_status", "1");
		$query = $this->db->get();
	    //echo $this->db->last_query();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}

	public function getByLand($land)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("config_building_type", "config_building_type.building_type_id = building.building_type_id", "LEFT");
		$this->db->join("master_district", "master_district.district_id = building.building_district_id", "LEFT");
		$this->db->join("master_amphur", "master_amphur.amphur_id = building.building_amphur_id", "LEFT");
		$this->db->join("master_province", "master_province.province_id = building.building_province_id", "LEFT");
		//$this->db->where("building_record.building_record_status !=", "0");
		//$this->db->where("building.building_status !=", "0");
		$this->db->where("building.land_id", $land);

		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}
}
?>
