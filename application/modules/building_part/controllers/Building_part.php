<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_part extends MY_Controller
{
	private $title_page = 'ส่วนต่างๆ ของสิ่งปลูกสร้าง';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('building_part_model');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->building_part_model->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_building_part/building_part.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('building_part', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('building_part')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่ม'.$this->title_page;
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('building_part_model');
			$data_content['data'] = $this->building_part_model->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่ม'.$this->title_page;
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไข'.$this->title_page;
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_title/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('building_part_model');
		
		$array['building_part_desc'] = $this->input->post('txtBuildingPartDesc');
		$array['building_part_status'] = '1';
		
		$id = $this->building_part_model->save($array, $this->input->post('txtBuildingPartId'));
		
		if($this->input->post('txtBuildingPartId')=='')
		{
			$data = array(
					'alert' => 'บันทึก'.$this->title_page.'เรียบร้อยแล้ว',
					'link' => site_url('building_part'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไข'.$this->title_page.'เรียบร้อยแล้ว',
					'link' => site_url('building_part'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('building_part_model');
		$this->building_part_model->del($id);
		
		$data = array(
				'alert' => 'ยกเลิก'.$this->title_page.'เรียบร้อยแล้ว',
				'link' => site_url('building_part'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['building_part_desc'] = '';
		
		return $data;
	}
}