<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url('building_part/form'); ?>'">เพิ่มส่วนต่างๆ ของสิ่งปลูกสร้าง</button>
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url('building_part_sub'); ?>'">ชนิดของส่วนต่างๆ ของสิ่งปลูกสร้าง</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
		            	<th>ส่วนต่างๆ ของสิ่งปลูกสร้าง</th>
		            	<th>ชนิดของส่วนต่างๆ ของสิ่งปลูกสร้าง</th>
		                <th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					if(!empty($result))
					{
						$i = 1;
						foreach ($result as $row)
						{
							$this->load->model('building_part_sub/building_part_sub_model');
							$building_part_sub = $this->building_part_sub_model->getByPartId($row['building_part_id']);
					?>
					<tr>
						<td><?php echo $i++; ?></td>
		           		<td><?php echo $row['building_part_desc']; ?></td>
		           		<td>
		           		<?php 
		           			$html_part_sub = '';
		           			if(!empty($building_part_sub)){
		           				foreach ($building_part_sub as $key => $row) {
		           					$html_part_sub .= '<a href="'.base_url().'building_part_sub/form/'.$row['building_part_sub_id'].'">'.$row['building_part_sub_desc'].'</a>';
		           					$html_part_sub .= ($key == (count($building_part_sub)-1)) ? '' : ', ';
		           					$html_part_sub .= ($key%5==4) ? '<br/>' : '';
		           				}
		           			}
		           			echo $html_part_sub;
		           		?>
		           		</td>
		           		<td>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="location.href='<?php echo site_url('building_part/form/'.$row['building_part_id']); ?>'"><span class="fa fa-edit"></span></button>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ยกเลิก" data-original-title="ยกเลิก" onclick="del('<?php echo $row['building_part_id']; ?>')"><span class="fa fa-trash-o"></span></button>
		           		</td>
		           	</tr>
				<?php
						}	
					} 
				?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>