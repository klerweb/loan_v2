<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delay_model extends CI_Model
{	
	private $table = 'loan_delay';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	function search_delay($loan_code='', $loan_contract_no='', $thaiid='',$fullname='')
	{
		$where = '';
		if($loan_code!='') $where .= "OR loan.loan_code LIKE '%".$loan_code."%' ";
		if($loan_contract_no!='') $where .= "OR loan_contract.loan_contract_no LIKE '%".$loan_contract_no."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan_contract", "loan_contract.loan_contract_id = loan_delay.loan_contract_id", "LEFT");
		$this->db->join("loan", "loan.loan_id = loan_delay.loan_id", "LEFT");		
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan.loan_status", "2");
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	function search_contract($loan_code='', $loan_contract_no='', $thaiid='',$fullname='')
	{
		$where = '';
		if($loan_code!='') $where .= "OR loan.loan_code LIKE '%".$loan_code."%' ";
		if($loan_contract_no!='') $where .= "OR loan_contract.loan_contract_no LIKE '%".$loan_contract_no."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from("loan_contract");
		$this->db->join("loan", "loan.loan_id = loan_contract.loan_id", "LEFT");		
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan.loan_status", "2");
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	function get_loan_by_id($id)
	{
		$this->db->select("*");
        $this->db->from('loan');
        $this->db->where('loan_id', $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_type_by_loan_id($loan_id)
	{
		$this->db->select("*");
        $this->db->from('loan_type');
        $this->db->join("master_loan_objective", "master_loan_objective.loan_objective_id = loan_type.loan_objective_id", "LEFT");		
        $this->db->where('loan_id', $loan_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_contract_by_loan_id($loan_id)
	{
		$this->db->select("*");
        $this->db->from('loan_contract');
        $this->db->where('loan_id', $loan_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_balance_by_loan_contract_id($loan_contract_id)
	{
		$this->db->select("*");
        $this->db->from('loan_balance');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_payment_schedule($loan_contract_id,$date_payout=null){
		$this->db->select("*");
        $this->db->from('payment_schedule');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $this->db->where('status', 'N');
        if($date_payout)$this->db->where('date_trunc(\'day\',date_payout) =', $date_payout);
        $this->db->order_by("loan_pay_number", "asc");
        $this->db->order_by("date_payout", "asc");
       // $this->db->limit(1);
        $query = $this->db->get();
        
        //echo $this->db->last_query();

        $result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
	
	function get_payment_schedule_behide_by_loan_contract_id($loan_contract_id){
		$this->db->select("count(*) as sum_behide_number,sum(money_pay) as sum_behide_money");
        $this->db->from('payment_schedule');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $this->db->where('status', 'N');
        $this->db->where('isinvgenerate', '1');
        $this->db->where('date_trunc(\'day\',date_payout) <', date('Y-m-d'));
        $query = $this->db->get();
        
       // echo $this->db->last_query();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_invoice_by_loan_contract_id($loan_contract_id,$loan_pay_number){
		$this->db->select("*");
        $this->db->from('loan_invoice');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $this->db->where('loan_pay_number', $loan_pay_number);
        $query = $this->db->get();
        
      //  echo $this->db->last_query();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_delay($loan_id,$loan_contract_id='',$delaydate='')
	{
		$this->db->select("*");
        $this->db->from('loan_delay');
        $this->db->where('loan_id', $loan_id);
        if($loan_contract_id)$this->db->where('loan_contract_id', $loan_contract_id);
        if($delaydate)$this->db->where('date_trunc(\'day\',delaydate)', $delaydate);
        $this->db->order_by("delaydate", "asc");
        $query = $this->db->get();
		//echo $this->db->last_query();
		//echo $query->num_rows();
        $result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
	
	function save($table,$array)
	{
		$this->db->set($array);
		$this->db->insert($table);
		//$id = $this->db->insert_id();		
		$num_row = $this->db->affected_rows();	
		//echo $this->db->last_query();	
		//return array('id' => $id, 'rows' => $num_row);
		return array('rows' => $num_row);
	}
	
	function update_payment_schedule($table,$array,$where)
	{
		if (!empty($where)){
			foreach ($where as $key => $row) {
				$this->db->where($key, $row);
			}		
			$this->db->update($table, $array);
			//echo $this->db->last_query();
		}
	}
}