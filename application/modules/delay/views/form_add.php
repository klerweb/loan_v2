<form id="frm1" name="frm1" method="post" action="<?php echo base_url().'delay/save'?>" enctype="multipart/form-data">
<div class="row">
	<div class="col-sm-12">
		<h4>แบบแสดงความจำนงขอผ่อนผันการชำระสินเชื่อ</h4>
	</div>
</div>
<?php 
if(empty($delaydate)){
	$delaylocation = 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)';
	$delaywritedate = date('d/m/').(date('Y')+543);
	$date_payout_max = date('d/m/').(date('Y')+543);
	$loan_amount = @$loan_balance_data['loan_amount'];	 
	$princle_pay = @$invoice_data['principle'];
	$interest_pay = @$invoice_data['interest'];
	$money_pay = $princle_pay+$interest_pay;
	$behind_number = @$payment_behide_data['sum_behide_number'];
	$behind_money = @$payment_behide_data['sum_behide_money'];
	$nopay_reason = '';
	$delay_amt = @$payment_data[0]['money_pay'];
	$delay_paydate = date('Y-m-d',strtotime(date('Y-m-d').' + 12 months'));
	list($y, $m, $d) = explode('-', $delay_paydate);
	$delay_paydate = $d.'/'.$m.'/'.(intval($y)+543);	
	
}else{
	if (empty($delay_data))redirect_url(base_url().'delay','ขออภัยค่ะ ไม่พบข้อมูล');
	$delaylocation = @$delay_data[0]['delaylocation'];
	list($y, $m, $d) = explode('-', @$delay_data[0]['delaywritedate']);
	$delaywritedate = $d.'/'.$m.'/'.(intval($y)+543);
	list($y, $m, $d) = explode('-', @$delay_data[0]['delaydate']);
	$date_payout_max = $d.'/'.$m.'/'.(intval($y)+543);
	$loan_amount = @$delay_data[0]['loan_amount'];
	$princle_pay = @$delay_data[0]['princle_pay'];
	$interest_pay = @$delay_data[0]['interest_pay'];
	$money_pay = $princle_pay+$interest_pay;
	$behind_number = @$delay_data[0]['behind_number'];
	$behind_money = @$delay_data[0]['behind_money'];
	$nopay_reason = @$delay_data[0]['nopay_reason']; 
	$delay_amt = @$delay_data[0]['delay_amt']; 
	list($y, $m, $d) = explode('-', @$delay_data[0]['delay_paydate']);
	$delay_paydate = $d.'/'.$m.'/'.(intval($y)+543);
}
?>
<div class="row">
	    <div class="col-sm-6 form-group">
	         <label class="control-label">เขียนที่ <span class="asterisk">*</span></label>
	         <input type="text" name="delaylocation" id="delaylocation" class="form-control" value="<?php echo @$delaylocation?>" required>
	    </div>
	    <div class="col-sm-3 form-group">
	         <label class="control-label">วันที่ <span class="asterisk">*</span></label>
	         <div class="input-group">
	         	<input type="text" name="delaywritedate" id="delaywritedate" class="form-control" value="<?php echo $delaywritedate?>" readonly="readonly" />
				<span class="input-group-addon hand" id="icon_delaywritedate"><i class="glyphicon glyphicon-calendar"></i></span>
             </div>
	    </div> 
</div>
<div class="row">
	<div class="col-sm-12">
		<h5>ผู้ขอสินเชื่อ</h5>
	</div>
</div>
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">ชื่อ-สกุล</label>
		<input type="text" name="person_name" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_fname'].' '.@$person_data['person_lname'];?>">
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">วัน เดือน ปี เกิด</label>
		<?php 
		if($person_data['person_birthdate']){
			list($y, $m, $d) = explode('-', $person_data['person_birthdate']);
			$person_data['person_birthdate'] = $d.'/'.$m.'/'.(intval($y)+543);	
		}
		?>
		<input type="text" name="person_birthdate" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_birthdate'];?>">
	</div> 
	<!--<div class="col-sm-3 form-group">
		<label class="control-label">อายุ</label>
		<div class="input-group">
			<input type="text" name="person_age" id="person_age" readonly="readonly" class="form-control"/>
			<span class="input-group-addon">ปี</span>
		</div>
	</div> 
	--><div class="col-sm-3 form-group">
		<label class="control-label">เพศ</label>
		<select name="sex_id" id="sex_id" class="width100p" disabled="disabled" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($sex as $val){?>
				<option value="<?php echo $val['sex_id']; ?>" <?php echo $person_data['sex_id'] == $val['sex_id'] ? 'selected' : '';?>><?php echo $val['sex_name'];?></option>
			<?php } ?>
		</select>
	</div>   
</div><!-- row -->	
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">เชื้อชาติ</label>
		<select name="race_id" id="race_id" class="width100p" disabled="disabled" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($race as $val){?>
				<option value="<?php echo $val['race_id']; ?>" <?php echo $person_data['race_id'] == $val['race_id'] ? 'selected' : '';?>><?php echo $val['race_name'];?></option>
			<?php } ?>
		</select>
	</div>   
	<div class="col-sm-3 form-group">
		<label class="control-label">สัญชาติ</label>
		<select name="nationality_id" id="nationality_id" class="width100p" disabled="disabled" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($nationality as $val){?>
				<option value="<?php echo $val['nationality_id']; ?>" <?php echo $person_data['nationality_id'] == $val['nationality_id'] ? 'selected' : '';?>><?php echo $val['nationality_name'];?></option>
			<?php } ?>
		</select>
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">ศาสนา</label>
		<select name="religion_id" id="religion_id" class="width100p" disabled="disabled" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($religion as $val){?>
				<option value="<?php echo $val['religion_id']; ?>" <?php echo $person_data['religion_id'] == $val['religion_id'] ? 'selected' : '';?>><?php echo $val['religion_name'];?></option>
			<?php } ?>
		</select>
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">เลขประจำตัวประชาชน</label>
		<input type="text" name="person_thaiid" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_thaiid'];?>">
	</div>      
</div><!-- row -->	
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">วันออกบัตร</label>
		<?php 
		if(@$person_data['person_card_startdate']){
			list($y, $m, $d) = explode('-', $person_data['person_card_startdate']);
			$person_data['person_card_startdate'] = $d.'/'.$m.'/'.(intval($y)+543);	
		}
		?>
		<input type="text" name="person_card_startdate" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_card_startdate'];?>">
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">บัตรหมดอายุ</label>
		<?php 
		if(@$person_data['person_card_expiredate']){
			list($y, $m, $d) = explode('-', $person_data['person_card_expiredate']);
			$person_data['person_card_expiredate'] = $d.'/'.$m.'/'.(intval($y)+543);	
		}
		?>
		<input type="text" name="person_card_expiredate" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_card_expiredate'];?>">
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">ที่อยู่ตามบัตรประชาชน เลขที่</label>
		<input type="text" name="person_addr_card_no" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_addr_card_no'];?>">
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">หมู่ที่</label>
		<input type="text" name="person_addr_card_moo" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_addr_card_moo'];?>">
	</div> 
</div>
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">ตำบล/แขวง</label>
		<select name="person_addr_card_district_id" id="person_addr_card_district_id" disabled="disabled" class="width100p" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($district_card as $val){?>
				<option value="<?php echo $val['district_id']; ?>" <?php echo $person_data['person_addr_card_district_id'] == $val['district_id'] ? 'selected' : '';?>><?php echo $val['district_name'];?></option>
			<?php } ?>
		</select>
	</div>   
	<div class="col-sm-3 form-group">
		<label class="control-label">อำเภอ/เขต</label>
		<select name="person_addr_card_amphur_id" id="person_addr_card_amphur_id" disabled="disabled" class="width100p" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($amphur_card as $val){?>
				<option value="<?php echo $val['amphur_id']; ?>" <?php echo $person_data['person_addr_card_amphur_id'] == $val['amphur_id'] ? 'selected' : '';?>><?php echo $val['amphur_name'];?></option>
			<?php } ?>
		</select>
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">จังหวัด</label>
		<select name="person_addr_card_province_id" id="person_addr_card_province_id" disabled="disabled" class="width100p" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($province_card as $val){?>
				<option value="<?php echo $val['province_id']; ?>" <?php echo $person_data['person_addr_card_province_id'] == $val['province_id'] ? 'selected' : '';?>><?php echo $val['province_name'];?></option>
			<?php } ?>
		</select>
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">ที่อยู่ปัจจุบัน เลขที่</label>
		<input type="text" name="person_addr_pre_no" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_addr_pre_no'];?>">
	</div>      
</div><!-- row -->
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">หมู่ที่</label>
		<input type="text" name="person_addr_pre_moo" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_addr_pre_moo'];?>">
	</div> 
	<div class="col-sm-3 form-group">
		<label class="control-label">ตำบล/แขวง</label>
		<select name="person_addr_pre_district_id" id="person_addr_pre_district_id" disabled="disabled" class="width100p" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($district_pre as $val){?>
				<option value="<?php echo $val['district_id']; ?>" <?php echo $person_data['person_addr_pre_district_id'] == $val['district_id'] ? 'selected' : '';?>><?php echo $val['district_name'];?></option>
			<?php } ?>
		</select>
	</div>   
	<div class="col-sm-3 form-group">
		<label class="control-label">อำเภอ/เขต</label>
		<select name="person_addr_pre_amphur_id" id="person_addr_pre_amphur_id" disabled="disabled" class="width100p" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($amphur_pre as $val){?>
				<option value="<?php echo $val['amphur_id']; ?>" <?php echo $person_data['person_addr_pre_amphur_id'] == $val['amphur_id'] ? 'selected' : '';?>><?php echo $val['amphur_name'];?></option>
			<?php } ?>
		</select>
	</div>  
	<div class="col-sm-3 form-group">
		<label class="control-label">จังหวัด</label>
		<select name="person_addr_pre_province_id" id="person_addr_pre_province_id" disabled="disabled" class="width100p" placeholder="กรุณาเลือก">
		<option value="">กรุณาเลือก</option>
			<?php foreach($province_pre as $val){?>
				<option value="<?php echo $val['province_id']; ?>" <?php echo $person_data['person_addr_card_province_id'] == $val['province_id'] ? 'selected' : '';?>><?php echo $val['province_name'];?></option>
			<?php } ?>
		</select>
	</div>      
</div><!-- row -->
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">โทรศัพท์บ้าน</label>
		<input type="text" name="person_phone" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_phone'];?>">
	</div> 
	<div class="col-sm-3 form-group">
		<label class="control-label">มือถือ</label>
		<input type="text" name="person_mobile" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_mobile'];?>">
	</div> 
	<div class="col-sm-3 form-group">
		<label class="control-label">E-mail</label>
		<input type="text" name="person_email" class="form-control" readonly="readonly" value="<?php echo @$person_data['person_email'];?>">
	</div> 
</div>
<div class="row">
	<div class="col-sm-12">
	   <hr/>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h5>รายละเอียดเงินกู้</h5>
	</div>
</div>
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">มีหนี้ที่จะต้องชำระคืนแก่ บจธ. ณ วันที่</label>
		<input type="text" name="date_payout_max" class="form-control" readonly="readonly" value="<?php echo $date_payout_max?>">
	</div> 
	<div class="col-sm-3 form-group">
		<label class="control-label">รวมเป็นเงินทั้งสิ้น</label>
		<div class="input-group">
			<input type="text" name="loan_amount" class="form-control right" readonly="readonly" value="<?php echo @number_format($loan_amount,2)?>">
			<span class="input-group-addon">บาท</span>
		</div>		
	</div> 
</div><!-- row -->
<div class="row">
	<div class="col-sm-6 form-group">
		<label class="control-label">เงินกู้สินเชื่อเพื่อ</label>
		<input type="text" name="loan_type" class="form-control" readonly="readonly" value="<?php echo @$loan_type_data['loan_objective_name']?>">
	</div> 
</div><!-- row -->
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">สัญญาฉบับลงวันที่</label>
		<?php 
		if($loan_contract_data['loan_contract_date']){
			list($y, $m, $d) = explode('-', $loan_contract_data['loan_contract_date']);
			$loan_contract_data['loan_contract_date'] = $d.'/'.$m.'/'.(intval($y)+543);	
		}
		?>
		<input type="text" name="loan_contract_date" class="form-control" readonly="readonly" value="<?php echo @$loan_contract_data['loan_contract_date']?>">
	</div>      
	<div class="col-sm-3 form-group">
		<label class="control-label">วงเงินสินเชื่อ</label>
		<div class="input-group">
			<input type="text" name="loan_contract_amount" class="form-control right" value="<?php echo @number_format($loan_contract_data['loan_contract_amount'],2)?>" readonly="readonly"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div>    
</div><!-- row -->	
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">ณ วันที่</label>
		<input type="text" name="now_date" class="form-control" readonly="readonly" value="<?php echo @$date_payout_max?>">
	</div> 
	<div class="col-sm-3 form-group">
		<label class="control-label">มียอดหนี้ค้างชำระเป็นเงินต้นคงเหลือ</label>
		<div class="input-group">
			<input type="text" name="money_pay_sum" class="form-control right" value="<?php echo @number_format($loan_amount,2)?>" readonly="readonly"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div>  
</div>
<div class="row"> 
	<div class="col-sm-3 form-group">
		<label class="control-label">โดยส่งชำระงวดละ</label>
		<div class="input-group">
			<input type="text" name="money_pay" class="form-control right" value="<?php echo @number_format($money_pay,2)?>" readonly="readonly"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div>
	<div class="col-sm-3 form-group">
		<label class="control-label">เงินต้น</label>
		<div class="input-group">
			<input type="text" name="princle_pay" class="form-control right" value="<?php echo @number_format($princle_pay,2)?>" readonly="readonly"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div>
	<div class="col-sm-3 form-group">
		<label class="control-label">ดอกเบี้ย</label>
		<div class="input-group">
			<input type="text" name="interest_pay" class="form-control right" value="<?php echo @number_format($interest_pay,2)?>" readonly="readonly"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div>
</div>	
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">กรณีมีเงินค้างชำระโปรดระบุ จำนวน</label>
		<div class="input-group">
			<input type="text" name="behind_number" class="form-control" value="<?php echo @number_format($behind_number)?>"  readonly="readonly"/>
			<span class="input-group-addon">งวด</span>
		</div>
	</div>      
	<div class="col-sm-3 form-group">
		<label class="control-label">เป็นจำนวนเงิน</label>
		<div class="input-group">
			<input type="text" name="behind_money" class="form-control  right" value="<?php echo @number_format($behind_money,2)?>"  readonly="readonly"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div>    
</div><!-- row -->	
<div class="row">
	<div class="col-sm-12">
	   <hr/>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 form-group">
		<label>ปัจจุบันมีปัญหาที่ทำให้ไม่สามารถส่งชำระเงินกู้ได้ตามเงินงวดปกติที่กำหนด เนื่องจาก (ระบุปัญหาสาเหตุโดยละเอียด)<span class="asterisk">*</span></label>
		<textarea id="nopay_reason" name="nopay_reason" class="form-control" rows="3" required><?php echo @$nopay_reason;?></textarea>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<h5>ข้าพเจ้าขอความอนุเคราะห์ผ่อนผันการชำระสินเชื่อที่ค้างชำระดังกล่าว</h5>
	</div>
</div>
<div class="row">
	<div class="col-sm-3 form-group">
		<label class="control-label">งวดที่<span class="asterisk">*</span></label>
		<input type="text" name="delay_period" class="form-control" readonly="readonly" value="<?php echo @$payment_data[0]['loan_pay_number']?>">
		<input type="hidden" name="delay_date_payout" class="form-control" readonly="readonly" value="<?php echo @$payment_data[0]['date_payout']?>">
		<input type="hidden" name="delay_loan_id" class="form-control" readonly="readonly" value="<?php echo @$payment_data[0]['loan_id']?>">
		<input type="hidden" name="delay_loan_contract_id" class="form-control" readonly="readonly" value="<?php echo @$payment_data[0]['loan_contract_id']?>">
		<input type="hidden" name="interest_rate" class="form-control" readonly="readonly" value="<?php echo @$payment_data[0]['interest_rate']?>">
	</div>      
	<div class="col-sm-3 form-group">
		<label class="control-label">โดยขอผ่อนชำระ<span class="asterisk">*</span></label>
		<div class="input-group">
			<input type="text" name="delay_amt" id="delay_amt" readonly="readonly" class="form-control right" value="<?php echo @number_format($delay_amt,2)?>"/>
			<span class="input-group-addon">บาท</span>
		</div>
	</div> 
	<div class="col-sm-3 form-group">
	    <label class="control-label">กำหนดชำระ <span class="asterisk">*</span></label>
	    <div class="input-group">
            <input type="text" name="delay_paydate" id="delay_paydate" class="form-control" value="<?php echo @$delay_paydate?>" readonly="readonly" />
			<span class="input-group-addon hand" id="icon_delay_paydate"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
	</div>       
</div><!-- row -->	


</form>
<script type="text/javascript">
jQuery(document).ready(function () {
	  $('#btnSubmit').click(function (e) {
	    	$('#frm1').submit();
	    });
	    
	  $("#sex_id, #race_id, #nationality_id, #religion_id, #person_addr_card_district_id, #person_addr_card_amphur_id, #person_addr_card_province_id, #person_addr_pre_district_id,#person_addr_pre_amphur_id,#person_addr_pre_province_id").select2({
			minimumResultsForSearch: -1
		});

	 $("#delaywritedate,#delay_paydate").datepicker({ 
	    	dateFormat: "dd/mm/yy", 
	    	
	    	isBuddhist: true,
	    	dayNames: ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"],
	        dayNamesMin: ["อา.","จ.","อ.","พ.","พฤ.","ศ.","ส."],
	        monthNames: ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"],
	        monthNamesShort: ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."],
	        changeMonth: true,  
	        changeYear: true ,  
	        beforeShow:function(){  
	            if($(this).val()!="")
	            {  
	                var arrayDate = $(this).val().split("/");       
	                arrayDate[2] = parseInt(arrayDate[2])-543;  
	                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
	            }  
	            setTimeout(function(){  
	                $.each($(".ui-datepicker-year option"),function(j,k){  
	                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
	                    $(".ui-datepicker-year option").eq(j).text(textYear);  
	                });               
	            },50);
	        },  
	        onChangeMonthYear: function(){  
	            setTimeout(function(){  
	                $.each($(".ui-datepicker-year option"),function(j,k){  
	                    var textYear = parseInt($(".ui-datepicker-year option").eq(j).val())+543;  
	                    $(".ui-datepicker-year option").eq(j).text(textYear);  
	                });               
	            },50);        
	        },  
	        onClose:function(){  
	            if($(this).val()!="" && $(this).val()==dateBefore){           
	                var arrayDate = dateBefore.split("/");  
	                arrayDate[2] = parseInt(arrayDate[2])+543;  
	                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);      
	            }         
	        },  
	        onSelect: function(dateText, inst){   
	            dateBefore = $(this).val();  
	            var arrayDate = dateText.split("/");  
	            arrayDate[2] = parseInt(arrayDate[2])+543;  
	            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);  
	        }  
	    });

	    $("#icon_delaywritedate").click(function(){ 
	    	$("#delaywritedate").datepicker("show"); 
	    });

	    $("#icon_delay_paydate").click(function(){ 
	    	$("#delay_paydate").datepicker("show"); 
	    });

	    $("#frm1").validate({
			rules: {
			},
			highlight: function(element) {
				$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
			},
			unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    },
			errorElement: 'span',
		    errorClass: 'help-block',
		    errorPlacement: function(error, element) {
		        if(element.parent('.input-group').length) {
		            error.insertAfter(element.parent());
		        } else {
		            error.insertAfter(element);
		        }
		    },
			success: function(element) {
				$(element).closest(".form-group").removeClass("has-error");
			}		
		});			    
});
</script>
