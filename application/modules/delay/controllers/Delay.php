<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delay extends MY_Controller
{
	private $menu = 'แบบปรับโครงสร้างหนี้';
	private $title = 'แบบแสดงความจำนงขอผ่อนผันการชำระสินเชื่อ';
	private $message;

	function __construct()
	{
		parent::__construct();

		$this->load->model('delay_model');
		$this->load->model('loan/mst_model');
		$this->load->model('person/person_model');
		$this->load->model('province/province_model');
		$this->load->model('amphur/amphur_model');
		$this->load->model('district/district_model');
		$this->message = array('200'=> 'บันทึกข้อมูลเรียบร้อย');
	}

	public function index()
	{
		$loan_code = '';
		$loan_contract_no = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan_code = $_POST['txtLoan'];
		if(!empty($_POST['loan_contract_no'])) $loan_contract_no = $_POST['loan_contract_no'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_menu['menu'] = 'debt';
		$data_breadcrumb['menu'] = array($this->menu =>'#');

		$data_content = array(
				'loan_code' => $loan_code,
				'loan_contract_no' => $loan_contract_no,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->delay_model->search_delay($loan_code, $loan_contract_no, $thaiid, $fullname)
		);

		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('delay', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	function form($loan_id='',$loan_contract_id='',$delaydate='',$status='')
	{
		$data_menu['menu'] = 'debt';
		$data_breadcrumb['menu'] = array(
				$this->menu =>'#',
				$this->title => site_url('delay')
			);

		$data_content = array(
			'status'=>$status,
			'message'=>!empty($this->message[$status]) ? $this->message[$status] : '-',
			'loan_id' => $loan_id > 0 ? $loan_id : '',
			'loan_contract_id' => $loan_contract_id > 0 ? $loan_contract_id :'',
			'delaydate' => $delaydate > 0 ? $delaydate :''
		);

		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	function search()
	{
		$loan_code = '';
		$loan_contract_no = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan_code = $_POST['txtLoan'];
		if(!empty($_POST['loan_contract_no'])) $loan_contract_no = $_POST['loan_contract_no'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_content = array(
				'loan_code' => $loan_code,
				'loan_contract_no' => $loan_contract_no,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->delay_model->search_contract($loan_code, $loan_contract_no, $thaiid, $fullname)
		);
		$this->parser->parse('form_list', $data_content);
	}

	function form_add($loan_id='',$loan_contract_id='',$delaydate='')
	{
		$delay_data = $this->delay_model->get_loan_delay($loan_id,$loan_contract_id,$delaydate);
		$payment_data = $this->delay_model->get_payment_schedule($loan_contract_id);

		if((count($delay_data) < 2 && count($payment_data) > 0)){
			if (!empty($delay_data) && empty($delaydate)){
				foreach ($delay_data as $row) {
					if($row['delaydate'] >= date('Y-m-d')){
						redirect_url(base_url().'delay','ขออภัยค่ะ รายการสินเชื่อ ได้ทำการขอผ่อนผันการชำระสินเชื่อไปแล้ว');
						exit(0);
					}
				}
			}

			$loan_data = $this->delay_model->get_loan_by_id($loan_id);
			$person_data = $this->person_model->getById($loan_data['person_id']);
			$loan_type_data = $this->delay_model->get_loan_type_by_loan_id($loan_id);
			$loan_contract_data = $this->delay_model->get_loan_contract_by_loan_id($loan_id);
			$loan_balance_data = $this->delay_model->get_loan_balance_by_loan_contract_id($loan_contract_id);
			$payment_behide_data = $this->delay_model->get_payment_schedule_behide_by_loan_contract_id($loan_contract_id);
			$invoice_data = $this->delay_model->get_loan_invoice_by_loan_contract_id($loan_contract_id,$payment_data[0]['loan_pay_number']);

			$data_content = array(
				'loan_id' => $loan_id,
				'loan_contract_id' => $loan_contract_id,
				'delaydate' => $delaydate,
				'loan_data' => $loan_data,
				'person_data' => $person_data,
				'loan_type_data' => $loan_type_data,
				'loan_contract_data' => $loan_contract_data,
				'loan_balance_data' => $loan_balance_data,
				'payment_data' => $payment_data,
				'payment_behide_data' => $payment_behide_data,
				'invoice_data' => $invoice_data,
				'delay_data' => $delay_data,
				'sex' => $this->mst_model->get_all('sex'),
				'race' => $this->mst_model->get_all('race'),
				'nationality' => $this->mst_model->get_all('nationality'),
				'religion' => $this->mst_model->get_all('religion'),
				'province_card' => $this->province_model->all(),
				'amphur_card' => $this->amphur_model->getByProvince($person_data['person_addr_card_province_id']),
				'district_card' => $this->district_model->getByAmphur($person_data['person_addr_card_amphur_id']),
				'province_pre' => $this->province_model->all(),
				'amphur_pre' => $this->amphur_model->getByProvince($person_data['person_addr_pre_province_id']),
				'district_pre' => $this->district_model->getByAmphur($person_data['person_addr_pre_amphur_id']),
			);

			$this->parser->parse('form_add', $data_content);

		}else{
			redirect_url(base_url().'delay','ขออภัยค่ะ รายการสินเชื่อ ไม่เข้าเงื่อนไขที่จะขอผ่อนผันการชำระสินเชื่อได้');
			exit(0);
		}
	}

	function save()
	{
		list($d, $m, $y) = explode('/', $_POST['delay_paydate']);
		$_POST['delay_paydate'] = (intval($y)-543).'-'.$m.'-'.$d;

		list($d, $m, $y) = explode('/', $_POST['delaywritedate']);
		$_POST['delaywritedate'] = (intval($y)-543).'-'.$m.'-'.$d;

		$_POST['delay_amt'] = str_replace(",","",$_POST['delay_amt']);
		$_POST['loan_amount'] = str_replace(",","",$_POST['loan_amount']);
		$_POST['princle_pay'] = str_replace(",","",$_POST['princle_pay']);
		$_POST['interest_pay'] = str_replace(",","",$_POST['interest_pay']);
		$_POST['behind_number'] = str_replace(",","",$_POST['behind_number']);
		$_POST['behind_money'] = str_replace(",","",$_POST['behind_money']);

		$delay_data = array(
				'loan_id'=> $_POST['delay_loan_id'],
				'loan_contract_id' => $_POST['delay_loan_contract_id'],
				'delaydate'=> date($this->config->item('log_date_format')),
				'nopay_reason'=> $_POST['nopay_reason'],
				'delay_period'=> $_POST['delay_period'],
				'delay_amt'=> $_POST['delay_amt'],
				'delaylocation'=> $_POST['delaylocation'],
				'delaywritedate'=> $_POST['delaywritedate'],
				'delay_paydate'=> $_POST['delay_paydate'],
				'loan_amount'=> $_POST['loan_amount'],
				'princle_pay'=> $_POST['princle_pay'],
				'interest_pay'=> $_POST['interest_pay'],
				'behind_number'=> $_POST['behind_number'],
				'behind_money'=> $_POST['behind_money'],
		);
		$this->delay_model->save('loan_delay',$delay_data);

		$payment_data = $this->delay_model->get_payment_schedule($_POST['delay_loan_contract_id']);
		if (!empty($payment_data)){
			foreach ($payment_data as $list) {
				if ($list['date_payout'] < $_POST['delay_paydate']){
					$data['duedelaydate'] = $_POST['delay_paydate'];
					$where['loan_contract_id'] = $list['loan_contract_id'];
					$where['loan_pay_number'] = $list['loan_pay_number'];
					$where['date_trunc(\'day\',date_payout) ='] = $list['date_payout'];
					$this->delay_model->update_payment_schedule('payment_schedule',$data,$where);
				}
			}
		}
		redirect(base_url().'delay/form/0/0/0/200');
	}

	public function pdf($loan_id,$loan_contract_id,$delaydate)
	{
		ob_start();
		$this->load->library('tcpdf');
		$this->load->helper('currency_helper');

		$filename = 'delay_'.date('Ymd');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
		$pdf->SetTitle($filename);//  กำหนด Title
		$pdf->SetSubject('Export receipt'); // กำหนด Subject
		$pdf->SetKeywords($filename); // กำหนด Keyword

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->SetMargins(20, 15, 20, true);
		$pdf->AddPage();

		$delay_data = $this->delay_model->get_loan_delay($loan_id,$loan_contract_id,$delaydate);
		$loan_data = $this->delay_model->get_loan_by_id($loan_id);
		$loan_type_data = $this->delay_model->get_loan_type_by_loan_id($loan_id);
		$loan_contract_data = $this->delay_model->get_loan_contract_by_loan_id($loan_id);

		$person_data = $this->person_model->getById($loan_data['person_id']);
		$person_title_list = $this->mst_model->get_by_id('title',$person_data['title_id']);
		$person_data['title_name'] = $person_title_list['title_name'];
		list($y, $m, $d) = explode('-', $person_data['person_birthdate']);
		$person_data['person_birthdate'] = $d.'/'.$m.'/'.(intval($y)+543);
		$sex_list = $this->mst_model->get_all('sex');
		$person_race_list = $this->mst_model->get_by_id('race',$person_data['race_id']);
		$person_data['race_name'] = $person_race_list['race_name'];
		$person_nationality_list = $this->mst_model->get_by_id('nationality',$person_data['nationality_id']);
		$person_data['nationality_name'] = $person_nationality_list['nationality_name'];
		$person_religion_list = $this->mst_model->get_by_id('religion',$person_data['religion_id']);
		$person_data['religion_name'] = $person_religion_list['religion_name'];
		list($y, $m, $d) = explode('-', $person_data['person_card_startdate']);
		$person_data['person_card_startdate'] = $d.'/'.$m.'/'.(intval($y)+543);
		list($y, $m, $d) = explode('-', $person_data['person_card_expiredate']);
		$person_data['person_card_expiredate'] = $d.'/'.$m.'/'.(intval($y)+543);
		$card_addr_district = $this->mst_model->get_table_location_by_id('district',$person_data['person_addr_card_district_id']);
		$person_data['person_addr_card_district_name'] = $card_addr_district['district_name'];
		$card_addr_amphur = $this->mst_model->get_table_location_by_id('amphur',$person_data['person_addr_card_amphur_id']);
		$person_data['person_addr_card_amphur_name'] = $card_addr_amphur['amphur_name'];
		$card_addr_province = $this->mst_model->get_table_location_by_id('province',$person_data['person_addr_card_province_id']);
		$person_data['person_addr_card_province_name'] = $card_addr_province['province_name'];
		$pre_addr_district = $this->mst_model->get_table_location_by_id('district',$person_data['person_addr_pre_district_id']);
		$person_data['person_addr_pre_district_name'] = $pre_addr_district['district_name'];
		$pre_addr_amphur = $this->mst_model->get_table_location_by_id('amphur',$person_data['person_addr_pre_amphur_id']);
		$person_data['person_addr_pre_amphur_name'] = $pre_addr_amphur['amphur_name'];
		$pre_addr_province = $this->mst_model->get_table_location_by_id('province',$person_data['person_addr_pre_province_id']);
		$person_data['person_addr_pre_province_name'] = $pre_addr_province['province_name'];

		$html_sex = '';
		if (!empty($sex_list)){
			$html_sex .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
			$html_sex .= '<tr>';
			foreach ($sex_list as $list) {
				$html_sex .= '<td>';
				if($list['sex_id'] == $person_data['sex_id']){
					$html_sex .= '<img src="'.base_url().'assets\icon\checkbox_check.png">';
				}else{
					$html_sex .= '<img src="'.base_url().'assets\icon\checkbox_uncheck.png">';
				}
				$html_sex .= '&nbsp;'.$list['sex_name'].'&nbsp;';
				$html_sex .= '</td>';

			}
			$html_sex .= '</tr>';
			$html_sex .= '</table>';
		}

		$htmlcontent = '
		<style>
		.line{border-bottom: 1px solid #000;}
		u{border-bottom: 1px solid #000;text-decoration:underline solid}
		</style>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="'.base_url().'assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. ๑๒</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบแสดงความจำนงขอผ่อนผันการชำระสินเชื่อ
				</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="7%">เขียนที่</td>
				<td width="48%" class="line">'.$delay_data[0]['delaylocation'].'</td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="7%">วันที่</td>
				<td width="48%" class="line">'.num2Thai(thai_display_date($delay_data[0]['delaywritedate'])).'</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>เรื่อง&nbsp;&nbsp;ขอผ่อนผันการชำระสินเชื่อ</td>
			</tr>
			<tr>
				<td>เรียน&nbsp;&nbsp;ผู้อำนวยการสถาบันบริหารจัดการธนาคารที่ดิน</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="23%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าพเจ้า &nbsp;</td>
				<td width="60%" class="line">'.$person_data['title_name']. $person_data['person_fname'].' '.$person_data['person_lname'].'</td>
				<td width="17%">(ผู้ได้รับสินเชื่อ)</td>
			</tr>
			<tr>
				<td width="20%">วันเกิด (วัน/เดือน/ปี)&nbsp;</td>
				<td width="30%" class="line">'.num2Thai($person_data['person_birthdate']).'</td>
				<td width="50%">เพศ&nbsp;'.$html_sex.'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="7%">เชื้อชาติ</td>
				<td width="23%" class="line">&nbsp;'.$person_data['race_name'].'</td>
				<td width="7%">สัญชาติ</td>
				<td width="23%" class="line">&nbsp;'.$person_data['nationality_name'].'</td>
				<td width="7%">ศาสนา</td>
				<td width="33%" class="line">&nbsp;'.$person_data['religion_name'].'</td>
			</tr>
			<tr>
				<td width="22%">เลขประจำตัวประชาชน</td>
				<td width="24%" class="line">&nbsp;'.num2Thai(formatThaiid($person_data['person_thaiid'])).'&nbsp;</td>
				<td width="12%">วันออกบัตร</td>
				<td width="15%" class="line">&nbsp;'.num2Thai($person_data['person_card_startdate']).'&nbsp;</td>
				<td width="12%">บัตรหมดอายุ</td>
				<td width="15%" class="line">&nbsp;'.num2Thai($person_data['person_card_expiredate']).'</td>
			</tr>
			<tr>
				<td width="25%">ที่อยู่ตามบัตรประชาชน</td>
				<td width="5%">เลขที่</td>
				<td width="30%" class="line">&nbsp;'.num2Thai($person_data['person_addr_card_no']).'</td>
				<td width="5%">หมู่ที่</td>
				<td width="35%" colspan="2" class="line">&nbsp;'.num2Thai($person_data['person_addr_card_moo']).'</td>
			</tr>
			<tr>
				<td width="11%">ตำบล/แขวง</td>
				<td width="24%" class="line">&nbsp;'.$person_data['person_addr_card_district_name'].'&nbsp;</td>
				<td width="10%">อำเภอ/เขต</td>
				<td width="25%" class="line">&nbsp;'.$person_data['person_addr_card_amphur_name'].'&nbsp;</td>
				<td width="7%">จังหวัด</td>
				<td width="23%" class="line">&nbsp;'.$person_data['person_addr_card_province_name'].'</td>
			</tr>
			<tr>
				<td width="25%">ที่อยู่ปัจจุบัน</td>
				<td width="5%">เลขที่</td>
				<td width="30%" class="line">&nbsp;'.num2Thai($person_data['person_addr_pre_no']).'</td>
				<td width="5%">หมู่ที่</td>
				<td width="35%" colspan="2" class="line">&nbsp;'.num2Thai($person_data['person_addr_pre_moo']).'</td>
			</tr>
			<tr>
				<td width="11%">ตำบล/แขวง</td>
				<td width="24%" class="line">&nbsp;'.$person_data['person_addr_pre_district_name'].'&nbsp;</td>
				<td width="10%">อำเภอ/เขต</td>
				<td width="25%" class="line">&nbsp;'.$person_data['person_addr_pre_amphur_name'].'&nbsp;</td>
				<td width="7%">จังหวัด</td>
				<td width="23%" class="line">&nbsp;'.$person_data['person_addr_pre_province_name'].'</td>
			</tr>
			<tr>
				<td width="13%">โทรศัพท์บ้าน</td>
				<td width="22%" class="line">&nbsp;'.num2Thai($person_data['person_phone']).'</td>
				<td width="7%">มือถือ</td>
				<td width="23%" class="line">&nbsp;'.num2Thai($person_data['person_mobile']).'</td>
				<td width="7%">E:mail</td>
				<td width="28%" class="line">&nbsp;'.$person_data['person_email'].'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3">ได้ กู้เงินไว้กับสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.) มีความประสงค์ขอให้ บจธ. ผ่อนผันการชำระหนี้ ดังนี้</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๑. ข้าพเจ้ายอมรับว่าเป็นหนี้ต่อ บจธ. โดยมีหนี้ที่จะต้องชำระคืนแก่ บจธ. ณ วันที่ &nbsp;<u>&nbsp;'.num2Thai(thai_display_date($delay_data[0]['delaydate']).'&nbsp;</u> รวมเป็นเงินทั้งสิ้น &nbsp;<u>&nbsp;'.number_format($delay_data[0]['loan_amount'],2).'&nbsp;</u>&nbsp;บาท (<u>'.num2wordsThai($delay_data[0]['loan_amount'])).'</u>) มีรายละเอียด ดังนี้</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๑.๑ &nbsp;เงินกู้สินเชื่อเพื่อ'.$loan_type_data['loan_objective_name'].'</td>
			</tr>
			<tr>
				<td colspan="3">ตามสัญญาฉบับลงวันที่&nbsp;<u>'.num2Thai(thai_display_date($loan_contract_data['loan_contract_date']).'</u>&nbsp;&nbsp;&nbsp;วงเงินสินเชื่อ&nbsp;<u>'.number_format($loan_contract_data['loan_contract_amount'],2).'</u>&nbsp;บาท ณ วันที่ &nbsp;<u>'.thai_display_date($delay_data[0]['delaydate']).'</u>&nbsp;&nbsp;&nbsp;มียอดหนี้ค้างชำระเป็นเงินต้นคงเหลือ&nbsp;<u>'.number_format($delay_data[0]['loan_amount'],2).'</u>&nbsp;บาท โดยส่งชำระงวดละ <u>'.number_format($delay_data[0]['princle_pay']+$delay_data[0]['interest_pay'],2).'</u> บาท (เงินต้น&nbsp;<u>'.number_format($delay_data[0]['princle_pay'],2).'</u>&nbsp;บาท &nbsp;ดอกเบี้ย&nbsp;<u>'.number_format($delay_data[0]['interest_pay'],2).'</u>&nbsp;บาท) กรณีมีเงินค้างชำระโปรดระบุจำนวน&nbsp;<u>'.number_format($delay_data[0]['behind_number']).'</u>&nbsp;งวด&nbsp;เป็นจำนวนเงิน&nbsp;<u>'.number_format($delay_data[0]['behind_money'],2)).'</u>&nbsp;บาท</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๒. ปัจจุบันมีปัญหาที่ทำให้ไม่สามารถส่งชำระเงินกู้ได้ตามเงินงวดปกติที่กำหนด เนื่องจาก (ระบุปัญหาสาเหตุโดยละเอียด)</td>
			</tr>
			<tr>
				<td colspan="3" class="line">'.$delay_data[0]['nopay_reason'].'</td>
			</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๓. ข้าพเจ้าขอความอนุเคราะห์ผ่อนผันการชำระสินเชื่อที่ค้างชำระดังกล่าว งวดที่ &nbsp;<u>&nbsp;&nbsp;'.num2Thai(number_format($delay_data[0]['delay_period']).'&nbsp;&nbsp;&nbsp;&nbsp;</u> &nbsp;เป็นจำนวนเงิน โดยขอผ่อนชำระ  &nbsp;<u>&nbsp;&nbsp;'.number_format($delay_data[0]['delay_amt'],2)).'&nbsp;&nbsp;&nbsp;&nbsp;</u>&nbsp;บาท</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;๔. หากข้าพเจ้าไม่ปฏิบัติตามข้อตกลงข้อหนึ่งข้อใดของหนังสือนี้ ให้ถือว่าผิดนัดชำระหนี้ที่ค้างชำระ
ทั้งหมด และยินยอมให้ บจธ. บังคับคิดดอกเบี้ยในอัตราผิดนัดตามที่ประกาศกำหนด และให้ฟ้องร้องบังคับคดีได้ทันที</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ในการนี้ ขอให้ บจธ.ดำเนินการดังกล่าวข้างต้น ข้าพเจ้ายอมผูกพันปฏิบัติตามระเบียบข้อบังคับ เงื่อนไขหรือหลักเกณฑ์ที่ บจธ.กำหนดให้ต้องปฏิบัติตามทุกประการ</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อโปรดพิจารณา และให้ความอนุเคราะห์แก่ข้าพเจ้าตามสมควร</td>
			</tr>
			<tr>
				<td><br/><br/></td>
			</tr>
			<tr>
				<td></td>
				<td>ขอแสดงความนับถือ</td>
				<td></td>
			</tr>
			<tr>
				<td><br/><br/></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="45%" style="text-align:left;"></td>
					<td width="55%" style="text-align:left;">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> (ผู้ได้รับสินเชื่อ)</td>
				</tr>
				<tr>
					<td width="45%" style="text-align:left;"></td>
					<td width="55%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>)</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
				<tr>
					<td width="45%" style="text-align:left;"></td>
					<td width="55%" style="text-align:left;">ลงชื่อ <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> (ผู้ค้ำประกัน/จำนอง/จำนำ)</td>
				</tr>
				<tr>
					<td width="45%" style="text-align:left;"></td>
					<td width="55%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>)</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
				<tr>
					<td width="45%" style="text-align:left;"></td>
					<td width="55%" style="text-align:left;">ลงชื่อ  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> (ผู้ค้ำประกัน/จำนอง/จำนำ)</td>
				</tr>
				<tr>
					<td width="45%" style="text-align:left;"></td>
					<td width="55%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>)</td>
				</tr>
				<tr>
					<td colspan="2" width="100%"><br/><br/></td>
				</tr>
			</table>
		';
		//echo $htmlcontent;
		$pdf->SetFont('thsarabun', '', 16);
		$pdf->writeHTML($htmlcontent, true, 0, true, true);

		$pdf->Output($filename.'.pdf', 'I');
	}

	public function word($loan_id,$loan_contract_id,$delaydate)
	{
		include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$this->load->helper('loan');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$delay_data = $this->delay_model->get_loan_delay($loan_id,$loan_contract_id,$delaydate);
		$loan_data = $this->delay_model->get_loan_by_id($loan_id);
		$loan_type_data = $this->delay_model->get_loan_type_by_loan_id($loan_id);
		$loan_contract_data = $this->delay_model->get_loan_contract_by_loan_id($loan_id);

		$person_data = $this->person_model->getById($loan_data['person_id']);
		$person_title_list = $this->mst_model->get_by_id('title',$person_data['title_id']);
		$person_data['title_name'] = $person_title_list['title_name'];
		$person_data['person_age'] = calculate_age($person_data['person_birthdate']);
		list($y, $m, $d) = explode('-', $person_data['person_birthdate']);
		$person_data['person_birthdate'] = $d.'/'.$m.'/'.(intval($y)+543);
		$sex_list = $this->mst_model->get_all('sex');
		$person_race_list = $this->mst_model->get_by_id('race',$person_data['race_id']);
		$person_data['race_name'] = $person_race_list['race_name'];
		$person_nationality_list = $this->mst_model->get_by_id('nationality',$person_data['nationality_id']);
		$person_data['nationality_name'] = $person_nationality_list['nationality_name'];
		$person_religion_list = $this->mst_model->get_by_id('religion',$person_data['religion_id']);
		$person_data['religion_name'] = $person_religion_list['religion_name'];
		list($y, $m, $d) = explode('-', $person_data['person_card_startdate']);
		$person_data['person_card_startdate'] = $d.'/'.$m.'/'.(intval($y)+543);
		list($y, $m, $d) = explode('-', $person_data['person_card_expiredate']);
		$person_data['person_card_expiredate'] = $d.'/'.$m.'/'.(intval($y)+543);
		$card_addr_district = $this->mst_model->get_table_location_by_id('district',$person_data['person_addr_card_district_id']);
		$person_data['person_addr_card_district_name'] = $card_addr_district['district_name'];
		$card_addr_amphur = $this->mst_model->get_table_location_by_id('amphur',$person_data['person_addr_card_amphur_id']);
		$person_data['person_addr_card_amphur_name'] = $card_addr_amphur['amphur_name'];
		$card_addr_province = $this->mst_model->get_table_location_by_id('province',$person_data['person_addr_card_province_id']);
		$person_data['person_addr_card_province_name'] = $card_addr_province['province_name'];
		$pre_addr_district = $this->mst_model->get_table_location_by_id('district',$person_data['person_addr_pre_district_id']);
		$person_data['person_addr_pre_district_name'] = $pre_addr_district['district_name'];
		$pre_addr_amphur = $this->mst_model->get_table_location_by_id('amphur',$person_data['person_addr_pre_amphur_id']);
		$person_data['person_addr_pre_amphur_name'] = $pre_addr_amphur['amphur_name'];
		$pre_addr_province = $this->mst_model->get_table_location_by_id('province',$person_data['person_addr_pre_province_id']);
		$person_data['person_addr_pre_province_name'] = $pre_addr_province['province_name'];

		$person_sex_list = $person_data['sex_id'] == '1' ? '( √ ) ชาย ' : '( ) ชาย';
        $person_sex_list .= $person_data['sex_id'] == '2' ? '( √ ) หญิง' : '( ) หญิง';

        $GLOBALS = null;
        $GLOBALS['delaylocation'] = num2Thai($delay_data[0]['delaylocation']);
		$GLOBALS['delaywritedate'] = num2Thai(thai_display_date($delay_data[0]['delaywritedate']));
		$GLOBALS['person_name'] = $person_data['title_name']. $person_data['person_fname'].' '.$person_data['person_lname'];
		$GLOBALS['person_birthdate'] = num2Thai($person_data['person_birthdate']);
		$GLOBALS['person_age'] = num2Thai($person_data['person_age']);
		$GLOBALS['person_sex_list'] = $person_sex_list;
		$GLOBALS['person_race'] = $person_data['race_name'];
		$GLOBALS['person_nationality'] = $person_data['nationality_name'];
		$GLOBALS['person_religion'] = $person_data['religion_name'];
		$GLOBALS['person_thaiid'] = num2Thai(formatThaiid($person_data['person_thaiid']));
		$GLOBALS['person_card_startdate'] = num2Thai($person_data['person_card_startdate']);
		$GLOBALS['person_card_expiredate'] = num2Thai($person_data['person_card_expiredate']);
		$GLOBALS['person_addr_card_no'] = num2Thai($person_data['person_addr_card_no']);
		$GLOBALS['person_addr_card_moo'] = num2Thai($person_data['person_addr_card_moo']);
		$GLOBALS['person_addr_card_district_name'] = $person_data['person_addr_card_district_name'];
		$GLOBALS['person_addr_card_amphur_name'] = $person_data['person_addr_card_amphur_name'];
		$GLOBALS['person_addr_card_province_name'] = $person_data['person_addr_card_province_name'];
		$GLOBALS['person_addr_pre_no'] = num2Thai($person_data['person_addr_pre_no']);
		$GLOBALS['person_addr_pre_moo'] = num2Thai($person_data['person_addr_pre_moo']);
		$GLOBALS['person_addr_pre_district_name'] = $person_data['person_addr_pre_district_name'];
		$GLOBALS['person_addr_pre_amphur_name'] = $person_data['person_addr_pre_amphur_name'];
		$GLOBALS['person_addr_pre_province_name'] = $person_data['person_addr_pre_province_name'];
		$GLOBALS['person_phone'] = num2Thai($person_data['person_phone']);
		$GLOBALS['person_mobile'] = num2Thai($person_data['person_mobile']);
		$GLOBALS['person_email'] = @$person_data['person_email'] ? $person_data['person_email'] :'';
		$GLOBALS['delaydate'] = num2Thai(thai_display_date($delay_data[0]['delaydate']));
		$GLOBALS['loan_amount'] = num2Thai(number_format($delay_data[0]['loan_amount'],2));
		$GLOBALS['loan_amount_text'] = num2wordsThai($delay_data[0]['loan_amount']);
		$GLOBALS['loan_objective_name'] = $loan_type_data['loan_objective_name'];
		$GLOBALS['loan_contract_date'] = num2Thai(thai_display_date($loan_contract_data['loan_contract_date']));
		$GLOBALS['loan_contract_amount'] = num2Thai(number_format($loan_contract_data['loan_contract_amount'],2));
		$GLOBALS['loan_amont_pay'] = num2Thai(number_format($delay_data[0]['princle_pay']+$delay_data[0]['interest_pay'],2));
		$GLOBALS['princle_pay'] = num2Thai(number_format($delay_data[0]['princle_pay'],2));
		$GLOBALS['interest_pay'] = num2Thai(number_format($delay_data[0]['interest_pay'],2));
		$GLOBALS['behind_number'] = num2Thai(number_format($delay_data[0]['behind_number']));
		$GLOBALS['behind_money'] = num2Thai(number_format($delay_data[0]['behind_money'],2));
		$GLOBALS['nopay_reason'] = $delay_data[0]['nopay_reason'];
		$GLOBALS['delay_period'] = num2Thai(number_format($delay_data[0]['delay_period']));
		$GLOBALS['delay_amt'] = num2Thai(number_format($delay_data[0]['delay_period'],2));

		$template = 'word/template/12_delay.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		// Delete comments
		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'delay_'.date('Y-m-d').'.docx';
		$temp_file = tempnam(sys_get_temp_dir(), 'Docx');
	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);
	}


	public function send_download($temp_file,$file) {
	      $basename = basename($file);
	      $length   = sprintf("%u", filesize($temp_file));

	      header('Content-Description: File Transfer');
	      header('Content-Type: application/octet-stream');
	      header('Content-Disposition: attachment; filename="' . $basename . '"');
	      header('Content-Transfer-Encoding: binary');
	      header('Connection: Keep-Alive');
	      header('Expires: 0');
	      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	      header('Pragma: public');
	      header('Content-Length: ' . $length);
	      ob_clean();
	      flush();
	      set_time_limit(0);
	      readfile($temp_file);
	      exit();
	  }
}
