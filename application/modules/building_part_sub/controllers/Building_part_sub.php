<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_part_sub extends MY_Controller
{
	private $title_page_part = 'ส่วนต่างๆ ของสิ่งปลูกสร้าง';
	private $title_page = 'ชนิดของส่วนต่างๆ ของสิ่งปลูกสร้าง';	
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('building_part_sub_model');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page_part => site_url('building_part'),
				$this->title_page => site_url('building_part_sub')	
			);
		
		$data_content = array(
				'result' => $this->building_part_sub_model->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_building_part_sub/building_part_sub.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('building_part_sub', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page_part => site_url('building_part'),
				$this->title_page => site_url('building_part_sub')				
			);			
			
		$this->load->model('building_part/building_part_model');
		$data_content['building_part'] = $this->building_part_model->all();
		
		if($id=='')
		{
			$title_page = 'เพิ่ม'.$this->title_page;
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('building_part_sub_model');
			$data_content['data'] = $this->building_part_sub_model->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่ม'.$this->title_page;
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไข'.$this->title_page;
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_title/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('building_part_sub_model');
		
		$array['building_part_id'] = $this->input->post('ddlBuildingPartId');
		$array['building_part_sub_desc'] = $this->input->post('txtBuildingPartSubDesc');
		$array['building_part_sub_status'] = '1';
		
		$id = $this->building_part_sub_model->save($array, $this->input->post('txtBuildingPartSubId'));
		
		if($this->input->post('txtBuildingPartSubId')=='')
		{
			$data = array(
					'alert' => 'บันทึก'.$this->title_page.'เรียบร้อยแล้ว',
					'link' => site_url('building_part_sub'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไข'.$this->title_page.'เรียบร้อยแล้ว',
					'link' => site_url('building_part_sub'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('building_part_sub_model');
		$this->building_part_sub_model->del($id);
		
		$data = array(
				'alert' => 'ยกเลิก'.$this->title_page.'เรียบร้อยแล้ว',
				'link' => site_url('building_part_sub'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['building_part_id'] = '';
		$data['building_part_sub_desc'] = '';
		
		return $data;
	}
}