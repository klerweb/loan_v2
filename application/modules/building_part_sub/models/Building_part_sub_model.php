<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_part_sub_model extends CI_Model
{
	private $table = 'config_building_part_sub';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("building_part_sub_status", "1");
		$this->db->order_by("building_part_sub_desc", "asc");
		$query = $this->db->get();
		
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function getByPartId($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("building_part_id", $id);
		$this->db->where("building_part_sub_status", "1");
		$this->db->order_by("building_part_sub_desc", "asc");
		$query = $this->db->get();
		
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("building_part_sub_id", $id);
		$this->db->where("building_part_sub_status", "1");
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function save($array, $id='')
	{
		if($id=='')
		{
			$this->db->set($array);
			$this->db->set("building_part_sub_createdate", "NOW()", FALSE);
			$this->db->set("building_part_sub_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
			
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("building_part_sub_updatedate", "NOW()", FALSE);
			$this->db->where("building_part_sub_id", $id);
			$this->db->update($this->table, $array);
		}
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("building_part_sub_status" => "0");
	
		$this->db->set($array);
		$this->db->where("building_part_sub_id", $id);
		$this->db->update($this->table, $array);
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>