<div class="table-responsive">
	<table class="table table-striped mb30">
		<thead>
			<tr>	
            	<th>ลำดับที่</th>
            	<th>ชื่อ-สกุล</th>
            	<th>ที่อยู่</th>
            	<th>เลขที่สัญญา</th>
                <th>วัตถุประสงค์</th>
                <th>จำนวนเงินกู้</th>
                <th>จำนวนเงินกู้คงเหลือ</th>
                <th>สถานะสัญญา</th>
                <th>สถานะของผู้กู้</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if(!empty($result))
			{
				$i = 1;
				foreach ($result as $row)
				{
					switch ($row['loan_status']) {
						case '1': $status = 'กำลังดำเนินการ';break;
						case '2': $status = 'อนุมัติ';break;
						case '3': $status = 'ไม่อนุมัติ';break;
						case '4': $status = 'ปิดสัญญา';break;
						case '5': $status = 'ปรับปรุงโครงสร้างสัญญา';break;
						default: $status = '-';break;
					}
		?>
			<tr>
				<td><?php echo ($i++) ?></td>
				<td><?php echo $row['title_name'].$row['person_fname'].' '.$row['person_lname']; ?></td>
				<td><?php echo show_address('person_addr_card',$row);?></td>
				<td><?php echo @$row['loan_contract_no']; ?></td>	
				<td><?php echo @$row['loan_objective_name'];?></td>			
				<td><?php echo number_format(@$row['loan_contract_amount'],2); ?></td>
				<td><?php echo number_format(@$row['loan_amount'],2)?></td>
				<td><?php echo $status;?></td>
				<td>ผู้กู้หลัก</td>
           	</tr>
		<?php
				}	
			} 
		?>
		</tbody>
	</table>
</div><!-- table-responsive -->