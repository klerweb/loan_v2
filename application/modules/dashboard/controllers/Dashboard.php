<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller 
{
	private $title = 'หน้าหลัก';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('list_model');
	}
	
	public function index()
	{
		
		$data_menu['menu'] = 'dashboard';
		$data_breadcrumb['menu'] = array($this->title =>'#');
		
		$data_content = array(
			'result' => $this->list_model->get_list_loan()	
		);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('list', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
}
