<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_model extends CI_Model
{	
	private $table = 'loan';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	function get_list_loan()
	{		
		$this->db->select("loan.loan_id,loan.loan_status,config_title.title_name,person.*,
			loan_contract.loan_contract_no,master_loan_objective.loan_objective_name,
			loan_contract.loan_contract_amount,loan_balance.loan_amount,
			(select province_name as person_addr_card_province_name from master_province where province_id=person.person_addr_card_province_id),
			(select amphur_name as person_addr_card_amphur_name from master_amphur where amphur_id=person.person_addr_card_amphur_id),
			(select district_name as person_addr_card_district_name from master_district where district_id=person.person_addr_card_district_id)");
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan_type", "loan_type.loan_id = loan.loan_id", "LEFT");
		$this->db->join("master_loan_objective", "master_loan_objective.loan_objective_id = loan_type.loan_objective_id", "LEFT");
		$this->db->join("loan_contract", "loan_contract.loan_id = loan.loan_id", "LEFT");
		$this->db->join("loan_balance", "loan_balance.loan_contract_id = loan_contract.loan_contract_id", "LEFT");	
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		//$this->db->where_in('loan_contract.loan_contract_status', array('1','2'));
	 	if(check_permission_isowner('loan')){
	   		$this->db->where("loan.loan_createby", get_uid_login());   
	    }
		$this->db->order_by("loan.loan_id", "asc");
		$query = $this->db->get();
		//echo $this->db->last_query();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
		
	function get_loan_co_by_loanid($id)
	{
		$this->db->select("config_title.title_name,person.person_fname,person.person_lname");
        $this->db->from('loan_co');
        $this->db->join("person", "loan_guarantee_bondsman.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
        $this->db->where('loan_co.loan_id', $id);
        $this->db->where('loan_co.loan_co_status', '1');
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
	
	function get_loan_guarantee_bondsman_by_loanid($id)
	{
		$this->db->select("config_title.title_name,person.person_fname,person.person_lname");
        $this->db->from('loan_guarantee_bondsman');
        $this->db->join("person", "loan_guarantee_bondsman.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
        $this->db->where('loan_guarantee_bondsman.loan_id', $id);
        $this->db->where('loan_guarantee_bondsman.loan_bondsman_status', '1');
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
	
	
}