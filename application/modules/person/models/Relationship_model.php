<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relationship_model extends CI_Model
{
	private $table = 'config_relationship';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("relationship_status", "1");
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>