<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person_model extends CI_Model
{
	private $table = 'person';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("person_status", "1");
		
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function search($thaiid='')
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->like("person_thaiid", $thaiid);
		$this->db->where("person_status", "1");
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("person_id", $id);
		//$this->db->where("person_status", "1");
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	private function getMaxId()
	{
		$this->db->select_max("person_id");
		$query = $this->db->get($this->table);
	
		return $query->num_rows()!=0? $query->row_array() : null;
	}
	
	public function save($array, $id='')
	{
		if($id=='')
		{
			$array['person_status'] = '1';
			$this->db->set($array);
			$this->db->set("person_createdate", "NOW()", FALSE);
			$this->db->set("person_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("person_updatedate", "NOW()", FALSE);
			$this->db->where("person_id", $id);
			$this->db->update($this->table, $array);
		}
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("person_status" => "0");
	
		$this->db->set($array);
		$this->db->where("person_id", $id);
		$this->db->update($this->table, $array);
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>