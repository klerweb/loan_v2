<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Non_loan_objective extends MY_Controller
{
	private $title_page = 'วัตถุประสงค์ (กรณีไม่รับคำขอ)';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('non_loan_objective_model', 'non_loan_objective');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->non_loan_objective->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_non_loan_objective/non_loan_objective.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('non_loan_objective', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('non_loan_objective')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มวัตถุประสงค์ (กรณีไม่รับคำขอ)';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('non_loan_objective_model', 'non_loan_objective');
			$data_content['data'] = $this->non_loan_objective->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มวัตถุประสงค์ (กรณีไม่รับคำขอ)';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขวัตถุประสงค์ (กรณีไม่รับคำขอ)';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_non_loan_objective/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('non_loan_objective_model', 'non_loan_objective');
		
		$array['non_loan_objective_name'] = $this->input->post('txtNonLoanObjectiveName');
		$array['non_loan_objective_status'] = '1';
		
		$id = $this->non_loan_objective->save($array, $this->input->post('txtNonLoanObjectiveId'));
		
		if($this->input->post('txtNonLoanObjectiveId')=='')
		{
			$data = array(
					'alert' => 'บันทึกวัตถุประสงค์ (กรณีไม่รับคำขอ) เรียบร้อยแล้ว',
					'link' => site_url('non_loan_objective'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขวัตถุประสงค์ (กรณีไม่รับคำขอ) เรียบร้อยแล้ว',
					'link' => site_url('non_loan_objective'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('non_loan_objective_model', 'non_loan_objective');
		$this->non_loan_objective->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกวัตถุประสงค์ (กรณีไม่รับคำขอ) เรียบร้อยแล้ว',
				'link' => site_url('non_loan_objective'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['non_loan_objective_name'] = '';
		
		return $data;
	}
}