<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller 
{
	
	function __construct()
	{
		parent::__construct();		
		$this->load->model('home_model');
	}
	
	public function index()
	{
		$this->parser->parse('login', array());
	}
	
	public function authen()
	{
		$user_name = $_POST['Utxt'];
		$user_pass = $_POST['Ptxt'];
		if($user_name && $user_pass){
			$result = $this->home_model->get_User_Login($user_name,$user_pass);
			if (!empty($result)) {
				$user_role = $this->home_model->get_User_Role($result['user_id']);
				if(!empty($user_role)){
					$role_id = $user_role[0]['role_id'];
					$role_name = $user_role[0]['role_name'];
					$role_module = array();
					$user_module = $this->home_model->get_Module_Role($role_id);
					if(!empty($user_module)){
						foreach ($user_module as $module) {
							$role_module[$module['module_name']] = array(
								'isread'=>$module['module_isread'],
								'iswrite'=>$module['module_iswrite'],
								'isedit'=>$module['module_isedit'],
								'isdelete'=>$module['module_isdelete'],
								'isowner'=>$module['module_isowner']
							);
						}
					}
				}			
				
	            $session_x = array(
		        	'u_id' => $result['user_id'],
					'u_fullname' => $result['user_fname'].' '.$result['user_lname'],
	            	'u_role' => @$role_name,
	            	'u_module' => @$role_module
		        );
				$this->session->set_userdata('session_x', $session_x);	
	            echo json_encode(array('status'=>true,'text'=> base_url().'dashboard' ));    	
				
			}else{
				echo json_encode(array('status'=>false,'text'=> 'ไม่พบข้อมูล'));   
			}
		}else{
			echo json_encode(array('status'=>false,'text'=> 'กรุณากรอกข้อมูลให้ถูกต้อง'));   
		}
	}
	
	public function logout()
	{
		$this->session->unset_userdata('session_x');
		redirect_url(base_url(),'ออกจากระบบ');
	}
}
