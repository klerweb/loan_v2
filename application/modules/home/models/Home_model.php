<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function get_User_Login($user,$pass)
	{
		$this->db->select("*");
		$this->db->from("sys_user");
		$this->db->where("user_name", $user);
		$this->db->where("user_pass", $pass);
		$this->db->where("user_status", '1');
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->row_array() : null;
		return $result;
	}
	
	public function get_User_Role($user_id)
	{		
		$this->db->select("sys_user_role.role_id,sys_role.role_name ");
		$this->db->from("sys_user_role");
		$this->db->join("sys_role", "sys_user_role.role_id = sys_role.role_id ", "LEFT");		
		$this->db->where('sys_user_role.user_id', $user_id);	
		$this->db->where('sys_role.role_status', '1');
		$this->db->order_by("sys_user_role.role_id", "asc");
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : null;
		return $result;
	}
	
	public function get_Module_Role($role_id)
	{
		$this->db->select("sys_role_module.module_id,sys_module.module_name,sys_role_module.module_isread,sys_role_module.module_iswrite,sys_role_module.module_isedit,sys_role_module.module_isdelete,sys_role_module.module_isowner");
		$this->db->from("sys_role_module");
		$this->db->join("sys_module", "sys_role_module.module_id = sys_module.module_id", "LEFT");		
		$this->db->where('sys_role_module.role_id', $role_id);	
		$this->db->where('sys_module.module_status', '1');
		$this->db->order_by("sys_role_module.module_id", "asc");
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : null;
		return $result;
	}
}
?>