<p class="nomargin" style="margin-bottom:20px; text-align:right;">
	<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo base_url()?>user/form'">เพิ่มข้อมูล</button>
</p>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-btns">
			<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
		</div><!-- panel-btns -->
		<h4 class="panel-title">ค้นหา<?php echo $title;?></h4>
	</div>
	<div class="panel-body">
		<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo base_url()?>user">
			<div class="form-group">
				<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
				<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
			</div><!-- form-group -->
			<button type="submit" class="btn btn-default btn-sm">ค้นหา</button>
		</form>
	</div><!-- panel-body -->
</div><!-- panel -->
<div class="table-responsive">
	<table class="table table-striped mb30">
		<thead>
			<tr>
            	<th>#</th>
                <th>Username</th>
                <th>ชื่อ-สกุล</th>
                <th>อีเมลล์</th>
                <th>เบอร์โทร</th>
                <th>สถานะ</th>
                <th></th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if(!empty($result))
			{
				$i = 1;
				foreach ($result as $row)
				{
		?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $row['user_name']; ?></td>
				<td><?php echo $row['user_fname'].' '.$row['user_lname']; ?></td>
				<td><?php echo $row['user_email']; ?></td>
				<td><?php echo $row['user_mobile']; ?></td>
				<td><?php echo $row['user_status'] == '1' ? 'Active' : 'Cancel'; ?></td>
           		<td><a href="<?php echo base_url().'user/form/'.$row['user_id']?>" data-toggle="tooltip" title="Edit" class="delete-row tooltips" data-original-title="Edit"><i class="fa fa-edit"></i></a></td>
           	</tr>
		<?php
				}	
			} 
		?>
		</tbody>
	</table>
</div><!-- table-responsive -->