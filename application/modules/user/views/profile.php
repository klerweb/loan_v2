<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo $title_page;?></h4>
	</div>
	<div class="panel-body">
    <form id="frm1" name="frm1" method="post" action="#" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">Username</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo @$result['user_name'];?>" readonly="readonly">
		</div>  
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">ชื่อ</label>
	    </div>
		<div class="col-sm-3 form-group">
	    	<input type="text" name="user_fname" id="user_fname" class="form-control" value="<?php echo @$result['user_fname'];?>" readonly="readonly">
		</div>  
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">นามสกุล</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_lname" id="user_lname" class="form-control" value="<?php echo @$result['user_lname'];?>" readonly="readonly">
		</div>                                                                            
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">อีเมลล์</label>
	    </div>    
		<div class="col-sm-3 form-group">
	    	<input type="text" name="user_email" id="user_email" class="form-control" value="<?php echo @$result['user_email'];?>" readonly="readonly">
		</div>                                             
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">เบอร์โทร</label>
	    </div>   
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_mobile" id="user_mobile" class="form-control" value="<?php echo @$result['user_mobile'];?>" readonly="readonly">
		</div>                                          
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">สถานะ</label>
	    </div>
	    <div class="col-sm-3 form-group">
	        	<div class="row" >
	            	<div class="col-sm-6">
	            		<input type="radio" id="user_status1" name="user_status" value="1" <?php echo @$result['user_status']!='0' ? 'checked="checked"' : ''?> readonly="readonly">
		            	<label>Active</label>
	                </div>
	                <div class="col-sm-6">
	            		<input type="radio" id="user_status0" name="user_status" value="0" <?php echo @$result['user_status']=='0' ? 'checked="checked"' : ''?> readonly="readonly">
		            	<label>Cancel</label>
	                </div>
	       		</div>
		 </div>
	 </div>
	 <div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">สิทธิการเข้าใช้งาน</label>
	    </div>
	    <div class="col-sm-3 form-group">
	        	<div class="row" >
	            	<div class="col-sm-12">
	            		<select class="form-control" name="user_role[]" readonly="readonly">
	            		<?php 
	            		if (!empty($role)){
	            			foreach ($role as $row) {
	            				if($row['role_status'] == '1'){
	            					$selected = '';
	            					if (!empty($user_role)){
	            						foreach ($user_role as $row2) {
	            							if($row['role_id'] == $row2['role_id'])$selected = 'selected';
	            						}
	            					}
	            					echo '<option value='.$row['role_id'].' '.$selected.'>'.$row['role_name'].'</option>';
	            				}
	            			}
	            		}
	            		?>
                        </select>
	                </div>
	       		</div>
		 </div>
	 </div>
	</form>
	</div><!-- panel-body -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>
