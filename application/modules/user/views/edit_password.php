<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo $title_page?></h4>
	</div>
	<div class="panel-body">
    <form id="frm1" name="frm1" method="post" action="<?php echo base_url().'user/save_password'?>" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">Username</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo @$result['user_name'];?>" <?php echo $id ? 'disabled':'';?>>
		</div>  
	</div>
	 <div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">New Password</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="password" name="user_pass" id="user_pass" class="form-control" value="" required>
		</div>  
	</div>
	 <input type="hidden" name="user_id" value="<?php echo @$id;?>">
	</form>
	</div><!-- panel-body -->
	<div class="panel-footer right">
         <button id="btnSubmit" class="btn btn-primary">บันทึก</button>
	</div><!-- panel-footer -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>
<script>
        jQuery(document).ready(function () {     

             
        	$("#frm1").validate({
        		highlight: function(element) {
        			$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        		},
        		unhighlight: function(element) {
        	        $(element).closest('.form-group').removeClass('has-error');
        	    },
        		errorElement: 'span',
        	    errorClass: 'help-block',
        	    errorPlacement: function(error, element) {
        	        if(element.parent('.input-group').length) {
        	            error.insertAfter(element.parent());
        	        } else {
        	            error.insertAfter(element);
        	        }
        	    },
        		success: function(element) {
        			$(element).closest(".form-group").removeClass("has-error");
        		}		
        	});

        	jQuery('#btnSubmit').click(function (e) {
            	$('#frm1').submit();
            });
        });
</script>
