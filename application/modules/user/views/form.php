<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo !empty($id) ? 'แก้ไข' : 'เพิ่ม';?>ข้อมูล</h4>
	</div>
	<div class="panel-body">
    <form id="frm1" name="frm1" method="post" action="<?php echo base_url().'user/save'?>" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">Username</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo @$result['user_name'];?>" <?php echo $id ? 'disabled':'';?>>
		</div>  
	</div>
	 <div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">Password</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="password" name="user_pass" id="user_pass" class="form-control" value="<?php echo @$result['user_pass'];?>" required>
		</div>  
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">ชื่อ</label>
	    </div>
		<div class="col-sm-3 form-group">
	    	<input type="text" name="user_fname" id="user_fname" class="form-control" value="<?php echo @$result['user_fname'];?>" required>
		</div>  
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">นามสกุล</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_lname" id="user_lname" class="form-control" value="<?php echo @$result['user_lname'];?>" required>
		</div>                                                                            
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">อีเมลล์</label>
	    </div>    
		<div class="col-sm-3 form-group">
	    	<input type="text" name="user_email" id="user_email" class="form-control" value="<?php echo @$result['user_email'];?>" required>
		</div>                                             
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">เบอร์โทร</label>
	    </div>   
		<div class="col-sm-3 form-group">
	        <input type="text" name="user_mobile" id="user_mobile" class="form-control" value="<?php echo @$result['user_mobile'];?>" required>
		</div>                                          
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">สถานะ</label>
	    </div>
	    <div class="col-sm-3 form-group">
	        	<div class="row" >
	            	<div class="col-sm-6">
	            		<input type="radio" id="user_status1" name="user_status" value="1" <?php echo @$result['user_status']!='0' ? 'checked="checked"' : ''?>>
		            	<label>Active</label>
	                </div>
	                <div class="col-sm-6">
	            		<input type="radio" id="user_status0" name="user_status" value="0" <?php echo @$result['user_status']=='0' ? 'checked="checked"' : ''?>>
		            	<label>Cancel</label>
	                </div>
	       		</div>
		 </div>
	 </div>
	 <div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">สิทธิการเข้าใช้งาน</label>
	    </div>
	    <div class="col-sm-3 form-group">
	        	<div class="row" >
	            	<div class="col-sm-12">
	            		<select class="form-control" name="user_role[]" required>
	            		<?php 
	            		if (!empty($role)){
	            			foreach ($role as $row) {
	            				if($row['role_status'] == '1'){
	            					$selected = '';
	            					if (!empty($user_role)){
	            						foreach ($user_role as $row2) {
	            							if($row['role_id'] == $row2['role_id'])$selected = 'selected';
	            						}
	            					}
	            					echo '<option value='.$row['role_id'].' '.$selected.'>'.$row['role_name'].'</option>';
	            				}
	            			}
	            		}
	            		?>
                        </select>
	                </div>
	       		</div>
		 </div>
	 </div>
	 <input type="hidden" name="user_id" value="<?php echo @$id;?>">
	</form>
	</div><!-- panel-body -->
	<div class="panel-footer right">
         <button id="btnSubmit" class="btn btn-primary">บันทึก</button>
	</div><!-- panel-footer -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>
<script>
        jQuery(document).ready(function () {     

        	var response;
            $.validator.addMethod(
                "check_dup", 
                function(value, element) {
                    $.ajax({
                        type: "POST",
                        url: base_url+"user/check_dup",
                        data: "user_name="+value,
                        dataType:"html",
                        success: function(msg)
                        {
                            response = ( msg == true ) ? true : false;
                        }
                     });
                    return response;
                },
                "Username is Already Taken"
            );
             
        	$("#frm1").validate({
        		rules: {
        	        "user_email":{
        				email:true,
        	        	required: true
        	        },
        	        "user_name":{
        	        	//check_dup:true,
        	        	required: true
        	        },
        		},
        		highlight: function(element) {
        			$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        		},
        		unhighlight: function(element) {
        	        $(element).closest('.form-group').removeClass('has-error');
        	    },
        		errorElement: 'span',
        	    errorClass: 'help-block',
        	    errorPlacement: function(error, element) {
        	        if(element.parent('.input-group').length) {
        	            error.insertAfter(element.parent());
        	        } else {
        	            error.insertAfter(element);
        	        }
        	    },
        		success: function(element) {
        			$(element).closest(".form-group").removeClass("has-error");
        		}		
        	});

        	jQuery('#btnSubmit').click(function (e) {
            	$('#frm1').submit();
            });
        });
</script>
