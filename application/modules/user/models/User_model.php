<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	private $table = 'sys_user';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function search($fullname='')
	{
		$this->db->select("*");
		$this->db->from($this->table);		
		if($fullname!='') $this->db->where("(user_fname LIKE '%".$fullname."%' OR user_lname LIKE '%".$fullname."%')", NULL, FALSE);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function check_dup($user_name='')
	{
		$this->db->select("*");
		$this->db->from($this->table);		
		$this->db->where('user_name', $user_name);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function get_table_by_id($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("user_id", $id);
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->row_array() : null;
		return $result;
	}
	
	public function save($array, $id='')
	{
		unset($array['user_id']);
		if($id=='')
		{
			$array['user_createdate'] = date($this->config->item('log_date_format'));
			$array['user_createby'] = get_uid_login();
    
			$this->db->set($array);
			$this->db->insert($this->table);
			$id = $this->db->insert_id();
		}
		else
		{			
			unset($array['user_name']);
			$array['user_updatedate'] = date($this->config->item('log_date_format'));
			$array['user_updateby'] = get_uid_login();
			$this->db->set($array);
			$this->db->where("user_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>