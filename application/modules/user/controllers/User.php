<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller 
{
	private $title = 'พนักงาน';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('role/role_model');
		$this->load->model('role/user_role_model');
	}
	
	public function index()
	{
		$fullname = '';
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_menu['menu'] = 'user';
		$data_breadcrumb['menu'] = array('ผู้ใช้งาน' =>'#');
		
		$data_content = array(
				'title' => $this->title,
				'fullname' => $fullname,
				'result' => $this->user_model->search($fullname)
		);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('user', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'user';
		$data_breadcrumb['menu'] = array('ผู้ใช้งาน' =>'#',$this->title => site_url('user'));
		
		$data_content = array(
				'id' => $id,
				'result' => $id ?  $this->user_model->get_table_by_id($id) : null,
				'role' => $this->role_model->search(),
				'user_role' => $id ? $this->user_role_model->search($id) : null
			);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$user_role = $_POST['user_role'];		
		unset($_POST['user_role']);
		$result = $this->user_model->save($_POST,$_POST['user_id']);		
		if(!empty($user_role))
		{
			$user_id = $result['id'];
			$this->user_role_model->delete_by_userid($user_id);
			foreach ($user_role as $row) {
				$data['user_id'] = $user_id;
				$data['role_id'] = $row;
				$resultx = $this->user_role_model->save($data);
			}
		}
		if($result ['rows'] > 0)
		{
			redirect_url(base_url().'user','บันทึกข้อมูลเรียบร้อยแล้ว');
		}else{
			redirect_url(base_url().'user','ไม่สามารถบันทึกข้อมูลได้');
		}
	}
	
	public function check_dup()
	{
		$result = $this->user_model->check_dup($_POST['user_name']);
		if(!empty($result)){
			echo false;
		}else{
			echo true;
		}
	}
	
	public function profile()
	{
		$id = get_uid_login();
		$title = 'My Profile';
		
		$data_menu['menu'] = '';
		$data_breadcrumb['menu'] = array($title => site_url('user/profile'));
					
		$data_content = array(
				'id' => $id,
				'title_page' => $title,
				'result' => $id ?  $this->user_model->get_table_by_id($id) : null,
				'role' => $this->role_model->search(),
				'user_role' => $id ? $this->user_role_model->search($id) : null
			);
		
		$data = array(
				'title' => $title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $id ? $this->parser->parse('profile', $data_content, TRUE) : null
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function edit_password()
	{
		$id = get_uid_login();
		$title = 'Edit Password';
		
		$data_menu['menu'] = '';
		$data_breadcrumb['menu'] = array($title => site_url('user/edit_password'));
					
		$data_content = array(
				'id' => $id,
				'title_page' => $title,
				'result' => $id ?  $this->user_model->get_table_by_id($id) : null
			);
		
		$data = array(
				'title' => $title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $id ? $this->parser->parse('edit_password', $data_content, TRUE) : null
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function save_password()
	{
		$result = $this->user_model->save($_POST,$_POST['user_id']);			
		if($result['rows'] > 0)
		{
			$this->session->unset_userdata('session_x');
			redirect_url(base_url(),'บันทึกข้อมูลเรียบร้อยแล้ว กรุณาล๊อกอินใหม่อีกครั้ง');
		}else{
			redirect_url(base_url().'user/edit_password','ไม่สามารถบันทึกข้อมูลได้');
		}
	}
	
}
