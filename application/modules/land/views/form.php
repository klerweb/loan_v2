 <?=form_open_multipart('land/land_save',array('id'=>'frm_land','enctype'=>'multipart/form-data'));?>
<input type="hidden" name="loan_id" value="<?php echo $loan_data['loan_id']; ?>"/>
<input type="hidden" name="land_id" value="<?php echo $land_data['land_id']; ?>"/>
<input type="hidden" name="person_id" value="<?php echo $loan_data['person_id']; ?>"/>
<input type="hidden" name="del_owner" id="del_owner" value=""/>
<input type="hidden" name="tabs" value=""/>

<div class="row">
	<div class="col-sm-12">
		<h4 class="panel-title">แบบบันทึกการตรวจสอบที่ดินที่จะจำนองเป็นหลักประกันการชำระหนี้ของผู้ขอสินเชื่อ</h4>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
	<!-- Nav tabs -->
<?php 
	if($this->session->flashdata('message')){
		
?>		<div class="row">
			<div class="col-sm-12">
			<div class="alert alert-success">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong><?php echo $this->session->flashdata('message'); ?></strong>
			</div>
			</div>
		</div>
<?php 
	}
?>
			<ul class="nav nav-tabs nav-justified" id="tabs_land">
			  <li class="<?php echo $tabs_action[0];?>" tabindex="0">
				<a data-toggle="tab" href="#land_owner_info" role="tab" >ข้อมูลผู้ขอสินเชื่อ</a>
			  </li>
			  <li class="<?php echo $tabs_action[1];?>" tabindex="1">
				<a data-toggle="tab" href="#land_info" role="tab">ข้อมูลที่ดิน</a>
			  </li>
			  <li class="<?php echo $tabs_action[2];?>" tabindex="2">
				<a data-toggle="tab" href="#land_location" role="tab">ข้อมูลที่ตั้ง</a>
			  </li>			  
			  <li class="<?php echo $tabs_action[3];?>" tabindex="3">
				<a data-toggle="tab" href="#inspect" role="tab">ราคาที่ดินขั้นต้น</a>
			  </li>
			  <li class="<?php echo $tabs_action[4];?>" tabindex="4">
				<a  data-toggle="tab" href="#staff_inspect" role="tab">ผู้ตรวจสอบขั้นต้น</a>
			  </li>
			  <li class="<?php echo $tabs_action[5];?>" tabindex="5">
				<a  data-toggle="tab" href="#assign_inspect" role="tab">ผู้ได้รับมอบอำนาจ</a>
			  </li>
			  
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
			  <div class="tab-pane <?php echo $tabs_action[0];?>" id="land_owner_info" role="tabpanel">
				<?=$view_tab1;?>
			  </div>
			  <div class="tab-pane <?php echo $tabs_action[1];?>" id="land_info" role="tabpanel">
			  	<?=$view_tab2;?>
			  </div>
			   <div class="tab-pane <?php echo $tabs_action[2];?>" id="land_location" role="tabpanel">
			  	<?=$view_tab3;?>
			  </div>
			  
			  <div class="tab-pane <?php echo $tabs_action[3];?>" id="inspect" role="tabpanel">
				<?=$view_tab4;?>
			  </div>
			  
			  <div class="tab-pane <?php echo $tabs_action[4];?>" id="staff_inspect" role="tabpanel">
				<?=$view_tab5;?>
			  </div>
			  <div class="tab-pane <?php echo $tabs_action[5];?>" id="assign_inspect" role="tabpanel">
			  	<?=$view_tab6;?>
			  </div>				
			</div>
		</div>
</div>		
<div class="row" style="padding-top:5px;">
	<div class="col-sm-12">
		<div class="form-group text-right">
			<input type="submit" name="btn_land_save"  value="บันทึก" class="btn btn-primary">
		</div>
	</div>
</div>	

<?php
	echo form_close();
?>



