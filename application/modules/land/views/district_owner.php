<select name="ddl_district_owner" id="ddl_district_owner" data-placeholder="กรุณาเลือก" class=" width100p css_req" >
	<option value="">กรุณาเลือก</option>
	<?php foreach ($district as $district_data) { ?>
		<option value="<?php echo $district_data['district_id']; ?>"  <?php if($district_data['district_id']== $selected){ ?> selected="selected" <?php }?>><?php echo $district_data['district_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#ddl_district_owner").select2({
		minimumResultsForSearch: -1
	});
</script>