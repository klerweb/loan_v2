<select name="ddl_amphur" id="ddl_amphur" data-placeholder="กรุณาเลือก" class=" width100p" >
	<option value="">กรุณาเลือก</option>
	<?php foreach ($amphur as $amphur_data) { ?>
	<option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if($amphur_data['amphur_id']==$selected){?> selected="seleted" <?php } ?> ><?php echo $amphur_data['amphur_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#ddl_amphur").select2({
		minimumResultsForSearch: -1
	});

	$("#ddl_amphur").change(function(){
		$("#div_district").load(base_url+"land/district/"+$("#ddl_amphur").val());
	});
</script>