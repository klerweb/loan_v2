<form name="frm_land_owner" id="frm_land_owner" method="post" action="<?php echo site_url(); ?>land/save_owner" enctype="multipart/form-data">
    <input type="hidden" name="person_id" id="person_id" value="<?php echo $person_data['person_id']; ?>"/>
    <input type="hidden" name="owner_id" id="owner_id" value="<?php echo $owner_data['land_owner_id']; ?>"/>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group text-right">
                <a href="<?php echo $close_url; ?>" class="btn btn-default">ปิด</a>
            </div>
        </div>
    </div>
    <?php
    if ($this->session->flashdata('message')) {
        ?>		
    <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong><?php echo $this->session->flashdata('message'); ?></strong>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="row">
        <h4>ผู้ที่มีชื่อในหนังสือแสดงกรรมสิทธิในที่ดิน</h4>
        <hr/>
        <div class="col-sm-4">
            <div class="form-group">
                <label>เลขที่บัตรประชาชน</label>
                <ul class="fa-ul idload hide"><li><i class="fa-li fa fa-spinner fa-spin"></i> Loading..</li></ul>
                <input type="text" name="txt_card_id" id="txt_card_id" class="form-control css_req" value="<?php echo formatThaiid($person_data['person_thaiid']); ?>" placeholder="ค้นหาจากรหัสบัตรประชาชน"  />
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label class="">คำนำหน้าชื่อ</label>
                <select name="ddl_title" id="ddl_title" class="width100p" >
                    <option value=''>กรุณาเลือก</option>
                    <?php
                    foreach ($title_name as $val) {
                        $select = '';
                        if ($val['title_id'] == $person_data['title_id'])
                            $select = 'selected';

                        echo '<option value=' . $val['title_id'] . ' ' . $select . '>  ' . $val['title_name'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label>ชื่อ</label>
                <input type="text" name="txt_owner_name" id="txt_owner_name" class="form-control css_req" maxlength="50" value="<?php echo $person_data['person_fname']; ?>" >
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" name="txt_owner_last_name" id="txt_owner_last_name" class="form-control css_req" maxlength="50" value="<?php echo $person_data['person_lname']; ?>">
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-sm-4">
            <div class="form-group">
                <label class="">วัน เดือน ปี เกิด</label>
                <div class="input-group">

                    <input type="text" name="txt_owner_birthday" id="txt_owner_birthday" class="form-control css_req" value="<?php echo convert_dmy_th($person_data['person_birthdate']); ?>"/>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div><!-- form-group -->
        </div><!-- col-sm-3 -->
        <div class="col-sm-4">
            <div class="form-group">
                <label class="">อายุ</label>
                <div class="input-group">
                    <input type="text" name="txt_owner_age" id="txt_owner_age" class="form-control" value="" readonly />
                    <span class="input-group-addon">ปี</span>
                </div>
            </div><!-- form-group -->
        </div><!-- col-sm-3 -->
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>บ้านเลขที่</label>
                <input type="text" name="txt_owner_addr_no" id="txt_owner_addr_no" class="form-control css_req" value="<?php echo $person_data['person_addr_card_no']; ?>">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>หมู่ที่</label>
                <input type="text" name="txt_owner_addr_moo" id="txt_owner_addr_moo" class="form-control" value="<?php echo $person_data['person_addr_card_moo']; ?>" maxlength="2">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>ถนน</label>
                <input type="text" name="txt_owner_addr_road" id="txt_owner_addr_road" class="form-control" value="<?php echo $person_data['person_addr_card_road']; ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label class="">จังหวัด</label>
                <select name="ddl_owner_province" id="ddl_owner_province" class="width100p css_req" placeholder="กรุณาเลือก">
                    <option value="">กรุณาเลือก</option>
                    <?php
                    foreach ($province_data as $val) {
                        $select = '';
                        if ($val['province_id'] == $person_data['person_addr_card_province_id'])
                            $select = 'selected';

                        echo '<option value=' . $val['province_id'] . ' ' . $select . '>  ' . $val['province_name'] . '</option>';
                    }
                    ?>
                </select>

            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="hidden" name="hidden_owner_amphur" id="hidden_owner_amphur" value="<?php echo $person_data['person_addr_card_amphur_id']; ?>">
                <label class="">อำเภอ</label>
                <div id="div_owner_amphur">						
                    <select name="ddl_amphur_owner" id="ddl_amphur_owner" data-placeholder="กรุณาเลือก" class="width100p css_req" >
                        <option value="">กรุณาเลือก</option>
                    </select>						
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="hidden" name="hidden_owner_district" id="hidden_owner_district" value="<?php echo $person_data['person_addr_card_district_id']; ?>">
                <label class="">ตำบล</label>
                <div id="div_owner_district">
                    <select name="ddl_district_owner" id="ddl_district_owner" data-placeholder="กรุณาเลือก" class="width100p css_req" >
                        <option value="">กรุณาเลือก</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>รหัสไปรษณีย์</label>
                <input type="text" name="txt_owner_zipcode" id="txt_owner_zipcode" value="<?php echo $person_data['person_addr_card_zipcode']; ?>" class="form-control" readonly/>
            </div>
        </div>
    </div>
    <div class="row">
        <hr/>
        <h4>ชื่อบิดา-มารดา ของผู้มีชื่อในหนังสือกรรมสิทธิ</h4>
    </div>

    <div class="row">			
        <div class="col-sm-4">
            <div class="form-group">
                <label>คำนำหน้าชื่อ</label>
                <select name="ddl_title_father" id="ddl_title_father" class="width100p">
                    <option value="">กรุณาเลือก</option>
                    <?php
                    $selected = '1';
                    if (isset($owner_data['land_owner_father_title']))
                        $selected = $owner_data['land_owner_father_title'];

                    foreach ($title_name as $val) {
                        $ftitle = '';
                        if ($val['title_id'] == $selected)
                            $ftitle = 'selected';

                        echo '<option value=' . $val['title_id'] . ' ' . $ftitle . '>' . $val['title_name'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>ชื่อบิดา</label>
                <input type="text" name="txt_owner_fa_fname" id="txt_owner_fa_fname" class="form-control css_req" value="<?php echo $owner_data['land_owner_father_fname']; ?>" maxlength="200">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" name="txt_owner_fa_lname" id="txt_owner_fa_lname" class="form-control css_req" value="<?php echo $owner_data['land_owner_father_lname']; ?>" maxlength="200">
            </div>
        </div>

    </div>
    <div class="row">			
        <div class="col-sm-4">
            <div class="form-group">
                <label>คำนำหน้าชื่อ</label>
                <select name="ddl_title_mather" id="ddl_title_mather" class="width100p">
                    <option value="0">กรุณาเลือก</option>
                    <?php
                    $selected = '2';
                    if (isset($owner_data['land_owner_mather_title']))
                        $selected = $owner_data['land_owner_mather_title'];

                    foreach ($title_name as $val) {
                        $mtitle = '';
                        if ($val['title_id'] == $selected)
                            $mtitle = 'selected';

                        echo '<option value=' . $val['title_id'] . ' ' . $mtitle . '>' . $val['title_name'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>ชื่อมารดา</label>
                <input type="text" name="txt_owner_ma_fname" id="txt_owner_ma_fname" class="form-control css_req" value="<?php echo $owner_data['land_owner_mather_fname']; ?>" maxlength="200" >
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" name="txt_owner_ma_lname" id="txt_owner_ma_lname" class="form-control css_req" value="<?php echo $owner_data['land_owner_mather_lname']; ?>" maxlength="200" >
            </div>
        </div>

    </div>
    <div class="row">
        <hr/>
        <h4>ความเกี่ยวข้องกับผู้ขอสินเชื่อ</h4>
    </div>
    <div class="row">
        <div class="col-sm-4">					
            <div class="form-group">
                <label></label>
                <select name="ddl_relation" id="ddl_relation" data-placeholder="กรุณาเลือก" class="width100p">
                    <option value="0">กรุณาเลือก</option>
                    <?php
                    $default = $owner_data['land_owner_relationship_id'] == '' ? "6" : $owner_data['land_owner_relationship_id'];
                    foreach ($person_relation as $val) {
                        $rselected = '';
                        if ($val['relationship_id'] == $default)
                            $rselected = 'selected';

                        echo '<option value=' . $val['relationship_id'] . '  ' . $rselected . '>' . $val['relationship_name'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>#</th>
                        <th>ชื่อไฟล์</th>
                        <th>จัดการ</th>
                    </tr>
                    <?php if (!empty($owner_data['land_owner_file'])) { 
                        $filesname = explode(',',$owner_data['land_owner_file']);
                        if(count($filesname)>0){
                            
                        foreach($filesname as $file){
                        ?>
                        <tr>
                            <td width="4%"></td>
                            <td><?php echo $file; ?></td>
                            <td width="4%">
                                <a href="<?php echo base_url() . 'assets/files/land_owners/' . $file; ?>" target="_bank"><i class="fa fa-file-text-o" aria-hidden="true"></i></a> &nbsp;
                                <a href="<?php echo base_url() . 'land/delete_files_owner/'.$owner_data['land_owner_id'].'/' .$file; ?>" onclick="return confirm('ยืนยันการลบไฟล์');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>					
                        </tr>
                    <?php }
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>แนบไฟล์  (.GIF,JPG,PNG,DOC,PDF)</label>
                <input type="hidden" name="filesdb" value="<?php echo $owner_data['land_owner_file'];?>"/>
                <input type="file" name="file_owmer[]" id="file_owner" multiple="" >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group text-right">

                <input type="submit" name="btn_save_owner" id="btn_save_owner" value="บันทึก" class="btn btn-primary"/>
                &nbsp;&nbsp;
                <a href="<?php echo $close_url; ?>" class="btn btn-default">ปิด</a>
            </div>
        </div>
    </div>