<select name="ddl_district" id="ddl_district" data-placeholder="กรุณาเลือก" class=" width100p" >
	<option value="">กรุณาเลือก</option>
	<?php foreach ($district as $district_data) { ?>
		<option value="<?php echo $district_data['district_id']; ?>"  <?php if($district_data['district_id']==$selected){?> selected="seleted" <?php }?>><?php echo $district_data['district_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#ddl_district").select2({
		minimumResultsForSearch: -1
	});
</script>