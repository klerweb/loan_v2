<select name="ddl_amphur_owner" id="ddl_amphur_owner" data-placeholder="กรุณาเลือก" class=" width100p css_req" >
	<option value="">กรุณาเลือก</option>
	<?php foreach ($amphur as $amphur_data) { ?>
	<option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if($amphur_data['amphur_id']== $selected){ ?> selected="selected" <?php }?>><?php echo $amphur_data['amphur_name']; ?></option>
	<?php } ?>
</select>

<script type="text/javascript">
	$("#ddl_amphur_owner").select2({
		minimumResultsForSearch: -1
	});

	$("#ddl_amphur_owner").change(function(){
		$("#div_owner_district").load(base_url+"land/district_owner/"+$("#ddl_amphur_owner").val());
	});
        
               
</script>