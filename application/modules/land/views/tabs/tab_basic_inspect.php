<div class="row">
    <h4>11. การประเมินราคาที่ดินขั้นต้น</h4>
    <div class="col-sm-12">

        <table class="table table-bordered" id="table_price">
            <thead>
                <tr>
                    <th>#</th>
                    <th colspan="2" class="text-center">บริเวณที่ดินตั้งอยู่</th>
                    <th class="text-center">เนื้อที่จำนอง</th>
                    <th colspan="2" class="text-center">ราคาซื้อขายในปัจจุบัน</th>
                    <th class="text-center">ราคาประเมินทุนทรัพย์ที่ดินของกรมธนารักษ์ไร่ล่ะ
                        (บาท)</th>
                    <th class="text-center text-nowrap">ราคาประเมินขั้นต้น
                        <div class="text-center" style="border-bottom: 1px solid #ccc;">=
                            (4+5&#247;2)+(6)</div>
                        <div class="text-center">2</div>
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <th>
                        <div class="text-center">
                            โซน/บล็อก
                            <div>(1)</div>
                        </div>
                    </th>
                    <th><div class="text-center">
                            หน่วยที่
                            <div>(2)</div>
                        </div></th>
                    <th><div class="text-center">
                            (ไร่/งาน/ตารางวา)
                            <div>(3)</div>
                        </div></th>
                    <th><div class="text-center">
                            อย่างสูงไร่ล่ะ(บาท)
                            <div>(4)</div>
                        </div></th>
                    <th><div class="text-center">
                            อย่างต่ำไร่ล่ะ(บาท)
                            <div>(5)</div>
                        </div></th>
                    <th><div class="text-center">(6)</div></th>
                    <th><div class="text-center">(7)</div></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $row_assess = 0;
                $css_price = '';
                foreach ($land_assess as $row) {
                    $css_price = 'inspect_price_' . $row_assess;
                    ?>
                    <tr>
                        <td><a
                                href="<?php echo base_url() ?>land/assess_delete/<?php echo $row['land_assess_id']; ?>"
                                class="close btn btn-sm" aria-label="Close" onclick="return confirm('ยืนยันการลบข้อมูล');"><span aria-hidden="true">&times;</span></a>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="hidden" name="hidden_land_assess_id[]"
                                       value="<?php echo $row['land_assess_id']; ?>"> <input
                                       type="text" name="txt_land_assess_zone[]" class="form-control"
                                       placeholder="" value="<?php echo $row['land_assess_zone']; ?>">
                            </div>
                        </td>
                        <td><input type="text" name="txt_land_assess_unit[]"
                                   class="form-control" placeholder=""
                                   value="<?php echo $row['land_assess_unit']; ?>"></td>
                        <td>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="txt_land_assess_rai[]"
                                           class="form-control css_digit" placeholder="ไร่"
                                           value="<?php echo $row['land_assess_area_rai']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="txt_land_assess_ngan[]"
                                           class="form-control max4" placeholder="งาน"
                                           value="<?php echo $row['land_assess_area_ngan']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="txt_land_assess_wa[]"
                                           class="form-control max100w" placeholder="ตารางวา"
                                           value="<?php echo $row['land_assess_area_wah']; ?>">
                                </div>
                            </div>

                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="txt_land_assess_price_pre_max[]"
                                       class="form-control css_decimal text-right <?php echo $css_price; ?>"
                                       value="<?php echo $row['land_assess_price_pre_max']; ?>" rel="<?php echo $row_assess; ?>">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="txt_land_assess_price_pre_min[]"
                                       class="form-control css_decimal text-right <?php echo $css_price; ?>"
                                       value="<?php echo $row['land_assess_price_pre_min']; ?>" rel="<?php echo $row_assess; ?>">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="land_assess_price[]"
                                       class="form-control css_decimal text-right <?php echo $css_price; ?>"
                                       value="<?php echo $row['land_assess_price']; ?>" rel="<?php echo $row_assess; ?>">
                            </div>
                        </td>
                        <td class="text-right"><input type="hidden"
                                                      class="result_price text-right" name="result_basic[]" id="result_price_<?php echo $row_assess; ?>"
                                                      value="<?php echo $row['land_assess_price_basic']; ?>" /> <span
                                                      class="result_price_<?php echo $row_assess; ?>"><?php echo number_format($row['land_assess_price_basic'], 2); ?></span>
                        </td>
                    </tr>
                    <?php
                    $row_assess ++;
                }
                $css_hide = '';
                //$css_hide_icon = '';
                $css_price = '';
                for ($i = $row_assess; $i < 11; $i ++) {
                    if ($i != $row_assess)
                        $css_hide = 'hide';
                    $css_price = 'inspect_price_' . $i;
                    ?>
                    <tr class="<?php echo $css_hide . ' rows_add_' . $i; ?> ">
                        <td><a href="#<?php echo 'rows_add_' . $i; ?>" class="close btn btn-sm" aria-label="Close" onclick="fnDelRow('<?php echo 'rows_add_' . $i; ?>');"><span
                                    aria-hidden="true">&times;</span></a></td>
                        <td>
                            <div class="form-group">
                                <input type="hidden" name="hidden_land_assess_id[]" value=""> <input
                                    type="text" name="txt_land_assess_zone[]"
                                    id="land_assess_zone_10" class="form-control">
                            </div>
                        </td>
                        <td><input type="text" name="txt_land_assess_unit[]"
                                   id="land_assess_unit_10" class="form-control"  value="0"></td>
                        <td>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="txt_land_assess_rai[]"
                                           id="txt_land_assess_rai_10" class="form-control css_digit"
                                           placeholder="ไร่" value="0">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="txt_land_assess_ngan[]"
                                           id="txt_land_assess_ngan_10" class="form-control  max4"
                                           placeholder="งาน" value="0">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="txt_land_assess_wa[]"
                                           id="txt_land_assess_wa_10" class="form-control max100w"
                                           placeholder="ตารางวา" value="0">
                                </div>
                            </div>
                        </td>
                        <td><input type="text" name="txt_land_assess_price_pre_max[]"
                                   id="'land_assess_price_pre_max_10"
                                   class="form-control css_decimal text-right <?php echo $css_price; ?>" rel="<?php echo $i; ?>"></td>
                        <td><input type="text" name="txt_land_assess_price_pre_min[]"
                                   id="'land_assess_price_pre_min_10"
                                   class="form-control css_decimal text-right <?php echo $css_price; ?>" rel="<?php echo $i; ?>"></td>
                        <td><input type="text" name="land_assess_price[]"
                                   id="'land_assess_price_10"
                                   class="form-control css_decimal text-right <?php echo $css_price; ?>" rel="<?php echo $i; ?>"></td>

                        <td class="text-right"><input type="hidden"
                                                      class="result_price text-right" name="result_basic[]" id="result_price_<?php echo $i; ?>"/> <span
                                                      class="result_price_<?php echo $i; ?>"></span></td>
                    </tr>
                    <?php
                }
                ?>

                <tr>
                    <td></td>
                    <td colspan="2" class="text-center">รวมทั้งแปลง</td>
                    <td class="text-center"><input type="hidden"
                                                   class="result_area_total" id="result_area_total"
                                                   name="land_area_total" /> <span class="result_area_total"></span>
                    </td>
                    <td class="text-center">XX <!--
                                                    <input type="hidden" class="result_price_max" name="price_max"/>
                                                    <span class="result_price_max"></span>
                        -->
                    </td>
                    <td class="text-center">XX <!--
                                                    <input type="hidden" class="result_price_min" name="price_min"/>
                                                    <span class="result_price_min"></span>
                        -->
                    </td>
                    <td class="text-center">XX <!--
                                                    <input type="hidden" class="result_price_total" name="price_inspect"/>
                                                    <span class="result_price_total"></span>
                        -->
                    </td>
                    <td class="text-center">XX <!--
                                                    <input type="hidden" class="result_price_net" name="price_inspect_basic"/>
                                                    <span class="result_price_net"></span>
                        -->
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-sm-12 text-right">
        <div class="form-group">
            <a href="#table_price" class="btn btn-default" id="btn-add-price"> <i class="fa fa-plus-circle"></i> เพิ่มข้อมูลการประเมินราคา</a>
        </div>
    </div>
</div>