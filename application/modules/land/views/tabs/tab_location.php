<div class="row">
    <h4>แผนที่โดยสังเขปของที่ตั้งของที่ดิน</h4>
    <hr/>
    <div class="form-inline form-group">
        <label>ระบุพิกัด ละติจูด</label>

        <input type="text" autocomplete="off" class="form-control" name="txt_lat" value="<?php echo $lat; ?>" >

        <label>ลองติจูด</label>

        <input type="text" autocomplete="off" class="form-control" name="txt_long" value="<?php echo $lng; ?>">

        <a href="#" id="btn_setmap" class="btn btn-default">ค้นหา</a>
    </div>
</div>
<div class="row">
    <div class="row">
        <div id="map" style="height: 500px;"></div>
        <!-- Replace the value of the key parameter with your own API key. -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK8BC2HL9EyuJCth1meiTjVf0htdQPA8o&libraries=drawing" async defer></script>
    <script src="<?php echo base_url('assets/js/modules_land/map.js') ?>"></script>    
        
    </div>
</div>


<div class="row">
    <hr/>
    <div class="col-sm-12">
        <div class="table-responsive">
            <input type="hidden" name="filesland" value="<?php echo $land_data['land_map_file'];?>"/>
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>ชื่อไฟล์</th>
                    <th>รายละเอียด/ลบ</th>
                    
                </tr>
                <?php
                if (!empty($land_data)) {
                    $arr_map_file = explode(',', $land_data['land_map_file']);
                    $i = 1;

                    foreach ($arr_map_file as $val) {
                        ?>

                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $val; ?></td>
                            <td width="10%">
                                <a href="<?php echo base_url() . 'assets/files/land/' . $val; ?>" target="_bank"><i class="fa fa-file-text-o" aria-hidden="true"></i></a> &nbsp;
                                <a href="<?php echo base_url() . 'land/file_map_del/' . $land_data['land_id'] . '/' . $val; ?>" onclick="return confirm('ยืนยันการลบข้อมูล');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>					
                        </tr>
        <?php $i++;
    }
} ?>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label>เอกสารแนบ  (.GIF,JPG,PNG,DOC,PDF)</label>
            <input type="file" name="file_localtion[]" id="file_localtion" multiple="">
        </div>
    </div>
</div>
<!-- 10 -->
