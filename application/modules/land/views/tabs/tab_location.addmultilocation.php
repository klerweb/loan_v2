<div class="row">
	<h4>แผนที่โดยสังเขปของที่ตั้งของที่ดิน</h4>
	<hr/>
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>	
					<th>#</th>	
					<th>ละติจูด</th>
					<th>ลองติจูด</th>
					<th>รายละเอียด/ลบ</th>
				</tr>
				<?php if(!empty($land_data)) { 
					$lat = explode(',',$land_data['land_latitude']);
					$long = explode(',',$land_data['land_longitude']);
					$i = 1;
					foreach($lat as $val){
				?>
				
				<tr>
					<td><?php echo $i;?></td>
					<td><?php echo $val;?></td>
					<td><?php $long[$i]; ?></td>
					<td width="10%">
					<a href="<?php echo base_url().'assets/files/land/'.$val;?>" target="_bank"><i class="fa fa-file-text-o" aria-hidden="true"></i></a> &nbsp;
					<a href="<?php echo base_url().'land/file_map_del/'.$land_data['land_id'].'/'.$val;?>" onclick="return confirm('ยืนยันการลบข้อมูล');    "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					</td>
				</tr>
					<?php $i++; }}?>
			</table>
		</div>
	</div>
</div>
<?php for($i=1;$i<5;$i++){ 
		$hide = '';
		$icon = 'fa-plus';
		//if($i > 1)
			//$hide = 'hide';
		
?>

	<div class="row <?php echo $hide;?>" id="loc_row<?php echo $i;?>">	
		<div class="form-inline form-group">
			<label>ระบุพิกัด ละติจูด</label>
			<div class="input-group">  
			  <input type="text" autocomplete="off" class="form-control" name="txt_lat[]" >
			</div>
			<label>ลองติจูด</label>
			<div class="input-group">
			  <input type="text" autocomplete="off" class="form-control" name="txt_long[]" >
			  <!--<span class="input-group-addon" rel="<?php echo $i;?>">
				<i class="fa <?php echo $icon; ?> " aria-hidden="true" ></i>
			  </span>
			  -->
			</div>
		</div>
	</div>
<?php } ?>


<div class="row">
	<hr/>
	<div class="col-sm-12">
		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>#</th>
					<th>ชื่อไฟล์</th>
					<th>รายละเอียด/ลบ</th>
					
				</tr>
				<?php if(!empty($land_data)) { 
					$arr_map_file = explode(',',$land_data['land_map_file']);
					$i=1;
					foreach($arr_map_file as $val){
				?>
				
				<tr>
					<td><?php echo $i;?></td>
					<td><?php echo $val;?></td>
					<td width="10%">
						<a href="<?php echo base_url().'assets/files/land/'.$val;?>" target="_bank"><i class="fa fa-file-text-o" aria-hidden="true"></i></a> &nbsp;
					<a href="<?php echo base_url().'land/file_map_del/'.$land_data['land_id'].'/'.$val;?>" onclick="return confirm('ยืนยันการลบข้อมูล');    "><i class="fa fa-trash-o" aria-hidden="true"></i></a>
					</td>					
				</tr>
					<?php $i++; }}?>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label>เอกสารแนบ  (.GIF,JPG,PNG,DOC,PDF)</label>
			<input type="file" name="file_localtion" id="file_localtion">
		</div>
	</div>
</div>
<!-- 10 -->
