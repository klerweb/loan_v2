<div class="row">
    <input type="hidden" name="hidden_record_id"
           value="<?php echo $land_record['land_record_id']; ?>">

    <h4>ผู้ตรวจสอบ</h4>
    <div class="col-sm-6">
        <div class="form-group">
            <label>เขียนที่</label> <input type="text" name="txt_write_at"
                                           id="txt_write_at" class="form-control css_req"
                                           value="<?php echo $land_record['land_record_writeat'] != "" ? $land_record['land_record_writeat'] : 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)'; ?>" maxlength="255">
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>วันที่</label>
            <div class="input-group">
                <input type="text" name="txtDate" id="txtDate" class="form-control css_req"
                       value="<?php echo conver_date_en_th(empty($land_record['land_record_writedate']) ? date('Y-m-d') : $land_record['land_record_writedate']); ?>" />
                <span class="input-group-addon"><i
                        class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>
</div>
<!-- record at -->
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>เจ้าหน้าที่ตรวจสอบที่ 1.</label> 
            <!--<input type="text"	name="txt_inspector_1" id="txt_inspector_1" class="form-control css_req"
                    value="<?php echo $land_record['land_record_inspector1']; ?>">-->
            <select name="ddl_inspector_1" id="ddl_inspector_1" placeholder="กรุณาเลือก"  class="width100p">
                <option value="0" >กรุณาเลือก</option>
                <?php
                foreach ($employee as $val) {
                    $emp = '';
                    if ($land_record['land_record_inspector1'] == $val['employee_id'])
                        $emp = 'selected="selected"';
                    ?>
                    <option value="<?= $val['employee_id'] ?>" rel="<?= $val['employee_position_id'] ?>" <?= $emp; ?>><?= $val['employee_fname'] . ' ' . $val['employee_lname']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>ตำแหน่ง</label> 
            <select name="ddl_insp_position"
                    id="ddl_insp_position" placeholder="กรุณาเลือก" class="width100p">
                <option value="0">กรุณาเลือก</option>
                <?php
                foreach ($position as $val) {
                    $selc = '';
                    if ($val['position_id'] == $land_record['land_record_inspector1_position_id'])
                        $selc = 'selected="selected"';
                    ?>
                    <option value="<?= $val['position_id'] ?>" <?= $selc; ?>><?= $val['position_name'] ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label>เจ้าหน้าที่ตรวจสอบที่ 2.</label> 
            <!--<input type="text"
                    name="txt_inspector_2" id="txt_inspector_2" class="form-control css_req"
                    value="<?php echo $land_record['land_record_inspector2']; ?>">-->
            <select name="ddl_inspector_2" id="ddl_inspector_2" placeholder="กรุณาเลือก"  class="width100p">
                <option value="0">กรุณาเลือก</option>
                <?php
                foreach ($employee as $val) {
                    $emp = '';
                    if ($land_record['land_record_inspector2'] == $val['employee_id'])
                        $emp = 'selected';
                    ?>
                    <option value="<?= $val['employee_id']; ?>" <?= $emp; ?>><?= $val['employee_fname'] . ' ' . $val['employee_lname']; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label>ตำแหน่ง</label> <select name="ddl_insp_position_2"
                                           id="ddl_insp_position_2" placeholder="กรุณาเลือก" class="width100p">
                <option value="0">กรุณาเลือก</option>
                <?php
                foreach ($position as $val) {
                    $selc = '';
                    if ($val['position_id'] == $land_record['land_record_inspector1_position_id'])
                        $selc = 'selected="selected"';
                    ?>
                    <option value="<?= $val['position_id'] ?>" <?= $selc; ?>><?= $val['position_name'] ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    </div>
</div>
<!--  inspector -->
<div class="row">
    <hr />
    <h4>1. ตำแหน่งที่ดิน</h4>

    <div class="col-sm-12">
        <div class="form-group">
            <div class="col-sm-2">ทิศเหนือ</div>
            <div class="col-sm-1">ยาว</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_north_m" id="txt_north_m"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_north_long_m']; ?>"> <span
                           class="input-group-addon">เมตร</span>
                </div>
            </div>
            <div class="col-sm-1">
                <div>หรือ</div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_north_wa" id="txt_north_wa"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_north_long_wa']; ?>">
                    <span class="input-group-addon">วา</span>
                </div>
            </div>
            <div class="col-sm-1 form-group">จด</div>
            <div class="col-sm-3">
                <input type="text" name="txt_north_jd" id="txt_north_jd"
                       class="form-control"
                       value="<?php echo $land_data['land_position_north_long_jd']; ?>">
            </div>
        </div>
    </div>
    <!-- north-->

    <div class="col-sm-12">
        <div class="form-group">
            <div class="col-sm-2">ทิศใต้</div>
            <div class="col-sm-1">ยาว</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_south_m" id="txt_south_m"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_south_long_m']; ?>"> <span
                           class="input-group-addon">เมตร</span>
                </div>
            </div>
            <div class="col-sm-1">หรือ</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_south_wa" id="txt_south_wa"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_south_long_wa']; ?>">
                    <span class="input-group-addon">วา</span>
                </div>
            </div>
            <div class="col-sm-1">จด</div>
            <div class="col-sm-3">
                <input type="text" name="txt_south_jd" id="txt_south_jd"
                       class="form-control"
                       value="<?php echo $land_data['land_position_south_long_jd']; ?>">
            </div>
        </div>
    </div>
    <!--south-->

    <div class="col-sm-12">
        <div class="form-group">
            <div class="col-sm-2">ทิศตะวันออก</div>
            <div class="col-sm-1">ยาว</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_east_m" id="txt_east_m"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_east_long_m']; ?>"> <span
                           class="input-group-addon">เมตร</span>
                </div>
            </div>
            <div class="col-sm-1">หรือ</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_east_wa" id="txt_east_wa"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_east_long_wa']; ?>"> <span
                           class="input-group-addon">วา</span>
                </div>
            </div>
            <div class="col-sm-1">จด</div>
            <div class="col-sm-3">
                <input type="text" name="txt_east_jd" id="txt_east_jd"
                       class="form-control"
                       value="<?php echo $land_data['land_position_east_long_jd']; ?>">
            </div>
        </div>
    </div>
    <!--east-->


    <div class="col-sm-12">
        <div class="form-group">
            <div class="col-sm-2">ทิศตะวันตก</div>
            <div class="col-sm-1">ยาว</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_west_m" id="txt_wast_m"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_west_long_m']; ?>"> <span
                           class="input-group-addon">เมตร</span>
                </div>
            </div>
            <div class="col-sm-1">หรือ</div>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="txt_wast_wa" id="txt_wast_wa"
                           class="form-control css_decimal"
                           value="<?php echo $land_data['land_position_west_long_wa']; ?>"> <span
                           class="input-group-addon">วา</span>
                </div>
            </div>
            <div class="col-sm-1">จด</div>
            <div class="col-sm-3">
                <input type="text" name="txt_wast_jd" id="txt_wast_jd"
                       class="form-control"
                       value="<?php echo $land_data['land_position_west_long_jd']; ?>">
            </div>
        </div>
    </div>
    <!--west-->

    <div class="col-sm-12">
        <div class="col-sm-2">คิดเป็นเนื้อที่ประมาณ</div>
        <div class="col-sm-2">
            <div class="input-group form-group">
                <input type="text" class="form-control css_digit" value="<?= $land_data['land_area_mortgage_rai']; ?>"> <span
                    class="input-group-addon">ไร่</span>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group form-group">
                <input type="text" class="form-control css_digit max4" value="<?= $land_data['land_area_mortgage_ngan']; ?>""><span
                    class="input-group-addon">งาน</span>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group form-group">
                <input type="text" class="form-control css_digit max100w" value="<?= $land_data['land_area_mortgage_wa']; ?>""><span
                    class="input-group-addon">ตารางวา</span>
            </div>
        </div>
    </div>
</div>
<!--land location-->
<div class="row">
    <hr />
    <h4>2. ประเภท ลักษณะ และคุณภาพของที่ดิน ขณะนี้ทำประโยชน์อย่างใด
        และได้ผลดีเพียงใด</h4>
    <div class="col-sm-12">
        <div class="form-group">
            <textarea name="txt_land_quality" class="form-control"><?php echo $land_data['land_type_quality']; ?></textarea>
        </div>
    </div>
</div>
<!-- 2 ประเภท ลักษณะ และคุณภาพของที่ดิน ขณะนี้ทำประโยชน์อย่างใด และได้ผลดีเพียงใด -->
<div class="row">
    <hr />
    <h4>3. มีสิ่งปลูกสร้างในที่ดินแปลงนี้หรือไม่</h4>
    <div class="col-sm-12">
        <div class="form-group">
            <textarea name="txt_land_buiding_detail" class="form-control"><?php echo $land_data['land_buiding']; ?></textarea>
        </div>
    </div>
</div>
<!-- 3 มีสิ่งปลูกสร้างในที่ดินแปลงนี้หรือไม่  -->
<div class="row">
    <hr />
    <h4>4. ทำเลของที่ดินและความสะดวกในการคมนาคม</h4>
    <div class="col-sm-12">
        <div class="form-group">
            <textarea name="txt_land_location_detail" class="form-control"><?php echo $land_data['land_location']; ?></textarea>
        </div>
    </div>
</div>
<!-- 5 ทำเลของที่ดินและความสะดวกในการคมนาคม  -->
<div class="row">
    <hr />
    <h4>5. การชลประทานที่ได้รับ</h4>
    <div class="col-sm-12">
        <div class="form-group">
            <textarea name="txt_land_irigation" class="form-control"><?php echo $land_data['land_irigation']; ?></textarea>
        </div>
    </div>
</div>
<!-- 4 การชลประทานที่ได้รับ   -->
<div class="row">
    <hr />
    <h4>6. ผลิตผลที่ได้รับในรอบปีหนึ่ง</h4>
    <div class="col-sm-12">
        <div class="form-group">
            <textarea name="txt_land_product" class="form-control text-left"><?php echo $land_data['land_product']; ?></textarea>
        </div>
    </div>
</div>
<!-- 6 ผลิตผลที่ได้รับในรอบปีหนึ่ง   -->
<div class="row">
    <hr />
    <h4>7. ได้เคยจำนอง ขายฝาก หรือใช้ประกันหนี้หรือไม่ ราคาประเมินเท่าใด</h4>
    <div class="col-sm-12">
        <div class="form-group">
            <input type="radio" name="rdo_mortagage" value="1"
                   <?php if ($land_data['land_mortagage'] == '1') { ?> checked="checked"
                   <?php } ?> /> ไม่เคย &nbsp;&nbsp;&nbsp;<input type="radio"
                   name="rdo_mortagage" value="2"
                   <?php if ($land_data['land_mortagage'] == '2') { ?> checked="checked"
                   <?php } ?> /> เคย
        </div>
        <div class="form-group">
            <input type="text" name="txt_land_mortagage"
                   class="form-control " id="txt_land_mortagage"
                   value="<?php echo $land_data['land_mortagage_price'] == '' ? '' : $land_data['land_mortagage_price']; ?>"
                   <?php if ($land_data['land_mortagage'] != '2') { ?> readonly
                   <?php } ?> />
        </div>
        <!-- /input-group -->
    </div>
    <!-- /.col-lg-6 -->

</div>
<!-- 7 ผลิตผลที่ได้รับในรอบปีหนึ่ง   -->
<div class="row">
    <hr />
    <h4>8. ลักษณะและสภาพของที่ดินที่ทำให้ราคาที่ดินต่ำลง</h4>	
    <?php
    $arr_condi_price_drop = explode(',', $land_data ['land_condition_price_drop']);
    ?>
    <div class="col-sm-12">
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="1" <?php if (in_array('1', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>> สภาพที่ดินเป็นบ่อลึก เนินสูง ลาดเทมาก
                น้ำท่วมขัง หรือเป็นพรุ </label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="2" <?php if (in_array('2', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>
                หน้าดินส่วนใหญ่ถูกขุดขายจนไม่สามารถใช้ประโยชน์ตามปกติได้</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="3" <?php if (in_array('3', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ที่ดินถูกเปลี่ยนแปลงไปในทางที่เลวลงจากสภาพที่เคยใช้ประโยชน์ตามปกติอย่างมาก</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="4" <?php if (in_array('4', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>สภาพที่ดินเลว เช่น เป็นดินเค็ม
                ดินเปรี้ยว หรือมีหิน กรวด ทราย เป็นจำนวนมาก เป็นต้น</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="5" <?php if (in_array('5', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ตั้งอยู่ในเขตผู้ร้ายชุกชุม
                หรือในเขตครอบงำของผู้มีอิทธิพล</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="6" <?php if (in_array('6', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ตั้งอยู่ในเขตอันตราย เช่น
                ชายแดนที่อาจถูกรุกราน หรือสนามซ้อมยิงปืนของทหาร</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="7" <?php if (in_array('7', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ตั้งอยู่ในเขตชุมชนที่ไม่มีทางเข้า-ออก
                หรือทางเข้า-ออก ไม่สะดวก</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="8" <?php if (in_array('8', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ตั้งอยู่ในบริเวณที่ได้รับมลพิษจากโรงงาน
                เช่น น้ำเสีย ควันพิษ กลิ่น หรือเสียงรบกวน เป็นต้น</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="9" <?php if (in_array('9', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>เนื้อที่ส่วนใหญ่มีสายไฟฟ้าแรงสูงผ่านพื้นที่</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="10" <?php if (in_array('10', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ติดภาระจำยอมต่าง ๆ ตามกฎหมาย เช่น
                ใช้เป็นทางเดินผ่าน หรือที่เลี้ยงสัตว์สาธารณะ</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="11" <?php if (in_array('11', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>มีภาระผูกพันจากการให้เช่าเป็นเวลานาน
                หรือมีผู้เช่าจำนวนมาก</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="land_condition_price_drop[]"
                          value="12" <?php if (in_array('12', $arr_condi_price_drop)) { ?>
                              checked="checked" <?php } ?>>ตั้งอยู่ในบริเวณที่อาจถูกเวนคืนหรือเพิกถอนเอกสารสิทธิ</label>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <div class="chekbox">
                        <label><input type="checkbox" name="land_condition_price_drop[]"
                                      id="land_condition_price_drop13" value="13"
                                      <?php if (in_array('13', $arr_condi_price_drop)) { ?>
                                          checked="checked" <?php } ?>> อื่น ๆ</label>
                    </div>
                    <input type="text" name="txt_condition_price_drop13"
                           id="txt_condition_price_drop13" class="form-control"
                           <?php if (!in_array('13', $arr_condi_price_drop)) { ?> readonly
                           <?php } ?>
                           value="<?php echo $land_data['land_condition_price_drop13_text']; ?>">
                </div>
                <!-- /input-group -->
            </div>
        </div>
    </div>
</div>
<!-- 8 -->
<div class="row">
    <hr />
    <h4>9. ที่ดินอยู่ในเขตที่ทางราชการสงวนไว้หรือไม่</h4>
    <h5>เขตสงวนหวงห้ามของทางราชการ เช่น เขตทหาร ป่าสงวนแห่งชาติ
        อุทยานแห่งชาติ ที่สาธารณประโยชน์ หรือเขตปฏิรูปที่ดิน เป็นต้น</h5>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="radio">
                <label><input type="radio" name="rdo_reserve" value="0"
                              <?php if ($land_data['land_reservation'] == '0') { ?> checked
                              <?php } ?>>ไม่สามารถตรวจสอบได้ เนื่องจาก </label>
            </div>
            <input type="text" name="txt_unable_reserve" id="txt_unable_reserve"
                   class="form-control"
                   <?php if ($land_data['land_reservation'] != '0') { ?> readonly
                   <?php } ?>
                   value="<?php echo $land_data['land_reservation_checkdesc']; ?>">
        </div>
        <!-- /input-group -->
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="radio">
                <label><input type="radio" name="rdo_reserve" value="1"
                              <?php if ($land_data['land_reservation'] == '1') { ?> checked
                              <?php } ?>>อยู่ในเขต (ระบุ) </label>
            </div>
            <input type="text" name="txt_reserve_location"
                   id="txt_reserve_location" class="form-control"
                   <?php if ($land_data['land_reservation'] != '1') { ?> readonly
                   <?php } ?>
                   value="<?php echo $land_data['land_reservation_location'] ?>">
        </div>
        <!-- /input-group -->
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-contol">
            <div class="radio">
                <label><input type="radio" name="rdo_reserve" value="2"
                              <?php if ($land_data['land_reservation'] == '2') { ?> checked
                              <?php } ?>>ไม่อยู่ในเขตพื้นที่สงวนหวงห้ามของทางราชการ </label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="radio">
                <label><input type="radio" name="rdo_reserve" value="3"
                              <?php if ($land_data['land_reservation'] == '3') { ?> checked
                              <?php } ?>>อาจจะเป็นที่ดินในพื้นที่คาบเกี่ยวกับเขต </label>
            </div>
            <input type="text" name="txt_reserve_zone" id="txt_reserve_zone"
                   class="form-control"
                   <?php if ($land_data['land_reservation'] != '3') { ?> readonly
                   <?php } ?>
                   value="<?php echo $land_data['land_reservation_desc']; ?>">
        </div>
        <!-- /input-group -->
    </div>
</div>
<div class="row">
    <div class="col-sm-11 col-sm-offset-1">
        โดยได้ตรวจสอบจาก
        <div class="row">
            <div class="col-sm-12">
                <div class="radio">
                    <label><input type="radio" name="rdo_map_check" value="1"
                        <?php if ($land_data['land_reservation'] == '3' && $land_data['land_reservation_map'] == '1') { ?>
                                      checked <?php } if ($land_data['land_reservation'] != '3') { ?>
                                      disabled <?php } ?>>แผนที่ภูมิประเทศ </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <div class="radio">
                        <label><input type="radio" name="rdo_map_check" value="2"
                            <?php if ($land_data['land_reservation'] == '3' && $land_data['land_reservation_map'] == '2') { ?>
                                          checked <?php } if ($land_data['land_reservation'] != '3') { ?>
                                          disabled <?php } ?>>แผนที่ระวางรูปถ่ายทางอากาศ เมื่อวันที่ </label>
                    </div>
                    <div class="input-group">
                        <input type="text" name="txt_map_check_date"
                               id="txt_map_check_date" class="form-control" <?php if ($land_data['land_reservation_map'] != '2') { ?>readonly <?php } ?>
                               value="<?php echo empty($land_data['land_reservation_checkdate']) ? '' : conver_date_en_th($land_data['land_reservation_checkdate']); ?>" />
                        <span class="input-group-addon"><i
                                class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
                <!-- /input-group -->
                
            </div>
        </div>
        
    </div>
    <div class="row">
            <div class="col-sm-12">
                <label>หมายเหตุ</label>
                <input type="text" name="land_reservation_other" id="land_reservation_other" value="<?php echo $land_data['land_reservation_other'];?>" class="form-control" />
            </div>
    </div>
</div>
<!--10-->
<div class="row">
    <hr />
    <h4>10. รายละเอียดอื่น ๆ</h4>

    <div class="col-sm-12">
        <h5>10.1 กรณีผู้ขอสินเชื่อนำที่ดินของบุคคลอื่นที่ไม่ใช่คู่สมรสมาจำนอง
            มีเหตุผลความจำเป็นอย่างไร และมีลักษณะกู้เงินแทนบุคคลอื่นหรือไม่</h5>
        <textarea rows="5" class="form-control" name="txt_land_other"><?php echo $land_data['land_of_other']; ?></textarea>
    </div>

    <div class="col-sm-12">
        <h5>10.2 อื่น ๆ</h5>
        <textarea rows="5" class="form-control" name="txt_other"><?php echo $land_data['land_other']; ?></textarea>
    </div>
</div>
<!-- 10 -->
