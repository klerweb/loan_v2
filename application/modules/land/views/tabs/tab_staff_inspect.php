<!--12-->
<div class="row">
	<div class="col-sm-12">
		<h4>12. ความเห็นของเจ้าหน้าที่ผู้ตรวจสอบประเมินราคาที่ดินขั้นต้น</h4>
		<div class="text-right">
			<button type="submit" class="btn btn-primary" id="btn_inspect_copy">คัดลอกข้อมูลจากข้อ
				11</button>
		</div>
	</div>
	<!-- panel-footer -->

</div>
<div class="row">
	<div class="col-sm-12">บจธ. ควรประเมินราคาจำนองที่ดิน เนื้อที่</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered" id="table_org_price">
			<thead>
				<tr>
					<th colspan="2" class="text-center">บริเวณที่ดินตั้งอยู่</th>
					<th class="text-center">เนื้อที่จำนอง</th>
					<th class="text-center">บจธ. ควรประเมินไร่ล่ะ (บาท)</th>
					<th class="text-center text-nowrap">ราคาประเมินขั้นต้น
						<div class="text-center">= (3)x(4)</div>
					</th>
				</tr>
				<tr>
					<th>
						<div class="text-center">
							โซน/บล็อก
							<div>(1)</div>
						</div>
					</th>
					<th><div class="text-center">
							หน่วยที่
							<div>(2)</div>
						</div></th>
					<th><div class="text-center">
							(ไร่/งาน/ตารางวา)
							<div>(3)</div>
						</div></th>
					<th><div class="text-center">(4)</div></th>
					<th><div class="text-center">(5)</div></th>
				</tr>
			</thead>
			<tbody>
							<?php
							$land_assess_inspector_sum = 0.00;
							$land_assess_staff_sum = 0.00;
							$i = 1;
							foreach ( $land_assess as $row ) {
								$land_assess_inspector_sum = $row['land_assess_inspector_sum'];
								$land_assess_staff_sum = $row['land_assess_staff_sum'];
								$row_staff_price = 'row_staff_'.$i;
								$row_staff_price_total = 'row_staff_total_'.$i;
								?>
							<tr>
					<td>
						<div class="form-group">
							<input type="hidden" name="hidden_land_assess_inspect_id[]"
								value="<?php  echo $row['land_assess_id']; ?>"> <input
								type="text" name="txt_land_assess_inspect_zone[]"
								class="form-control" placeholder=""
								value="<?php echo $row['land_assess_zone']; ?>" disabled>
						</div>
					</td>
					<td><input type="text" name="txt_land_assess_inspect_unit[]"
						class="form-control" placeholder=""
						value="<?php  echo $row['land_assess_unit']; ?>" disabled></td>
					<td>

						<div class="col-sm-4">
							<div class="form-group">
								<input type="text" name="txt_land_assess_inspect_rai[]"
									class="form-control css_digit <?php echo $row_staff_price; ?>" placeholder="ไร่"
									value="<?php  echo $row['land_assess_area_rai']; ?>" rel="<?php echo $i;?>" disabled>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<input type="text" name="txt_land_assess_inspect_ngan[]"
									class="form-control css_digit <?php echo $row_staff_price; ?>" placeholder="งาน"
									value="<?php  echo $row['land_assess_area_ngan']; ?>" rel="<?php echo $i;?>" disabled>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<input type="text" name="txt_land_assess_inspect_wa[]"
									class="form-control css_digit <?php echo $row_staff_price; ?>" placeholder="ตารางวา"
									value="<?php  echo $row['land_assess_area_wah']; ?>" rel="<?php echo $i;?>" disabled>
							</div>
						</div>

					</td>

					<td>
						<div class="form-group">
							<input type="text" name="land_assess_inspect_price[]"
								class="form-control css_decimal text-right <?php echo $row_staff_price; ?>"
								value="<?php  echo !empty($row['land_assess_inspector_price']) ? ($row['land_assess_inspector_price']!=$row['land_assess_price_basic']?$row['land_assess_price_basic']:$row['land_assess_inspector_price'])  : $row['land_assess_price_basic']; ?>" rel="<?php echo $i;?>" readonly>
						</div>
					</td>
					<td class="text-right">
						<input type="hidden" class="<?php echo $row_staff_price_total;?> text-right" name="result_inspect_basic[]" value="<?php  echo $row['land_assess_inspector_sum']; ?>" rel="<?php echo $i;?>" /> 
						<span class="<?php echo 'disp_'.$row_staff_price_total;?>"><?php  echo number_format($row['land_assess_inspector_sum'],2); ?></span>
					</td>
				</tr>
							<?php
								$i++;
							}
							?>
							<tr>
					<td colspan="2" class="text-center">รวมทั้งแปลง</td>
					<td class="text-center"><span class="result_area_total"></span></td>
					<td class="text-center">XX <!--
									<input type="hidden" class="result_price_max" name="price_max"/>
									<span class="result_price_max"></span>
									-->
					</td>
					<td class="text-center">
					<input type="hidden" name="price_org_toal" id="price_org_toal" value="<?=isset($land_assess[0]['land_assess_inspector_total']) ? $land_assess[0]['land_assess_inspector_total'] : '';?>"> 
					<span class="price_org_toal"></span> <!--
									<input type="hidden" class="result_price_min" name="price_min"/>
									<span class="result_price_min"></span>
									--></td>

				</tr>
			
			
			<tbody>
		
		</table>
	</div>
</div>
<!--
			<div class="row">
				
				<div class="col-sm-3">
					<div class="input-group form-group">
						<input type="text" class="form-control css_digit" name="txt_land_assess_org_rai" id="txt_land_assess_org_rai"  value="<?=$land_data['land_area_mortgage_rai'];?>" disabled>
						<span class="input-group-addon">ไร่</span>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="input-group form-group">			
						<input type="text" class="form-control css_digit" name="txt_land_assess_org_ngan" id="txt_land_assess_org_ngan"  value="<?=$land_data['land_area_mortgage_ngan'];?>" disabled>
						<span class="input-group-addon">งาน</span>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="input-group form-group">
						<input type="text" class="form-control css_digit" name="txt_land_assess_org_wah" id="txt_land_assess_org_wah"  value="<?=$land_data['land_area_mortgage_wah'];?>" disabled>
						<span class="input-group-addon">ตารางวา</span>
					</div>	
				</div>
			</div>-->
<!-- assess -->



<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label>เป็นจำนวนเงิน </label>
			<div class="input-group">
				<input type="text" name="txt_sum_inspect_price"
					class="form-control css_decimal"
					value="<?php echo number_format($land_record['land_record_inspector_mortagage_price']); ?>" />
                                        
				<span class="input-group-addon">บาท</span>
			</div>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="form-group">
			<span id="span_disp_assess_price"></span>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<label>เหตุผลที่ประเมินราคาสูงหรือต่ำกว่าราคาตามข้อ 11</label>
		<textarea class="form-control" name="txt_reason1"><?php echo $land_record['land_record_inspector_mortagage_reason'];?></textarea>
	</div>
</div>