<div class="row"><div class="col-sm-12"><h4>ผู้ขอสินเชื่อ</h4></div></div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="txt_lastname">คำนำหน้าชื่อ</label>
            <input type="text" name="txt_person_title" value="<?php echo $loan_data['title_name']; ?>" class="form-control" disabled />
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label class="" for="txt_name">ชื่อ </label>
            <input type="text" name="txt_name" id="txt_name" class="form-control" value="<?php echo $loan_data['person_fname']; ?>" disabled>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label for="txt_lastname">นามสกุล</label>
            <input type="text" class="form-control" id="txt_lastname"  value="<?php echo $loan_data['person_lname']; ?>" disabled>
        </div>
    </div>

</div><!-- person info -->
<div class="row"><div class="col-sm-12"><h4>หนังสือแสดงสิทธิในที่ดิน</h4></div></div>
<div class="row">

    <div class="col-sm-3">
        <div class="form-group">
            <label class="control-label">ประเภทเอกสารสิทธิ</label>
            <select name="ddl_land_type" id="ddl_land_type" data-placeholder="กรุณาเลือก" class="width100p css_req" >
                <option value="">กรุณาเลือก</option>
                <?php
                $lt = 'selected';
                foreach ($land_type as $val) {
                    if ($val['land_type_id'] == $land_data['land_type_id'])
                        $lt = 'selected = "selected" ';
                    ?>
                    <option value="<?php echo $val['land_type_id']; ?>" <?php echo $lt; ?>><?php echo $val['land_type_name']; ?></option>
                    <?php
                    $lt = '';
                }
                ?>

            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="land_no">เลขที่ </label>
            <input type="text" name="txt_land_no" id="txt_land_no" class="form-control css_digit" value="<?php echo $land_data['land_no']; ?>" >
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label for="land_no">เลขที่ดิน </label>
            <input type="text" name="txt_land_addr_no" id="txt_land_addr_no" class="form-control css_digit" value="<?php echo $land_data['land_addr_no']; ?>" >
        </div>
    </div>
     <div class="col-sm-3">
        <div class="form-group">
            <label for="txt_address_ban">ตั้งอยู่บ้าน</label>
            <input type="text" name="txt_address_ban" id="land_addr_no" class="form-control css_req" value="<?php echo $land_data['land_addr_ban']; ?>" />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="land_addr_moo">หมู่ที่</label>
            <input type="text" name="land_addr_moo" id="land_addr_moo" class="form-control" value="<?php echo $land_data['land_addr_moo'] ?>" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="land_area_rai">เนื้อที่</label>
            <div class="input-group">
                <input type="text" name="txt_land_area_rai" id="land_area_rai" class="form-control css_req" value="<?php echo $land_data['land_area_rai']; ?>" >
                <span class="input-group-addon">ไร่</span>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label for="land_area_ngan">&nbsp;</label>
            <div class="input-group">
                <input type="text" name="txt_land_area_ngan" id="land_area_ngan" class="form-control max4" value="<?php echo $land_data['land_area_ngan']; ?>" >
                <span class="input-group-addon">งาน</span>
            </div>
        </div>
    </div>
    <div  class="col-sm-3">
        <div class="form-group">
            <label for="land_area_wah">&nbsp;</label>
            <div class="input-group">
                <input type="text" name="txt_land_area_wah" id="land_area_wah" class="form-control max100w" value="<?php echo $land_data['land_area_wah']; ?>" >
                <span class="input-group-addon">ตารางวา</span>
            </div>
        </div>
    </div>
</div><!-- land info-->
<!--
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="txt_address_ban">ตั้งอยู่บ้าน</label>
            <input type="text" name="txt_address_ban" id="land_addr_no" class="form-control css_req" value="<?php echo $land_data['land_addr_ban']; ?>" />
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="land_addr_moo">หมู่ที่</label>
            <input type="text" name="land_addr_moo" id="land_addr_moo" class="form-control" value="<?php echo $land_data['land_addr_moo'] ?>" />
        </div>
    </div>
</div>
-->
<div class="row">

    <div class="col-sm-3">
        <div class="form-group">
            <label class="control-label">จังหวัด</label>
            <select name="ddl_province" id="ddl_province" data-placeholder="กรุณาเลือก" class="width100p">
                <option value="">กรุณาเลือก</option>
                <?php foreach ($province as $province_data) { ?>
                    <option value="<?php echo $province_data['province_id']; ?>"  <?php if ($province_data['province_id'] == $land_data['land_addr_province_id']) { ?>selected="selected"<?php } ?> ><?php echo $province_data['province_name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <input type="hidden" name="hidden_amphur" id="hidden_amphur" value="<?php echo $land_data['land_addr_amphur_id']; ?>">
            <label class="control-label">อำเภอ</label>
            <div id="div_amphur">
                <select name="ddl_amphur" id="ddl_amphur" data-placeholder="กรุณาเลือก" class="width100p" >
                    <option value="">กรุณาเลือก</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <input type="hidden" name="hidden_district" id="hidden_district" value="<?php echo $land_data['land_addr_district_id']; ?>">
            <label class="control-label">ตำบล</label>
            <div id="div_district">
                <select name="ddl_district" id="ddl_district" data-placeholder="กรุณาเลือก" class="width100p">
                    <option value="">กรุณาเลือก</option>
                </select>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label">การใช้ประโยชน์ในที่ดิน <span class="asterisk">*</span></label>
            <textarea class="form-control" rows="5" maxlength="200" name="land_benefit" id="land_benefit" placeholder="การใช้ประโยชน์ในที่ดิน" required><?php echo $land_data['land_benefit'] ?></textarea>
        </div>
    </div>
</div>
<!-- land address -->
<div class="row">
    <hr/>
    <div class="col-sm-3">
        <div class="form-group">
            <label  for="ddl_land_area_mortgage">จะจำนอง</label>
            <select name="ddl_land_area_mortgage" id="ddl_land_area_mortgage"  data-placeholder="กรุณาเลือก" class="width100p">
                <!--<option value="0">--กรุณาเลือก--</option>-->
                <option value="1" <?php if ($land_data['land_area_mortgage'] == '1') {
                    echo 'selected="selected"';
                } ?>>ทั้งแปลง</option>
                <option value="2" <?php if ($land_data['land_area_mortgage'] == '2') {
                    echo 'selected="selected"';
                } ?>>เฉพาะส่วนของ</option>
            </select>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="txt_land_area_mortgage_rai">เนื้อที่</label>
            <div class="input-group">
                <input type="text" name="txt_land_area_mortgage_rai" id="txt_land_area_mortgage_rai" class="form-control css_digit" value="<?php echo isset($land_data['land_area_mortgage_rai']) ? $land_data['land_area_mortgage_rai'] : $land_data['land_area_rai']; ?>">
                <span class="input-group-addon">ไร่</span>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label for="txt_land_area_mortgage_ngan">&nbsp;</label>
            <div class="input-group">
                <input type="text" name="txt_land_area_mortgage_ngan" id="txt_land_area_mortgage_ngan" class="form-control css_digit" value="<?php echo isset($land_data['land_area_mortgage_ngan']) ? $land_data['land_area_mortgage_ngan'] : $land_data['land_area_ngan']; ?>">
                <span class="input-group-addon">งาน</span>
            </div>
        </div>
    </div>
    <div  class="col-sm-3">
        <div class="form-group">
            <label for="txt_land_area_mortgage_wah">&nbsp;</label>
            <div class="input-group">
                <input type="text" name="txt_land_area_mortgage_wah" id="txt_land_area_mortgage_wah" class="form-control max100w" value="<?php echo isset($land_data['land_area_mortgage_wa']) ? $land_data['land_area_mortgage_wa'] : $land_data['land_area_wah']; ?>">
                <span class="input-group-addon">ตารางวา</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <label>ชื่อผู้กู้ร่วม</label>
<?php
if (count($loanco) > 0) {
    foreach ($loanco as $val) {
        ?>
                <div class="form-group">
                    <input type="text" name="txt_join" id="txt_join_01" class="form-control" value="<?php echo $val['title_name'] . ' ' . $val['person_fname'] . ' ' . $val['person_lname']; ?>" disabled/>
                </div>
            <?php }
        } else { ?>
            <div class="form-group">
                <input type="text" name="txt_join" id="txt_join_01" class="form-control"  disabled/>
            </div>
    <?php }
?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h4>ผู้ที่มีชื่อในหนังสือแสดงกรรมสิทธิในที่ดิน

        </h4>
        <a href="<?php echo base_url(); ?>land/owner" class="btn btn-default" id="btn-add-owner">
            <i class="fa fa-plus-circle"></i> เพิ่มผู้มีชื่อในหนังสือกรรมสิทธิ
        </a>
    </div>
</div>
<!-- owner -->
<div class="row">
    <div class="col-sm-12">
        <table class="table" id="tb-land-owner">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ผู้ที่มีชื่อในหนังสือแสดงกรรมสิทธิในที่ดิน</th>
                    <th width="6%">อายุ (ปี) </th>
                    <th>เป็นบุตรของนาย-นาง/นางสาว</th>
                    <th>ที่อยู่</th>
                    <th width="10%">ความเกี่ยวข้องกับผู้ขอสินเชื่อ</th>
                </tr>
            </thead>
            <tbody>
<?php
foreach ($land_owner as $oval) {
    ?>
                    <tr>
                        <td class="text-nowrap">
                            <a href="<?php echo site_url("land/owner/" . $oval['land_owner_id'] . '/' . $oval['person_id']); ?>" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="แก้ไข"><i class="fa fa-pencil-square-o"></i></a>
                            <a href="<?php echo site_url("land/delete_owner/".$oval['land_owner_id']);?>" onClick="return confirm('ยืนยันการลบข้อมูล');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                        <td class="text-nowrap">
                            <input type="hidden" name="person_id[]"/>
                            <p><?php echo $oval['person_fname'] . ' ' . $oval['person_lname']; ?></p>
                        </td>
                        <td><p><?php echo $oval['land_owner_age']; ?></p></td>
                        <td><p><?php echo $oval['land_owner_father_fname'] . ' ' . $oval['land_owner_father_lname'] . '-' . $oval['land_owner_mather_fname'] . ' ' . $oval['land_owner_mather_lname']; ?></p></td>
                        <td><p><?php echo show_address_master('person_addr_card', $oval); ?></p></td>
                        <td><p><?php echo $oval['relationship_name']; ?></p></td>
                    </tr>
    <?php
}
?>

            </tbody>
        </table>
    </div>
</div>


<!-- owner -->
<!-- land info-->
<!-- tab ข้อมูลที่ดินของผู้ขอสินเชื้อ -->
