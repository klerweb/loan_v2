<?php

defined('BASEPATH') or exit('No direct script access allowed');

class land extends MY_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    private $title = 'รายการแบบบันทึกการตรวจสอบที่ดิน';
    private $valid = false;
    private $land_area_mortgage = array('1' => 'ทั้งแปลง', '2' => 'เฉพาะส่วนของ');
    private $land_mortagage = array('0' => '', '1' => 'ไม่เคย', '2' => 'เคย');
    private $land_id = '';
    private $loan_id = '';
    private $owner_id = '';
    private $owner_person_id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('pagination');

        $this->load->helper('form');
        $this->load->helper('address_helper');
        $this->load->helper('land_helper');
        $this->load->helper('loan_helper');
        $this->load->helper('date_helper');
        $this->load->helper('currency_helper');
        $this->load->helper('common_helper');

        //modules
        $this->load->model("loan_model");
        $this->load->model("land_model");
        $this->load->model('landowner_model');
        $this->load->model('landrecord_model');
        $this->load->model('landassess_model');
        $this->load->model('title/title_model');

        $this->load->model('province/province_model');
        $this->load->model('district/district_model');
        $this->load->model('amphur/amphur_model');

        $this->load->model('loan/loan_co_model');
        $this->load->model('relationship/relationship_model');
        $this->load->model('person/person_model');
        $this->load->model('land_type_model');
        $this->load->model('zipcode_model');
        $this->load->model('employee_model');

        $this->load->model('landlatitude_model');
    }

    public function validate_land()
    {
        if ($this->session->has_userdata('loan_id')) {
            $this->loan_id = $this->session->userdata('loan_id');
        }

        if ($this->session->has_userdata('land_id')) {
            $this->land_id = $this->session->userdata('land_id');
        }

        if ($this->session->has_userdata('owner_id')) {
            $this->owner_id = $this->session->userdata('owner_id');
        }

        if ($this->session->has_userdata('owner_person_id')) {
            $this->owner_person_id = $this->session->userdata('owner_person_id');
        }


        if (empty($this->land_id) || empty($this->loan_id)) {
            redirect(site_url('land'));
        }
    }

    public function index()
    {
        $this->load->model('search_model');


        $page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
        $get_per_page = isset($_per_page) ? get_per_page($_per_page) : get_per_page();


        $land_no = $this->input->post('land_no');
        $person_thaiid = $this->input->post('person_card_id');
        $person_fname = $this->input->post('person_name');
        $person_lname = $this->input->post('person_lastname');
        $search_data = $this->search_model->get($land_no, $person_thaiid, $person_fname, $person_lname);
        //status add assess land


        //split page
        $arr_page_data = array_chunk($search_data, $get_per_page, false);

        //config pagging
        $total_rows = count($search_data);
        $config_pagination = config_pagination();
        $config_pagination['base_url'] = current_url();
        $config_pagination['total_rows'] = $total_rows;
        $config_pagination['per_page'] = $get_per_page;

        $this->pagination->initialize($config_pagination);
        $pagination = $this->pagination->create_links();
        $title_pagination = title_pagination($page, $get_per_page, $total_rows);

        $data_on_page = array();
        if (count($arr_page_data)) {
            $data_on_page = $arr_page_data[$page - 1];
        }
        $data = array(
            'data' => $data_on_page, //is page
            'pagging' => $pagination,
            'pagging_row' => $get_per_page,
            'visible_button_add' => true
        );

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array('สินเชื่อ' => '#');

        $data = array(
            'title' => $this->title,
            'css_other' => array('modules_loan.css'),
            'menu' => $this->parser->parse('page/menu', $data_menu, true),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
            'content' => $this->parser->parse('search', $data, true)
        );
        $this->parser->parse('main', $data);
    }

    //add list land
    public function add()
    {
        $this->load->model('search_model');

        $page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
        $get_per_page = isset($_per_page) ? get_per_page($_per_page) : get_per_page();


        $land_no = $this->input->post('land_no');
        $person_thaiid = $this->input->post('person_card_id');
        $person_fname = $this->input->post('person_name');
        $person_lname = $this->input->post('person_lastname');
        $search_data = $this->search_model->get_add($land_no, $person_thaiid, $person_fname, $person_lname);

        //split page
        $arr_page_data = array_chunk($search_data, $get_per_page, false);

        //config pagging
        $total_rows = count($search_data);
        $config_pagination = config_pagination();
        $config_pagination['base_url'] = current_url() . '/';
        $config_pagination['total_rows'] = $total_rows;
        $config_pagination['per_page'] = $get_per_page;

        $this->pagination->initialize($config_pagination);
        $pagination = $this->pagination->create_links();
        $title_pagination = title_pagination($page, $get_per_page, $total_rows);

        $data_on_page = array();
        if (count($arr_page_data)) {
            $data_on_page = $arr_page_data[$page - 1];
        }
        /*
          print('<pre>');
          print_r($data_on_page);
          print('</pre>');
         */

        $data = array(
            'data' => $data_on_page, //is page
            'pagging' => $pagination,
            'pagging_row' => $get_per_page,
            'visible_button_add' => false
        );

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array('สินเชื่อ' => '#', $this->title => site_url('land'));

        $data = array(
            'title' => 'เพิ่มแบบบันทึกการตรวจสอบที่ดิน',
            'css_other' => array('modules_loan.css'),
            'menu' => $this->parser->parse('page/menu', $data_menu, true),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
            'content' => $this->parser->parse('search', $data, true)
        );
        $this->parser->parse('main', $data);
    }

    public function form($loan_id, $land_id)
    {
        $this->session->set_userdata('loan_id', $loan_id);
        $this->session->set_userdata('land_id', $land_id);

        $this->validate_land();



        $arr_loan = $this->loan_model->get_loan($this->loan_id);
        $arr_loan['title_name'] = null;
        $arr_land = $this->land_model->get_land($this->land_id);
        $latlng =  $this->landlatitude_model->getby_landid($this->land_id);
        $lat = '';
        $lng = '';
        //$this->log($latlng);
        if (count($latlng) > 0) {
            $arr_latlng = explode(',', $latlng[0]['land_latitude_val']);
            $lat = $arr_latlng[0];
            $lng = $arr_latlng[1];
        }

        if (!empty($arr_loan)) {
            $arr_person_title = $this->title_model->getById($arr_loan['title_id']);
            $arr_loan['title_name'] = empty($arr_person_title) ? '' : $arr_person_title['title_name'];
        }

        //set active tabs

        if (!$this->session->has_userdata('ss_tabs')) {
            $this->get_tabs();
        }


        /* * * set data to view ** */
        $arr_land_info = array(
            'loan_data' => $arr_loan,
            'land_data' => $arr_land,
            'province' => $this->province_model->all(),
            'land_type' => $this->land_type_model->all(),
            'loanco' => $this->loan_co_model->report_list(array('loan_id' => $this->loan_id)),
            'land_owner' => $this->landowner_model->get_by_land_id($this->land_id),
            'land_record' => $this->landrecord_model->get_by_land($this->land_id),
            'land_assess' => $this->landassess_model->GetAssessById($this->land_id),
            //'message' => $this->get_message(),
            'employee' => $this->employee_model->getall(),
            'position' => $this->employee_model->positionall(),
            'lat' =>$lat,
            'lng'=>$lng,
            'tabs_action' => $this->session->userdata('ss_tabs')
        );
        //$this->log($arr_land_info);
        /*         * * tabs view ** */
        $data = array(
            'view_tab1' => $this->load->view('tabs/tab_owner', $arr_land_info, true),
            'view_tab2' => $this->load->view('tabs/tab_land', $arr_land_info, true),
            'view_tab3' => $this->load->view('tabs/tab_location', $arr_land_info, true),
            'view_tab4' => $this->load->view('tabs/tab_basic_inspect', $arr_land_info, true),
            'view_tab5' => $this->load->view('tabs/tab_staff_inspect', $arr_land_info, true),
            'view_tab6' => $this->load->view('tabs/tab_assign_inspect', $arr_land_info, true)
        );

        /*         * * control view ** */
        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array('สินเชื่อ' => '#', $this->title => site_url('land'));

        $data = array(
            'title' => $this->title,
            'js_other' => array('modules_land/custom_daetpicker.js', 'modules_land/land.js?r='.rand(00, 99), 'modules_land/address.js', 'modules_land/form_validate.js'),
            'css_other' => array(),
            'menu' => $this->parser->parse('page/menu', $data_menu, true),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
            'content' => $this->parser->parse('form', $data, true)
        );
        $this->parser->parse('main', $data);
    }

    /*     * * Land ** */

    public function land_save()
    {
        $this->validate_land();


        $error = false;
        $error_code = array();
        $post = $this->input->post();
        $land_array = array(
            'land_type_id' => $this->set_default($post, 'ddl_land_type', 'n'),
            'land_no' => $this->set_default($post, 'txt_land_no', 's'),
            'land_addr_no' => $this->set_default($post, 'txt_land_addr_no', 's'),
            'land_area_rai' => $this->set_default($post, 'txt_land_area_rai', 'n'),
            'land_area_ngan' => $this->set_default($post, 'txt_land_area_ngan', 'n'),
            'land_area_wah' => $this->set_default($post, 'txt_land_area_wah', 'n'),
            'land_addr_ban' => $this->set_default($post, 'txt_address_ban', 's'),
            'land_addr_moo' => $this->set_default($post, 'land_addr_moo', 's'),
            'land_addr_province_id' => $this->set_default($post, 'ddl_province', 'n'),
            'land_addr_amphur_id' => $this->set_default($post, 'ddl_amphur', 'n'),
            'land_addr_district_id' => $this->set_default($post, 'ddl_district', 'n'),
            'land_benefit' => $this->set_default($post, 'land_benefit', 'n'),
            'land_area_mortgage' => $this->set_default($post, 'ddl_land_area_mortgage', 'n'),
            'land_area_mortgage_rai' => $this->set_default($post, 'txt_land_area_mortgage_rai', 'n'),
            'land_area_mortgage_ngan' => $this->set_default($post, 'txt_land_area_mortgage_ngan', 'n'),
            'land_area_mortgage_wa' => $this->set_default($post, 'txt_land_area_mortgage_wah', 'n'),
            //tab 1
            'land_position_north_long_m' => $this->set_default($post, 'txt_north_m', 'n'),
            'land_position_north_long_wa' => $this->set_default($post, 'txt_north_wa', 'n'),
            'land_position_north_long_jd' => $this->set_default($post, 'txt_north_jd', 's'),
            'land_position_south_long_m' => $this->set_default($post, 'txt_south_m', 'n'),
            'land_position_south_long_wa' => $this->set_default($post, 'txt_south_wa', 'n'),
            'land_position_south_long_jd' => $this->set_default($post, 'txt_south_jd', 's'),
            'land_position_east_long_m' => $this->set_default($post, 'txt_east_m', 'n'),
            'land_position_east_long_wa' => $this->set_default($post, 'txt_east_wa', 'n'),
            'land_position_east_long_jd' => $this->set_default($post, 'txt_east_jd', 's'),
            'land_position_west_long_m' => $this->set_default($post, 'txt_west_m', 'n'),
            'land_position_west_long_wa' => $this->set_default($post, 'txt_wast_wa', 'n'),
            'land_position_west_long_jd' => $this->set_default($post, 'txt_wast_jd', 's'),
            'land_type_quality' => $this->set_default($post, 'txt_land_quality', 's'),
            'land_buiding' => $this->set_default($post, 'txt_land_buiding_detail', 's'),
            'land_location' => $this->set_default($post, 'txt_land_location_detail', 's'),
            'land_irigation' => $this->set_default($post, 'txt_land_irigation', 's'),
            'land_product' => $this->set_default($post, 'txt_land_product', 's'),
            'land_mortagage' => $this->set_default($post, 'rdo_mortagage', 'n'),
            'land_mortagage_price' => $this->set_default($post, 'txt_land_mortagage', 's'),
            'land_condition_price_drop' => empty($post['land_condition_price_drop']) ? '' : join(",", $post['land_condition_price_drop']),
            'land_condition_price_drop13_text' => $this->set_default($post, 'txt_condition_price_drop13', 's'),
            'land_reservation' => $this->set_default($post, 'rdo_reserve', 'n'),
            'land_reservation_location' => $this->set_default($post, 'txt_reserve_location', 's'),
            'land_reservation_desc' => $this->set_default($post, 'txt_reserve_zone', 's'),
            'land_reservation_map' => $this->set_default($post, 'rdo_map_check', 's'),
            'land_reservation_checkdate' => empty($post['txt_map_check_date']) ? null : convert_ymd_en($post['txt_map_check_date']),
            'land_of_other' => $this->set_default($post, 'txt_land_other', 's'),
            'land_other' => $this->set_default($post, 'txt_other', 's'),
            'land_latitude' => $this->set_default($post, 'txt_lat', 's'),
            'land_longitude' => $this->set_default($post, 'txt_long', 's'),
            'land_reservation_checkdesc' => $this->set_default($post, 'txt_unable_reserve', 's'),
            'land_reservation_other' => $this->set_default($post, 'land_reservation_other', 's')
        );

        $land_record = array(
            'land_id' => $this->land_id,
            'land_record_writeat' => $this->set_default($post, 'txt_write_at', 's'),
            'land_record_writedate' => !empty($post['txtDate']) ? convert_ymd_en($post['txtDate']) : null,
            'land_record_inspector1' => $this->set_default($post, 'ddl_inspector_1', 's'),
            'land_record_inspector1_position_id' => $this->set_default($post, 'ddl_insp_position', 'n'),
            'land_record_inspector2' => $this->set_default($post, 'ddl_inspector_2', 's'),
            'land_record_inspector2_position_id' => $this->set_default($post, 'ddl_insp_position_2', 'n'),
            'land_record_inspector_mortagage_price' => $this->set_default($post, 'txt_sum_inspect_price', 'n'),
            'land_record_inspector_mortagage_reason' => $this->set_default($post, 'txt_reason1', 's'),
            'land_record_staff_mortagage_price' => $this->set_default($post, 'txt_sum_staff_price', 'n'),
            'land_record_staff_mortagage_reason' => $this->set_default($post, 'txt_reason2', 's')
        );


        if (isset($post['result_basic'])) {
            for ($i = 0; $i < count($post['result_basic']); $i++) {
                if (!empty($post['txt_land_assess_rai'][$i]) || !empty($post['txt_land_assess_ngan'][$i]) || !empty($post['txt_land_assess_wa'][$i])) {
                    $land_assess = array(
                        'land_id' => $this->land_id,
                        'land_assess_zone' => $this->set_default($post['txt_land_assess_zone'], $i, 's'),
                        'land_assess_unit' => $this->set_default($post['txt_land_assess_unit'], $i, 's'),
                        'land_assess_area_rai' => $this->set_default($post['txt_land_assess_rai'], $i, 'n'),
                        'land_assess_area_ngan' => $this->set_default($post['txt_land_assess_ngan'], $i, 'n'),
                        'land_assess_area_wah' => $this->set_default($post['txt_land_assess_wa'], $i, 'n'),
                        'land_assess_price_pre_max' => $this->set_default($post['txt_land_assess_price_pre_max'], $i, 'd'),
                        'land_assess_price_pre_min' => $this->set_default($post['txt_land_assess_price_pre_min'], $i, 'd'),
                        'land_assess_price' => $this->set_default($post['land_assess_price'], $i, 'd'),
                        'land_assess_price_basic' => $this->set_default($post['result_basic'], $i, 'd'),
                        'land_assess_inspector_price' => isset($post['land_assess_inspect_price']) ? $this->set_default($post['land_assess_inspect_price'], $i, 'd') : '0.00',
                        'land_assess_inspector_sum' => isset($post['result_inspect_basic'])  ? $this->set_default($post['result_inspect_basic'], $i, 'd') : '0.00',
                        'land_assess_staff_price' => isset($post['land_assess_assign_price']) ?  $this->set_default($post['land_assess_assign_price'], $i, 'd') : '0.00',
                        'land_assess_staff_sum' => isset($post['result_assign_basic_sum']) ? $this->set_default($post['result_assign_basic_sum'], $i, 'd') : '0.00',
                        'land_assess_staff_total' =>isset($post['price_org_assign_toal']) ? $this->set_default($post, 'price_org_assign_toal', 'd') : '0.00',
                        'land_assess_inspector_total' =>isset($post['price_org_toal']) ? $this->set_default($post, 'price_org_toal', 'd') : '0.00',
                    );
                    $land_res = $this->landassess_model->save($post['hidden_land_assess_id'][$i], $land_assess);
                    if (empty($land_res['id'])) {
                        $error = true;
                        $error_code[0] = 'E01';
                    }
                }
            }//loop get assess
        }//land assess



        $files = $this->upload_files($this->land_id, 'assets/files/land/', $_FILES['file_localtion']);

        //savefile map
        $file_name = $this->input->post('filesland');
        foreach ($files as $name) {
            if (empty($file_name)) {
                $file_name = $name;
            } else {
                $file_name = $file_name . ',' . $name;
            }
        }
        $land_array['land_map_file'] = $file_name;

        $land_save = $this->land_model->save($this->land_id, $land_array);
        if (empty($land_save['id'])) {
            if ($error === false) {
                $error = true;
                $error_code[1] = 'E01';
            }
        }



        //$this->land_model->save($this->land_id, $arr_land_file);


        $land_rec_save = $this->landrecord_model->save($post['hidden_record_id'], $land_record);
        if (empty($land_rec_save['id'])) {
            if ($error === false) {
                $error = true;
                $error_code[2] = 'E02';
            }
        }

        //get error
        if ($error) {
            $err_code = implode(':', $error_code);
            $this->session->set_flashdata('message', $err_code.'พบข้อผิดพลาดกรุณาตรวจสอบข้อมูลอีกครั้ง');
        } else {
            $this->session->set_flashdata('message', 'บันทึกข้อมูลเรียบร้อย');
        }
        $this->get_tabs($post['tabs']);
        redirect(site_url('land/form/' . $this->loan_id . '/' . $this->land_id));

        /*
        echo $this->session->flashdata('message');

        print('<pre>');
        print_r($land_array);
        print_r($land_record);
        print('</pre>');

        $this->get_tabs($post['tabs']);
        $url = site_url('land/form/' . $this->loan_id . '/' . $this->land_id);

        echo '<a href="' . $url . '">--> Back <--</a>';
        */
    }
    public function assess_delete($id)
    {
        if (isset($id)) {
            $this->load->model('landassess_model');
            $this->landassess_model->del($id);
        }


        redirect('land/form/' . $this->loan_id . '/' . $this->land_id . '#table_price');
    }

    /*
     *Land Owner *
     */

    public function owner($owner_id = -1, $person_id = -1)
    {
        $this->session->set_userdata('owner_id', $owner_id);
        $this->session->set_userdata('owner_person_id', $person_id);

        $this->validate_land();

        //innit load
        $arr_owner = $this->landowner_model->get_id($owner_id);
        $arr_person = $this->person_model->getById($person_id);


        $arr_data = array(
            'close_url' => site_url('land/form/' . $this->loan_id . '/' . $this->land_id),
            'title_name' => $this->title_model->all(),
            'person_relation' => $this->relationship_model->all(),
            'province_data' => $this->province_model->all(),
            'owner_data' => $arr_owner,
            'person_data' => $arr_person
        );

        /*         * * control view ** */
        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array('สินเชื่อ' => '#', $this->title => site_url('land'));

        $data = array(
            'title' => $this->title,
            'js_other' => array('modules_land/address.js', 'modules_land/custom_daetpicker.js', 'modules_land/land.js?r=2', 'modules_land/form_validate.js', 'additional-methods.js'),
            'css_other' => array(),
            'menu' => $this->parser->parse('page/menu', $data_menu, true),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
            'content' => $this->parser->parse('owner', $arr_data, true)
        );
        $this->parser->parse('main', $data);
    }

    public function save_owner($owner_id = '', $person_id = '')
    {
        $this->validate_land();


        if (!empty($this->input->post())) {
            $owner_id = $this->input->post('owner_id');
            $person_id = $this->input->post('person_id');

            //check dup
            if ($person_id == '' && $this->landowner_model->check_thaiid($this->input->post('txt_card_id'), $this->land_id) > 0) {
                $this->session->set_flashdata('message', 'ไม่สามรถบันทึกข้อมูลได้ ข้อมูลนี้มีอยู่แล้ว');
                redirect('land/owner/');
            }

            $arr_person = array(
                'title_id' => $this->input->post('ddl_title'),
                'person_fname' => $this->input->post('txt_owner_name'),
                'person_lname' => $this->input->post('txt_owner_last_name'),
                'person_thaiid' => str_replace('-', '', $this->input->post('txt_card_id')),
                'person_birthdate' => convert_ymd_en($this->input->post('txt_owner_birthday')),
                'person_addr_card_no' => $this->input->post('txt_owner_addr_no'),
                'person_addr_card_moo' => $this->input->post('txt_owner_addr_moo'),
                'person_addr_card_road' => $this->input->post('txt_owner_addr_road'),
                'person_addr_card_province_id' => $this->input->post('ddl_owner_province'),
                'person_addr_card_amphur_id' => $this->input->post('ddl_amphur_owner'),
                'person_addr_card_district_id' => $this->input->post('ddl_district_owner'),
                'person_addr_card_zipcode' => $this->input->post('txt_owner_zipcode'),
            );
            $person_save = $this->person_model->save($arr_person, $person_id);

            if ($person_save['rows'] > 0) {
                $person_id = $person_save['id'];
                $arr_owner = array(
                    'land_id' => $this->land_id,
                    'person_id' => $person_id,
                    'land_owner_father_title' => $this->input->post('ddl_title_father'),
                    'land_owner_mather_title' => $this->input->post('ddl_title_mather'),
                    'land_owner_father_fname' => $this->input->post('txt_owner_fa_fname'),
                    'land_owner_father_lname' => $this->input->post('txt_owner_fa_lname'),
                    'land_owner_mather_fname' => $this->input->post('txt_owner_ma_fname'),
                    'land_owner_mather_lname' => $this->input->post('txt_owner_ma_lname'),
                    'land_owner_age' => $this->input->post('txt_owner_age'),
                    'land_owner_relationship_id' => $this->input->post('ddl_relation'),
                );
                $owner_save = $this->landowner_model->save($owner_id, $arr_owner);

                if ($owner_save['rows'] > 0) {
                    $owner_id = $owner_save['id'];

                    //upload file
                    $files = $this->upload_files($owner_id, 'assets/files/land_owners/', $_FILES['file_owmer']);
                    if (!$files) { //error upload
                        if (!empty($owner_id) && !empty($person_id)) {
                            redirect('land/owner/' . $owner_id . '/' . $person_id);
                        } else {
                            redirect('land/owner');
                        }
                    } else {
                        //savefile name
                        $file_name = $this->input->post('filesdb');
                        foreach ($files as $name) {
                            if (empty($file_name)) {
                                $file_name = $name;
                            } else {
                                $file_name = $file_name . ',' . $name;
                            }
                        }
                        $arr_owner_file = array('land_owner_file' => $file_name);
                        $this->landowner_model->save($owner_id, $arr_owner_file);
                    }
                } //upload file
            }//insert owner
        }//insert person

        if (!empty($owner_id) && !empty($person_id)) {
            $this->session->set_flashdata('message', 'บันทึกข้อมูลเรียบร้อย');
            redirect('land/owner/' . $owner_id . '/' . $person_id);
        } else {
            redirect('land/owner');
        }
    }

    private function upload_files($id, $path, $files)
    {
        $this->load->library('encrypt');

        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'gif|jpg|png|doc|docx|pdf|jpeg'
        );

        $this->load->library('upload', $config);

        $images = array();
        $no = 0;
        foreach ($files['name'] as $key => $image) {
            if ($files['error'][$key] != 4) {
                $_FILES['images[]']['name'] = $files['name'][$key];
                $_FILES['images[]']['type'] = $files['type'][$key];
                $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['images[]']['error'] = $files['error'][$key];
                $_FILES['images[]']['size'] = $files['size'][$key];

                $fileName = date('YmdHis') . $no . md5($id);
                $no++;
                $ext = explode('.', $files['name'][$key]);
                $images[] = $fileName . '.' . end($ext);

                $config['file_name'] = $fileName;

                $this->upload->initialize($config);

                if ($this->upload->do_upload('images[]')) {
                    $this->upload->data();
                } else {
                    $this->session->set_flashdata('message', $this->upload->display_errors());
                    return false;
                }
            }//error 4
        }
        return $images;
    }

    public function delete_files_owner($owner_id, $delfile)
    {
        //file on server not delete.
        $data = $this->landowner_model->get_id($owner_id);
        if (count($data) > 0) {
            if ($data['land_owner_file']) {
                $files = explode(',', $data['land_owner_file']);
                $tmp = array();
                foreach ($files as $name) {
                    if ($name != $delfile) {
                        $tmp[] = $name;
                    }
                }

                $update = implode(",", $tmp);
                $this->landowner_model->save($owner_id, array('land_owner_file' => $update));
                $this->session->set_flashdata('message', 'ลบข้อมูลเรียบร้อย');
                redirect(site_url('land/owner/' . $this->owner_id . '/' . $this->owner_person_id));
            }
        }
    }

    public function delete_owner($landowner_id)
    {
        $this->landowner_model->del($landowner_id);
        $this->session->flashdata('message', 'ลบข้อมูลเรียบร้อย');
        redirect(site_url('land/form/' . $this->loan_id . '/' . $this->land_id));
    }

    /*     * *End Land Owner ** */

    public function get_tabs($index = '')
    {
        if (!$this->session->has_userdata('ss_tabs')) {
            $arr = array('active', '', '', '', '', '', '');
            $this->session->set_userdata('ss_tabs', $arr);
        }

        if (!empty($index)) {
            $arr = array('', '', '', '', '', '');
            $arr[$index] = 'active';
            $this->session->set_userdata('ss_tabs', $arr);
        }
        /*
         $arr = array('', '', '', '', '', '');
         $arr[0] = 'active';
         $this->session->set_userdata('ss_tabs', $arr);
         *
         */
    }

    //***service***
    public function pdf($loan_id = '', $land_id = '')
    {
        $arr_loan = $this->loan_model->get_loan($loan_id);
        $arr_land = $this->land_model->get_land($land_id);

        $arr_land['land_type_name'] = $this->land_model->get_type($arr_land['land_type_id']);
        //$this->log($arr_land);
        //exit;

        $arr_prov = $this->province_model->getById($arr_land['land_addr_province_id']);
        $arr_amph = $this->amphur_model->getById($arr_land['land_addr_amphur_id']);
        $arr_dist = $this->district_model->getById($arr_land['land_addr_district_id']);
        $arr_land['land_area_mortgage_text'] = $this->land_area_mortgage[$arr_land['land_area_mortgage']];
        $arr_land['province_name'] = '';
        $arr_land['amphur_name'] = '';
        $arr_land['district_name'] = '';
        if (isset($arr_prov)) {
            $arr_land['province_name'] = $arr_prov['province_name'];
        }
        if (isset($arr_amph)) {
            $arr_land['amphur_name'] = $arr_amph['amphur_name'];
        }
        if (isset($arr_dist)) {
            $arr_land['district_name'] = $arr_dist['district_name'];
        }
        if (isset($arr_land['land_mortagage'])) {
            $arr_land['land_mortagage'] = $this->land_mortagage[$arr_land['land_mortagage']];
        }

        if (isset($arr_land['land_condition_price_drop'])) {
            $arr_land['land_condition_price_drop'] = explode(',', $arr_land['land_condition_price_drop']);
        }

        $arr_record = $this->landrecord_model->get_by_land($land_id);

        if (!empty($arr_record)) {
            $arr_emp = $this->employee_model->emp_position($arr_record['land_record_inspector1_position_id']);
            $arr_emp1_name = $this->employee_model->getEmployeeByid($arr_record['land_record_inspector1']);
            $arr_emp = $this->employee_model->emp_position($arr_record['land_record_inspector2_position_id']);
            $arr_emp2_name = $this->employee_model->getEmployeeByid($arr_record['land_record_inspector2']);
            $arr_record['emp1_name'] = '';
            $arr_record['emp2_name'] = '';
            $arr_record['emp1_pos_name'] = '';
            $arr_record['emp2_pos_name'] = '';

            if (isset($arr_emp1_name)) {
                $arr_record['emp1_name'] = $arr_emp1_name['employee_fname'] . '&nbsp;' . $arr_emp1_name['employee_lname'];
            }
            if (isset($arr_emp2_name)) {
                $arr_record['emp2_name'] = $arr_emp2_name['employee_fname'] . '&nbsp;' . $arr_emp2_name['employee_lname'];
            }
            if (isset($arr_emp)) {
                $arr_record['emp1_pos_name'] = $arr_emp['position_name'];
            }
            if (isset($arr_emp)) {
                $arr_record['emp2_pos_name'] = $arr_emp['position_name'];
            }
        }

        $data = array(
            'loan_data' => $arr_loan,
            'land_data' => $arr_land,
            'loanco' => $this->loan_co_model->report_list(array('loan_id' => $loan_id)),
            'land_owner' => $this->landowner_model->get_by_land_id($land_id),
            'land_record' => $arr_record,
            'land_assess' => $this->landassess_model->GetAssessById($land_id),
                // 'employee' => $arr_employee
        );

        //$data = $this->info($loan_id, $land_id);
        /*
          print('<pre>');
          print_r($data);
          print('</pre>');
          exit;
         */

        ob_start();
        $this->load->library('tcpdf');


        $filename = 'land_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->Settitle($filename); //  กำหนด title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->AddPage();

        $this->load->model('pdf_model');
        $pdf  =    $this->pdf_model->send($pdf, $data);

        $pdf->Output($filename . '.pdf', 'I');
    }

    public function gen_data_word($loan_id = '', $land_id = '')
    {
        $arr_loan = $this->loan_model->get_loan($loan_id);
        $arr_land = $this->land_model->get_land($land_id);

        $arr_land['land_type_name'] = $this->land_model->get_type($arr_land['land_type_id']);

        $arr_prov = $this->province_model->getById($arr_land['land_addr_province_id']);
        $arr_amph = $this->amphur_model->getById($arr_land['land_addr_amphur_id']);
        $arr_dist = $this->district_model->getById($arr_land['land_addr_district_id']);
        $arr_land['land_area_mortgage_text'] = $this->land_area_mortgage[$arr_land['land_area_mortgage']];
        $arr_land['province_name'] = '';
        $arr_land['amphur_name'] = '';
        $arr_land['district_name'] = '';
        if (isset($arr_prov)) {
            $arr_land['province_name'] = $arr_prov['province_name'];
        }
        if (isset($arr_amph)) {
            $arr_land['amphur_name'] = $arr_amph['amphur_name'];
        }
        if (isset($arr_dist)) {
            $arr_land['district_name'] = $arr_dist['district_name'];
        }
        if (isset($arr_land['land_mortagage'])) {
            $arr_land['land_mortagage'] = $this->land_mortagage[$arr_land['land_mortagage']];
        }

        if (isset($arr_land['land_condition_price_drop'])) {
            $arr_land['land_condition_price_drop'] = explode(',', $arr_land['land_condition_price_drop']);
        }

        $arr_record = $this->landrecord_model->get_by_land($land_id);

        if (!empty($arr_record)) {
            $arr_emp = $this->employee_model->emp_position($arr_record['land_record_inspector1']);
            // echo "<pre>";
            // print_r( $arr_emp);
            // echo "</pre>";
            $arr_emp1_name = $this->employee_model->getEmployeeByid($arr_record['land_record_inspector1']);
            $arr_emp2 = $this->employee_model->emp_position($arr_record['land_record_inspector2']);
            // echo "<pre>";
            // print_r($arr_emp2);
            // echo "</pre>";
            $arr_emp2_name = $this->employee_model->getEmployeeByid($arr_record['land_record_inspector2']);
            $arr_record['emp1_name'] = '';
            $arr_record['emp2_name'] = '';
            $arr_record['emp1_pos_name'] = '';
            $arr_record['emp2_pos_name'] = '';

            if (isset($arr_emp1_name)) {
                $arr_record['emp1_name'] = $arr_emp1_name['employee_fname'] . ' ' . $arr_emp1_name['employee_lname'];
            }
            if (isset($arr_emp2_name)) {
                $arr_record['emp2_name'] = $arr_emp2_name['employee_fname'] . ' ' . $arr_emp2_name['employee_lname'];
            }
            if (isset($arr_emp)) {
                $arr_record['emp1_pos_name'] = $arr_emp['position_name'];
            }
            if (isset($arr_emp2)) {
                $arr_record['emp2_pos_name'] = $arr_emp2['position_name'];
            }
        }

        $data = array(
            'loan_data' => $arr_loan,
            'land_data' => $arr_land,
            'land_type_data' =>$this->land_model->get_type_list(),
            'loanco' => $this->loan_co_model->report_list(array('loan_id' => $loan_id)),
            'land_owner' => $this->landowner_model->get_by_land_id($land_id),
            'land_record' => $arr_record,
            'land_assess' => $this->landassess_model->GetAssessById($land_id)
        );
        return $data;
    }

    private function ReplaceEmpty($str)
    {
        return str_replace(' ', '&nbsp;', $str);
    }

    private function land_area($arr_land)
    {
        $rai = 0;
        $ngan = 0;
        $wa = 0;
        foreach ($arr_land as $val) {
            $rai = $rai + $val['land_assess_area_rai'] * 400;
            $ngan = $ngan + $val['land_assess_area_ngan'] * 100;
            $wa = $wa + floatval($val['land_assess_area_wah']);
        }
        $total_in_wah = $rai + $ngan + $wa;
        $total_rai = intval($total_in_wah / 400);
        $total_ngan = intval(($total_in_wah % 400)/100);
        $total_wah = ($total_in_wah % 400)%100;

        $total = '';
        if (isset($rai) || isset($ngan) || isset($wa)) {
            $wa_total = floatval($wa);// % floatval(100.00);
            $ngan_total = ($ngan + ($wa / 100)) % 4;
            //$rai_total = $rai + intval($ngan + intval($wa / 100)  / 4);
            $rai_total = $rai + intval(($ngan + intval($wa / 100)) / 4);
            $total = $total_rai . ' ไร่  ' . $total_ngan . ' งาน ' . $total_wah . ' ตารางวา';
        }
        return $total;
    }


    private function land_area2($arr_land)
    {
        $rai = 0;
        $ngan = 0;
        $wa = 0;
        foreach ($arr_land as $val) {
            $rai = $rai + $val['land_assess_area_rai'] * 400;
            $ngan = $ngan + $val['land_assess_area_ngan'] * 100;
            $wa = $wa + floatval($val['land_assess_area_wah']);
        }
        $total_in_wah = $rai + $ngan + $wa;
        $total_rai = intval($total_in_wah / 400);
        $total_ngan = intval(($total_in_wah % 400)/100);
        $total_wah = ($total_in_wah % 400)%100;

        $total = '';
        if (isset($rai) || isset($ngan) || isset($wa)) {
            $wa_total = floatval($wa);// % floatval(100.00);
            $ngan_total = ($ngan + ($wa / 100)) % 4;
            //$rai_total = $rai + intval($ngan + intval($wa / 100)  / 4);
            $rai_total = $rai + intval(($ngan + intval($wa / 100)) / 4);
            $total = $total_rai . '-' . $total_ngan . '-' . $total_wah ;
        }
        // echo "<pre>total=";
        // print_r($total);
        // echo "</pre>";
        return $total;
    }


    public function word($loan_id = '', $land_id = '')
    {
        include_once('word/class/tbs_class.php');
        include_once('word/class/tbs_plugin_opentbs.php');

        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $data = $this->gen_data_word($loan_id, $land_id);
        // echo "<pre>";
        // print_r($data['land_record']);
        // echo "</pre>";


        $land_type_name_list = '';
        foreach ($data['land_type_data'] as $list) {
            $select = $list['land_type_id'] == $data['land_data']['land_type_id'] ? '√' : '';
            $land_type_name_list .= ' ( '.$select.' ) '.$list['land_type_name'];
        }

        $land_co_person_list = '';
        foreach ($data['loanco'] as $list) {
            $land_co_person_list = $list['title_name'] . $list['person_fname'] . ' ' . $list['person_lname'].', ';
        }
        if (!empty($land_co_person_list)) {
            $land_co_person_list = substr($land_co_person_list, 0, -2);
        }


        $land_reservation_map = $data['land_data']['land_reservation_map'] == '1' ? '( √ ) แผนที่ภูมิประเทศ ' : '( ) แผนที่ภูมิประเทศ ';
        $land_reservation_map .= $data['land_data']['land_reservation_map'] == '2' ? '( √ ) แผนที่ระวางรูปถ่ายทางอากาศ' : '( ) แผนที่ระวางรูปถ่ายทางอากาศ';

        // Retrieve the user name to display
        $GLOBALS['personname'] = $data['loan_data']['title_name'].''.$data['loan_data']['person_fname'].' '.$data['loan_data']['person_lname'];
        $GLOBALS['land_type_name_list'] = $land_type_name_list;
        $GLOBALS['land_co_person_list'] = $land_co_person_list;
        $GLOBALS['land_no'] = $data['land_data']['land_no'] ? num2Thai($data['land_data']['land_no']) : '-';

        $GLOBALS['land_area_rai'] = $data['land_data']['land_area_rai'] ? num2Thai($data['land_data']['land_area_rai']) : '-';
        $GLOBALS['land_area_ngan'] = $data['land_data']['land_area_ngan'] ? num2Thai($data['land_data']['land_area_ngan']) : '-';
        $GLOBALS['land_area_wah'] = $data['land_data']['land_area_wah'] ? num2Thai($data['land_data']['land_area_wah']) : '-';


        $GLOBALS['land_area_rai2'] = ($data['land_data']['land_area_mortgage'] == 2) && $data['land_data']['land_area_rai'] ? num2Thai($data['land_data']['land_area_rai']) : '-';
        $GLOBALS['land_area_ngan2'] = ($data['land_data']['land_area_mortgage'] == 2) && $data['land_data']['land_area_ngan'] ? num2Thai($data['land_data']['land_area_ngan']) : '-';
        $GLOBALS['land_area_wah2'] = ($data['land_data']['land_area_mortgage'] == 2) && $data['land_data']['land_area_wah'] ? num2Thai($data['land_data']['land_area_wah']) : '-';
        $GLOBALS['land_addr_ban'] = $data['land_data']['land_addr_ban'] ? num2Thai($data['land_data']['land_addr_ban']) : '-';
        $GLOBALS['land_addr_no'] = $data['land_data']['land_addr_no'] ? num2Thai($data['land_data']['land_addr_no']): '-';
        //= $data['land_data']['land_addr_no'] ? num2Thai($data['land_data']['land_addr_no'].' '.$data['land_data']['land_addr_ban']) : '-';
        $GLOBALS['land_addr_moo'] = $data['land_data']['land_addr_moo'] ? num2Thai($data['land_data']['land_addr_moo']) : '-';
        $GLOBALS['land_addr_district_name'] = $data['land_data']['district_name'] ? $data['land_data']['district_name'] : '-';
        $GLOBALS['land_addr_amphur_name'] = $data['land_data']['amphur_name'] ? $data['land_data']['amphur_name'] : '-';
        $GLOBALS['land_addr_province_name'] = $data['land_data']['province_name'] ? $data['land_data']['province_name'] : '-';
        $GLOBALS['land_area_mortgage_text'] = $data['land_data']['land_area_mortgage']==2 ? $data['land_data']['land_area_mortgage_text'] : '';
        $GLOBALS['land_record_writeat'] = $data['land_record']['land_record_writeat'] ? $data['land_record']['land_record_writeat'] : '-';
        $GLOBALS['land_record_writedate'] = $data['land_record']['land_record_writedate'] ? num2Thai(thai_display_date($data['land_record']['land_record_writedate'])) : '-';
        $GLOBALS['land_record_emp1_name'] = $data['land_record']['emp1_name'] ? $data['land_record']['emp1_name'] : '-';
        $GLOBALS['land_record_emp1_pos_name'] = $data['land_record']['emp1_pos_name'] ? $data['land_record']['emp1_pos_name'] : '-';
        $GLOBALS['land_record_emp2_name'] = $data['land_record']['emp2_name'] ? $data['land_record']['emp2_name'] : '-';
        $GLOBALS['land_record_emp2_pos_name'] = $data['land_record']['emp2_pos_name'] ? $data['land_record']['emp2_pos_name'] : '-';
        $GLOBALS['land_north_m'] = $data['land_data']['land_position_north_long_m'] ? num2Thai($data['land_data']['land_position_north_long_m']) : '-';
        $GLOBALS['land_north_w'] = $data['land_data']['land_position_north_long_wa'] ? num2Thai($data['land_data']['land_position_north_long_wa']) : '-';
        $GLOBALS['land_north_jd'] = $data['land_data']['land_position_north_long_jd'] ? num2Thai($data['land_data']['land_position_north_long_jd']) : '-';
        $GLOBALS['land_south_m'] = $data['land_data']['land_position_south_long_m'] ? num2Thai($data['land_data']['land_position_south_long_m']) : '-';
        $GLOBALS['land_south_w'] = $data['land_data']['land_position_south_long_wa'] ? num2Thai($data['land_data']['land_position_south_long_wa']) : '-';
        $GLOBALS['land_south_jd'] = $data['land_data']['land_position_south_long_jd'] ? num2Thai($data['land_data']['land_position_south_long_jd']) : '-';
        $GLOBALS['land_east_m'] = $data['land_data']['land_position_east_long_m'] ? num2Thai($data['land_data']['land_position_east_long_m']) : '-';
        $GLOBALS['land_east_w'] = $data['land_data']['land_position_east_long_wa'] ? num2Thai($data['land_data']['land_position_east_long_wa']) : '-';
        $GLOBALS['land_east_jd'] = $data['land_data']['land_position_east_long_jd'] ? num2Thai($data['land_data']['land_position_east_long_jd']) : '-';
        $GLOBALS['land_west_m'] = $data['land_data']['land_position_west_long_m'] ? num2Thai($data['land_data']['land_position_west_long_m']) : '-';
        $GLOBALS['land_west_w'] = $data['land_data']['land_position_west_long_wa'] ? num2Thai($data['land_data']['land_position_west_long_wa']) : '-';
        $GLOBALS['land_west_jd'] = $data['land_data']['land_position_west_long_jd'] ? num2Thai($data['land_data']['land_position_west_long_jd']) : '-';
        $GLOBALS['land_area_mortgage_rai'] = $data['land_data']['land_area_mortgage_rai'] ? num2Thai($data['land_data']['land_area_mortgage_rai']) : '-';
        $GLOBALS['land_area_mortgage_ngan'] = $data['land_data']['land_area_mortgage_ngan'] ? num2Thai($data['land_data']['land_area_mortgage_ngan']) : '-';
        $GLOBALS['land_area_mortgage_wa'] = $data['land_data']['land_area_mortgage_wa'] ? num2Thai($data['land_data']['land_area_mortgage_wa']) : '-';
        $GLOBALS['land_type_quality'] = $data['land_data']['land_type_quality'] ? num2Thai($data['land_data']['land_type_quality']) : '-';
        $GLOBALS['land_building'] = $data['land_data']['land_buiding'] ? num2Thai($data['land_data']['land_buiding']) : '-';
        $GLOBALS['land_location'] = $data['land_data']['land_location'] ? num2Thai($data['land_data']['land_location']) : '-';
        $GLOBALS['land_irigation'] = $data['land_data']['land_irigation'] ? num2Thai($data['land_data']['land_irigation']) : '-';
        $GLOBALS['land_product'] = $data['land_data']['land_product'] ? num2Thai($data['land_data']['land_product']) : '-';
        $GLOBALS['land_mortagage'] = $data['land_data']['land_mortagage'] ? num2Thai($data['land_data']['land_mortagage']) : '-';
        $GLOBALS['land_mortagate_price'] = $data['land_data']['land_mortagage_price'] ? $data['land_data']['land_mortagage_price'] : '';
        $GLOBALS['land_condition_price_drop13_text'] = $data['land_data']['land_condition_price_drop13_text'] ? num2Thai($data['land_data']['land_condition_price_drop13_text']) : '-';
        $GLOBALS['land_reservation_location'] = $data['land_data']['land_reservation_location'] ? num2Thai($data['land_data']['land_reservation_location']) :'-';
        $GLOBALS['land_reservation_desc'] = $data['land_data']['land_reservation_desc'] ? $data['land_data']['land_reservation_desc'] : '-';
        $GLOBALS['land_reservation_map'] = $land_reservation_map;
        $GLOBALS['land_reservation_checkdesc'] = $data['land_data']['land_reservation_checkdesc'] ? $data['land_data']['land_reservation_checkdesc'] : '-';
        $GLOBALS['land_reservation_checkdate'] = empty($data['land_data']['land_reservation_checkdate']) ? '-' : num2Thai(conver_date_en_th($data['land_data']['land_reservation_checkdate']));
        $GLOBALS['land_of_other'] = $data['land_data']['land_of_other'] ? $data['land_data']['land_of_other'] : '-';
        $GLOBALS['land_other'] = $data['land_data']['land_other'] ? $data['land_data']['land_other'] : '-';
        $GLOBALS['land_assess_area'] = num2Thai($this->land_area2($data['land_assess']));
        $GLOBALS['land_record_ins_mor_price'] = num2Thai(number_format($data['land_record']['land_record_inspector_mortagage_price'], 2));
        $GLOBALS['land_record_ins_mor_price_text'] = num2wordsThai(number_format($data['land_record']['land_record_inspector_mortagage_price'], 2));
        $GLOBALS['land_record_ins_mor_price_reason'] = $data['land_record']['land_record_inspector_mortagage_reason'] ? $data['land_record']['land_record_inspector_mortagage_reason'] : '-';
        $GLOBALS['land_record_staff_mor_price'] = num2Thai(number_format($data['land_record']['land_record_staff_mortagage_price'], 2));
        $GLOBALS['land_record_staff_mor_price_text'] = num2wordsThai(number_format($data['land_record']['land_record_staff_mortagage_price'], 2));
        $GLOBALS['land_record_staff_mor_price_reason'] = $data['land_record']['land_record_staff_mortagage_reason'] ? $data['land_record']['land_record_staff_mortagage_reason'] : '-';
        $GLOBALS['land_type_name'] = $data['land_data']['land_type_name'];

        $template = 'word/template/3_land.docx';
        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

        $land_map_file = null;
        if (isset($data['land_data']['land_map_file'])) {
            $data['land_data']['land_map_file'] = @explode(',', $data['land_data']['land_map_file']);
            $land_map_file = !empty($data['land_data']['land_map_file'][0]) ? $data['land_data']['land_map_file'][0] : null;
        }

        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_area_mortgage1#', ($data['land_data']['land_area_mortgage']==1 ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 1));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_area_mortgage2#', ($data['land_data']['land_area_mortgage']==2 ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 2));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop1#', (in_array(1, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 3));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop2#', (in_array(2, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 4));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop3#', (in_array(3, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 5));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop4#', (in_array(4, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 6));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop5#', (in_array(5, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 7));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop6#', (in_array(6, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 8));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop7#', (in_array(7, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 9));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop8#', (in_array(8, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 10));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop9#', (in_array(9, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 11));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop10#', (in_array(10, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 12));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop11#', (in_array(11, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 13));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop12#', (in_array(12, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 14));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_condition_price_drop13#', (in_array(13, $data['land_data']['land_condition_price_drop']) ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 15));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_reservation1#', ($data['land_data']['land_reservation']==1 ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 16));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_reservation2#', ($data['land_data']['land_reservation']==2 ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 17));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_reservation3#', ($data['land_data']['land_reservation']==3 ? 'word/template/check.png' : 'word/template/uncheck.png'), array('unique' => 18));
        $TBS->Plugin(OPENTBS_CHANGE_PICTURE, '#pic_land_map#', (!empty($land_map_file) ? 'assets/files/land/'.$land_map_file : 'word/template/map.png'), array('unique' => 19));

        $datax = array();
        foreach ($data['land_owner'] as $val) {
            $child = $val['land_owner_father_fname'] /*. ' ' . $val['land_owner_father_lname']*/ . ' - ' . $val['land_owner_mather_fname'] /*. ' ' . $val['land_owner_mather_lname']*/;
            $relation = $this->relationship_model->getById($val['land_owner_relationship_id']);
            $arr_title = $this->title_model->getById($val['title_id']);
            if (isset($relation)) {
                $val['relationship_name'] = $relation['relationship_name'];
            }
            if (isset($arr_title)) {
                $val['title_name'] = $arr_title['title_name'];
            }
            $datax[] = array('personname'=> $val['title_name'] . '' . $val['person_fname'] . ' ' . $val['person_lname'],
                            'age'=>num2Thai(age($val['person_birthdate'])),
                            'child'=>$child,
                            'addr'=>num2Thai(show_address_master('person_addr_pre', $val)),
                            'relationship'=>$val['relationship_name']);
        }
        $TBS->MergeBlock('land_owner', $datax);

        $datax = array();
        $land_assess_inspector_total = 0;
        foreach ($data['land_assess'] as $val) {
            $land_assess_inspector_total = $val['land_assess_inspector_total'];
            $datax[] = array('zone'=> num2Thai($val['land_assess_zone']),
                            'unit'=>num2Thai($val['land_assess_unit']),
                            'area'=>num2Thai($val['land_assess_area_rai'] . '-' . $val['land_assess_area_ngan'] . '-' . $val['land_assess_area_wah']),
                            'price_max'=>num2Thai(number_format($val['land_assess_price_pre_max'], 2)),
                            'price_min'=>num2Thai(number_format($val['land_assess_price_pre_min'], 2)),
                            'price'=>num2Thai(number_format($val['land_assess_price'], 2)),
                            'price_basic'=>num2Thai(number_format($val['land_assess_price_basic'], 2)));
        }
        $TBS->MergeBlock('land_assess1', $datax);
        $GLOBALS['land_assess_inspector_total'] = num2Thai(number_format($land_assess_inspector_total, 2));

        $datax = array();
        foreach ($data['land_assess'] as $val) {
            $datax[] = array('zone'=> num2Thai($val['land_assess_zone']),
                            'unit'=>num2Thai($val['land_assess_unit']),
                            'price'=>num2Thai(number_format($val['land_assess_inspector_price'], 2)),
                            'area'=>num2Thai($val['land_assess_area_rai'] . '-' . $val['land_assess_area_ngan'] . '-' . $val['land_assess_area_wah']),
                            'sum'=>num2Thai(number_format($val['land_assess_inspector_sum'], 2)));
        }
        $TBS->MergeBlock('land_assess2', $datax);

        $datax = array();
        foreach ($data['land_assess'] as $val) {
            $datax[] = array('zone'=> num2Thai($val['land_assess_zone']),
                            'unit'=>num2Thai($val['land_assess_unit']),
                            'price'=>num2Thai(number_format($val['land_assess_staff_price'], 2)),
                            'area'=>num2Thai($val['land_assess_area_rai'] . '-' . $val['land_assess_area_ngan'] . '-' . $val['land_assess_area_wah']),
                            'sum'=>num2Thai(number_format($val['land_assess_staff_sum'], 2)));
        }
        $TBS->MergeBlock('land_assess3', $datax);

        // Delete comments
        $TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
        $output_file_name = 'land_'.date('Y-m-d').'.docx';
        $temp_file = tempnam(sys_get_temp_dir(), 'Docx');

        $TBS->Show(OPENTBS_FILE, $temp_file);
        $this->send_download($temp_file, $output_file_name);
    }

    public function send_download($temp_file, $file)
    {
        $basename = basename($file);
        $length   = sprintf("%u", filesize($temp_file));

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $basename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $length);
        ob_clean();
        flush();
        set_time_limit(0);
        readfile($temp_file);
        exit();
    }
    public function save_latlong()
    {
        $land_id = $_POST['land_id'];
        $latlng = $_POST['latlng'];
        $row = $this->landlatitude_model->saveby_landid($land_id, array('land_latitude_val'=>$latlng));
        if ($row>0) {
            echo true;
        } else {
            echo false;
        }
    }


    public function emp_position($emp)
    {
        $arr = $this->employee_model->emp_position($emp);
        echo $arr['employee_position_id'];
    }
    public function amphur($province = '', $default = '')
    {
        if ($province == '') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');
            $data['selected'] = $default;
            $data['amphur'] = $this->amphur->getByprovince($province);
        }

        $this->parser->parse('amphur', $data);
    }

    public function district($amphur = '', $default = '')
    {
        if ($amphur == '') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');
            $data['selected'] = $default;
            $data['district'] = $this->district->getByamphur($amphur);
        }

        $this->parser->parse('district', $data);
    }

    public function amphur_owner($province = '', $default = '')
    {
        if ($province == '') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');
            $data['selected'] = $default;
            $data['amphur'] = $this->amphur->getByprovince($province);
        }

        $this->parser->parse('amphur_owner', $data);
    }

    public function district_owner($amphur = '', $default = '')
    {
        if ($amphur == '') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');
            $data['selected'] = $default;
            $data['district'] = $this->district->getByamphur($amphur);
        }

        $this->parser->parse('district_owner', $data);
    }

    public function zipcode($province, $amphur, $district)
    {
        $arr = $this->zipcode_model->get($province, $amphur, $district);
        echo $arr['zipcode'];
    }

    public function person_service()
    {
        $arr_person = $this->person_model->search($this->input->post('thaiid'));
        echo json_encode($arr_person);
    }

    /*
     * set defaule value
     * s is dataype is string
     * n is number
     *
     */
    private function set_default($post, $index, $type = 's')
    {
        if ($type === 's') {
            if (isset($post[$index])) {
                return $post[$index];
            } else {
                return '';
            }
        } elseif ($type === 'n') {
            if (isset($post[$index])) {
                if (!empty($post[$index])) {
                    return str_replace(',', '', $post[$index]);
                } else {
                    return '0';
                }
            } else {
                return '0';
            }
        } elseif ($type === 'd') {
            if (isset($post[$index])) {
                if (!empty($post[$index])) {
                    return floatval(str_replace(',', '', $post[$index]));
                } else {
                    return '0.00';
                }
            } else {
                return '0.00';
            }
        }
    }

    //end services
    private function log($arr)
    {
        print('<pre>');
        print_r($arr);
        print('</pre>');
    }
}
