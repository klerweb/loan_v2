<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class pdf_model extends CI_Model {

  

    function __construct() {
        parent::__construct();
        
        $this->load->helper('address_helper');
        $this->load->helper('land_helper');
        $this->load->helper('date_helper');
        $this->load->helper('currency_helper');
        
        $this->load->model('relationship/relationship_model');
        $this->load->model('title/title_model');
       
    }

    public function send($pdf,$data) {
       
        
        $html_loanco = '';
        foreach ($data['loanco'] as $val) {
            if (empty($html_loanco)) {
                $html_loanco = $html_loanco . 'ชื่อผู้กู้ร่วม&nbsp;' . $val['title_name'] . $val['person_fname'] . '&nbsp;&nbsp;' . $val['person_lname'];
            } else {
                $html_loanco = $html_loanco . ',&nbsp;' . $val['title_name'] . $val['person_fname'] . '&nbsp;&nbsp;' . $val['person_lname'];
            }
        }
        $html_loanco = '<td width="95%">' . $html_loanco . '</td>';




        $html_landowner = '';
        foreach ($data['land_owner'] as $val) {
            $child = $val['land_owner_father_fname'] . '&nbsp;&nbsp;' . $val['land_owner_father_lname'] . '-' . $val['land_owner_mather_fname'] . '&nbsp;&nbsp;' . $val['land_owner_mather_lname'];
            $relation = $this->relationship_model->getById($val['land_owner_relationship_id']);
            $arr_title = $this->title_model->getById($val['title_id']);
            if (isset($relation)) {
                $val['relationship_name'] = $relation['relationship_name'];
            }
            if (isset($arr_title)) {
                $val['title_name'] = $arr_title['title_name'];
            }
            $html_landowner = $html_landowner . '
                       <tr>
                            <td width="25%">' . $val['title_name'] . '' . $val['person_fname'] . '&nbsp;&nbsp;' . $val['person_lname'] . '</td>
                            <td width="10%" align="center">' . num2Thai(age($val['person_birthdate'])) . '</td>
                            <td width="20%">' . $child . '</td>
                            <td width="30%">' . show_address_master('person_land_addr', $val) . '</td>
                            <td width="15%">' . $val['relationship_name'] . '</td>
                       </tr>
                    ';
        }

        $html_landowner = '<table width="100%" border="1" cellpadding="3" cellspacing="0">
                       <tr>
                            <td width="25%" align="center">ผู้ที่มีชื่อในหนังสือแสดง<br/>กรรมสิทธิในที่ดิน</td>
                            <td width="10%" align="center">อายุ (ปี) </td>
                            <td width="20%" align="center">เป็นบุตรของนาย-นาง/นางสาว</td>
                            <td width="30%" align="center">ที่อยู่</td>
                            <td width="15%" align="center">ความเกี่ยวข้องกับผู้ขอสินเชื่อ</td>
                       </tr>' . $html_landowner . '</table>
                       <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="100%">(แนบหนังสือแสดงสิทธิในที่ดินที่จะจำนองไว้กับบันทึกนี้ด้วย)</td>
                           </tr>
                        </table>';

        $html_insp = '';
        foreach ($data['land_assess'] as $val) {
            $html_insp = $html_insp . '<tr>
                                    <td align="center" width="10%">' . num2Thai($val['land_assess_zone']) . '</td>
                                    <td align="center" width="10%">' . num2Thai($val['land_assess_unit']) . '</td>
                                    <td align="center" width="20%" align="center">' . num2Thai($val['land_assess_area_rai'] . '-' . $val['land_assess_area_ngan'] . '-' . $val['land_assess_area_wah']) . '</td>
                                    <td align="center">' . num2Thai(number_format($val['land_assess_price_pre_max'], 2)) . '</td>
                                    <td align="center">' . num2Thai(number_format($val['land_assess_price_pre_min'], 2)) . '</td>
                                    <td align="center">' . num2Thai(number_format($val['land_assess_price'], 2)) . '</td>
                                    <td align="center">' . num2Thai(number_format($val['land_assess_price_basic'], 2)) . '</td>
                            </tr>';
        }

        $html_stf_insp = '';
        foreach ($data['land_assess'] as $val) {
            $html_stf_insp = $html_stf_insp . '<tr>
                                    <td align="center" width="10%">' . num2Thai($val['land_assess_zone']) . '</td>
                                    <td align="center" width="10%">' . num2Thai($val['land_assess_unit']) . '</td>                                   
                                    <td align="center" >' . num2Thai(number_format($val['land_assess_inspector_price'], 2)) . '</td>
                                    <td align="center">' . num2Thai($val['land_assess_area_rai'] . '-' . $val['land_assess_area_ngan'] . '-' . $val['land_assess_area_wah']) . '</td>
                                    <td align="center">' . num2Thai(number_format($val['land_assess_inspector_sum'], 2)) . '</td>
                                    
                            </tr>';
        }

        $html_assign_insp = '';
        foreach ($data['land_assess'] as $val) {
            $html_assign_insp = $html_assign_insp . '<tr>
                                    <td align="center" width="10%">' . num2Thai($val['land_assess_zone']) . '</td>
                                    <td align="center" width="10%">' . num2Thai($val['land_assess_unit']) . '</td>                                   
                                    <td align="center" >' . num2Thai(number_format($val['land_assess_staff_price'], 2)) . '</td>
                                    <td align="center">' . num2Thai($val['land_assess_area_rai'] . '-' . $val['land_assess_area_ngan'] . '-' . $val['land_assess_area_wah']) . '</td>
                                    <td align="center">' . num2Thai(number_format($val['land_assess_staff_sum'], 2)) . '</td>
                                    
                            </tr>';
        }




        //main html
        $htmlcontent = '		
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๓</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบบันทึกการตรวจสอบที่ดินที่จะจำนองเป็นหลักประกันการชำระหนี้ของผู้ขอสินเชื่อ
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>	
				<td width="5%"></td>
				<td width="95%">ชื่อผู้ขอสินเชื่อ&nbsp;&nbsp;' . $data['loan_data']['title_name'] . '' . $data['loan_data']['person_fname'] . '&nbsp;&nbsp;' . $data['loan_data']['person_lname'] . '</td>
			</tr>			
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="40%">หนังสือแสดงสิทธิในที่ดิน &nbsp;&nbsp;' . num2Thai($data['land_data']['land_type_name']) . '</td>
				<td width="15%">เลขที่&nbsp;&nbsp;' . num2Thai($data['land_data']['land_no']) . '</td>
				<td width="45%">เนื้อที่&nbsp;&nbsp;' . num2Thai($data['land_data']['land_area_rai'] . ' &nbsp;ไร่&nbsp;&nbsp;' . $data['land_data']['land_area_ngan'] . '&nbsp;งาน&nbsp;&nbsp;' . $data['land_data']['land_area_wah']) . '&nbsp;ตารางวา</td>
			</tr>			
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="25%">เลขที่ดิน &nbsp;' . num2Thai($data['land_data']['land_addr_no']) . '</td>
				<td width="10%">หมู่ที่&nbsp;' . num2Thai($data['land_data']['land_addr_moo']) . '</td>
				<td width="25%">ตำบล/แขวง&nbsp;' . $data['land_data']['district_name'] . '</td>
				<td width="20%">อำเภอ/เขต&nbsp;' . $data['land_data']['amphur_name'] . '</td>
				<td width="20%">จังหวัด&nbsp;' . $data['land_data']['province_name'] . '</td>
				
			</tr>			
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="25%">จะจำนอง &nbsp;' . $data['land_data']['land_area_mortgage_text'] . '</td>
				<td width="75%">เนื้อที่&nbsp;&nbsp;' . num2Thai($data['land_data']['land_area_rai'] . ' &nbsp;ไร่&nbsp;&nbsp;' . $data['land_data']['land_area_ngan'] . '&nbsp;งาน&nbsp;&nbsp;' . $data['land_data']['land_area_wah']) . '&nbsp;ตารางวา</td>
				
			</tr>			
		</table>
                             
                                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                        <td width="5%"></td>
                                                        ' . $html_loanco . '
			</tr>			
		</table>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
			</tr>			
		</table>
                                ' . $html_landowner . '
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
			<tr>
                                                        <td width="60%"></td>
                                                        <td width="40%">เขียนที่.....................................................................</td>
			</tr>
                                                <tr>
                                                        <td width="60%"></td>
                                                        <td width="40%">วันที่............เดือน............................พ.ศ...................</td>
			</tr>
		</table>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="5%"></td>
                                                    <td width="50%">ข้าพเจ้า ' . $data['land_record']['emp1_name'] . '</td>
                                                    <td width="45%">ตำแหน่ง ' . $data['land_record']['emp1_pos_name'] . '</td>
                                                </tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="5%"></td>
                                                    <td width="50%">และข้าพเจ้า ' . $data['land_record']['emp2_name'] . '</td>
                                                    <td width="45%">ตำแหน่ง ' . $data['land_record']['emp2_pos_name'] . '</td>
                                                </tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="5%"></td>
                                                    <td width="50%">และข้าพเจ้า* ' . $data['loan_data']['title_name'] . '' . $data['loan_data']['person_fname'] . '&nbsp;&nbsp;' . $data['loan_data']['person_lname'] . '</td>
                                                    <td width="45%">เจ้าของที่ดินที่เสนอขอจำนองที่ดินและผู้ขอสินเชื่อ</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">ได้ทำการตรวจสอบที่ดินแปลงเครื่องหมายดังกล่าวข้างต้นนี้แล้ว ขอเสนอรายงานการตรวจสอบเพื่อเป็นหลักฐานดังต่อไปนี้</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>
                                                    <td width="100%">๑. ตำแหน่งที่ดิน</td>
                                                </tr>
                                                <tr>
                                                    <td width="10%"></td>
                                                    <td width="100%">
                                                        <table>
                                                            <tr>
                                                                <td width="15%">ทิศเหนือ</td>
                                                                <td width="15%">ยาว</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_north_long_m']) . '&nbsp;&nbsp;เมตร หรือ</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_north_long_wa']) . '&nbsp;&nbsp;วา&nbsp;จด</td>
                                                                <td width="30%">' . num2Thai($data['land_data']['land_position_north_long_jd']) . '</td>                                                                
                                                            </tr>
                                                            <tr>
                                                                <td width="15%">ทิศใต้</td>
                                                               <td width="15%">ยาว</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_south_long_m']) . '&nbsp;&nbsp;เมตร หรือ</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_south_long_wa']) . '&nbsp;&nbsp;วา&nbsp;จด</td>
                                                                <td width="30%">' . num2Thai($data['land_data']['land_position_south_long_jd']) . '</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td width="15%">ทิศตะวันออก</td>
                                                                <td width="15%">ยาว</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_east_long_m']) . '&nbsp;&nbsp;เมตร หรือ</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_east_long_wa']) . '&nbsp;&nbsp;วา&nbsp;จด</td>
                                                                <td width="30%">' . num2Thai($data['land_data']['land_position_east_long_jd']) . '</td>                                                                
                                                            </tr>
                                                            <tr>
                                                                <td width="15%">ทิศตะวันตก</td>
                                                                <td width="15%">ยาว</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_west_long_m']) . '&nbsp;&nbsp;เมตร หรือ</td>
                                                                <td width="20%">' . num2Thai($data['land_data']['land_position_west_long_wa']) . '&nbsp;&nbsp;วา&nbsp;จด</td>
                                                                <td width="30%">' . num2Thai($data['land_data']['land_position_west_long_jd']) . '</td>                                                                
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>
                                                    <td width="30%">คิดเป็นเนื้อที่ประมาณ</td>
                                                    <td width="20%">' . num2Thai($data['land_data']['land_area_mortgage_rai']) . '&nbsp;&nbsp;ไร่ </td>
                                                    <td width="20%">' . num2Thai($data['land_data']['land_area_mortgage_ngan']) . '&nbsp;&nbsp;งาน</td>
                                                    <td width="20%">' . num2Thai($data['land_data']['land_area_mortgage_wa']) . '&nbsp;&nbsp;ตารางวา</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%"></td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%" style="border-bottom:1px solid #000;"></td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                
                                                <tr>
                                                    <td width="100%">* กรณีเจ้าของที่ดินที่เสนอขอจำนองมีมากกว่า ๑ คน ให้จัดหาผู้แทนเข้าร่วมตรวจสอบที่ดินอย่างน้อย ๑ คน</td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๒. ประเภท&nbsp;ลักษณะ&nbsp;และคุณภาพของที่ดิน&nbsp;ขณะนี้ทำประโยชน์อย่างใด&nbsp;และได้ผลดีเพียงใด</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="85%">' . $this->ReplaceEmpty($data['land_data']['land_type_quality']) . '</td>
                                                </tr>
		</table>
                 
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๓.&nbsp;มีสิ่งปลูกสร้างในที่ดินแปลงนี้หรือไม่</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="85%">' . $this->ReplaceEmpty($data['land_data']['land_buiding']) . '</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๔.&nbsp;ทำเลของที่ดินและความสะดวกในการคมนาคม</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td> 
                                                    <td width="85%">' . $this->ReplaceEmpty($data['land_data']['land_location']) . '</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๕.&nbsp;การชลประทานที่ได้รับ</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="85%">' . $this->ReplaceEmpty($data['land_data']['land_irigation']) . '</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๖. &nbsp;ผลิตผลที่ได้รับในรอบปีหนึ่ง ๆ</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>  
                                                    <td width="85%">' . $this->ReplaceEmpty($data['land_data']['land_product']) . '</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๗.&nbsp;ได้เคยจำนอง&nbsp;ขายฝาก&nbsp;หรือใช้ประกันหนี้หรือไม่&nbsp;ราคาประเมินเท่าใด</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>
                                                    <td width="85%">' . $this->ReplaceEmpty($data['land_data']['land_mortagage']) . ' &nbsp;' . ($data['land_data']['land_mortagage_price'] == 0 ? '' : num2Thai($data['land_data']['land_mortagage_price'])) . '</td>
                                                </tr>
		</table>
                
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๘.&nbsp;ลักษณะและสภาพของที่ดินที่ทำให้ราคาที่ดินต่ำลง</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>
                                                   <td width="5%">' . $this->checkmark('1', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">สภาพที่ดินเป็นบ่อลึก เนินสูง ลาดเทมาก น้ำท่วมขัง หรือเป็นพรุ</td>
                                                </tr>
		</table>
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('2', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">หน้าดินส่วนใหญ่ถูกขุดขายจนไม่สามารถใช้ประโยชน์ตามปกติได้</td>
                                                </tr>
		</table>
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('3', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ที่ดินถูกเปลี่ยนแปลงไปในทางที่เลวลงจากสภาพที่เคยใช้ประโยชน์ตามปกติอย่างมาก</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('4', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">สภาพที่ดินเลว เช่น เป็นดินเค็ม ดินเปรี้ยว หรือมีหิน กรวด ทราย เป็นจำนวนมาก เป็นต้น</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('5', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ตั้งอยู่ในเขตผู้ร้ายชุกชุม หรือในเขตครอบงำของผู้มีอิทธิพล</td>
                                                </tr>
		</table>
               <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('6', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ตั้งอยู่ในเขตอันตราย เช่น ชายแดนที่อาจถูกรุกราน หรือสนามซ้อมยิงปืนของทหาร</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('7', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ตั้งอยู่ในเขตชุมชนที่ไม่มีทางเข้า-ออก หรือทางเข้า-ออก ไม่สะดวก</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('8', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ตั้งอยู่ในบริเวณที่ได้รับมลพิษจากโรงงาน เช่น น้ำเสีย ควันพิษ กลิ่น หรือเสียงรบกวน เป็นต้น</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('9', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">เนื้อที่ส่วนใหญ่มีสายไฟฟ้าแรงสูงผ่านพื้นที่</td>
                                                </tr>
		</table>
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('10', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ติดภาระจำยอมต่าง ๆ ตามกฎหมาย เช่น ใช้เป็นทางเดินผ่าน หรือที่เลี้ยงสัตว์สาธารณะ</td>
                                                </tr>
		</table>
               <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('11', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">มีภาระผูกพันจากการให้เช่าเป็นเวลานาน หรือมีผู้เช่าจำนวนมาก</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('12', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">ตั้งอยู่ในบริเวณที่อาจถูกเวนคืนหรือเพิกถอนเอกสารสิทธิ</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="5%">' . $this->checkmark('13', $data['land_data']['land_condition_price_drop']) . '</td>
                                                    <td width="85%">อื่นๆ &nbsp;' . $data['land_data']['land_condition_price_drop13_text'] . '</td>
                                                </tr>
		</table>
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๙. ที่ดินอยู่ในเขตที่ทางราชการสงวนไว้หรือไม่</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="85%">เขตสงวนหวงห้ามของทางราชการ&nbsp;เช่น เขตทหาร&nbsp;ป่าสงวนแห่งชาติ&nbsp;อุทยานแห่งชาติ&nbsp;ที่สาธารณประโยชน์</td>
                                                </tr>
                                                 <tr>
                                                    <td colspan="2" width="100%">หรือเขตปฏิรูปที่ดิน&nbsp;เป็นต้น</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="15%"></td>                   
                                                    <td width="5%">' . ($data['land_data']['land_reservation'] == '1' ? $this->addChecbox() : $this->addUnChecbox()) . '</td>
                                                    <td width="80%">อยู่ในเขต (ระบุ)</td>
                                                </tr>
                                                 <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="5%">' . ($data['land_data']['land_reservation'] == '2' ? $this->addChecbox() : $this->addUnChecbox()) . '</td>
                                                    <td width="80%">ไม่อยู่ในเขตพื้นที่สงวนหวงห้ามของทางราชการ </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="5%">' . ($data['land_data']['land_reservation'] == '3' ? $this->addChecbox() : $this->addUnChecbox()) . '</td>
                                                    <td width="80%">อาจจะเป็นที่ดินในพื้นที่คาบเกี่ยวกับเขต</td>
                                                </tr>
                                                 <tr>
                                                    <td width="15%"></td>   
                                                    <td width="5%"></td>
                                                    <td width="85%">โดยได้ตรวจสอบจาก ' . ($data['land_data']['land_reservation_map'] == '1' ? $this->addChecbox() : $this->addUnChecbox()) . ' แผนที่ภูมิประเทศ ' . ($data['land_data']['land_reservation_map'] == '2' ? $this->addChecbox() : $this->addUnChecbox()) . ' แผนที่ระวางรูปถ่ายทางอากาศ</td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>              
                                                    <td width="5%"></td>
                                                    <td width="85%">๙.๒ ไม่สามารถตรวจสอบได้ เนื่องจาก&nbsp;' . $data['land_data']['land_reservation_checkdesc'] . '</td>
                                                </tr>
		</table>
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๑๐.&nbsp;รายละเอียดอื่นๆ </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="85%">๑๐.๑&nbsp;กรณีผู้ขอสินเชื่อนำที่ดินของบุคคลอื่นที่ไม่ใช่คู่สมรสมาจำนอง มีเหตุผลความจำเป็นอย่างไร</td>
                                                </tr>
                                                <tr>                                                                                                    
                                                    <td colspan="2"  width="100%">และมีลักษณะกู้เงินแทนบุคคลอื่นหรือไม่</td>
                                                </tr>
                                                <tr>           
                                                    <td width="15%"></td>  
                                                    <td width="85%">' . $data['land_data']['land_of_other'] . '</td>
                                                </tr>
		</table>
                 <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๑๐.&nbsp;รายละเอียดอื่นๆ </td>
                                                </tr>
                                                <tr>
                                                    <td width="15%"></td>                                                    
                                                    <td width="85%">*๑๐.๒	อื่นๆ (ระบุ)</td>
                                                </tr>
                                                
                                                <tr>           
                                                    <td width="15%"></td>  
                                                    <td width="85%">' . $data['land_data']['land_other'] . '</td>
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๑๑.&nbsp;การประเมินราคาที่ดินขั้นต้น</td>
                                                </tr>
                                                <tr><td colspan="2"></td></tr>
		</table>
                <table width="100%" border="1" cellpadding="3" cellspacing="0">
			
				<tr>
					
					<td colspan="2" align="center" width="20%">บริเวณที่ดินตั้งอยู่</td>
					<td align="center" width="20%">เนื้อที่จำนอง</td>
					<td colspan="2" align="center" width="30%">ราคาซื้อขายในปัจจุบัน</td>
					<td align="center" width="15%">ราคาประเมินทุนทรัพย์ที่ดินของ<br/>กรมธนารักษ์<br/>ไร่ล่ะ<br/>(บาท)</td>
					<td align="center" width="15%">ราคาประเมินขั้นต้น<div align="center" style="border-bottom: 1px solid #ccc;" >=(๔+๕&#247;๒)+(๖)</div><span align="center" height="5">๒</span></td>
				</tr>
				<tr>
					<td align="center" width="10%">โซน/บล็อก<br/>(๑)</td>
					<td align="center" width="10%">หน่วยที่<br/>(๒)</td>
					<td align="center" width="20%">(ไร่/งาน/ตารางวา)<br/>(๓)</td>
					<td align="center" width="15%">อย่างสูง<br/>ไร่ล่ะ<br/>(บาท)<br/>(๔)</td>
					<td align="center" width="15%">อย่างต่ำ<br/>ไร่ล่ะ<br/>(บาท)<br/>(๕)</td>
					<td align="center" width="15%">(๖)</td>
					<td align="center" width="15%">(๗)</td>
				</tr>
                 
			' . $html_insp . '
                            <tr>
                                    <td align="center" width="20%" colspan="2">รวมทั้งแปลง</td>                                    
                                    <td align="center" width="20%" align="center">' . num2Thai($this->land_area($data['land_assess'])) . '</td>
                                    <td align="center">xx</td>
                                    <td align="center">xx</td>
                                    <td align="center">xx</td>
                                    <td align="center">xx</td>
                            </tr>
                        </table> 
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">๑๒.&nbsp;ความเห็นของเจ้าหน้าที่ผู้ตรวจสอบประเมินราคาที่ดินขั้นต้น</td>
                                                </tr>
                                                <tr><td colspan="2"></td></tr>
		</table>
                <table width="100%" border="1" cellpadding="3" cellspacing="0">
			
				<tr>
					
					<td colspan="2" align="center" width="20%">บริเวณที่ดินตั้งอยู่</td>
					<td align="center" width="20%" rowspan="2">บจธ. ควรประเมินราคาไร่ละ (บาท)</td>					
					<td align="center" width="30%" rowspan="2">เนื้อที่จำนอง(ไร่/งาน/ตารางวา)</td>
					<td align="center" width="30%" rowspan="2">รวมเป็นเงิน (บาท) = (๓) x (๔)</td>
				</tr>
				<tr>
					<td align="center" width="10%">โซน/บล็อก<br/>(๑)</td>
					<td align="center" width="10%">หน่วยที่<br/>(๒)</td>
					
				</tr>
                 
			' . $html_stf_insp . '
                            
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">บจธ. ควรประเมินราคาจำนองที่ดิน เนื้อที่&nbsp;&nbsp;' . num2Thai($this->land_area($data['land_assess'])) . '</td>
                                                </tr>
                                                
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">เป็นเงินจำนวน ' . num2Thai(number_format($data['land_record']['land_record_inspector_mortagage_price'], 2)) . ' &nbsp;('.num2wordsThai(number_format($data['land_record']['land_record_inspector_mortagage_price'], 2)).')</td>                                                    
                                                  </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">เหตุผลที่ประเมินราคาสูงหรือต่ำกว่าราคาตามข้อ ๑๒ &nbsp;' . $data['land_record']['land_record_inspector_mortagage_reason'] . '</td>                                                    
                                                  </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%" style="border-bottom:1px solid #000;"></td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                
                                                <tr>
                                                    <td width="100%">*&nbsp;ให้ตรวจสอบโดยระบุให้ชัดเจนว่า&nbsp;ใครเป็นผู้ครอบครองและทำประโยชน์อยู่ในที่ดิน&nbsp;ครอบครองในฐานะเป็นเจ้าของ&nbsp;เป็นผู้เช่า&nbsp;<br/>หรือครอบครอง&nbsp;โดยแสดงการเป็นเจ้าของโดยเปิดเผยในลักษณะการครอบครองปรปักษ์ตามประมวลกฎหมายแพ่งและพาณิชย์
หรือโดยประการอื่น&nbsp;ๆ&nbsp;รวมทั้งระยะเวลาที่ได้ครอบครองและทำประโยชน์มาแล้ว&nbsp;หากมีเหตุผิดปกติไม่ควรรับจำนอง
</td>                                                    
                                                </tr>
		</table>
                ';

        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        if ($pdf->GetY() > 190) {

            $pdf->AddPage();
        }

        $htmlcontent = '
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr><td></td><td></td></tr>
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">บันทึกการตรวจสอบที่ดินข้างต้นนี้&nbsp;เจ้าหน้าที่&nbsp;บจธ.&nbsp;ผู้ตรวจสอบได้อ่านให้ผู้เกี่ยวข้องในบันทึกนี้ทราบโดยตลอดแล้ว</td>
                                                </tr>
                                                
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                
                                                <tr>
                                                    <td width="100%">รับรองว่าถูกต้อง&nbsp;จึงพร้อมกันลงลายมือชื่อไว้เป็นสำคัญ</td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                
                                                <tr>
                                                    <td width="50%" align="center">ลงชื่อ..............................................................ผู้ขอสินเชื่อ</td>   
                                                    <td width="50%"align="center">ลงชื่อ..............................................................*เจ้าของที่ดิน</td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" align="center">(...................................................................)</td>   
                                                    <td width="50%"align="center">(...................................................................)ผู้ขอจำนอง</td>
                                                </tr>
                                                
                                                <tr><td></td><td></td></tr>
                                                <tr>
                                                    <td width="50%" align="center">ลงชื่อ..............................................................ผู้ตรวจสอบ</td>   
                                                    <td width="50%"align="center">ลงชื่อ..............................................................*เจ้าของที่ดิน</td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" align="center">(...................................................................)/ประเมินราคา</td>   
                                                    <td width="50%"align="center">(...................................................................)ผู้ขอจำนอง</td>
                                                </tr>
                                                <tr><td></td><td></td></tr>
                                                <tr>
                                                    <td width="50%" align="center">ลงชื่อ..............................................................**เจ้าของที่ดิน</td>   
                                                    <td width="50%"align="center">ลงชื่อ..............................................................ข้างเคียง</td>
                                                </tr>
                                                <tr>
                                                    <td width="50%" align="center">(...................................................................)**ผู้ครอบครอง</td>   
                                                    <td width="50%"align="center">(...................................................................) ที่ดินจำนอง</td>
                                                </tr>
		</table>
		';

        //echo $htmlcontent;
        //exit;


        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');

        $pdf->AddPage();

        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					บันทึกของเจ้าหน้าที่ผู้ได้รับมอบอำนาจ
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>
                <table width="100%" border="1" cellpadding="3" cellspacing="0">
			
				<tr>
					
					<td colspan="2" align="center" width="20%">บริเวณที่ดินตั้งอยู่</td>
					<td align="center" width="20%" rowspan="2">ประเมินราคาไร่ละ (บาท)</td>					
					<td align="center" width="30%" rowspan="2">เนื้อที่จำนอง(ไร่/งาน/ตารางวา)</td>
					<td align="center" width="30%" rowspan="2">รวมเป็นเงิน (บาท) = (๓) x (๔)</td>
				</tr>
				<tr>
					<td align="center" width="10%">โซน/บล็อก<br/>(๑)</td>
					<td align="center" width="10%">หน่วยที่<br/>(๒)</td>
					
				</tr>
                 
			' . $html_assign_insp . '
                            
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="10%"></td>                                                    
                                                    <td width="90%">บจธ. ควรประเมินราคาจำนองที่ดิน เนื้อที่&nbsp;&nbsp;' . num2Thai($this->land_area($data['land_assess'])) . '</td>
                                                </tr>
                                                
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">เป็นเงินจำนวน ' . num2Thai(number_format($data['land_record']['land_record_staff_mortagage_price'], 2)) . ' &nbsp;('.num2wordsThai(number_format($data['land_record']['land_record_staff_mortagage_price'], 2)).')</td>                                                    
                                                  </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">เหตุผลที่ประเมินราคาสูงหรือต่ำกว่าราคาตามข้อ  ๑๑ (๗) &nbsp;' . $data['land_record']['land_record_staff_mortagage_reason'] . '</td>                                                    
                                                  </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center">ลงชื่อ.........................................................</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center">(.........................................................)</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center">ตำแหน่ง..............................................</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center">วันที่..............................................</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%" style="border-bottom:1px solid #000;"></td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">* &nbsp;ในกรณีที่ผู้ขอสินเชื่อนำที่ดินของบุคคลอื่นมาจำนองจะต้องให้เจ้าของที่ดินทุกรายที่ขอจำนองลงลายมือชื่อด้วย</td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100%">**&nbsp;ในกรณีที่ดินที่ขอจำนองเจ้าของที่ดินไม่ได้ครอบครองทำประโยชน์ด้วยตนเอง&nbsp;ให้เจ้าของที่ดินข้างเคียงอย่างน้อย&nbsp;๑&nbsp;คน</td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100%">และผู้ครอบครองทำประโยชน์ที่ดินปัจจุบันลงลายมือชื่อ&nbsp;เพื่อรับทราบการจำนองที่ดินดังกล่าวด้วย</td>                                                    
                                                </tr>
		</table>
               <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................</td>
                                                    <td width="50%" align="center">เจ้าหน้าที่ผู้ตรวจสอบ/ประเมินราคาที่ดิน</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................</td>
                                                    <td width="50%" align="center">เจ้าหน้าที่ผู้ตรวจสอบ/ประเมินราคาที่ดิน และเขียนแผนที่</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
		</table>
                ';

        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');

        $pdf->AddPage();

        $htmlcontent = '
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					คำรับรองการเป็นเจ้าของและทำเลที่ตั้งของที่ดิน
				</td>
			</tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                           <td width="70%"></td>
				<td width="30%">
					วันที่..........................................
				</td>
			</tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                           <td width="5%"></td>
				<td width="90%">ข้าพเจ้าขอรับรองว่าที่ดินประเภท&nbsp;'.$data['land_data']['land_type_name'].'&nbsp;เลขที่&nbsp;'.num2Thai($data['land_data']['land_no']).'&nbsp;เลขที่ดิน&nbsp;'.num2Thai($data['land_data']['land_addr_no']).'</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                           
				<td width="100%">ตำบล/แขวง&nbsp;'.$data['land_data']['district_name'].'&nbsp;ตำบล/แขวง&nbsp;'.$data['land_data']['amphur_name'].'&nbsp;จังหวัด&nbsp;'.$data['land_data']['province_name'].'&nbsp;เนื้อที่&nbsp;'.num2Thai($data['land_data']['land_area_rai']).'&nbsp;ไร่&nbsp;'.num2Thai($data['land_data']['land_area_ngan']).'&nbsp;งาน&nbsp;'.num2Thai($data['land_data']['land_area_wah']).'&nbsp;ตารางวา</td>
			</tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                           
				<td width="100%">ที่ขอเสนอจำนองต่อ บจธ. นั้น มีทำเลที่ตั้งของที่ดินแสดงตามแผนที่ข้างบนนี้ และขอรับรองว่าที่ดินดังกล่าวข้างต้น</td>
			</tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                           
				<td width="100%">เป็นของ&nbsp;'.$data['loan_data']['title_name'] . '' . $data['loan_data']['person_fname'] . '&nbsp;&nbsp;' . $data['loan_data']['person_lname'].'</td>
			</tr>
			
		</table><table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
                                                           
				<td width="100%">หากไม่เป็นความจริงให้ใช้คำรับรองนี้เป็นหลักฐานในการดำเนินอาญากับข้าพเจ้าได้</td>
			</tr>
			
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................</td>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center">(...............................................................)</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................</td>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center">(...............................................................)</td>                                                    
                                                  </tr>
                                                  
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%" style="border-bottom:1px solid #000;"></td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="30%" ><b>คำแนะนำ</b></td>                                                    
                                                </tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">๑.&nbsp;ในแผนที่ให้แสดงเส้นทางคมนาคมและระยะทางอย่างต่อเนื่องจากสถานที่สำคัญที่เป็นถาวรวัตถุใกล้เคียง (ระบุชื่อ) </td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100%">'.$this->ReplaceEmpty('เช่น สถานที่ราชการ วัด โรงเรียน ชุมชน เป็นต้น จนถึงที่ตั้งของที่ดินหรือใกล้เคียงที่ดินที่สุด พร้อมทั้งระบุชื่อเส้นทางคมนาคม').'</td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100%">และระบุว่าเส้นทางนี้เชื่อมโยงระหว่างหมู่บ้าน&nbsp;หรือชุมชนอะไร</td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100%">๒.&nbsp;เขียนรูปแปลงที่ดินโดยระบุความยาวของแต่ละด้านโดยประมาณให้ใกล้เคียงความจริงมากที่สุด&nbsp;และระบุชื่อเจ้าของที่ดินข้าง<br/>เคียงคนปัจจุบัน&nbsp;ตลอดจนผู้เช่าหรือผู้ทำประโยชน์ในที่ดินข้างเคียงให้ชัดเจน</td>                                                    
                                                </tr>
                                                <tr>
                                                    <td width="100%">๓.&nbsp;ให้เจ้าของที่ดินผู้ขอจำนองและผู้ขอสินเชื่อลงลายมือชื่อรับรองเป็นเจ้าของและทำเลที่ตั้งของที่ดิน</td>                                                    
                                                </tr>
		</table>
            ';

        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        
        $pdf->Addpage();
        
        $htmlcontent = '                
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="100%" align="center"><b>หนังสือให้ความยินยอมของคู่สมรส</b></td></tr>
                    <tr><td width="100%" align="center"></td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="60%"></td><td width="50%">เขียนที่............................................................</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="60%"></td><td width="50%">วันที่..............................................................</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="10%"></td><td width="95%">ข้าพเจ้ายินยอมให้คู่สมรสของข้าพเจ้าทำนิติกรรมจำนองที่ดินแปลงที่ปรากฏในหน้าแรกของบันทึกการตรวจสอบที่ดิน</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr><td width="100%">ที่จะจำนองเป็นหลักประกันการชำระหนี้ของผู้ขอสินเชื่อฉบับนี้&nbsp;รวมทั้งการทำนิติกรรมจดทะเบียนระหว่างจำนองทุกประเภทของที่ดินแปลงดังกล่าวต่อไปในภายหน้ากับสถาบันบริหารจัดการธนาคารที่ดิน&nbsp;(องค์การมหาชน)&nbsp;ด้วย</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................ผู้ให้คำยินยอม</td>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................ผู้ให้คำยินยอม</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center">(...............................................................)</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................ผู้ให้คำยินยอม</td>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................ผู้ให้คำยินยอม</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center">(...............................................................)</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................พยาน</td>
                                                    <td width="50%" align="center">ลงชื่อ...............................................................พยาน</td>                                                    
                                                  </tr>
                                                  <tr>
                                                    <td width="50%" align="center">(...............................................................)</td>
                                                    <td width="50%" align="center">(...............................................................)</td>                                                    
                                                  </tr>
                                                   <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr> <tr>
                                                    <td width="50%" align="center"></td>
                                                    <td width="50%" align="center"></td>                                                    
                                                  </tr>
		</table>
                ';
        
         //$pdf->SetFont('thsarabun', '', 12);
        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        
               $htmlcontent = '
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr><td width="100%" ><span style="text-decoration: underline;">หมายเหตุ</span>&nbsp;:&nbsp;กรณีที่ดินที่จำนองมีผู้ถือกรรมสิทธิ์หลายคนให้คู่สมรสทำหนังสือให้ความยินยอมของคู่สมรสในกรอบเดียวกันแต่ถ้าคู่สมรสทำหนังสือให้ความยินยอมของคู่สมรสต่างวันหรือต่างสถานที่กันให้จัดทำหนังสือให้ความยินยอมของคู่สมรสแยกกรอบกัน
</td></tr>
                            </table>
                ';
        $pdf->SetFont('thsarabun', '', 12);
        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        $pdf->AddPage();
        
        $htmlcontent ='<table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr><td width="100%" align="center"><b>การบันทึกรายการในบันทึกการตรวจสอบที่ดินฯ</b></td></tr>
                <tr><td width="100%" align="center"></td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="20%" align="left"><span style="text-decoration: underline; font-weight:bold;">ชื่อผู้ขอสินเชื่อ/ผู้กู้ร่วม</span></td>
                    <td width="75%">บันทึก&nbsp;ชื่อ-นามสกุลของผู้ขอสินเชื่อและผู้กู้ร่วม&nbsp;(ถ้ามี)&nbsp;โดยการจำนองที่ดิน</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="35%" align="left"><span style="text-decoration: underline;"><b>ประเภทและเลขที่หนังสือแสดงสิทธิในที่ดิน</b></span></td>
                    <td width="55%">ให้ระบุประเภทของหนังสือแสดงสิทธิในที่ดิน&nbsp;เช่น&nbsp;โฉนดที่ดิน</td>
                </tr>
                <tr><td colspan="3">โฉนดตราจอง&nbsp;น.ส.&nbsp;๓&nbsp;ก&nbsp;น.ส.&nbsp;๓&nbsp;&nbsp;แบบหมายเลข&nbsp;๓&nbsp;เป็นต้น&nbsp;สำหรับเลขที่ให้ระบุเลขที่ของหนังสือแสดงสิทธิในที่ดินนั้น</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="7%" align="left"><span style="text-decoration: underline;"><b>เนื้อที่</b></span></td>
                    <td width="88%">ให้บันทึกเนื้อที่คงเหลือทั้งหมดของที่ดินแปลงนั้น ตามที่ปรากฏในหนังสือแสดงสิทธิในที่ดินแม้ว่าจะ</td>
                </tr>
                <tr><td colspan="3">เป็นการขอจำนองที่ดินเฉพาะส่วนก็ตาม</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="7%" align="left"><span style="text-decoration: underline;"><b>ตั้งอยู่</b></span></td>
                    <td width="88%">ให้ระบุชื่อบ้าน&nbsp;หมู่บ้าน&nbsp;ตำบล/แขวง&nbsp;อำเภอ/เขต&nbsp;ตามที่ระบุไว้ในหนังสือแสดงสิทธิในที่ดิน&nbsp;กรณีที่ชื่อสถานที่ตั้ง</td>
                </tr>
                <tr><td colspan="3">ของที่ดินในปัจจุบันได้เปลี่ยนแปลงไปจากที่ระบุไว้ในหนังสือแสดงสิทธิในที่ดินให้บันทึกชื่อสถานที่ตั้งในปัจจุบันไว้ในวงเล็บต่อท้ายข้อความ</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="30%" align="left"><span style="text-decoration: underline;"><b>จะจำนองทั้งแปลงหรือเฉพาะส่วน</b></span></td>
                    <td width="65%">ให้ทำเครื่องหมาย&nbsp;"/"&nbsp;ในกรอบที่เกี่ยวข้อง หากจำนองเฉพาะส่วนให้ระบุชื่อ</td>
                </tr>
                <tr><td colspan="3">เจ้าของที่ดินที่ต้องการจำนองและเนื้อที่ของที่ดินเฉพาะส่วนที่จะจำนองด้วย</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="30%" align="left"><span style="text-decoration: underline;"><b>ผู้มีชื่อในหนังสือแสดงสิทธิในที่ดิน</b></span></td>
                    <td width="65%">ให้ระบุชื่อผู้ถือกรรมสิทธิ์ในที่ดินแปลงนั้นทุกรายตามลำดับที่ปรากฏ</td>
                </tr>
                <tr><td colspan="3">ในหนังสือแสดงสิทธิในที่ดิน&nbsp;โดยระบุอายุของผู้ขอจำนอง&nbsp;ชื่อบิดา&nbsp;มารดา&nbsp;ที่อยู่ของผู้จำนองตามหลักฐานสำเนาทะเบียนบ้าน&nbsp;สำหรับการจำนองเฉพาะส่วนให้ขีดเส้นใต้รายชื่อผู้ต้องการจำนองด้วยหมึกสีแดงไว้ด้วย&nbsp;ในกรณีผู้จำนองเป็นผู้ขอสินเชื่อและผู้ขอกู้ร่วม&nbsp;(ถ้ามี)&nbsp;ให้ระบุไว้ด้วยว่าเป็นผู้ขอสินเชื่อและผู้ขอกู้ร่วม&nbsp;(ถ้ามี)&nbsp;และผู้จำนอง</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="25%" align="left"><span style="text-decoration: underline;"><b>เกี่ยวข้องกับผู้ขอสินเชื่อ</b></span></td>
                    <td width="70%">ในกรณีที่นำที่ดินของบุคคลอื่นมาจำนอง&nbsp;ให้ระบุความสัมพันธ์กับผู้ขอสินเชื่อ</td>
                </tr>
                <tr><td colspan="3">ตรงกับความเป็นจริง&nbsp;เช่น&nbsp;ภรรยา&nbsp;บิดา&nbsp;มารดา&nbsp;พี่เขย&nbsp;ลูกสะใภ้&nbsp;เป็นต้น&nbsp;ทั้งนี้&nbsp;เพื่อใช้ประกอบการพิจารณาของผู้รับมอบอำนาจว่ามีเหตุผลสมควรในการรับจำนองหรือไม่</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="10%" align="left"><span style="text-decoration: underline;"><b>เขียนที่</b></span></td>
                    <td width="80%">ให้ระบุสถานที่ตั้งของที่ดินที่ทำการสอบสวน&nbsp;เช่น&nbsp;ระบุว่า&nbsp;"ที่ตั้งของที่ดินแปลงเครื่องหมาย</td>
                </tr>
                <tr><td colspan="3">ดังกล่าวข้างต้น"</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="10%" align="left"><span style="text-decoration: underline;"><b>วันที่</b></span></td>
                    <td width="85%">ให้ระบุวัน&nbsp;เดือน&nbsp;พ.ศ.&nbsp;ที่ทำการตรวจสอบที่ดิน</td>
                </tr>
                
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="15%" align="left"><span style="text-decoration: underline;"><b>ผู้ตรวจสอบที่ดิน</b></span></td>
                    <td width="80%">ให้ระบุชื่อ&nbsp;ตำแหน่ง&nbsp;หรือสถานะของผู้ร่วมตรวจสอบที่ดิน&nbsp;ผู้ตรวจสอบที่ดินได้จะต้องเป็นไปตาม</td>
                </tr>
                <tr><td colspan="3">หลักเกณฑ์ที่กำหนดไว้</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left">กรณีที่ผู้เช่าหรือผู้ทำประโยชน์ในที่ดินแปลงข้างเคียงเป็นผู้ไปร่วมตรวจสอบที่ดิน&nbsp;ให้ระบุที่อยู่ตามสำเนาทะเบียน</td>
                    
                </tr>
                <tr><td colspan="2">บ้านของบุคคลดังกล่าวไว้ด้วย</td></tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๑.&nbsp;</b><span style="text-decoration: underline;"><b>ตำแหน่งที่ดิน</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">ให้เจ้าหน้าที่&nbsp;บจธ.&nbsp;หรือผู้ที่ได้รับมอบหมายจาก&nbsp;บจธ.&nbsp;ซึ่งเป็นผู้ทำการตรวจสอบที่ดิน&nbsp;ดำเนินการตรวจสอบและ</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('สอบถามผู้ร่วมตรวจสอบที่ดินเกี่ยวกับทำเลที่ตั้ง ตำแหน่งที่ดิน ขนาดเนื้อที่ของที่ดินที่ทำการตรวจสอบเปรียบเทียบกับรายการต่าง ๆ ที่ปรากฏในหนังสือแสดงสิทธิในที่ดินว่า ถูกต้องตรงกันหรือไม่ เพียงใด ในกรณีดำเนินการตรวจสอบแล้วปรากฏว่า ไม่ถูกต้องตรงกัน ให้ดำเนินการระงับการรับจำนอง เช่น เนื้อที่ดินจริงน้อยกว่าที่ปรากฏในหนังสือแสดงสิทธิ ตำแหน่งที่ดินที่ทำการตรวจสอบเป็นคนละแปลงกับที่ดินตามหนังสือแสดงสิทธิ เป็นต้น').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๒.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ประเภท ลักษณะ และคุณภาพของที่ดินขณะนี้ทำประโยชน์อย่างใด และได้ผลดีเพียงใด').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกตามที่ตรวจสอบได้ เช่น ที่ดินประเภทนาข้าว ไร่สับปะรด สวนมะม่วง ที่ปลูกบ้าน บ่อปลา').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('เป็นต้น ลักษณะเป็นดินร่วน ดินเหนียว ดินร่วนปนทราย ที่ลุ่ม ที่ดอน มีน้ำใช้ประโยชน์ตลอดปี มีความเหมาะสมที่จะใช้ทำประโยชน์อย่างใดบ้าง ในขณะนี้ได้ใช้<br/>ประโยชน์อย่างใด และได้ผลดีเพียงใด').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๓.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('มีสิ่งปลูกสร้างในที่ดินแปลงนี้หรือไม่').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('หากมีให้ระบุประเภทโรงเรือน ลักษณะ ขนาด อายุการใช้งานมาแล้ว ราคาที่ปลูกสร้าง มูลค่าคงเหลือ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ของสิ่งปลูกสร้างนั้น').'</td>
                </tr>
                
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('กรณีนำโรงเรือนที่ตั้งอยู่ในที่ดินมาใช้ประกอบการประเมินราคาที่ดินตามที่กำหนดไว้ให้บันทึกว่า "มีรายละเอียด').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ปรากฏตามบันทึกการประเมินราคาโรงเรือน"').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๔.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ทำเลของที่ดินและความสะดวกในการคมนาคม').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกว่า ที่ดินตั้งอยู่ห่างไกลชุมชน หรือหมู่บ้าน หรือสถานที่สำคัญเพียงใด มีสาธารณูปโภค เช่น ไฟฟ้า').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ประปา โทรศัพท์ หรือไม่ การเข้าถึงที่ดินมีเส้นทางใดบ้าง และสามารถเดินทางได้สะดวกตลอดปีหรือไม่').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๕.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('การชลประทานที่ได้รับ').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกว่า ได้รับน้ำชลประทานจากแหล่งใด เช่น โครงการชลประทานของรัฐ ระบบการชลประทานของ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ตนเอง เป็นต้น สามารถใช้น้ำเพื่อการผลิตเพียงพอตลอดปีหรือไม่ อย่างไร').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๖.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ผลิตผลที่ได้รับในรอบปีหนึ่งๆ').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกผลผลิตการเกษตรที่ได้รับจากที่ดินแปลงนั้น ๆ ถ้าเป็นที่ดินที่ใช้ทำการผลิตหลายประเภท ให้บันทึกผลผลิตที่').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ได้รับแยกไว้แต่ละประเภท').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๗.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ได้เคยจำนอง ขายฝาก หรือใช้ประกันหนี้บ้างหรือไม่ ราคาประเมินเท่าใด').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้ตรวจสอบจากสารบัญจดทะเบียนในหนังสือแสดงสิทธิในที่ดิน และสอบถามจากเจ้าของที่ดิน โดยจะต้อง').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('บันทึกด้วยว่า ได้เคยจำนอง ขายฝาก หรือใช้ประกันหนี้กับใคร เมื่อใด วงเงินที่จำนอง ขายฝาก หรือค้ำประกันหนี้  ทั้งนี้ เพื่อใช้เปรียบเทียบในการประเมินราคาที่ดินของธนาคาร').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๘.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ลักษณะและสภาพของที่ดินที่ทำให้ราคาที่ดินต่ำลง').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกข้อมูลตามลักษณะและสภาพของที่ดิน กรณีที่ดินมีลักษณะใด ลักษณะหนึ่งตามข้อนี้ หากพิจารณา').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('แล้วเห็นว่าลักษณะและสภาพของที่ดินไม่มีความเหมาะสมที่จะใช้ทำประโยชน์ได้ ให้พิจารณาระงับการรับจำนอง สำหรับที่ดินยัง<br/>สามารถใช้ทำประโยชน์ได้ ให้กำหนดราคาประเมินที่ดินโดยพิจารณาตามสมควร').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๙.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ที่ดินอยู่ในเขตที่ทางราชการสงวนไว้หรือไม่').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้ผู้ตรวจสอบที่ดินตรวจสอบด้วยว่า ที่ดินแปลงนั้นอยู่ในท้องที่ซึ่งมีเขตสงวนหวงห้ามของทางราชการ เช่น ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ป่าสงวนแห่งชาติ อุทยานแห่งชาติ เขตทหาร เป็นต้น หรือเขตสาธารณประโยชน์ตามหลักเกณฑ์ที่ระบุไว้หรือไม่ แล้วบันทึกไว้ตามสภาพที่ตรวจพบ').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp;๑๐.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('รายละเอียดอื่น ๆ').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๐.๑ กรณีผู้ขอสินเชื่อนำที่ดินของบุคคลอื่นที่ไม่ใช่คู่สมรสของผู้ขอสินเชื่อมาจำนอง').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ให้ผู้ตรวจสอบที่ดินสอบสวนการกู้เงินของผู้ขอสินเชื่อว่า มีลักษณะกู้เงินแทนเจ้าของที่ดินหรือบุคคลอื่นหรือไม่ หากมีลักษณะ<br/>ดังกล่าวให้ระงับการรับจำนอง').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๐.๒ อื่นๆ (ระบุ) ').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้ตรวจสอบโดยระบุให้ชัดเจนว่า ใครเป็นผู้ครอบครองและทำประโยชน์อยู่ในที่ดิน ครอบครองในฐานะเป็นเจ้าของ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('เป็นผู้เช่า หรือครอบครองโดยเปิดเผยในลักษณะการครอบครองปรปักษ์ตามประมวลกฎหมายแพ่งและพาณิชย์ หรือโดย<br/>ประการอื่น ๆ รวมทั้งระยะเวลาที่ได้ครอบครองทำประโยชน์มาแล้ว หากมีเหตุผิดปกติเกี่ยวกับการครอบครองที่ดิน<br/>อันอาจทำให้ บจธ. เสียหาย ให้ดำเนินการระงับการรับจำนอง และบันทึกรายการอื่น ๆ ที่สมควรบันทึกไว้ เพื่อเป็นข้อมูลประกอบการพิจารณาของเจ้าหน้าที่ บจธ. หรือผู้รับมอบอำนาจจาก บจธ. ต่อไป').'</td>
                </tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp; ๑๑.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('การประเมินราคาที่ดินขั้นต้น').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกข้อมูลที่ได้รับจากการตรวจสอบที่ดินลงในตาราง โดยพิจารณาที่ตั้งของที่ดินอยู่ในโซน/บล็อก ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('และหน่วยเดียว หรือตั้งอยู่ในโซน/บล็อก มากกว่า ๑ หน่วย').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๑.๑ บริเวณที่ดินตั้งอยู่').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้เจ้าหน้าที่ บจธ. หรือผู้ที่ได้รับมอบหมายจาก บจธ. ซึ่งเป็นผู้ทำการตรวจสอบที่ดินตรวจสอบตำแหน่งที่ตั้ง').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ที่ดินแปลงที่ประเมิน โดยพิจารณาว่า ตั้งอยู่ในโซน/บล็อกไหน หน่วยที่เท่าใด มีราคาทุนทรัพย์ในการจดทะเบียนสิทธิและนิติกรรมมากกว่า ๑ รายการ ให้บันทึกแยกแต่ละรายการให้ครบถ้วน').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๑.๒ เนื้อที่จำนอง').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกเนื้อที่ของที่ดินที่จะจำนอง ทั้งนี้ หากที่ดินดังกล่าวมีราคาประเมินทุนทรัพย์ที่ดินมากกว่า ๑ รายการ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ตามข้อ ๑๑.๑ ให้ระบุจำนวนเนื้อที่แต่ละรายการตามหนังสือรับรองของสำนักงานที่ดินที่ได้รับจากผู้ขอสินเชื่อ หรือประมาณการจำนวนเนื้อที่ดินแต่ละหน่วย/โซน/บล็อก').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๑.๓ ราคาซื้อขายในปัจจุบัน').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกโดยการสอบถามจากชาวบ้านหรือกำนัน หรือผู้ใหญ่บ้าน หรือเจ้าพนักงานที่ดินในท้องที่นั้น เพื่อทราบ').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ราคาการซื้อขายที่ดินที่มีลักษณะทำเลคล้ายคลึงกันและอยู่ใกล้เคียงกัน ที่มีการซื้อขายกันจริง เพื่อกำหนดราคาที่ควรจะเป็น<br/>ราคาซื้อขายในปัจจุบันของที่ดินแปลงนี้ ทั้งราคาขั้นสูงและราคาขั้นต่ำ กรณีที่ดินแปลงที่ขอจำนองมีราคาทุนทรัพย์ใน<br/>การจดทะเบียนสิทธิและนิติกรรมมากกว่า ๑ รายการ ให้บันทึกแยกแต่ละรายการให้สอดคล้องกัน').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๑.๔ ราคาประเมินทุนทรัพย์ที่ดินของกรมธนารักษ์').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกราคาประเมินของที่ดินแปลงนั้น โดยใช้ราคาประเมินทุนทรัพย์ที่ดินของกรมธนารักษ์ ที่มีผลใช้บังคับอยู่ใน').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ขณะนั้น ซึ่งราคาประเมินดังกล่าวสามารถขอข้อมูลได้จากส่วนราชการที่เกี่ยวข้อง ได้แก่ สำนักงานเทศบาล องค์การบริหารส่วน<br/>ตำบล ที่ว่าการอำเภอ หรือสำนักงานที่ดิน เป็นต้น').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๑.๕ ราคาประเมินขั้นต้น').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้นำราคาซื้อขายที่ดินเฉลี่ยรวมกับราคาประเมินทุนทรัพย์ที่ดินต่อไร่ในปัจจุบันหารด้วยสอง').'</td>
                </tr>                
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><b>ข้อ&nbsp; ๑๒.&nbsp;</b><span style="text-decoration: underline;"><b>'.$this->ReplaceEmpty('ความเห็นของเจ้าหน้าที่ผู้ตรวจสอบประเมินราคาที่ดินขั้นต้น').'</b></span></td>
                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('บจธ. ควรประเมินราคาไร่ละ (บาท)').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ให้พิจารณาประเมินราคาโดยใช้ข้อมูลจากราคาประเมินขั้นต้นตามข้อ ๑๑.๕ และรายละเอียดในข้ออื่น ๆ แล้วกำหนดราคาที่ บจธ. ควรประเมินต่อไร่ พร้อมราคาประเมินที่ดินทั้งแปลงไว้').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('๑๒.๒ บจธ. ควรประเมินจำนอง เนื้อที่…………. ไร่……….. งาน……………. ตารางวา').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ให้บันทึกโดยนำจำนวนเนื้อที่จำนองจากข้อ ๑๑.๒ และให้บันทึกผลรวมของจำนวนเงินที่ประเมินราคาที่ดินตาม').'</td>
                </tr>
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('ข้อ ๑๒ สดมภ์ที่ ๕ ของแบบพิมพ์ ได้ผลลัพธ์เท่าใด ให้ปัดเศษของพันบาททิ้ง ตัวอย่างได้ผลรวมของจำนวนเงินที่ประเมินราคาที่ดินเป็นเงิน ๑๒๑,๗๕๐ บาท ให้บันทึกเป็น ๑๒๑,๐๐๐ บาท').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ในกรณีที่ผู้ตรวจสอบที่ดินเห็นสมควรประเมินราคาสูงหรือต่ำกว่าราคาประเมินขั้นต้นให้ชี้แจงเหตุผล').'</td>
                </tr>                
                <tr>
                <td colspan="2">'.$this->ReplaceEmpty('และข้อมูลสนับสนุนว่า ที่ดินแปลงดังกล่าว มีทำเลที่ตั้ง ลักษณะ และสภาพของที่ดินดีเลวอย่างไร จึงสมควรประเมินราคาแตกต่างจากราคาประเมินขั้นต้น ทั้งนี้ ห้ามประเมินราคาให้เป็นไปตามผู้ขอสินเชื่อต้องการเงินสินเชื่อ').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('เมื่อตรวจสอบที่ดินแล้ว ให้ผู้ขอจำนอง เจ้าของที่ดินผู้ขอจำนองทุกคน ผู้ตรวจสอบที่ดินประเมินราคาลง').'</td>
                </tr>
                <tr>
                
                <td width="90%">'.$this->ReplaceEmpty('ลายมือชื่อไว้ด้วย').'</td>
                </tr>                             
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="5%" align="center"></td>
                    <td width="95%" align="left"><span style="text-decoration: underline;"><b>หมายเหตุ :</b></span>การบันทึกของเจ้าหน้าที่ผู้ได้รับมอบอำนาจจาก บจธ.</td>                    
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ในการพิจารณาการประเมินราคาที่ดินให้ใช้แนวทางตามข้อ ๑๑ และข้อ ๑๒ รวมทั้งรายละเอียดในข้ออื่นๆ ').'</td>
                </tr>                
                 <tr>                
                <td colspan="2">'.$this->ReplaceEmpty('ประกอบด้วย').'</td>
                </tr>
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%"><b>ตัวอย่าง</b></td>
                </tr> 
                <tr>
                <td width="10%" align="center"></td>
                <td width="90%">'.$this->ReplaceEmpty('ที่ดินเนื้อที่ ๑๐ ไร่ ๑ งาน กำหนดราคาที่ บจธ. ควรประเมินไร่ละ ๑๕,๐๐๐ บาท ราคาประเมินทั้งแปลง ').'</td>
                </tr>
                <tr>
               
                <td colspan="2">'.$this->ReplaceEmpty('เท่ากับ ๑๕,๐๐๐ × ๑๐.๒๕ = ๑๕๓,๗๕๐ บาท ปัดเศษของพันทิ้งจะได้ ๑๕๓,๐๐๐ บาท ').'</td>
                </tr>    
                <tr>
                <td width="100%" align="center">..........................................................................................</td>
                
                </tr> 
                </table>
';              
        
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        
        
        return $pdf;

    }
    
    private function ReplaceEmpty($str) {
        return str_replace(' ', '&nbsp;', $str);
    }

    private function addChecbox(){
        return '<img src="'.site_url('assets/icon/checkbox_check.png').'"/>';
    }
    private function addUnChecbox(){
        return '<img src="'.site_url('assets/icon/checkbox_uncheck.png').'"/>';
    }
    
    private function checkmark($val, $arr) {
        if (empty($arr))
            return $this->addUnChecbox ();

        if (in_array($val, $arr)) {
            return $this->addChecbox();
        }
        return '';
    }

    private function land_area_array($arr_land) {
        $rai = 0;
        $ngan = 0;
        $wa = 0;
        foreach ($arr_land as $val) {
            $rai = $rai + $val['land_assess_area_rai'];
            $ngan = $ngan + $val['land_assess_area_ngan'];
            $wa = $wa + $val['land_assess_area_wah'];
        }

        if (isset($rai) || isset($ngan) || isset($wa)) {
            $wa_total = $wa % 100.00;
            $ngan_total = ($ngan + ($wa / 100)) % 4;
            //$rai_total = $rai + intval($ngan + intval($wa / 100)  / 4);
            $rai_total = $rai + intval(($ngan + intval($wa / 100) ) / 4);
            return array(0 => $rai_total, 1 => $ngan_total, 2 => $wa_total);
        }
        return array();
    }

    //return rai/ngan/wa
    private function land_area($arr_land) {
        $rai = 0;
        $ngan = 0;
        $wa = 0;
        foreach ($arr_land as $val) {
            $rai = $rai + $val['land_assess_area_rai'];
            $ngan = $ngan + $val['land_assess_area_ngan'];
            $wa = $wa + $val['land_assess_area_wah'];
        }

        $total = '';
        if (isset($rai) || isset($ngan) || isset($wa)) {
            $wa_total = $wa % 100.00;
            $ngan_total = ($ngan + ($wa / 100)) % 4;
            //$rai_total = $rai + intval($ngan + intval($wa / 100)  / 4);
            $rai_total = $rai + intval(($ngan + intval($wa / 100) ) / 4);
            $total = $rai_total . '&nbsp; ไร่ &nbsp;' . $ngan_total . '&nbsp;งาน&nbsp;' . $wa_total . '&nbsp;วา';
        }
        return $total;
    }

}
