<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class zipcode_model extends CI_Model {
	//Table Model
	private $table = 'master_zipcode';
	
	function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
		
	}
	
	public function get($province_id,$amphur_id,$district_id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('province_id',$province_id);
		$this->db->where('amphur_id',$amphur_id);
		$this->db->where('district_id',$district_id);
		
		$result = $this->db->get();
		return $result->row_array();
		
	}
	
}