<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class land_model extends CI_Model {

    //Table Model
    private $table = 'land';
    private $id;
    private $status;
    private $create;
    private $create_by;
    private $update;
    private $update_by;
    
    

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        $this->id = $this->table . '_id';
        $this->status = $this->table . '_status';       
        $this->create = $this->table . '_createdate';
        $this->create_by = $this->table . '_createby';
        $this->update = $this->table . '_updatedate';
        $this->update_by = $this->table . '_updateby';
        $this->_active = '1';
    }

    public function get_land($land_id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->id, $land_id);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function save($id, $arr_data) {

        if ($id == '') {
            //$arr_data[$this->id] = $id;
            $arr_data[$this->create] = date("Y-m-d H:i:s");
            $arr_data[$this->create_by] = get_uid_login();
            $arr_data[$this->status] = $this->_active;
            //$arr_data = $this->field_data($arr_data);			
            $this->db->insert($this->table, $arr_data);

            $id = $this->db->insert_id();
        } else {
            $arr_data[$this->update] = date("Y-m-d H:i:s");
            $arr_data[$this->update_by] = get_uid_login();
            $this->db->where($this->id, $id);
            $this->db->update($this->table, $arr_data);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function get_type($id) {
        $this->db->select('*');
        $this->db->from('config_land_type');
        $this->db->where('land_type_id', $id);
        $res = $this->db->get();

        if (isset($res)) {
            return $res->row()->land_type_name;
        }
        return '';
    }
    
 
    public function getByLeasingId($leasing_id){
        $this->db->select('*');                                                             
        $this->db->from($this->table);
        $this->db->where('leasing_id', $leasing_id);
        $this->db->where('land_status !=', '0');
        $query = $this->db->get(); 
                                         
         
        $result =  $query->result_array();    
        
        $this->load->model('province/province_model', 'province');
        $this->load->model('amphur/amphur_model', 'amphur');
        $this->load->model('district/district_model', 'district');
        $this->load->model('land_type/land_type_model', 'land_type');     
        
        if($result){
            foreach($result as $key=>$val){
                $type_name =  $this->land_type->getById($val['land_type_id']);
                 $result[$key]['land_type_name'] = $type_name['land_type_name'];
 
               $province = $this->province->getById($val['land_addr_province_id']);
               $distinct_name = $this->district->getById($val['land_addr_district_id']);
               $amphur_name = $this->amphur->getById($val['land_addr_amphur_id']); 
               
                $result[$key]['land_province_name'] = $province['province_name'];
                $result[$key]['land_district_name'] = $distinct_name['district_name'];
                $result[$key]['land_amphur_name'] = $amphur_name['amphur_name'];       
            }
        } 
 
        return $result ; 
    }
    
    public function getByLeasingIdCount($leasing_id){
        $this->db->select('*');                                                             
        $this->db->from($this->table);
        $this->db->where('leasing_id', $leasing_id);
        $this->db->where('land_status !=', '0');
        $query = $this->db->get(); 
                                         
         
      return  $query->num_rows();  
    }
    
	public function get_type_list() {
        $this->db->select('*');
        $this->db->from('config_land_type');
        $result = $this->db->get();
 		return $result->result_array();
 
    }
    
    public function delete($id)
    {
        $array =  array("land_status" => "0");
        
        $this->db->set($array);
        $this->db->where("land_id", $id);
        $this->db->update($this->table, $array);
        
        $num_row = $this->db->affected_rows();
        
        return array('id' => $id, 'rows' => $num_row);
    }

    
}
