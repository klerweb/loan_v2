<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class employee_model extends CI_Model {
	//Table Model
	private $table = 'employee';
	private $table_position = 'config_position';
	
	function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
		
	}
	
	public function getall(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('employee_status','1');
		
		$result = $this->db->get();
		return $result->result_array();
		
	}
	
	public function positionall(){
		$this->db->select('*');
		$this->db->from($this->table_position);
		$this->db->where('position_status','1');
		
		$result = $this->db->get();
		return $result->result_array();
		
		
	}
        
    public function getEmployeeByid($id){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('employee_id',$id);
		
		$result = $this->db->get();
		return $result->row_array();
		
		
	}
	
	public function emp_position($emp){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join($this->table_position,"{$this->table}.employee_position_id={$this->table_position}.position_id",'inner');
		$this->db->where("{$this->table}.employee_id",$emp);
		
		$result = $this->db->get();
		return $result->row_array();
		
	}
    public function getPositionByid($postion_id){
        $this->db->select('*');
        $this->db->from($this->table_position);
        $this->db->where('position_id',$postion_id);
        
        $result = $this->db->get();
        return $result->row_array();
    }
}