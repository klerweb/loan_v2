<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class landAssess_model extends CI_Model {

    //Table Model
    private $_table = 'land_assess';
    private $_col_id;
    private $_col_create;
    private $_col_update;
    private $_col_create_by;
    private $_col_update_by;
    private $_status;
    private $_active = "1"; //active
    private $_inactive = "0"; //cancel

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        $this->_col_id = $this->_table . '_id';
        $this->_status = $this->_table . '_status';
        $this->_col_create = $this->_table . '_createdate';
        $this->_col_update = $this->_table . '_updatedate';
        $this->_col_create_by = $this->_table . '_createby';
        $this->_col_update_by = $this->_table . '_updateby';
    }

    public function save($id, $arr_data) {
        if ($id == '') {
            $arr_data[$this->_col_create] = date("Y-m-d H:i:s");
            $arr_data[$this->_col_create_by] = get_uid_login();
            $arr_data[$this->_status] = $this->_active;
            //$arr_data = $this->field_data($arr_data);
            $this->db->insert($this->_table, $arr_data);

            $id = $this->db->insert_id();
        } else {
            $arr_data[$this->_status] = $this->_active;
            $arr_data[$this->_col_update] = date("Y-m-d H:i:s");
            $arr_data[$this->_col_update_by] = get_uid_login();
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->_table, $arr_data);
        }
        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function GetAssessById($id) {
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where('land_id', $id);
        $this->db->where("{$this->_status} !=", $this->_inactive);
        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }

    public function del($id) {
        $arr_data[$this->_col_update] = date("Y-m-d H:i:s");
        $arr_data[$this->_col_update_by] = get_uid_login();
        $arr_data[$this->_status] = $this->_inactive;
        $this->db->where($this->_col_id, $id);
        $this->db->update($this->_table, $arr_data);
        return $this->db->affected_rows();
    }

}
