<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class search_model extends CI_Model {

    private $table_guarantee_land = 'loan_guarantee_land';
    private $table_person = 'person';
    private $table_land = 'land';
    private $table_loan = 'loan';
    private $table_loan_type = 'loan_type';
    private $table_loan_objective_id = 'master_loan_objective';
    private $table_province = 'master_province';
    private $table_amphur = 'master_amphur';
    private $table_district = 'master_district';
    private $table_assess = 'land_assess';
    public $_status_desc = array('ยกเลิก', 'แบบร่าง', 'เรียบร้อย');

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    public function get($land_no, $person_card_id, $person_fname, $person_lname) {
        $this->db->select(" ROW_NUMBER()  OVER (ORDER BY {$this->table_guarantee_land}.loan_id desc) rownum,*");
        $this->db->select("{$this->table_land}.land_id");
        $this->db->from($this->table_guarantee_land);
        $this->db->join($this->table_loan, "{$this->table_guarantee_land}.loan_id={$this->table_loan}.loan_id", "inner");
        $this->db->join($this->table_person, "{$this->table_loan}.person_id={$this->table_person}.person_id", "inner");
        $this->db->join($this->table_land, "{$this->table_guarantee_land}.land_id={$this->table_land}.land_id", "inner");
        $this->db->join($this->table_loan_type, "{$this->table_loan}.loan_id={$this->table_loan_type}.loan_id", "inner");
        $this->db->join($this->table_loan_objective_id, "{$this->table_loan_type}.loan_objective_id={$this->table_loan_objective_id}.loan_objective_id", "inner");


        $this->db->join($this->table_province, "{$this->table_land}.land_addr_province_id={$this->table_province}.province_id", 'left');
        $this->db->join($this->table_amphur, "{$this->table_land}.land_addr_amphur_id={$this->table_amphur}.amphur_id", 'left');
        $this->db->join($this->table_district, "{$this->table_land}.land_addr_district_id={$this->table_district}.district_id", 'left');
        
        $this->db->join("(select count(*) cnt,land_id from {$this->table_assess} group by land_id) land_ass", "{$this->table_land}.land_id=land_ass.land_id", 'left');


        if (!empty($land_no))
            $this->db->like("{$this->table_land}.land_no", $land_no);

        if (!empty($person_card_id))
            $this->db->like("{$this->table_person}.person_thaiid", $person_card_id);

        if (!empty($person_fname))
            $this->db->like("{$this->table_person}.person_fname", $person_fname);

        if (!empty($person_lname))
            $this->db->like("{$this->table_person}.person_lname", $person_lname);

        $this->db->where("land_ass.cnt is not null ");
        $this->db->where("({$this->table_land}.land_status='1' OR {$this->table_land}.land_status='2') ");
        //$this->db->or_where("{$this->table_loan}.loan_status", '2');
        //$this->db->where("{$this->table_assess}.land_assess_id is not null ");
        
	    if(check_permission_isowner('loan')){
	   		$this->db->where("{$this->table_land}.land_createby", get_uid_login());   
	    }
	            
        $this->db->order_by("{$this->table_loan}.loan_id", 'desc');

        $query = $this->db->get();
        //print_r($this->db->last_query());

        return $query->result_array();
    }

    public function get_add($land_no, $person_card_id, $person_fname, $person_lname) {
        $this->db->select(" ROW_NUMBER()  OVER (ORDER BY {$this->table_guarantee_land}.loan_id desc) rownum,");
        $this->db->select("{$this->table_land}.land_id");
        $this->db->select(" {$this->table_guarantee_land}.loan_id,{$this->table_guarantee_land}.land_id,");
        $this->db->select(" {$this->table_loan}.*");
        $this->db->select(" {$this->table_person}.*");
        $this->db->select(" {$this->table_land}.*");
        $this->db->select(" {$this->table_loan_type}.*");
        $this->db->select(" {$this->table_loan_objective_id}.*");
        $this->db->select(" {$this->table_province}.*");
        $this->db->select(" {$this->table_amphur}.*");
        $this->db->select(" {$this->table_district}.*");


        $this->db->from($this->table_guarantee_land);
        $this->db->join($this->table_loan, "{$this->table_guarantee_land}.loan_id={$this->table_loan}.loan_id", "inner");
        $this->db->join($this->table_person, "{$this->table_loan}.person_id={$this->table_person}.person_id", "inner");
        $this->db->join($this->table_land, "{$this->table_guarantee_land}.land_id={$this->table_land}.land_id", "inner");
        $this->db->join($this->table_loan_type, "{$this->table_loan}.loan_id={$this->table_loan_type}.loan_id", "inner");
        $this->db->join($this->table_loan_objective_id, "{$this->table_loan_type}.loan_objective_id={$this->table_loan_objective_id}.loan_objective_id", "inner");


        $this->db->join($this->table_province, "{$this->table_land}.land_addr_province_id={$this->table_province}.province_id", 'left');
        $this->db->join($this->table_amphur, "{$this->table_land}.land_addr_amphur_id={$this->table_amphur}.amphur_id", 'left');
        $this->db->join($this->table_district, "{$this->table_land}.land_addr_district_id={$this->table_district}.district_id", 'left');

        $this->db->join("(select count(*) cnt,land_id from {$this->table_assess} group by land_id) land_ass", "{$this->table_land}.land_id=land_ass.land_id", 'left');

        if (!empty($land_no))
            $this->db->like("{$this->table_land}.land_no", $land_no);

        if (!empty($person_card_id))
            $this->db->like("{$this->table_person}.person_thaiid", $person_card_id);

        if (!empty($person_fname))
            $this->db->like("{$this->table_person}.person_fname", $person_fname);

        if (!empty($person_lname))
            $this->db->like("{$this->table_person}.person_lname", $person_lname);

        $this->db->where("land_ass.cnt is null ");
        $this->db->where("({$this->table_land}.land_status='1' OR {$this->table_land}.land_status='2') ");
        //$this->db->where("{$this->table_assess}.land_assess_id is null ");
        
    	if(check_permission_isowner('loan')){
	   		$this->db->where("{$this->table_land}.land_createby", get_uid_login());   
	    }

        $this->db->order_by("{$this->table_loan}.loan_id", 'desc');

        $query = $this->db->get();


        return $query->result_array();
    }

    public function get_assess($land_id) {
        $this->db->select("land_id");
        $this->db->from('land_assess');
        $this->db->where('land_id', $land_id);
        $res = $this->db->get();
        return $res->num_rows();
    }

}

?>