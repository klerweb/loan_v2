<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class landlatitude_model extends CI_Model {
    private $table = 'land_latitude';
    private $active = '1';
            
     function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
     }
     function save($id,$arr_data){
        if($id==''){
            $arr_data['land_latitude_createdate'] = date("Y-m-d H:i:s");     
            $arr_data['land_latitude_createby'] = get_uid_login();        
            $this->db->insert($this->table, $arr_data);            
            $id = $this->db->insert_id();
        }else{
            $arr_data['land_latitude_updatedate'] = date("Y-m-d H:i:s");
            $arr_data['land_latitude_updateby'] = get_uid_login();   
            $this->db->where('land_latitude_id', $id);
            $this->db->update($this->table, $arr_data);
        }
        
        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
     }
     
     function saveby_landid($id,$arr_data){
         //update
        if(!empty($id)){           
            $arr_data['land_latitude_updatedate'] = date("Y-m-d H:i:s");
            $arr_data['land_latitude_updateby'] = get_uid_login();  
            $this->db->where('land_id', $id);
            $this->db->update($this->table, $arr_data);
        }        
        
        $num_row = $this->db->affected_rows();        
        //insert
        if($num_row==0){            
             $arr_data['land_id'] = $id;
            $arr_data['land_latitude_status'] =$this->active;
            $arr_data['land_latitude_createdate'] = date("Y-m-d H:i:s");      
            $arr_data['land_latitude_createby'] = get_uid_login();         
            $this->db->insert($this->table, $arr_data);            
            $id = $this->db->insert_id();
            $num_row = $this->db->affected_rows();
        }      

        return array('id' => $id, 'rows' => $num_row);
     }
     
     
     public function getby_landid($id){
         $this->db->select('*');
         $this->db->from($this->table);
         $this->db->where('land_id',$id);
         $res = $this->db->get();
         //print($this->db->last_query());
         return $res->result_array();
     }
}
