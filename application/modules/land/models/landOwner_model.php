<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class landOwner_model extends CI_Model {

    //Table Model
    private $_table = 'land_owner';
    private $_table_person = 'person';
    private $_table_relationship = 'config_relationship';
    private $_table_province = 'master_province';
    private $_table_amphur = 'master_amphur';
    private $_table_district = 'master_district';
    private $_col_id;
    private $_col_create;
    private $_col_update;
    private $_col_create_by;
    private $_col_update_by;
    private $_col_status;
    private $_active = "1";
    private $_inactive = "0";

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        $this->_col_id = $this->_table . '_id';
        $this->_col_status = $this->_table . '_status';
        $this->_col_create = $this->_table . '_createdate';
        $this->_col_update = $this->_table . '_updatedate';
        $this->_col_create_by = $this->_table . '_createby';
        $this->_col_update_by = $this->_table . '_updateby';
    }

    public function save($id, $arr_data) {
        if ($id == '') {
            $arr_data[$this->_col_create] = date("Y-m-d H:i:s");
            $arr_data[$this->_col_create_by] = get_uid_login();
            $arr_data[$this->_col_status] = $this->_active;
            //$arr_data = $this->field_data($arr_data);
            $this->db->insert($this->_table, $arr_data);
            $id = $this->db->insert_id();
        } else {
            $arr_data[$this->_col_update] = date("Y-m-d H:i:s");
            $arr_data[$this->_col_update_by] = get_uid_login();
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->_table, $arr_data);
        }
        $num_row = $this->db->affected_rows();
        return array('id' => $id, 'rows' => $num_row);
    }
    
    public function del($id) {
        $arr[$this->_col_status] = $this->_inactive;
        $this->db->where($this->_col_id, $id);
        $this->db->update($this->_table, $arr);
    }

    public function get_id($id) {
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where($this->_col_id, $id);
        $this->db->where($this->_col_status, $this->_active);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function check_thaiid($thaiid,$land_id) {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->join($this->_table_person, "{$this->_table}.person_id={$this->_table_person}.person_id");
        $this->db->where("{$this->_table_person}.person_thaiid", $thaiid);
        $this->db->where("{$this->_table}.land_id", $land_id);
        $this->db->where("{$this->_table}.{$this->_col_status}", $this->_active);
        $res = $this->db->get();
        
        return $res->num_rows();
    }

    public function get_by_land_id($id) {
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->join($this->_table_person, "{$this->_table}.person_id={$this->_table_person}.person_id", 'inner');
        $this->db->join($this->_table_relationship, "{$this->_table}.land_owner_relationship_id={$this->_table_relationship}.relationship_id", 'left');
        $this->db->join($this->_table_province, "{$this->_table_person}.person_addr_card_province_id={$this->_table_province}.province_id", 'left');
        $this->db->join($this->_table_amphur, "{$this->_table_person}.person_addr_card_amphur_id={$this->_table_amphur}.amphur_id", 'left');
        $this->db->join($this->_table_district, "{$this->_table_person}.person_addr_card_district_id={$this->_table_district}.district_id", 'left');

        $this->db->where("{$this->_table}.land_id", $id);
        $this->db->where("{$this->_table}.land_owner_status", $this->_active);
        $query = $this->db->get();

        return $query->result_array();
    }

}
