<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class landRecord_model extends CI_Model {

    private $_table = 'land_record';
    private $_col_id;
    private $_col_create;
    private $_col_update;
    private $_col_create_by;
    private $_col_update_by;
    private $_col_status;
    private $_active = '1';
    private $_inactive = '0';

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        $this->_col_id = $this->_table . '_id';
        $this->_col_status = $this->_table . '_status';
        $this->_col_create = $this->_table . '_createdate';
        $this->_col_update = $this->_table . '_updatedate';
        $this->_col_create_by = $this->_table . '_createby';
        $this->_col_update_by = $this->_table . '_updateby';
    }

    public function get_by_land($land_id) {
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where("{$this->_col_status} !=", $this->_inactive);
        $this->db->where('land_id', $land_id);
        $query = $this->db->get();

        //$result = $query->num_rows()!=0? $query->result_array() : array();
        //print($this->db->last_query());
        return $query->row_array();
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $this->db->from($this->_table);
        $query = $this->db->get();
        return $query->num_rows() != 0 ? ($query->row()->land_record_id) + 1 : 1;
    }

    public function field_data($arr = array()) {
        $this->load->library('DB_lib');
        $lib = new DB_lib();
        $fields = $this->db->field_data($this->_table);
        $arr_field = $lib->field_data($fields);
        foreach ($arr as $key => $val) {
            if ($key != '' && $val != '')
                $arr_field[$key] = $val;
        }
        return $arr_field;
    }

    public function save($id, $arr_data) {
        if ($id == '') {
            //$id = $this->get_max_id();
            //$arr_data[$this->_col_id] = $id;
            $arr_data[$this->_col_create] = date("Y-m-d H:i:s");
            $arr_data[$this->_col_create_by] = get_uid_login();
            $arr_data[$this->_col_status] = $this->_active;
            //$arr_data = $this->field_data($arr_data);
            $this->db->insert($this->_table, $arr_data);

            $id = $this->db->insert_id();
        } else {
            //$arr_data[$this->_col_id] = $id;
            $arr_data[$this->_col_update] = date("Y-m-d H:i:s");
            $arr_data[$this->_col_update_by] = get_uid_login();
            $this->db->where($this->_col_id, $id);
            //$arr_data = $this->field_data($arr_data);
            $this->db->update($this->_table, $arr_data);
        }
        $num_row = $this->db->affected_rows();
        return array('id' => $id, 'rows' => $num_row);
    }

}

?>