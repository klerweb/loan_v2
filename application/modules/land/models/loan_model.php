 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loan_model extends CI_Model
{
	private $table = 'loan';
	private $table_person = 'person';
                private $table_title = 'config_title';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function get_loan($loan_id)
	{
		$this->db->select(" * ");
		$this->db->from($this->table);
		$this->db->join($this->table_person,"{$this->table}.person_id={$this->table_person}.person_id","inner");
                                $this->db->join($this->table_title,"{$this->table_person}.title_id={$this->table_title}.title_id",'left');
		$this->db->where("loan_id", $loan_id);
		$query = $this->db->get();
	
		return $query->row_array();
	}
}
?>