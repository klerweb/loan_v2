<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approve extends MY_Controller
{
	public $redeem_type = array(
			'1' => 'การจำนอง',
			'2' => 'การขายฝาก',
	);
	private $title_page = 'รายงานคำขออนุมัติ';

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('analysis/loan_analysis_model', 'loan_analysis');

		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');

		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->loan_analysis->search($loan, $thaiid, $fullname)
		);

		$data = array(
				'title' => $this->title_page,
				'js_other' => array(),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('approve', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	public function word($id)
	{
		include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$this->load->model('analysis/loan_analysis_model', 'loan_analysis');
		$this->load->model('analysis/loan_model', 'loan');
		$this->load->model('person/person_model', 'person');
		$this->load->model('title/title_model', 'title');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('career/career_model', 'career');
		$this->load->model('analysis/loan_type_model', 'loan_type');
		$this->load->model('analysis/loan_co_model', 'loan_co');

		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('analysis/loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('analysis/loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('analysis/loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('analysis/loan_income_model', 'loan_income');
		$this->load->model('analysis/loan_expenditure_model', 'loan_expenditure');
		$this->load->model('analysis/loan_schedule_model', 'loan_schedule');
		$this->load->model('loan/loan_guarantee_land_model', 'loan_guarantee_land');
		$this->load->model('analysis/land_assess_model', 'land_assess');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$GLOBALS['company'] = '';
		$GLOBALS['location'] = '';
		$GLOBALS['date'] = '';

		$analysis = $this->loan_analysis->getById($id);
		// echo "<pre>";
		// print_r($analysis);
		// echo "</pre>";
		$data = $this->loan->getById($analysis['loan_id']);

		$title = $this->title->getById($data['person']['title_id']);
		$district = $this->district->getById($data['person']['person_addr_pre_district_id']);
		$amphur = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
		$province = $this->province->getById($data['person']['person_addr_pre_province_id']);
		$career = $this->career->getById($data['person']['career_id']);

		$type_name = '';
		if($analysis['loan_analysis_length']=='1')
		{
			$type_name = 'รายเดือน';
		}
		else if($analysis['loan_analysis_length']=='6')
		{
			$type_name = 'ราย ๖ เดือน';
		}
		else if($analysis['loan_analysis_length']=='12')
		{
			$type_name = 'รายปี';
		}

		$GLOBALS['person_fullname'] = $title['title_name'].$data['person']['person_fname'].' '.$data['person']['person_lname'];
		$GLOBALS['person_age'] = num2Thai(empty($data['person']['person_birthdate'])? ' - ' : age($data['person']['person_birthdate']));
		$GLOBALS['person_thaiid'] = num2Thai(formatThaiid($data['person']['person_thaiid']));
		$GLOBALS['person_home_no'] = num2Thai($data['person']['person_addr_pre_no']);
		$GLOBALS['person_home_moo'] = empty($data['person']['person_addr_pre_moo'])? ' - ' : num2Thai($data['person']['person_addr_pre_moo']);
		$GLOBALS['person_home_road'] = empty($data['person']['person_addr_pre_road'])? ' - ' : num2Thai($data['person']['person_addr_pre_road']);
		$GLOBALS['person_home_district'] = empty($district['district_name'])?' - ':$district['district_name'];
		$GLOBALS['person_home_amphur'] =  empty($amphur['amphur_name'])?' - ':$amphur['amphur_name'];
		$GLOBALS['person_home_province'] = empty($province['province_name'])?' - ':$province['province_name'];
		$GLOBALS['person_career'] = $career['career_name'];
		$GLOBALS['person_career_other'] = empty($data['person']['person_career_other'])?' - ':$data['person']['person_career_other'];
		// $GLOBALS['building_state'] = empty($data['building']['building_state'])?' - ':$data['building']['building_state'];
		$GLOBALS['person_mobile'] = empty($data['person']['person_mobile'])? ' - ' : num2Thai($data['person']['person_mobile']);
		$GLOBALS['amount'] = num2Thai(number_format($data['loan_amount'], 2));
		$GLOBALS['amount_text'] = num2wordsThai(number_format($data['loan_amount'], 2));
		$GLOBALS['type_name'] = $type_name;


		$loan_start_date = $analysis['loan_analysis_start'];
		list($d, $m, $y) = explode('/', $loan_start_date);

		$income_year = intval($y);
		if($analysis['loan_analysis_income_year'] ==1 ){
			//current year
			$income_year = intval($y);
		}else if($analysis['loan_analysis_income_year'] ==2 ){
			//last year
			$income_year = intval($y) - 1;
		}else if($analysis['loan_analysis_income_year'] ==3 ){
			//next year
			$income_year = intval($y) + 1;
		}
		$GLOBALS['income_year'] = num2Thai($income_year);

		$loan_type = $this->loan_type->getByLoan($analysis['loan_id']);

		$loan_type_objective = '';
		$html_type = '';
		$loan_creditor = ' - ';
		$loan_creditor_age = ' - ';
		$loan_creditor_thaiid = ' - ';
		$loan_creditor_no = ' - ';
		$loan_creditor_moo = ' - ';
		$loan_creditor_road = ' - ';
		$loan_creditor_district = ' - ';
		$loan_creditor_amphur = ' - ';
		$loan_creditor_province = ' - ';
		$loan_creditor_career = ' - ';
		$loan_creditor_mobile = ' - ';
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{


				// echo "<pre>";
				// print_r( $list_type);
				// echo "</pre>";
				if($list_type['loan_objective_id']=='1')
				{
					$loan_type_objective = 'การไถ่ถอนที่ดินจากการจำนองและขายฝาก';
					$html_type .= 'ไถ่ถอนที่ดินจำนอง หรือขายฝาก จากผู้รับจำนองหรือผู้รับซื้อฝากซึ่งอยู่ในอายุสัญญา ';

					if(!empty($list_type['loan_type_redeem_owner']))
					{
						// $creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						// $creditor_title = $this->title->getById($creditor['title_id']);
						// $creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
						// $creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
						// $creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
						// $creditor_career = $this->career->getById($creditor['career_id']);

						// $loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
						// $loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
						// $loan_creditor_thaiid = $creditor['person_thaiid'];
						// $loan_creditor_no = $creditor['person_addr_pre_no'];
						// $loan_creditor_moo = $creditor['person_addr_pre_moo'];
						// $loan_creditor_road = $creditor['person_addr_pre_road'];
						// $loan_creditor_district = $creditor_district['district_name'];
						// $loan_creditor_amphur = $creditor_amphur['amphur_name'];
						// $loan_creditor_province = $creditor_province['province_name'];
						// $loan_creditor_career = $creditor_career['career_name'];
						// $loan_creditor_mobile = $creditor['person_mobile'];
						$loan_type_objective = $list_type['loan_objective_name'].'จาก'
							.$this->redeem_type[$list_type['loan_type_redeem_type']];

						$loan_creditor = ' - ';
						$loan_creditor_age = ' - ';
						$loan_creditor_thaiid = ' - ';
						$loan_creditor_no = ' - ';
						$loan_creditor_moo = ' - ';
						$loan_creditor_road = ' - ';
						$loan_creditor_district = ' - ';
						$loan_creditor_amphur = ' - ';
						$loan_creditor_province = ' - ';
						$loan_creditor_career = ' - ';
						$loan_creditor_mobile = ' - ';
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$loan_type_objective = 'การชำระหนี้ตามสัญญากู้ยืมเงิน ซึ่งผู้กู้ยืมได้นำโฉนดที่ดิน น.ส. ๓ก. หรือเอกสารสิทธิตามประมวลกฎหมายที่ดินไปเป็นหลักประกัน';
					$html_type .= 'ชำระหนี้ตามสัญญากู้ยืมเงิน ซึ่งได้นำโฉนดที่ดิน';

					if(!empty($list_type['loan_type_analysis_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_analysis_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$creditor_district = $this->district->getById($creditor['person_addr_card_district_id']);
						$creditor_amphur = $this->amphur->getById($creditor['person_addr_card_amphur_id']);
						$creditor_province = $this->province->getById($creditor['person_addr_card_province_id']);
						$creditor_career = $this->career->getById($creditor['career_id']);

						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
						$loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
						$loan_creditor_thaiid = $creditor['person_thaiid'];
						$loan_creditor_district = $creditor['person_addr_card_no'];
						$loan_creditor_moo = $creditor['person_addr_card_moo'];
						$loan_creditor_road = $creditor['person_addr_card_road'];
						$loan_creditor_district = $creditor_district['district_name'];
						$loan_creditor_amphur = $creditor_amphur['amphur_name'];
						$loan_creditor_province = $creditor_province['province_name'];
						$loan_creditor_career = $creditor_career['career_name'];
						$loan_creditor_mobile = $creditor['person_mobile'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$loan_type_objective = 'การชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน';
					$html_type .= 'เอกสารสิทธิในที่ดินให้เจ้าหนี้ยึดถือไว้หรือชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน';

					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$creditor_district = $this->district->getById($creditor['person_addr_card_district_id']);
						$creditor_amphur = $this->amphur->getById($creditor['person_addr_card_amphur_id']);
						$creditor_province = $this->province->getById($creditor['person_addr_card_province_id']);
						$creditor_career = $this->career->getById($creditor['career_id']);

						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'].' (เจ้าหนี้ตามคำพิพากษา)';
						$loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
						$loan_creditor_thaiid = $creditor['person_thaiid'];
						$loan_creditor_no = $creditor['person_addr_card_no'];
						$loan_creditor_moo = $creditor['person_addr_card_moo'];
						$loan_creditor_road = $creditor['person_addr_card_road'];
						$loan_creditor_district = $creditor_district['district_name'];
						$loan_creditor_amphur = $creditor_amphur['amphur_name'];
						$loan_creditor_province = $creditor_province['province_name'];
						$loan_creditor_career = $creditor_career['career_name'];
						$loan_creditor_mobile = $creditor['person_mobile'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$loan_type_objective = 'การซื้อที่ดินที่ถูกขายทอดตลาดหรือหลุดขายฝากไปแล้ว ไม่เกิน ๕ ปี';
					$html_type .= 'ซื้อที่ดินที่ถูกขายทอดตลาด หรือหลุดขายฝากไปแล้วไม่เกิน ๕ ปี โดยเจ้าของที่ดินเดิมที่ถูกบังคับจำนองด้วยการขายทอดตลาด';
				}
				else if($list_type['loan_objective_id']=='5')
				{
					$loan_type_objective = 'การประกอบอาชีพเกษตรกรรม';
					$html_type .= 'ผู้ขายฝากหรือทายาทที่ประสงค์จะนำที่ดินไปประกอบเกษตรกรรม หรือเพื่อการประกอบอาชีพเกษตรกรรม';
				}
			}
		}

		// echo "<pre>";
		// print_r( $loan_type_objective);
		// echo "</pre>";
		$GLOBALS['type_object'] = $loan_type_objective;
		$GLOBALS['type'] = $html_type;
		$GLOBALS['creditor_fullname'] = $loan_creditor;
		$GLOBALS['creditor_age'] = num2Thai($loan_creditor_age);
		$GLOBALS['creditor_thaiid'] = num2Thai($loan_creditor_thaiid);
		$GLOBALS['creditor_home_no'] = num2Thai($loan_creditor_no);
		$GLOBALS['creditor_home_moo'] = num2Thai($loan_creditor_moo);
		$GLOBALS['creditor_home_road'] = num2Thai($loan_creditor_road);
		$GLOBALS['creditor_home_district'] = empty($loan_creditor_district)?' - ':$loan_creditor_district;
		$GLOBALS['creditor_home_amphur'] = empty($loan_creditor_amphur)?' - ':$loan_creditor_amphur;
		$GLOBALS['creditor_home_province'] = empty($loan_creditor_province)?' - ':$loan_creditor_province;
		$GLOBALS['creditor_career'] = empty($loan_creditor_career)?' - ':$loan_creditor_career;
		$GLOBALS['creditor_mobile'] = num2Thai($loan_creditor_mobile);

		$loan_co =  $this->loan_co->getByLoan($analysis['loan_id']);

		$data_co = array();
		$loan_co_details = array();
		if(!empty($loan_co))
		{
			$relation = '';
			foreach ($loan_co as $list_co)
			{
				$co_name = $list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname'];
				$co_age = empty($list_co['person_birthdate'])? ' - ' : age($list_co['person_birthdate']);

				$data_co[] = array(
						'co_fullname' => $co_name,
						'co_age' =>  num2Thai($co_age),
						'co_thaiid' => num2Thai(formatThaiid($list_co['person_thaiid'])),
						'co_home_no' => num2Thai($list_co['person_addr_pre_no']),
						'co_home_moo' => (empty($list_co['person_addr_pre_moo'])? ' - ' : num2Thai($list_co['person_addr_pre_moo'])),
						'co_home_road' => (empty($list_co['person_addr_pre_road'])? ' - ' : num2Thai($list_co['person_addr_pre_road'])),
						'co_home_district' => $list_co['district_name'],
						'co_home_amphur' => $list_co['amphur_name'],
						'co_home_province' => $list_co['province_name'],
						'co_career' => $list_co['career_name'],
						'co_mobile' => (empty($list_co['person_mobile'])? ' - ' : num2Thai($list_co['person_mobile']))
				);


				/************************Loan Co begin******************************/

				$loan_co_income_1 = $this->loan_income->getByPerson($list_co['person_id'], '1');
				$loan_co_income_2 = $this->loan_income->getByPerson($list_co['person_id'], '2');
				$loan_co_income_3 = $this->loan_income->getByPerson($list_co['person_id'], '3');

				$loan_co_income = 0;
				$loan_co_expenditure_farm = 0;
				if(!empty($loan_co_income_1))
				{
					foreach ($loan_co_income_1 as $list_income_1)
					{
						$year1_income = floatval($list_income_1['loan_activity_income1']);
						$loan_co_income += $year1_income;

						$year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
						$loan_co_expenditure_farm += $year1_expenditure;
					}
				}


				$loan_co_income_other = 0;
				$loan_co_expenditure_other = 0;
				$loan_co_html_income_other = '';
				$loan_co_expenditure_other_text = '';
				if(!empty($loan_co_income_2))
				{
					foreach ($loan_co_income_2 as $list_income_2)
					{
						$loan_co_expenditure_other_text .= $list_income_2['loan_activity_name'].', ';
						$loan_co_html_income_other .= $list_income_2['loan_activity_name'].', ';
						$year1_income = floatval($list_income_2['loan_activity_income1']);
						$loan_co_income_other += $year1_income;

						$year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
						$loan_co_expenditure_other += $year1_expenditure;
					}
				}


				if(!empty($loan_co_income_3))
				{
					foreach ($loan_co_income_3 as $list_income_3)
					{
						$loan_co_expenditure_other_text .= $list_income_3['loan_activity_name'].', ';

						$loan_co_html_income_other .= $list_income_3['loan_activity_name'].', ';
						$year1_income = floatval($list_income_3['loan_activity_income1']);
						$loan_co_income_other += $year1_income;

						$year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
						$loan_co_expenditure_other += $year1_expenditure;
					}

					//$expenditure_other_text =  substr($expenditure_other_text, 0, -2);

					$loan_co_html_income_other =  substr($html_income_other, 0, -2);
				//	$loan_co_income_other_str = num2Thai(number_format($loan_co_income_other, 2));
				}
				else
				{
					//$loan_co_html_income_other = ' - ';
					//$loan_co_income_other_str = ' - ';
					//$loan_co_expenditure_other = ' - ';
				}


				$loan_co_expenditure_1 = $this->loan_expenditure->getByPerson($list_co['person_id'], '1');
				$loan_co_expenditure_2 = $this->loan_expenditure->getByPerson($list_co['person_id'], '2');
				$loan_co_expenditure_3 = $this->loan_expenditure->getByPerson($list_co['person_id'], '3');

				$loan_co_expenditure_family = 0;
				if(!empty($loan_co_expenditure_1))
				{
					foreach ($loan_co_expenditure_1 as $list_expenditure_1)
					{
						$year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
						$loan_co_expenditure_family += $year1_amount;
					}
				}

				if(!empty($loan_co_expenditure_2))
				{
					foreach ($loan_co_expenditure_2 as $list_expenditure_2)
					{
						$loan_co_expenditure_other_text .= $list_expenditure_2['loan_expenditure_name'].', ';
						$year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
						$loan_co_expenditure_other += $year1_amount;
					}
				}


				$loan_co_expenditure_child = 0;
				if(!empty($loan_co_expenditure_3))
				{
					foreach ($loan_co_expenditure_3 as $list_expenditure_3)
					{
						$year1_amount = floatval($list_expenditure_3['loan_expenditure_amount1']);
						$loan_co_expenditure_child += $year1_amount;
					}
				}

				$sum_expenditure = $loan_co_expenditure_farm + $loan_co_expenditure_family + $loan_co_expenditure_other + $loan_co_expenditure_child ;
				$sum_income = $loan_co_income + $loan_co_income_other - $sum_expenditure;

				if($loan_co_expenditure_farm==0)
				{
					$loan_co_check_farm = '( )';
					$loan_co_expenditure_farm = ' - ';
				}
				else
				{
					$loan_co_check_farm = '(√)';
					$loan_co_expenditure_farm = num2Thai(number_format($loan_co_expenditure_farm, 2));
				}


				if($loan_co_expenditure_family==0)
				{
					$loan_co_check_family = '( )';
					$loan_co_expenditure_family = ' - ';
				}
				else
				{
					$loan_co_check_family = '(√)';
					$loan_co_expenditure_family = num2Thai(number_format($loan_co_expenditure_family, 2));
				}


				if($loan_co_expenditure_other==0)
				{
					$loan_co_check_other = '( )';
					// $loan_co_expenditure_other_text = ' - ';
					$loan_co_expenditure_other = ' - ';
				}
				else
				{
					$loan_co_check_other = '(√)';
					$loan_co_expenditure_other = num2Thai(number_format($loan_co_expenditure_other, 2));
				}


				if($loan_co_expenditure_child==0)
				{
					$loan_co_check_child = '( )';
					$loan_co_expenditure_child = ' - ';
				}
				else
				{
					$loan_co_check_child = '(√)';
					$loan_co_expenditure_child = num2Thai(number_format($loan_co_expenditure_child, 2));
				}



				$loan_co_details[] = array(
					'co_career' => $list_co['career_name'],
					'income' => num2Thai(number_format($loan_co_income, 2)),
					'html_income_other' => $loan_co_html_income_other,
					'income_other' =>   num2Thai(number_format($loan_co_income_other, 2)),
					'expenditure_other_text' => $loan_co_expenditure_other_text,
					'sum_expenditure' => num2Thai(number_format($sum_expenditure, 2)),
					//num2Thai(number_format($loan_co_expenditure_farm + $loan_co_expenditure_family + $loan_co_expenditure_other + $loan_co_expenditure_child, 2)),
					'sum_income' => num2Thai(number_format($sum_income, 2)),
					'check_farm' => $loan_co_check_farm,
					'expenditure_farm' => $loan_co_expenditure_farm,
					'check_family' => $loan_co_check_family,
					'expenditure_family' => $loan_co_expenditure_family,
					'check_other' => $loan_co_check_other,
					'expenditure_other' => $loan_co_expenditure_other,
					'check_child' => $loan_co_check_child,
					'expenditure_child' => $loan_co_expenditure_child
				);

				/************************Loan Co end******************************/
				$relation .= $list_co['relationship_name'].', ';
			}
			/*
			echo '<pre>';
			print_r('-------');
			print_r($loan_co_details);
			echo '</pre>';
*/
			$relation =  substr($relation, 0, -2);
		}
		else
		{
			$relation = ' - ';
			$loan_co_details[] = array(
				'co_career' => ' - ',
				'income' => ' - ',
				'html_income_other' => ' - ',
				'income_other' =>  ' - ',
				'expenditure_other_text' => ' - ',
				'sum_expenditure' => ' - ',

				'sum_income' => ' - ',
				'check_farm' => ' - ',
				'expenditure_farm' => ' - ',
				'check_family' => ' - ',
				'expenditure_family' => ' - ',
				'check_other' => ' - ',
				'expenditure_other' => ' - ',
				'check_child' => ' - ',
				'expenditure_child' => ' - '
			);
		}

		$GLOBALS['relation'] = $relation;


		$land_record =  $this->land_record->getByLoan($analysis['loan_id']);

/*
		echo '<pre>';
		print_r($land_record);
		echo '</pre>';
*/
		$data_land = array();
		$data_land_2 = array();
		$building_state = '-';
		$building_sum = 0 ;
		$sum_assess = 0;
		$land_type_quality = '';
		if(!empty($land_record))
		{
			foreach ($land_record as $key_land => $land)
			{
				$building_record = $this->building_record->getByLand($land['land_id']);

				$data_building = array();
				if(!empty($building_record))
				{
					foreach ($building_record as $key_building => $building)
					{
						$building_record_price1 = floatval($building['building_record_price1']);
						$building_sum += $building_record_price1;
						// $building_sum = num2Thai(number_format($building_record_price1_t, 2));
						$land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรม ที่อยู่อาศัยและมีสภาพดี';
						$state_building = $building['building_state'];
						$other_state_building = $building['building_state_other'];
						$building_state = '';
						if($state_building=='1')
						{
							$building_state = 'ใหม่';
							// $land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรม ที่อยู่อาศัยและมีสภาพดี';
						}
						else if($state_building=='2')
						{
							$building_state = 'ปานกลาง';
							// $land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรม ที่อยู่อาศัยและมีสภาพดี';
						}
						else if($state_building=='3')
						{
							$building_state = 'เก่า';
							// $land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรม ที่อยู่อาศัยและมีสภาพดี';
						}
						else if($state_building=='4')
						{
							$building_state = 'มีการตบแต่งบำรุงรักษา';
							// $land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรม ที่อยู่อาศัยและมีสภาพดี';
						}
						else if($state_building=='0')
						{
							if(!empty($other_state_building))
							{
								$building_state = $other_state_building;
								// $land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรมและมีสถาพดี';
							}
							else {
								$building_state = '-';
								// $land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรมและมีสถาพดี';
							}

						}
						$data_building[] = array(
								'building_type_name' => $building['building_type_name'],
								'building_no' => num2Thai($building['building_no']),
								'building_moo' => (empty($building['building_moo'])? ' - ' : num2Thai($building['building_moo'])),
								'building_road' => (empty($building['building_road'])? ' - ' : num2Thai($building['building_road'])),
								'building_district'  => $building['district_name'],
								'building_amphur'  => $building['amphur_name'],
								'building_province'  => $building['province_name'],
								'building_state' => $building_state,
								// 'land_type_quality' => $land_type_quality,
						);
					}


				}
				else {
					$land_type_quality = 'เป็นที่ดินสำหรับเกษตรกรรมและมีสถาพดี';
				}

				if(!empty($land['loan_type_id']))
				{
					$data_land[] = array(
							'land_type_name' => num2Thai($land['land_type_name']),
							'land_no' => num2Thai($land['land_no']),
							'land_addr_no' => num2Thai($land['land_addr_no']),
							'land_district' => $land['district_name'],
							'land_amphur' => $land['amphur_name'],
							'land_province' => $land['province_name'],
							'land_area' => num2Thai($land['land_area_rai']).' - '.num2Thai($land['land_area_ngan']).' - '.num2Thai($land['land_area_wah']),
							'land_rai' => num2Thai($land['land_area_rai']),
							'land_ngan' => num2Thai($land['land_area_ngan']),
							'land_wah' => num2Thai($land['land_area_wah']),
							// 'land_type_quality' => $land['land_type_quality'],
							// 'land_type_quality' => empty($data_building)?' - ':$data_building[0]['land_type_quality'],
							'land_type_quality' => $land_type_quality,
							'loan_amount' =>  num2Thai(number_format($data['loan_amount'], 2)),
							'building_type_name' => empty($data_building)?' - ':$data_building[0]['building_type_name'],
							'building_no' => empty($data_building)?' - ':$data_building[0]['building_no'],
							'building_moo' => empty($data_building)?' - ':$data_building[0]['building_moo'],
							'building_road' => empty($data_building)?' - ':$data_building[0]['building_road'],
							'building_district' => empty($data_building)?' - ':$data_building[0]['building_district'],
							'building_amphur' => empty($data_building)?' - ':$data_building[0]['building_amphur'],
							'building_province' => empty($data_building)?' - ':$data_building[0]['building_province'],
							'building_state' => empty($data_building)?' - ':$data_building[0]['building_state'],
							// 'building_record_price1' => empty($data_building)?' - ':$data_building[0]['building_record_price1']
							);

							$data_land_2[] = array(
									'land_type_name' => ' - ',
									'land_no' => ' - ',
									'land_addr_no' => ' - ',
									'land_district' =>' - ',
									'land_amphur' => ' - ',
									'land_province' => ' - ',
									'land_area' => ' - ',
									'land_rai' => ' - ',
									'land_ngan' => ' - ',
									'land_wah' => ' - ',
									'land_type_quality' => ' - ',
									'loan_amount' =>  ' - ');



				}else {
					$data_land_2[] = array(
							'land_type_name' => num2Thai($land['land_type_name']),
							'land_no' => num2Thai($land['land_no']),
							'land_addr_no' => num2Thai($land['land_addr_no']),
							'land_district' => $land['district_name'],
							'land_amphur' => $land['amphur_name'],
							'land_province' => $land['province_name'],
							'land_area' => num2Thai($land['land_area_rai']).' - '.num2Thai($land['land_area_ngan']).' - '.num2Thai($land['land_area_wah']),
							'land_rai' => num2Thai($land['land_area_rai']),
							'land_ngan' => num2Thai($land['land_area_ngan']),
							'land_wah' => num2Thai($land['land_area_wah']),
							// 'land_type_quality' => $land['land_type_quality'],
							'land_type_quality' => $land_type_quality,
							'loan_amount' =>  num2Thai(number_format($data['loan_amount'], 2)),
							// 'building_type_name' => empty($data_building)?' - ':$data_building[0]['building_type_name'],
							// 'building_no' => empty($data_building)?' - ':$data_building[0]['building_no'],
							// 'building_moo' => empty($data_building)?' - ':$data_building[0]['building_moo'],
							// 'building_road' => empty($data_building)?' - ':$data_building[0]['building_road'],
							// 'building_district' => empty($data_building)?' - ':$data_building[0]['building_district'],
							// 'building_amphur' => empty($data_building)?' - ':$data_building[0]['building_amphur'],
							// 'building_province' => empty($data_building)?' - ':$data_building[0]['building_province']
					);
				}
				$land_assess = $this->land_assess->getByLand($land['land_id']);


				if(!empty($land_assess))
				{
					foreach ($land_assess as $list_assess)
					{

						$cal_assess = floatval($list_assess['land_assess_inspector_sum']);
						$sum_assess += floatval($cal_assess);

						//$key_assess = array_search($list_assess['land_id'], $list_land);

						// if(!$key_assess)
						// {
						// 	$key_assess = $i_land;
						// 	$list_land[$i_land++] = $list_assess['land_id'];
						// }

					}
				}





				// $building_state = '';
				// if($state_building=='1')
				// {
				// 	$building_state = 'ใหม่';
				// }
				// else if($state_building=='2')
				// {
				// 	$building_state = 'ปานกลาง';
				// }
				// else if($state_building=='3')
				// {
				// 	$building_state = 'เก่า';
				// }
				// else if($state_building=='4')
				// {
				// 	$building_state = 'มีการตบแต่งบำรุงรักษา';
				// }
				// else if($state_building=='0')
				// {
				// 	if(!empty($other_state_building))
				// 	{
				// 		$building_state = $other_state_building;
				// 	}
				// 	else {
				// 		$building_state = '-';
				// 	}
        //
				// }
			}
		}
		else
		{
			$building_state = ' - ';
		}
		// echo "<pre>building_sum";
		// print_r($building_sum);
		// echo "</pre>-----";
	  // $sum_record = $sum_assess + $building_sum;
		$sum_record = floor(($sum_assess + $building_sum) /100) * 100;
		$GLOBALS['building_state'] = $building_state;
		$GLOBALS['sum_record'] = num2Thai(number_format($sum_record));

		$percentage = floor($sum_record*100/$data['loan_amount']);
		$percentage_str = num2Thai(number_format($percentage)).' หรือ '.num2Thai(number_format($percentage/100,2)).' เท่า';
		$GLOBALS['percentage'] = $percentage_str;



		if ($percentage >= 100) {
			$percentage_text = '(เต็มจำนวนที่ขอสินเชื่อ)';
		}
		else {
			$percentage_text = '(ร้อยละ..'.num2Thai(number_format($percentage)).'..ของที่ขอสินเชื่อ)';
		}
		$GLOBALS['percentage_text'] = $percentage_text;
		// $GLOBALS['building_state'] = $data_building[0]['building_state'];

		$loan_guarantee_bond =  $this->loan_guarantee_bond->getByLoan($analysis['loan_id']);

		$data_bond = array();
		if(!empty($loan_guarantee_bond))
		{
			$sum_bond_1 = 0;
			foreach ($loan_guarantee_bond as $list_bond)
			{
				$sum_bond_1 += intval($list_bond['loan_bond_amount']);

				$data_bond[] = array(
						'bond_owner' => $list_bond['loan_bond_owner'],
						'bond_type' => num2Thai($list_bond['loan_bond_type']),
						'bond_startnumber' => num2Thai($list_bond['loan_bond_startnumber']),
						'bond_endnumber' => num2Thai($list_bond['loan_bond_endnumber']),
						'bond_amount' => num2Thai(number_format($list_bond['loan_bond_amount'], 2)),
						'bond_amount_text' => num2wordsThai(number_format($list_bond['loan_bond_amount'], 2))
				);
			}

			$sum_bond = num2Thai(number_format($sum_bond_1, 2));
			$per_bond = num2Thai(number_format(($sum_bond_1 / $data['loan_amount']) * 100, 2));
		}
		else
		{
			$sum_bond = ' - ';
			$per_bond = ' - ';
		}

		$GLOBALS['sum_bond'] = $sum_bond;
		$GLOBALS['per_bond'] = $per_bond;

		$loan_guarantee_bookbank =  $this->loan_guarantee_bookbank->getByLoan($analysis['loan_id']);

		$loan_bookbank_type = array(
				'1' => 'ออมทรัพย์',
				'2' => 'ฝากประจำ',
				'3' => 'เผื่อเรียก'
		);

		$data_bookbank = array();
		if(!empty($loan_guarantee_bookbank))
		{
			$sum_bookbank_1 = 0;
			foreach ($loan_guarantee_bookbank as $list_bookbank)
			{
				$sum_bookbank_1 += intval($list_bookbank['loan_bookbank_amount']);

				$data_bookbank[] = array(
						'bookbank_owner' => $list_bookbank['loan_bookbank_owner'],
						'bookbank_bank' => num2Thai($list_bookbank['bank_name']),
						'bookbank_branch' => num2Thai($list_bookbank['loan_bookbank_branch']),
						'bookbank_no' => num2Thai($list_bookbank['loan_bookbank_no']),
						'bookbank_type' => num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]),
						// 'bookbank_date' => num2Thai(convert_dmy_th($list_bookbank['loan_bookbank_date'])),
						'bookbank_date' => num2Thai(thai_display_date($list_bookbank['loan_bookbank_date'])),
						'bookbank_amount' => num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)),
						'bookbank_amount_text' => num2wordsThai(number_format($list_bookbank['loan_bookbank_amount'], 2)),
				);
			}

			$sum_bookbank = num2Thai(number_format($sum_bookbank_1, 2));
			$per_bookbank = num2Thai(number_format(($sum_bookbank_1 / $data['loan_amount']) * 100, 2));
		}
		else
		{
			$sum_bookbank = ' - ';
			$per_bookbank = ' - ';
		}

		$GLOBALS['sum_bookbank'] = $sum_bookbank;
		$GLOBALS['per_bookbank'] = $per_bookbank;

		$loan_guarantee_bondsman =  $this->loan_guarantee_bondsman->getByLoan($analysis['loan_id']);

		$data_bondsman = array();
		if(!empty($loan_guarantee_bondsman))
		{
			$sum_bondsman = 0;
			// $type_bondsman = $loan_guarantee_bondsman['bondsman_type'];
			foreach ($loan_guarantee_bondsman as $list_bondsman)
			{
				$age_bondsman = empty($list_bondsman['person_birthdate'])? ' - ' : num2Thai(age($list_bondsman['person_birthdate']));
				$sum_bondsman += intval(0);
				$type_bondsman =  empty($list_bondsman['bondsman_type'])? ' - ' : $list_bondsman['bondsman_type'];

				$data_bondsman[] = array(
						'bondsman_fullname' => $list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'],
						'bondsman_age' => $age_bondsman,
						'bondsman_thaiid' => num2Thai(formatThaiid($list_bondsman['person_thaiid'])),
						'bondsman_career' => num2Thai($list_bondsman['career_name']),
						'bondsman_position' => (empty($list_bondsman['person_position'])? ' - ' : num2Thai($list_bondsman['person_position'])),
						'bondsman_belong' => (empty($list_bondsman['person_belong'])? ' - ' : num2Thai($list_bondsman['person_belong'])),
						'bondsman_income' => num2Thai(number_format($list_bondsman['person_income_per_month'], 2)),
						'bondsman_home_no' => num2Thai($list_bondsman['person_addr_pre_no']),
						'bondsman_home_moo' => (empty($list_bondsman['person_addr_pre_moo'])? ' - ' : num2Thai($list_bondsman['person_addr_pre_moo'])),
						'bondsman_home_road' => (empty($list_bondsman['person_addr_pre_road'])? ' - ' : num2Thai($list_bondsman['person_addr_pre_road'])),
						'bondsman_home_district' => $list_bondsman['district_name'],
						'bondsman_home_amphur' => $list_bondsman['amphur_name'],
						'bondsman_home_province' => $list_bondsman['province_name'],
						'bondsman_mobile' => (empty($list_bondsman['person_mobile'])? '-' : num2Thai($list_bondsman['person_mobile']))
				);
			}

			$sum_bondsman = num2Thai(number_format($sum_bondsman, 2));

			// $type_bondsman = $loan_guarantee_bondsman['bondsman_type'];
			$bondsman_type = '';
			if($type_bondsman=='1')
			{
				$bondsman_type = '๓๐๐,๐๐๐';
			}
			else if($type_bondsman=='2')
			{
				$bondsman_type = '๕๐๐,๐๐๐';
			}
			else if($type_bondsman=='3')
			{
				$bondsman_type = '๓๐๐,๐๐๐';
			}
			// else
			// {
			// 	$bondsman_type = ' - ';
			// }
		}
		else
		{
			$sum_bondsman = ' - ';
			$bondsman_type = ' - ';
		}

		$GLOBALS['sum_bondsman'] = $sum_bondsman;
		$GLOBALS['bondsman_type'] = $bondsman_type;

		$loan_income_1 = $this->loan_income->getByPerson($data['person_id'], '1');
		$loan_income_2 = $this->loan_income->getByPerson($data['person_id'], '2');
		$loan_income_3 = $this->loan_income->getByPerson($data['person_id'], '3');




		$income = 0;
		$expenditure_farm = 0;
		if(!empty($loan_income_1))
		{
			foreach ($loan_income_1 as $list_income_1)
			{
				$year1_income = floatval($list_income_1['loan_activity_income1']);
				$income += $year1_income;

				$year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
				$expenditure_farm += $year1_expenditure;
			}
		}
	  $income_other = 0;
		$expenditure_other = 0;
		$html_income_other = '';
		$expenditure_other_text = '';
		$income_other_str ='';
		if(!empty($loan_income_2))
		{
			foreach ($loan_income_2 as $list_income_2)
			{
				if($list_income_2['loan_activity_type'] == '3')
				{
					// $expenditure_other_text .= (empty($list_income_2['loan_activity_name'])? '' : $list_income_2['loan_activity_name'].', ');
					// $expenditure_other_text .= $list_income_2['loan_activity_name'].', ';

					$year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
					if($year1_expenditure == '0.00')
					{
						$expenditure_other_text .= '';
					}
					else {
						$expenditure_other_text .= $list_income_2['loan_activity_name'].', ';
					}
					$expenditure_other += $year1_expenditure;
				}
			$html_income_other .= $list_income_2['loan_activity_name'].', ';
			$year1_income = floatval($list_income_2['loan_activity_income1']);
			$income_other += $year1_income;
			}
		}


		if(!empty($loan_income_3))
		{
			foreach ($loan_income_3 as $list_income_3)
			{
				if($list_income_3['loan_activity_type'] == '3')
				{
					// $expenditure_other_text .= (empty($list_income_3['loan_activity_name'])? '' : $list_income_2['loan_activity_name'].', ');
					// $expenditure_other_text .= $list_income_3['loan_activity_name'].', ';
					$year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
					if($year1_expenditure == '0.00')
					{
						$expenditure_other_text .= '';
					}
					else {
						$expenditure_other_text .= $list_income_3['loan_activity_name'].', ';
					}
					$expenditure_other += $year1_expenditure;
				}
			$html_income_other .= $list_income_3['loan_activity_name'].', ';
			$year1_income = floatval($list_income_3['loan_activity_income1']);
			$income_other += $year1_income;
			}

			//$expenditure_other_text =  substr($expenditure_other_text, 0, -2);

			$html_income_other =  substr($html_income_other, 0, -2);
			// $income_other = num2Thai(number_format($income_other, 2));

		}
		else
		{
			// $html_income_other = ' - ';
			// $income_other_str = ' - ';
			$expenditure_other_text = '';

		}

		$GLOBALS['income'] = num2Thai(number_format($income, 2));
		$GLOBALS['html_income_other'] = empty($html_income_other)? '-' : $html_income_other;
		$GLOBALS['income_other'] = empty($income_other)? '-' : num2Thai(number_format($income_other, 2));





		$loan_expenditure_1 = $this->loan_expenditure->getByPerson($data['person_id'], '1');
		$loan_expenditure_2 = $this->loan_expenditure->getByPerson($data['person_id'], '2');
	 	$loan_expenditure_3 = $this->loan_expenditure->getByPerson($data['person_id'], '3');

		$expenditure_family = 0;
		if(!empty($loan_expenditure_1))
		{
			foreach ($loan_expenditure_1 as $list_expenditure_1)
			{
				$year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
				$expenditure_family += $year1_amount;
			}
		}

		if(!empty($loan_expenditure_2))
		{
			foreach ($loan_expenditure_2 as $list_expenditure_2)
			{
				$expenditure_other_text .= $list_expenditure_2['loan_expenditure_name'].', ';
				$year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
				$expenditure_other += $year1_amount;
			}
		}
		elseif (empty($expenditure_other_text)) {
			$expenditure_other_text = ' - ';
		}

    $GLOBALS['expenditure_other_text'] = $expenditure_other_text;
		$expenditure_child = 0;
		if(!empty($loan_expenditure_3))
		{
			foreach ($loan_expenditure_3 as $list_expenditure_3)
			{
				$year1_amount = floatval($list_expenditure_3['loan_expenditure_amount1']);
				$expenditure_child += $year1_amount;
			}
		}
		/*
		echo '<pre>';
		print_r($income + $income_other);
		echo '</pre>';
		*/
		$GLOBALS['sum_expenditure'] = num2Thai(number_format($expenditure_farm + $expenditure_family + $expenditure_other + $expenditure_child, 2));
		$GLOBALS['sum_income'] = num2Thai(number_format($income + $income_other - $expenditure_farm - $expenditure_family - $expenditure_other - $expenditure_child, 2));

		if($expenditure_farm==0)
		{
			$check_farm = '( )';
			$expenditure_farm = ' - ';
		}
		else
		{
			$check_farm = '(√)';
			$expenditure_farm = num2Thai(number_format($expenditure_farm, 2));
		}

		$GLOBALS['check_farm'] = $check_farm;
		$GLOBALS['expenditure_farm'] = $expenditure_farm;

		if($expenditure_family==0)
		{
			$check_family = '( )';
			$expenditure_family = ' - ';
		}
		else
		{
			$check_family = '(√)';
			$expenditure_family = num2Thai(number_format($expenditure_family, 2));
		}

		$GLOBALS['check_family'] = $check_family;
		$GLOBALS['expenditure_family'] = $expenditure_family;

		if($expenditure_other==0)
		{
			$check_other = '( )';
			$expenditure_other = ' - ';
		}
		else
		{
			$check_other = '(√)';
			$expenditure_other = num2Thai(number_format($expenditure_other, 2));
		}

		$GLOBALS['check_other'] = $check_other;
		$GLOBALS['expenditure_other'] = $expenditure_other;

		if($expenditure_child==0)
		{
			$check_child = '( )';
			$expenditure_child = ' - ';
		}
		else
		{
			$check_child = '(√)';
			$expenditure_child = num2Thai(number_format($expenditure_child, 2));
		}

		$GLOBALS['check_child'] = $check_child;
		$GLOBALS['expenditure_child'] = $expenditure_child;




		$schedule = $this->loan_schedule->getByLoan($analysis['loan_id']);

		$count_round = count($schedule);
		$round_start = '';
		$round_end = '';
		$sum_estimate_amt = 0;
		$first_amount = 0;
		$list_round =  array();

		if(!empty($schedule))
		{
			foreach ($schedule as $list_schedule)
			{
				// if($round_start=='') $round_start = num2Thai(thai_display_date($list_schedule['loan_schedule_date']));
				// $round_end = num2Thai(thai_display_date($list_schedule['loan_schedule_date']));
				if($round_start=='') $round_start = $list_schedule['loan_schedule_date'];
				$round_end = $list_schedule['loan_schedule_date'];

				$list_round[$list_schedule['loan_schedule_times']] = array(
						'principle' => floatval($list_schedule['loan_schedule_amount']),
						'other' => intval($list_schedule['loan_schedule_other']),
						'principle_other' => intval($list_schedule['loan_schedule_principle']),
						'interest_other' => intval($list_schedule['loan_schedule_interest'])
				);
			}
			//calc interest per cycle
			//this part copy idea from Analysis line 781 - 813
			$i_key = 1;
			$remain = $analysis['loan_analysis_amount'];

			$date_start =  new DateTime(convert_ymd_en($analysis['loan_analysis_start'])) ;
			$date_start->sub(new DateInterval('P1D'));

			$date_start = $date_start->format('Y-m-d');
			$date_end =  convert_ymd_en($analysis['loan_analysis_pay']);


			foreach ($list_round as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				if($i_key==1) $day_between--;
				$interest_per = $remain * ($analysis['loan_analysis_interest'] /100)  * ($day_between / 365);
				if($interest_per < 0)  $interest_per = 0;
				// echo "<pre>";
				// print_r( $date_start);
				// print_r("---");
				// print_r( $date_end);
				// print_r("---");
				// print_r( $remain);
				// print_r("---");
				// print_r( $day_between);
				// print_r("---");
				// print_r( $r_value['principle']);
				// print_r("---");
				// print_r( $interest_per);
				// echo "</pre>";
				if($r_key=='1')
				{
						$first_amount = $r_value['principle'] ;
				}
				$sum_estimate_amt += $r_value['principle'] + $interest_per;

				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$analysis['loan_analysis_length'].'M'));
				$date_end = $date_end->format('Y-m-d');
				$i_key++;
			}
			// end calc interest per cycle
		}


    $per_amount =  floor($sum_estimate_amt / $count_round /100) * 100;
		//$per_amount = $data['loan_amount'] / $count_round;

		$start_round = new DateTime($round_start);
		$end_round = new DateTime($round_end);
		$interval = $start_round->diff($end_round);
		$interval = $interval->format('%y/%m');
		$year_explode = explode("/", $interval);
		$year = $year_explode[0];
		$month = $year_explode[1];
		if ($month == '0') {
			$count_year = $year.' ปี ';
		}
		else {
			$count_year = $year.' ปี '.$month.' เดือน';
		}
		$GLOBALS['count_round'] = num2Thai($count_round);
		// $GLOBALS['round_start'] = $round_start;
		// $GLOBALS['round_end'] = $round_end;
		$GLOBALS['round_start'] = num2Thai(thai_display_date($round_start));
		$GLOBALS['round_end'] = num2Thai(thai_display_date($round_end));
		$GLOBALS['interest'] = num2Thai($analysis['loan_analysis_interest']);
		//first amount
		$GLOBALS['per_amount'] = num2Thai(number_format($first_amount, 2));;
		// num2Thai(number_format($per_amount, 2));
		$GLOBALS['per_sum'] = num2Thai(number_format($per_amount, 2));
		//num2Thai(number_format($per_amount * ((100 + intval($analysis['loan_analysis_interest'])) / 100), 2));
		// $GLOBALS['year'] = num2Thai($analysis['loan_analysis_year']);
		$GLOBALS['year'] = num2Thai($count_year);
		// $GLOBALS['loan_amount'] = num2Thai(number_format($data['loan_amount'], 2);

		$template = 'word/template/14_approve.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).
		if (!count($data_bond)) {
			$data_bond[] = array(
				'bond_owner' => '-',
				'bond_type' => '-',
				'bond_startnumber' => '-',
				'bond_endnumber' => '-',
				'bond_amount' => '-',
				'bond_amount_text' => '-'
			);
		}
		if (!count($data_bookbank)) {
			$data_bookbank[] = array(
					'bookbank_owner' => '-',
					'bookbank_bank' => '-',
					'bookbank_branch' => '-',
					'bookbank_no' =>'-',
					'bookbank_type' => '-',
					'bookbank_date' => '-',
					'bookbank_amount' => '-',
					'bookbank_amount_text' => '-',
			);
		}
		if (!count($data_bondsman)) {
			$data_bondsman[] = array(
					'bondsman_fullname' => '-',
					'bondsman_age' => '-',
					'bondsman_thaiid' => '-',
					'bondsman_career' => '-',
					'bondsman_position' => '-',
					'bondsman_belong' => '-',
					'bondsman_income' => '-',
					'bondsman_home_no' => '-',
					'bondsman_home_moo' => '-',
					'bondsman_home_road' => '-',
					'bondsman_home_district' => '-',
					'bondsman_home_amphur' => '-',
					'bondsman_home_province' => '-',
					'bondsman_mobile' => '-'
			);
		}
		if (!count($data_co)) {
			$data_co[] = array(
			'co_fullname' => '-',
			'co_age' =>  '-',
			'co_thaiid' => '-',
			'co_home_no' => '-',
			'co_home_moo' => '-',
			'co_home_road' => '-',
			'co_home_district' =>'-',
			'co_home_amphur' => '-',
			'co_home_province' => '-',
			'co_career' => '-',
			'co_mobile' => '-'
			);
		}
 		$TBS->MergeBlock('co', $data_co);
		$TBS->MergeBlock('co2', $data_co);
		$TBS->MergeBlock('co_detail', $loan_co_details);
 		$TBS->MergeBlock('land', $data_land);
	  $TBS->MergeBlock('land2', $data_land);
		$TBS->MergeBlock('land3', $data_land_2);
 		$TBS->MergeBlock('bond', $data_bond);
 		$TBS->MergeBlock('bookbank', $data_bookbank);
 		$TBS->MergeBlock('bondsman', $data_bondsman);


 		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
 		$output_file_name = 'approve_'.date('Y-m-d').'.docx';
		$temp_file = tempnam(sys_get_temp_dir(), 'Docx');
 		//$TBS->Show(OPENTBS_DOWNLOAD, $output_file_name);
	  //$TBS->PlugIn(OPENTBS_DEBUG_XML_CURRENT);
	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);

	}


public function send_download($temp_file,$file) {
      $basename = basename($file);
      $length   = sprintf("%u", filesize($temp_file));

      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="' . $basename . '"');
      header('Content-Transfer-Encoding: binary');
      header('Connection: Keep-Alive');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: ' . $length);
      ob_clean();
      flush();
      set_time_limit(0);
      readfile($temp_file);
      exit();
  }
	public function pdf($id)
	{
		ob_start();
		$this->load->library('tcpdf');

		$this->load->model('analysis/loan_analysis_model', 'loan_analysis');
		$this->load->model('analysis/loan_model', 'loan');
		$this->load->model('person/person_model', 'person');
		$this->load->model('title/title_model', 'title');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('career/career_model', 'career');
		$this->load->model('analysis/loan_type_model', 'loan_type');
		$this->load->model('analysis/loan_co_model', 'loan_co');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('analysis/loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('analysis/loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('analysis/loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('analysis/loan_income_model', 'loan_income');
		$this->load->model('analysis/loan_expenditure_model', 'loan_expenditure');
		$this->load->model('analysis/loan_schedule_model', 'loan_schedule');
		$this->load->model('analysis/loan_analysis_model', 'loan_analysis');

		$analysis = $this->loan_analysis->getById($id);
		$data = $this->loan->getById($analysis['loan_id']);
		$analysis = $this->loan_analysis->getByLoan($analysis['loan_id']);

		$type_name = '';
		if($analysis['loan_analysis_per_year']=='1')
		{
			$type_name = 'รายปี';
		}
		else if($analysis['loan_analysis_per_year']=='6')
		{
			$type_name = 'ราย 6 เดือน';
		}
		else if($analysis['loan_analysis_per_year']=='12')
		{
			$type_name = 'รายเดือน';
		}

		$data['person']['title'] = $this->title->getById($data['person']['title_id']);
		$data['person']['person_age'] = empty($data['person']['person_birthdate'])? '' : age($data['person']['person_birthdate']);
		$data['person']['district'] = $this->district->getById($data['person']['person_addr_pre_district_id']);
		$data['person']['amphur'] = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
		$data['person']['province'] = $this->province->getById($data['person']['person_addr_pre_province_id']);
		$data['person']['career'] = $this->career->getById($data['person']['career_id']);

		$loan_type = $this->loan_type->getByLoan($analysis['loan_id']);

		$loan_type_objective = '';
		$html_type = '';
		$loan_creditor = '';
		$loan_creditor_age = '';
		$loan_creditor_thaiid = '';
		$loan_creditor_no = '';
		$loan_creditor_moo = '';
		$loan_creditor_road = '';
		$loan_creditor_district = '';
		$loan_creditor_amphur = '';
		$loan_creditor_province = '';
		$loan_creditor_career = '';
		$loan_creditor_mobile = '';
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$loan_type_objective = 'ไถ่ถอนที่ดินจากการจำนองหรือขายฝาก';
					$html_type .= 'ไถ่ถอนที่ดินจำนอง หรือขายฝาก จากผู้รับจำนองหรือผู้รับซื้อฝากซึ่งอยู่ในอายุสัญญา ';

					if(!empty($list_type['loan_type_redeem_owner']))
					{
						$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
						$creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
						$creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
						$creditor_career = $this->career->getById($creditor['career_id']);

						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
						$loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
						$loan_creditor_thaiid = $creditor['person_thaiid'];
						$loan_creditor_no = $creditor['person_addr_pre_no'];
						$loan_creditor_moo = $creditor['person_addr_pre_moo'];
						$loan_creditor_road = $creditor['person_addr_pre_road'];
						$loan_creditor_district = $creditor_district['district_name'];
						$loan_creditor_amphur = $creditor_amphur['amphur_name'];
						$loan_creditor_province = $creditor_province['province_name'];
						$loan_creditor_career = $creditor_career['career_name'];
						$loan_creditor_mobile = $creditor['person_mobile'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$loan_type_objective = 'การชำระหนี้ตามสัญญากู้ยืมเงิน ซึ่งผู้กู้ยืมได้นำโฉนดที่ดิน น.ส. ๓ก. หรือเอกสารสิทธิตามประมวลกฎหมายที่ดินไปเป็นหลักประกัน';
					$html_type .= 'ชำระหนี้ตามสัญญากู้ยืมเงิน ซึ่งได้นำโฉนดที่ดิน';

					if(!empty($list_type['loan_type_analysis_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_analysis_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
						$creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
						$creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
						$creditor_career = $this->career->getById($creditor['career_id']);

						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
						$loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
						$loan_creditor_thaiid = $creditor['person_thaiid'];
						$loan_creditor_district = $creditor['person_addr_pre_no'];
						$loan_creditor_moo = $creditor['person_addr_pre_moo'];
						$loan_creditor_road = $creditor['person_addr_pre_road'];
						$loan_creditor_district = $creditor_district['district_name'];
						$loan_creditor_amphur = $creditor_amphur['amphur_name'];
						$loan_creditor_province = $creditor_province['province_name'];
						$loan_creditor_career = $creditor_career['career_name'];
						$loan_creditor_mobile = $creditor['person_mobile'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$loan_type_objective = 'การชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน';
					$html_type .= 'เอกสารสิทธิในที่ดินให้เจ้าหนี้ยึดถือไว้หรือชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน';

					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
						$creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
						$creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
						$creditor_career = $this->career->getById($creditor['career_id']);

						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
						$loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
						$loan_creditor_thaiid = $creditor['person_thaiid'];
						$loan_creditor_no = $creditor['person_addr_pre_no'];
						$loan_creditor_moo = $creditor['person_addr_pre_moo'];
						$loan_creditor_road = $creditor['person_addr_pre_road'];
						$loan_creditor_district = $creditor_district['district_name'];
						$loan_creditor_amphur = $creditor_amphur['amphur_name'];
						$loan_creditor_province = $creditor_province['province_name'];
						$loan_creditor_career = $creditor_career['career_name'];
						$loan_creditor_mobile = $creditor['person_mobile'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$loan_type_objective = 'การซื้อที่ดินที่ถูกขายทอดตลาดหรือหลุดขายฝากไปแล้ว ไม่เกิน ๕ ปี';
					$html_type .= 'ซื้อที่ดินที่ถูกขายทอดตลาด หรือหลุดขายฝากไปแล้วไม่เกิน ๕ ปี โดยเจ้าของที่ดินเดิมที่ถูกบังคับจำนองด้วยการขายทอดตลาด';
				}
				else if($list_type['loan_objective_id']=='5')
				{
					$loan_type_objective = 'ซื้อคืนที่ดิน';
					$html_type .= 'ผู้ขายฝากหรือทายาทที่ประสงค์จะนำที่ดินไปประกอบเกษตรกรรม หรือเพื่อการประกอบอาชีพเกษตรกรรม';
				}
			}
		}

		$i_1 = 2;

		$loan_co =  $this->loan_co->getByLoan($analysis['loan_id']);

		$html_loan_co = '';
		$html_loan_co2 = '';
		$html_loan_co3 = '';
		if(!empty($loan_co))
		{
			$html_loan_co = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="20%"></td>
						<td width="80%">'.num2Thai($i_1).') ผู้กู้ร่วม&nbsp;&nbsp;</td>
					</tr>
				</table>';

			$html_loan_co3 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="20%"></td>
						<td width="80%">'.num2Thai($i_1++).') ผู้กู้ร่วม&nbsp;&nbsp;</td>
					</tr>
				</table>';

			$i_co = 1;
			foreach ($loan_co as $list_co)
			{
				$name_co = $list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname'];
				$age_co = age($list_co['person_birthdate']);

				$html_loan_co .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="60%">('.num2Thai($i_co).') ชื่อ&nbsp;&nbsp;'.$name_co.'</td>
							<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($age_co).'&nbsp;&nbsp;ปี</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($list_co['person_thaiid'])).'</td>
							<td width="20%">ที่อยู่เลขที่&nbsp;&nbsp;'.num2Thai($list_co['person_addr_pre_no']).'</td>
							<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($list_co['person_addr_pre_moo'])? '-' : num2Thai($list_co['person_addr_pre_moo'])).'</td>
							<td width="25%">ถนน&nbsp;&nbsp;'.(empty($list_co['person_addr_pre_road'])? '-' : num2Thai($list_co['person_addr_pre_road'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20%">ตำบล/แขวง&nbsp;&nbsp;'.$list_co['district_name'].'</td>
							<td width="20%">อำเภอ&nbsp;&nbsp;'.$list_co['amphur_name'].'</td>
							<td width="20%">จังหวัด&nbsp;&nbsp;'.$list_co['province_name'].'</td>
							<td width="40%">อาชีพ&nbsp;&nbsp;'.$list_co['career_name'].'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>โทรศัพท์&nbsp;&nbsp;'.(empty($list_co['person_mobile'])? '-' : num2Thai($list_co['person_mobile'])).'</td>
						</tr>
					</table>';

				$html_loan_co2 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="20%"></td>
						<td width="80%">('.num2Thai($i_co++).') '.$name_co.'โดยมีความสัมพันธ์เป็น '.$list_co['relationship_name'].' กับผู้ขอสินเชื่อ</td>
					</tr>
				</table>';

				$html_loan_co3 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="60%">('.num2Thai($i_co).') ชื่อ&nbsp;&nbsp;'.$name_co.'</td>
							<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($age_co).'&nbsp;&nbsp;ปี</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($list_co['person_thaiid'])).'</td>
							<td width="20%">ที่อยู่เลขที่&nbsp;&nbsp;'.num2Thai($list_co['person_addr_pre_no']).'</td>
							<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($list_co['person_addr_pre_moo'])? '-' : num2Thai($list_co['person_addr_pre_moo'])).'</td>
							<td width="25%">ถนน&nbsp;&nbsp;'.(empty($list_co['person_addr_pre_road'])? '-' : num2Thai($list_co['person_addr_pre_road'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20%">ตำบล/แขวง&nbsp;&nbsp;'.$list_co['district_name'].'</td>
							<td width="20%">อำเภอ&nbsp;&nbsp;'.$list_co['amphur_name'].'</td>
							<td width="20%">จังหวัด&nbsp;&nbsp;'.$list_co['province_name'].'</td>
							<td width="40%">อาชีพ&nbsp;&nbsp;'.$list_co['career_name'].'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>โทรศัพท์&nbsp;&nbsp;'.(empty($list_co['person_mobile'])? '-' : num2Thai($list_co['person_mobile'])).'</td>
						</tr>
					</table>';
			}
		}

		$loan_guarantee_bond =  $this->loan_guarantee_bond->getByLoan($analysis['loan_id']);

		$i_guarantee = 2;
		$i_guarantee3 = 1;
		$i_guarantee4 = 1;
		$html_guarantee_bond = '';
		$html_guarantee_bond3 = '';
		$html_guarantee_bond4 = '';
		if(!empty($loan_guarantee_bond))
		{
			$html_guarantee_bond = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.num2Thai($i_guarantee++).') พันธบัตรรัฐบาล&nbsp;&nbsp;โดยมี</td>
				</tr>
			</table>';

			$html_guarantee_bond3 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.num2Thai($i_guarantee3++).') พันธบัตรรัฐบาล&nbsp;&nbsp;โดยมี</td>
				</tr>
			</table>';

			$i_bond = 1;
			$sum_bond = 0;
			foreach ($loan_guarantee_bond as $list_bond)
			{
				$sum_bond += intval($list_bond['loan_bond_amount']);

				$html_guarantee_bond .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="75%">('.num2Thai($i_bond).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bond['loan_bond_owner'].'&nbsp;&nbsp;เป็นผู้ถือกรรมสิทธิ์</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="40%">เป็นชนิดพันธบัตร&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_type']).'</td>
							<td width="30%">เลขที่ต้น&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_startnumber']).'</td>
							<td width="30%">เลขที่ท้าย&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_endnumber']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>จำนวนเงินราคาที่ตราไว้&nbsp;&nbsp;'.num2Thai(number_format($list_bond['loan_bond_amount'], 2)).' บาท&nbsp;&nbsp;('.num2wordsThai(number_format($list_bond['loan_bond_amount'], 2)).')</td>
						</tr>
					</table>';

				$html_guarantee_bond3 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="75%">('.num2Thai($i_bond++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bond['loan_bond_owner'].'&nbsp;&nbsp;เป็นผู้ถือกรรมสิทธิ์</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="40%">ชนิดพันธบัตร&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_type']).'</td>
							<td width="30%">เลขที่ต้น&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_startnumber']).'</td>
							<td width="30%">เลขที่ท้าย&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_endnumber']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>จำนวนเงินราคาที่ตราไว้&nbsp;&nbsp;'.num2Thai(number_format($list_bond['loan_bond_amount'], 2)).' บาท&nbsp;&nbsp;('.num2wordsThai(number_format($list_bond['loan_bond_amount'], 2)).')</td>
						</tr>
					</table>';
			}

			$html_guarantee_bond4 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="30%"></td>
						<td width="70%">('.num2Thai($i_guarantee4++).')&nbsp;&nbsp;พันธบัตรรัฐบาล ตามข้อ ๑.๑ ๒) ซึ่งได้จำนำสิทธิไว้กับ บจธ. มีจำนวน '.num2Thai(number_format($sum_bond, 2)).'</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>บาท โดยคิดเป็นจำนวนร้อยละ '.num2Thai(number_format(($sum_bond / $data['loan_amount']) * 100, 2)).' ของจำนวนเงินที่ขอรับสินเชื่อ</td>
					</tr>
				</table>';
		}

		$loan_guarantee_bookbank =  $this->loan_guarantee_bookbank->getByLoan($analysis['loan_id']);

		$loan_bookbank_type = array(
				'1' => 'ออมทรัพย์',
				'2' => 'ฝากประจำ',
				'3' => 'เผื่อเรียก'
		);

		$html_guarantee_bookbank = '';
		$html_guarantee_bookbank3 = '';
		$html_guarantee_bookbank4 = '';
		if(!empty($loan_guarantee_bookbank))
		{
			$html_guarantee_bookbank = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="20%"></td>
						<td width="80%">'.num2Thai($i_guarantee++).') เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ &nbsp;&nbsp;โดยมี</td>
					</tr>
				</table>';

			$html_guarantee_bookbank3 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.num2Thai($i_guarantee3++).') เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ &nbsp;&nbsp;โดยมี</td>
				</tr>
			</table>';

			$i_bookbank = 1;
			$sum_bookbank = 0;
			foreach ($loan_guarantee_bookbank as $list_bookbank)
			{
				$sum_bookbank += intval($list_bookbank['loan_bookbank_amount']);

				$html_guarantee_bookbank .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="75%">('.num2Thai($i_bookbank).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bookbank['loan_bookbank_owner'].'&nbsp;&nbsp;เป็นเจ้าของบัญชี</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="65%">ธนาคาร/สหกรณ์&nbsp;&nbsp;'.num2Thai($list_bookbank['bank_name']).'</td>
							<td width="35%">สาขา&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_branch']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="30%">เลขที่บัญชี&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_no']).'</td>
							<td width="30%">ประเภทของการฝาก&nbsp;&nbsp;'.num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]).'</td>
							<td width="40%">ซึ่งมียอดเงินฝาก ณ วันที่&nbsp;&nbsp;'.num2Thai(convert_dmy_th($list_bookbank['loan_bookbank_date'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>จำนวนเงิน&nbsp;&nbsp;'.num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)).'&nbsp;&nbsp;บาท&nbsp;&nbsp;('.num2wordsThai(number_format($list_bookbank['loan_bookbank_amount'], 2)).')</td>
						</tr>
					</table>';

				$html_guarantee_bookbank3 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="75%">('.num2Thai($i_bookbank++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bookbank['loan_bookbank_owner'].'&nbsp;&nbsp;เป็นเจ้าของบัญชี</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="65%">ธนาคาร/สหกรณ์&nbsp;&nbsp;'.num2Thai($list_bookbank['bank_name']).'</td>
							<td width="35%">สาขา&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_branch']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="30%">เลขที่บัญชี&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_no']).'</td>
							<td width="30%">ประเภทของการฝาก&nbsp;&nbsp;'.num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]).'</td>
							<td width="40%">ซึ่งมียอดเงินฝาก ณ วันที่&nbsp;&nbsp;'.num2Thai(convert_dmy_th($list_bookbank['loan_bookbank_date'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>จำนวนเงิน&nbsp;&nbsp;'.num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)).'&nbsp;&nbsp;บาท&nbsp;&nbsp;('.num2wordsThai(number_format($list_bookbank['loan_bookbank_amount'], 2)).')</td>
						</tr>
					</table>';
			}

			$html_guarantee_bookbank4 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="30%"></td>
						<td width="70%">('.num2Thai($i_guarantee4++).')&nbsp;&nbsp;เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ ตามข้อ ๑.๑ ๓)</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>จำนวน '.num2Thai(number_format($sum_bookbank, 2)).' บาท โดยคิดเป็นจำนวนร้อยละ '.num2Thai(number_format(($sum_bookbank / $data['loan_amount']) * 100, 2)).' ของจำนวนเงินที่ขอรับสินเชื่อ</td>
					</tr>
				</table>';
		}

		$loan_guarantee_bondsman =  $this->loan_guarantee_bondsman->getByLoan($analysis['loan_id']);

		$html_guarantee_bondsman = '';
		$html_guarantee_bondsman3 = '';
		$html_guarantee_bondsman4 = '';
		if(!empty($loan_guarantee_bondsman))
		{
			$html_guarantee_bondsman = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="20%"></td>
						<td width="80%">'.num2Thai($i_guarantee++).') การค้ำประกันด้วยบุคคล &nbsp;&nbsp;โดยมี</td>
					</tr>
				</table>';

			$html_guarantee_bondsman3 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.num2Thai($i_guarantee3++).') การค้ำประกันด้วยบุคคล &nbsp;&nbsp;โดยมี</td>
				</tr>
			</table>';

			$i_bondsman = 1;
			$sum_bondsman = 0;
			foreach ($loan_guarantee_bondsman as $list_bondsman)
			{
				$age_bondsman = age($list_bondsman['person_birthdate']);
				$sum_bondsman += intval(0);

				$html_guarantee_bondsman .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="60%">('.num2Thai($i_bondsman).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'].'ล&nbsp;&nbsp;เป็นผู้ค้ำประกัน</td>
							<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($age_bondsman).'&nbsp;&nbsp;ปี</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($list_bondsman['person_thaiid'])).'</td>
							<td width="30%">อาชีพ&nbsp;&nbsp;'.num2Thai($list_bondsman['career_name']).'</td>
							<td width="30%">ตำแหน่ง&nbsp;&nbsp;'.(empty($list_bondsman['person_position'])? '-' : num2Thai($list_bondsman['person_position'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="35%">สังกัด&nbsp;&nbsp;'.(empty($list_bondsman['person_belong'])? '-' : num2Thai($list_bondsman['person_belong'])).'</td>
							<td width="45%">ได้รับเงินเดือน/ค่าจ้าง เดือนละ&nbsp;&nbsp;'.num2Thai(number_format($list_bondsman['person_income_per_month'], 2)).'&nbsp;&nbsp;บาท</td>
							<td width="20%">ที่อยู่เลขที่&nbsp;&nbsp;'.num2Thai($list_bondsman['person_addr_pre_no']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($list_bondsman['person_addr_pre_moo'])? '-' : num2Thai($list_bondsman['person_addr_pre_moo'])).'</td>
							<td width="25%">ถนน&nbsp;&nbsp;'.(empty($list_bondsman['person_addr_pre_road'])? '-' : num2Thai($list_bondsman['person_addr_pre_road'])).'</td>
							<td width="30%">ตำบล/แขวง&nbsp;&nbsp;'.$list_bondsman['district_name'].'</td>
							<td width="30%">อำเภอ&nbsp;&nbsp;'.$list_bondsman['amphur_name'].'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="30%">จังหวัด&nbsp;&nbsp;'.$list_bondsman['province_name'].'</td>
							<td width="70%">หมายเลขโทรศัพท์&nbsp;&nbsp;'.(empty($list_bondsman['person_mobile'])? '-' : num2Thai($list_bondsman['person_mobile'])).'</td>
						</tr>
					</table>';

				$html_guarantee_bondsman3 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="60%">('.num2Thai($i_bondsman++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'].'ล&nbsp;&nbsp;เป็นผู้ค้ำประกัน</td>
							<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($age_bondsman).'&nbsp;&nbsp;ปี</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($list_bondsman['person_thaiid'])).'</td>
							<td width="30%">อาชีพ&nbsp;&nbsp;'.num2Thai($list_bondsman['career_name']).'</td>
							<td width="30%">ตำแหน่ง&nbsp;&nbsp;'.(empty($list_bondsman['person_position'])? '-' : num2Thai($list_bondsman['person_position'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="35%">สังกัด&nbsp;&nbsp;'.(empty($list_bondsman['person_belong'])? '-' : num2Thai($list_bondsman['person_belong'])).'</td>
							<td width="45%">ได้รับเงินเดือน/ค่าจ้าง เดือนละ&nbsp;&nbsp;'.num2Thai(number_format($list_bondsman['person_income_per_month'], 2)).'&nbsp;&nbsp;บาท</td>
							<td width="20%">ที่อยู่เลขที่&nbsp;&nbsp;'.num2Thai($list_bondsman['person_addr_pre_no']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($list_bondsman['person_addr_pre_moo'])? '-' : num2Thai($list_bondsman['person_addr_pre_moo'])).'</td>
							<td width="25%">ถนน&nbsp;&nbsp;'.(empty($list_bondsman['person_addr_pre_road'])? '-' : num2Thai($list_bondsman['person_addr_pre_road'])).'</td>
							<td width="30%">ตำบล/แขวง&nbsp;&nbsp;'.$list_bondsman['district_name'].'</td>
							<td width="30%">อำเภอ&nbsp;&nbsp;'.$list_bondsman['amphur_name'].'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="30%">จังหวัด&nbsp;&nbsp;'.$list_bondsman['province_name'].'</td>
							<td width="70%">หมายเลขโทรศัพท์&nbsp;&nbsp;'.(empty($list_bondsman['person_mobile'])? '-' : num2Thai($list_bondsman['person_mobile'])).'</td>
						</tr>
					</table>';
			}

			$html_guarantee_bondsman4 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="30%"></td>
						<td width="70%">('.num2Thai($i_guarantee4++).')&nbsp;&nbsp;การค้ำประกันด้วยบุคคล ตามข้อ ๑.๑ ๔) เมื่อพิจารณาบุคคลซึ่งเป็นผู้ค้ำประกัน</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>จากรายได้ฐานะ ระดับ และอัตราเงินเดือน รวมทั้งภาระผูกพันจากการทำสัญญาค้ำประกันรายอื่นแล้ว บุคคลดังกล่าวสามารถค้ำประกันการชำระหนี้ของผู้ขอสินเชื่อได้ ในวงเงินจำนวน '.num2Thai(number_format($sum_bondsman, 2)).' บาท ซึ่งเป็นการค้ำประกันการชำระหนี้ไม่เกินวงเงินที่บุคคลดังกล่าวสามารถค้ำประกันได้ ตามหลักเกณฑ์แห่งข้อบังคับฯ</td>
					</tr>
				</table>';
		}

		$land_record =  $this->land_record->getByLoan($analysis['loan_id']);

		$html_land = '';
		if(!empty($land_record))
		{
			$i_land = 1;
			foreach ($land_record as $key_land => $land)
			{
				$html_land .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="25%"></td>
							<td width="35%">('.num2Thai($i_land).')&nbsp;ที่ดิน ประเภท&nbsp;&nbsp;'.num2Thai($land['land_type_name']).'</td>
							<td width="20%">เลขที่&nbsp;&nbsp;'.num2Thai($land['land_no']).'</td>
							<td width="20%">เลขที่ดิน&nbsp;&nbsp;'.num2Thai($land['land_addr_no']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="20%">ตำบล&nbsp;&nbsp;'.$land['district_name'].' </td>
							<td width="20%">อำเภอ&nbsp;&nbsp;'.$land['amphur_name'].'</td>
							<td width="25%">จังหวัด&nbsp;&nbsp;'.$land['province_name'].'</td>
							<td width="35%">เนื้อที่&nbsp;&nbsp;'.num2Thai($land['land_area_rai']).' ไร่&nbsp;&nbsp;'.num2Thai($land['land_area_ngan']).' งาน&nbsp;&nbsp;'.num2Thai($land['land_area_wah']).' ตารางวา</td>
						</tr>
					</table>';

				$building_record = $this->building_record->getByLand($land['land_id']);

				if(!empty($building_record))
				{
					$html_land .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30%"></td>
								<td width="70%">พร้อมทั้งสิ่งปลูกสร้าง (ถ้ามี) </td>
							</tr>
						</table>';

					$i_building = 1;
					foreach ($building_record as $key_building => $building)
					{
						$html_land .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="30%"></td>
									<td width="30%">('.num2Thai($i_land.'.'.$i_building++).')&nbsp;ประเภท&nbsp;&nbsp;'.$building['building_type_name'].'</td>
									<td width="20%">เลขที่&nbsp;&nbsp;'.num2Thai($building['building_no']).'</td>
									<td width="20%">หมู่ที่&nbsp;&nbsp;'.(empty($building['building_moo'])? '-' : num2Thai($building['building_moo'])).'</td>
								</tr>
							</table>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="25%">ถนน&nbsp;&nbsp;'.(empty($building['building_road'])? '-' : num2Thai($building['building_road'])).'</td>
									<td width="25%">ตำบล&nbsp;&nbsp;'.$building['district_name'].'</td>
									<td width="25%">อำเภอ&nbsp;&nbsp;'.$building['amphur_name'].'</td>
									<td width="25%">จังหวัด&nbsp;&nbsp;'.$building['province_name'].'</td>
								</tr>
							</table>';
					}
				}

				$i_land++;
			}
		}

		$loan_income_1 = $this->loan_income->getByPerson($data['person_id'], '1');
		$loan_income_2 = $this->loan_income->getByPerson($data['person_id'], '2');
		$loan_income_3 = $this->loan_income->getByPerson($data['person_id'], '3');

		$income = 0;
		$expenditure_farm = 0;
		if(!empty($loan_income_1))
		{
			foreach ($loan_income_1 as $list_income_1)
			{
				$year1_income = floatval($list_income_1['loan_activity_income1']);
				$income += $year1_income;

				$year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
				$expenditure_farm += $year1_expenditure;
			}
		}

		$expenditure_other = 0;
		if(!empty($loan_income_2))
		{
			foreach ($loan_income_2 as $list_income_2)
			{
				$year1_income = floatval($list_income_2['loan_activity_income1']);
				$income += $year1_income;

				$year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
				$expenditure_other += $year1_expenditure;
			}
		}

		$html_income_other = '';
		$income_other = 0;
		$expenditure_other_text = '';
		if(!empty($loan_income_3))
		{
			foreach ($loan_income_3 as $list_income_3)
			{
				$expenditure_other_text .= $list_income_2['loan_activity_name'].', ';

				$html_income_other .= $list_income_3['loan_activity_name'].', ';
				$year1_income = floatval($list_income_3['loan_activity_income1']);
				$income_other += $year1_income;

				$year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
				$expenditure_other += $year1_expenditure;
			}

			$expenditure_other_text =  substr($expenditure_other_text, 0, -2);

			$html_income_other =  substr($html_income_other, 0, -2);
			$html_income_other = 'และมีรายได้อื่น ๆ (ถ้ามี) '.num2Thai($html_income_other).' จำนวน '.num2Thai(number_format($income_other, 2)).' บาท ต่อปี';
		}

		$loan_expenditure_1 = $this->loan_expenditure->getByPerson($data['person_id'], '1');
		$loan_expenditure_2 = $this->loan_expenditure->getByPerson($data['person_id'], '2');

		$expenditure_1 = 0;
		$expenditure_family = 0;
		if(!empty($loan_expenditure_1))
		{
			foreach ($loan_expenditure_1 as $list_expenditure_1)
			{
				$year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
				$expenditure_family += $year1_amount;
			}
		}

		if(!empty($loan_expenditure_2))
		{
			foreach ($loan_expenditure_2 as $list_expenditure_2)
			{
				$year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
				$expenditure_other += $year1_amount;
			}
		}

		$schedule = $this->loan_schedule->getByLoan($analysis['loan_id']);

		$count_round = count($schedule);
		$round_start = '';
		$round_end = '';
		if(!empty($schedule))
		{
			foreach ($schedule as $list_schedule)
			{
				if($round_start=='') $round_start = num2Thai(convert_dmy_th($list_schedule['loan_schedule_date']));
				$round_end = num2Thai(convert_dmy_th($list_schedule['loan_schedule_date']));
			}
		}

		$per_amount = $data['loan_amount'] / $count_round;

		$filename = 'approve_'.date('Ymd');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
		$pdf->SetTitle($filename);//  กำหนด Title
		$pdf->SetSubject('Export receipt'); // กำหนด Subject
		$pdf->SetKeywords($filename); // กำหนด Keyword

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->SetMargins(10, 10, 10, true);
		$pdf->AddPage();

		$htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
					<td width="80%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๑๔</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 10px;"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-weight: bold; text-align:center; font-size: 24px;">บันทึกข้อความ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" colspan="2"><b>หน่วยงาน</b>&nbsp;&nbsp;สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
				</tr>
				<tr>
					<td width="50%"><b>ที่</b>&nbsp;&nbsp;</td>
					<td width="50%"><b>วันที่</b>&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><b>เรื่อง</b>&nbsp;&nbsp;รายงานการขออนุมัติสินเชื่อ</td>
				</tr>
			</table>
			<hr />
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%">เรียน	ผอ.บจธ.</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%" style="font-weight: bold;">๑. <u>ข้อเท็จจริง</u></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๑.๑&nbsp;&nbsp;รายละเอียดของผู้ขอสินเชื่อ ผู้กู้ร่วม และ/หรือเจ้าหนี้หรือผู้รับซื้อฝาก</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="65%">๑) ผู้ขอสินเชื่อ&nbsp;&nbsp;'.$data['person']['title']['title_name'].$data['person']['person_fname'].' '.$data['person']['person_lname'].'</td>
					<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($data['person']['person_age']).'&nbsp;&nbsp;ปี</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($data['person']['person_thaiid'])).'</td>
					<td width="20%">ที่อยู่เลขที่&nbsp;&nbsp;'.num2Thai($data['person']['person_addr_pre_no']).'</td>
					<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($data['person']['person_addr_pre_moo'])? '-' : num2Thai($data['person']['person_addr_pre_moo'])).'</td>
					<td width="25%">ถนน&nbsp;&nbsp;'.(empty($data['person']['person_addr_pre_road'])? '-' : num2Thai($data['person']['person_addr_pre_road'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="35%">ตำบล/แขวง&nbsp;&nbsp;'.$data['person']['district']['district_name'].'</td>
					<td width="30%">อำเภอ&nbsp;&nbsp;'.$data['person']['amphur']['amphur_name'].'</td>
					<td width="35%">จังหวัด&nbsp;&nbsp;'.$data['person']['province']['province_name'].'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="40%">อาชีพ&nbsp;&nbsp;'.$data['person']['career']['career_name'].'</td>
					<td width="60%">โทรศัพท์&nbsp;&nbsp;'.(empty($data['person']['person_mobile'])? '-' : num2Thai($data['person']['person_mobile'])).'</td>
				</tr>
			</table>
			'.$html_loan_co.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.num2Thai($i_1).') เจ้าหนี้/ผู้รับซื้อฝาก&nbsp;&nbsp;'.(empty($loan_creditor)? '-' : $loan_creditor).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%">ที่อยู่เลขที่&nbsp;&nbsp;'.(empty($loan_creditor_no)? '-' : num2Thai($loan_creditor_no)).'</td>
					<td width="25%">หมู่ที่&nbsp;&nbsp;'.(empty($loan_creditor_moo)? '-' : num2Thai($loan_creditor_moo)).'</td>
					<td width="25%">ถนน&nbsp;&nbsp;'.(empty($loan_creditor_road)? '-' : num2Thai($loan_creditor_road)).'</td>
					<td width="25%">ตำบล/แขวง&nbsp;&nbsp;'.(empty($loan_creditor_district)? '-' : num2Thai($loan_creditor_district)).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%">อำเภอ&nbsp;&nbsp;'.(empty($loan_creditor_amphur)? '-' : num2Thai($loan_creditor_amphur)).'</td>
					<td width="30%">จังหวัด&nbsp;&nbsp;'.(empty($loan_creditor_province)? '-' : num2Thai($loan_creditor_province)).'</td>
					<td width="40%">อาชีพ&nbsp;&nbsp;'.(empty($loan_creditor_career)? '-' : num2Thai($loan_creditor_career)).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>โทรศัพท์&nbsp;&nbsp;'.(empty($loan_creditor_mobile)? '-' : num2Thai($loan_creditor_mobile)).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๑.๒&nbsp;&nbsp;ผู้ขอสินเชื่อ หรือผู้กู้ร่วม (ถ้ามี) ได้ขอสินเชื่อเป็นเงินจำนวน '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>('.num2wordsThai(number_format($data['loan_amount'], 2)).') โดยมีวัตถุประสงค์ เพื่อนำไป '.$loan_type_objective.'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๑.๓&nbsp;&nbsp;ผู้ขอสินเชื่อ และผู้กู้ร่วม (ถ้ามี) ได้เสนอหลักประกันเพื่อเป็นประกันการชำระหนี้ ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๑) ที่ดิน&nbsp;&nbsp;โดยมี</td>
				</tr>
			</table>
			'.$html_land.'
			'.$html_guarantee_bond.'
			'.$html_guarantee_bookbank.'
			'.$html_guarantee_bondsman.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๑.๔&nbsp;&nbsp;การประกอบอาชีพ และความสามารถในการชำระหนี้ของผู้ขอสินเชื่อ และผู้กู้ร่วม (ถ้ามี) ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๑) ผู้ขอสินเชื่อประกอบอาชีพ '.$data['person']['career']['career_name'].' รายได้เฉลี่ยต่อปี '.num2Thai(number_format($income, 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>'.$html_income_other.' โดยมีรายการค่าใช้จ่าย ประกอบด้วย '.($expenditure_farm==0? '( )' : '(/)').' ค่าใช้จ่ายในการทำเกษตรกรรม จำนวน '.num2Thai(number_format($expenditure_farm, 2)).' บาท ต่อปี '.($expenditure_family==0? '( )' : '(/)').' ค่าใช้จ่ายในครัวเรือน จำนวน '.num2Thai(number_format($expenditure_family, 2)).' บาท ต่อปี '.($expenditure_other==0? '( )' : '(/)').' ค่าใช้จ่ายอื่นๆ '.$expenditure_other_text.' จำนวน '.num2Thai(number_format($expenditure_other, 2)).' บาท ต่อปี ทั้งนี้ โดยผู้ขอสินเชื่อมีค่าใช้จ่ายต่างๆ รวมเป็นเงินจำนวน '.num2Thai(number_format($expenditure_farm + $expenditure_family + $expenditure_other, 2)).' บาท ต่อปี และมีรายได้คงเหลือเมื่อหักค่าใช้จ่ายต่างๆ แล้ว เป็นเงินจำนวน '.num2Thai(number_format($income + $income_other - $expenditure_farm - $expenditure_family - $expenditure_other, 2)).' บาท ต่อปี</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๑.๕&nbsp;&nbsp;ผู้ขอสินเชื่อ และผู้กู้ร่วม (ถ้ามี) ตกลงชำระคืนต้นเงินกู้ พร้อมดอกเบี้ย เป็นงวด'.$type_name.' เท่าๆ กัน</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ไม่ต่ำกว่า '.num2Thai(number_format($per_amount * ((100 + intval($analysis['loan_analysis_interest'])) / 100), 2)).' บาทต่องวด รวมจำนวน '.num2Thai($count_round).' งวด โดยคิดเป็นต้นเงินกู้จำนวนไม่ต่ำกว่า '.num2Thai(number_format($per_amount, 2)).' บาทต่องวด และชำระดอกเบี้ย ในอัตราร้อยละ '.num2Thai($analysis['loan_analysis_interest']).' ต่อปี นับแต่วันถัดจากวันรับเงินกู้ โดยเริ่มชำระงวดแรกในวันที่ '.$round_start.' และชำระต้นเงินคงเหลือทั้งหมดพร้อมดอกเบี้ยในงวดสุดท้ายในวันที่ '.$round_end.' รวมเป็นระยะเวลา '.num2Thai($analysis['loan_analysis_year']).' ปี ซึ่งเป็นระยะเวลาไม่เกิน ๓๐ ปี นับจากวันที่ทำสัญญาเสร็จสมบูรณ์ และเป็นไปตามข้อบังคับ บจธ. ว่าด้วย การให้สินเชื่อเพื่อป้องกันและแก้ไขปัญหาการสูญเสียสิทธิในที่ดินเกษตรกรรม พ.ศ. ๒๕๕๙</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">ทั้งนี้ ผู้ขอสินเชื่อ และผู้กู้ร่วม (ถ้ามี) ยินยอมชำระค่าธรรมเนียมและค่าใช้จ่าย ในการอำนวยสินเชื่อของ บจธ.</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>และยินยอมชำระดอกเบี้ยตามอัตราที่ บจธ. กำหนด</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%" style="font-weight: bold;">๒. <u>การดำเนินการ</u></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">เจ้าหน้าที่สินเชื่อ ได้ดำเนินการตรวจสอบรายละเอียดและเอกสารหลักฐานต่างๆ เกี่ยวกับการขอสินเชื่อของ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ผู้ขอสินเชื่อ และผู้กู้ร่วม (ถ้ามี) แล้วสรุปได้ ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๒.๑&nbsp;&nbsp;ผู้ขอสินเชื่อมีคุณสมบัติในเบื้องต้นครบถ้วนตามข้อบังคับฯ และผู้ขอสินเชื่อมีอายุ&nbsp;&nbsp;'.num2Thai($data['person']['person_age']).'&nbsp;&nbsp;ปี</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>โดยผู้ขอสินเชื่อได้ขอสินเชื่อเงินจำนวน '.num2Thai(number_format($data['loan_amount'], 2)).' บาท เพื่อนำไปใช้ '.$html_type.' (ให้ระบุว่า ไถ่ถอนที่ดินจำนอง หรือขายฝาก จากผู้รับจำนองหรือผู้รับซื้อฝากซึ่งอยู่ในอายุสัญญา หรือชำระหนี้ตามสัญญากู้ยืมเงิน ซึ่งได้นำโฉนดที่ดิน หรือเอกสารสิทธิในที่ดินให้เจ้าหนี้ยึดถือไว้หรือชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน หรือซื้อที่ดินที่ถูกขายทอดตลาด หรือหลุดขายฝากไปแล้วไม่เกิน ๕ ปี โดยเจ้าของที่ดินเดิม ที่ถูกบังคับจำนองด้วยการขายทอดตลาด หรือผู้ขายฝากหรือทายาทที่ประสงค์จะนำที่ดินไปประกอบเกษตรกรรม หรือเพื่อการประกอบอาชีพเกษตรกรรม) ซึ่งเป็นวงเงินสินเชื่อไม่เกินรายละ  ๓,๐๐๐,๐๐๐ หรือ ๒๐๐,๐๐๐ บาท เป็นไปตามข้อบังคับฯ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">สำหรับในกรณีผู้ขอสินเชื่ออายุเกิน ๖๕ ปีบริบูรณ์ หรือไม่มีความสามารถชำระหนี้ได้เพียงพอ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ผู้ขอสินเชื่อได้จัดให้มีผู้กู้ร่วม ซึ่งมีรายละเอียดตามข้อ ๑.๑ ๒) โดยมีความสัมพันธ์เป็น</td>
				</tr>
			</table>
			'.$html_loan_co2.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">และมีคุณสมบัติเบื้องต้นตามที่กำหนดไว้ตามข้อบังคับฯ ด้วยแล้ว</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">อย่างไรก็ดี จากการตรวจสอบที่ดิน พร้อมทั้งสิ่งปลูกสร้าง (ถ้ามี) เป็นที่ดินไม่อยู่ในเขตที่ดินของรัฐ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>หรือเขตสงวนหวงห้ามของทางราชการ เช่น เขตทหาร ป่าสงวนแห่งชาติ อุทยานแห่งชาติ หรือเป็นที่สาธารณประโยชน์ เป็นต้น</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๒.๓&nbsp;&nbsp;พันธบัตรรัฐบาล เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ หรือการค้ำประกัน</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ด้วยบุคคล ตามข้อ ๑.๓ ๒) – ๔) แล้วปรากฏว่า</td>
				</tr>
			</table>
			'.$html_guarantee_bond3.'
			'.$html_guarantee_bookbank3.'
			'.$html_guarantee_bondsman3.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%" style="font-weight: bold;">๓. <u>ข้อพิจารณา</u></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">เจ้าหน้าที่สินเชื่อพิจารณาแล้วว่า จากการตรวจข้อมูลและเอกสารหลักฐานต่างๆ เกี่ยวกับผู้ขอสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>และผู้กู้ร่วม (ถ้ามี) รวมทั้งหลักประกันการชำระหนี้ แล้วมีความเห็น ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๓.๑&nbsp;&nbsp;วงเงินที่ขอสินเชื่อ '.num2Thai(number_format($data['loan_amount'], 2)).' บาท ('.num2wordsThai(number_format($data['loan_amount'], 2)).')</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๓.๒&nbsp;&nbsp;หลักประกัน ดังนี้</td>
				</tr>
			</table>
			'.$html_guarantee_bond4.'
			'.$html_guarantee_bookbank4.'
			'.$html_guarantee_bondsman4.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%"></td>
					<td width="70%">อย่างไรก็ดี ในกรณีราคาประเมินหลักประกันมีจำนวนน้อยกว่าจำนวนเงิน ที่ขอรับสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>แต่ไม่น้อยกว่าร้อยละ ๔๐ ของจำนวนเงินที่ขอรับสินเชื่อ ไม่ให้มีการค้ำประกันด้วยบุคคลแต่อย่างใด</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%"></td>
					<td width="70%">ทั้งนี้ เมื่อคิดราคาประเมินที่ดิน พร้อมทั้งสิ่งปลูกสร้าง (ถ้ามี) และ/หรือมูลค่าพันธบัตร</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>รัฐบาล เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ การค้ำประกันด้วยบุคคล รวมกับราคาประเมินอสังหา<br/>ริมทรัพย์ตามข้อ ๓.๑ แล้ว สามารถนำมาเป็นหลักประกันเพิ่มเติมได้ เนื่องจากถือว่าเป็นหลักประกันที่นำมาประกันการชำระ<br/>หนี้ซึ่งไม่น้อยกว่าจำนวนเงินที่ขอรับสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๓.๓&nbsp;&nbsp;ความสามารถในการชำระคืนสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">ได้ ภายใน '.num2Thai($count_round).' งวด '.num2Thai($analysis['loan_analysis_year']).' ปี งวดละ '.num2Thai(number_format($per_amount, 2)).' บาท </td>
				</tr>
				<tr>
					<td width="25%"></td>
					<td width="75%">เห็นควร ( ) อนุมัติ ( ) ไม่อนุมัติ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๓.๔&nbsp;&nbsp;จากการพิจารณาหลักประกันตามข้อ ๓.๑ และ ๓.๒ (ถ้ามี) แล้ว </td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td></td>
				</tr>
				<tr>
					<td>รายละเอียดปรากฏตามเอกสารแนบท้าย</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%" style="font-weight: bold;">๔. <u>ข้อเสนอ</u></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">จึงเรียนมาเพื่อโปรดพิจารณา หากเห็นชอบ จักได้ดำเนินการในส่วนที่เกี่ยวข้องต่อไป</td>
				</tr>
			</table>
			<br/><br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" style="text-align:center;"></td>
					<td width="70%" style="text-align:center;">
						ลงชื่อ ......................................................<br/>
						(...........................................................)<br/>
						เจ้าหน้าที่สินเชื่อ
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%" style="font-weight: bold;">ความเห็นเพิ่มเติม</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>……………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………………...</td>
				</tr>
			</table>
			<br/><br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" style="text-align:center;"></td>
					<td width="70%" style="text-align:center;">
						ลงชื่อ ......................................................<br/>
						(...........................................................)<br/>
						ตำแหน่ง ................................................<br/>
						วันที่ .........................................
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%" style="font-weight: bold;">ข้อสั่งการ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="35%">( ) อนุมัติ ในวงเงินสินเชื่อ</td>
					<td width="50%">บาท เนื่องจาก</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">( ) ไม่อนุมัติ เนื่องจาก </td>
				</tr>
			</table>
			<br/><br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" style="text-align:center;"></td>
					<td width="70%" style="text-align:center;">
						ลงชื่อ ......................................................<br/>
						(...........................................................)<br/>
						ตำแหน่ง ผอ.บจธ.<br/>
						วันที่ .........................................
					</td>
				</tr>
			</table>';

		$pdf->SetFont('thsarabun', '', 16);
		$pdf->writeHTML($htmlcontent, true, 0, true, true);

		$pdf->Output($filename.'.pdf', 'I');
	}
}
?>
