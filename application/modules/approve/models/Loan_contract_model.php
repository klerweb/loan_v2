<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_contract_model extends CI_Model
{
	private $table = 'loan_contract';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_contract_status !=", "0");
		$this->db->where("updatedby", get_uid_login());
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_contract_id", $id);
		$this->db->where("loan_contract_status  !=", "0");
		$this->db->where("updatedby", get_uid_login());
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function search($loan='', $thaiid='', $fullname='')
	{
		$where = '';
		if($loan!='') $where .= "OR loan.loan_code LIKE '%".$loan."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan", "loan.loan_id = loan_contract.loan_id", "LEFT");
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan_contract.loan_contract_status !=", "0");
		$this->db->where("loan_contract.updatedby", get_uid_login());
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>