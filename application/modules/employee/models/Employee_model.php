<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model
{
	private $table = 'employee';
	private $table_user = 'sys_user';
	private $table_position = 'config_position';
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function getByUserId($uid) {
	        $this->db->select("*");
	        $this->db->from($this->table);
	        $this->db->join($this->table_user,"{$this->table}.employee_id = {$this->table_user}.employee_id", "LEFT");
					$this->db->join($this->table_position,"{$this->table}.employee_position_id = {$this->table_position}.position_id", "LEFT");
  				$this->db->where("{$this->table_user}.user_id", $uid);
					$query = $this->db->get();
	        return $query->row_array();
	}

	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("employee_id", $id);
		$query = $this->db->get();

		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}

		return $result;
	}


}
?>
