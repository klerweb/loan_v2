<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relationship extends MY_Controller
{
	private $title_page = 'ความสัมพันธ์';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('relationship_model', 'relationship');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->relationship->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_relationship/relationship.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('relationship', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('relationship')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มความสัมพันธ์';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('relationship_model', 'relationship');
			$data_content['data'] = $this->relationship->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มความสัมพันธ์';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขความสัมพันธ์';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_relationship/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('relationship_model', 'relationship');
		
		$array['relationship_name'] = $this->input->post('txtRelationshipName');
		$array['relationship_status'] = '1';
		
		$id = $this->relationship->save($array, $this->input->post('txtRelationshipId'));
		
		if($this->input->post('txtRelationshipId')=='')
		{
			$data = array(
					'alert' => 'บันทึกความสัมพันธ์เรียบร้อยแล้ว',
					'link' => site_url('relationship'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขความสัมพันธ์เรียบร้อยแล้ว',
					'link' => site_url('relationship'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('relationship_model', 'relationship');
		$this->relationship->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกความสัมพันธ์เรียบร้อยแล้ว',
				'link' => site_url('relationship'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['relationship_name'] = '';
		
		return $data;
	}
}