<script type="text/javascript">
$(document).ready(function(){
	$('#ddlTitle').on('change', function()
	{
		
		var data_old=$('#ddlObjective').val();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable",
			data: {}})
			.success(function(result) {
				
				var split_data_person=result.split("\\");
				
				var count_data = $("#ddlObjective").children().length;
				for(var i=1;i<=count_data;i++)
				{
					$("#ddlObjective").children(i).remove();
			
				}
				$("#ddlObjective").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 2"));
				//alert(split_data_person.length);
				for(var i=0;i<split_data_person.length-1;i++)
				{
						var split_person=split_data_person[i].split(",");
						if ($('#ddlTitle').val()==0)
						{
							$("#ddlObjective").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
						else if (split_person[0]!=$('#ddlTitle').val())
						{
							$("#ddlObjective").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
				}
				if (data_old==$('#ddlTitle').val())
				{
					$("#ddlObjective").val(0);
				}
				else if (data_old!=$('#ddlTitle').val())
				{
					$("#ddlObjective").val(data_old);
				}
			});

	});
	$('#ddlObjective').on('change', function()
	{
		var data_old=$('#ddlTitle').val();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable",
			data: {}})
			.success(function(result) {
				//alert(result);
				var split_data_person=result.split("\\");
				
				var count_data = $("#ddlTitle").children().length;
				for(var i=1;i<=count_data;i++)
				{
					$("#ddlTitle").children(i).remove();
			
				}
				$("#ddlTitle").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 1"));
				//alert(split_data_person.length);
				for(var i=0;i<split_data_person.length-1;i++)
				{
						var split_person=split_data_person[i].split(",");
						if ($('#ddlObjective').val()==0)
						{
							$("#ddlTitle").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
						else if (split_person[0]!=$('#ddlProvince').val())
						{
							$("#ddlTitle").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
				}
				if (data_old==$('#ddlObjective').val())
				{
					$("#ddlTitle").val(0);
				}
				else if (data_old!=$('#ddlObjective').val())
				{
					$("#ddlTitle").val(data_old);
				}
			});

	});
	$("#save_data").click(function(){
		
		var b=true;
		if ($("#lo_id").val()=="")
		{
			alert("กรุณาใส่เลขที่สัญญากู้เงิน");
			b=false;
		}
		else if ($("#id_check").val()=="")
		{
			alert("กรุณาใส่หมายเลขเช็ค");
			b=false;
		}
		else if ($("#ddlStatus").val()==0)
		{
			alert("กรุณาเลือกธนาคาร");
			b=false;
		}
		else if ($("#txtDate").val()=="")
		{
			alert("กรุณาเลือกวันที่โอนให้ถูกต้อง");
			b=false;
		}
		else if ($("#lo_id").val()=="")
		{
			alert("กรุณาใส่เลขที่สัญญาเงินกู้");
			b=false;
		}
		else if ($("#ddlTitle").val()==0)
		{
			alert("กรุณาเลือกผู้อนุมัติเบิกเงิน คนที่ 1");
			b=false;
		}
		else if ($("#ddlObjective").val()==0)
		{
			alert("กรุณาเลือกผู้อนุมัติเบิกเงิน คนที่ 2");
			b=false;
		}
		
		if (b)
		{
			
			var loan_id=$('#loan_id').val();
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/save_finance_disburse",
			data: {loan_contract_id:$("#lo_c_id").val(),loan_contract_no:$("#lo_id").val(),id_check:$("#id_check").val(),bank:$("#ddlStatus").val(), date_tranfer:$("#txtDate").val(),person_payable1:$("#ddlTitle").val(),person_payable2:$("#ddlObjective").val(),ref:$("#txtref").val()}})
			.success(function(result2) {
				
				if(result2=="success")
				{
					
					window.location.href='<?php echo site_url()?>finance_disburse/show_none_withdraw';
				}
				else
				{
					alert(result2);
				}
			});
		}
	});
		});


//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
//------------id_card
function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
        var pattern=new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้
        var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){           
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);           
        }
}
//--------------end id card-----------
</script>

<?php $piece_loan_disbursements=explode("\\",$loan_one_disbursements);
$split2_loan_disbursements=explode(",",$piece_loan_disbursements[0]); ?>

<form>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">ข้อมูลการเบิกจ่ายเงินกู้</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เลขที่สัญญากู้เงิน <span class="asterisk">*</span></label>
								<input type="text" name="lo_id" id="lo_id" class="form-control" value="<?php echo $count_loan_contract_no; ?>" disabled="disabled"/><input type="hidden" name="lo_c_id" id="lo_c_id" value="<?php echo $loan_c_id; ?>">
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ชื่อผู้กู้ <span class="asterisk"></span></label>
								<input type="text" name="lo_name" id="lo_name"  maxlength="255" disabled="disabled" class="form-control" value="<?php echo $split2_loan_disbursements[1]; ?>"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เลขบัตรประชาชน<span class="asterisk"></span></label>
								<input type="text" name="lo_id_card" id="lo_id_card" maxlength="17" value="<?php echo $split2_loan_disbursements[2]; ?>" disabled="disabled" class="form-control"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">จำนวนเงินกู้ <span class="asterisk"></span></label>
								<input type="text" name="lo_amount" id="lo_amount"  maxlength="150" disabled="disabled" class="form-control" value="<?php echo number_format($split2_loan_disbursements[3],2); ?>"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
					</div><!-- row -->

					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมายเลขเช็ค <span class="asterisk">*</span></label>
								<input type="text" name="id_check" id="id_check"  maxlength="10" onkeydown="number();" class="form-control" value=""/> <input type="hidden" name="id_c_check" id="id_c_check"  />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ธนาคาร<span class="asterisk">*</span></label>
								<select name="ddlStatus" id="ddlStatus" data-placeholder="กรุณาเลือกบัญชีธนาคาร" class="width100p" required>
									<option value="">กรุณาเลือกบัญชีธนาคาร</option>
									<?php
									$data_bank=explode("\\",$bank);
									for ($array_bank=0;$array_bank<count($data_bank)-1;$array_bank++)
									{ 
										$split_data_bank=explode(",",$data_bank[$array_bank]);
								?>
								<option value=<?php echo $split_data_bank[0];?>>  <?php echo $split_data_bank[0]." ".$split_data_bank[1]." ".$split_data_bank[2];?> </option>
							<?php }
							?>
							</select> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">วันที่โอน<span class="asterisk">*</span></label>
								<div class="input-group">
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="" readonly="readonly"/>
									<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมายเลขอ้างอิง<span class="asterisk"></span></label>
								<input type="text" name="txtref" id="txtref"  maxlength="10" class="form-control" value=""/> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ผู้อนุมัติการเบิกจ่ายคนที่ 1<span class="asterisk">*</span></label>
								<select name="ddlTitle" id="ddlTitle" data-placeholder="ผู้่อนุมัติเบิกเงิน คนที่ 1" class="width100p" required>
									<option value="">ผู้อนุมัติเบิกเงิน คนที่ 1</option>
									<?php
	$split_data_person=explode("\\",$person_payable1);
	for ($i_person=0;$i_person<count($split_data_person)-1;$i_person++)
	{
		$split_person_payable=explode(",",$split_data_person[$i_person]); ?>
		<option value=<?php echo $split_person_payable[0];?>><?php echo $split_person_payable[1]; ?> </option>
<?php	}
	?>
							</select> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ผู้อนุมัติการเบิกจ่ายคนที่ 2 <span class="asterisk">*</span></label>
								<select name="ddlObjective" id="ddlObjective" data-placeholder="ผู้่อนุมัติเบิกเงิน คนที่ 1" class="width100p" required>
									<option value="">ผู้อนุมัติเบิกเงิน คนที่ 2</option>
									<?php
	$split_data_person=explode("\\",$person_payable1);
	for ($i_person=0;$i_person<count($split_data_person)-1;$i_person++)
	{
		$split_person_payable=explode(",",$split_data_person[$i_person]); ?>
		<option value=<?php echo $split_person_payable[0]; ?>><?php echo $split_person_payable[1]; ?> </option>
<?php	}
	?>
							</select> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					
				<div class="panel-footer" style="text-align:right;">
					<button type="button" class="btn btn-primary" id="save_data" name="save_data">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>