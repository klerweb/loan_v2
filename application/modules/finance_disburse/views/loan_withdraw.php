
<script type="text/javascript">

//id_card: $("#id_card_search").val(),name:$("#name_search").val(),contact_id:$("#contact_id").val()
function fncSubmit()
{
	if ($("#id_card_search").val()=="" && $("#name_search").val()=="" && $("#contact_id").val()=="")
	 {
		 alert("กรุณาใส่ข้อมูลที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}
//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
</script>
<style>
<?php /*body { font-size: 1.2em; }*/?>
</style>
<?php $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
?>

<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url()?>finance_disburse/show_none_withdraw'">เพิ่มการเบิกจ่าย</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('finance_disburse/search_all_loan_disbursement'); ?>" onSubmit="JavaScript:return fncSubmit();">
					<div class="form-group">
						<label class="sr-only" for="id_card_search">บัตรประชาชน</label>
						<input type="text" class="form-control" id="id_card_search" name="id_card_search" autofocus="autofocus"  placeholder="<?php if(isset($txt_id_card)==false)
{
	echo "หมายเลขบัตรประชาชน";
} else { echo $txt_id_card; } ?>" maxlength="13" onkeydown="number();"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="name_search">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="name_search" name="name_search" autofocus="autofocus" placeholder="<?php if(isset($txt_name)==false)
{
	echo "ชื่อนามสกุลผู้กู้";
} else { echo $txt_name; } ?>" maxlength="200"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="contact_id">เลขที่สัญญาเงินกู้</label>
						<input type="text" class="form-control" id="contact_id" name="contact_id" autofocus="autofocus" placeholder="<?php if(isset($txt_contact_id)==false)
{
	echo "เลขที่สัญญาเงินกู้";
} else { echo $txt_contact_id; } ?>" maxlength="50" /> 
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30" id="table_show_loan_withdraw" width="100%" height="100%">
				<thead class="fd">
					<tr>
		            	<th>#</th>
            <th>เลขที่สัญญาเงินกู้</th>
            <th><center>ชื่อผู้กู้</center></th>
            <th><center>ที่อยู่</center></th>
			<th>จำนวนเงินกู้</th>
			<th>หมายเลขอ้างอิง</th>
			<th>วันที่จ่ายสินเชื่อ</th>
			<th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
		<?php
if($loan_disbursements!="no data")
{
$i_no = 1;
$pieces_loan_disbursements = explode("\\", $loan_disbursements);
for ($show_array=0;$show_array<count($pieces_loan_disbursements)-1;$show_array++)
{
	$split2_loan_disbursements=explode(",",$pieces_loan_disbursements[$show_array]); ?>
<tr>
						<td><?php echo $i_no++; ?></td>
						<td><?php echo $split2_loan_disbursements[1]; ?></td>
						<td><?php echo $split2_loan_disbursements[2]; ?></td>
						<td><?php echo $split2_loan_disbursements[10]; ?></td>
		           		<td><p align="right"><?php echo number_format($split2_loan_disbursements[9],2); ?></p></td>
		           		<td><?php echo $split2_loan_disbursements[11]; ?></td>
						
						<td><?php $split_day=explode("-", $split2_loan_disbursements[6]);

$year=intval($split_day[0])+543;
echo intval($split_day[2])." ".$month[intval($split_day[1])-1]." ".$year; ?></td>
		           		<td>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="
							if(confirm('คุณต้องการแก้ไขบันทึกการเบิกจ่ายใช่หรือไม่?'))
	{ location.href='<?php echo site_url()?>finance_disburse/show_one_person_loan_disbursements/<?php echo $split2_loan_disbursements[0];?>';

	}"><span class="fa fa-edit"></span></button>
		           		</td>
		           	</tr>
					<?php
}
}
?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>