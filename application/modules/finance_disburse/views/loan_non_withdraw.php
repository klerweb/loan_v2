<script type="text/javascript">
function fncSubmit()
{
	if ($("#id_card_search").val()=="" && $("#name_search").val()=="" && $("#contact_id").val()=="")
	 {
		 alert("กรุณาใส่ข้อมูลที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}
//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
</script>
<?php $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('finance_disburse/search_non_loan_disbursement'); ?>" onSubmit="JavaScript:return fncSubmit();">
					<div class="form-group">
						<label class="sr-only" for="id_card_search">บัตรประชาชน</label>
						<input type="text" class="form-control" id="id_card_search" name="id_card_search" autofocus="autofocus"  placeholder="<?php if(isset($txt_id_card)==false)
{
	echo "หมายเลขบัตรประชาชน";
} else { echo $txt_id_card; } ?>" maxlength="13" onkeydown="number();"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="name_search">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="name_search" name="name_search" autofocus="autofocus" placeholder="<?php if(isset($txt_name)==false)
{
	echo "ชื่อนามสกุลผู้กู้";
} else { echo $txt_name; } ?>" maxlength="200"/> 
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5" id="btn_search">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
            <th>ชื่อผู้กู้</th>
            <th>ที่อยู่</th>
			<th>จำนวนเงินกู้</th>
					</tr>
				</thead>
				<tbody>
		<?php
if($loan_disbursements!="no data")
{
$i_no = 1;
$pieces_loan_disbursements = explode("\\", $loan_disbursements);
for ($show_array=0;$show_array<count($pieces_loan_disbursements)-1;$show_array++)
{
	$split2_loan_disbursements=explode(",",$pieces_loan_disbursements[$show_array]); ?>
<tr>
						<td><?php echo $i_no++; ?></td>
						<td><?php echo $split2_loan_disbursements[1]; ?></td>
						<td><?php echo $split2_loan_disbursements[4]; ?></td>
		           		<td><?php echo number_format($split2_loan_disbursements[3],2); ?></td>
		           		<td>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="เพิ่ม" data-original-title="เพิ่ม" onclick="
							if(confirm('คุณต้องการบันทึกการเบิกจ่ายใช่หรือไม่?'))
	{ location.href='<?php echo site_url()?>finance_disburse/show_non_person_for_withdraw/<?php echo $split2_loan_disbursements[0];?>';

	}"><span class="fa fa-edit"></span></button>
		           		</td>
		           	</tr>
					<?php
}
}
?>

			
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>