<br>

<script type="text/javascript">
var month = ["มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
$(document).ready(function(){
  
  $('#search_id_card').click(function(){
	  var id_card="";
	  if ($('#lo_id_card').val()!="" )
	  {
		  var lo_id_card=$('#lo_id_card').val();
		  var split_id_card=lo_id_card.split("-");
		
		for (var i_card=0;i_card<split_id_card.length ;i_card++ )
		{
			id_card=id_card+split_id_card[i_card];
		}
	  }
	  else {
			//var lo_id_card=$("#show_thaiid").val();
			if($("#lo_id_card1").val()!="")
			{
				id_card=$("#lo_id_card1").val();
			}
			else
			{
				alert("กรุณากรอกหมายเลขบัตรประชาชน");
			}
	  }

		$.ajax({
type: "POST",
url: "<?php echo site_url(); ?>" + "finance_disburse/search_id_card",
data: {id_card:id_card}})
.success(function(result) { 
	
	$("#lo_fname").val("");
	$("#loan_name").val("");
	$("#title").val(0);
	if (result=="ไม่มีหมายเลขบัตรประชาชนนี้" || result=="ไม่มีข้อมูลการอนุมัติการกู้ยืมเงิน")
	{
		$("#div_error_id_card").html(result);
		$("#contact_id_show").html("");
		$("#contact_id_text").html("");
		var contact_id_text=document.createElement("INPUT");
		contact_id_text.setAttribute("type", "text");
		contact_id_text.setAttribute("id", "lo_id");
		$("#contact_id_text").html(contact_id_text);
		$("#lo_id").val("");
		$("#lo_id").attr("disabled", "disabled");
		$("#lo_name").val("");
		$("#lo_name").attr("disabled", "disabled");
		$("#lo_amount").val("");
		$("#lo_amount").attr("disabled", "disabled");
		$("#bank").val(0);
		//$("#bank").attr("disabled", "disabled");
		$("#id_check").val("");
		$("#id_check").attr("disabled", "disabled");
			$("#day_tran").val(0);
			$("#year_tran").val(0);
			$("#month_tran").val(0);
			$("#day_tran").attr("disabled", "disabled");
			$("#year_tran").attr("disabled", "disabled");
			$("#month_tran").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			$("#loan_id").val("");
			$("#person_payable1").val(0);
			$("#person_payable2").val(0);
	}
	else
	{
		
		var res=result.split(",");
		$("#lo_id").val("");
		$("#div_error_id_card").html("");
		if (res[0]=="1")
		{
			$("#contact_id_show").html("");
			$("#contact_id_text").html("");
			var contact_id_text=document.createElement("INPUT");
			contact_id_text.setAttribute("type", "text");
			contact_id_text.setAttribute("id", "lo_id");
			$("#contact_id_text").html(contact_id_text);
			$("#lo_id").val(res[1]);
			$("#lo_id").attr("disabled", "disabled");
			$("#lo_name").val(res[2]);
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").val(res[3]);
			$("#lo_amount").attr("disabled", "disabled");
			$("#bank").val(res[5]);
			$("#bank").attr("disabled", "disabled");
			$("#id_check").val(res[4]);
			$("#id_check").attr("disabled", "disabled");
			res_date_tran=res[6].split("-");
			var show_date_tran=res_date_tran[2]+" "+month[parseInt(res_date_tran[1])-1]+" พ.ศ."+(parseInt(res_date_tran[0])+543);
			//$("#date_trans").html(show_date_tran);
			$("#id_tran").attr("disabled", "disabled"); 
			$("#id_check").attr("disabled", "disabled"); 
			$("#branch_bank_name").attr("disabled", "disabled"); 
			$("#bank").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			$("#day_tran").removeAttr("disabled");
			var count_day = $("#day_tran").children().length;
			for(var i=1;i<=count_day;i++)
			{
				$("#day_tran").children(i).remove();
			
			}
			$("#day_tran").append($("<option></option>").val(0).html("วัน"));
			for (var day_tr=1;day_tr<=31 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			$("#day_tran").attr("disabled", "disabled");
			var count_month = $("#month_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#month_tran").children(i).remove();
			
			}
			$("#month_tran").attr("disabled", "disabled");
			$("#month_tran").append($("<option></option>").val(0).html("เดือน"));
			for (var month_tr=0;month_tr<=11 ;month_tr++ )
			{
				$("#month_tran").append($("<option></option>").val(month_tr+1).html(month[month_tr]));
			}
			var count_year = $("#year_tran").children().length;
			for(var i=1;i<=count_year;i++)
			{
				$("#year_tran").children(i).remove();
			
			}
			var d = new Date();
			var n = d.getFullYear();
			$("#year_tran").attr("disabled", "disabled");
			$("#year_tran").append($("<option></option>").val(0).html("ปี พ.ศ."));
			for (var year_tr=n-10;year_tr<=n ;year_tr++ )
			{
				$("#year_tran").append($("<option></option>").val(year_tr).html(year_tr+543));
			}
			$("#day_tran").val(parseInt(res_date_tran[2]));
			$("#year_tran").val(parseInt(res_date_tran[0]));
			$("#month_tran").val(parseInt(res_date_tran[1]));

			var count_payable1 = $("#person_payable1").children().length;
			for(var i=1;i<=count_payable1;i++)
			{
				$("#person_payable1").children(i).remove();
				$("#person_payable2").children(i).remove();
			}
			$("#person_payable1").attr("disabled", "disabled");
			$("#person_payable2").attr("disabled", "disabled");
			$("#person_payable1").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 1"));
			$("#person_payable2").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 2"));
			$.ajax({
		type: "POST",
		url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable_non_active",
		data: {}})
		.success(function(result) {
			var res1=result.split("\\");
			for(var i_payable=0;i_payable<res1.length-1;i_payable++)
			{
				var res_split1=res1[i_payable].split(",");
				$("#person_payable1").append($("<option></option>").val(res_split1[0]).html(res_split1[1]));
				$("#person_payable2").append($("<option></option>").val(res_split1[0]).html(res_split1[1]));
				
			}
			$("#person_payable1").val(res[7]);
			$("#person_payable2").val(res[8]);
			
			
		})	
		}
		else if (res[0]=="0")
		{
			$("#contact_id_show").html("");
			$("#contact_id_text").html("");
			var contact_id_text=document.createElement("INPUT");
			contact_id_text.setAttribute("type", "text");
			contact_id_text.setAttribute("id", "lo_id");
			$("#contact_id_text").html(contact_id_text);
			$("#lo_id").val("");
			$("#lo_id").removeAttr("disabled");
			$("#lo_name").val(res[1]);
			$("#lo_name").removeAttr("disabled");
			$("#lo_amount").val(res[2]);
			$("#lo_amount").removeAttr("disabled");
			$("#bank").val(0);
			$("#bank").removeAttr("disabled");
			$("#id_check").val("");
			$("#id_check").removeAttr("disabled");
			$("#loan_id").val(res[3]);
			$("#person_payable1").val(0);
			$("#person_payable2").val(0);
			$("#person_payable1").removeAttr("disabled");
			$("#person_payable2").removeAttr("disabled");
			var count = $("#bank").children().length;
			for(var i=1;i<=count;i++)
			{
				$("#bank").children(i).remove();
			}
			$("#bank").append($("<option></option>").val(0).html("กรุณาเลือกบัญชีธนาคาร"));
			$.ajax({
			
				type: "POST",
				url: "<?php echo site_url(); ?>" + "finance_disburse/check_bank_active",
				})
				.success(function(result1) {
					var data_bank_active=result1.split("\\");
					for (var i=0;i<data_bank_active.length-1 ;i++ )
					{
						var split_data_bank=data_bank_active[i].split(",");
						$("#bank").append($("<option></option>").val(split_data_bank[0]).html(split_data_bank[0]+" "+split_data_bank[1]+" "+split_data_bank[2]));
					}
				});
			$("#day_tran").removeAttr("disabled");
			var count_day = $("#day_tran").children().length;
			for(var i=1;i<=count_day;i++)
			{
				$("#day_tran").children(i).remove();
			
			}
			$("#day_tran").append($("<option></option>").val(0).html("วัน"));
			for (var day_tr=1;day_tr<=31 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			$("#day_tran").removeAttr("disabled");
			var count_month = $("#month_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#month_tran").children(i).remove();
			
			}
			$("#month_tran").removeAttr("disabled");
			$("#month_tran").append($("<option></option>").val(0).html("เดือน"));
			for (var month_tr=0;month_tr<=11 ;month_tr++ )
			{
				$("#month_tran").append($("<option></option>").val(month_tr+1).html(month[month_tr]));
			}
			var count_year = $("#year_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#year_tran").children(i).remove();
			
			}
			var d = new Date();
			var n = d.getFullYear();
			$("#year_tran").removeAttr("disabled");
			$("#year_tran").append($("<option></option>").val(0).html("ปี พ.ศ."));
			for (var year_tr=n-10;year_tr<=n ;year_tr++ )
			{
				$("#year_tran").append($("<option></option>").val(year_tr).html(year_tr+543));
			}
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			var save_button=document.createElement("INPUT");
				save_button.setAttribute("type", "button");
				save_button.setAttribute("value", "บันทึกข้อมูล");
				save_button.setAttribute("id", "save_button");
				$("#save_data").html(save_button);
		}
		else
		{
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			$("#person_payable1").val(0);
			$("#person_payable2").val(0);
			$("#lo_name").val("");
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").val("");
			$("#lo_amount").attr("disabled", "disabled");
			$("#bank").val(0);
			//$("#bank").attr("disabled", "disabled");
			$("#id_check").val("");
			$("#id_check").attr("disabled", "disabled");
			$("#day_tran").val(0);
			$("#year_tran").val(0);
			$("#month_tran").val(0);
			$("#day_tran").attr("disabled", "disabled");
			$("#year_tran").attr("disabled", "disabled");
			$("#month_tran").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			var res_check=res[0].split("\\");
			var res_data_split=result.split("\\");
			if (res_check[0]=="more")
			{
				
				$("#contact_id_show").html("");
				$("#contact_id_text").html("");
				var contact_id_select=document.createElement("SELECT");
				contact_id_select.setAttribute("id", "loan_id_select");
				$("#contact_id_show").html(contact_id_select);
				$("#loan_id_select").append('<option value=0>กรุณาเลือกสัญญาเงินกู้</option>');
				for(var i_select=1;i_select<res_data_split.length-1;i_select++)
				{
					var split_contact=res_data_split[i_select].split(",");
					if (split_contact[1]!="")
					{
						$("#loan_id_select").append($("<option></option>").val(split_contact[0]).html(split_contact[1]));
					}
					else
					{
						$("#loan_id_select").append($("<option></option>").val(split_contact[0]).html("ยังไม่มีหมายเลขสัญญาเงินกู้"));
					}
				}
				$("#loan_id").val(res_data_split[res_data_split.length-1]);
			}
		}
	}
	
	//	.error(function() { $("#div1").html("error"); })
		//.complete(function() { $("#div1").after("Ajax load finished"); });
})
});
$('#search_loan_fname').click(function(){
	var c=true;
	if($("#title").val()==0)
	{
		c=false;
		alert("กรุณาเลือกคำนำหน้านาม");
		
	}
	else if ($("#loan_fname").val()=="")
	{
		c=false;
		alert("กรุณาใส่ชื่อในการค้นหา");
	}
	else if ($("#loan_name").val()=="")
	{
		c=false;
		alert("กรุณาใส่นามสกุลในการค้นหา");
	}
	if (c)
	{
		$("#person_thaiid").html("");
						$("#span_show_thaiid").html("");
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/search_loan_name",
			data: {title: $("#title").val(),fname:$("#loan_fname").val(),lname:$("#loan_name").val()}})
			.success(function(result) {
				
				$("#lo_id_card").val("");
				if(result=="ไม่พบข้อมูลในการค้นหา" || result=="ไม่พบข้อมูลในการกู้เงิน")
				{

					$("#contact_id_show").html("");
						$("#contact_id_text").html("");
						var contact_id_text=document.createElement("INPUT");
						contact_id_text.setAttribute("type", "text");
						contact_id_text.setAttribute("id", "lo_id");
						$("#contact_id_text").html(contact_id_text);
						$("#lo_id").val("");
						$("#lo_id").attr("disabled", "disabled");
						$("#lo_name").val("");
						$("#lo_name").attr("disabled", "disabled");
						$("#lo_amount").val("");
						$("#lo_amount").attr("disabled", "disabled");
						$("#bank").val(0);
						$("#bank").attr("disabled", "disabled");
						$("#id_check").val("");
						$("#id_check").attr("disabled", "disabled");
						$("#day_tran").val(0);
						$("#year_tran").val(0);
						$("#month_tran").val(0);
						$("#day_tran").attr("disabled", "disabled");
						$("#year_tran").attr("disabled", "disabled");
						$("#month_tran").attr("disabled", "disabled");
						$("#confirm_data").html("");
						$("#save_data").html("");
						$("#delete_data").html("");
						$("#loan_id").val("");
						$("#person_payable1").val(0);
						$("#person_payable2").val(0);
						$("#person_payable1").attr("disabled", "disabled");
						$("#person_payable2").attr("disabled", "disabled");

				}
				else
				{

					var res=result.split("\\");
					if(res[res.length-2]>1)
					{
						$("#person_thaiid").html("ข้อมูลที่ค้นหาพบ:");
						var show_thaiid_select=document.createElement("SELECT");
						show_thaiid_select.setAttribute("id", "show_thaiid");
						$("#span_show_thaiid").html(show_thaiid_select);
						$("#show_thaiid").append('<option value=0>กรุณาเลือกหมายเลขบัตรประชาชน</option>')

						for(var i=0;i<res.length-2;i++)
						{
							var res_split=res[i].split(",");	
								$("#show_thaiid").append($("<option></option>").val(res_split[0]).html(res_split[0]+" "+res_split[1]));
						}
						$("#contact_id_show").html("");
						$("#contact_id_text").html("");
						var contact_id_text=document.createElement("INPUT");
						contact_id_text.setAttribute("type", "text");
						contact_id_text.setAttribute("id", "lo_id");
						$("#contact_id_text").html(contact_id_text);
						$("#lo_id").val("");
						$("#lo_id").attr("disabled", "disabled");
						$("#lo_name").val("");
						$("#lo_name").attr("disabled", "disabled");
						$("#lo_amount").val("");
						$("#lo_amount").attr("disabled", "disabled");
						$("#bank").val(0);
						$("#bank").attr("disabled", "disabled");
						$("#id_check").val("");
						$("#id_check").attr("disabled", "disabled");
						$("#day_tran").val(0);
						$("#year_tran").val(0);
						$("#month_tran").val(0);
						$("#day_tran").attr("disabled", "disabled");
						$("#year_tran").attr("disabled", "disabled");
						$("#month_tran").attr("disabled", "disabled");
						$("#confirm_data").html("");
						$("#save_data").html("");
						$("#delete_data").html("");
						$("#loan_id").val("");
						$("#person_payable1").val(0);
						$("#person_payable2").val(0);
						$("#person_payable1").attr("disabled", "disabled");
						$("#person_payable2").attr("disabled", "disabled");
					}
					else
					{
							var res_split=res[0].split(",");
						$.ajax({
type: "POST",
url: "<?php echo site_url(); ?>" + "finance_disburse/search_id_card",
data: {id_card:res_split[0]}})
.success(function(result) { 
	
			var res=result.split(",");
		$("#div_error_id_card").html("");
		if (res[0]=="1")
		{
			$("#contact_id_show").html("");
			$("#contact_id_text").html("");
			var contact_id_text=document.createElement("INPUT");
			contact_id_text.setAttribute("type", "text");
			contact_id_text.setAttribute("id", "lo_id");
			$("#contact_id_text").html(contact_id_text);
			$("#lo_id").val(res[1]);
			$("#lo_id").attr("disabled", "disabled");
			$("#lo_name").val(res[2]);
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").val(res[3]);
			$("#lo_amount").attr("disabled", "disabled");
			$("#bank").val(res[5]);
			$("#bank").attr("disabled", "disabled");
			$("#id_check").val(res[4]);
			$("#id_check").attr("disabled", "disabled");
			$("#person_payable1").val(res[7]);
			$("#person_payable2").val(res[8]);
			res_date_tran=res[6].split("-");
			var show_date_tran=res_date_tran[2]+" "+month[parseInt(res_date_tran[1])-1]+" พ.ศ."+(parseInt(res_date_tran[0])+543);
			//$("#date_trans").html(show_date_tran);
			$("#id_tran").attr("disabled", "disabled"); 
			$("#id_check").attr("disabled", "disabled"); 
			$("#branch_bank_name").attr("disabled", "disabled"); 
			$("#bank").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			$("#day_tran").removeAttr("disabled");
			var count_day = $("#day_tran").children().length;
			for(var i=1;i<=count_day;i++)
			{
				$("#day_tran").children(i).remove();
			
			}
			$("#day_tran").append($("<option></option>").val(0).html("วัน"));
			for (var day_tr=1;day_tr<=31 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			$("#day_tran").attr("disabled", "disabled");
			var count_month = $("#month_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#month_tran").children(i).remove();
			
			}
			$("#month_tran").attr("disabled", "disabled");
			$("#month_tran").append($("<option></option>").val(0).html("เดือน"));
			for (var month_tr=0;month_tr<=11 ;month_tr++ )
			{
				$("#month_tran").append($("<option></option>").val(month_tr+1).html(month[month_tr]));
			}
			var count_year = $("#year_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#year_tran").children(i).remove();
			
			}
			var d = new Date();
			var n = d.getFullYear();
			$("#year_tran").attr("disabled", "disabled");
			$("#year_tran").append($("<option></option>").val(0).html("ปี พ.ศ."));
			for (var year_tr=n-10;year_tr<=n ;year_tr++ )
			{
				$("#year_tran").append($("<option></option>").val(year_tr).html(year_tr+543));
			}
			$("#day_tran").val(parseInt(res_date_tran[2]));
			$("#year_tran").val(parseInt(res_date_tran[0]));
			$("#month_tran").val(parseInt(res_date_tran[1]));
		}
		else if (res[0]=="0")
		{
			$("#contact_id_show").html("");
			$("#contact_id_text").html("");
			var contact_id_text=document.createElement("INPUT");
			contact_id_text.setAttribute("type", "text");
			contact_id_text.setAttribute("id", "lo_id");
			$("#contact_id_text").html(contact_id_text);
			$("#lo_id").val("");
			$("#lo_id").removeAttr("disabled");
			$("#lo_name").val(res[1]);
			$("#lo_name").removeAttr("disabled");
			$("#lo_amount").val(res[2]);
			$("#lo_amount").removeAttr("disabled");
			$("#bank").val(0);
			$("#bank").removeAttr("disabled");
			$("#id_check").val("");
			$("#id_check").removeAttr("disabled");
			$("#loan_id").val(res[3]);
			$("#person_payable1").val(0);
			$("#person_payable2").val(0);
			$("#person_payable1").removeAttr("disabled");
			$("#person_payable2").removeAttr("disabled");
			var count = $("#bank").children().length;
			for(var i=1;i<=count;i++)
			{
				$("#bank").children(i).remove();
			}
			$("#bank").append($("<option></option>").val(0).html("กรุณาเลือกบัญชีธนาคาร"));
			$.ajax({
			
				type: "POST",
				url: "<?php echo site_url(); ?>" + "finance_disburse/check_bank_active",
				})
				.success(function(result1) {
					var data_bank_active=result1.split("\\");
					for (var i=0;i<data_bank_active.length-1 ;i++ )
					{
						var split_data_bank=data_bank_active[i].split(",");
						$("#bank").append($("<option></option>").val(split_data_bank[0]).html(split_data_bank[0]+" "+split_data_bank[1]+" "+split_data_bank[2]));
					}
				});
			$("#day_tran").removeAttr("disabled");
			var count_day = $("#day_tran").children().length;
			for(var i=1;i<=count_day;i++)
			{
				$("#day_tran").children(i).remove();
			
			}
			$("#day_tran").append($("<option></option>").val(0).html("วัน"));
			for (var day_tr=1;day_tr<=31 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			$("#day_tran").removeAttr("disabled");
			var count_month = $("#month_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#month_tran").children(i).remove();
			
			}
			$("#month_tran").removeAttr("disabled");
			$("#month_tran").append($("<option></option>").val(0).html("เดือน"));
			for (var month_tr=0;month_tr<=11 ;month_tr++ )
			{
				$("#month_tran").append($("<option></option>").val(month_tr+1).html(month[month_tr]));
			}
			var count_year = $("#year_tran").children().length;
			for(var i=1;i<=count_month;i++)
			{
				$("#year_tran").children(i).remove();
			
			}
			var d = new Date();
			var n = d.getFullYear();
			$("#year_tran").removeAttr("disabled");
			$("#year_tran").append($("<option></option>").val(0).html("ปี พ.ศ."));
			for (var year_tr=n-10;year_tr<=n ;year_tr++ )
			{
				$("#year_tran").append($("<option></option>").val(year_tr).html(year_tr+543));
			}
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			var save_button=document.createElement("INPUT");
				save_button.setAttribute("type", "button");
				save_button.setAttribute("value", "บันทึกข้อมูล");
				save_button.setAttribute("id", "save_button");
				$("#save_data").html(save_button);
		}
		else
		{
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			$("#person_payable1").val(0);
			$("#person_payable2").val(0);
			$("#lo_name").val("");
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").val("");
			$("#lo_amount").attr("disabled", "disabled");
			$("#bank").val(0);
			
			$("#id_check").val("");
			$("#id_check").attr("disabled", "disabled");
			$("#day_tran").val(0);
			$("#year_tran").val(0);
			$("#month_tran").val(0);
			$("#day_tran").attr("disabled", "disabled");
			$("#year_tran").attr("disabled", "disabled");
			$("#month_tran").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			var res_check=res[0].split("\\");
			var res_data_split=result.split("\\");
			if (res_check[0]=="more")
			{
				
				$("#contact_id_show").html("");
				$("#contact_id_text").html("");
				var contact_id_select=document.createElement("SELECT");
				contact_id_select.setAttribute("id", "loan_id_select");
				$("#contact_id_show").html(contact_id_select);
				$("#loan_id_select").append('<option value=0>กรุณาเลือกสัญญาเงินกู้</option>');
				for(var i_select=1;i_select<res_data_split.length-1;i_select++)
				{
					var split_contact=res_data_split[i_select].split(",");
					
					if (split_contact[1]!="")
					{
						$("#loan_id_select").append($("<option></option>").val(split_contact[0]).html(split_contact[1]));
					}
					else
					{
						$("#loan_id_select").append($("<option></option>").val(split_contact[0]).html("ยังไม่มีหมายเลขสัญญาเงินกู้"));
					}
				}
				$("#loan_id").val(res_data_split[res_data_split.length-1]);
			}
		}
})					
					}
				}
			});
	}
	
});

$('#span_show_thaiid').on('change', function()
{
	
	if ($("#show_thaiid").val()==0 || $("#show_thaiid").val()=="")
	{
		$("#loan_fname").val("");
		$("#loan_name").val("");
		$("#title").val(0);
			$("#contact_id_show").html("");
			$("#contact_id_text").html("");
			var contact_id_text=document.createElement("INPUT");
			contact_id_text.setAttribute("type", "text");
			contact_id_text.setAttribute("id", "lo_id");
			contact_id_text.setAttribute("disabled", "true");
			$("#contact_id_text").html(contact_id_text);
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
			$("#person_payable1").val(0);
			$("#person_payable2").val(0);
			$("#lo_name").val("");
			$("#lo_name").attr("disabled", "disabled");
			$("#lo_amount").val("");
			$("#lo_amount").attr("disabled", "disabled");
			$("#bank").val(0);
			
			$("#id_check").val("");
			$("#id_check").attr("disabled", "disabled");
			$("#day_tran").val(0);
			$("#year_tran").val(0);
			$("#month_tran").val(0);
			$("#day_tran").attr("disabled", "disabled");
			$("#year_tran").attr("disabled", "disabled");
			$("#month_tran").attr("disabled", "disabled");
			$("#confirm_data").html("");
			$("#save_data").html("");
			$("#delete_data").html("");
	}
	else
	{
		$("#loan_fname").val("");
		$("#loan_name").val("");
		$("#title").val(0);
		$("#lo_id_card1").val($("#show_thaiid").val());
		$('#search_id_card').click();	
	}
});
$('#contact_id_show').on('change', function()
{
	if($('#loan_id_select').val()==0)
	{
		$("#lo_name").val("");
		$("#lo_name").attr("disabled", "disabled");
		$("#lo_amount").val("");
		$("#lo_amount").attr("disabled", "disabled");
		$("#bank").val(0);
		$("#bank").attr("disabled", "disabled");
		$("#id_check").val("");
		$("#id_check").attr("disabled", "disabled");
		$("#day_tran").val(0);
		$("#year_tran").val(0);
		$("#month_tran").val(0);
		$("#day_tran").attr("disabled", "disabled");
		$("#year_tran").attr("disabled", "disabled");
		$("#month_tran").attr("disabled", "disabled");
		$("#confirm_data").html("");
		$("#save_data").html("");
		$("#delete_data").html("");
		$("#person_payable1").val(0);
		$("#person_payable2").val(0);
		$("#loan_c_id").html("");
	}
	else
	{
		
		$("#lo_name").val("");
		$("#lo_name").attr("disabled", "disabled");
		$("#lo_amount").val("");
		$("#lo_amount").attr("disabled", "disabled");
		$("#bank").val(0);
		$("#bank").attr("disabled", "disabled");
		$("#id_check").val("");
		$("#id_check").attr("disabled", "disabled");
		$("#day_tran").val(0);
		$("#year_tran").val(0);
		$("#month_tran").val(0);
		$("#day_tran").attr("disabled", "disabled");
		$("#year_tran").attr("disabled", "disabled");
		$("#month_tran").attr("disabled", "disabled");
		$("#person_payable1").val(0);
		$("#person_payable2").val(0);
		$.ajax({
		type: "POST",
		url: "<?php echo site_url(); ?>" + "finance_disburse/check_loan_disbursements",
		data: {loan_contact_id: $('#loan_id_select').val()}})
		.success(function(result) {
			
			if(result!="no data")
			{
				var split_data=result.split(",");
				$("#lo_name").val(split_data[0]);
				$("#lo_amount").val(split_data[1]);
				$("#id_check").val(split_data[3]);
				var count_day = $("#day_tran").children().length;
				for(var i=1;i<=count_day;i++)
				{
					$("#day_tran").children(i).remove();
				}
				$("#day_tran").append($("<option></option>").val(0).html("วัน"));
				for (var day_tr=1;day_tr<=31 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				$("#day_tran").attr("disabled", "disabled");
				var count_month = $("#month_tran").children().length;
				for(var i=1;i<=count_month;i++)
				{
					$("#month_tran").children(i).remove();
				}
				$("#month_tran").attr("disabled", "disabled");
				$("#month_tran").append($("<option></option>").val(0).html("เดือน"));
				for (var month_tr=0;month_tr<=11 ;month_tr++ )
				{
					$("#month_tran").append($("<option></option>").val(month_tr+1).html(month[month_tr]));
				}
				var count_year = $("#year_tran").children().length;
				for(var i=1;i<=count_month;i++)
				{
					$("#year_tran").children(i).remove();
				}
				var d = new Date();
				var n = d.getFullYear();
				$("#year_tran").attr("disabled", "disabled");
				$("#year_tran").append($("<option></option>").val(0).html("ปี พ.ศ."));
				for (var year_tr=n-10;year_tr<=n ;year_tr++ )
				{
					$("#year_tran").append($("<option></option>").val(year_tr).html(year_tr+543));
				}
				
				$("#bank").val(split_data[2]);
				var res_date_tran=split_data[4].split("-");
				$("#day_tran").val(res_date_tran[2]);
				$("#month_tran").val(parseInt(res_date_tran[1]));
				$("#year_tran").val(parseInt(res_date_tran[0]));
				$("#person_payable1").val(split_data[5]);
				$("#person_payable2").val(split_data[6]);
				$("#loan_c_id").html("");
				$("#person_payable1").attr("disabled", "disabled");
				$("#person_payable2").attr("disabled", "disabled");
				}
				else
				{
					$("#confirm_data").html("");
					$("#save_data").html("");
					$("#delete_data").html("");
					var save_button1=document.createElement("INPUT");
					save_button1.setAttribute("type", "button");
					save_button1.setAttribute("value", "บันทึกข้อมูล");
					save_button1.setAttribute("id", "save_button");
					$("#confirm_data").html(save_button1);
					var input_text_loan_contact1=document.createElement("INPUT");
					input_text_loan_contact1.setAttribute("type", "text");
					input_text_loan_contact1.setAttribute("id", "loan_contact_name1");
					$("#loan_c_id").html(input_text_loan_contact1);
					$("#lo_name").val("");
					$("#lo_name").attr("disabled", "disabled");
					$("#lo_amount").val("");
					$("#lo_amount").attr("disabled", "disabled");
					$("#bank").val(0);
					$("#bank").removeAttr("disabled");
					$("#id_check").val("");
					$("#id_check").attr("disabled", "disabled");
					$("#day_tran").val(0);
					$("#year_tran").val(0);
					$("#month_tran").val(0);
					$("#day_tran").attr("disabled", "disabled");
					$("#year_tran").attr("disabled", "disabled");
					$("#month_tran").attr("disabled", "disabled");
					$("#person_payable1").val(0);
					$("#person_payable2").val(0);
					$.ajax({
					type: "POST",
					url: "<?php echo site_url(); ?>" + "finance_disburse/check_more_loan_contact",
					data: {loan_id: $("#loan_id").val()}})
					.success(function(result) {
					var split_data=result.split(",");
					$("#lo_name").val(split_data[0]);
					$("#lo_name").attr("disabled", "disabled");
					$("#lo_amount").val(split_data[1]);
					$("#lo_amount").attr("disabled", "disabled");
					$("#bank").removeAttr("disabled");
					$("#id_check").removeAttr("disabled");
					$("#person_payable1").removeAttr("disabled");
					$("#person_payable2").removeAttr("disabled");
					$("#day_tran").removeAttr("disabled");
					$("#month_tran").removeAttr("disabled");
					$("#year_tran").removeAttr("disabled");
					var count_day = $("#day_tran").children().length;
					for(var i=1;i<=count_day;i++)
					{
					$("#day_tran").children(i).remove();
					}
					$("#day_tran").append($("<option></option>").val(0).html("วัน"));
					for (var day_tr=1;day_tr<=31 ;day_tr++ )
					{
						$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
					}
					$("#day_tran").removeAttr("disabled");
					var count_month = $("#month_tran").children().length;
					for(var i=1;i<=count_month;i++)
					{	
						$("#month_tran").children(i).remove();
			
					}
					$("#month_tran").removeAttr("disabled");
					$("#month_tran").append($("<option></option>").val(0).html("เดือน"));
					for (var month_tr=0;month_tr<=11 ;month_tr++ )
					{
						$("#month_tran").append($("<option></option>").val(month_tr+1).html(month[month_tr]));
					}
					var count_year = $("#year_tran").children().length;
					for(var i=1;i<=count_month;i++)
					{
						$("#year_tran").children(i).remove();
			
					}
					var d = new Date();
					var n = d.getFullYear();
					$("#year_tran").removeAttr("disabled");
					$("#year_tran").append($("<option></option>").val(0).html("ปี พ.ศ."));
					for (var year_tr=n-10;year_tr<=n ;year_tr++ )
					{
						$("#year_tran").append($("<option></option>").val(year_tr).html(year_tr+543));
					}
				});
				}
			});
			
	}
});
	$('#person_payable1').on('change', function()
	{
		var data_old=$('#person_payable2').val();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable",
			data: {}})
			.success(function(result) {
				//alert(result);
				var split_data_person=result.split("\\");
				
				var count_data = $("#person_payable2").children().length;
				for(var i=1;i<=count_data;i++)
				{
					$("#person_payable2").children(i).remove();
			
				}
				$("#person_payable2").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 2"));
				//alert(split_data_person.length);
				for(var i=0;i<split_data_person.length-1;i++)
				{
						var split_person=split_data_person[i].split(",");
						if ($('#person_payable1').val()==0)
						{
							$("#person_payable2").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
						else if (split_person[0]!=$('#person_payable1').val())
						{
							$("#person_payable2").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
				}
				if (data_old==$('#person_payable1').val())
				{
					$("#person_payable2").val(0);
				}
				else if (data_old!=$('#person_payable1').val())
				{
					$("#person_payable2").val(data_old);
				}
			});

	});
	$('#person_payable2').on('change', function()
	{
		var data_old=$('#person_payable1').val();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable",
			data: {}})
			.success(function(result) {
				//alert(result);
				var split_data_person=result.split("\\");
				
				var count_data = $("#person_payable1").children().length;
				for(var i=1;i<=count_data;i++)
				{
					$("#person_payable1").children(i).remove();
			
				}
				$("#person_payable1").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 1"));
				//alert(split_data_person.length);
				for(var i=0;i<split_data_person.length-1;i++)
				{
						var split_person=split_data_person[i].split(",");
						if ($('#person_payable2').val()==0)
						{
							$("#person_payable1").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
						else if (split_person[0]!=$('#person_payable2').val())
						{
							$("#person_payable1").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
				}
				if (data_old==$('#person_payable2').val())
				{
					$("#person_payable1").val(0);
				}
				else if (data_old!=$('#person_payable2').val())
				{
					$("#person_payable1").val(data_old);
				}
			});

	});
	$("#delete_data").click(function(){ //ไม่ได้ใช้
		var lo_id=$('#lo_id').val();
		var question = confirm("ยืนยันการลบข้อมูลการเบิกเงินหรือไม่");
			if (question !="0"){
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/delete_finance_disburse",
			data: {id: lo_id}})
			.success(function(result) {
				if(result=="success")
				{
					$('#btnsearch').click();
				}
			});
		}
	});//ไม่ได้ใช้
	$("#save_data").click(function(){
		//alert($("#path_file").val());
		var b=true;
		$("#div_error_contact_id").html("");
		$("#error_id_check").html("");
		$("#error_bank").html("");
		$("#error_date_tran").html("");
		$("#error_person1").html("");
		$("#error_person2").html("");
		if ($("#id_check").val()=="")
		{
			$("#error_id_check").html("กรุณาใส่หมายเลขเช็ค");
			b=false;
		}
		if ($("#bank").val()==0)
		{
			$("#error_bank").html("กรุณาเลือกธนาคาร");
			b=false;
		}
		if ($("#day_tran").val()==0 || $("#month_tran").val()==0 || $("#year_tran").val()==0 )
		{
			$("#error_date_tran").html("กรุณาเลือกวันที่โอนให้ถูกต้อง");
			b=false;
		}
		if ($("#lo_id").val()=="")
		{
			$("#div_error_contact_id").html("กรุณาใส่เลขที่สัญญาเงินกู้");
			b=false;
		}
		if ($("#person_payable1").val()==0)
		{
			$("#error_person1").html("กรุณาเลือกผู้อนุมัติเบิกเงิน คนที่ 1");
			b=false;
		}
		if ($("#person_payable2").val()==0)
		{
			$("#error_person2").html("กรุณาเลือกผู้อนุมัติเบิกเงิน คนที่ 1");
			b=false;
		}
		if (b)
		{
			
			var loan_id=$('#loan_id').val();
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/save_finance_disburse",
			data: {id: loan_id,id_check:$("#id_check").val(),bank:$("#bank").val(),date_tran:$("#year_tran").val()+"-"+$("#month_tran").val()+"-"+$("#day_tran").val(),loan_contact_id:$("#lo_id").val(),person_payable1:$("#person_payable1").val(),person_payable2:$("#person_payable2").val(),loan_contact_amount:$("#lo_amount").val()}})
			.success(function(result2) {
				
				if(result2=="success")
				{
					
					$('#search_id_card').click();
				}
				else
				{
					alert(result2);
				}
			});
		}	
	});
$("#confirm_data").click(function(){
		var b=true;
		$("#div_error_contact_id").html("");
		$("#error_id_check").html("");
		$("#error_bank").html("");
		$("#error_date_tran").html("");
		$("#error_person1").html("");
		$("#error_person2").html("");
		if ($("#id_check").val()=="")
		{
			$("#error_id_check").html("กรุณาใส่หมายเลขเช็ค");
			b=false;
		}
		if ($("#bank").val()==0)
		{
			$("#error_bank").html("กรุณาเลือกธนาคาร");
			b=false;
		}
		if ($("#day_tran").val()==0 || $("#month_tran").val()==0 || $("#year_tran").val()==0 )
		{
			$("#error_date_tran").html("กรุณาเลือกวันที่โอนให้ถูกต้อง");
			b=false;
		}
		if ($("#loan_contact_name1").val()=="")
		{
			$("#div_error_contact_id").html("กรุณาใส่เลขที่สัญญาเงินกู้");
			b=false;
		}
		if ($("#person_payable1").val()==0)
		{
			$("#error_person1").html("กรุณาเลือกผู้อนุมัติเบิกเงิน คนที่ 1");
			b=false;
		}
		if ($("#person_payable2").val()==0)
		{
			$("#error_person2").html("กรุณาเลือกผู้อนุมัติเบิกเงิน คนที่ 2");
			b=false;
		}
		if (b)
		{
			var loan_id=$('#loan_id').val();
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/save_finance_disburse_t",
			data: {id: loan_id,id_check:$("#id_check").val(),bank:$("#bank").val(),date_tran:$("#year_tran").val()+"-"+$("#month_tran").val()+"-"+$("#day_tran").val(),loan_contact_id:$("#loan_contact_name1").val(),person_payable1:$("#person_payable1").val(),person_payable2:$("#person_payable2").val(),loan_c_id:$('#loan_id_select').val(),loan_contact_amount:$("#lo_amount").val()}})
			.success(function(result2) {
				if(result2=="success")
				{
					
					if($("#lo_id_card").val()!="")
					{
						$('#search_id_card').click();
						$("#loan_c_id").html("");
						$('#loan_id_select').val(0)
					}
					else
					{
						$('#search_loan_fname').click();
						$("#loan_c_id").html("");
						$('#loan_id_select').val(0)
					}

				}
				else
				{
					alert(result2);
				}
			});
		}
		
	});
	$( "#month_tran" ).change(function() {
		var date=$("#day_tran").val();
		var count_day = $("#day_tran").children().length;
		for(var i=1;i<=count_day;i++)
		{
			$("#day_tran").children(i).remove();
			
		}
		$("#day_tran").append($("<option></option>").val(0).html("วัน"));
		if ($( "#month_tran" ).val()==1 || $( "#month_tran" ).val()==3 || $( "#month_tran" ).val()==5 || $( "#month_tran" ).val()==7 || $( "#month_tran" ).val()==8 || $( "#month_tran" ).val()==10 || $( "#month_tran" ).val()==12)
		{
			for (var day_tr=1;day_tr<=31 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			$("#day_tran").val(date);
		}
		else if ($( "#month_tran" ).val()==2)
		{
			if ($( "#year_tran" ).val()!=0 && $( "#year_tran" ).val()%4==0)
			{
				
				for (var day_tr=1;day_tr<=29 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				if (date<=29)
				{
					$("#day_tran").val(date);
				}
				else
				{
					$("#day_tran").val(0);
				}
			}
			else if ($( "#year_tran" ).val()!=0 && $( "#year_tran" ).val()%4!=0)
			{
				for (var day_tr=1;day_tr<=28 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				if (date<=28)
				{
					$("#day_tran").val(date);
				}
				else
				{
					$("#day_tran").val(0);
				}
			}
			else
			{
				for (var day_tr=1;day_tr<=28 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				if (date<=28)
				{
					$("#day_tran").val(date);
				}
				else
				{
					$("#day_tran").val(0);
				}
			}
		}
		else
		{
			for (var day_tr=1;day_tr<=30 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			if (date<=30)
			{
				$("#day_tran").val(date);
			}
			else
			{
				$("#day_tran").val(0);
			}
			
		}
	});
	$( "#year_tran" ).change(function() {
		var date=$("#day_tran").val();
		var count_day = $("#day_tran").children().length;
		for(var i=1;i<=count_day;i++)
		{
			$("#day_tran").children(i).remove();
			
		}
		$("#day_tran").append($("<option></option>").val(0).html("วัน"));
		if ($( "#month_tran" ).val()==1 || $( "#month_tran" ).val()==3 || $( "#month_tran" ).val()==5 || $( "#month_tran" ).val()==7 || $( "#month_tran" ).val()==8 || $( "#month_tran" ).val()==10 || $( "#month_tran" ).val()==12)
		{
			for (var day_tr=1;day_tr<=31 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			$("#day_tran").val(date);
		}
		else if ($( "#month_tran" ).val()==2)
		{
			if ($( "#year_tran" ).val()!=0 && $( "#year_tran" ).val()%4==0)
			{
				
				for (var day_tr=1;day_tr<=29 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				if (date<=29)
				{
					$("#day_tran").val(date);
				}
				else
				{
					$("#day_tran").val(0);
				}
			}
			else if ($( "#year_tran" ).val()!=0 && $( "#year_tran" ).val()%4!=0)
			{
				for (var day_tr=1;day_tr<=28 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				if (date<=28)
				{
					$("#day_tran").val(date);
				}
				else
				{
					$("#day_tran").val(0);
				}
			}
			else
			{
				for (var day_tr=1;day_tr<=28 ;day_tr++ )
				{
					$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
				}
				if (date<=28)
				{
					$("#day_tran").val(date);
				}
				else
				{
					$("#day_tran").val(0);
				}
			}
		}
		else
		{
			for (var day_tr=1;day_tr<=30 ;day_tr++ )
			{
				$("#day_tran").append($("<option></option>").val(day_tr).html(day_tr));
			}
			if (date<=30)
			{
				$("#day_tran").val(date);
			}
			else
			{
				$("#day_tran").val(0);
			}
			
		}

	});
	$("#imgcheck").mouseover (function() {
		$("#imgcheck").style.height = "1000px";
		$("#imgcheck").style.width = "1000px";
	});

});
//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
//------------id_card
function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
        var pattern=new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้
        var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){           
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);           
        }
}
//--------------end id card-----------
</script>


<form method="post">
<table width="933" border="0">
<tr>

  <td width="81" height="20"></td>
    <td width="350"></td>
    <td width="245"><h3>การเบิกจ่ายเงินกู้</h3></td>
    <td width="238"></td>
	<td>&nbsp;</td>
  </tr>
  <tr>
  <td width="81" height="20"></td>
    <td width="350"></td>
    <td width="245"></td>
    <td width="238"></td>
	<td>&nbsp;</td>
  </tr>
  <tr>
  <td width="81" height="34"></td>
    <td width="350">หมายเลขบัตรประชาชน:</td>
    <td width="245"><span id="span_id_card"></label>
	
      <input type="text" name="lo_id_card" id="lo_id_card" maxlength="17" value="" onkeyup="autoTab(this)"/>   <input type="button" name="search_id_card" id="search_id_card" value="ค้นหาเลขบัตร"/><span><div id="div_error_id_card" style="color:red"></div><input type="hidden" name="loan_id" id="loan_id"></input><input type="hidden" name="lo_id_card1" id="lo_id_card1"></input></td>
    <td width="238"></td>
	<td>&nbsp;</td>
  </tr>
 
  <tr>
  <td width="81" height="34"></td>
    <td width="350">ชื่อ-นามสกุลที่ต้องการค้นหา:</td>
    <td width="1300"><span id="span_loan_name"></label>
	<select id="title" name="title">
	<option value=0>คำนำหน้านาม </option>
	<?php 
		foreach($title_show as $show_title)
		{
			?>
				<option value=<?php echo $show_title->title_id; ?>><?php echo $show_title->title_name; ?></option>
			<?php
		}
	?>
	</select>
        ชื่อ : <input type="text" name="loan_fname" id="loan_fname" value=""/>  นามสกุล : <input type="text" name="loan_name" id="loan_name" value=""/> <input type="button" name="search_loan_name" id="search_loan_fname" value="ค้นหาชื่อผู้กู้"/></span>
		<div id="div_error_name" style="color:red"></div>
		</td>
  </tr>
  <tr height="25">
  <td ></td>
    <td width="350"><span id="person_thaiid"></span></td>
    <td width="245"><span id="span_show_thaiid"></span>
	
      </td>
    <td width="238"></td>
	<td>&nbsp;</td>
  </tr>
  <tr>
  <td width="81"></td>
    <td width="350">เลขที่สัญญากู้เงิน:</td>
    <td width="245"><label for="lo_id"></label>
	
      <span id="contact_id_text"><input type="text" name="lo_id" id="lo_id" value="" disabled/></span><span id="contact_id_show"></span>&nbsp&nbsp<span id="loan_c_id"></span><div id="div_error_contact_id" style="color:red"></div></td>
    <td width="238"></td>
	<td>&nbsp;</td>
    
    
  </tr>
  <td height="34"></td>
    <td width="350">ชื่อผู้กู้:</td>
    <td width="245"><label for="lo_name"></label>
      <input type="text" name="lo_name" id="lo_name"  maxlength="255" disabled="disabled"/>      </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td height="34"></td>
    <td width="350">จำนวนเงินกู้:</td>
    <td width="245"><label for="lo_amount"></label>
      <input type="text" name="lo_amount" id="lo_amount"  maxlength="150" disabled="disabled"/>      </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td height="31"></td>
    <td width="350">หมายเลขเช็ค:</td>
    <td width="245"><input type="text" name="id_check" id="id_check"  maxlength="10" disabled="disabled" onkeydown="number();"/> <div id="error_id_check" style="color:red"></div></td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td height="30"></td>
    <td width="350">ธนาคาร :</td>
    <td><select id="bank" name="bank" disabled >
	<option value=0>กรุณาเลือกบัญชีธนาคาร</option>
	<?php
		$data_bank=explode("\\",$bank);
		for ($array_bank=0;$array_bank<count($data_bank)-1;$array_bank++)
		{ 
			$split_data_bank=explode(",",$data_bank[$array_bank]);
			?>
		
			<option value=<?php echo $split_data_bank[0];?>>  <?php echo $split_data_bank[0]." ".$split_data_bank[1]." ".$split_data_bank[2];?> </option>
		<?php }
	?>
	</select> <div id="error_bank" style="color:red"></div>
	</td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td height="33"></td>
    <td width="350">วันที่โอนเงิน  </td>
    <td width="300"> วัน : <select id="day_tran" name="day_tran" disabled>
	<option value=0>วัน</option>
    </select>
	 เดือน : <select id="month_tran" name="month_tran" disabled>
	<option value=0>เดือน</option></select>
	  ปี :  <select id="year_tran" name="year_tran" disabled>
	<option value=0>ปี พ.ศ.</option></select> <div id="error_date_tran" style="color:red"></div>
	</td>
    
    
  </tr>
  <tr>
  <td height="33"></td>
    <td>ผู้อนุมัติการเบิกจ่ายเงิน คนที่ 1: </td>
    <td width="350"> <select id="person_payable1" name="person_payable1" disabled>
	<option value=0>ผู้่อนุมัติเบิกเงิน คนที่ 1</option>
	<?php
	$split_data_person=explode("\\",$person_payable1);
	for ($i_person=0;$i_person<count($split_data_person)-1;$i_person++)
	{
		$split_person_payable=explode(",",$split_data_person[$i_person]); ?>
		<option value=<?php echo $split_person_payable[0]; ?>><?php echo $split_person_payable[1]; ?> </option>
<?php	}
	?>
    </select>
	 <div id="error_person1" style="color:red"></div>
	</td>
  </tr>

   <tr>
  <td height="33"></td>
    <td>ผู้อนุมัติการเบิกจ่ายเงิน คนที่ 2: </td>
    <td width="350"> <select id="person_payable2" name="person_payable2" disabled>
	<option value=0>ผู้่อนุมัติเบิกเงิน คนที่ 2</option>
	<?php
	$split_data_person=explode("\\",$person_payable1);
	for ($i_person=0;$i_person<count($split_data_person)-1;$i_person++)
	{
		$split_person_payable=explode(",",$split_data_person[$i_person]); ?>
		<option value=<?php echo $split_person_payable[0]; ?>><?php echo $split_person_payable[1]; ?> </option>
<?php	}
	?>
    </select>
	 <div id="error_person2" style="color:red"></div>
	</td>
    
    
  </tr>
  <tr>
  <td height="33"></td>
    <td width="350"></td>
    <td><span id="save_data"></span>  <span id="confirm_data"></span> <span id="delete_data"></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

</form>









