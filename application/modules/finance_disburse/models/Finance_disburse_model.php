<?php
class finance_disburse_model extends CI_Model{

	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	function count_page_loan_disbursements()
{
		$query = $this->db->get('loan_disbursements');
		if ($query->num_rows()>0)
		{
			
			$page=floor($query->num_rows()/10);
			//return $query->result();
			if ($query->num_rows()%9>0)
			{
				$page=$page+1;
			}
			return $page;
		}
		else
		{
			return 0;
		}
}

	function load_loan_disbursements()
	{
    
		$data_array_loan_disbursements="";
		$this->db->order_by('id_disbursements','desc');
		$query_loan_disbursements = $this->db->get('loan_disbursements');
		if ($query_loan_disbursements->num_rows()>0)
		{
			foreach($query_loan_disbursements->result() as $data_loan_disbursements)
			{
				$query_loan_contract=$this->db->query("SELECT *  FROM loan_contract where loan_contract_id='$data_loan_disbursements->loan_contract_id'");
				foreach($query_loan_contract->result() as $data_loan_contract)
				{
					$this->db->where("loan_id",$data_loan_contract->loan_id);
					$query_loan=$this->db->get("loan");
					
					foreach($query_loan->result() as $data_loan)
					{
						$this->db->where("person_id",$data_loan->person_id);
						$query_person=$this->db->get("person");
						foreach($query_person->result() as $data_person)
						{
							$this->db->where("employee_id",$data_loan_disbursements->person_payable_1);
							$query_person_payable1=$this->db->get("employee");
							foreach($query_person_payable1->result() as $data_payable1)
							{
								$this->db->where("title_id",$data_payable1->title_id);
								$query_title=$this->db->get("config_title");
								foreach($query_title->result() as $data_title)
								{
									$title_payable1=$data_title->title_name;
								}
							}
							$this->db->where("employee_id",$data_loan_disbursements->person_payable_2);
							$query_person_payable2=$this->db->get("employee");
							foreach($query_person_payable2->result() as $data_payable2)
							{
								$this->db->where("title_id",$data_payable2->title_id);
								$query_title=$this->db->get("config_title");
								foreach($query_title->result() as $data_title)
								{
									$title_payable2=$data_title->title_name;
								}
							}
							$this->db->where("title_id",$data_person->title_id);
							$query_title=$this->db->get("config_title");
							foreach($query_title->result() as $data_title)
							{
								$title_person=$data_title->title_name;
							}
							$this->db->where("com_code",$data_loan_disbursements->com_code);
							$query_com_code=$this->db->get("bank_loan_payment");
							foreach($query_com_code->result() as  $data_com_code)
							{
								$this->db->where('district_id',$data_person->person_addr_card_district_id);
								$query_data_district=$this->db->get('master_district');
								$data_district=$query_data_district->result();
								$this->db->where('amphur_id',$data_person->person_addr_card_amphur_id);
								$query_data_amphur=$this->db->get('master_amphur');
								$data_amphur=$query_data_amphur->result();
								$this->db->where('province_id',$data_person->person_addr_card_province_id);
								$query_data_province=$this->db->get('master_province');
								$data_province=$query_data_province->result();
								$address="";
								if($data_person->person_addr_card_no!=null){
									$address=$address."บ้านเลขที่ ".$data_person->person_addr_card_no." ";
								}
								if($data_person->person_addr_card_moo!=null){
									$address=$address."หมู่ที่ ".$data_person->person_addr_card_moo." ";
								}
								if($data_person->person_addr_card_road!=null){
									$address=$address."ถ. ".$data_person->person_addr_card_road." ";
								}
									$data_array_loan_disbursements=$data_array_loan_disbursements.$data_loan_disbursements->id_disbursements.",".$data_loan_contract->loan_contract_no.",".$title_person." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_com_code->com_code.",".$data_loan_disbursements->id_check.",".$data_loan_disbursements->date_transfer.",".$title_payable1." ".$data_payable1->employee_fname." ".$data_payable1->employee_lname.",".$title_payable2." ".$data_payable2->employee_fname." ".$data_payable2->employee_lname.",".$data_loan_contract->loan_contract_amount.",".$address." ต.".$data_district[0]->district_name." อ.".$data_amphur[0]->amphur_name." จ.".$data_province[0]->province_name.",".$data_loan_disbursements->reference."\\";
							}
							
						}
					}

				}
			}
			return $data_array_loan_disbursements;
		}
		else
		{
			return "no data";
		}
}
function loan_one_disbursements($data_id_disbursement)
{
	
		$data_array_loan_disbursements="";
		$this->db->where('id_disbursements',$data_id_disbursement);
		$query_loan_disbursements = $this->db->get('loan_disbursements');
		if ($query_loan_disbursements->num_rows()>0)
		{
			foreach($query_loan_disbursements->result() as $data_loan_disbursements)
			{
				$query_loan_contract=$this->db->query("SELECT *  FROM loan_contract where loan_contract_id='$data_loan_disbursements->loan_contract_id'");
				foreach($query_loan_contract->result() as $data_loan_contract)
				{
					$this->db->where("loan_id",$data_loan_contract->loan_id);
					$query_loan=$this->db->get("loan");
					foreach($query_loan->result() as $data_loan)
					{
						$this->db->where("person_id",$data_loan->person_id);
						$query_person=$this->db->get("person");
						foreach($query_person->result() as $data_person)
						{
							$this->db->where("title_id",$data_person->title_id);
							$query_title=$this->db->get("config_title");
							foreach($query_title->result() as $data_title)
							{
								$title_person=$data_title->title_name;
							}
							$this->db->where("com_code",$data_loan_disbursements->com_code);
							$query_com_code=$this->db->get("bank_loan_payment");
							foreach($query_com_code->result() as  $data_com_code)
							{
								$this->db->where("bank_id",$data_com_code->bank_id);
								$query_bank=$this->db->get("config_bank");
								foreach($query_bank->result() as  $data_bank)
								{
									$data_array_loan_disbursements=$data_array_loan_disbursements.$data_loan_disbursements->id_disbursements.",".$data_loan_contract->loan_contract_no.",".$title_person." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_com_code->com_code.",".$data_loan_disbursements->id_check.",".$data_loan_disbursements->date_transfer.",".$data_loan_disbursements->person_payable_1.",".$data_loan_disbursements->person_payable_2.",".$data_loan_contract->loan_contract_amount.",".$data_loan_contract->loan_contract_id.",".$data_loan_disbursements->reference."\\";
								}
							}
						}
					}

				}
			}
			return $data_array_loan_disbursements;
		}
		else
		{
			return "no data";
		}
}
function loan_non_disbursements($data_id)
{

	$data_array_loan_disbursements="";
	$this->db->where('loan_contract_id',$data_id);
	$query_non_loan_dis=$this->db->get('loan_contract');
	if($query_non_loan_dis->num_rows()>0)
	{
		foreach($query_non_loan_dis->result() as $data_loan_contract)
		{
			$this->db->where("loan_id",$data_loan_contract->loan_id);
			$query_loan=$this->db->get("loan");
			foreach($query_loan->result() as $data_loan)
			{
				$this->db->where("person_id",$data_loan->person_id);
				$query_person=$this->db->get("person");
				foreach($query_person->result() as $data_person)
				{
					$this->db->where("title_id",$data_person->title_id);
					$query_title=$this->db->get("config_title");
					foreach($query_title->result() as $data_title)
					{
						$title_person=$data_title->title_name;
					}
						
							$data_array_loan_disbursements=$data_array_loan_disbursements.$data_loan_contract->loan_contract_id.",".$title_person." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_contract->loan_contract_amount."\\";			
				}
			}
		}
		return $data_array_loan_disbursements;
	}
	else
	{
		return "no data";
	}
}
function load_bank()
	{
		$data_bank="";
		$query=$this->db->get('bank_loan_payment');
		if($query->num_rows()>0)
		{
			foreach($query->result() as $bank_active)
			{
				$this->db->where('bank_id',$bank_active->bank_id);
				$query1=$this->db->get('config_bank');
				foreach($query1->result() as $bank_name)
				{
					$data_bank=$data_bank.$bank_active->com_code.",".$bank_active->account_name.",".$bank_name->bank_name."\\";
				}
			}
			return $data_bank;
		}
		else
		{
			return "ไม่มี";
		}
	}
	function load_title()
	{
		$query=$this->db->get('config_title');
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return "ไม่มี";
		}
	}
	function load_person_payable1()
	{
		$this->db->where('employee_status','1');
		$query_person_payable1=$this->db->get('employee');
		$data_person_payable="";
		if ($query_person_payable1->num_rows() >0)
		{
			foreach ($query_person_payable1->result() as $data_person_payable1)
			{
				$this->db->where('title_id',$data_person_payable1->title_id);
				$query_title=$this->db->get('config_title');
				if ($query_title->num_rows()>0)
				{
					foreach($query_title->result() as $data_title)
					{
						$data_person_payable=$data_person_payable.$data_person_payable1->employee_id.",".$data_title->title_name." ".$data_person_payable1->employee_fname." ".$data_person_payable1->employee_lname."\\";
					}
				}
			}
			return $data_person_payable;
		}

	}
	function search_non_l_disbursement()
	{
		if($_POST['id_card_search']!="")
		{
			$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			return $this->finance_disburse_m->check_id_card_l1($_POST['id_card_search']);
		}
		if($_POST['name_search']!="")
		{
			$split_name=explode(" ",$_POST['name_search']);
			if (count($split_name)>2)
			{
				$split_name[1]=$split_name[1]." ".$split_name[2];
				$this->load->model('Finance_disburse_model', 'finance_disburse_m');
				return $this->finance_disburse_m->search_name_in_person1($split_name[0],$split_name[1]);
			}
			else if (count($split_name)==1)
			{
				$this->load->model('Finance_disburse_model', 'finance_disburse_m');
				return $this->finance_disburse_m->search_name1_in_person1($_POST['name_search']);
			}
			else
			{
				$this->load->model('Finance_disburse_model', 'finance_disburse_m');
				return $this->finance_disburse_m->search_name_in_person1($split_name[0],$split_name[1]);
			}
		}
	}
function search_name_in_person1($name,$lname)
{
	$check_state=array('person_fname'=>$name,'person_lname'=>$lname);
	
	$this->db->where($check_state);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			return $this->finance_disburse_m->check_id_card_l1($data_person->person_thaiid);
		}
	}
	else
	{
		return "no data";
	}
	
}
function search_name1_in_person1($name)
{
	$this->db->where('person_fname',$name);
	$this->db->or_where('person_lname',$name);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			return $this->finance_disburse_m->check_id_card_l1($data_person->person_thaiid);
		}
	}
	else
	{
		return "no data";
	}
}
function check_id_card_l1($id_card)
{
	$data_loan_disbursement="";
	$this->db->where('person_thaiid',$id_card);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0) {
	$data_person=$query_data_person->result();
	$this->db->where('person_id',$data_person[0]->person_id);
	$query_data_loan=$this->db->get('loan');
	if($query_data_loan->num_rows()>0)
	{
		foreach($query_data_loan->result() as $data_loan)
		{
			$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no'=>null);
			$this->db->where($check_data);
			$query_data_loan_contract=$this->db->get('loan_contract');
			if($query_data_loan_contract->num_rows()>0)
			{
				foreach($query_data_loan_contract->result() as $data_loan_contract)
				{
					$this->db->where('title_id',$data_person[0]->title_id);
					$query_title_name=$this->db->get('config_title');
					$data_title_name_loan=$query_title_name->result();
					//-----------------------------------------

					$this->db->where('district_id',$data_person[0]->person_addr_card_district_id);
								$query_data_district=$this->db->get('master_district');
								$data_district=$query_data_district->result();
								$this->db->where('amphur_id',$data_person[0]->person_addr_card_amphur_id);
								$query_data_amphur=$this->db->get('master_amphur');
								$data_amphur=$query_data_amphur->result();
								$this->db->where('province_id',$data_person[0]->person_addr_card_province_id);
								$query_data_province=$this->db->get('master_province');
								$data_province=$query_data_province->result();
								$address="";
								if($data_person[0]->person_addr_card_no!=null){
									$address=$address."บ้านเลขที่ ".$data_person[0]->person_addr_card_no." ";
								}
								if($data_person[0]->person_addr_card_moo!=null){
									$address=$address."หมู่ที่ ".$data_person[0]->person_addr_card_moo." ";
								}
								if($data_person[0]->person_addr_card_road!=null){
									$address=$address."ถนน ".$data_person[0]->person_addr_card_road." ";
								}



					//----------------------------------------------------
							$data_loan_disbursement=$data_loan_disbursement.$data_loan_contract->loan_contract_id.",".$data_title_name_loan[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_contract->loan_contract_amount.",".$address." ตำบล".$data_district[0]->district_name." อำเภอ".$data_amphur[0]->amphur_name." จังหวัด".$data_province[0]->province_name."\\";	
				}
			}
		}
		return $data_loan_disbursement;
	}
	}
	else
	{
		return "no data";
	}
}



	function search_all_l_disbursement()
{
		$data_loan_disbursement="";
		if($_POST['id_card_search']!="")
		{
			$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			$data_loan_disbursement=$data_loan_disbursement.$this->finance_disburse_m->check_id_card_l($_POST['id_card_search']);
			
		}
		if($_POST['name_search']!="")
		{
			$split_name=explode(" ",$_POST['name_search']);
			if (count($split_name)>2)
			{
				$split_name[1]=$split_name[1]." ".$split_name[2];
				$this->load->model('Finance_disburse_model', 'finance_disburse_m');
				$data_loan_disbursement=$data_loan_disbursement.$this->finance_disburse_m->search_name_in_person($split_name[0],$split_name[1]);
				
			}
			else if (count($split_name)==1)
			{
				$this->load->model('Finance_disburse_model', 'finance_disburse_m');
				$data_loan_disbursement=$data_loan_disbursement.$this->finance_disburse_m->search_name1_in_person($_POST['name_search']);
				
			}
			else
			{
				$this->load->model('Finance_disburse_model', 'finance_disburse_m');
				$data_loan_disbursement=$data_loan_disbursement.$this->finance_disburse_m->search_name_in_person($split_name[0],$split_name[1]);
				
			}
			
		}
		if($_POST['contact_id']!="")
		{
			$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			$data_loan_disbursement=$data_loan_disbursement.$this->finance_disburse_m->search_contract_no($_POST['contact_id']);
			
		}
		return $data_loan_disbursement;
}
function search_contract_no($loan_contract_no)
{
	$data_loan_disbursement="";
	$this->db->where('loan_contract_no',$loan_contract_no);
	$query_data_loan_contract=$this->db->get('loan_contract');
	if($query_data_loan_contract->num_rows()>0)
	{
		$data_loan_contract=$query_data_loan_contract->result();
		$this->db->where('loan_contract_id',$data_loan_contract[0]->loan_contract_id);
		$query_data_loan_disbursements=$this->db->get('loan_disbursements');
		if($query_data_loan_disbursements->num_rows()>0)
		{
			$data_loan_disbursements=$query_data_loan_disbursements->result();
			$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
			$query_data_loan=$this->db->get('loan');
			$data_loan=$query_data_loan->result();
			$this->db->where('person_id',$data_loan[0]->person_id);
			$query_data_person=$this->db->get('person');
			$data_person=$query_data_person->result();
			$this->db->where('title_id',$data_person[0]->title_id);
			$query_title_name=$this->db->get('config_title');
			$data_title_name_loan=$query_title_name->result();
			$this->db->where('employee_id',$data_loan_disbursements[0]->person_payable_1);
			$query_data_payable1=$this->db->get('employee');
			$data_payable1=$query_data_payable1->result();
			$this->db->where('title_id',$data_payable1[0]->title_id);
			$query_title_name_payable1=$this->db->get('config_title');
			$title_name_payable1=$query_title_name_payable1->result();
			$this->db->where('employee_id',$data_loan_disbursements[0]->person_payable_2);
			$query_data_payable2=$this->db->get('employee');
			$data_payable2=$query_data_payable2->result();
			$this->db->where('title_id',$data_payable2[0]->title_id);
			$query_title_name_payable2=$this->db->get('config_title');
			$title_name_payable2=$query_title_name_payable2->result();
								$this->db->where('district_id',$data_person[0]->person_addr_card_district_id);
								$query_data_district=$this->db->get('master_district');
								$data_district=$query_data_district->result();
								$this->db->where('amphur_id',$data_person[0]->person_addr_card_amphur_id);
								$query_data_amphur=$this->db->get('master_amphur');
								$data_amphur=$query_data_amphur->result();
								$this->db->where('province_id',$data_person[0]->person_addr_card_province_id);
								$query_data_province=$this->db->get('master_province');
								$data_province=$query_data_province->result();
								$address="";
								if($data_person[0]->person_addr_card_no!=null){
									$address=$address."บ้านเลขที่ ".$data_person[0]->person_addr_card_no." ";
								}
								if($data_person[0]->person_addr_card_moo!=null){
									$address=$address."หมู่ที่ ".$data_person[0]->person_addr_card_moo." ";
								}
								if($data_person[0]->person_addr_card_road!=null){
									$address=$address."ถ. ".$data_person[0]->person_addr_card_road." ";
								}
			return $data_loan_disbursements[0]->id_disbursements.",".$data_loan_contract[0]->loan_contract_no.",".$data_title_name_loan[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_disbursements[0]->com_code.",".$data_loan_disbursements[0]->id_check.",".$data_loan_disbursements[0]->date_transfer.",".$title_name_payable1[0]->title_name." ".$data_payable1[0]->employee_fname." ".$data_payable1[0]->employee_lname.",".$title_name_payable2[0]->title_name." ".$data_payable2[0]->employee_fname." ".$data_payable2[0]->employee_lname.",".$data_loan_contract[0]->loan_contract_amount.",".$address." ต.".$data_district[0]->district_name." อ.".$data_amphur[0]->amphur_name." จ.".$data_province[0]->province_name.",".$data_loan_disbursements[0]->reference."\\";
		}
		else
		{
			return "no data";
		}
	}
	else
	{
		return "no data";
	}

}
function search_name_in_person($name,$lname)
{
	$check_state=array('person_fname'=>$name,'person_lname'=>$lname);
	$this->db->where($check_state);
	$query_data_person=$this->db->get('person');
	$data_person1="";
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			//$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			//$data_person=$data_person.$this->finance_disburse_m->check_id_card_l($data_person->person_thaiid);
			$data_person1=$data_person1.$data_person->person_thaiid.",";
		}
		//-----------------------------
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person1);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		
		//-----------------------
		$this->load->model('Finance_disburse_model', 'finance_disburse_m');
		return $this->finance_disburse_m->check_id_card_l($data_person1);
	}
	else
	{
		return "no data";
	}
	
}
function search_name1_in_person($name)
{
	$this->db->where('person_fname',$name);
	$this->db->or_where('person_lname',$name);
	$this->db->select('person_thaiid');
	$query_data_person=$this->db->get('person');
	$data_person1="";
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			//$this->load->model('Finance_disburse_model', 'finance_disburse_m');
			//$data_person=$data_person.$this->finance_disburse_m->check_id_card_l($data_person->person_thaiid);
			$data_person1=$data_person1.$data_person->person_thaiid.",";
		}
		//-----------------------------
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person1);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		
		//-----------------------
		$this->load->model('Finance_disburse_model', 'finance_disburse_m');
		return $this->finance_disburse_m->check_id_card_l($data_person1);
		
	}
	else
	{
		return "no data";
	}
}
function check_id_card_l($id_card)
{
	$split_id_card=explode(",",$id_card);
	if(count($split_id_card)>0)
	{
		$data_loan_disbursement="";
		for($i=0;$i<count($split_id_card);$i++)
		{
	
			$this->db->where('person_thaiid',$split_id_card[$i]);
			$query_data_person=$this->db->get('person');
			if($query_data_person->num_rows()>0) {
				$data_person=$query_data_person->result();
				$this->db->where('person_id',$data_person[0]->person_id);
				$query_data_loan=$this->db->get('loan');
				if($query_data_loan->num_rows()>0)
				{
					foreach($query_data_loan->result() as $data_loan)
					{
						$this->db->where('loan_id',$data_loan->loan_id);
						$query_data_loan_contract=$this->db->get('loan_contract');
						if($query_data_loan_contract->num_rows()>0)
						{
							foreach($query_data_loan_contract->result() as $data_loan_contract)
							{
								$check_statement=array('loan_id'=>$data_loan->loan_id,'loan_contract_id'=>$data_loan_contract->loan_contract_id);
								$this->db->where($check_statement);
								$query_data_loan_disbursements=$this->db->get('loan_disbursements');
								if ($query_data_loan_disbursements->num_rows()>0)
								{
									foreach($query_data_loan_disbursements->result() as $data_loan_disbursements)
									{
										$this->db->where('title_id',$data_person[0]->title_id);
										$query_title_name=$this->db->get('config_title');
										$data_title_name_loan=$query_title_name->result();
										$this->db->where('employee_id',$data_loan_disbursements->person_payable_1);
										$query_data_payable1=$this->db->get('employee');
										$data_payable1=$query_data_payable1->result();
										$this->db->where('title_id',$data_payable1[0]->title_id);
										$query_title_name_payable1=$this->db->get('config_title');
										$title_name_payable1=$query_title_name_payable1->result();
										$this->db->where('employee_id',$data_loan_disbursements->person_payable_2);
										$query_data_payable2=$this->db->get('employee');
										$data_payable2=$query_data_payable2->result();
										$this->db->where('title_id',$data_payable2[0]->title_id);
										$query_title_name_payable2=$this->db->get('config_title');
										$title_name_payable2=$query_title_name_payable2->result();
										$this->db->where('district_id',$data_person[0]->person_addr_card_district_id);
										$query_data_district=$this->db->get('master_district');
										$data_district=$query_data_district->result();
										$this->db->where('amphur_id',$data_person[0]->person_addr_card_amphur_id);
										$query_data_amphur=$this->db->get('master_amphur');
										$data_amphur=$query_data_amphur->result();
										$this->db->where('province_id',$data_person[0]->person_addr_card_province_id);
										$query_data_province=$this->db->get('master_province');
										$data_province=$query_data_province->result();
										$address="";
										if($data_person[0]->person_addr_card_no!=null){
											$address=$address."บ้านเลขที่ ".$data_person[0]->person_addr_card_no." ";
										}
										if($data_person[0]->person_addr_card_moo!=null){
											$address=$address."หมู่ที่ ".$data_person[0]->person_addr_card_moo." ";
										}
										if($data_person[0]->person_addr_card_road!=null){
											$address=$address."ถ. ".$data_person[0]->person_addr_card_road." ";
										}
										$data_loan_disbursement=$data_loan_disbursement.$data_loan_disbursements->id_disbursements.",".$data_loan_contract->loan_contract_no.",".$data_title_name_loan[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_disbursements->com_code.",".$data_loan_disbursements->id_check.",".$data_loan_disbursements->date_transfer.",".$title_name_payable1[0]->title_name." ".$data_payable1[0]->employee_fname." ".$data_payable1[0]->employee_lname.",".$title_name_payable2[0]->title_name." ".$data_payable2[0]->employee_fname." ".$data_payable2[0]->employee_lname.",".$data_loan_contract->loan_contract_amount.",".$address." ต.".$data_district[0]->district_name." อ.".$data_amphur[0]->amphur_name." จ.".$data_province[0]->province_name.",".$data_loan_disbursements->reference."\\";
								}
							}
						}	
					}		
				}
			}	
			return $data_loan_disbursement;
		}
		}
	}
	else
	{
		return "no data";
	}
}
function check_id_card()
	{
		$this->db->where('person_thaiid',$_POST['id_card']);
		$query_person=$this->db->get('person');
		$data_loan_show="";
		if ($query_person->num_rows()>0)
		{
			foreach($query_person->result() as $data_person)
			{
				$data_loan_contract1="";
				$this->db->where('person_id',$data_person->person_id);
				$query_loan=$this->db->get('loan');
				
				if ($query_loan->num_rows()>0)
				{
					foreach($query_loan->result() as $data_loan)
					{
						$this->db->where('loan_id',$data_loan->loan_id);
						$query_loan_contract=$this->db->get('loan_contract');
						
						if ($query_loan_contract->num_rows()==1)
						{
							foreach($query_loan_contract->result() as $data_loan_contract)
							{
								if ($query_loan_contract->num_rows()==1)
								{
									$this->db->where('loan_contract_id',$data_loan_contract->loan_contract_id);
									$query_loan_disbursement=$this->db->get('loan_disbursements');
									
									if ($query_loan_disbursement->num_rows()>0) //กรณีมีข้อมูลการเบิกจ่ายแล้ว
									{
										foreach ($query_loan_disbursement->result() as $data_disbursement)
										{
											$this->db->where('title_id',$data_person->title_id);
											$query_title=$this->db->get('config_title');
											
											foreach($query_title->result() as $data_title)
											{
											
											$data_loan_contract1="1".",".$data_loan_contract->loan_contract_no.",".$data_title->title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_contract->loan_contract_amount.",".$data_disbursement->id_check.",".$data_disbursement->com_code.",".$data_disbursement->date_transfer.",".$data_disbursement->person_payable_1.",".$data_disbursement->person_payable_2;
											}
										}
									}
									else //กรณียังไม่มีการเบิกจ่าย
									{
										$this->db->where('title_id',$data_person->title_id);
											$query_title=$this->db->get('config_title');
										foreach($query_title->result() as $data_title)
											{
											$data_loan_contract1="0".",".$data_title->title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_contract->loan_contract_amount.",".$data_loan->loan_id;
											}
									}
								}
								
							}	
						}
						elseif($query_loan_contract->num_rows()>=2)
						{
							foreach($query_loan_contract->result() as $data_loan_contract)
							{
								$data_loan_contract1=$data_loan_contract1.$data_loan_contract->loan_contract_id.",".$data_loan_contract->loan_contract_no."\\";
							}
								$data_loan_contract1="more"."\\".$data_loan_contract1.$data_loan->loan_id;
						}
						elseif($query_loan_contract->num_rows()==0)
						{
							$data_loan_contract1=$data_loan_contract1."ไม่มีข้อมูลการอนุมัติการกู้ยืมเงิน";
						}
					}
				}
				else
				{
					$data_loan_contract1=$data_loan_contract1."ไม่มีข้อมูลการอนุมัติการกู้ยืมเงิน";
				}
			}
			echo $data_loan_contract1;

		}
		else
		{
			echo "ไม่มีหมายเลขบัตรประชาชนนี้";
		}
	}
	function load_person_payable2()
	{
		$query_person_payable1=$this->db->get('employee');
		$data_person_payable="";
		if ($query_person_payable1->num_rows() >0)
		{
			foreach ($query_person_payable1->result() as $data_person_payable1)
			{
				$this->db->where('title_id',$data_person_payable1->title_id);
				$query_title=$this->db->get('config_title');
				if ($query_title->num_rows()>0)
				{
					foreach($query_title->result() as $data_title)
					{
						$data_person_payable=$data_person_payable.$data_person_payable1->employee_id.",".$data_title->title_name." ".$data_person_payable1->employee_fname." ".$data_person_payable1->employee_lname."\\";
					}
				}
			}
			return $data_person_payable;
		}

	}
	function load_bank_active()
	{
		$data_bank="";
		$this->db->where("status_com_code","1");
		$query=$this->db->get('bank_loan_payment');
		
		if($query->num_rows()>0)
		{
			
			foreach($query->result() as $bank_active)
			{
				$this->db->where('bank_id',$bank_active->bank_id);
				$query_bank_id=$this->db->get('config_bank');
				if($query_bank_id->num_rows()>0)
				{
					foreach($query_bank_id->result() as $bank_id)
					{
						$data_bank=$data_bank.$bank_active->com_code.",".$bank_active->account_name.",".$bank_id->bank_name."\\";
					}
				}
			}
			return $data_bank;
		}
		else
		{
			return "ไม่มี";
		}
	}
	function check_loan_name()
{
	$data_person1="";
	$query_person=$this->db->query("select * from person where title_id=$_POST[title] AND person_fname='$_POST[fname]' AND person_lname='$_POST[lname]'");
	if ($query_person->num_rows()>0)
	{
		foreach ($query_person->result() as $data_person)
		{
			$this->db->where('title_id',$data_person->title_id);
			$query_title=$this->db->get('config_title');
			foreach ($query_title->result() as $data_title)
			{
				$data_person1=$data_person1.$data_person->person_thaiid.",".$data_title->title_name." ".$data_person->person_fname." ".$data_person->person_lname."\\";
			}
		}
		echo $data_person1.$query_person->num_rows()."\\";
	}
	else
	{
		echo "ไม่พบรายชื่อในการค้นหา";
	}
}
function load_loan_person_disbursements()
	{

		$this->db->where('loan_contract_id',$_POST['loan_contact_id']);
		$query_loan_person_disbursements=$this->db->get('loan_disbursements');
		if($query_loan_person_disbursements->num_rows()>0)
		{
			foreach($query_loan_person_disbursements->result() as $data_loan_person_disbursements)
			{
		$My_Multiple_Statements_Array = array('loan_id' => $data_loan_person_disbursements->loan_id, 'loan_contract_id' => $_POST['loan_contact_id']);
		//$this->db->where('loan_id',$data_loan_person_disbursements->loan_id);
		$this->db->where($My_Multiple_Statements_Array);
		$query_load_more_loan_contract=$this->db->get('loan_contract');
		if ($query_load_more_loan_contract->num_rows()>0)
		{
			foreach($query_load_more_loan_contract->result() as $data_loan_contract)
			{
				$this->db->where('loan_id',$data_loan_contract->loan_id);
				$query_data_loan=$this->db->get('loan');
				if ($query_data_loan->num_rows()>0)
				{
					foreach ($query_data_loan->result() as $data_loan)
					{
						$this->db->where('person_id',$data_loan->person_id);
						$query_data_person=$this->db->get('person');
						if ($query_data_person->num_rows()>0)
						{
							foreach($query_data_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_data_title=$this->db->get('config_title');
								if ($query_data_title->num_rows()>0)
								{
									foreach($query_data_title->result() as $data_title)
									{
										echo $data_title->title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_contract->loan_contract_amount.",".$data_loan_person_disbursements->com_code.",".$data_loan_person_disbursements->id_check.",".$data_loan_person_disbursements->date_transfer.",".$data_loan_person_disbursements->person_payable_1.",",$data_loan_person_disbursements->person_payable_2;
									}
								}
							}
						}
					}
				}
			}
		}
				
			}
		}
		else
		{
			echo "no data";
		}
	}
	function load_more_loan_contact()
	{
		$My_Multiple_Statements_Array = array('loan_id' => $_POST['loan_id'], 'loan_contract_no' => null);
		$this->db->where($My_Multiple_Statements_Array);
		$query_load_more_loan_contact=$this->db->get('loan_contract');
		if ($query_load_more_loan_contact->num_rows()>0)
		{
			foreach($query_load_more_loan_contact->result() as $data_loan_contact)
			{
				$this->db->where('loan_id',$_POST['loan_id']);
				$query_data_loan=$this->db->get('loan');
				if ($query_data_loan->num_rows()>0)
				{
					foreach ($query_data_loan->result() as $data_loan)
					{
						$this->db->where('person_id',$data_loan->person_id);
						$query_data_person=$this->db->get('person');
						if ($query_data_person->num_rows()>0)
						{
							foreach($query_data_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_data_title=$this->db->get('config_title');
								if ($query_data_title->num_rows()>0)
								{
									foreach($query_data_title->result() as $data_title)
									{
										echo $data_title->title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_contact->loan_contract_amount;
									}
								}
							}
						}
					}
				}
			}
		}
		
	}
	function count_loan_contract_no($id)
	{
		
		$this->db->where('loan_contract_id',$id);
		$query_data_loan_id=$this->db->get('loan_contract');
		$data_loan_id=$query_data_loan_id->result();
		$this->db->where('loan_id',$data_loan_id[0]->loan_id);
		$query_data_loan_type_id=$this->db->get('loan_type');
		$data_loan_type_id=$query_data_loan_type_id->result();
		$data_loan_type_name="";
		if($data_loan_type_id[0]->loan_objective_id==1)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="ถจ";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="ถฝ";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==2)
		{
			$data_loan_type_name="ชก";
		}
		else if($data_loan_type_id[0]->loan_objective_id==3)
		{
			$data_loan_type_name="ชพ";
		}
		else if($data_loan_type_id[0]->loan_objective_id==4)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="ชฝ";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="ชท";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==5)
		{
			$data_loan_type_name="ปก";
		}

		if(intval(date("m"))>=10)
		{
			$year=intval(date("Y"))+544;
		}
		else
		{
			$year=intval(date("Y"))+543;
		}
		$query_data_count_contract_no=$this->db->query("select loan_contract_no from loan_contract where loan_contract_no like '%".$data_loan_type_name."%/".$year."' order by loan_contract_no desc limit 1");
		if($query_data_count_contract_no->num_rows()>0)
		{
			$data_count_contract_no=$query_data_count_contract_no->result();

			$split_data=explode("/",$data_count_contract_no[0]->loan_contract_no);
			$rest = iconv_substr($split_data[0], 0, 2, 'utf-8');
			$rest2=iconv_substr($split_data[0], 2,8);  // abcdef
			$data_loan_contract_no=$rest.(intval($rest2)+1)."/".$year;
			
		}
		else
		{
			$data_loan_contract_no= $data_loan_type_name."1/".$year;
			
		}
		return $data_loan_contract_no;
		
		
	}
	function save_data_finance()
	{
		$this->db->where('loan_contract_id',$_POST['loan_contract_id']);
		$query_data_loan_id=$this->db->get('loan_contract');
		$data_loan_id=$query_data_loan_id->result();
		$this->db->where('loan_id',$data_loan_id[0]->loan_id);
		$query_data_loan_type_id=$this->db->get('loan_type');
		$data_loan_type_id=$query_data_loan_type_id->result();
		
		$data_loan_type_name="";
		if($data_loan_type_id[0]->loan_objective_id==1)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="ถจ";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="ถฝ";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==2)
		{
			$data_loan_type_name="ชก";
		}
		else if($data_loan_type_id[0]->loan_objective_id==3)
		{
			$data_loan_type_name="ชพ";
		}
		else if($data_loan_type_id[0]->loan_objective_id==4)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="ชฝ";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="ชท";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==5)
		{
			$data_loan_type_name="ปก";
		}

		if(intval(date("m"))>=10)
		{
			$year=intval(date("Y"))+544;
		}
		else
		{
			$year=intval(date("Y"))+543;
		}
		$query_data_count_contract_no=$this->db->query("select loan_contract_no from loan_contract where loan_contract_no like '%".$data_loan_type_name."%/".$year."' order by loan_contract_no desc limit 1");
		
		if($query_data_count_contract_no->num_rows()>0)
		{
			$data_count_contract_no=$query_data_count_contract_no->result();

			$split_data=explode("/",$data_count_contract_no[0]->loan_contract_no);
			$rest = iconv_substr($split_data[0], 0, 2, 'utf-8');
			$rest2=iconv_substr($split_data[0], 2,8);  // abcdef
			$data_loan_contract_no=$rest.(intval($rest2)+1)."/".$year;
		}
		else
		{
			$data_loan_contract_no= $data_loan_type_name."1/".$year;
			
		}
			$this->db->where('loan_contract_id',$_POST['loan_contract_id']);
			$query_data_loan_contract=$this->db->get('loan_contract');
			
			if($query_data_loan_contract->num_rows()>0)
			{
				$data_loan_contract=$query_data_loan_contract->result();
				$this->db->order_by('id_disbursements','asc');
				$this->db->limit(0);
				$query = $this->db->get('loan_disbursements');
				$id_new;
				if ($query->num_rows()>0)
				{
					foreach($query->result() as $id_dis)
					{
						$id_new=($id_dis->id_disbursements)+1;
					}
				}
				else
				{
					$id_new=1;
				}
				$split_date=explode("/",$_POST['date_tranfer']);
				$split_date2=(intval($split_date[2])-543)."-".$split_date[1]."-".$split_date[0];
				if($_POST['ref']!="")
				{
					
					$data_insert=array(
					'id_disbursements'=>$id_new,
					'id_check'=>$_POST['id_check'],
					'loan_id'=>$data_loan_contract[0]->loan_id,
					'com_code'=>$_POST['bank'],
					'loan_contract_id'=>$_POST['loan_contract_id'],
					'date_transfer'=>$split_date2,
					'person_payable_1'=>$_POST['person_payable1'],
					'person_payable_2'=>$_POST['person_payable2'],
					'createddate'=>date("Y-m-d"),
					'updatedate'=>date("Y-m-d"),
					'reference'=>$_POST['ref']
					);
				}
				else
				{
					$data_insert=array(
					'id_disbursements'=>$id_new,
					'id_check'=>$_POST['id_check'],
					'loan_id'=>$data_loan_contract[0]->loan_id,
					'com_code'=>$_POST['bank'],
					'loan_contract_id'=>$_POST['loan_contract_id'],
					'date_transfer'=>$split_date2,
					'person_payable_1'=>$_POST['person_payable1'],
					'person_payable_2'=>$_POST['person_payable2'],
					'createddate'=>date("Y-m-d"),
					'updatedate'=>date("Y-m-d")
					);
				}
					
				$data_insert_loan_balance=array(
				'loan_contract_id'=>$_POST['loan_contract_id'],
				'loan_amount'=>$data_loan_contract[0]->loan_contract_amount,
				'accrued_interest'=>0,
				'status_loan'=>'N',
				'createddate'=>date("Y-m-d"),
				'updatedate'=>date("Y-m-d")
				);
				
				
				$data_insert_loan_contract=array('loan_contract_no'=>$data_loan_contract_no,'updatedate'=>date("Y-m-d"),'loan_contract_date'=>$split_date2);
				
				if ($this->db->insert('loan_disbursements',$data_insert))
				{
					$My_Multiple_Statements_Array = array('loan_id' => $data_loan_contract[0]->loan_id,'loan_contract_id'=>$_POST['loan_contract_id'], 'loan_contract_no' => null);
					
					$this->db->where($My_Multiple_Statements_Array);
					if ($this->db->update('loan_contract', $data_insert_loan_contract))
					{
						if($this->db->insert('loan_balance',$data_insert_loan_balance))
						{
							
							$data_update_payment_schedule=array('status'=>'N','updatedate'=>date("Y-m-d"));
							$My_Multiple_payment_sechedule_check = array('loan_contract_id'=> $_POST['loan_contract_id']);
							$this->db->where($My_Multiple_payment_sechedule_check);
							if($this->db->update('payment_schedule', $data_update_payment_schedule))
							{
								return "success";	
							}
							else
							{
								return "ไม่สามาถบันทึกได้";
							}				
						}
						else
						{
							return "ไม่สามาถบันทึกได้";
						}	
					}
					else
					{
						return "ไม่สามาถบันทึกได้";
					}	
				}
			}
			else
			{
				return "ไม่สามาถบันทึกได้";
			}
			

		
	}
	
function edit_f_disburse()
{
				$date_transfer_split=explode("/",$_POST['date_tranfer']);
				$data_transfer_split1= intval($date_transfer_split[2]-543)."-".$date_transfer_split[1]."-".$date_transfer_split[0];
				if($_POST['ref']!="")
				{
					$data_update_loan_disbursment=array('id_check'=>$_POST['id_check'],'date_transfer'=>$data_transfer_split1,'person_payable_1'=>$_POST['person_payable1'],'person_payable_2'=>$_POST['person_payable2'],'updatedate'=>date("Y-m-d"),'reference'=>$_POST['ref']);
				}
				else
				{
					$data_update_loan_disbursment=array('id_check'=>$_POST['id_check'],'date_transfer'=>$data_transfer_split1,'person_payable_1'=>$_POST['person_payable1'],'person_payable_2'=>$_POST['person_payable2'],'updatedate'=>date("Y-m-d"));
				}
				$this->db->where('loan_contract_id',$_POST['loan_contract_id']);
				if($this->db->update('loan_disbursements',$data_update_loan_disbursment))
				{
					return "success";
				}
				else
				{
					return "ไม่สามารถบันทึกข้อมูลได้";
				}
}
function load_non_loan_disbursements()
{
	$data_array_loan_disbursements="";
	$this->db->where('loan_contract_no',null);
	$this->db->order_by('loan_contract_no','desc');
	$query_non_loan_dis=$this->db->get('loan_contract');
	if($query_non_loan_dis->num_rows()>0)
	{
		foreach($query_non_loan_dis->result() as $data_loan_contract)
		{
			$this->db->where("loan_id",$data_loan_contract->loan_id);
			$query_loan=$this->db->get("loan");
			foreach($query_loan->result() as $data_loan)
			{
				$this->db->where("person_id",$data_loan->person_id);
				$query_person=$this->db->get("person");
				foreach($query_person->result() as $data_person)
				{
					$this->db->where("title_id",$data_person->title_id);
					$query_title=$this->db->get("config_title");
					foreach($query_title->result() as $data_title)
					{
						$title_person=$data_title->title_name;
					}
					//-------------------------------
					$this->db->where('district_id',$data_person->person_addr_card_district_id);
								$query_data_district=$this->db->get('master_district');
								$data_district=$query_data_district->result();
								$this->db->where('amphur_id',$data_person->person_addr_card_amphur_id);
								$query_data_amphur=$this->db->get('master_amphur');
								$data_amphur=$query_data_amphur->result();
								$this->db->where('province_id',$data_person->person_addr_card_province_id);
								$query_data_province=$this->db->get('master_province');
								$data_province=$query_data_province->result();
								$address="";
								if($data_person->person_addr_card_no!=null){
									$address=$address."บ้านเลขที่ ".$data_person->person_addr_card_no." ";
								}
								if($data_person->person_addr_card_moo!=null){
									$address=$address."หมู่ที่ ".$data_person->person_addr_card_moo." ";
								}
								if($data_person->person_addr_card_road!=null){
									$address=$address."ถนน ".$data_person->person_addr_card_road." ";
								}
						
					//-------------------------------------------------
						
							$data_array_loan_disbursements=$data_array_loan_disbursements.$data_loan_contract->loan_contract_id.",".$title_person." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_contract->loan_contract_amount.",".$address." ตำบล".$data_district[0]->district_name." อำเภอ".$data_amphur[0]->amphur_name." จังหวัด".$data_province[0]->province_name."\\";			
				}
			}
		}
		return $data_array_loan_disbursements;
	}
	else
	{
		return "no data";
	}
}
function load_data_loan_non_disbursements($id)
{
	$data_array_loan_disbursements="";
	$this->db->where('loan_contract_id',$id);
	$query_non_loan_dis=$this->db->get('loan_contract');
	if($query_non_loan_dis->num_rows()>0)
	{
		foreach($query_non_loan_dis->result() as $data_loan_contract)
		{
			$this->db->where("loan_id",$data_loan_contract->loan_id);
			$query_loan=$this->db->get("loan");
			foreach($query_loan->result() as $data_loan)
			{
				$this->db->where("person_id",$data_loan->person_id);
				$query_person=$this->db->get("person");
				foreach($query_person->result() as $data_person)
				{
					$this->db->where("title_id",$data_person->title_id);
					$query_title=$this->db->get("config_title");
					foreach($query_title->result() as $data_title)
					{
						$title_person=$data_title->title_name;
					}
							$data_array_loan_disbursements=$data_array_loan_disbursements.$data_loan_contract->loan_contract_id.",".$title_person." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_contract->loan_contract_amount."\\";			
				}
			}
		}
		return $data_array_loan_disbursements;
	}
	else
	{
		return "no data";
	}
}
}

?>