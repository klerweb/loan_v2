<?php
class finance_disburse extends MY_Controller{
	private $title_page = 'ข้อมูลการเบิกจ่ายเงินกู้';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		
		$data_content = array(
				
				'loan_disbursements' => $this->finance_disburse_m->load_loan_disbursements()
		);
		
		$data = array(
				'title' => $this->title_page,
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('loan_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function show_one_person_for_withdraw($id)
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array(
				'การเงิน' =>'#',
				$this->title_page => site_url('finance_disburse')
			);
		$title_page = 'แก้ไขการเบิกจ่ายเงินกู้';
		$data_content = array(
				'loan_one_disbursements' => $this->finance_disburse_m->load_data_loan_non_disbursements($id),
			'bank' => $this->finance_disburse_m->load_bank_active(),
				
				'person_payable1'=>$this->finance_disburse_m->load_person_payable1()
		);
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form_non_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function edit_finance_disburse()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_edit_loan_con_dis=$this->finance_disburse_m->edit_f_disburse();
		echo $data_edit_loan_con_dis;
	}
	public function show_none_withdraw()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('finance_disburse'));
		
		$data_content = array(
				'loan_disbursements' => $this->finance_disburse_m->load_non_loan_disbursements()
		);
		$title_page = 'ข้อมูลการกู้ที่ยังไม่มีการเบิกจ่าย';
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('loan_non_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function show_one_person_loan_disbursements($id)
	{
		
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array(
				'การเงิน' =>'#',
				$this->title_page => site_url('finance_disburse')
			);
		$title_page = 'แก้ไขการเบิกจ่ายเงินกู้';
		$data_content = array(
				'loan_one_disbursements' => $this->finance_disburse_m->loan_one_disbursements($id),
			'bank' => $this->finance_disburse_m->load_bank(),
				'person_payable1'=>$this->finance_disburse_m->load_person_payable1()
		);
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function show_non_person_for_withdraw($id)
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array(
				'การเงิน' =>'#',
				$this->title_page => site_url('finance_disburse')
			);
		$title_page = 'เบิกจ่ายเงินกู้';
		$data_content = array(
				'loan_one_disbursements' => $this->finance_disburse_m->loan_non_disbursements($id),
			'bank' => $this->finance_disburse_m->load_bank_active(),
				'person_payable1'=>$this->finance_disburse_m->load_person_payable1(),
				'count_loan_contract_no'=>$this->finance_disburse_m->count_loan_contract_no($id),
				'loan_c_id'=>$id
		);
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form_non_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}

	public function withdraw()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		$data_content = array(
				'bank' => $this->finance_disburse_m->load_bank(),
				'title_show'=>$this->finance_disburse_m->load_title(),
				'person_payable1'=>$this->finance_disburse_m->load_person_payable1()	
		);
		$data = array(
				'title' => $this->title_page,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_all_loan_disbursement()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		
		$data_content = array(
				
				'loan_disbursements' => $this->finance_disburse_m->search_all_l_disbursement()
		);
		
		$data = array(
				'title' => $this->title_page,
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('loan_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_non_loan_disbursement()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		
		$data_content = array(
				
				'loan_disbursements' => $this->finance_disburse_m->search_non_l_disbursement()
		);
		
		$data = array(
				'title' => $this->title_page,
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('loan_non_withdraw', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_id_card()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		
		$data_id=$this->finance_disburse_m->check_id_card();
		echo $data_id;
	}
	public function show_person_payable_non_active()
	{
		
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_person=$this->finance_disburse_m->load_person_payable2();
		echo $data_person;
	}
	public function check_bank_active()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_bank=$this->finance_disburse_m->load_bank_active();
		echo $data_bank;
	}
	function search_loan_name()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_loan=$this->finance_disburse_m->check_loan_name();
		echo $data_loan;
	}
	public function check_loan_disbursements()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_loan_person_disbursements=$this->finance_disburse_m->load_loan_person_disbursements();
		echo $data_loan_person_disbursements;
	}
	public function check_more_loan_contact()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_show_loan_contact=$this->finance_disburse_m->load_more_loan_contact();
		echo $status_show_loan_contact;
	}
	public function show_person_payable()
	{
		
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_person=$this->finance_disburse_m->load_person_payable1();
		echo $data_person;
	}
	public function delete_finance_disburse()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_delete_disburse=$this->finance_disburse_m->delete_finance();
		echo $status_delete_disburse;
		
	}
	public function save_finance_disburse()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_save_disburse=$this->finance_disburse_m->save_data_finance();
		echo $status_save_disburse;
		
	}
	public function save_finance_disburse_t()
	{
		$this->load->model('finance_disburse_model', 'finance_disburse_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_save_disburse=$this->finance_disburse_m->save_data_finance_t();
		echo $status_save_disburse;
	}

	}


?>