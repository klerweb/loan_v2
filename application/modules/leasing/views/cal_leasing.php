<?php 
	$count = 1;
	$sum = 0;
	$html = '';
	if(empty($_SESSION['leasing_list_cal']))
	{
		$html = '<tr id="tr_1">
				<td align="center">1<input type="hidden" id="txtCalRound_1" name="txtCalRound[]" value="1" /></td>
				<td><input type="text" id="txtCalStart_1" name="txtCalStart[]"  class="form-control" style="width:100px; float:left;" value="1" readonly /> <span style="width:100px; float:left;padding:10px; text-align:center;">ถึง</span> <input type="text" id="txtCalEnd_1" name="txtCalEnd[]"  class="form-control" style="width:100px; float:left;" value="'.$round.'" /></td>
				<td><input type="text" id="txtCalAmount_1" name="txtCalAmount[]"  class="form-control cal_amount" style="width:200px;" value="0" /></td>
				<td align="center">
					<button type="button" id="plus_1" class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="เพิ่ม" data-original-title="เพิ่ม" onclick="add_tr(1)"><span class="fa fa-plus"></span></button>
				</td>
			</tr>';
	}
	else
	{
		$count = count($_SESSION['leasing_list_cal']);
		$i_cal = 1;
		foreach ($_SESSION['leasing_list_cal'] as $list_cal)
		{
			$sum += ((intval($list_cal['leasing_cal_end']) - intval($list_cal['leasing_cal_start'])) + 1) * floatval($list_cal['leasing_cal_amount']);
			
			$minus = '';
			if($i_cal!=1) $minus = '<button type="button" id="minus_'.$i_cal.'" class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ลบ" data-original-title="ลบ"  onclick="minus_tr('.$i_cal.')"'.($i_cal>$count? ' style="display:none;"' : '').'><span class="fa fa-minus"></span></button>';
			
			$html .= '<tr id="tr_'.$i_cal.'">
					<td align="center">'.$i_cal.'<input type="hidden" id="txtCalRound_'.$i_cal.'" name="txtCalRound[]" value="'.$i_cal.'" /></td>
					<td><input type="text" id="txtCalStart_'.$i_cal.'" name="txtCalStart[]"  class="form-control" style="width:100px; float:left;" value="'.$list_cal['leasing_cal_start'].'" readonly /> <span style="width:100px; float:left;padding:10px; text-align:center;">ถึง</span> <input type="text" id="txtCalEnd_'.$i_cal.'" name="txtCalEnd[]"  class="form-control" style="width:100px; float:left;" value="'.$list_cal['leasing_cal_end'].'"'.($i_cal>$count? ' readonly' : '').' /></td>
					<td><input type="text" id="txtCalAmount_'.$i_cal.'" name="txtCalAmount[]"  class="form-control cal_amount" style="width:200px;" value="'.$list_cal['leasing_cal_amount'].'" /></td>
					<td align="center">
						<button type="button" id="plus_'.$i_cal.'" class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="เพิ่ม" data-original-title="เพิ่ม" onclick="add_tr('.$i_cal.')"'.($i_cal>$count? ' style="display:none;"' : '').'><span class="fa fa-plus"></span></button>
						'.$minus.'
					</td>
				</tr>';
			
			$i_cal++;
		}
	}
?>
<input type="hidden" id="txtCalTime" name="txtCalTime" value="<?php echo $count; ?>">
<input type="hidden" id="txtCalUse" name="txtCalUse" value="<?php echo $sum; ?>">
<div class="row">
	<div class="col-sm-12 col-md-12">
		<h4 style="float:right;">จำนวนงวด <?php echo $round; ?> งวด คงเหลือ <span id="divCalUse"><?php echo number_format($amount, 2); ?></span></h4>
	</div>
</div>
<div class="table-responsive">
	<table id="tblCal" class="table table-hover mb30">
		<thead>
			<tr>
				<th>#</th>
				<th>งวดที่</th>
				<th>เงินต้น</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php  echo $html; ?>
		</tbody>
	</table>
</div><!-- table-responsive -->
<script>
	function add_tr(time)
	{
		var end = $("#txtCalEnd_"+time).val();
		var amount = $("#txtCalAmount_"+time).val();
		
		if(end=="" || amount=="")
		{
			alert("กรุณากรอกงวดหรือเงินต้น");
		}
		else if(!(/^\d+$/.test(end)) || !(/^-?\d*(\.\d+)?$/.test(amount)))
		{
			alert("กรุณากรอกงวดให้ถูกต้องหรือเงินต้น");
		}
		else
		{
			end = parseInt(end);
			start = parseInt($("#txtCalStart_"+time).val());

			var nexttime = time + 1;
			
			if(end==<?php echo $round; ?>)
			{
				alert("จำนวนงวดครบแล้ว");
			}
			else if(start>end)
			{
				alert("งวดเริ่มต้นมากกว่างวดสิ้นสุด");
			}
			else
			{
				var amount = parseFloat(amount);
				var round = (end - start) + 1;

				var use = cal_time();
				var remain = <?php echo $amount; ?> - use;

				if(use<<?php echo $amount; ?>)
				{
					$("#plus_"+time).hide();
					$("#minus_"+time).hide();
					$("#txtCalEnd_"+time).prop("readonly", true);
					$("#tblCal > tbody:last-child").append("<tr id=\"tr_"+nexttime+"\"><td align=\"center\">"+nexttime+"<input type=\"hidden\" id=\"txtCalRound_"+nexttime+"\" name=\"txtCalRound[]\" value=\""+nexttime+"\" /></td><td><input type=\"text\" id=\"txtCalStart_"+nexttime+"\" name=\"txtCalStart[]\" class=\"form-control\" style=\"width:100px; float:left;\" value=\""+(end+1)+"\" readonly /> <span style=\"width:100px; float:left;padding:10px; text-align:center;\">ถึง</span> <input type=\"text\" id=\"txtCalEnd_"+nexttime+"\" name=\"txtCalEnd[]\" class=\"form-control\" style=\"width:100px; float:left;\" value=\"\" /></td><td><input type=\"text\" id=\"txtCalAmount_"+nexttime+"\" name=\"txtCalAmount[]\"  class=\"form-control cal_amount\" style=\"width:200px;\" value=\"0\" /></td><td align=\"center\"><button type=\"button\" id=\"plus_"+nexttime+"\" class=\"btn btn-default btn-xs tooltips\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"เพิ่ม\" data-original-title=\"เพิ่ม\" onclick=\"add_tr("+nexttime+")\"><span class=\"fa fa-plus\"></span></button> <button type=\"button\" id=\"minus_"+nexttime+"\" class=\"btn btn-default btn-xs tooltips\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ลบ\" data-original-title=\"ลบ\"  onclick=\"minus_tr("+nexttime+")\"><span class=\"fa fa-minus\"></span></button></td></tr>");

					$("#txtCalTime").val(nexttime);
					$("#txtCalUse").val(use);
					$("#divCalUse").text(addCommas(remain.toFixed(2)));
				}
				else
				{
					alert("เงินต้นเกิน");
				}
			}
		}
	}

	function minus_tr(time)
	{
		var backtime = time - 1;

		$("#tr_"+time).remove();
		
		$("#plus_"+backtime).show();
		$("#minus_"+backtime).show();
		$("#txtCalEnd_"+backtime).prop("readonly", false);

		$("#txtCalTime").val(backtime);

		var use = cal_time();
		var remain = <?php echo $amount; ?> - use;

		$("#txtCalUse").val(use);
		$("#divCalUse").text(addCommas(remain.toFixed(2)));
	}

	function addCommas(nStr)
	{
		nStr += "";
		x = nStr.split(".");
		x1 = x[0];
		x2 = x.length > 1 ? "." + x[1] : "";
		
		var rgx = /(\d+)(\d{3})/;
		
		while (rgx.test(x1))
		{
			x1 = x1.replace(rgx, "$1" + "," + "$2");
		}
		
		return x1 + x2;
	}
</script>