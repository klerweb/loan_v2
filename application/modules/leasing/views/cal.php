<div class="contentpanel">
<div class="row">
	<div class="col-sm-12">
		<br/>
		<?php 
			$html = '';
			$show_first = 0;
            $total_interest=0;
			if(!empty($list_round))
			{ 
				$remain = 0;
				$i_round = 1;
				$i_tab = 1;
				$i_start = 1;
				$c_round = 1;
                
				$count_round = count($list_round);
				
				$html_round = '';
				$html_2 = '';
                $html_2_0 = '';
				$html_2_1 = '';
				$html_2_2 = '';
				$html_2_3 = '';
				$html_2_4 = '';   
				$html_2_9 = '';
            
				foreach ($list_round as $key_round => $round)
				{
 
					if($key_round=='1')
					{

						$data2_2 = 0;
						
						$show_first = $round['principle'] + $round['interest'];
					}
					else
					{
						
						$data2_2 = 0;
					}
					
					$data2_3 = $round['principle'];
					$data2_4 = $round['interest']; 
                    $total_interest +=  $round['interest'];          
						
					$data2_9 = $data2_2 + $data2_3 + $data2_4 ;
					
						
					$remain += 0;
						
					$html_round .= '<th style="text-align:center;">'.$key_round.'</th>';

					$html_2 .= '<td></td>';
                    $html_2_0 .= '<td align="center">'.thai_display_date($round['date']).'</td>';
					$html_2_1 .= '<td align="right">'.number_format($round['remain'], 2).'</td>';
					$html_2_2 .= '<td align="right">'.number_format($data2_2, 2).'</td>';
					$html_2_3 .= '<td align="right">'.number_format($data2_3, 2).'</td>';
					$html_2_4 .= '<td align="right">'.number_format($data2_4, 2).'</td>';
					$html_2_9 .= '<td align="right">'.number_format($data2_9, 2).'</td>';      
					
					if($i_round%5==0 || $i_round==$count_round)
					{
						$style = $i_tab==1? ' in' : '';
						$html .= '<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse_'.$i_tab.'">งวดที่ '.$i_start.' - '.$i_round.'</a>
								</h4>
							</div>
							<div id="collapse_'.$i_tab.'" class="panel-collapse collapse'.$style.'">
								<div class="panel-body">
									<div class="col-sm-12">
										<div class="table-responsive">
											<table class="table table-bordered mb30" style="width:100%;">
												<thead>
													<tr>
														<th rowspan="2" style="text-align:center; vertical-align: middle; width:350px;">รายการ</th>
														<th colspan="'.$c_round.'" style="text-align:center;">งวดที่ (ตั้งแต่งวดที่ '.$i_start.' จนถึงงวดที่ '.$i_round.')</th>
													</tr>
													<tr>
														'.$html_round.'
													</tr>
												</thead>
												<tbody>
													
													<tr><td style="width:350px;">1. เงินสดจ่าย (บาท)</td>'.$html_2.'</tr>
                                                    <tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;กำหนดชำระ</td>'.$html_2_0.'</tr>         
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(1) สินเชื่อ คงเหลือ</td>'.$html_2_1.'</tr>         
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(2) ชำระต้นเงินสินเชื่อ</td>'.$html_2_3.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(3) ชำระดอกเบี้ยของสินเชื่อ</td>'.$html_2_4.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(4) รวมเงินสดจ่าย (2)+(3)</td>'.$html_2_9.'</tr>
													  
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div><!-- panel -->';
						
						$i_tab++;
						$i_start = $i_round + 1;
						$c_round = 1;
						
						$html_round = '';
					 
						$html_2 = '';
                        $html_2_0 = '';
						$html_2_1 = '';
						$html_2_2 = '';
						$html_2_3 = '';
						$html_2_4 = '';   
						$html_2_9 = '';      
					}
					else
					{
						$c_round++;
					}
					
					$i_round++;
				}
	 		} 
	 	?>
	 	<div class="panel-group" id="accordion">
	 		<?php echo $html; ?>
	 	</div><!-- panel-group -->
	</div>
</div> 
<div class="row" style="padding:10px;">
 
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label">ชำระหนี้ได้ปีละ</label>
				<div class="input-group">
					<?php 
						switch ($type)
						{
							case 1: $type = 12;
								break;
							case 6: $type = 2;
								break;
							case 12: $type = 1;
								break;
						}
					?>
					<input type="text" id="txtShowTime"  name="txtShowTime" class="form-control right" value="<?php echo $type; ?>" disabled="disabled" />
					<span class="input-group-addon">ครั้ง</span>
				</div>
			</div><!-- form-group -->
		</div><!-- col-sm-4 -->
		<div class="col-sm-4">
			<div class="form-group">
				<label class="control-label">ในแต่ละปีชำระได้ในเดือน</label>
				<input type="text" id="txtShowMonth"  name="txtShowMonth" class="form-control" value="<?php echo $month; ?>" disabled="disabled" />
			</div><!-- form-group -->
		</div><!-- col-sm-4 -->
	</div><!-- row -->
</div>
<script>
         $('#txtLeasing_pay_amount_interest').val('<?php echo number_format($total_interest,2)?>');     
</script>