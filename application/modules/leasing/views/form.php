
<form id="frmLeasing" name="frmLeasing" action="<?php echo base_url().'leasing/save'; ?>" method="post">
    <input type="hidden" id="txtId" name="txtId" value="<?php echo $id; ?>" />
	<input type="hidden" id="txtOrganization_id" name="txtOrganization_id" value="<?php echo $data['organization']['org_id']; ?>" />
    <input type='hidden' id="delete_land" name="delete_land" value="">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
                    <h4 class="panel-title">หนังสือสัญญาเช่าซื้อที่ดิน  สำหรับองค์กรชุมชน เครื่อข่ายองค์กรชุมชน กลุ่มเกษตรกร หรือ สถาบันเกษตรกร</h4>   
					<h5 class="panel-title"></h5>
				</div>
				<div class="panel-body">
                
                  <div class="row">
                    <div class="col-sm-5 col-sm-offset-7">
                            <div class="form-group">
                                <label class="control-label">สัญญาเช่าซื้อ เลขที่</label>
                                <input type="text" name="txtLeasing_contract_no" id="txtLeasing_contract_no" class="form-control" value="<?php echo @$data['leasing_contract_no']; ?>" maxlength="50" required />
                            </div><!-- form-group -->
                        </div><!-- col-sm-6 -->
                  </div> <!-- row -->
				  
                  <div class="row">                                                                                                                             
						<div class="col-sm-8">
							<div class="form-group">
								<label class="control-label">สัญญานี้ทำ ณ</label>
								<input type="text" name="txtLeasing_location" id="txtLeasing_location" class="form-control" value="<?php echo @$data['leasing_location']; ?>" maxlength="255"  required/>
							</div><!-- form-group -->
						</div><!-- col-sm-6 -->
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">วันที่ <span class="asterisk">*</span></label>
								<div class="input-group">
									<input type="text" name="txtDate" id="txtDate" class="txtDate form-control" value="<?php echo @$data['leasing_date']; ?>" readonly="readonly" required/>
									<span class="iconDate input-group-addon hand" id="iconDate" data-ref="txtDate"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
                               
					</div> <!-- row -->
                    
                   <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                    <label class="control-label">ระหว่างสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.) โดย  <span class="asterisk">*</span></label>
                                    <select name="ddlDirector" id="ddlDirector" data-placeholder="กรุณาเลือก" class="width100p" >
                                        <option value="0">กรุณาเลือก</option>
                                           <?php
                                                foreach ($employee as $val) {
                                                    $emp = '';
                                                    if ($data['leasing_director_id'] == $val['employee_id'])
                                                        $emp = 'selected';
                                                    ?>
                                                    <option value="<?= $val['employee_id']; ?>" <?= $emp; ?>><?= $val['employee_fname'] . ' ' . $val['employee_lname']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                    </select>
                                </div><!-- form-group -->
                            </div>
                        <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">ตำแหน่ง ผู้อำนวยการสถาบันบริหารจัดการธนาคารที่ดิน </label>     
                                </div>
                        </div>  
                   </div> <!-- row -->
                   
                   <div class="row">
                       <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label">หรือ นาย/นาง/นางสาว<span class="asterisk">*</span></label>
                                    <select name="ddlAttorney" id="ddlAttorney" data-placeholder="กรุณาเลือก" class="width100p">
                                        <option value="0">กรุณาเลือก</option>
                                             <?php
                                                foreach ($employee as $val) {
                                                    $emp = '';
                                                    if ($data['leasing_attorney_id'] == $val['employee_id'])
                                                        $emp = 'selected';
                                                    ?>
                                                    <option value="<?= $val['employee_id']; ?>" <?= $emp; ?>><?= $val['employee_fname'] . ' ' . $val['employee_lname']; ?></option>
                                                    <?php
                                                }
                                                ?>
                                    </select>
                                </div><!-- form-group -->
                            </div>
                       <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">ตำแหน่ง </label> 
                                    <select name="ddlPosition" id="ddlPosition" data-placeholder="กรุณาเลือก" class="width100p">
                                        <option value="0">กรุณาเลือก</option>
                                        <?php
                                        foreach ($position as $val) {
                                            $selc = '';
                                            if ($val['position_id'] == $data['leasing_position_id'])
                                                $selc = 'selected="selected"';
                                            ?>
                                            <option value="<?= $val['position_id'] ?>" <?= $selc; ?>><?= $val['position_name'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>    
                                </div>
                        </div>      
                   </div> <!-- row -->
                   
                   <div class="row">
                        <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label">ผู้รับมอบอำนาจให้ทำสัญญาแทน ตามหนังสือมอบอำนาจ ลงวันที่ <span class="asterisk">*</span></label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_attorney_date" id="txtLeasing_attorney_date"  class="txtDate form-control" value="<?php echo @$data['leasing_date']; ?>" readonly="readonly" required/>
                                    <span class="iconDate input-group-addon hand" id="attorney_date" data-ref="txtLeasing_attorney_date"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                </div><!-- form-group -->
                            </div>
                       <div class="col-sm-7">
                                <div class="form-group">
                                    <label class="control-label">ซึ่งต่อไปในสัญญานี้เรียกว่า “ผู้ให้เช่าซื้อ” ฝ่ายหนึ่ง กับ <span class="asterisk">*</span></label> 
                                    <input type="text" name="txtOrganization_name" id="txtOrganization_name" class="form-control" value="<?php echo @$data['organization']['org_name']; ?>" maxlength="255" required />
                                </div>
                        </div> 
                   
                   </div> <!-- row -->
					 
                    <div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ตั้งอยู่เลขที่ <span class="asterisk">*</span></label>
								<input type="text" name="txtOrg_addr_no" id="txtOrg_addr_no" class="form-control" value="<?php echo @$data['organization']['org_addr_no']; ?>" maxlength="17"  required/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมู่ที่ </label>
								<input type="text" name="txtOrg_addr_moo" id="txtOrg_addr_moo" class="form-control" value="<?php echo @$data['organization']['org_addr_moo']; ?>" maxlength="17" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
					</div><!-- row -->
 
                    <div class="row">
                       <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                                <select name="ddlProvince" id="ddlProvince" data-placeholder="กรุณาเลือก" class="width100p" required>
                                    <option value="0">กรุณาเลือก</option>
                                    <?php foreach ($province as $province_data) { ?>
                                        <option value="<?php echo $province_data['province_id']; ?>"<?php if($data['organization']['org_addr_province_id']==$province_data['province_id']) echo ' SELECTED'; ?>><?php echo $province_data['province_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div><!-- form-group -->
                        </div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">อำเภอ <span class="asterisk">*</span></label>
								<div id="divAmphur">
									<select name="ddlAmphur" id="ddlAmphur" data-placeholder="กรุณาเลือก" class="width100p" required>
										<option value="0">กรุณาเลือก</option>
										<?php foreach ($amphur as $amphur_data) { ?>
											<option value="<?php echo $amphur_data['amphur_id']; ?>"<?php if($data['organization']['org_addr_amphur_id']==$amphur_data['amphur_id']) echo ' SELECTED'; ?>><?php echo $amphur_data['amphur_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ตำบล <span class="asterisk">*</span></label>
								<div id="divDistrict">
									<select name="ddlDistrict" id="ddlDistrict" data-placeholder="กรุณาเลือก" class="width100p" required>
										<option value="0">กรุณาเลือก</option>
										<?php foreach ($district as $district_data) { ?>
											<option value="<?php echo $district_data['district_id']; ?>"<?php if($data['organization']['org_addr_district_id']==$district_data['district_id']) echo ' SELECTED'; ?>><?php echo $district_data['district_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
                    
                    <div class="row">
                       <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label">ตามหนังสือรับรองของนายทะเบียน <span class="asterisk">*</span></label> 
                                    <input type="text" name="txtLeasing_registrar1" id="txtLeasing_registrar1" class="form-control" value="<?php echo @$data['leasing_registrar1']; ?>" maxlength="255" required/>
                                </div>
                        </div>
                        <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">เลขทะเบียนที่ <span class="asterisk">*</span></label> 
                                    <input type="text"  name="txtLeasing_registrar1_no" id="txtLeasing_registrar1_no" class="form-control" value="<?php echo @$data['leasing_registrar1_no']; ?>" maxlength="255" Min="0" required />
                                </div>
                        </div>
                        <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ลงวันที่ <span class="asterisk">*</span></label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_registrar1_date" id="txtLeasing_registrar1_date"  class="txtDate form-control" value="<?php echo @$data['leasing_registrar1_date']; ?>" readonly="readonly" required/>
                                    <span class="iconDate input-group-addon hand" id="attorney_date" data-ref="txtLeasing_registrar1_date"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                </div><!-- form-group -->
                            </div> 
                    
                    </div> <!-- row -->
                    
                    <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="ddLeasing_registrar_title_id">คำนำหน้าชื่อ</label>
                            <select name="ddLeasing_registrar_title_id" id="ddLeasing_registrar_title_id" data-placeholder="กรุณาเลือก" class="width100p" >
                                    <option value="0">กรุณาเลือก</option>
                                    <?php foreach ($title as $title_data) { ?>
                                        <option value="<?php echo $title_data['title_id']; ?>"<?php if(@$data['leasing_registrar_title_id']==$title_data['title_id']) echo ' SELECTED'; ?>><?php echo $title_data['title_name']; ?></option>
                                    <?php } ?>
                                </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">ชื่อ <span class="asterisk">*</span></label> 
                            <input type="text" name="txtLeasing_registrar_name" id="txtLeasing_registrar_name" class="form-control" value="<?php echo @$data['leasing_registrar_name']; ?>" >
                        </div>
                    </div> 
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">นามสกุล  <span class="asterisk">*</span></label> 
                            <input type="text" class="form-control"name="txtLeasing_registrar_surname" id="txtLeasing_registrar_surname"  value="<?php echo @$data['leasing_registrar_surname']; ?>" >
                        </div>
                    </div>                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">ตำแหน่ง </label> 
                            <input type="text" class="form-control"name="txtLeasing_registrar_position" id="txtLeasing_registrar_position"  value="<?php echo @$data['leasing_registrar_position']; ?>" >
                        </div>
                    </div>
                 </div><!-- row -->
                    
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">ซึ่งเป็นผู้มีอำนาจดำเนินกิจการของ </label> 
                            <textarea id="txtLeasing_authorities" name="txtLeasing_authorities" class="form-control" rows="5" ><?php echo @$data['leasing_authorities']; ?></textarea>  
                        </div>
                    </div>
                </div>  <!-- row -->    
                
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="control-label">ตามหนังสือรับรองของนายทะเบียน </label> 
                             <input type="text" name="txtLeasing_registrar2" id="txtLeasing_registrar2" class="form-control" value="<?php echo @$data['leasing_registrar2']; ?>" maxlength="255" />
                        </div>
                    </div>     
                   <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">เลขที่  </label> 
                            <input type="number" class="form-control" name="txtLeasing_registrar2_no" id="txtLeasing_registrar2_no"  value="<?php echo @$data['leasing_registrar2_no']; ?>" min='0'  required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">ลงวันที่ <span class="asterisk">*</span></label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_registrar2_date" id="txtLeasing_registrar2_date"  class="txtDate form-control" value="<?php echo @$data['leasing_registrar2_date']; ?>" readonly="readonly" required/>
                                    <span class="iconDate input-group-addon hand" id="attorney_date" data-ref="txtLeasing_registrar2_date"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                </div><!-- form-group -->
                            </div> 
                </div>  <!-- row -->
                
                 <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">หรือตามข้อบังคับ</label> 
                            <textarea id="txtLeasing_rules" name="txtLeasing_rules" class="form-control" rows="5" ><?php echo @$data['leasing_rules']; ?></textarea>  
                        </div>
                    </div>
                </div>  <!-- row --> 
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">หรือมติคณะกรรมการ</label> 
                            <textarea id="txtLeasing_board_resolutions" name="txtLeasing_board_resolutions" class="form-control" rows="5" ><?php echo @$data['leasing_board_resolutions']; ?></textarea>  
                        </div>
                    </div>
                </div>  <!-- row --> 
                
                <div class="row">
                     <div class="col-sm-5">
                         <div class="form-group">
                                    <label class="control-label"> ผู้เช่าซื้อตกลงเช่าซื้อ และผู้ให้เช่าซื้อตกลงให้เช่าซื้อที่ดิน จำนวน</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_area_amount" id="txtLeasing_area_amount"  class="form-control" value="<?php echo @number_format($data['leasing_area_amount'],2); ?>" readonly="readonly" />
                                    <span class="input-group-addon">แปลง</span>
                                </div>
                          </div><!-- form-group --> 
                    </div>
                    <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">จำนวนเนื้อที่ <span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="input-group mb15">
                                            <input type="text" name="txtRai" id="txtRai" class="form-control right" value="<?php echo @$data['leasing_area_rai']; ?>" readonly="readonly" />
                                            <span class="input-group-addon">ไร่</span>
                                        </div>
                                        <label class="error" for="txtRai"></label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group mb15">
                                            <input type="text" name="txtNgan" id="txtNgan" class="form-control right" value="<?php echo @$data['leasing_area_ngan']; ?>" readonly="readonly" />
                                            <span class="input-group-addon">งาน</span>
                                        </div>
                                        <label class="error" for="txtNgan"></label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group mb15">
                                            <input type="text" name="txtWah" id="txtWah" class="form-control right" value="<?php echo @$data['leasing_area_wah']; ?>"  readonly="readonly"/>
                                            <span class="input-group-addon">ตารางวา</span>
                                        </div>
                                        <label class="error" for="txtWah"></label>
                                    </div>
                                </div>
                            </div><!-- form-group -->
                        </div>
                </div> <!-- row -->
                 
                <div align="left"><button type="button" class="btn btn-default btn-sm modalLand"><i class="fa fa-plus"></i> เพิ่มรายการที่ดิน</button>
                <br><br>
                 
                </div>
                <table id="area_landClone" align="center">
                  <tr>
                   <th>ลำดับที่</th>
                   <th>รายละเอียด</th>
                   <th width="10%">จัดการ</th>
                  </tr>
                  <?php
                   if(isset($data['land']) && $data['land'] !=""){
                       $row=0;                                                                             
                       foreach($data['land'] as $key=>$val){                   
                           ?>
                               <!-- --------------------------------- Land ----------------------------------------- -->
                               <tr>
                                <td width="7%"><div class="row_land">1.<?php echo ++$row ; ?></div></td>
                                <td>
                                <div class="row">
                                       <div class="col-sm-4">
                                           <div class="form-group">
                                                <label class="control-label">ตามหนังสือแสดงสิทธิที่ดิน</label>     
                                                 <select  data-placeholder="กรุณาเลือก" class="width100p form-control" name="edit_land_type_id" disabled="disabled" >
                                                    <option value="0">กรุณาเลือก</option>
                                                    <?php foreach ($land_type as $land_type_data) { ?>
                                                        <option value="<?php echo $land_type_data['land_type_id']; ?>"<?php if($val['land_type_id']==$land_type_data['land_type_id']) echo ' SELECTED'; ?>><?php echo $land_type_data['land_type_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <input type="hidden" name="edit_land_id" value="<?php echo @$val['land_id']?>" disabled="disabled">
                                            </div>
                                       </div><!-- form-group --> 
                                       <div class="col-sm-2">
                                           <div class="form-group">
                                                <label class="control-label">เลขที่ </label> 
                                                <input type="number" class="form-control" name="edit_land_no"   value="<?php echo @$val['land_no']; ?>" readonly="readonly" >
                                            </div>
                                       </div><!-- form-group -->
                                       <div class="col-sm-2">
                                           <div class="form-group">
                                                <label class="control-label">เลขที่ดิน </label> 
                                                <input type="number" class="form-control" name="edit_land_addr_no"    value="<?php echo @$val['land_addr_no']; ?>" readonly="readonly" >
                                            </div>
                                       </div><!-- form-group -->
                                       <div class="col-sm-3">
                                           <div class="form-group">
                                                <label class="control-label">หน้าสำรวจ </label> 
                                                <input type="number" class="form-control" name="edit_land_survey_page"   value="<?php echo  $val['land_survey_page'] ?>" readonly="readonly" >
                                            </div>
                                       </div><!-- form-group -->  
                                     </div> <!-- row -->
                                       
                                <div class="row">
                                           <div class="col-sm-3">
                                               <div class="form-group">
                                                    <label class="control-label">ตั้งอยู่ หมู่ที่ </label> 
                                                    <input type="number" class="form-control" name="edit_land_addr_moo" id="txtLand_addr_moo"  value="<?php echo $val['land_addr_moo'] ?>" readonly="readonly" >
                                                </div>
                                           </div>
                                           <div class="col-sm-3">
                                              
                                                <div class="form-group">
                                                    <label class="control-label">จังหวัด </label>
                                                 <select  data-placeholder="กรุณาเลือก" class="width100p form-control"  name="edit_province" disabled="disabled">
                                                    <option value="0">กรุณาเลือก</option>
                                                    <?php foreach ($province as $province_data) { ?>
                                                        <option value="<?php echo $province_data['province_id']; ?>"<?php if($val['land_addr_province_id']==$province_data['province_id']) echo ' SELECTED'; ?>><?php echo $province_data['province_name']; ?></option>
                                                    <?php } ?>
                                                </select> 
                                                </div><!-- form-group -->
                                            </div><!-- col-sm-3 -->
                                           <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label">อำเภอ </label>
                                                    <select  data-placeholder="กรุณาเลือก" class="width100p form-control" disabled="disabled" name="edit_amphur">
                                                         <?php foreach ($amphur as $amphur_data) { ?>
                                                            <option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if($val['land_addr_amphur_id']==$amphur_data['amphur_id']) echo ' SELECTED'; ?>><?php echo $amphur_data['amphur_name']; ?></option>
                                                        <?php } ?>
                                                    </select>     
                                                </div><!-- form-group -->
                                            </div><!-- col-sm-3 -->
                                           <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="control-label">ตำบล </label>
                                                      <select data-placeholder="กรุณาเลือก" class="width100p form-control" name="edit_district" disabled="disabled" name="edit_district">
                                                        <?php foreach ($district as $district_data) { ?>
                                                            <option value="<?php echo $district_data['district_id']; ?>" <?php if($val['land_addr_district_id']==$district_data['district_id']) echo ' SELECTED'; ?>><?php echo $district_data['district_name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                                     
                                                </div><!-- form-group -->
                                            </div><!-- col-sm-3 -->
                                        </div>  <!-- row -->
                                         
                                <div class="row">    
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label">จำนวนเนื้อที่ </label>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb15">
                                                            <input type="text" name="edit_land_area_rai"   class="land_rai form-control right" value="<?php echo $val['land_area_rai'] ?>"  readonly="readonly"/>
                                                            <span class="input-group-addon">ไร่</span>                                               
                                                        </div>                                                                    
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb15">
                                                            <input type="text" name="edit_land_area_ngan"   class="land_ngan form-control right" value="<?php echo $val['land_area_ngan'] ?>"  readonly="readonly"/>
                                                            <span class="input-group-addon">งาน</span>
                                                        </div>                                                                  
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="input-group mb15">
                                                            <input type="text" name="txtLand_area_wah"   class="land_wah form-control right" value="<?php echo $val['land_area_wah'] ?>" readonly="readonly" />
                                                            <span class="input-group-addon">ตารางวา</span>
                                                        </div>                                                                       
                                                    </div>
                                                </div>
                                            </div><!-- form-group -->
                                        </div> 
                                 </div>  <!-- row -->
                                 </td>
                                 <td>
                                 <button class="btn btn-success btn-xs editrowLand" type="button" title="แก้ไข"><i class="fa fa-edit"></i></button>
                                 <button class="btn btn-danger btn-xs"  type="button" title="ลบ" onclick="return confirm('คุณต้องการลบข้อมูล  ? ')? delRowLand($(this).closest('tr').index()):'';"><i class="fa fa-trash-o"></i></button>
                                  </td>
                               </tr>
                               <!-- --------------------------------- Land ----------------------------------------- -->
                           <?php
                       }
                   }
                  
                  ?>
                </table>
                 <br>
 
                <div class="row">
                     <div class="col-sm-12">
                     <div class="form-group">
                               <label class="control-label">เพื่อ</label>
                               <textarea id="txtLeasing_detail" name="txtLeasing_detail" class="form-control" rows="5" ><?php echo @$data['leasing_detail']; ?></textarea>
                      </div> 
                     </div>
                </div> <!-- row -->
                
                <div class="row">
                    <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">มีผลบังคับตั้งแต่ วันที่</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_effective_date" id="txtLeasing_effective_date"  class="txtDate form-control" value="<?php echo @$data['leasing_effective_date']; ?>" readonly="readonly" required/>
                                    <span class="iconDate input-group-addon hand"  data-ref="txtLeasing_effective_date"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                </div><!-- form-group -->
                            </div> 
                    <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ค่าเช่าซื้อที่ดิน เป็นเงินจำนวน</label>
                                    <div class="input-group">
                                    <input type="text" min="0" name="txtLeasing_amount" id="txtLeasing_amount"  class="form-control" value="<?php echo @number_format($data['leasing_amount'],2); ?>"  />
                                    <span class="input-group-addon" >บาท</span>                                                     
                                </div>
                                </div><!-- form-group -->
                     </div> 
                    <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">ได้ชำระค่าเช่าซื้อหรือค่าเช่าที่ดิน</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_pay_amount" id="txtLeasing_pay_amount"  class="form-control" value="<?php echo @number_format($data['leasing_pay_amount'],2); ?>" step="0.1"  />
                                    <span class="input-group-addon" >บาท</span>
                                </div>
                                </div><!-- form-group -->
                      </div>
                    </div> <!-- row -->
                     
                     <div class="row">
                    <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ต้นเงิน</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_pay_principal" id="txtLeasing_pay_principal"  class="form-control" value="<?php echo @number_format($data['leasing_pay_principal'],2); ?>"/>
                                    <span class="input-group-addon" >บาท</span>
                                </div>
                                </div><!-- form-group -->
                     </div>
                     <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ดอกเบี้ย</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_pay_interest" id="txtLeasing_pay_interest"  class="form-control" value="<?php echo @number_format($data['leasing_pay_interest'],2); ?>"/>
                                    <span class="input-group-addon" >บาท</span>
                                </div>
                                </div><!-- form-group -->
                     </div>
                     <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ตั้งแต่ ปี พ.ศ. <span class="asterisk">*</span></label>
                                    <div class="input-group">
                                        <input type="text" name="txtLeasing_pay_amount_date" id="txtLeasing_pay_amount_date"  class="txtDate form-control" value="<?php echo @$data['leasing_pay_amount_date']; ?>" readonly="readonly" required/>
                                        <span class="iconDate input-group-addon hand"   data-ref="txtLeasing_pay_amount_date"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>  
                                </div><!-- form-group --> 
                            </div>
                </div>  <!-- row -->
                    <div class="row">
                    <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">คงเหลือค่าเช่าที่ดิน</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_pay_amount_principal" id="txtLeasing_pay_amount_principal"  class="form-control" value="<?php echo @number_format($data['leasing_pay_amount_principal'],2); ?>"  readonly="readonly"  />
                                    <span class="input-group-addon" >บาท</span>
                                </div>
                                </div><!-- form-group -->
                     </div>
                     <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ดอกเบี้ย</label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_pay_amount_interest" id="txtLeasing_pay_amount_interest"  class="form-control" value="<?php echo @number_format($data['leasing_pay_amount_interest'],2); ?>"  readonly="readonly"  />
                                    <span class="input-group-addon" >บาท</span>
                                </div>
                                </div><!-- form-group -->
                     </div> 
                </div>  <!-- row --> 
                <div class="row">
                   <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">เป็นงวดราย <span class="asterisk">*</span></label>
                                    <select name="txtLeasing_length" id="ddlLeasing_length" data-placeholder="กรุณาเลือก" class="width100p" required>
                                        <option value="0">กรุณาเลือก</option>                                                                                                       
                                           <?php
                                                if($leasing_length){
                                                   foreach($leasing_length as $key=>$val){
                                                        ?>
                                                        <option value="<?php echo $key ?>" <?php if(@$data['leasing_length']==$key) echo ' SELECTED'; ?>><?php echo $val; ?></option>
                                                        <?php 
                                                    }
                                                }
                                                ?>
                                    </select> 
                                </div>
                     </div><!-- form-group -->
                      
                     <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label">ในระยะเวลา <span class="asterisk">*</span></label>
                                     <div class="input-group">
                                        <input type="number" name="txtLeasing_periods" id="txtLeasing_periods"  class="form-control" value="<?php echo @$data['leasing_periods']; ?>" required />
                                        <span class="input-group-addon" >งวด</span>
                                    </div>
                                </div>
                     </div><!-- form-group -->
                     <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">อัตราดอกเบี้ย</label>    
                                       <select name="txtLeasing_interest" id="ddlLeasing_interest" data-placeholder="กรุณาเลือก" class="width100p">
                                                    <option value="3"<?php echo $data['leasing_interest']=='3'? ' selected' : ''; ?>>3</option>
                                                    <option value="4"<?php echo $data['leasing_interest']=='4'? ' selected' : ''; ?>>4</option>
                                                    <option value="5"<?php echo $data['leasing_interest']=='5'? ' selected' : ''; ?>>5</option>
                                                </select>  
                                     
                                </div>
                     </div>  
                </div> <!-- row --> 
                
                <div class="row">
                     <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">เริ่มชำระค่าเช่าซื้อ ตั้งแต่วันที่ <span class="asterisk">*</span></label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_from_date" id="txtLeasing_from_date"  class="txtDateStartDate form-control" value="<?php echo @$data['leasing_from_date']; ?>" readonly="readonly" required/>
                                    <span class="iconDate input-group-addon hand" id="attorney_date" data-ref="txtLeasing_from_date"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                </div><!-- form-group -->
                    </div>
                   <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">ครบกำหนดงวดสุดท้าย ในวันที่ <span class="asterisk">*</span></label>
                                    <div class="input-group">
                                    <input type="text" name="txtLeasing_to_date" id="txtLeasing_to_date"  class="txtDateLongYear2 form-control" value="<?php echo @$data['leasing_to_date']; ?>" readonly="readonly" required/>
                                    <span class="iconDateLongYear input-group-addon hand" data-ref="txtLeasing_to_date2"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                </div><!-- form-group -->
                    </div>
                    <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">เป็นมูลค่าที่ดิน งวดละ</label>                                                                                                                                  
                                    <div class="input-group">
                                        <input type="text"   min="0" name="txtLeasing_payment_period" id="txtLeasing_payment_period"  class="form-control" value="<?php echo @number_format($data['leasing_payment_period'],2); ?>"  readonly="readonly" />
                                        <span class="input-group-addon" >บาท</span>
                                    </div> 
                                </div>   
                     </div>
                     <div class="col-sm-3">
                     <div class="form-group">
                                    <label class="control-label">&nbsp;</label>                                                                                                                                  
                                    <div class="input-group">
                                        <button type="button" id="btnLeasingCal" class="btn btn-primary">คำนวน</button> 
                                    </div> 
                                </div>     
                    </div>
                </div> <!-- row -->
                
                <div class="row">
                    <div id="div_leasingCal" class="row" style="width: 100%; overflow:scroll;"></div>
                
                </div>
                   
                
                <div class="row">
                   <div class="col-sm-9">
                                <div class="form-group">
                                    <label class="control-label"> จัดการเกี่ยวกับค่าเช่าซื้อ โดยให้ถือว่าค่าเช่าซื้อที่ได้รับชำระไว้แล้ว เป็นค่าเช่าตลอดระยะเวลาที่ผู้เช่าซื้อได้เข้าทำประโยชน์ในที่ดิน ในอัตราไร่ละ</label>
                                    <div class="input-group">
                                        <input type="text"   min="0" name="txtLeasing_rate" id="txtLeasing_rate"  class="form-control" value="<?php echo @number_format($data['leasing_rate'],2); ?>"  />
                                        <span class="input-group-addon" >บาท ต่อปี</span>
                                    </div>
                                </div>
                     </div>
                </div> <!-- row -->
                 
                
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>
     

<div id="modalLand" class="modal  fade" tabindex="-1" role="dialog"  >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title">เพิ่มรายการที่ดิน</h4>
            </div>
            <div class="modal-body">
                <form id="frmAddLeasingLand" action="<?php echo base_url().'leasing/save_principle_other'; ?>" method="post" class="form-horizontal form-bordered">
                       <div class="row">
                           <div class="col-sm-4">
                               <div class="form-group">
                                    <label class="control-label">ตามหนังสือแสดงสิทธิที่ดิน </label>            
                                    <select name="ddlLandType" id="ddlLandType" data-placeholder="กรุณาเลือก" class="width100p" required>
                                        <option value="">กรุณาเลือก</option>
                                        <?php foreach ($land_type as $land_type_data) { ?>
                                            <option value="<?php echo $land_type_data['land_type_id']; ?>"><?php echo $land_type_data['land_type_name']; ?></option>
                                        <?php } ?>
                                    </select>    
                                </div>
                                <input type="hidden" id="row_id">
                                <input type="hidden" name="land_id" id="land_id">
                           </div><!-- form-group -->   
                           <div class="col-sm-2">
                               <div class="form-group">
                                    <label class="control-label">เลขที่ </label> 
                                    <input type="number" class="form-control" name="txtLand_no" id="txtLand_no"  value="<?php echo @$data['land_no']; ?>"  required>
                                </div>
                           </div><!-- form-group -->
                           <div class="col-sm-2">
                               <div class="form-group">
                                    <label class="control-label">เลขที่ดิน </label> 
                                    <input type="number" class="form-control" name="txtLand_addr_no" id="txtLand_addr_no"  value="<?php echo @$data['land_addr_no']; ?>" required>
                                </div>
                           </div><!-- form-group -->
                           <div class="col-sm-3">
                               <div class="form-group">
                                    <label class="control-label">หน้าสำรวจ </label> 
                                    <input type="number" class="form-control" name="txtLand_survey_page" id="txtLand_survey_page"  value="<?php echo @$data['land_survey_page']; ?>" required>
                                </div>
                           </div><!-- form-group -->  
                         </div> <!-- row -->
                           
                    <div class="row">
                               <div class="col-sm-2">
                                   <div class="form-group">
                                        <label class="control-label">ตั้งอยู่ หมู่ที่ </label> 
                                        <input type="number" class="form-control" name="txtLand_addr_moo" id="txtLand_addr_moo"  value="<?php echo @$data['land_addr_moo']; ?>" >
                                    </div>
                               </div>
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                                        <select name="ddlCreditorProvince" id="ddlCreditorProvince" data-placeholder="กรุณาเลือก" class="width100p" required>
                                            <option value="">กรุณาเลือก</option>
                                            <?php foreach ($province as $province_data) { ?>
                                                <option value="<?php echo $province_data['province_id']; ?>"><?php echo $province_data['province_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div><!-- form-group -->
                                </div><!-- col-sm-3 -->
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">อำเภอ <span class="asterisk">*</span></label>
                                        <div id="divCreditorAmphur">
                                            <select name="ddlCreditorAmphur" id="ddlCreditorAmphur" data-placeholder="กรุณาเลือก" class="width100p" required>
                                                <option value="">กรุณาเลือก</option>
                                                <?php foreach ($amphur as $amphur_data) { ?>
                                                    <option value="<?php echo $amphur_data['amphur_id']; ?>"><?php echo $amphur_data['amphur_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- form-group -->
                                </div><!-- col-sm-3 -->
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">ตำบล <span class="asterisk">*</span></label>
                                        <div id="divCreditorDistrict">
                                            <select name="ddlCreditorDistrict" id="ddlCreditorDistrict" data-placeholder="กรุณาเลือก" class="width100p" required>
                                                <option value="">กรุณาเลือก</option>
                                                <?php foreach ($district as $district_data) { ?>
                                                    <option value="<?php echo $district_data['district_id']; ?>" ><?php echo $district_data['district_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div><!-- form-group -->
                                </div><!-- col-sm-3 -->
                            </div>  <!-- row -->
                             
                    <div class="row">    
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">จำนวนเนื้อที่ <span class="asterisk">*</span></label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group mb15">
                                                <input type="number" name="txtLeasing_area_rai" id="txtLeasing_area_rai" class="form-control right" value="<?php echo @$data['leasing_area_rai']; ?>" min="0" required/>
                                                <span class="input-group-addon">ไร่</span>
                                            </div>
                                            <label class="error" for="txtLeasing_area_rai"></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group mb15">
                                                <input type="number" name="txtLeasing_area_ngan" id="txtLeasing_area_ngan" class="form-control right" value="<?php echo @$data['leasing_area_ngan']; ?>"  min="0" max="3" required/>
                                                <span class="input-group-addon">งาน</span>
                                            </div>
                                            <label class="error" for="txtLeasing_area_ngan"></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group mb15">
                                                <input type="number" name="txtWah" id="txtLeasing_area_wah" class="form-control right" value="<?php echo @$data['leasing_area_wah']; ?>"  min="0" step="0.01" max="99.99" required/>
                                                <span class="input-group-addon">ตารางวา</span>
                                            </div>
                                            <label class="error" for="txtLeasing_area_wah"></label>
                                        </div>
                                    </div>
                                </div><!-- form-group -->
                            </div> 
                     </div>  <!-- row -->
                 </form> 
            </div>
            <div class="modal-footer">
                 <div style="display: none;" id="btn_edit"><button id="btnEditLeasingLand" type="button" class="btn btn-primary">แก้ไข</button></div>
                <div id="btn_add"><button id="btnSaveLeasingLand" type="button" class="btn btn-primary">บันทึก</button></div>
            </div>
        </div>
    </div>
</div>
 
<style type="text/css">
has-error.error{color:#C27A78;}
label.error{color:#C27A78;}
table#area_landClone{
    width:90%;
}
table#area_landClone tr td {
    border: 1px solid #ddd;
    padding: 10px;
    background: #f7f7f7;
}
table#area_landClone tr th {
    border: 1px solid #ddd;
    padding: 10px;
    background: #ededed;
    font-weight: bold;
}
</style>


<div id="modalLeasingCal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title">ประมาณการ</h4>
            </div>
            <div class="modal-body">
                <form id="frmLeasingCal" action="<?php echo base_url().'leasing/save_leasingCal'; ?>" method="post" class="form-horizontal form-bordered">
                    <div id="divCalLeasing"></div>
                </form> 
            </div>
            <div class="modal-footer">
                <button id="btnLeasingCalSave" type="button" class="btn btn-primary">บันทึก</button>
            </div>
        </div>
    </div>
</div>
