<?php
     $total_pay =0;
     $time =1;
    if(isset($schedule_all)){
        foreach($schedule_all as $key=>$val){
            $total_pay += $val['leasing_schedule_money_pay']; 
            $remain_principal[$time] = $data['leasing_pay_amount_principal'] - $total_pay;
             
            $time++;
        }
    }
    
 ?> 

      <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
                <td width="80%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="text-align:right; color:#aaa;"></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 10px;"></td>
                        </tr>
                    </table>
                    <hr />
                </td>
            </tr>
        </table>   

<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" style="font-weight: bold; text-align:center;">
                    ใบแจ้งหนี้การชำระ งวดที่ &nbsp;<?php echo $schedule['leasing_schedule_pay_number']?>&nbsp;
                </td>
            </tr>
            </table> 

 <table width="100%" border="0" cellpadding="0" cellspacing="0"   >
 <tr>
    <td width="70%">  <!-- colum1 -->
        <table>
            <tr>
                <td width="15%">ชื่อผู้กู้</td>
                <td width="90%"><?php echo $organization['org_name']?></td>
            </tr>
            <tr>
                <td>ที่อยู่</td>
                <td><?php echo "เลขที่ ".$organization['org_addr_no']."&nbsp;"."หมู่ที่&nbsp;".$organization['org_addr_moo']." ตำบล ".$organization['org_district_name']." <br>อำเภอ ".$organization['org_amphur_name']." จังหวัด".$organization['org_province_name']?></td>
            </tr>
            <tr>
                <td>การนำส่ง</td>
                <td>โอนผ่าน ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร  สาขา บางเขน</td>
            </tr>
            <tr>
                <td>ชื่อบัญชี</td>
                <td>สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
            </tr>
            <tr>
                <td></td>
                <td>ประเภทเงินฝากออมทรัพย์ เลขที่ 020082094471</td>
            </tr>
            <tr><td></td><td></td></tr>
        </table>
    </td>
    <td width="30%"> <!-- colum2 -->
        <table>
            <tr>
                <td>วันที่</td>
                <td><?php echo thai_display_date_short(date('Y-m-d'))?></td>
            </tr>
            <tr>
                <td>เลขที่สัญญา</td>
                <td><?php echo $data['leasing_contract_no']?></td>
            </tr>
            <tr>
            <td>ดอกเบี้ย</td>
            <td>ร้อยละ <?php echo number_format($data['leasing_interest'],2)?>  ต่อปี</td>
            </tr>
            <tr>
            <td>จำนวนงวดชำระ</td>
            <td><?php echo $data['leasing_periods']?> งวด</td>
            </tr>
            <tr>
                <td>วงเงินเช่าซื้อ</td>
                <td><?php echo number_format($data['leasing_pay_amount_principal'],2)?></td>
            </tr>
        </table>
    </td>
 </tr>
 </table>
 
 <table style="border: 1px solid #333;">
 <tr>
    <td align="center" width="13%" style="border: 1px solid #7f7f7f;">วัน/เดือน/ปี<br>ที่ชำระ</td>
    <td align="center" width="7%" style="border: 1px solid #7f7f7f;">งวดที่</td>        
    <td align="center" width="20%" style="border: 1px solid #7f7f7f;">เงินต้น (บาท)</td>        
    <td align="center" width="20%" style="border: 1px solid #7f7f7f;">ดอกเบี้ย (บาท)</td>        
    <td align="center" width="20%" style="border: 1px solid #7f7f7f;">ยอดชำระทั้งสิ้น (บาท)</td>        
    <td align="center" width="20%" style="border: 1px solid #7f7f7f;">เงินต้นคงเหลือ (บาท)</td>    
 </tr>
 <tr>
 <td align="center" style="border: 1px solid #7f7f7f;"><?php echo thai_display_date_short($schedule['leasing_schedule_date_payout'])?></td>
 <td align="center" style="border: 1px solid #7f7f7f;"><?php echo $schedule['leasing_schedule_pay_number']?></td>
 <td align="right" style="border: 1px solid #7f7f7f;"><?php echo number_format($schedule['leasing_schedule_money_pay'],2)?></td>
 <td align="right" style="border: 1px solid #7f7f7f;"><?php echo number_format($schedule['leasing_schedule_pay_interest'],2)?></td>
 <td align="right" style="border: 1px solid #7f7f7f;"><?php echo number_format(($schedule['leasing_schedule_money_pay']+$schedule['leasing_schedule_pay_interest']),2)?></td>
 <td align="right" style="border: 1px solid #7f7f7f;"><?php echo number_format($remain_principal[$schedule['leasing_schedule_pay_number']],2)?></td>
 </tr>
 </table>
 
 <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" style="color:#7f7f7f">
                    <br><br><br>ทั้งนี้  ท่านต้องเป็นผู้รับภาระค่าธรรมเนียมในการโอนเงินที่ธนาคารการเกษตรและสหกรณ์การเกษตร  
                </td>
            </tr>
            <tr>
                <td width="100%" style="color:#7f7f7f">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เรียกเก็บเพิ่มอีกจำนวน 10 บาท (สิบบาทถ้วน)
                </td>
            </tr>
            <tr>
                <td width="100%"  align="right">
                    <p></p>นางราตรี  พยนต์รักษ์ &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;<br>ผู้อำนวยการกองการเงินและบัญชี
                </td>
            </tr>
            </table>
 

