<select name="ddlCreditorAmphur" id="ddlCreditorAmphur" data-placeholder="กรุณาเลือก" class="width100p">
    <option value="">กรุณาเลือก</option>
    <?php foreach ($amphur as $amphur_data) { ?>
        <option value="<?php echo $amphur_data['amphur_id']; ?>"><?php echo $amphur_data['amphur_name']; ?></option>
    <?php } ?>
</select>
<script type="text/javascript">
    $("#ddlCreditorAmphur").select2({
        minimumResultsForSearch: -1
    });

    $("#ddlCreditorAmphur").change(function(){
        $("#divCreditorDistrict").load(base_url+"leasing/district_creditor/"+$("#ddlCreditorAmphur").val());
    });
</script>