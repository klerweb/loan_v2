<?php 
$this->load->model('land_type/land_type_model', 'land_type');  
$this->load->model('province/province_model', 'province');   
$this->load->model('amphur/amphur_model', 'amphur'); 
$this->load->model('district/district_model', 'district');      
       
$land_type =    $this->land_type->all();  
$province =  $this->province->all();

$amphur = $ddlCreditorProvince ? $this->amphur->getByProvince($ddlCreditorProvince): array();
$district = $ddlCreditorAmphur ? $this->district->getByAmphur($ddlCreditorAmphur) : array() ; 
 
?>                 
                    <td width="7%"><div class="row_land"></div></td>
                    <td>
                    <div class="row">
                           <div class="col-sm-4">
                               <div class="form-group">
                                    <label class="control-label">ตามหนังสือแสดงสิทธิที่ดิน</label>     
                                     <select   data-placeholder="กรุณาเลือก" class="width100p form-control" disabled="disabled" >
                                        <option value="">กรุณาเลือก</option>
                                        <?php foreach ($land_type as $land_type_data) { ?>
                                            <option value="<?php echo $land_type_data['land_type_id']; ?>"<?php if($ddlLandType==$land_type_data['land_type_id']) echo ' SELECTED'; ?>><?php echo $land_type_data['land_type_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                     <input type="hidden" name="ddlLand_type_id[]" value="<?php echo $ddlLandType ?>">
                                     <input type="hidden" name="land_id[]" value="<?php echo @$land_id ?>">
                                </div>
                           </div><!-- form-group --> 
                           <div class="col-sm-2">
                               <div class="form-group">
                                    <label class="control-label">เลขที่ </label> 
                                    <input type="number" class="form-control" name="txtLand_no[]"    value="<?php echo @$txtLand_no; ?>" readonly="readonly" >
                                </div>
                           </div><!-- form-group -->
                           <div class="col-sm-2">
                               <div class="form-group">
                                    <label class="control-label">เลขที่ดิน </label> 
                                    <input type="number" class="form-control" name="txtLand_addr_no[]"    value="<?php echo @$txtLand_addr_no; ?>" readonly="readonly" >
                                </div>
                           </div><!-- form-group -->
                           <div class="col-sm-3">
                               <div class="form-group">
                                    <label class="control-label">หน้าสำรวจ </label> 
                                    <input type="number" class="form-control" name="txtLand_survey_page[]"   value="<?php echo  $txtLand_survey_page ?>" readonly="readonly" >
                                </div>
                           </div><!-- form-group -->  
                         </div> <!-- row -->
                           
                    <div class="row">
                               <div class="col-sm-3">
                                   <div class="form-group">
                                        <label class="control-label">ตั้งอยู่ หมู่ที่ </label> 
                                        <input type="number" class="form-control" name="txtLand_addr_moo[]" id="txtLand_addr_moo"  value="<?php echo $txtLand_addr_moo ?>" readonly="readonly" >
                                    </div>
                               </div>
                               <div class="col-sm-3">
                                  
                                    <div class="form-group">
                                        <label class="control-label">จังหวัด </label>
                                     <select  data-placeholder="กรุณาเลือก" class="width100p form-control"  disabled="disabled">
                                        <option value="">กรุณาเลือก</option>
                                        <?php foreach ($province as $province_data) { ?>
                                            <option value="<?php echo $province_data['province_id']; ?>"<?php if($ddlCreditorProvince==$province_data['province_id']) echo ' SELECTED'; ?>><?php echo $province_data['province_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" name="landProvince[]" value="<?php echo $ddlCreditorProvince ?>"> 
                                    </div><!-- form-group -->
                                </div><!-- col-sm-3 -->
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">อำเภอ </label>
                                        <select  data-placeholder="กรุณาเลือก" class="width100p form-control" disabled="disabled">
                                             <?php foreach ($amphur as $amphur_data) { ?>
                                                <option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if($ddlCreditorAmphur==$amphur_data['amphur_id']) echo ' SELECTED'; ?>><?php echo $amphur_data['amphur_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="landAmphur[]" value="<?php echo $ddlCreditorAmphur ?>"> 
                                    </div><!-- form-group -->
                                </div><!-- col-sm-3 -->
                               <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label">ตำบล </label>
                                          <select data-placeholder="กรุณาเลือก" class="width100p form-control" disabled="disabled">
                                            <?php foreach ($district as $district_data) { ?>
                                                <option value="<?php echo $district_data['district_id']; ?>" <?php if($ddlCreditorDistrict==$district_data['district_id']) echo ' SELECTED'; ?>><?php echo $district_data['district_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                         <input type="hidden" name="landDistrict[]" value="<?php echo $ddlCreditorDistrict ?>">               
                                    </div><!-- form-group -->
                                </div><!-- col-sm-3 -->
                            </div>  <!-- row -->
                             
                    <div class="row">    
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">จำนวนเนื้อที่ <span class="asterisk">*</span></label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group mb15">
                                                <input type="text" name="leasing_area_rai[]"   class="land_rai form-control right" value="<?php echo $txtLeasing_area_rai ?>"  readonly="readonly"/>
                                                <span class="input-group-addon">ไร่</span>                                               
                                            </div>                                                                    
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group mb15">
                                                <input type="text" name="txtLeasing_area_ngan[]"   class="land_ngan form-control right" value="<?php echo $txtLeasing_area_ngan ?>"  readonly="readonly"/>
                                                <span class="input-group-addon">งาน</span>
                                            </div>                                                                  
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group mb15">
                                                <input type="text" name="txtLeasing_area_wah[]"   class="land_wah form-control right" value="<?php echo $txtWah ?>" readonly="readonly" />
                                                <span class="input-group-addon">ตารางวา</span>
                                            </div>                                                                       
                                        </div>
                                    </div>
                                </div><!-- form-group -->
                            </div> 
                     </div>  <!-- row -->
                     </td>
                     <td>
                     <button class="btn btn-success btn-xs" type="button" title="แก้ไข" onclick="editRowLand($(this).closest('tr').index())"><i class="fa fa-edit"></i></button>
                     <button class="btn btn-danger btn-xs"  type="button" onclick="return confirm('คุณต้องการลบข้อมูล  ? ')? delRowLand($(this).closest('tr').index()):'';"><i class="fa fa-trash-o"></i></button>
                      </td>
                                                                                                              
 
