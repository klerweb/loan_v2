<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('leasing/report_invoice2'); ?>">
					<div class="form-group">
						<label class="sr-only" for="leasing_contract_no">สัญญาเช่าซื้อ</label>
						<input type="text" class="form-control" id="contract_no" name="contract_no" placeholder="สัญญาเช่าซื้อ" value="<?php echo @$_POST['contract_no']?>"  />
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="organization_name">ผู้เช่าซื้อ</label>
						<input type="text" class="form-control" id="organization" name="organization" placeholder="ชื่อผู้เช่าซื้อ" value="<?php echo @$_POST['organization']?>" />
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">Search</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
		            	<th>สัญญาเช่าซื้อ</th> nb
		                <th>ผู้เช่าซื้อ</th> 
                        <th>จำนวนเงินเช่าซื้อ</th>
                        <th>วันที่เริ่มทำสัญญา</th>
                        <th>วันที่สิ้นสุดสัญญา</th>
		                <th> </th>
					</tr>
				</thead>
				<tbody>
				<?php 
					if(!empty($result))
					{
						$i = 1;
						foreach ($result as $row)
						{                                                                
							if($row['leasing_status']=='1')
							{
                                 
								$row['leasing_status'] = 'แบบร่าง';
								$menu = '<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="edit(\''.$row['leasing_id'].'\')"><span class="fa fa-edit"></span></button>
											<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ยกเลิก" data-original-title="ยกเลิก" onclick="del(\''.$row['leasing_id'].'\')"><span class="fa fa-trash-o"></span></button>';
							}
							else
							{
								$row['leasing_status'] = 'บันทึกเรียบร้อย';
								$menu = '';
							}
							            
				?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $row['leasing_contract_no']; ?></td>    
                        <td><?php echo $row['org_name'];?></td>
                        <td><?php echo number_format($row['leasing_pay_amount_principal'],2)?></td>  
                        <td><?php echo thai_display_date($row['leasing_date']);?></td>  
						<td><?php echo  thai_display_date($row['leasing_to_date']); ?></td>    
		           		<td align="center">
                             <a href="<?php echo base_url()?>leasing/report_invoice_show/<?php echo $row['leasing_id']?>/1">ใบแจ้งหนี้</a>
		           		</td>
		           	</tr>
				<?php
						}	
					} 
				?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>