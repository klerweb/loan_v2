<?php
 function dateDiff($datetime1,$datetime2,$format='%d'){
     
          // $datediff = strtotime($datetime2)- strtotime($datetime1);   
         // return  floor($datediff / (60 * 60 * 24)); 
               $date1 = new DateTime($datetime1);
               $date2 = new DateTime($datetime2);
        
           return  $date1->diff($date2)->format("%a"); 
             
    }  
 function dateDifffull($date1,$date2){          
        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $interval = $datetime1->diff($datetime2);
        $year = $interval->format('%y');
         
        if($year =='0') return $interval->format('%m เดือน %d วัน');   
        else   return $interval->format('%y ปี %m เดือน %d วัน');   
    }   
        
           $datetime1 = new DateTime('2037-12-01');
        $datetime2 = new DateTime('2038-06-01');
        echo  $datetime1->diff($datetime2)->format("%a");
        
    /*echo  date('Y-m-d',strtotime('2037-12-01'))."<br>";
    echo  date('Y-m-d',strtotime('2038-12-31'))."<br>";        */

 ?> 
 <?php
  if($pdf){
      ?>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
                <td width="80%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="text-align:right; color:#aaa;"></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="100%" style="font-size: 10px;"></td>
                        </tr>
                    </table>
                    <hr />
                </td>
            </tr>
        </table>   
      <?php
  }
 
 ?>
 <center><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" style="font-weight: bold; text-align:center;">
                    <?php if($invoice==1) echo  "ใบแจ้งหนี้" ; else  echo "การ์ดลูกหนี้"; ?>
                </td>                            
            </tr>
            </table> 
<?php if(!$pdf) { ?> <p></p> <?php }?> 
<table width="100%" height="100%" border="0" id="show_card_debtor" name="show_card_debtor">
  <tr>
    <td colspan="4">ชื่อ: <u><?php echo $organization['org_name'] ?></u></td>
    <td colspan="4">ประเภท <u><?php echo $data['leasing_length_name']?></u></td>
    <td colspan="6"></td>
  </tr>
  <tr>
    <td colspan="4">สัญญาเลขที่: <u><?php echo $data['leasing_contract_no']; ?></u></td>
    <td colspan="4">วันที่: <u>   <?php if($pdf) { echo thai_display_date_short($data['leasing_date_org']); }else{ echo $data['leasing_datefull']; }   ?></u></td>
    <td colspan="3">วงเงินกู้: <u><?php echo number_format($data['leasing_pay_amount_principal'],2); ?></u></td>
    <td colspan="3">อัตราดอกเบี้ย:  <u><?php echo number_format($data['leasing_interest'],2); ?>%</u> ปี</td>
  </tr>
  <tr>
    <td colspan="4">วันที่สิ้นสุดสัญญา: <u><?php if($pdf) { echo thai_display_date_short($data['leasing_to_date_org']); }else{ echo $data['leasing_to_datefull']; }   ?> 
    </u></td>
    <td colspan="4">ระยะเวลา: <u> <?php echo $data['leasing_periods']." งวด";  ?></u></td>
	<td colspan="4"><u><?php echo dateDifffull($data['leasing_date_org'],$data['leasing_to_date_org'])?></u></td>
  </tr>
  <tr>
    <td colspan="15"><center>
    <br /> <br />
    <table width="100%" height="100%"  style="border:1px solid #7f7f7f;"  > 
    <tr>
    <td colspan="1" align="center" style="border:1px solid #7f7f7f;">ลำดับที่</td>
    <td colspan="1" align="center" style="border:1px solid #7f7f7f;">งวดที่</td>
    <td colspan="2" align="center" style="border:1px solid #7f7f7f;">กำหนดชำระ</td>
    <td colspan="1" align="center" style="border:1px solid #7f7f7f;">วัน</td>
    <td colspan="2" align="center" style="border:1px solid #7f7f7f;">เงินต้นยกมา</td>
    <td colspan="1" align="center" style="border:1px solid #7f7f7f;">ดอกเบี้ย</td>
    <td colspan="2" align="center" style="border:1px solid #7f7f7f;">ยอดส่ง</td>
    <td colspan="2" align="center" style="border:1px solid #7f7f7f;">เป็นเงินต้น</td>
    <td colspan="1" align="center" style="border:1px solid #7f7f7f;" width="10%">เป็นดอกเบี้ย</td>
    <td colspan="2" align="center" style="border:1px solid #7f7f7f;">เงินต้นคงเหลือ</td>
    <?php if($invoice==1 && $pdf==0){?><td colspan="1" style="border:1px solid #7f7f7f;"></td><?php }?>
    </tr>
    <?php
	$total=0;
	$total1=0;
	$total2=0;
	$total3=0;
	$fdate=0;
    $total_pay =0;
    $total_pay_interest = 0;
    $total_interest = 0;
    $total_principal =0;
	//echo $fdate;
 
	if(isset($schedule) && $schedule !="")
    {
        $i=1;
        0;
	    foreach($schedule as $key=>$val)
	    {
 
		    ?><tr> 
		    <td colspan="1" style="font-sizne: 10px; text-align:center;border:1px solid #7f7f7f;"><?php echo number_format($i);?></td>
            <td colspan="1" align="center" style="border:1px solid #7f7f7f;"><?php echo number_format($val['leasing_schedule_pay_number']);?></td>
    	    <td colspan="2" align="center" style="border:1px solid #7f7f7f;"><?php  echo thai_display_date_short($val['leasing_schedule_date_payout'])?></td>
    	    <td colspan="1" align="center" style="border:1px solid #7f7f7f;"><?php 
               
            if($i==1){
               echo dateDiff($data['leasing_date_org'],$val['leasing_schedule_date_payout']); 
            }else{
               echo dateDiff($schedule[$key-1]['leasing_schedule_date_payout'],$val['leasing_schedule_date_payout']);        
            }
             ?>
		   </td>
    	    <td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo number_format($data['leasing_pay_amount_principal']-$total_pay,2);?></td>
    	    <td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo $val['leasing_schedule_interest_rate']."%";?></td>
    	    <td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo number_format(($val['leasing_schedule_pay_interest']+$val['leasing_schedule_money_pay']),2);?></td>
    	    <td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo number_format($val['leasing_schedule_money_pay'],2); ?></td>
    	    <td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;" width="10%"> <?php echo number_format($val['leasing_schedule_pay_interest'],2);  ?></td>
            <?php  $total_pay += $val['leasing_schedule_money_pay'];    ?>
    	    <td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php  echo number_format($data['leasing_pay_amount_principal']-$total_pay,2); ?></td>
            <?php if($invoice==1 && $pdf==0){?><td colspan="1" style="text-align:center;border:1px solid #7f7f7f;"><button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="พิมพ์" data-original-title="พิมพ์" onclick="window.open('<?php echo site_url('leasing/pdf_invoice/'.$data['leasing_id'].'/'.$val['leasing_schedule_id']); ?>', '_blank')"><span class="fa fa-print"></span></button></td><?php }?>
            </tr>
            <?php
            
		    $total_principal += $val['leasing_schedule_money_pay']; 
            $total_pay_interest +=$val['leasing_schedule_pay_interest']+$val['leasing_schedule_money_pay'] ; 
            $total_interest +=$val['leasing_schedule_pay_interest'];
            $i++;
	    }
    }
	?>
	<tr>
		<td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
        <td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo number_format($total_pay_interest,2);?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo number_format($total_principal,2);?></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"><?php echo number_format($total_interest,2);?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;border:1px solid #7f7f7f;"></td>
        <?php if($invoice==1 && $pdf==0){?><td></td><?php }?>
	</tr>
    </table></center></td>
  </tr>
</table>
</center>
<br />
<center>
<?php  /* if($invoice=='0' && $pdf=='0'){ ?><a class="btn btn-default" href='<?php echo base_url()."leasing/pdf_card/".$id?>' target="_blank" />ส่งออกข้อมูลการ์ดลูกหนี้</a></center><?php } */ ?>
<br>
                         
<style>
 #show_invoice tr td{ border:1px solid #333;}
</style>


