<select name="ddlDistrict" id="ddlDistrict" data-placeholder="กรุณาเลือก" class="width100p" required>
	<option value="">กรุณาเลือก</option>
	<?php foreach ($district as $district_data) { ?>
		<option value="<?php echo $district_data['district_id']; ?>"><?php echo $district_data['district_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#ddlDistrict").select2({
		minimumResultsForSearch: -1
	});
</script>