<select name="ddlCreditorDistrict" id="ddlCreditorDistrict" data-placeholder="กรุณาเลือก" class="width100p">
	<option value="">กรุณาเลือก</option>
	<?php foreach ($district as $district_data) { ?>
		<option value="<?php echo $district_data['district_id']; ?>"><?php echo $district_data['district_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#ddlCreditorDistrict").select2({
		minimumResultsForSearch: -1
	});
</script>