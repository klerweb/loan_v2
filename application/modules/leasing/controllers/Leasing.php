<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Leasing extends MY_Controller
{
    private $title_page = 'หนังสือสัญญาเช่าซื้อที่ดิน';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('leasing_model', 'leasing');
    }

    public function index()
    {
        $data_menu['menu'] = 'leasing';
        $data_breadcrumb['menu'] = array('หนังสือสัญญาเช่าซื้อที่ดิน' =>'#');


        $contract_no = '';
        $organization = '';
        if (!empty($_POST['contract_no'])) {
            $contract_no = $_POST['contract_no'];
        }
        if (!empty($_POST['organization'])) {
            $organization = $_POST['organization'];
        }

        $data_content = array(
                'result' => $this->leasing->search($contract_no, $organization),
        );

        $data = array(
                'title' => $this->title_page,
                'js_other' => array('modules_leasing/leasing.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('leasing', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function form($id='')
    {
        $this->load->model('title/title_model', 'title');
        $this->load->model('province/province_model', 'province');
        $this->load->model('land/employee_model', 'employee');
        $this->load->model('land/land_model', 'land');
        $this->load->model('land_type/land_type_model', 'land_type');

        unset($_SESSION['leasing']);
        unset($_SESSION['leasing_cal']);
        unset($_SESSION['leasing_list_cal']);



        $data_menu['menu'] = 'leasing';
        $data_breadcrumb['menu'] = array(
                'เช่าซื้อ' =>'#',
                $this->title_page => site_url('leasing')
            );

        $data_content = array(
                'title' => $this->title->all(),
                'province' => $this->province->all(),
                'amphur' => array(),
                'district' => array(),
                'amphur_creditor' => array(),
                'district_creditor' => array(),
                'employee' => $this->employee->getall(),
                'position' => $this->employee->positionall(),
                'leasing_length'=>$this->leasing->getLeasing_length(),
                'title' => $this->title->all(),
                'land_type' => $this->land_type->all(),
            );

        if ($id=='') {
            $title_page = 'เพิ่มแบบบันทึก';

            $data_content['id'] = '';
            $data_content['data'] = $this->leasing->getModel();
        } else {
            $this->load->model('leasing_model', 'leasing');
            $data_content['data'] = $this->leasing->getById($id);


            if (empty($data_content['data'])) {
                $title_page = 'เพิ่มแบบบันทึก';

                $data_content['id'] = '';
                $data_content['data'] = $this->leasing->getModel();
            } else {
                $this->load->model('amphur/amphur_model', 'amphur');
                $this->load->model('district/district_model', 'district');
                $this->load->model('organization_model', 'organization');
                $this->load->model('leasing_cal_model', 'leasing_cal');
                $this->load->model('leasing_schedule_model', 'leasing_schedule');

                $title_page = 'แก้ไขแบบบันทึก';



                $data_content['id'] = $id;

                $data_content['data']['organization']  = $this->organization->getById($data_content['data']['org_id']);
                $data_content['data']['land'] = $this->land->getByLeasingId($id);
                $data_content['amphur'] = $this->amphur->getByProvince($data_content['data']['organization']['org_addr_province_id']);
                $data_content['district'] = $this->district->getByAmphur($data_content['data']['organization']['org_addr_amphur_id']);


                $leasing_cal =   $this->leasing_cal->getByLeasing($id);

                if (!empty($leasing_cal)) {
                    $_SESSION['leasing'] = $leasing_cal;
                }

                $schedule = $this->leasing_schedule->getByLeasing($id);

                if (!empty($schedule)) {
                    $list_round =  array();
                    foreach ($schedule as $list_schedule) {
                        $list_round[$list_schedule['leasing_schedule_pay_number']] = array(
                                'principle' => floatval($list_schedule['leasing_schedule_money_pay']),
                                'interest_other' => intval($list_schedule['leasing_schedule_pay_interest'])
                        );
                    }

                    $_SESSION['leasing_cal'] = $list_round;
                }


                /*if(!empty($data_content['data']['creditor']['person_addr_card_province_id']))
                {
                    $data_content['amphur_creditor'] = $this->amphur->getByProvince($data_content['data']['creditor']['person_addr_card_province_id']);
                } */
            }
        }



        $data = array(
                'title' => $title_page,
                'js_other' => array('modules_leasing/form.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('form', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function person_search()
    {
        $this->load->model('person/person_model', 'person');

        $q = $this->input->get('q');
        $data = $this->person->search($q);

        echo json_encode($data);
    }

    public function amphur($province='')
    {
        if ($province=='') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');

            $data['amphur'] = $this->amphur->getByProvince($province);
        }

        $this->parser->parse('amphur', $data);
    }

    public function district($amphur='')
    {
        if ($amphur=='') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');

            $data['district'] = $this->district->getByAmphur($amphur);
        }

        $this->parser->parse('district', $data);
    }

    public function amphur_creditor($province='')
    {
        if ($province=='') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');

            $data['amphur'] = $this->amphur->getByProvince($province);
        }

        $this->parser->parse('amphur_creditor', $data);
    }

    public function district_creditor($amphur='')
    {
        if ($amphur=='') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');

            $data['district'] = $this->district->getByAmphur($amphur);
        }

        $this->parser->parse('district_creditor', $data);
    }

    public function save()
    {
        $id = $this->leasing->save($this->input->post(), $this->input->post('txtId'));

        if ($this->input->post('txtId')=='') {
            $data = array(
                    'alert' => 'บันทึกแบบบันทึกเรียบร้อยแล้ว',
                    'link' => site_url('leasing'),
            );
        } else {
            $data = array(
                    'alert' => 'แก้ไขแบบบันทึกเรียบร้อยแล้ว',
                    'link' => site_url('leasing'),
            );
        }

        $this->parser->parse('redirect', $data);
    }

    public function save_leasingLand()
    {
        $data=$_POST;
        $this->load->view('land', $data);
    }

    public function delete_land($land_id)
    {
        $this->load->model('land/land_model', 'land');
        $this->land->del($id);

        $data = array(
                'alert' => 'ยกเลิกแบบบันทึกเรียบร้อยแล้ว',
                'link' => site_url('non_loan'),
        );

        $this->parser->parse('redirect', $data);
    }

    public function del($leasing_id)
    {
        $this->leasing->del($leasing_id);

        $data = array(
                'alert' => 'ยกเลิกแบบบันทึกเรียบร้อยแล้ว',
                'link' => site_url('leasing'),
        );

        $this->parser->parse('redirect', $data);
    }

    public function report_leasing($leasing_id)
    {
        include_once('word/class/tbs_class.php');
        include_once('word/class/tbs_plugin_opentbs.php');

        $this->load->model('title/title_model', 'title');
        $this->load->model('province/province_model', 'province');
        $this->load->model('land/employee_model', 'employee');
        $this->load->model('land/land_model', 'land');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->model('organization_model', 'organization');
        $this->load->helper('loan_helper');



        $leasing_id = $this->uri->segment(3);

        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $leasing_model['oraganization']="";

        $leasing_model = $this->leasing->getById($leasing_id);
        if ($leasing_model['org_id'] !=0) {
            $leasing_model['organization'] = $this->organization->getById($leasing_model['org_id']);
        }
        $leasing_model['land'] = $this->land->getByLeasingId($leasing_model['leasing_id']);
        $director_name = "";
        $attorney_name = "";
        $position_name = "";
        $title = "" ;
        $leasing_position = "";
        $registar1="";


        // echo "<pre>"; print_r($leasing_model); echo "</pre>"; exit();
        if ($leasing_model['leasing_director_id'] !=0) {
            $director = $this->employee->getEmployeeByid($leasing_model['leasing_director_id']);
            $director_name = $director['employee_fname']." ".$director['employee_lname'] ;
        }
        if ($leasing_model['leasing_attorney_id'] !=0) {
            $attorney = $this->employee->getEmployeeByid($leasing_model['leasing_attorney_id']);
            if ($attorney['title_id'] != 0) {
                $title = $this->title->getById($attorney['title_id']);
            }
            $attorney_name = $title['title_name']." ".$attorney['employee_fname']." ".$attorney['employee_lname'] ;
        }
        if ($leasing_model['leasing_position_id'] !=0) {
            $position = $this->employee->getPositionByid($leasing_model['leasing_position_id']);
            $position_name = $position['position_name'];
        }

        if ($leasing_model['leasing_registrar_title_id'] == 0) {
            $registar_title =  $this->title->getById($leasing_model['leasing_registrar_title_id']);
            $registar1 = $registar_title['title_name']."  ".$leasing_model['leasing_registrar_name']." ".$leasing_model['leasing_registrar_surname'];
        }



        $GLOBALS['leasing_contract_no'] = view_value(num2Thai($leasing_model['leasing_contract_no']));
        $GLOBALS['leasing_location'] = $leasing_model['leasing_location'];
        $GLOBALS['leasing_date'] = view_value(num2Thai($leasing_model['leasing_datefull']));
        $GLOBALS['leasing_director'] = view_value($director_name);
        $GLOBALS['leasing_attorney'] = view_value($attorney_name);
        $GLOBALS['leasing_position'] = view_value($position_name);
        $GLOBALS['leasing_attorney_date'] = view_value(num2Thai($leasing_model['leasing_attorney_datefull']));
        $GLOBALS['org_name'] = view_value($leasing_model['organization']['org_name']);
        $GLOBALS['org_addr_no'] = view_value(num2Thai($leasing_model['organization']['org_addr_no']));
        $GLOBALS['org_addr_moo'] = view_value(num2Thai($leasing_model['organization']['org_addr_moo']));
        $GLOBALS['org_addr_district'] = view_value($leasing_model['organization']['org_district_name']);
        $GLOBALS['org_addr_amphur'] = view_value($leasing_model['organization']['org_amphur_name']);
        $GLOBALS['org_addr_province'] = view_value($leasing_model['organization']['org_province_name']);
        $GLOBALS['leasing_registrar1'] = view_value($leasing_model['leasing_registrar1']);
        $GLOBALS['leasing_registrar1_no'] = view_value(num2Thai($leasing_model['leasing_registrar1_no']));
        $GLOBALS['leasing_registrar1_date'] = view_value(num2Thai($leasing_model['leasing_registrar1_datefull']));
        $GLOBALS['leasing_registrar_name'] = view_value(num2Thai($registar1));
        $GLOBALS['leasing_registrar_position'] = view_value($leasing_model['leasing_registrar_position']);
        $GLOBALS['leasing_authorities'] = view_value(num2Thai($leasing_model['leasing_authorities']));
        $GLOBALS['leasing_registrar2'] = view_value(num2Thai($leasing_model['leasing_registrar2']));
        $GLOBALS['leasing_registrar2_no'] = view_value(num2Thai($leasing_model['leasing_registrar2_no']));
        $GLOBALS['leasing_registrar2_date'] = view_value(num2Thai($leasing_model['leasing_registrar2_datefull']));
        $GLOBALS['leasing_rules'] = view_value(num2Thai($leasing_model['leasing_rules']));
        $GLOBALS['leasing_board_resolutions'] = view_value(num2Thai($leasing_model['leasing_board_resolutions']));
        $GLOBALS['leasing_area_amount'] = view_value(num2Thai($leasing_model['leasing_area_amount']));
        $GLOBALS['leasing_area_rai'] = view_value(num2Thai(number_format($leasing_model['leasing_area_rai'], 0)));
        $GLOBALS['leasing_area_ngan'] = view_value(num2Thai($leasing_model['leasing_area_ngan']));
        $GLOBALS['leasing_area_wah'] = view_value(num2Thai($leasing_model['leasing_area_wah']));
        $GLOBALS['leasing_detail'] = view_value(num2Thai($leasing_model['leasing_detail']));
        $GLOBALS['leasing_effective_date'] = view_value(num2Thai($leasing_model['leasing_effective_datefull']));
        $GLOBALS['leasing_amount'] = view_value(num2Thai(number_format($leasing_model['leasing_amount'], 2)));
        $GLOBALS['leasing_pay_amount'] = view_value(num2Thai(number_format($leasing_model['leasing_pay_amount'], 2)));
        $GLOBALS['leasing_pay_amount_principal'] = view_value(num2Thai(number_format($leasing_model['leasing_pay_principal'], 2)));
        $GLOBALS['leasing_pay_amount_interest'] = view_value(num2Thai(number_format($leasing_model['leasing_pay_interest'], 2)));
        $GLOBALS['leasing_pay_amount_date'] = view_value(num2Thai($leasing_model['leasing_pay_amount_datefull']));
        $GLOBALS['leasing_length'] = view_value($leasing_model['leasing_length_name']);
        $GLOBALS['leasing_periods'] = view_value(num2Thai($leasing_model['leasing_periods']));
        $GLOBALS['leasing_payment_period'] = view_value(num2Thai(number_format($leasing_model['leasing_payment_period'])));
        $GLOBALS['leasing_interest'] = view_value(num2Thai(number_format($leasing_model['leasing_interest'])));
        $GLOBALS['leasing_from_date'] = view_value(num2Thai($leasing_model['leasing_from_datefull']));
        $GLOBALS['leasing_to_date'] = view_value(num2Thai($leasing_model['leasing_to_datefull']));
        $GLOBALS['leasing_rate'] = view_value(num2Thai(number_format($leasing_model['leasing_rate'])));
        $GLOBALS['leasing_rate'] = view_value(num2Thai(number_format($leasing_model['leasing_rate'])));

        // land

        $template = 'word/template/15_leasing.docx';
        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).


        $datax = array();
        $i=1;
        if ($leasing_model['land'] && $leasing_model['land']!="") {
            foreach ($leasing_model['land']  as $val) {
                $datax[$i][] = array(
                            //'no'=> num2Thai('1.'.(++$i)),
                            'land_type'=> view_value(num2Thai($val['land_type_name'])),
                            'land_no'=> view_value(num2Thai($val['land_no'])),
                            'addr_no'=> view_value(num2Thai($val['land_addr_no'])),
                            'survey_page'=> view_value(num2Thai($val['land_survey_page'])),
                            'addr_moo'=>view_value(num2Thai($val['land_addr_moo'])),
                            'addr_district_name'=>view_value($val['land_district_name']),
                            'amphur_name'=>view_value($val['land_amphur_name']),
                            'province_name'=>view_value($val['land_province_name']),
                            'area_rai'=>num2Thai($val['land_area_rai']),
                            'area_ngan'=>num2Thai($val['land_area_ngan']),
                            'area_wah'=>num2Thai($val['land_area_wah']),
                           );
                $i++;
            }

            $TBS->MergeBlock('leasing_land1', $datax[1]);
        }

        for ($row=2;$row<=5;$row++) {
            if (!isset($datax[$row])) {
                $datax[$row][] = array(
                            'land_type'=> "",
                            'land_no'=>"",
                            'addr_no'=> "",
                            'survey_page'=> "",
                            'addr_moo'=>"",
                            'addr_district_name'=>"",
                            'amphur_name'=>"",
                            'province_name'=>"",
                            'area_rai'=>"",
                            'area_ngan'=>"",
                            'area_wah'=>"",
                           );
            }

            $TBS->MergeBlock('leasing_land'.$row, $datax[$row]);
        }

        $TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
        $output_file_name = 'leasing_'.date('Y-m-d').'.docx';
        //  $TBS->Show(OPENTBS_DOWNLOAD, $output_file_name);
        //  exit();

        $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
        $TBS->Show(OPENTBS_FILE, $temp_file);
        $this->send_download($temp_file, $output_file_name);
    }

    public function send_download($temp_file, $file)
    {
        $basename = basename($file);
        $length   = sprintf("%u", filesize($temp_file));

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $basename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $length);
        ob_clean();
        flush();
        set_time_limit(0);
        readfile($temp_file);
        exit();
    }

    public function report_leasing_end($leasing_id)
    {
        include_once('word/class/tbs_class.php');
        include_once('word/class/tbs_plugin_opentbs.php');


        $this->load->model('land/land_model', 'land');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->helper('loan_helper');
        $GLOBALS['leasing_header'] = 'บัญชีแนบท้าย';


        $leasing_id = $this->uri->segment(3);
        $leasing_model = $this->leasing->getById($leasing_id);

        $leasing_model['land'] = $this->land->getByLeasingId($leasing_model['leasing_id']);

        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);



        // land

        $template = 'word/template/16_leasing_end.docx';
        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).


        $datax = array();
        $i=1;
        if ($leasing_model['land'] && $leasing_model['land']!="") {
            foreach ($leasing_model['land']  as $val) {
                $datax[$i][] = array(
                            //'no'=> num2Thai('1.'.(++$i)),
                            'land_type'=> view_value(num2Thai($val['land_type_name'])),
                            'land_no'=> view_value(num2Thai($val['land_no'])),
                            'addr_no'=> view_value(num2Thai($val['land_addr_no'])),
                            'survey_page'=> view_value(num2Thai($val['land_survey_page'])),
                            'addr_moo'=>view_value(num2Thai($val['land_addr_moo'])),
                            'addr_district_name'=>view_value($val['land_district_name']),
                            'amphur_name'=>view_value($val['land_amphur_name']),
                            'province_name'=>view_value($val['land_province_name']),
                            'area_rai'=>num2Thai($val['land_area_rai']),
                            'area_ngan'=>num2Thai($val['land_area_ngan']),
                            'area_wah'=>num2Thai($val['land_area_wah']),
                           );
                $i++;
            }
        }

        for ($row=6;$row<=55;$row++) {
            if (!isset($datax[$row])) {
                $datax[$row][] = array(
                            'land_type'=> "",
                            'land_no'=>"",
                            'addr_no'=> "",
                            'survey_page'=> "",
                            'addr_moo'=>"",
                            'addr_district_name'=>"",
                            'amphur_name'=>"",
                            'province_name'=>"",
                            'area_rai'=>"",
                            'area_ngan'=>"",
                            'area_wah'=>"",
                           );
            }

            $TBS->MergeBlock('leasing_land'.$row, $datax[$row]);
        }


        $TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
        $output_file_name = 'leasing_end_'.date('Y-m-d').'.docx';
        $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
        $TBS->Show(OPENTBS_FILE, $temp_file);
        $this->send_download($temp_file, $output_file_name);
    }

    public function cal_leasing()
    {
        $amount = floatval(str_replace(",", "", $this->input->post('amount')));
        $round = $this->input->post('round');


        $data = array(
                'round' => $round,
                'amount' => $amount
        );

        $this->parser->parse('cal_leasing', $data);
    }

    public function save_leasingCal()
    {
        $round = $this->input->post('txtCalRound');
        $amount = $this->input->post('txtCalAmount');
        $start_list = $this->input->post('txtCalStart');
        $end_list = $this->input->post('txtCalEnd');

        $list_cal = array();
        $list_round = array();
        foreach ($round as $r_key => $r_value) {
            $start = intval($start_list[$r_key]);
            $end = intval($end_list[$r_key]);

            $list_cal[] = array(
                        'leasing_cal_time' => $r_value,
                        'leasing_cal_start' => $start,
                        'leasing_cal_end' => $end,
                        'leasing_cal_amount' => $amount[$r_key]
                    );

            for ($r = $start; $r <= $end; $r++) {
                $list_round[$r] = $_SESSION['leasing_cal'][$r];
                $list_round[$r]['principle'] = $amount[$r_key];
            }
        }

        $_SESSION['leasing_list_cal'] = $list_cal;
        $_SESSION['leasing_cal'] = $list_round;
    }

    public function save_cal()
    {
        $round = $this->input->post('txtCalRound');
        $amount = $this->input->post('txtCalAmount');
        $start_list = $this->input->post('txtCalStart');
        $end_list = $this->input->post('txtCalEnd');

        $list_cal = array();
        $list_round = array();
        foreach ($round as $r_key => $r_value) {
            $start = intval($start_list[$r_key]);
            $end = intval($end_list[$r_key]);

            $list_cal[] = array(
                        'leasing_cal_time' => $r_value,
                        'leasing_cal_start' => $start,
                        'leasing_cal_end' => $end,
                        'leasing_cal_amount' => $amount[$r_key]
                    );

            for ($r = $start; $r <= $end; $r++) {
                $list_round[$r] = $_SESSION['leasing_cal'][$r];
                $list_round[$r]['principle'] = $amount[$r_key];
            }
        }

        $_SESSION['leasing_list_cal'] = $list_cal;
        $_SESSION['leasing_cal'] = $list_round;
    }

    public function cal()
    {
        $income = floatval(str_replace(",", "", $this->input->post('income')));
        $amount = floatval(str_replace(",", "", $this->input->post('amount')));
        $interest = intval($this->input->post('interest')) / 100;
        $type = intval($this->input->post('type'));
        $year = intval($this->input->post('year'));
        $pay = $this->input->post('pay');
        $start = $this->input->post('start');

        $start = $start? convert_ymd_en($start):"" ;
        $pay = $pay ? convert_ymd_en($pay): "" ;

        $date_start = $start;
        $date_start = new DateTime($date_start);
        $date_start->sub(new DateInterval('P1D'));
        $date_start = $date_start->format('Y-m-d');
        $date_end = $pay;

        $list_round = array();
        $list_month = array();
        $remain = $amount;
        if (!empty($_SESSION['leasing_cal'])) {
            $i_key = 1;
            foreach ($_SESSION['leasing_cal'] as $r_key => $r_value) {
                $day_between = cal_day_between($date_start, $date_end);
                if ($i_key==1) {
                    $day_between--;
                }

                $interest_per = number_format(($remain * $interest)  * ($day_between / 365), 2, '.', '');

                if ($interest_per < 0) {
                    $interest_per = 0;
                }

                $list_round[$r_key] = array(
                        'income' => $income,
                        'principle' => $r_value['principle'],
                        'remain' => $remain,
                        'interest' => $interest_per,
                        'date' => $date_end
                );

                list($y_end, $m_end, $d_end) = explode('-', $date_end);
                $list_month[] = $m_end;

                $remain -= $r_value['principle'];
                $date_start = $date_end;
                $date_end = new DateTime($date_end);
                $date_end->add(new DateInterval('P'.$type.'M'));
                $date_end = $date_end->format('Y-m-d');

                $i_key++;
            }
        }

        $list_month = array_unique($list_month);

        $month = '';
        foreach ($list_month as $data_month) {
            $month .= thai_month($data_month).', ';
        }

        if (!empty($month)) {
            $month = substr($month, 0, -2);
        }

        $data = array(
            'year' => $year,
            'month' => $month,
            'income' => $income,
            'amount' => $amount,
            'interest' => $interest,
            'type' => $type,
            'pay' => $pay,
            'start' => $start,
            'fiscal_start' => fiscalYear($start),
            'fiscal_end' => fiscalYear($date_start),
            'list_round' => $list_round
        );

        $_SESSION['leasing'] = $data;
        /*unset($_SESSION['leasing']);
        unset($_SESSION['leasing_list_cal']);
        unset($_SESSION['leasing_cal']);    */
        if (isset($_SESSION['leasing_cal'])) {
            $this->parser->parse('cal', $data);
        }
    }

    public function showSession()
    {
        echo "<pre>";
        print_r($_SESSION);
        echo "</pre>";
    }

    public function report_invoice()
    {
        $data_menu['menu'] = 'leasing';
        $data_breadcrumb['menu'] = array(
                'เช่าซื้อ' =>base_url()."leasing",
                'การ์ดลูกหนี้' => '#'
            );


        $contract_no = '';
        $organization = '';
        if (!empty($_POST['contract_no'])) {
            $contract_no = $_POST['contract_no'];
        }
        if (!empty($_POST['organization'])) {
            $organization = $_POST['organization'];
        }

        $data_content = array(
                'result' => $this->leasing->search($contract_no, $organization),
        );

        $data = array(
                'title' => 'การ์ดลูกหนี้',
                'js_other' => array('modules_leasing/leasing.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('report_invoice', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function report_invoice_show($id='', $invoice='0')
    {
        $this->load->model('title/title_model', 'title');
        $this->load->model('province/province_model', 'province');
        $this->load->model('land/employee_model', 'employee');
        $this->load->model('land/land_model', 'land');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->model('leasing_schedule_model', 'leasing_schedule');
        $this->load->model('organization_model', 'organization');
        $this->load->model('leasing_cal_model', 'leasing_cal');

        $data_menu['menu'] = 'leasing';
        $data_breadcrumb['menu'] = array(
                'เช่าซื้อ' =>'#',
                'การ์ดลูกหนี้'=> site_url('leasing/report_invoice')
            );

        $data_content = array(
                 'id'=>$id,
                 'data'=>$this->leasing->getById($id),
                 'schedule'=>$this->leasing_schedule->getByLeasing($id)
            );
        $data_content['organization']  = $this->organization->getById($data_content['data']['org_id']);
        $data_content['leasing_cal']  = $this->leasing_cal->getByLeasing($id);
        $data_content['pdf']  = 0;
        $data_content['invoice']  = $invoice;
        if ($invoice) {
            $data_breadcrumb['menu'] = array(
                'เช่าซื้อ' =>'#',
                'ใบแจ้งหนี้'=> site_url('leasing/report_invoice2')
            );
        }


        $data = array(
                'title' => 'การ์ดลูกหนี้',
                'js_other' => array('modules_leasing/form.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('show_invoice', $data_content, true)
        );
        if ($invoice) {
            $data['title'] = 'ใบแจ้งหนี้' ;
        }


        $this->parser->parse('main', $data);
    }

    public function pdf_card($id = '')
    {
        $this->load->model('title/title_model', 'title');
        $this->load->model('province/province_model', 'province');
        $this->load->model('land/employee_model', 'employee');
        $this->load->model('land/land_model', 'land');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->model('leasing_schedule_model', 'leasing_schedule');
        $this->load->model('organization_model', 'organization');
        $this->load->model('leasing_cal_model', 'leasing_cal');

        $data_content = array(
                 'id'=>$id,
                 'data'=>$this->leasing->getById($id),
                 'schedule'=>$this->leasing_schedule->getByLeasing($id)
            );
        $data_content['organization']  = $this->organization->getById($data_content['data']['org_id']);
        $data_content['leasing_cal']  = $this->leasing_cal->getByLeasing($id);
        $data_content['pdf']  = 1;
        $data_content['invoice']  = 0;


        ob_start();
        $this->load->library('tcpdf');


        $filename = 'leasing_card_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->Settitle($filename); //  กำหนด title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->AddPage();


        $htmlcontent  =  $this->load->view('show_invoice', $data_content, true);

        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        $pdf->Output($filename . '.pdf', 'I');
    }

    public function report_invoice2()
    {
        $data_menu['menu'] = 'leasing';
        $data_breadcrumb['menu'] = array(
                'เช่าซื้อ' =>base_url()."leasing",
                'ใบแจ้งหนี้' => '#'
            );


        $contract_no = '';
        $organization = '';
        if (!empty($_POST['contract_no'])) {
            $contract_no = $_POST['contract_no'];
        }
        if (!empty($_POST['organization'])) {
            $organization = $_POST['organization'];
        }

        $data_content = array(
                'result' => $this->leasing->search($contract_no, $organization),
        );

        $data = array(
                'title' => 'ใบแจ้งหนี้',
                'js_other' => array('modules_leasing/leasing.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('report_invoice2', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function pdf_invoice($id = '', $schedule_id='')
    {
        $this->load->model('land/land_model', 'land');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->model('leasing_schedule_model', 'leasing_schedule');
        $this->load->model('organization_model', 'organization');
        $this->load->model('leasing_cal_model', 'leasing_cal');

        $data_content = array(
                 'id'=>$id,
                 'data'=>$this->leasing->getById($id),
                 'schedule_all'=>$this->leasing_schedule->getByLeasing($id),
                 'schedule'=>$this->leasing_schedule->getById($schedule_id)
            );
        $data_content['organization']  = $this->organization->getById($data_content['data']['org_id']);
        $data_content['leasing_cal']  = $this->leasing_cal->getByLeasing($id);


        /*echo "<pre>"; print_r($data_content); echo "</pre>";


        exit(); */


        ob_start();
        $this->load->library('tcpdf');


        $filename = 'leasing_invoice_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->Settitle($filename); //  กำหนด title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->AddPage();


        $htmlcontent  =  $this->load->view('pdf_invoice', $data_content, true);
        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');
        $pdf->Output($filename . '.pdf', 'I');
    }
}
