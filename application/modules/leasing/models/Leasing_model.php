<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leasing_model extends CI_Model
{
	private $table = 'leasing';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
        $this->_active = '1';
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("leasing_status !=", "0");
		$this->db->where("leasing_createby", get_uid_login());
		$this->db->order_by("leasing_id", "desc");
		$query = $this->db->get();
		
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function search($contract_no='', $organization='')
	{
		$where = '';
		if($contract_no!='') $where .= "OR leasing.leasing_contract_no  LIKE '%".$contract_no."%' ";
		if($organization!='') $where .= "OR organization.org_name LIKE '%".$organization."%' ";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("organization", "leasing.org_id = organization.org_id", "LEFT");                                           
		$this->db->where("leasing.leasing_status !=", "0");
        
        if(check_permission_isowner('leasing')){
              $this->db->where("leasing.leasing_createby", get_uid_login());  
        }  
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);     
		
		$this->db->order_by("leasing.leasing_id", "desc");
		
		$query = $this->db->get();
        $data = $query->num_rows()!=0? $query->result_array() : array();     
        $this->load->model('land/land_model', 'land');        
        if(count($data)>0){
            foreach($data as $key=>$row){
                $list_land = $this->land->getByLeasingIdCount($row['leasing_id']);   
                 
                if($list_land>5) {
                    $data[$key]['land_end'] = true;
                }else{
                     $data[$key]['land_end'] = false;
                }
            }
        }
		return $data;
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("leasing_id", $id);
		$this->db->where("leasing_status !=", "0");
        
        if(check_permission_isowner('leasing')){    
		    $this->db->where("leasing_createby", get_uid_login());
        }
		$query = $this->db->get();
		
		if($query->num_rows()!=0)
		{
			$this->load->model('person/person_model', 'person');
			$this->load->model('title/title_model', 'title');
			$this->load->model('district/district_model', 'district');
			$this->load->model('amphur/amphur_model', 'amphur');
			$this->load->model('province/province_model', 'province');
			
			$result = $query->row_array();
			
			$this->load->helper('date');
               
            $result['leasing_datefull'] = thai_display_date($result['leasing_date']);  
            $result['leasing_date_org'] = $result['leasing_date'];   
            $result['leasing_date'] = convert_dmy_th($result['leasing_date']);   
            $result['leasing_attorney_datefull'] = thai_display_date($result['leasing_attorney_date']);  
            $result['leasing_attorney_date'] = convert_dmy_th($result['leasing_attorney_date']);   
            $result['leasing_registrar1_datefull'] = thai_display_date($result['leasing_registrar1_date']); 
            $result['leasing_registrar1_date'] = convert_dmy_th($result['leasing_registrar1_date']);    
            $result['leasing_registrar2_datefull'] = thai_display_date($result['leasing_registrar2_date']);   
            $result['leasing_registrar2_date'] = convert_dmy_th($result['leasing_registrar2_date']);    
            $result['leasing_effective_datefull'] = thai_display_date($result['leasing_effective_date']);     
            $result['leasing_effective_date'] = convert_dmy_th($result['leasing_effective_date']);    
            $result['leasing_from_datefull'] = thai_display_date($result['leasing_from_date']);   
            $result['leasing_from_date'] = convert_dmy_th($result['leasing_from_date']);    
            $result['leasing_to_datefull'] = thai_display_date($result['leasing_to_date']);    
            $result['leasing_to_date_org'] = $result['leasing_to_date'];    
            $result['leasing_to_date'] = convert_dmy_th($result['leasing_to_date']);    
            $result['leasing_pay_amount_datefull'] = thai_display_date($result['leasing_pay_amount_date']);   
            $result['leasing_pay_amount_date'] = convert_dmy_th($result['leasing_pay_amount_date']);    
               
			$result['leasing_length_name'] = $this->getLeasing_length($result['leasing_length']);    
            
 
		}
		else
		{
			$result = null;
		}

		return $result;
	}
	
	public function save($data, $id='')
	{
        $this->load->model('land/land_model', 'land');
        $this->load->model('organization_model', 'organizaiton');
        $this->load->model('leasing_cal_model', 'leasing_cal');
		$this->load->model('leasing_schedule_model', 'leasing_schedule');
		
        $array['leasing_contract_no'] = $data['txtLeasing_contract_no'];  
        $array['leasing_location'] = $data['txtLeasing_location'];  
        $array['leasing_position_id'] = $data['ddlPosition'];  
        $array['leasing_date'] = convert_ymd_en($data['txtDate']);  
        $array['leasing_director_id'] = $data['ddlDirector'];  
        $array['leasing_attorney_id'] = $data['ddlAttorney'];  
        $array['leasing_attorney_id'] = $data['ddlAttorney'];  
        $array['leasing_attorney_date'] = convert_ymd_en($data['txtLeasing_attorney_date']);  
        $array['leasing_registrar1'] = $data['txtLeasing_registrar1'];  
        $array['leasing_registrar1_no'] = $data['txtLeasing_registrar1_no'];  
        $array['leasing_registrar1_date'] = convert_ymd_en($data['txtLeasing_registrar1_date']);  
        $array['leasing_registrar_title_id'] = $data['ddLeasing_registrar_title_id'];  
        $array['leasing_registrar_name'] = $data['txtLeasing_registrar_name'];  
        $array['leasing_registrar_surname'] = $data['txtLeasing_registrar_surname'];  
        $array['leasing_registrar_position'] = $data['txtLeasing_registrar_position'];  
        $array['leasing_authorities'] = $data['txtLeasing_authorities'];  
        $array['leasing_registrar2'] = $data['txtLeasing_registrar2'];  
        $array['leasing_registrar2_no'] = $data['txtLeasing_registrar2_no'];  
        $array['leasing_registrar2_date'] = convert_ymd_en($data['txtLeasing_registrar2_date']);  
        $array['leasing_rules'] = $data['txtLeasing_rules'];  
        $array['leasing_board_resolutions'] = $data['txtLeasing_board_resolutions'];  
        $array['leasing_area_amount'] = forNumber($data['txtLeasing_area_amount']);  
        $array['leasing_area_rai'] = $data['txtRai'];  
        $array['leasing_area_ngan'] = $data['txtNgan'];  
        $array['leasing_area_wah'] = $data['txtWah'];  
        $array['leasing_detail'] = $data['txtLeasing_detail'];   
        $array['leasing_effective_date'] = convert_ymd_en($data['txtLeasing_effective_date']);  
        $array['leasing_amount'] = forNumber($data['txtLeasing_amount']);  
        $array['leasing_pay_amount'] = forNumber($data['txtLeasing_pay_amount']);  
        $array['leasing_pay_amount_principal'] = forNumber($data['txtLeasing_pay_amount_principal']);  
        $array['leasing_pay_amount_interest'] = forNumber($data['txtLeasing_pay_amount_interest']);  
        $array['leasing_pay_amount_date'] = convert_ymd_en($data['txtLeasing_pay_amount_date']);  
        $array['leasing_length'] = $data['txtLeasing_length'];  
        $array['leasing_periods'] = $data['txtLeasing_periods'];  
        $array['leasing_payment_period'] = forNumber($data['txtLeasing_payment_period']);
        $array['leasing_interest'] = forNumber($data['txtLeasing_interest']);  
        $array['leasing_from_date'] = convert_ymd_en($data['txtLeasing_from_date']);  
        $array['leasing_to_date'] = convert_ymd_en($data['txtLeasing_to_date']);  
        $array['leasing_rate'] = forNumber($data['txtLeasing_rate']);  
        $array['leasing_status'] = $this->_active;  
        $array['leasing_pay_principal'] = forNumber($data['txtLeasing_pay_principal']);  
        $array['leasing_pay_interest'] = forNumber($data['txtLeasing_pay_interest']);  
        
        
        
		if($id=='')
		{
           // insert ----------------------------- oraganitation
                    
            $array_org['org_name'] = $data['txtOrganization_name'];  
            $array_org['org_addr_no'] = $data['txtOrg_addr_no'];  
            $array_org['org_addr_moo'] = $data['txtOrg_addr_moo'];  
            $array_org['org_addr_province_id'] = $data['ddlProvince'];  
            $array_org['org_addr_amphur_id'] = $data['ddlAmphur'];  
            $array_org['org_addr_district_id'] = $data['ddlDistrict'];  
            
                        
            $id_organization = $this->organizaiton->save($array_org);  
            
            $array['org_id'] = $id_organization['id'] ;
        
			$this->db->set($array);
			$this->db->set("leasing_createby", get_uid_login());
			$this->db->set("leasing_createdate", "NOW()", FALSE);
			$this->db->set("leasing_updateby", get_uid_login());
			$this->db->set("leasing_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
			
			$id = $this->db->insert_id();
            
            
             if($id){
                    // insert  ------------------ land 
 
                    if(isset($data['ddlLand_type_id']) && count( $data['ddlLand_type_id'])> 0 ){
                        foreach($data['ddlLand_type_id'] as $key=>$val){
                            $array_land['leasing_id'] = $id;
                            $array_land['land_type_id'] = $val;
                            $array_land['land_no'] = $data['txtLand_no'][$key];
                            $array_land['land_addr_no'] = $data['txtLand_addr_no'][$key];
                            $array_land['land_survey_page'] = $data['txtLand_survey_page'][$key];
                            $array_land['land_addr_moo'] = $data['txtLand_addr_moo'][$key];
                            $array_land['land_addr_province_id'] = $data['landProvince'][$key];
                            $array_land['land_addr_amphur_id'] = $data['landAmphur'][$key];
                            $array_land['land_addr_district_id'] = $data['landDistrict'][$key];
                            $array_land['land_area_rai'] = $data['leasing_area_rai'][$key];
                            $array_land['land_area_ngan'] = $data['txtLeasing_area_ngan'][$key];
                            $array_land['land_area_wah'] = $data['txtLeasing_area_wah'][$key];
                            
                             
                            $this->land->save('',$array_land);  
                        }
                    }  
                }
            
		}
		else
		{
             
			$this->db->set($array);
			$this->db->set("leasing_updateby", get_uid_login());
			$this->db->set("leasing_updatedate", "NOW()", FALSE);
			$this->db->where("leasing_id", $id);
			$this->db->update($this->table, $array);
            
            // update organization 
            
            $array_org['org_name'] = $data['txtOrganization_name'];  
            $array_org['org_addr_no'] = $data['txtOrg_addr_no'];  
            $array_org['org_addr_moo'] = $data['txtOrg_addr_moo'];  
            $array_org['org_addr_province_id'] = $data['ddlProvince'];  
            $array_org['org_addr_amphur_id'] = $data['ddlAmphur'];  
            $array_org['org_addr_district_id'] = $data['ddlDistrict'];  
            
                        
            $id_organization = $this->organizaiton->save($array_org,$data['txtOrganization_id']);  
            
            
            if($id){
                    //----------- land
                    
                    if($data['delete_land'] !=""){
                        $land_del = explode(',',$data['delete_land']);
                        if($land_del){
                            foreach($land_del as $key=>$val){
                                if($val) $this->land->delete($val);
                            }
                        }
                    }
 
                    if(isset($data['ddlLand_type_id']) && count( $data['ddlLand_type_id'])> 0 ){
                        foreach($data['ddlLand_type_id'] as $key=>$val){
                            
                            $array_land['leasing_id'] = $id;
                            $array_land['land_type_id'] = $val;
                            $array_land['land_no'] = $data['txtLand_no'][$key];
                            $array_land['land_addr_no'] = $data['txtLand_addr_no'][$key];
                            $array_land['land_survey_page'] = $data['txtLand_survey_page'][$key];
                            $array_land['land_addr_moo'] = $data['txtLand_addr_moo'][$key];
                            $array_land['land_addr_province_id'] = $data['landProvince'][$key];
                            $array_land['land_addr_amphur_id'] = $data['landAmphur'][$key];
                            $array_land['land_addr_district_id'] = $data['landDistrict'][$key];
                            $array_land['land_area_rai'] = $data['leasing_area_rai'][$key];
                            $array_land['land_area_ngan'] = $data['txtLeasing_area_ngan'][$key];
                            $array_land['land_area_wah'] = $data['txtLeasing_area_wah'][$key];
                            
                             
                            if(isset($data['land_id'][$key]) && $data['land_id'][$key] ) $this->land->save($data['land_id'][$key],$array_land);  
                            else  $this->land->save('',$array_land);  
                        }
                    }  
                }
            
		}
        
        
        
        // insert cal 
        
          if(!empty($_SESSION['leasing_list_cal']))
        {
            $this->leasing_cal->delByleasing($id);     
            foreach ($_SESSION['leasing_list_cal'] as $key => $cal)
            {
                $array_cal['leasing_id'] = $id;                 
                $array_cal['leasing_cal_time'] = $cal['leasing_cal_time'];
                $array_cal['leasing_cal_start'] = $cal['leasing_cal_start'];
                $array_cal['leasing_cal_end'] = $cal['leasing_cal_end'];
                $array_cal['leasing_cal_amount'] = $cal['leasing_cal_amount'];
                    
                $this->leasing_cal->save($array_cal);
            }
        }
        
        if(!empty($_SESSION['leasing']['list_round']))
        {   
             $this->leasing_schedule->delByleasing($id);   
            foreach ($_SESSION['leasing']['list_round'] as $key => $schedule)
            {
                $array_schedule['leasing_id'] = $id;            
                $array_schedule['leasing_schedule_pay_number'] = $key;
                $array_schedule['leasing_schedule_date_payout'] = $schedule['date'];
                $array_schedule['leasing_schedule_money_pay'] = $schedule['principle'];        
                $array_schedule['leasing_schedule_pay_interest'] = $schedule['interest'];
                $array_schedule['leasing_schedule_interest_rate'] = $array['leasing_interest'];
                    
                $this->leasing_schedule->save($array_schedule);
            }
        }
        
        UNSET($_SESSION['leasing']);
        UNSET($_SESSION['leasing_list_cal']);
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("leasing_status" => "0");
		
		$this->db->set($array);
		$this->db->where("leasing_id", $id);
		$this->db->update($this->table, $array);
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function getModel()
	{
		$data['leasing_date'] = date('d/m/').(intval(date('Y'))+543);
		$data['leasing_location'] = 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)';
        $data['land_type_id'] = '';
        $data['leasing_director_id'] = 0;
        $data['leasing_attorney_id'] = 0;
        $data['leasing_position_id'] = 0;
        $data['leasing_registrar1_no'] = 0;
        $data['leasing_registrar_title_id'] = 0;
        $data['leasing_registrar2_no'] = 0;
        $data['leasing_area_amount'] = 0;
        $data['leasing_area_rai'] = 0;
		$data['leasing_area_ngan'] = 0;
		$data['leasing_area_wah'] = '0';
		$data['leasing_area_ngan'] = '0';
        $data['leasing_amount'] = 0;
        $data['leasing_pay_amount'] = 0;
        $data['leasing_pay_amount_principal'] = 0;
        $data['leasing_pay_amount_interest'] = 0;
        $data['leasing_length'] = 0;
        $data['leasing_periods'] = 0;
        $data['leasing_payment_period'] = 0;
        $data['leasing_interest'] = 0;         
		$data['leasing_rate'] = 0;         
		$data['leasing_status'] = '1';
        $data['organization']['org_id'] =0;
        $data['organization']['org_addr_province_id'] ="";
        $data['leasing_pay_principal'] =0;
        $data['leasing_pay_interest'] = 0;  
 
	
		return $data;
	}
    
    public function getLeasing_length($key=''){
        $array = array('1'=>'รายเดือน','6'=>'ราย 6 เดือน','12'=>'รายปี');
        if($key && isset($array[$key])) return $array[$key];
        else if($key !="" && !isset($array[$key]) ) return NULL;
        else return $array; 
    }
      
}
?>
