<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization_model extends CI_Model
{
	private $table = 'organization';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
        $this->_active = '1';
        $this->load->model('province/province_model', 'province');
        $this->load->model('amphur/amphur_model', 'amphur');
        $this->load->model('district/district_model', 'district');
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("leasing_status !=", "0");
        
        if(check_permission_isowner('leasing')){    
		    $this->db->where("leasing_createby", get_uid_login());
        }
		$this->db->order_by("leasing_id", "desc");
		$query = $this->db->get();
		
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function search($thaiid='', $fullname='')
	{
		$where = '';
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("person", "leasing.person_id = person.person_id", "RIGHT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->join("config_leasing_objective", "leasing.leasing_objective_id = config_leasing_objective.leasing_objective_id", "LEFT");
		$this->db->where("leasing.leasing_status !=", "0");
		$this->db->where("leasing.leasing_createby", get_uid_login());
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$this->db->order_by("leasing.leasing_id", "desc");
		
		$query = $this->db->get();
	
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("org_id", $id);
        $this->db->where("org_status !=", "0");
        
        if(check_permission_isowner('leasing')){    
           $this->db->where("org_createby", get_uid_login());
        }
		$query = $this->db->get();         
        
		if($query->num_rows()!=0)
		{                                                                                      
           $result = $query->row_array();    
                                                                                  
           $province_name = $this->province->getById($result['org_addr_province_id']);
           $distinct_name = $this->district->getById($result['org_addr_district_id']);
           $amphur_name = $this->amphur->getById($result['org_addr_amphur_id']); 
           
            $result['org_province_name'] = $province_name['province_name'];
            $result['org_district_name'] = $distinct_name['district_name'];
            $result['org_amphur_name'] = $amphur_name['amphur_name'];
		}
		else
		{
			$result = null;
		}
           
		return $result;
	}
	
	public function save($data, $id='')
	{
		  
        
		if($id=='')
		{
           
			$this->db->set($data);
            $this->db->set("org_status", $this->_active);
			$this->db->set("org_createby", get_uid_login());
            $this->db->set("org_createdate", "NOW()", FALSE);
            $this->db->set("org_updateby", get_uid_login());
            $this->db->set("org_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
			
			$id = $this->db->insert_id();         
		}
		else
		{
             
			$this->db->set($data);
			$this->db->set("org_updateby", get_uid_login());
            $this->db->set("org_updatedate", "NOW()", FALSE);
            $this->db->where("org_id", $id);
            $this->db->update($this->table, $data);
		}
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("org_status" => "0");
        
        $this->db->set($array);
        $this->db->where("org_id", $id);
        $this->db->update($this->table, $array);
        
        $num_row = $this->db->affected_rows();
        
        return array('id' => $id, 'rows' => $num_row);
	}
	
	public function getModel()
	{
		$data['leasing_date'] = date('d/m/').(intval(date('Y'))+543);
		$data['leasing_location'] = 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)';
		$data['person_id'] = '';
		$data['person']['person_id'] = '';
		$data['person']['title_id'] = '';
		$data['person']['person_fname'] = '';
		$data['person']['person_lname'] = '';
		$data['person']['person_thaiid'] = '';
		$data['person']['person_addr_card_no'] = '';
		$data['person']['person_addr_card_moo'] = '';
		$data['person']['person_addr_card_road'] = '';
		$data['person']['person_addr_card_district_id'] = '';
		$data['person']['person_addr_card_amphur_id'] = '';
		$data['person']['person_addr_card_province_id'] = '';
		$data['person']['person_phone'] = '';
		$data['leasing_creditor'] = '';
		$data['creditor']['person_id'] = '';
		$data['creditor']['title_id'] = '';
		$data['creditor']['person_fname'] = '';
		$data['creditor']['person_lname'] = '';
		$data['creditor']['person_thaiid'] = '';
		$data['creditor']['person_addr_card_no'] = '';
		$data['creditor']['person_addr_card_moo'] = '';
		$data['creditor']['person_addr_card_road'] = '';
		$data['creditor']['person_addr_card_district_id'] = '';
		$data['creditor']['person_addr_card_amphur_id'] = '';
		$data['creditor']['person_addr_card_province_id'] = '';
		$data['creditor']['person_phone'] = '';
		$data['leasing_objective_id'] = '';
		$data['land_type_id'] = '';
		$data['leasing_area_rai'] = '0';
		$data['leasing_area_ngan'] = '0';
		$data['leasing_area_wah'] = '0';
		$data['leasing_case_id'] = '';
		$data['leasing_cause_other'] = '';
		$data['leasing_status'] = '1';
	
		return $data;
	}
}
?>