<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leasing_schedule_model extends CI_Model
{
	private $table = 'leasing_schedule';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
    
	 public function getById($id)
    {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where("leasing_schedule_id", $id); 
        if(check_permission_isowner('leasing')){       
            $this->db->where("leasing_schedule_createby", get_uid_login());
        }
        $query = $this->db->get();
        
        
        echo $this->db->last_query();
        
        if($query->num_rows()!=0)
        {
             
            $result = $query->row_array();     
        }
        else
        {
            $result = null;
        }

        return $result;
    }
    
	public function getByleasing($leasing_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("leasing_id", $leasing_id);
		$this->db->order_by("leasing_schedule_pay_number", "asc");
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
 
	public function save($array, $id='')
	{
		if($id=='')
		{
            
			$this->db->set($array);    
            $this->db->set("leasing_schedule_createby", get_uid_login());
			$this->db->set("leasing_schedule_createdate", "NOW()", FALSE);
			$this->db->set("leasing_schedule_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
				
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
            $this->db->set("leasing_schedule_updateby", get_uid_login()); 
			$this->db->set("leasing_schedule_updatedate", "NOW()", FALSE);
			$this->db->where("leasing_schedule_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function delByleasing($leasing_id)
	{
		$this->db->where("leasing_id", $leasing_id);
		$this->db->delete($this->table);
	}
}
?>