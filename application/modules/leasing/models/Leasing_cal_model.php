<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Leasing_cal_model extends CI_Model
{
	private $table = 'leasing_cal';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLeasing($leasing_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("leasing_id", $leasing_id);            
		$this->db->order_by("leasing_cal_time", "asc");
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function save($array, $id='')
	{
		if($id=='')
		{
			 
            $this->db->set($array);
			$this->db->set("leasing_cal_createdate", "NOW()", FALSE);
			$this->db->insert($this->table);
				
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("leasing_cal_createdate", "NOW()", FALSE);
			$this->db->where("leasing_cal_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function delByLeasing($leasing_id)
	{
		$this->db->where("leasing_id", $leasing_id);
		$this->db->delete($this->table);
	}
}
?>