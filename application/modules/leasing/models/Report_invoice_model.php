<?php       
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_invoice_model extends CI_Model
{
    private $table_leasing = 'leasing';
    private $table_leasing_schedule = 'leasing_schedule';
    
    function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);      
           
        $this->_active = '1';
    }
    function load_schedule($id=''){
        $sql ='SELECT * FROM leasing_schedule Inner Join leasing ON leasing.leasing_id = leasing_schedule.leasing_id WHERE  leasing_schedule.leasing_id='.$id.'  order by leasing_schedule_pay_number asc  ';
        return $this->db->query($sql)->result();
        
    }
}
?>
