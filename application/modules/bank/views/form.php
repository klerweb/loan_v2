<form id="frmBank" name="frmBank" action="<?php echo base_url().'bank/save'; ?>" method="post">
	<input type="hidden" id="txtBankId" name="txtBankId" value="<?php echo $id; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-bank">ธนาคาร</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">ธนาคาร <span class="asterisk">*</span></label>
								<input type="text" name="txtBankName" id="txtBankName" class="form-control" value="<?php echo $data['bank_name']; ?>" maxlength="100" required />
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>