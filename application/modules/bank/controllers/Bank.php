<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MY_Controller
{
	private $title_page = 'ธนาคาร';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('bank_model', 'bank');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->bank->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_bank/bank.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('bank', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('bank')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มธนาคาร';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('bank_model', 'bank');
			$data_content['data'] = $this->bank->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มธนาคาร';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขธนาคาร';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_bank/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('bank_model', 'bank');
		
		$array['bank_name'] = $this->input->post('txtBankName');
		$array['bank_status'] = '1';
		
		$id = $this->bank->save($array, $this->input->post('txtBankId'));
		
		if($this->input->post('txtBankId')=='')
		{
			$data = array(
					'alert' => 'บันทึกธนาคารเรียบร้อยแล้ว',
					'link' => site_url('bank'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขธนาคารเรียบร้อยแล้ว',
					'link' => site_url('bank'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('bank_model', 'bank');
		$this->bank->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกธนาคารเรียบร้อยแล้ว',
				'link' => site_url('bank'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['bank_name'] = '';
		
		return $data;
	}
}