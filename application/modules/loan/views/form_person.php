<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/edit_loan/{$loan_data['loan_id']}"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_land"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>


        <form id="frm_person" name="form_person" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_person/save"); ?>">
            <input type="hidden" name="person_id" id="person_id" value="<?php echo $loan_model['person_id']; ?>">
            <input type="hidden" name="spouse_id" id="spouse_id" value="<?php echo $loan_model['loan_spouse']; ?>">
            <div class="row">
            	<div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เขียนที่ <span class="asterisk">*</span></label>
                        <input type="text" name="loan_location" id="loan_location" placeholder="เขียนที่" class="form-control" value="<?php echo $loan_model['loan_location'] ?>" required>
                    </div>
                </div>
			</div>
			<div class="row">
				<div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ปีงบประมาณ <span class="asterisk">*</span></label>
						<select name="loan_year" id="loan_year" data-placeholder="กรุณาเลือก" class="width100p" required>
							<option value="">กรุณาเลือก</option>
							<?php
								for ($y = 2017; $y <= intval(date('Y')); $y++)
								{
									$y_thai = $y + 543;
							?>
							<option value="<?php echo $y_thai; ?>"<?php if($loan_model['loan_year']==$y_thai) echo ' SELECTED'; ?>><?php echo $y_thai; ?></option>
							<?php
								}
							?>
						</select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โครงการ<span class="asterisk">*</span></label>
                        <div id="divProject">
							<select name="project_id" id="project_id" data-placeholder="กรุณาเลือก" class="width100p" required>
								<option value="">กรุณาเลือก</option>
							</select>
						</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันที่ (วัน-เดือน-ปี : <?php echo date_th_now(); ?>) <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_date" name="loan_date" value="<?php echo $loan_model['loan_date'] ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ผู้ขอสินเชื่อ</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan" id="person_thaiid_form">
                        <label class="control-label">เลขประจำตัวประชาชน <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_thaiid]" id="person_thaiid" placeholder="เลขประจำตัวประชาชน" class="form-control" value="<?php echo $person_data['person_thaiid']; ?>" <?php if ($person_data['person_id']): ?>readonly="readonly"<?php endif; ?> maxlength="13" required>
                        <div id="thaiid_error" class="thaiid_error">Please enter format not correctly.</div>
                        <div id="thaiid_error2" class="thaiid_error">Please enter your unique Thai id.</div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันออกบัตร <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_card_startdate" name="person[person_card_startdate]" value="<?php echo $person_data['person_card_startdate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันบัตรหมดอายุ <span class="asterisk">*</span>&nbsp;&nbsp;<input type="checkbox" id="person_card_expire" name="person[person_card_expire]" value="1" <?php echo $person_data['person_card_expire']=='1'? 'checked' : ''; ?>> ตลอดชีพ</label>
                        <div id="divExpire" class="input-group" <?php echo $person_data['person_card_expire']=='1'? 'style="display:none;"' : ''; ?>>
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_card_expiredate" name="person[person_card_expiredate]" value="<?php echo $person_data['person_card_expiredate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                        <select class="form-control" id="title_id" name="person[title_id]" required>
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_fname]" id="person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_lname]" id="person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันเกิด (วัน-เดือน-ปี : <?php echo date_th_now(); ?>) <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_birthdate" name="person[person_birthdate]" value="<?php echo $person_data['person_birthdate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อายุ (ปี)</label>
                        <input type="text" class="form-control" placeholder="อายุ (ปี)" id="person_age" name="person_age_nosave" value="" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เพศ <span class="asterisk">*</span></label>
                        <?php if ($mst['master_sex_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['master_sex_list'] as $data): ?>
                                    <label><input type="radio" class="sex_id" name="person[sex_id]" id="sex_id_<?php echo $data['sex_id']; ?>" value="<?php echo $data['sex_id']; ?>" <?php if ($person_data['sex_id'] == $data['sex_id']): ?>checked="checked"<?php endif; ?> required><?php echo $data['sex_name']; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เชื้อชาติ <span class="asterisk">*</span></label>
                        <select class="form-control" id="race_id" name="person[race_id]" required>
                            <option value="">เชื้อชาติ</option>
                            <?php if ($mst['config_race_list']): ?>
                                <?php foreach ($mst['config_race_list'] as $data): ?>
                                    <option value="<?php echo $data['race_id']; ?>" <?php if ($person_data['race_id'] == $data['race_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['race_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สัญชาติ <span class="asterisk">*</span></label>
                        <select class="form-control" id="nationality_id" name="person[nationality_id]" required>
                            <option value="">สัญชาติ</option>
                            <?php if ($mst['config_nationality_list']): ?>
                                <?php foreach ($mst['config_nationality_list'] as $data): ?>
                                    <option value="<?php echo $data['nationality_id']; ?>" <?php if ($person_data['nationality_id'] == $data['nationality_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['nationality_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ศาสนา <span class="asterisk">*</span></label>
                        <select class="form-control" id="religion_id" name="person[religion_id]" required>
                            <option value="">ศาสนา</option>
                            <?php if ($mst['config_religion_list']): ?>
                                <?php foreach ($mst['config_religion_list'] as $data): ?>
                                    <option value="<?php echo $data['religion_id']; ?>" <?php if ($person_data['religion_id'] == $data['religion_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['religion_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์บ้าน</label>
                        <input type="tel" pattern=".{9,9}" maxlength="9" title="9 characters" name="person[person_phone]" id="person_phone" placeholder="โทรศัพท์บ้าน" class="form-control" value="<?php echo $person_data['person_phone']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์มือถือ</label>
                        <input type="tel" pattern=".{10,10}" maxlength="10" title="10 characters" name="person[person_mobile]" id="person_mobile" placeholder="โทรศัพท์มือถือ" class="form-control" value="<?php echo $person_data['person_mobile']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">E-mail</label>
                        <input type="email" name="person[person_email]" id="person_email" placeholder="E-mail" class="form-control" value="<?php echo $person_data['person_email']; ?>">
                    </div>
                </div>
            </div>



            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่อยู่ตามบัตรประชาชน</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ที่อยู่เลขที่ <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_addr_card_no]" id="person_addr_card_no" placeholder="ที่อยู่เลขที่" class="form-control" value="<?php echo $person_data['person_addr_card_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label>
                        <input type="text" name="person[person_addr_card_moo]" id="person_addr_card_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $person_data['person_addr_card_moo']; ?>" maxlength="2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label>
                        <input type="text" name="person[person_addr_card_road]" id="person_addr_card_road" placeholder="ถนน" class="form-control" value="<?php echo $person_data['person_addr_card_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <div id="div_person_addr_card_province_id">
                            <select class="width100p" id="person_addr_card_province_id" name="person[person_addr_card_province_id]" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_addr_card_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                        <div id="div_person_addr_card_amphur_id">
                            <select class="width100p" id="person_addr_card_amphur_id" name="person[person_addr_card_amphur_id]" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_addr_card_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_person_addr_card_district_id">
                            <select class="width100p" id="person_addr_card_district_id" name="person[person_addr_card_district_id]" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_addr_card_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รหัสไปรษณีย์</label>
                        <input type="text" name="person[person_addr_card_zipcode]" id="person_addr_card_zipcode" placeholder="รหัสไปรษณีย์" class="form-control" value="<?php echo $person_data['person_addr_card_zipcode']; ?>" readonly>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่อยู่ปัจจุบัน </h4>
                        <input type="hidden" id="addr_same_val" value="1">
                        <label><input type="checkbox" value="1" <?php echo $person_data['person_addr_same']=='1'?'checked':''; ?> class="addr_same" id="person_addr_same" name="person[person_addr_same]">ตามที่ระบุไว้ในบัตรประชาชน</label>
                    </div>
                </div>
            </div>

            <div class="row div_addr_same">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label>
                        <input type="text" name="person[person_addr_pre_no]" id="person_addr_pre_no" placeholder="เลขที่" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label>
                        <input type="text" name="person[person_addr_pre_moo]" id="person_addr_pre_moo" placeholder="หมู่ที่" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_moo']; ?>" maxlength="2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label>
                        <input type="text" name="person[person_addr_pre_road]" id="person_addr_pre_road" placeholder="ถนน" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row div_addr_same">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <div id="div_person_addr_pre_province_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_province_id" name="person[person_addr_pre_province_id]" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                        <div id="div_person_addr_pre_amphur_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_amphur_id" name="person[person_addr_pre_amphur_id]" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_person_addr_pre_district_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_district_id" name="person[person_addr_pre_district_id]" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row div_addr_same">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รหัสไปรษณีย์</label>
                        <input type="text" name="person[person_addr_pre_zipcode]" id="person_addr_pre_zipcode" placeholder="รหัสไปรษณีย์" class="form-control" value="<?php echo $person_data['person_addr_pre_zipcode']; ?>" readonly>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่อยู่</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ลักษณะที่อยู่อาศัย</label>
                        <?php if ($mst['person_address_type']): ?>
                            <div class="radio">
                                <?php foreach ($mst['person_address_type'] as $key => $data): ?>
                                    <label><input type="radio" name="person[person_address_type]" class="person_address_type" id="person_address_type_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($person_data['person_address_type'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมายเหตุ</label>
                        <textarea class="form-control" rows="5" id="person_owner_building_note" name="person_owner_building_note"><?php echo $person_data['person_owner_building_note'] ?></textarea>
                    </div>
                </div>
            </div>

            <!--อาศัยอยู่กับผู้อื่น (ระบุ) & มีที่อยู่อาศัยเป็นของตน-->
            <!--<div class="row display_person_address_type_1 display_person_address_type_3 display_person_address_type_1_3">-->
            <div class="row display_person_address_type_1_3">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">คำนำหน้าชื่อ</label>
                        <select class="form-control" id="person_owner_building_title_id" name="person[person_owner_building_title_id]">
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['person_owner_building_title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ</label>
                        <input type="text" name="person[person_owner_building_fname]" id="person_owner_building_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_owner_building_fname']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล</label>
                        <input type="text" name="person[person_owner_building_lname]" id="person_owner_building_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_owner_building_lname']; ?>">
                    </div>
                </div>
            </div>

            <!--อาศัยอยู่กับผู้อื่น (ระบุ)-->
            <!--
            <div class="row display_person_address_type_3">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาศัยอยู่กับผู้อื่น (ระบุ)</label>
                        <input type="text" name="person[person_address_type_other]" id="person_address_type_other" placeholder="อาศัยอยู่กับผู้อื่น (ระบุ)" class="form-control" value="<?php echo $person_data['person_address_type_other']; ?>">
                    </div>
                </div>
            </div>
            -->

            <!--มีที่อยู่อาศัยเป็นของตน -->
            <div class="row display_person_address_type_1">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทที่ดิน</label>
                        <select class="form-control" id="person_land_type_id" name="person[person_land_type_id]">
                            <option value="">ประเภทที่ดิน</option>
                            <?php if ($mst['config_land_type_list']): ?>
                                <?php foreach ($mst['config_land_type_list'] as $data): ?>
                                    <option value="<?php echo $data['land_type_id']; ?>" <?php if ($person_data['person_land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label>
                        <input type="text" name="person[person_land_no]" id="person_land_no" placeholder="เลขที่" class="form-control" value="<?php echo $person_data['person_land_no']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ดิน</label>
                        <input type="number" name="person[person_land_addr_no]" id="person_land_addr_no" placeholder="เลขที่ดิน" class="form-control" value="<?php echo $person_data['person_land_addr_no']; ?>">
                    </div>
                </div>
            </div>

            <div class="row display_person_address_type_1">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด</label>
                        <div id="div_person_land_addr_province_id">
                            <select class="width100p" id="person_land_addr_province_id" name="person[person_land_addr_province_id]">
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต</label>
                        <div id="div_person_land_addr_amphur_id">
                            <select class="width100p" id="person_land_addr_amphur_id" name="person[person_land_addr_amphur_id]">
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง</label>
                        <div id="div_person_land_addr_district_id">
                            <select class="width100p" id="person_land_addr_district_id" name="person[person_land_addr_district_id]">
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row display_person_address_type_1">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ไร่)</label>
                        <input type="number" min="0" name="person[person_land_area_rai]" id="person_land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $person_data['person_land_area_rai']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(งาน)</label>
                        <input type="number" min="0" max="<?php echo get_max_ngan(); ?>" name="person[person_land_area_ngan]" id="person_land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $person_data['person_land_area_ngan']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ตารางวา)</label>
                        <input type="number" min="0" max="<?php echo get_max_wa(); ?>" name="person[person_land_area_wah]" id="person_land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $person_data['person_land_area_wah']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลอาชีพ</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพ <span class="asterisk">*</span></label>
                        <select class="form-control" id="career_id" name="person[career_id]" required="required">
                            <option value="">อาชีพ</option>
                            <?php if ($mst['config_career_list']): ?>
                                <?php foreach ($mst['config_career_list'] as $data): ?>
                                    <option value="<?php echo $data['career_id']; ?>" <?php if ($person_data['career_id'] == $data['career_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['career_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพอื่นๆ (ระบุ)</label>
                        <input type="text" name="person[person_career_other]" id="person_career_other" placeholder="อาชีพอื่นๆ (ระบุ)" class="form-control" value="<?php echo $person_data['person_career_other']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลคู่สมรส</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สถานภาพ</label>
                        <?php if ($mst['master_status_married_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['master_status_married_list'] as $data): ?>
                                    <label><input type="radio" name="person[status_married_id]" class="status_married_id" id="status_married_id_<?php echo $data['status_married_id']; ?>" value="<?php echo $data['status_married_id']; ?>" <?php if ($person_data['status_married_id'] == $data['status_married_id']): ?>checked="checked"<?php endif; ?>><?php echo $data['status_married_name']; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div id="spouse_form">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan" id="spouse_thaiid_form">
                            <label class="control-label">เลขประจำตัวประชาชน</label>
                            <input type="text" name="spouse[person_thaiid]" id="spouse_thaiid" placeholder="เลขประจำตัวประชาชน" class="form-control" value="<?php echo $spouse_data['person_thaiid']; ?>" maxlength="13">
                            <div id="spouse_thaiid_error" class="thaiid_error">Please enter format not correctly.</div>
                            <div id="spouse_thaiid_error2" class="thaiid_error">Please enter your unique Thai id.</div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">วันออกบัตร</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="spouse_card_startdate" name="spouse[person_card_startdate]" value="<?php echo $spouse_data['person_card_startdate']; ?>">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">วันบัตรหมดอายุ&nbsp;&nbsp;<input type="checkbox" id="spouse_card_expire" name="spouse[person_card_expire]" value="1" <?php echo $spouse_data['person_card_expire']=='1'? 'checked' : ''; ?>> ตลอดชีพ</label>
                            <div id="divExpireSpouse" class="input-group" <?php echo $spouse_data['person_card_expire']=='1'? 'style="display:none;"' : ''; ?>>
                                <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="spouse_card_expiredate" name="spouse[person_card_expiredate]" value="<?php echo $spouse_data['person_card_expiredate']; ?>">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">คำนำหน้าชื่อ</label>
                            <select class="form-control spouse_input" id="spouse_title_id" name="spouse[title_id]" >
                                <option value="">คำนำหน้าชื่อ</option>
                                <?php if ($mst['config_title_list']): ?>
                                    <?php foreach ($mst['config_title_list'] as $data): ?>
                                        <option value="<?php echo $data['title_id']; ?>" <?php if ($spouse_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ชื่อ</label>
                            <input type="text" name="spouse[person_fname]" id="spouse_fname" placeholder="ชื่อ" class="form-control spouse_input" value="<?php echo $spouse_data['person_fname']; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">นามสกุล</label>
                            <input type="text" name="spouse[person_lname]" id="spouse_lname" placeholder="นามสกุล" class="form-control spouse_input" value="<?php echo $spouse_data['person_lname']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">วันเกิด (วัน-เดือน-ปี : <?php echo date_th_now(); ?>)</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="spouse_birthdate" name="spouse[person_birthdate]" value="<?php echo $spouse_data['person_birthdate']; ?>">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อายุ (ปี)</label>
                            <input type="text" class="form-control" placeholder="อายุ (ปี)" id="spouse_age" name="spouse_age_nosave" value="" readonly="readonly">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อาชีพ</label>
                            <select class="form-control spouse_input" id="spouse_career_id" name="spouse[career_id]">
                                <option value="">อาชีพ</option>
                                <?php if ($mst['config_career_list']): ?>
                                    <?php foreach ($mst['config_career_list'] as $data): ?>
                                        <option value="<?php echo $data['career_id']; ?>" <?php if ($spouse_data['career_id'] == $data['career_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['career_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อาชีพอื่นๆ (ระบุ)</label>
                            <input type="text" name="spouse[person_career_other]" id="spouse_career_other" placeholder="อาชีพอื่นๆ (ระบุ)" class="form-control spouse_input" value="<?php echo $spouse_data['person_career_other']; ?>">
                        </div>
                    </div>

                </div>


                <hr>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">โทรศัพท์บ้าน</label>
                            <input type="tel" pattern=".{9,9}" title="9 characters" maxlength="9" name="spouse[person_phone]" id="spouse_phone" placeholder="โทรศัพท์บ้าน" class="form-control spouse_input" value="<?php echo $spouse_data['person_phone']; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">โทรศัพท์มือถือ</label>
                            <input type="tel" pattern=".{10,10}" title="10 characters" maxlength="10" name="spouse[person_mobile]" id="spouse_mobile" placeholder="โทรศัพท์มือถือ" class="form-control spouse_input" value="<?php echo $spouse_data['person_mobile']; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">E-mail</label>
                            <input type="email" name="spouse[person_email]" id="spouse_email" placeholder="E-mail" class="form-control spouse_input" value="<?php echo $spouse_data['person_email']; ?>">
                        </div>
                    </div>
                </div>

                <hr>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <h4>ที่อยู่ปัจจุบัน</h4>
                            <input type="hidden" id="spouse_addr_same_val" value="1">
                            <label><input type="checkbox" value="1" <?php echo $spouse_data['person_addr_same']?'checked':''; ?> id="spouse_addr_same" class="spouse_addr_same" name="spouse[person_addr_same]" class="spouse_input">ที่อยู่เดียวกับคู่สมรส</label>
                        </div>
                    </div>
                </div>

                <div class="row div_spouse_addr_same ">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ที่อยู่เลขที่</label>
                            <input type="text" name="spouse[person_addr_pre_no]" id="spouse_addr_pre_no" placeholder="ที่อยู่เลขที่" class="form-control spouse_addr_pre spouse_input" value="<?php echo $spouse_data['person_addr_pre_no']; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">หมู่ที่</label>
                            <input type="text" name="spouse[person_addr_pre_moo]" id="spouse_addr_pre_moo" placeholder="หมู่ที่" class="form-control spouse_addr_pre spouse_input" value="<?php echo $spouse_data['person_addr_pre_moo']; ?>" maxlength="2">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ถนน</label>
                            <input type="text" name="spouse[person_addr_pre_road]" id="spouse_addr_pre_road" placeholder="ถนน" class="form-control spouse_addr_pre spouse_input" value="<?php echo $spouse_data['person_addr_pre_road']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row div_spouse_addr_same ">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">จังหวัด</label>
                            <div id="div_spouse_addr_pre_province_id">
                                <select class="width100p spouse_addr_pre" id="spouse_addr_pre_province_id" name="spouse[person_addr_pre_province_id]">
                                    <option value="">จังหวัด</option>
                                    <?php if ($mst['master_province_list']): ?>
                                        <?php foreach ($mst['master_province_list'] as $data): ?>
                                            <option value="<?php echo $data['province_id']; ?>" <?php if ($spouse_data['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อำเภอ/เขต</label>
                            <div id="div_spouse_addr_pre_amphur_id">
                                <div id="div_spouse_addr_pre_amphur_id">
                                    <select class="width100p spouse_addr_pre" id="spouse_addr_pre_amphur_id" name="spouse[person_addr_pre_amphur_id]">
                                        <option value="">อำเภอ/เขต</option>
                                        <?php if ($mst['master_amphur_list']): ?>
                                            <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                                <option value="<?php echo $data['amphur_id']; ?>" <?php if ($spouse_data['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ตำบล/แขวง</label>
                            <div id="div_spouse_addr_pre_district_id">
                                <div id="div_spouse_addr_pre_district_id">
                                    <select class="width100p spouse_addr_pre" id="spouse_addr_pre_district_id" name="spouse[person_addr_pre_district_id]">
                                        <option value="">ตำบล/แขวง</option>
                                        <?php if ($mst['master_district_list']): ?>
                                            <?php foreach ($mst['master_district_list'] as $data): ?>
                                                <option value="<?php echo $data['district_id']; ?>" <?php if ($spouse_data['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row div_spouse_addr_same">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">รหัสไปรษณีย์</label>
                            <input type="text" name="spouse[person_addr_pre_zipcode]" id="spouse_addr_pre_zipcode" placeholder="รหัสไปรษณีย์" class="form-control" value="<?php echo $spouse_data['person_addr_pre_zipcode']; ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลสมาชิก</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนสมาชิกในครัวเรือน (คน)</label>
                        <input type="number" name="loan_member_amount" id="loan_member_amount" placeholder="จำนวนสมาชิกในครัวเรือน" class="form-control" value="<?php echo $loan_data['loan_member_amount']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนแรงงานในครัวเรือน (คน)</label>
                        <input type="number" name="loan_labor_amount" id="loan_labor_amount" placeholder="จำนวนแรงงานในครัวเรือน" class="form-control" value="<?php echo $loan_data['loan_labor_amount']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
    /*province, amphur, district addr_card for person*/
//     var person_pad_id_addr_card_list = 'province_id=person_addr_card_province_id&amphur_id=person_addr_card_amphur_id&district_id=person_addr_card_district_id&zipcode_id=person_addr_card_zipcode';
//     var person_pad_name_addr_card_list = 'province_name=person[person_addr_card_province_id]&amphur_name=person[person_addr_card_amphur_id]&district_name=person[person_addr_card_district_id]';
//     var person_pad_class_addr_card_list = 'province_class=person_addr_card&amphur_class=person_addr_card&district_class=person_addr_card';
//
//     //addr_card
//     var param_url_person_pad_addr_card = "/?" + person_pad_id_addr_card_list + "&" + person_pad_name_addr_card_list + "&" + person_pad_class_addr_card_list;
// <?php if ($person_data['person_addr_card_amphur_id']): ?>
//         $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_card_province_id").val() + "/" + <?php echo $person_data['person_addr_card_amphur_id'] ?> + param_url_person_pad_addr_card);
//         $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_addr_card_amphur_id'] ?> + "/" + <?php echo $person_data['person_addr_card_district_id'] ?> + param_url_person_pad_addr_card);
// <?php endif; ?>


  $("#person_addr_card_province_id").change(function () {
      $("#div_person_addr_card_amphur_id").load(base_url + "master/amphur/" + $("#person_addr_card_province_id").val() +
          "?id=person_addr_card_amphur_id&name=person[person_addr_card_amphur_id]&target=div_person_addr_card_district_id" +
          "&target_name=person[person_addr_card_district_id]&target_id=person_addr_card_district_id&zip_id=person_addr_card_zipcode" );

  });

  <?php if ($person_data['person_addr_card_amphur_id']): ?>
          $("#div_person_addr_card_amphur_id").load(base_url + "master/amphur/" + $("#person_addr_card_province_id").val() +
              "?selected=<?php echo $person_data['person_addr_card_amphur_id'] ?>&id=person_addr_card_amphur_id&name=person[person_addr_card_amphur_id]&target=div_person_addr_card_district_id" +
              "&target_name=person[person_addr_card_district_id]&target_id=person_addr_card_district_id&zip_id=person_addr_card_zipcode" );

          $("#div_person_addr_card_district_id").load(base_url+"master/district/<?php echo $person_data['person_addr_card_amphur_id'] ?>" +
     		     "?selected=<?php echo $person_data['person_addr_card_district_id'] ?>&name=person[person_addr_card_district_id]&id=person_addr_card_district_id&zip_id=person_addr_card_zipcode" );


  <?php endif; ?>

</script>


<script type="text/javascript">
    /*province, amphur, district addr_pre for person*/
//     var person_pad_id_addr_pre_list = 'province_id=person_addr_pre_province_id&amphur_id=person_addr_pre_amphur_id&district_id=person_addr_pre_district_id&zipcode_id=person_addr_pre_zipcode';
//     var person_pad_name_addr_pre_list = 'province_name=person[person_addr_pre_province_id]&amphur_name=person[person_addr_pre_amphur_id]&district_name=person[person_addr_pre_district_id]';
//     var person_pad_class_addr_pre_list = 'province_class=person_addr_pre&amphur_class=person_addr_pre&district_class=person_addr_pre';
//
//     //addr_pre
//     var param_url_person_pad_addr_pre = "/?" + person_pad_id_addr_pre_list + "&" + person_pad_name_addr_pre_list + "&" + person_pad_class_addr_pre_list;
// <?php if ($person_data['person_addr_pre_amphur_id']): ?>
//         $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_pre_province_id").val() + "/" + <?php echo $person_data['person_addr_pre_amphur_id'] ?> + param_url_person_pad_addr_pre);
//       //  $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $person_data['person_addr_pre_district_id'] ?> + param_url_person_pad_addr_pre);
// <?php endif; ?>

$("#person_addr_pre_province_id").change(function () {
    $("#div_person_addr_pre_amphur_id").load(base_url + "master/amphur/" + $("#person_addr_pre_province_id").val() +
        "?id=person_addr_pre_amphur_id&name=person[person_addr_pre_amphur_id]&target=div_person_addr_pre_district_id" +
        "&target_name=person[person_addr_pre_district_id]&target_id=person_addr_pre_district_id&zip_id=person_addr_pre_zipcode" );

});

<?php if ($person_data['person_addr_pre_amphur_id']): ?>
        $("#div_person_addr_pre_amphur_id").load(base_url + "master/amphur/" + $("#person_addr_pre_province_id").val() +
            "?selected=<?php echo $person_data['person_addr_pre_amphur_id'] ?>&id=person_addr_pre_amphur_id&name=person[person_addr_pre_amphur_id]&target=div_person_addr_pre_district_id" +
            "&target_name=person[person_addr_pre_district_id]&target_id=person_addr_pre_district_id&zip_id=person_addr_pre_zipcode" );

        $("#div_person_addr_pre_district_id").load(base_url+"master/district/<?php echo $person_data['person_addr_pre_amphur_id'] ?>" +
           "?selected=<?php echo $person_data['person_addr_pre_district_id'] ?>&name=person[person_addr_pre_district_id]&id=person_addr_pre_district_id&zip_id=person_addr_pre_zipcode" );


<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_pre for spouse*/
    var spouse_pad_id_addr_pre_list = 'province_id=spouse_addr_pre_province_id&amphur_id=spouse_addr_pre_amphur_id&district_id=spouse_addr_pre_district_id&zipcode_id=spouse_addr_pre_zipcode';
    var spouse_pad_name_addr_pre_list = 'province_name=spouse[person_addr_pre_province_id]&amphur_name=spouse[person_addr_pre_amphur_id]&district_name=spouse[person_addr_pre_district_id]';
    var spouse_pad_class_addr_pre_list = 'province_class=spouse_addr_pre&amphur_class=spouse_addr_pre&district_class=spouse_addr_pre';

    //addr_pre
    var param_url_spouse_pad_addr_pre = "/?" + spouse_pad_id_addr_pre_list + "&" + spouse_pad_name_addr_pre_list + "&" + spouse_pad_class_addr_pre_list;
<?php if ($spouse_data['person_addr_pre_amphur_id']): ?>
<?php if ($spouse_data['person_addr_pre_district_id']){ ?>
    $("#div_spouse_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $spouse_data['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $spouse_data['person_addr_pre_district_id'] ?> + param_url_spouse_pad_addr_pre);
<?php } else { ?>
    $("#div_spouse_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#spouse_addr_pre_province_id").val() + "/" + <?php echo $spouse_data['person_addr_pre_amphur_id'] ?> + param_url_spouse_pad_addr_pre);
<?php } ?>
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district person_land_addr for person*/
    var person_pad_id_person_land_addr_list = 'province_id=person_land_addr_province_id&amphur_id=person_land_addr_amphur_id&district_id=person_land_addr_district_id&zipcode_id=person_land_addr_pre_zipcode';
    var person_pad_name_person_land_addr_list = 'province_name=person[person_land_addr_province_id]&amphur_name=person[person_land_addr_amphur_id]&district_name=person[person_land_addr_district_id]';
    var person_pad_class_person_land_addr_list = 'province_class=person_land_addr&amphur_class=person_land_addr&district_class=person_land_addr';

    //person_land
    var param_url_person_pad_person_land_addr = "/?" + person_pad_id_person_land_addr_list + "&" + person_pad_name_person_land_addr_list + "&" + person_pad_class_person_land_addr_list;
<?php if ($person_data['person_land_addr_amphur_id']): ?>
        $("#div_person_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#person_land_addr_province_id").val() + "/" + <?php echo $person_data['person_land_addr_amphur_id'] ?> + param_url_person_pad_person_land_addr);
        $("#div_person_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_land_addr_amphur_id'] ?> + "/" + <?php echo $person_data['person_land_addr_district_id'] ?> + param_url_person_pad_person_land_addr);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*display_person_address_type*/
<?php if ($person_data['person_address_type'] == 1): ?>
    $(".display_person_address_type_1_3").show();
    $(".display_person_address_type_1").show();
<?php elseif ($person_data['person_address_type'] == 2): ?>
    $(".display_person_address_type_1_3").hide();
    $(".display_person_address_type_1").hide();
<?php elseif ($person_data['person_address_type'] == 3): ?>
    $(".display_person_address_type_1_3").show();
    $(".display_person_address_type_1").hide();
<?php else: ?>
    $(".display_person_address_type_1_3").hide();
    $(".display_person_address_type_1").hide();
<?php endif; ?>
<?php if ($person_data['person_addr_same'] == 1): ?>
    $(".div_addr_same").hide();
<?php else: ?>
    $(".div_addr_same").show();
<?php endif; ?>

<?php if ($spouse_data['person_addr_same'] == 1): ?>
    $(".div_spouse_addr_same").hide();
<?php else: ?>
    $(".div_spouse_addr_same").show();
<?php endif; ?>

</script>

<script type="text/javascript">

    /*อื่นๆ = 5*/
<?php if ($person_data['career_id'] != 5): ?>
        //$('#person_career_other').attr('readonly', true);
        //$('#person_career_other').val('');
<?php endif; ?>

<?php if ($spouse_data['career_id'] != 5): ?>
        $('#spouse_career_other').attr('readonly', true);
        $('#spouse_career_other').val('');
<?php endif; ?>

    /*career_id*/
    $("#career_id").change(function () {
        var career_id = $(this).val();

        /*อื่นๆ = 5*/
        /*if (career_id == 5) {
            $('#person_career_other').attr('readonly', false);
        } else {
            $('#person_career_other').attr('readonly', true);
            $('#person_career_other').val('');
        }*/
    });

    /*spouse_career_id*/
    $("#spouse_career_id").change(function () {
        var spouse_career_id = $(this).val();

        /*อื่นๆ = 5*/
        if (spouse_career_id == 5) {
            $('#spouse_career_other').attr('readonly', false);
        } else {
            $('#spouse_career_other').attr('readonly', true);
            $('#spouse_career_other').val('');
        }
    });


    /*person_address_type*/
    $(".person_address_type").change(function () {
        var person_address_type = $(this).val();

        /*มีที่อยู่อาศัยเป็นของตนเอง*/
        if (person_address_type == 1) {
            $('#person_owner_building_title_id').val($('#title_id').val());
            $('#person_owner_building_fname').val($('#person_fname').val());
            $('#person_owner_building_lname').val($('#person_lname').val());
        }
    });

    /*status_married_id change*/
    $(".status_married_id").change(function () {
        var status_married_id = $(this).val();

        /*โสด = 2*/
        if (status_married_id == 2) {
            $("#spouse_form").hide();
        } else {
            $("#spouse_form").show();
        }
    });

    //loan_labor_amount
    $("#loan_labor_amount").focus(function () {
        var loan_member_amount = $("#loan_member_amount").val();
        $(this).attr({
            "max": loan_member_amount,
            "min": 0
        });
    });

    <?php if(!empty($loan_model['loan_year'])) { ?>
    	$("#divProject").load(base_url+"loan/form_person/project/<?php echo $loan_model['loan_year']; ?>", function() {
    		<?php if(!empty($loan_model['project_id'])) { ?>$("#project_id").select2("val", "<?php echo $loan_model['project_id']; ?>"); <?php } ?>
      	});
   <?php } ?>
</script>
