<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1)</h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>

    </div>
    <div class="panel-body form-list">
        <?php if ($session_form && $session_form['status'] == 'fail'): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <ol>
            <li><a href="<?php echo site_url("loan/report_loan/{$loan_model['loan_id']}"); ?>" target="_blank">แบบบันทึกคำขอสินเชื่อ (บจธ.สช. ๑)</a></li>
            <li><a href="<?php echo site_url("loan/report_loan_guarantee_bondsman_word/{$loan_model['loan_id']}"); ?>" target="_blank">แบบค้ำประกัน (บจธ.สช. ๖)</a></li>
            <li><a href="<?php echo site_url("loan/report_spouse_agree_word/{$loan_model['loan_id']}"); ?>" target="_blank">แบบหนังสือยินยอมของคู่สมรส (บจธ.สช. ๗)</a></li>
            <li><a href="<?php echo site_url("loan/report_loan_guarantee_bookbank_word/{$loan_model['loan_id']}"); ?>" target="_blank">แบบสัญญานำเงินฝากหลักประกัน (บจธ.สช. ๘)</a></li>
            <li><a href="<?php echo site_url("loan/report_loan_guarantee_bond_word/{$loan_model['loan_id']}"); ?>" target="_blank">แบบจำนำพันธบัตร (บจธ.สช. ๙)</a></li>            
        </ol>
    </div><!-- panel-body -->
</div><!-- panel -->