<form id="form_loan" name="form_loan" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land/save_land1"); ?>">
    <input type="hidden" id="loan_objective_id" name="loan_objective_id" value="<?php echo $form_data['loan_objective_id']; ?>">
    <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
    <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">
    <input type="hidden" name="redeem_owner[person_id]" value="<?php echo $redeem_owner['person_id']; ?>">

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลการไถ่ถอนที่ดินจากการจำนอง/การขายฝาก</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">การไถ่ถอนที่ดินจาก <span class="asterisk">*</span></label>
                <?php if ($redeem_type_data): ?>
                    <div class="radio">
                        <?php foreach ($redeem_type_data as $key => $data): ?>
                            <label><input type="radio" name="loan_type_redeem_type" id="loan_type_redeem_type<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['loan_type_redeem_type'] == $key): ?>checked="checked"<?php endif; ?> required><?php echo $data; ?><?php echo nbs(2); ?></label>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <div class="form-group form-group-loan">
                <label class="control-label">สาเหตุของการนำที่ดินไปจำนองหรือขายฝาก <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_redeem_case" id="loan_type_redeem_case" placeholder="สาเหตุของการนำที่ดินไปจำนองหรือขายฝาก" required><?php echo $form_data['loan_type_redeem_case'] ?></textarea>
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลที่ดิน</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <div class="radio">
                    <?php foreach ($mst['land_buiding_detail_id_list'] as $key => $data): ?>
                    <label><input type="radio" name="land_buiding_detail_id" class="land_buiding_detail_id" id="land_buiding_detail_id_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['land_buiding_detail_id'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ประเภท <span class="asterisk">*</span></label>
                <select class="form-control" id="land_type_id" name="land_type_id" required>
                    <option value="">ประเภท</option>
                    <?php if ($mst['config_land_type_list']): ?>
                        <?php foreach ($mst['config_land_type_list'] as $data): ?>
                            <option value="<?php echo $data['land_type_id']; ?>" <?php if ($form_data['land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เลขที่ <span class="asterisk">*</span></label> 
                <input type="text" name="land_no" id="land_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['land_no']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เลขที่ดิน <span class="asterisk">*</span></label> 
                <input type="number" name="land_addr_no" id="land_addr_no" placeholder="เลขที่ดิน" class="form-control" value="<?php echo $form_data['land_addr_no']; ?>" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">หมู่ที่</label> 
                <input type="text" name="land_addr_moo" id="land_addr_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['land_addr_moo']; ?>" maxlength="2">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                <select class="width100p" id="land_addr_province_id" name="land_addr_province_id" required>
                    <option value="">จังหวัด</option>
                    <?php if ($mst['master_province_list']): ?>
                        <?php foreach ($mst['master_province_list'] as $data): ?>
                            <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>                    
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label> 
                <div id="div_land_addr_amphur_id">
                    <select class="width100p" id="land_addr_amphur_id" name="land_addr_amphur_id" required>
                        <option value="">อำเภอ/เขต</option>
                        <?php if ($mst['master_amphur_list']): ?>
                            <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                <div id="div_land_addr_district_id">
                    <select class="width100p" id="land_addr_district_id" name="land_addr_district_id" required>
                        <option value="">ตำบล/แขวง</option>
                        <?php if ($mst['master_district_list']): ?>
                            <?php foreach ($mst['master_district_list'] as $data): ?>
                                <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>                    
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เนื้อที่(ไร่) <span class="asterisk">*</span></label>
                <input type="number" min="0" name="land_area_rai" id="land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $form_data['land_area_rai']; ?>" required>                        
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เนื้อที่(งาน) <span class="asterisk">*</span></label>
                <input type="number" min="0" name="land_area_ngan" id="land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $form_data['land_area_ngan']; ?>" required>                        
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เนื้อที่(ตารางวา) <span class="asterisk">*</span></label>
                <input type="number" min="0" name="land_area_wah" id="land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $form_data['land_area_wah']; ?>" required>                        
            </div>
        </div>
    </div>

    <?php if (!$form_data['loan_type_id']): ?>

    <hr class="div_land_building">
        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ข้อมูลที่ดินพร้อมสิ่งปลูกสร้าง</h4>
                </div>
            </div>
        </div>


        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภทสิ่งปลูกสร้าง</label>
                    <select class="form-control" id="building_type_id" name="building_type_id" >
                        <option value="">ประเภทสิ่งปลูกสร้าง</option>
                        <?php if ($mst['config_building_type_list']): ?>
                            <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่</label> 
                    <input type="text" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" >
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label> 
                    <input type="text" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ซอย</label> 
                    <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                </div>
            </div> 

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label> 
                    <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด</label>
                    <select class="width100p" id="building_province_id" name="building_province_id">
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต</label>
                    <div id="div_building_amphur_id">
                        <select class="width100p" id="building_amphur_id" name="building_amphur_id">
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                        
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง</label>
                    <div id="div_building_district_id">
                        <select class="width100p" id="building_district_id" name="building_district_id">
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>                    
            </div>
        </div>
    <?php endif; ?>


    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลจำนองหรือขายฝาก</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จำนวนเงินที่นำไปจำนองหรือขายฝาก <span class="asterisk">*</span></label>
                <input type="number" min="0" name="loan_type_redeem_amount" id="loan_type_redeem_amount" placeholder="จำนวนเงินที่นำไปจำนองหรือขายฝาก" class="form-control" value="<?php echo $form_data['loan_type_redeem_amount']; ?>" required>                        
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                <select class="form-control" id="redeem_owner_title_id" name="redeem_owner[title_id]" required>
                    <option value="">คำนำหน้าชื่อ</option>
                    <?php if ($mst['config_title_list']): ?>
                        <?php foreach ($mst['config_title_list'] as $data): ?>
                            <option value="<?php echo $data['title_id']; ?>" <?php if ($redeem_owner['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>     

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                <input type="text" name="redeem_owner[person_fname]" id="redeem_owner_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $redeem_owner['person_fname']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                <input type="text" name="redeem_owner[person_lname]" id="redeem_owner_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $redeem_owner['person_lname']; ?>" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">อายุ</label>
                <input type="text" class="form-control" placeholder="อายุ" id="redeem_owner_person_age" value="">                            
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ที่อยู่ปัจจุบัน เลขที่ <span class="asterisk">*</span></label> 
                <input type="text" name="redeem_owner[person_addr_pre_no]" id="redeem_owner_person_addr_pre_no" placeholder="เลขที่" class="form-control redeem_owner_person_addr_pre" value="<?php echo $redeem_owner['person_addr_pre_no']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">หมู่ที่</label> 
                <input type="text" name="redeem_owner[person_addr_pre_moo]" id="redeem_owner_person_addr_pre_moo" placeholder="หมู่ที่" class="form-control redeem_owner_person_addr_pre" value="<?php echo $redeem_owner['person_addr_pre_moo']; ?>" maxlength="2">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ถนน</label> 
                <input type="text" name="redeem_owner[person_addr_pre_road]" id="redeem_owner_person_addr_pre_road" placeholder="ถนน" class="form-control redeem_owner_person_addr_pre" value="<?php echo $redeem_owner['person_addr_pre_road']; ?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                <div id="div_redeem_owner_person_addr_pre_province_id">
                    <select class="width100p redeem_owner_person_addr_pre" id="redeem_owner_person_addr_pre_province_id" name="redeem_owner[person_addr_pre_province_id]" required>
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($redeem_owner['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>                    
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label> 
                <div id="div_redeem_owner_person_addr_pre_amphur_id">
                    <select class="width100p redeem_owner_person_addr_pre" id="redeem_owner_person_addr_pre_amphur_id" name="redeem_owner[person_addr_pre_amphur_id]" required>
                        <option value="">อำเภอ/เขต</option>
                        <?php if ($mst['master_amphur_list']): ?>
                            <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                <option value="<?php echo $data['amphur_id']; ?>" <?php if ($redeem_owner['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                <div id="div_redeem_owner_person_addr_pre_district_id">
                    <select class="width100p redeem_owner_person_addr_pre" id="redeem_owner_person_addr_pre_district_id" name="redeem_owner[person_addr_pre_district_id]" required>
                        <option value="">ตำบล/แขวง</option>
                        <?php if ($mst['master_district_list']): ?>
                            <?php foreach ($mst['master_district_list'] as $data): ?>
                                <option value="<?php echo $data['district_id']; ?>" <?php if ($redeem_owner['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>                    
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">โทรศัพท์บ้าน</label> 
                <input type="tel" name="redeem_owner[person_phone]" id="redeem_owner_person_phone" placeholder="โทรศัพท์บ้าน" class="form-control" value="<?php echo $redeem_owner['person_phone']; ?>">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">โทรศัพท์มือถือ</label> 
                <input type="tel" name="redeem_owner[person_mobile]" id="redeem_owner_person_mobile" placeholder="โทรศัพท์มือถือ" class="form-control" value="<?php echo $redeem_owner['person_mobile']; ?>">
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <h4>ความสัมพันธ์กับผู้ขอสินเชื่อ</h4>
            </div>
        </div>
    </div>

    <div class="row">               
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ความสัมพันธ์กับผู้ขอสินเชื่อ <span class="asterisk">*</span></label>
                <select class="form-control" id="loan_type_redeem_relationship" name="loan_type_redeem_relationship" required>
                    <option value="">ความสัมพันธ์กับผู้ขอสินเชื่อ</option>
                    <?php if ($mst['config_relationship_list']): ?>
                        <?php foreach ($mst['config_relationship_list'] as $data): ?>
                            <option value="<?php echo $data['relationship_id']; ?>" <?php if ($form_data['loan_type_redeem_relationship'] == $data['relationship_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['relationship_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>                    
        </div>
    </div>

    <div class="row">
        &nbsp;
    </div>

    <div class="row">
        <div class="col-sm-12 right">
            <div class="form-group form-group-loan">
                <button class="btn btn-primary mr5">บันทึก</button>
                <button type="reset" class="btn btn-default">ยกเลิก</button>
            </div>
        </div>
    </div>
</form>

<?php if ($form_data['loan_type_id']): ?>
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลสิ่งปลูกสร้าง</h4>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped mb30">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ประเภทสิ่งปลูกสร้าง</th>                
                    <th>สถานที่ตั้ง</th>
                    <th>แก้ไข/ลบ</th>
                </tr>
            </thead>
            <?php if ($building_table): ?>
                <tbody>
                    <?php foreach ($building_table as $data): ?>
                        <tr>
                            <td><?php echo $data['building_id']; ?></td>
                            <td><?php echo $data['building_type_name']; ?></td>
                            <td><?php echo $data['address']; ?></td>
                            <td>
            <!--                                <a href="<?php echo site_url("loan/form_doc/?view=1"); ?>" class=""><span class="glyphicon glyphicon-zoom-in"></span></a>
                                &nbsp;&nbsp;-->
                                <a href="<?php echo site_url("loan/form_land/land_{$form_data['loan_objective_id']}/?edit={$form_data['loan_type_id']}&building_id={$data['building_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                &nbsp;&nbsp;
                                <a href="<?php echo site_url("loan/form_land/delete_building/{$data['building_id']}/?loan_objective_id={$form_data['loan_objective_id']}&loan_type_id={$form_data['loan_type_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>                                    
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>

        </table>
    </div><!-- table-responsive -->

    <form id="form_building" name="form_building" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land/save_building"); ?>">
        <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
        <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">
        <input type="hidden" id="loan_objective_id" name="loan_objective_id" value="<?php echo $form_data['loan_objective_id']; ?>">
        <input type="hidden" id="building_owner" name="building_owner" value="<?php echo $redeem_owner['person_id']; ?>">
        <input type="hidden" id="building_id" name="building_id" value="<?php echo $form_data['building_id']; ?>">


        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภทสิ่งปลูกสร้าง <span class="asterisk">*</span></label>
                    <select class="form-control" id="building_type_id" name="building_type_id" required>
                        <option value="">ประเภทสิ่งปลูกสร้าง</option>
                        <?php if ($mst['config_building_type_list']): ?>
                            <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่ <span class="asterisk">*</span></label> 
                    <input type="text" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label> 
                    <input type="text" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ซอย</label> 
                    <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                </div>
            </div> 

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label> 
                    <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                    <select class="width100p" id="building_province_id" name="building_province_id" required>
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                    <div id="div_building_amphur_id">
                        <select class="width100p" id="building_amphur_id" name="building_amphur_id" required>
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                        
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                    <div id="div_building_district_id">
                        <select class="width100p" id="building_district_id" name="building_district_id" required>
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>                    
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-12 right">
                <div class="form-group form-group-loan">
                    <button class="btn btn-primary mr5">บันทึก</button>
                    <button type="reset" class="btn btn-default">ยกเลิก</button>
                </div>
            </div>
        </div>
    </form>
<?php endif; ?>

<script type="text/javascript">
    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

<?php if ($form_data['land_addr_amphur_id']): ?>
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + "/" + <?php echo$form_data['land_addr_district_id'] ?> + param_url_pad_land_addr);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district for building*/
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';
    var pad_required_building_list = 'province_required=&amphur_required=&district_required=';
    var param_url_pad_building = "/?" + pad_id_building_list + "&" + pad_name_building_list + "&" + pad_required_building_list;

<?php if ($form_data['building_amphur_id']): ?>
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + "/" + <?php echo $form_data['building_amphur_id'] ?> + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['building_amphur_id'] ?> + "/" + <?php echo$form_data['building_district_id'] ?> + param_url_pad_building);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_card for redeem_owner*/
    var redeem_owner_pad_id_addr_card_list = 'province_id=redeem_owner_person_addr_card_province_id&amphur_id=redeem_owner_person_addr_card_amphur_id&district_id=redeem_owner_person_addr_card_district_id';
    var redeem_owner_pad_name_addr_card_list = 'province_name=redeem_owner[person_addr_card_province_id]&amphur_name=redeem_owner[person_addr_card_amphur_id]&district_name=redeem_owner[person_addr_card_district_id]';
    var param_url_redeem_owner_pad_addr_card = "/?" + redeem_owner_pad_id_addr_card_list + "&" + redeem_owner_pad_name_addr_card_list;

<?php if ($redeem_owner['person_addr_card_amphur_id']): ?>
        $("#div_redeem_owner_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#redeem_owner_person_addr_card_province_id").val() + "/" + <?php echo $redeem_owner['person_addr_card_amphur_id'] ?> + param_url_redeem_owner_pad_addr_card);
        $("#div_redeem_owner_person_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $redeem_owner['person_addr_card_amphur_id'] ?> + "/" + <?php echo $redeem_owner['person_addr_card_district_id'] ?> + param_url_redeem_owner_pad_addr_card);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_pre for redeem_owner*/
    var redeem_owner_pad_id_addr_pre_list = 'province_id=redeem_owner_person_addr_pre_province_id&amphur_id=redeem_owner_person_addr_pre_amphur_id&district_id=redeem_owner_person_addr_pre_district_id';
    var redeem_owner_pad_name_addr_pre_list = 'province_name=redeem_owner[person_addr_pre_province_id]&amphur_name=redeem_owner[person_addr_pre_amphur_id]&district_name=redeem_owner[person_addr_pre_district_id]';
    var redeem_owner_pad_class_addr_pre_list = 'province_class=redeem_owner_person_addr_pre&amphur_class=redeem_owner_person_addr_pre&district_class=redeem_owner_person_addr_pre';
    var param_url_redeem_owner_pad_addr_pre = "/?" + redeem_owner_pad_id_addr_pre_list + "&" + redeem_owner_pad_name_addr_pre_list + "&" + redeem_owner_pad_class_addr_pre_list;

<?php if ($redeem_owner['person_addr_pre_amphur_id']): ?>
        $("#div_redeem_owner_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#redeem_owner_person_addr_pre_province_id").val() + "/" + <?php echo $redeem_owner['person_addr_pre_amphur_id'] ?> + param_url_redeem_owner_pad_addr_pre);
        $("#div_redeem_owner_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $redeem_owner['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $redeem_owner['person_addr_pre_district_id'] ?> + param_url_redeem_owner_pad_addr_pre);
<?php endif; ?>
</script>