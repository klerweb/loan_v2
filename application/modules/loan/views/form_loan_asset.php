<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_land_occupy"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_debt"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <?php if ($loan_asset_data): ?>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>รายการทรัพย์สิน</th>
                            <th>จำนวนเงิน (บาท)</th>
                            <th>แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($loan_asset_data as $key => $row): ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $row['loan_asset_name']; ?></td>
                                <td><?php echo amount_format($row['loan_asset_amount']); ?></td>
                                <td>
                                    <a href="<?php echo site_url("loan/form_loan_asset/?edit={$row['loan_asset_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url("loan/form_loan_asset/delete/{$row['loan_asset_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach;
                        ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->
        <?php endif; ?>


        <form id="form_loan_asset" name="form_loan_asset" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_loan_asset/save"); ?>">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $loan_data['loan_id']; ?>">
            <input type="hidden" name="loan_asset_id" id="loan_asset_id" value="<?php echo $form_data['loan_asset_id']; ?>">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายการทรัพย์สิน <span class="asterisk">*</span></label>
                        <input type="text" name="loan_asset_name" id="loan_asset_name" placeholder="รายการทรัพย์สิน" class="form-control" value="<?php echo $form_data['loan_asset_name'] ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนเงิน (บาท) <span class="asterisk">*</span></label>
                        <input type="text" min="0" name="loan_asset_amount" id="loan_asset_amount" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_asset_amount'] ?>" required>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวน <span class="asterisk">*</span></label>
                        <input type="number" min="0" name="loan_asset_quantity" id="loan_asset_quantity" placeholder="จำนวน" class="form-control" value="<?php echo $form_data['loan_asset_quantity'] ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หน่วยนับ <span class="asterisk">*</span></label>
                        <select class="form-control" id="loan_asset_uom" name="loan_asset_uom" required>
                            <option value="">หน่วยนับ</option>
                            <?php if ($mst['config_uom_list']): ?>
                                <?php foreach ($mst['config_uom_list'] as $data): ?>
                                    <option value="<?php echo $data['uom_id']; ?>" <?php if ($form_data['loan_asset_uom'] == $data['uom_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['uom_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->
