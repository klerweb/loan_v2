<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">                    
                    <a href="<?php echo site_url("loan/form_loan_asset"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_co"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <?php if ($loan_debt_data): ?>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>รายการหนี้สิน</th>
                            <th>หนี้เริ่มต้น (บาท)</th>
                            <th>หนี้คงเหลือ (บาท)</th>
                            <th>จำนวนหนี้ที่ต้องชำระต่อปี (บาท)</th>
                            <th>แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($loan_debt_data as $key => $row): ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $row['loan_debt_name']; ?></td>
                                <td><?php echo amount_format($row['loan_debt_amount_start']); ?></td>
                                <td><?php echo amount_format($row['loan_debt_amount_remain']); ?></td>
                                <td><?php echo amount_format($row['loan_debt_amount']); ?></td>
                                <td>
                                    <a href="<?php echo site_url("loan/form_loan_debt/?edit={$row['loan_debt_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url("loan/form_loan_debt/delete/{$row['loan_debt_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach;
                        ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->
        <?php endif; ?>


        <form id="form_loan_debt" name="form_loan_debt" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_loan_debt/save"); ?>">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $loan_data['loan_id']; ?>">
            <input type="hidden" name="loan_debt_id" id="loan_debt_id" value="<?php echo $form_data['loan_debt_id']; ?>">


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ระบุเจ้าของหนี้สิน <span class="asterisk">*</span></label>
                        <?php if ($mst['debt_owner_type_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['debt_owner_type_list'] as $key => $data): ?>
                                    <label><input type="radio" class="loan_debt_owner_type" name="loan_debt_owner_type" id="loan_debt_owner_type<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['loan_debt_owner_type'] == $key): ?>checked="checked"<?php endif; ?> required><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายการหนี้สิน <span class="asterisk">*</span></label>
                        <input type="text" name="loan_debt_name" id="loan_debt_name" placeholder="รายการหนี้สิน" class="form-control" value="<?php echo $form_data['loan_debt_name'] ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนหนี้ที่ต้องชำระต่อปี (บาท) <span class="asterisk">*</span></label>
                        <input type="text" min="0" name="loan_debt_amount" id="loan_debt_amount" placeholder="จำนวนหนี้ที่ต้องชำระต่อปี (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_debt_amount']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หนี้เริ่มต้น (บาท)<span class="asterisk">*</span></label>
                        <input type="text" min="0" name="loan_debt_amount_start" id="loan_debt_amount_start" placeholder="หนี้เริ่มต้น (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_debt_amount_start']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หนี้คงเหลือ (บาท) <span class="asterisk">*</span></label>
                        <input type="text" min="0" name="loan_debt_amount_remain" id="loan_debt_amount_remain" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_debt_amount_remain']; ?>" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ดอกเบี้ยร้อยละต่อปี</label>
                        <input type="text" min="0" name="loan_debt_interest" id="loan_debt_interest" placeholder="ดอกเบี้ยร้อยละต่อปี" class="form-control money_right" value="<?php echo $form_data['loan_debt_interest']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">            
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">                        
                        <label class="control-label">เป็นหนี้เมื่อ (วัน-เดือน-ปี : <?php echo date_th_now(); ?>) <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_debt_date_start" name="loan_debt_date_start" value="<?php echo $form_data['loan_debt_date_start'] ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สิ้นสุดเมื่อ (วัน-เดือน-ปี : <?php echo date_th_now(); ?>) <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_debt_date_end" name="loan_debt_date_end" value="<?php echo $form_data['loan_debt_date_end'] ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->
