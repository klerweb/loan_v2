<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->                

        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url($form_data['table_land_building_param']['callbackurl']); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <!--<a href="<?php echo site_url("loan/form_loan_doc"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>-->
                </div>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>
        <?php if ($session_form && $session_form['status'] == 'fail'): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <form id="form_loan" name="form_loan" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/save_land_list/?table_land_building_param={$form_data['table_land_building_param_encode']}"); ?>">            
            <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
            <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">
            <input type="hidden" name="owner_id" value="<?php echo $person_data['person_id']; ?>">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่ดิน</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เจ้าของที่ดินคำนำหน้าชื่อ <span class="asterisk">*</span></label>
                        <select class="form-control" id="land_owner_title_id" required readonly="readonly">
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>     

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                        <input type="text" id="land_owner_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" required readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                        <input type="text" id="land_owner_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" required readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทเอกสารสิทธิ <span class="asterisk">*</span></label>
                        <select class="form-control" id="land_type_id" name="land_type_id" required>
                            <option value="">ประเภทเอกสารสิทธิ</option>
                            <?php if ($mst['config_land_type_list']): ?>
                                <?php foreach ($mst['config_land_type_list'] as $data): ?>
                                    <option value="<?php echo $data['land_type_id']; ?>" <?php if ($form_data['land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ <span class="asterisk">*</span></label> 
                        <input type="number" name="land_no" id="land_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['land_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ดิน <span class="asterisk">*</span></label> 
                        <input type="number" name="land_addr_no" id="land_addr_no" placeholder="เลขที่ดิน" class="form-control" value="<?php echo $form_data['land_addr_no']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="number" name="land_addr_moo" id="land_addr_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['land_addr_moo']; ?>" maxlength="2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <select class="width100p" id="land_addr_province_id" name="land_addr_province_id" required>
                            <option value="">จังหวัด</option>
                            <?php if ($mst['master_province_list']): ?>
                                <?php foreach ($mst['master_province_list'] as $data): ?>
                                    <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label> 
                        <div id="div_land_addr_amphur_id">
                            <select class="width100p" id="land_addr_amphur_id" name="land_addr_amphur_id" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_land_addr_district_id">
                            <select class="width100p" id="land_addr_district_id" name="land_addr_district_id" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                    
                    </div>                      
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ไร่) <span class="asterisk">*</span></label>
                        <input type="number" min="0" name="land_area_rai" id="land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $form_data['land_area_rai']; ?>" required>                        
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(งาน) <span class="asterisk">*</span></label>
                        <input type="number" min="0" max="<?php echo get_max_ngan(); ?>" name="land_area_ngan" id="land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $form_data['land_area_ngan']; ?>" required>                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ตารางวา) <span class="asterisk">*</span></label>
                        <input type="number" min="0" max="<?php echo get_max_wa(); ?>" name="land_area_wah" id="land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $form_data['land_area_wah']; ?>" required>                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">มูลค่าที่ดิน <span class="asterisk">*</span></label> 
                        <input type="number" min="0" name="land_estimate_price" id="land_estimate_price" placeholder="ราคาประเมิณ" class="form-control money_right valid" value="<?php echo $form_data['land_estimate_price']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">&nbsp;</div>
            
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id&zipcode_id=land_addr_zipcode';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

<?php if ($form_data['land_addr_amphur_id']): ?>
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + "/" + <?php echo$form_data['land_addr_district_id'] ?> + param_url_pad_land_addr);
<?php endif; ?>
</script>