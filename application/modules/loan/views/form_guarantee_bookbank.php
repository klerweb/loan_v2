<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์</h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <form id="form_guarantee_bookbank" name="form_guarantee_bookbank" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_guarantee_bookbank/save"); ?>" enctype="multipart/form-data">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $loan_data['loan_id']; ?>">
            <input type="hidden" name="loan_bookbank_id" id="loan_bookbank_id" value="<?php echo $form_data['loan_bookbank_id']; ?>">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ - นามสกุล ของเจ้าของบัญชี <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bookbank_owner" id="loan_bookbank_owner" placeholder="ชื่อ - นามสกุล ของเจ้าของบัญชี" class="form-control" value="<?php echo $form_data['loan_bookbank_owner']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ธนาคาร/สถาบันการเงิน/สหกรณ์ <span class="asterisk">*</span></label>                        
                        <select class="form-control" id="bank_id" name="bank_id" required>
                            <option value="">ธนาคาร/สถาบันการเงิน/สหกรณ์</option>
                            <?php if ($config_bank): ?>
                                <?php foreach ($config_bank as $key => $data): ?>
                                    <option value="<?php echo $data['bank_id']; ?>" <?php if ($form_data['bank_id'] == $data['bank_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['bank_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สาขา <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bookbank_branch" id="loan_bookbank_branch" placeholder="สาขา" class="form-control" value="<?php echo $form_data['loan_bookbank_branch']; ?>" required>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่บัญชี <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bookbank_no" id="loan_bookbank_no" placeholder="เลขที่บัญชี" class="form-control" value="<?php echo $form_data['loan_bookbank_no']; ?>" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทบัญชี <span class="asterisk">*</span></label>                        
                        <select class="form-control" id="loan_bookbank_type" name="loan_bookbank_type" required>
                            <option value="">ประเภทบัญชี</option>
                            <?php if ($bookbank_type_list): ?>
                                <?php foreach ($bookbank_type_list as $key => $data): ?>
                                    <option value="<?php echo $key; ?>" <?php if ($form_data['loan_bookbank_type'] == $key): ?>selected="selected"<?php endif; ?>><?php echo $data; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ยอดเงินฝาก ณ วันที่ <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bookbank_date" id="loan_bookbank_date" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" class="form-control" value="<?php echo $form_data['loan_bookbank_date']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนเงิน(บาท) <span class="asterisk">*</span></label>
                        <input type="text" min="0" name="loan_bookbank_amount" id="loan_bookbank_amount" placeholder="ราคาที่ตราไว้" class="form-control money_right" value="<?php echo $form_data['loan_bookbank_amount']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            แนบไฟล์
                            <?php if (isset($form_data['loan_bookbank_path']) && $form_data['loan_bookbank_path']): ?>
                                <a href="<?php echo base_url($form_data['loan_bookbank_path']); ?>" target="_bank" class="">[ดูไฟล์เอกสาร]</a>                            
                            <?php endif; ?>
                        </label>
                        <input type="file" name="loan_bookbank_path" id="loan_bookbank_path" placeholder="แนบไฟล์" class="form-control">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->
