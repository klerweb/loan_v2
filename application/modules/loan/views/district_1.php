<?php $class = isset($district_class) ? $district_class : ''?>
<select name="<?php echo $district_name?>" id="<?php echo $district_id?>" data-placeholder="ตำบล/แขวง" class="width100p <?php echo $class;?>" required>
	<option value="">ตำบล/แขวง</option>
	<?php foreach ($district as $district_data) { ?>
		<option value="<?php echo $district_data['district_id']; ?>" <?php if($district_val == $district_data['district_id']):?>selected="selected"<?php endif;?>><?php echo $district_data['district_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#<?php echo $district_id?>").select2({
		minimumResultsForSearch: -1
	});
</script>