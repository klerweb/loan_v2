<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <form id="form_guarantee_bondsman" name="form_guarantee_bondsman" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_guarantee_bondsman/save"); ?>">
            <input type="hidden" name="person[person_id]" id="person_id" value="<?php echo $person_data['person_id']; ?>">
            <input type="hidden" name="loan_bondsman_id" id="loan_bondsman_id" value="<?php echo $form_data['loan_bondsman_id']; ?>">
			<input type="hidden" name="loan_person_thaiid" id="loan_person_thaiid" value="<?php echo $loan_person_data['person_thaiid'] ?>" />
			<input type="hidden" name="loan_spouse_thaiid" id="loan_spouse_thaiid" value="<?php echo $loan_spouse_data['person_thaiid'] ?>" />
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>บุคคลค้ำประกัน</h4>                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan" id="person_thaiid_form">
                        <label class="control-label">เลขที่บัตรประจำตัวประชาชน <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_thaiid]" id="person_thaiid" placeholder="เลขที่บัตรประจำตัวประชาชน" class="form-control" value="<?php echo $person_data['person_thaiid']; ?>" <?php if ($person_data['person_id']): ?>readonly="readonly"<?php endif; ?> maxlength="13" required>
                        <div id="thaiid_error" class="thaiid_error">Please enter format not correctly.</div>
                        <div id="thaiid_error2" class="thaiid_error">Please enter your unique Thai id.</div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันออกบัตร <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_card_startdate" name="person[person_card_startdate]" value="<?php echo $person_data['person_card_startdate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันบัตรหมดอายุ <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_card_expiredate" name="person[person_card_expiredate]" value="<?php echo $person_data['person_card_expiredate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                        <select class="form-control" id="title_id" name="person[title_id]" required>
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_fname]" id="person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                        <input type="text" name="person[person_lname]" id="person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันเกิด (วัน-เดือน-ปี : <?php echo date_th_now(); ?>) <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_birthdate" name="person[person_birthdate]" value="<?php echo $person_data['person_birthdate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ" id="person_age" value="" readonly="readonly">                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เพศ <span class="asterisk">*</span></label>
                        <?php if ($mst['master_sex_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['master_sex_list'] as $data): ?>
                                    <label><input type="radio" name="person[sex_id]" id="sex_id_<?php echo $data['sex_id']; ?>" value="<?php echo $data['sex_id']; ?>" <?php if ($person_data['sex_id'] == $data['sex_id']): ?>checked="checked"<?php endif; ?> required><?php echo $data['sex_name']; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เชื้อชาติ <span class="asterisk">*</span></label>
                        <select class="form-control" id="race_id" name="person[race_id]" required>
                            <option value="">เชื้อชาติ</option>
                            <?php if ($mst['config_race_list']): ?>
                                <?php foreach ($mst['config_race_list'] as $data): ?>
                                    <option value="<?php echo $data['race_id']; ?>" <?php if ($person_data['race_id'] == $data['race_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['race_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สัญชาติ <span class="asterisk">*</span></label>
                        <select class="form-control" id="nationality_id" name="person[nationality_id]" required>
                            <option value="">สัญชาติ</option>
                            <?php if ($mst['config_nationality_list']): ?>
                                <?php foreach ($mst['config_nationality_list'] as $data): ?>
                                    <option value="<?php echo $data['nationality_id']; ?>" <?php if ($person_data['nationality_id'] == $data['nationality_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['nationality_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ศาสนา <span class="asterisk">*</span></label>
                        <select class="form-control" id="religion_id" name="person[religion_id]" required>
                            <option value="">ศาสนา</option>
                            <?php if ($mst['config_religion_list']): ?>
                                <?php foreach ($mst['config_religion_list'] as $data): ?>
                                    <option value="<?php echo $data['religion_id']; ?>" <?php if ($person_data['religion_id'] == $data['religion_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['religion_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>



            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ที่อยู่ตามทะเบียนบ้าน</h4>                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ที่อยู่เลขที่ <span class="asterisk">*</span></label> 
                        <input type="text" name="person[person_addr_card_no]" id="person_addr_card_no" placeholder="ที่อยู่เลขที่" class="form-control" value="<?php echo $person_data['person_addr_card_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="text" name="person[person_addr_card_moo]" id="person_addr_card_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $person_data['person_addr_card_moo']; ?>" maxlength="2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="person[person_addr_card_road]" id="person_addr_card_road" placeholder="ถนน" class="form-control" value="<?php echo $person_data['person_addr_card_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <div id="div_person_addr_card_province_id">
                            <select class="width100p" id="person_addr_card_province_id" name="person[person_addr_card_province_id]" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_addr_card_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                        <div id="div_person_addr_card_amphur_id">
                            <select class="width100p" id="person_addr_card_amphur_id" name="person[person_addr_card_amphur_id]" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_addr_card_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_person_addr_card_district_id">
                            <select class="width100p" id="person_addr_card_district_id" name="person[person_addr_card_district_id]" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_addr_card_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>                    
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รหัสไปรษณีย์</label> 
                        <input type="text" name="person[person_addr_card_zipcode]" id="person_addr_card_zipcode" placeholder="รหัสไปรษณีย์" class="form-control" value="<?php echo $person_data['person_addr_card_zipcode']; ?>" readonly>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ที่อยู่ปัจจุบัน</h4>
                        <input type="hidden" id="addr_same_val" value="1">
                        <label><input type="checkbox" value="1" class="addr_same" id="person_addr_same" name="person[person_addr_same]">ตามที่ระบุไว้ในบัตรประชาชน</label>
                    </div>
                </div>
            </div>

            <div class="row div_addr_same ">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ที่อยู่เลขที่ <span class="asterisk">*</span></label> 
                        <input type="text" name="person[person_addr_pre_no]" id="person_addr_pre_no" placeholder="ที่อยู่เลขที่" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="text" name="person[person_addr_pre_moo]" id="person_addr_pre_moo" placeholder="หมู่ที่" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_moo']; ?>" maxlength="2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="person[person_addr_pre_road]" id="person_addr_pre_road" placeholder="ถนน" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row div_addr_same ">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <div id="div_person_addr_pre_province_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_province_id" name="person[person_addr_pre_province_id]" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label> 
                        <div id="div_person_addr_pre_amphur_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_amphur_id" name="person[person_addr_pre_amphur_id]" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_person_addr_pre_district_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_district_id" name="person[person_addr_pre_district_id]" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>                    
                </div>
            </div>
            
            <div class="row div_addr_same">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รหัสไปรษณีย์</label> 
                        <input type="text" name="person[person_addr_pre_zipcode]" id="person_addr_pre_zipcode" placeholder="รหัสไปรษณีย์" class="form-control" value="<?php echo $person_data['person_addr_pre_zipcode']; ?>" readonly>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์บ้าน</label> 
                        <input type="tel" pattern=".{9,9}" maxlength="9" title="9 characters" name="person[person_phone]" id="person_phone" placeholder="โทรศัพท์บ้าน" class="form-control" value="<?php echo $person_data['person_phone']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์มือถือ</label> 
                        <input type="tel" pattern=".{10,10}" maxlength="10" title="10 characters" name="person[person_mobile]" id="person_mobile" placeholder="โทรศัพท์มือถือ" class="form-control" value="<?php echo $person_data['person_mobile']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">E-mail</label> 
                        <input type="email" name="person[person_email]" id="person_email" placeholder="E-mail" class="form-control" value="<?php echo $person_data['person_email']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลอาชีพ</h4>                        
                    </div>
                </div>
            </div>

            <div class="row">               
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพ</label>
                        <select class="form-control" id="career_id" name="person[career_id]">
                            <option value="">อาชีพ</option>
                            <?php if ($mst['config_career_list']): ?>
                                <?php foreach ($mst['config_career_list'] as $data): ?>
                                    <option value="<?php echo $data['career_id']; ?>" <?php if ($person_data['career_id'] == $data['career_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['career_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพอื่นๆ (ระบุ)</label> 
                        <input type="text" name="person[person_career_other]" id="person_career_other" placeholder="อาชีพอื่นๆ (ระบุ)" class="form-control" value="<?php echo $person_data['person_career_other']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำแหน่ง</label> 
                        <input type="text" name="person[person_position]" id="person_position" placeholder="ตำแหน่ง" class="form-control" value="<?php echo $person_data['person_position']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ลักษณะงาน</label> 
                        <input type="text" name="person[person_job_desc]" id="person_job_desc" placeholder="ลักษณะงาน" class="form-control" value="<?php echo $person_data['person_job_desc']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ลักษณะธุรกิจ</label> 
                        <input type="text" name="person[person_type_business]" id="person_type_business" placeholder="ลักษณะธุรกิจ" class="form-control" value="<?php echo $person_data['person_type_business']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สังกัด</label> 
                        <input type="text" name="person[person_belong]" id="person_belong" placeholder="สังกัด" class="form-control" value="<?php echo $person_data['person_belong']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">กระทรวง</label> 
                        <input type="text" name="person[person_ministry]" id="person_ministry" placeholder="กระทรวง" class="form-control" value="<?php echo $person_data['person_ministry']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สถานที่ทำงาน</label> 
                        <input type="text" name="person[person_workplace]" id="person_workplace" placeholder="สถานที่ทำงาน" class="form-control" value="<?php echo $person_data['person_belong']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">บัตรข้าราชการเลขที่</label> 
                        <input type="text" name="person[person_cardid_emp]" id="person_cardid_emp" placeholder="บัตรข้าราชการเลขที่" class="form-control" value="<?php echo $person_data['person_cardid_emp']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันออกบัตร</label> 
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_cardid_emp_startdate" name="person[person_cardid_emp_startdate]" value="<?php echo $person_data['person_cardid_emp_startdate']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันบัตรหมดอายุ</label> 
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="person_cardid_emp_expiredate" name="person[person_cardid_emp_expiredate]" value="<?php echo $person_data['person_cardid_emp_expiredate']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายได้ต่อเดือน <span class="asterisk">*</span></label> 
                        <input type="number" min="0" name="person[person_income_per_month]" id="person_income_per_month" placeholder="รายได้ต่อเดือน" class="form-control" value="<?php echo $person_data['person_income_per_month']; ?>" required>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายจ่ายต่อเดือน</label> 
                        <input type="text" name="person[person_expenditure_per_month]" id="person_expenditure_per_month" placeholder="รายจ่ายต่อเดือน" class="form-control" value="<?php echo $person_data['person_expenditure_per_month']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <h4>ภาระผูกพันที่ต้องชำระหนี้เงินกู้อื่น</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <div class="radio">                            
                            <label><input type="radio" name="person[loan_other_check]" id="loan_other_0" value="0" <?php if (!$person_data['person_loan_other_per_month']): ?>checked="checked"<?php endif; ?> required>ไม่มีภาระผูกพัน<?php echo nbs(2); ?></label>
                            <label><input type="radio" name="person[loan_other_check]" id="loan_other_1" value="1" <?php if ($person_data['person_loan_other_per_month']): ?>checked="checked"<?php endif; ?> required>มีภาระผูกพัน<?php echo nbs(2); ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายเดือน</label> 
                        <input type="number" min="0" name="person[person_loan_other_per_month]" id="person_loan_other_per_month" placeholder="รายเดือน" class="form-control" value="<?php echo $person_data['person_loan_other_per_month']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายปี</label> 
                        <input type="number" min="0" name="person[person_loan_other_per_year]" id="person_loan_other_per_year" placeholder="รายปี" class="form-control" value="<?php echo $person_data['person_loan_other_per_year']; ?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <h4>ภาระผูกพันในการค้ำประกันหนี้เงินกู้อื่น</h4>                            
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <div class="radio">
                            <label><input type="radio" name="person[guarantee_other_check]" id="guarantee_other_0" value="0" <?php if (!$person_data['person_guarantee_other_amount']): ?>checked="checked"<?php endif; ?> required>ไม่มีภาระผูกพัน<?php echo nbs(2); ?></label>
                            <label><input type="radio" name="person[guarantee_other_check]" id="guarantee_other_1" value="1" <?php if ($person_data['person_guarantee_other_amount']): ?>checked="checked"<?php endif; ?> required>มีภาระผูกพัน<?php echo nbs(2); ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนเงิน</label> 
                        <input type="number" min="0" name="person[person_guarantee_other_amount]" id="person_guarantee_other_amount" placeholder="จำนวนเงิน" class="form-control" value="<?php echo $person_data['person_guarantee_other_amount']; ?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <h4>ประเภทผู้ค้ำประกัน</h4>                            
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทผู้ค้ำประกัน <span class="asterisk">*</span></label>
                        <select class="form-control" id="bondsman_type" name="bondsman_type" required>
                            <option value="">ประเภทผู้ค้ำประกัน</option>
                            <?php if ($mst['config_bondsman_type_list']): ?>
                                <?php foreach ($mst['config_bondsman_type_list'] as $data): ?>
                                    <option value="<?php echo $data['bondsman_type_id']; ?>" <?php if ($form_data['bondsman_type'] == $data['bondsman_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['bondsman_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>

        </form>
    </div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
    /*province, amphur, district addr_card for person*/
    var person_pad_id_addr_card_list = 'province_id=person_addr_card_province_id&amphur_id=person_addr_card_amphur_id&district_id=person_addr_card_district_id&zipcode_id=person_addr_card_zipcode';
    var person_pad_name_addr_card_list = 'province_name=person[person_addr_card_province_id]&amphur_name=person[person_addr_card_amphur_id]&district_name=person[person_addr_card_district_id]';

    //addr_card        
    var param_url_person_pad_addr_card = "/?" + person_pad_id_addr_card_list + "&" + person_pad_name_addr_card_list;
<?php if ($person_data['person_addr_card_amphur_id']): ?>
        $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_card_province_id").val() + "/" + <?php echo $person_data['person_addr_card_amphur_id'] ?> + param_url_person_pad_addr_card);
        $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_addr_card_amphur_id'] ?> + "/" + <?php echo $person_data['person_addr_card_district_id'] ?> + param_url_person_pad_addr_card);
<?php endif; ?>
</script>


<script type="text/javascript">
    /*province, amphur, district addr_pre for person*/
    var person_pad_id_addr_pre_list = 'province_id=person_addr_pre_province_id&amphur_id=person_addr_pre_amphur_id&district_id=person_addr_pre_district_id&zipcode_id=person_addr_pre_zipcode';
    var person_pad_name_addr_pre_list = 'province_name=person[person_addr_pre_province_id]&amphur_name=person[person_addr_pre_amphur_id]&district_name=person[person_addr_pre_district_id]';
    var person_pad_class_addr_pre_list = 'province_class=person_addr_pre&amphur_class=person_addr_pre&district_class=person_addr_pre';

    //addr_pre        
    var param_url_person_pad_addr_pre = "/?" + person_pad_id_addr_pre_list + "&" + person_pad_name_addr_pre_list + "&" + person_pad_class_addr_pre_list;
<?php if ($person_data['person_addr_pre_amphur_id']): ?>
        $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_pre_province_id").val() + "/" + <?php echo $person_data['person_addr_pre_amphur_id'] ?> + param_url_person_pad_addr_pre);
        $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $person_data['person_addr_pre_district_id'] ?> + param_url_person_pad_addr_pre);
<?php endif; ?>


    /*อื่นๆ = 5*/
<?php if ($person_data['career_id'] != 5): ?>
        $('#person_career_other').attr('readonly', true);
        $('#person_career_other').val('');
<?php endif; ?>

    /*career_id*/
    $("#career_id").change(function () {
        var career_id = $(this).val();

        /*อื่นๆ = 5*/
        if (career_id == 5) {
            $('#person_career_other').attr('readonly', false);
        } else {
            $('#person_career_other').attr('readonly', true);
            $('#person_career_other').val('');
        }
    });

<?php if ($person_data['person_loan_other_per_month']): ?>
        $('#person_loan_other_per_month').attr('readonly', false);
        $('#person_loan_other_per_year').attr('readonly', false);
<?php else: ?>
        $('#person_loan_other_per_month').attr('readonly', true);
        $('#person_loan_other_per_month').val('');
        $('#person_loan_other_per_year').attr('readonly', true);
        $('#person_loan_other_per_year').val('');
<?php endif; ?>

<?php if ($person_data['person_guarantee_other_amount']): ?>
        $('#person_guarantee_other_amount').attr('readonly', false);
<?php else: ?>
        $('#person_career_other').attr('readonly', true);
        $('#person_career_other').val('');
<?php endif; ?>
</script>


