<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->                

        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url($form_data['table_land_building_param']['callbackurl']); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <!--<a href="<?php echo site_url("loan/form_loan_doc"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>-->
                </div>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>
        <?php if ($session_form && $session_form['status'] == 'fail'): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <form id="form_loan" name="form_loan" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/save_building_list/?table_land_building_param={$form_data['table_land_building_param_encode']}"); ?>">            
            <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
            <input type="hidden" id="building_id" name="building_id" value="<?php echo $form_data['building_id']; ?>">
            <input type="hidden" name="owner_id" value="<?php echo $person_data['person_id']; ?>">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลสิ่งปลูกสร้าง</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เจ้าของสิ่งปลูกสร้าง</label>
                        <div class="radio">
                            <?php foreach ($mst['buiding_owner_status_list'] as $key => $data): ?>
                                <label><input type="radio" name="buiding_owner_status" class="buiding_owner_status" id="buiding_owner_status_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['buiding_owner_status'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เจ้าของสิ่งปลูกสร้างคำนำหน้าชื่อ</label>
                        <select class="form-control" id="building_owner_title_id" name="building_owner_title_id" readonly="readonly">
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>     

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ</label>
                        <input type="text" name="building_owner_fname" id="building_owner_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล</label>
                        <input type="text" name="building_owner_lname" id="building_owner_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทสิ่งปลูกสร้าง</label>
                        <select class="form-control" id="building_type_id" name="building_type_id" >
                            <option value="">ประเภทสิ่งปลูกสร้าง</option>
                            <?php if ($mst['config_building_type_list']): ?>
                                <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                    <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label> 
                        <input type="number" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="number" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาคาร</label> 
                        <input type="text" name="building_name" id="building_name" placeholder="อาคาร" class="form-control" value="<?php echo $form_data['building_name']; ?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ซอย</label> 
                        <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด</label>
                        <select class="width100p" id="building_province_id" name="building_province_id">
                            <option value="">จังหวัด</option>
                            <?php if ($mst['master_province_list']): ?>
                                <?php foreach ($mst['master_province_list'] as $data): ?>
                                    <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต</label>
                        <div id="div_building_amphur_id">
                            <select class="width100p" id="building_amphur_id" name="building_amphur_id">
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                        
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง</label>
                        <div id="div_building_district_id">
                            <select class="width100p" id="building_district_id" name="building_district_id">
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                    
                    </div>                    
                </div>
            </div>
			<div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">มูลค่าสิ่งปลูกสร้าง</label> 
                        <input type="number" min="0" name="building_estimate_price" id="building_estimate_price" placeholder="ราคาประเมิณ" class="form-control money_right valid" value="<?php echo $form_data['building_estimate_price']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>

            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
    /*province, amphur, district for building*/
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';
    var pad_required_building_list = 'province_required=&amphur_required=&district_required=';
    var param_url_pad_building = "/?" + pad_id_building_list + "&" + pad_name_building_list + "&" + pad_required_building_list;

<?php if ($form_data['building_amphur_id']): ?>
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + "/" + <?php echo $form_data['building_amphur_id'] ?> + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['building_amphur_id'] ?> + "/" + <?php echo$form_data['building_district_id'] ?> + param_url_pad_building);
<?php endif; ?>
</script>

<script>
    $(".buiding_owner_status").change(function () {
        var title_id = $(this).val();
        if (title_id == 0) { //ตนเอง
            $("#building_owner_title_id").val('<?php echo $person_data['title_id']; ?>');
            $("#building_owner_fname").val('<?php echo $person_data['person_fname']; ?>');
            $("#building_owner_lname").val('<?php echo $person_data['person_lname']; ?>');

            $('#building_owner_title_id').attr('readonly', true);
            $('#building_owner_fname').attr('readonly', true);
            $('#building_owner_lname').attr('readonly', true);
        } else {
            $("#building_owner_title_id").val('');
            $("#building_owner_fname").val('');
            $("#building_owner_lname").val('');

            $('#building_owner_title_id').attr('readonly', false);
            $('#building_owner_fname').attr('readonly', false);
            $('#building_owner_lname').attr('readonly', false);
        }
    });
</script>