<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>


        <form id="frm_person" name="form_person" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_person/save"); ?>">
            <input type="hidden" name="person_id" id="person_id" value="<?php echo $person_data['person_id']; ?>">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ค้นหาผู้ขอสินเชื่อ</h4>                        
                    </div>
                </div>
            </div>

            <div class="row">
                <?php if (!$person_data['person_id']): ?>
                    <div class="col-sm-6">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เลขที่บัตรประจำตัวประชาชน</label>
                            <input type="text" name="person_search" id="person_search" placeholder="เลขที่บัตรประจำตัวประชาชน" class="form-control" value="<?php echo $person_data['person_thaiid']; ?>" maxlength="13">                      
                            <br>
                            <button type="button" class="btn btn-primary" id="btn_person_search">ค้นหา</button>
                            <span class="res_search_thaiid alert-success" id="res_success_person_search">พบข้อมูล</span>
                            <span class="res_search_thaiid alert-danger" id="res_fail_person_search">ไม่พบข้อมูล</span>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="col-sm-6">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่บัตรประจำตัวประชาชน (คู่สมรส)</label>
                        <input type="text" name="spouse[person_search" id="spouse_search" placeholder="เลขที่บัตรประจำตัวประชาชน (คู่สมรส)" class="form-control" value="<?php echo $spouse_data['person_thaiid']; ?>" maxlength="13">                      
                        <br>
                        <button type="button" class="btn btn-primary" id="btn_spouse_search">ค้นหา</button>
                        <span class="res_search_thaiid alert-success" id="res_success_spouse_search">พบข้อมูล</span>
                        <span class="res_search_thaiid alert-danger" id="res_fail_spouse_search">ไม่พบข้อมูล</span>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ผู้ขอสินเชื่อ</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">คำนำหน้าชื่อ</label>
                        <select class="form-control" id="title_id" name="person[title_id]" required>
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ</label>
                        <input type="text" name="person[person_fname]" id="person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล</label>
                        <input type="text" name="person[person_lname]" id="person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" required>
                    </div>
                </div>                
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันเกิด (ปี-เดือน-วัน : 2017-01-01)</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="person_birthdate" name="person[person_birthdate]" value="<?php echo $person_data['person_birthdate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ" id="person_age" value="" readonly="readonly">                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เพศ</label>
                        <?php if ($mst['master_sex_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['master_sex_list'] as $data): ?>
                                    <label><input type="radio" name="person[sex_id]" id="sex_id_<?php echo $data['sex_id']; ?>" value="<?php echo $data['sex_id']; ?>" <?php if ($person_data['sex_id'] == $data['sex_id']): ?>checked="checked"<?php endif; ?> required><?php echo $data['sex_name']; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เชื้อชาติ</label>
                        <select class="form-control" id="race_id" name="person[race_id]" required>
                            <option value="">เชื้อชาติ</option>
                            <?php if ($mst['config_race_list']): ?>
                                <?php foreach ($mst['config_race_list'] as $data): ?>
                                    <option value="<?php echo $data['race_id']; ?>" <?php if ($person_data['race_id'] == $data['race_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['race_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สัญชาติ</label>
                        <select class="form-control" id="nationality_id" name="person[nationality_id]" required>
                            <option value="">สัญชาติ</option>
                            <?php if ($mst['config_nationality_list']): ?>
                                <?php foreach ($mst['config_nationality_list'] as $data): ?>
                                    <option value="<?php echo $data['nationality_id']; ?>" <?php if ($person_data['nationality_id'] == $data['nationality_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['nationality_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ศาสนา</label>
                        <select class="form-control" id="religion_id" name="person[religion_id]" required>
                            <option value="">ศาสนา</option>
                            <?php if ($mst['config_religion_list']): ?>
                                <?php foreach ($mst['config_religion_list'] as $data): ?>
                                    <option value="<?php echo $data['religion_id']; ?>" <?php if ($person_data['religion_id'] == $data['religion_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['religion_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่บัตรประจำตัวประชาชน</label>
                        <input type="text" name="person[person_thaiid]" id="person_thaiid" placeholder="เลขที่บัตรประจำตัวประชาชน" class="form-control" value="<?php echo $person_data['person_thaiid']; ?>" <?php if ($person_data['person_id']): ?>readonly="readonly"<?php endif; ?> maxlength="13" required>
                    </div>
                </div>       

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันออกบัตร</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="person_card_startdate" name="person[person_card_startdate]" value="<?php echo $person_data['person_card_startdate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันบัตรหมดอายุ</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="person_card_expiredate" name="person[person_card_expiredate]" value="<?php echo $person_data['person_card_expiredate']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>  
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ที่อยู่ตามทะเบียนบ้าน</h4>                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label> 
                        <input type="text" name="person[person_addr_card_no]" id="person_addr_card_no" placeholder="เลขที่" class="form-control" value="<?php echo $person_data['person_addr_card_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="text" name="person[person_addr_card_moo]" id="person_addr_card_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $person_data['person_addr_card_moo']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="person[person_addr_card_road]" id="person_addr_card_road" placeholder="ถนน" class="form-control" value="<?php echo $person_data['person_addr_card_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด</label>
                        <div id="div_person_addr_card_province_id">
                            <select class="width100p" id="person_addr_card_province_id" name="person[person_addr_card_province_id]" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_addr_card_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต</label>
                        <div id="div_person_addr_card_amphur_id">
                            <select class="width100p" id="person_addr_card_amphur_id" name="person[person_addr_card_amphur_id]" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_addr_card_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง</label>
                        <div id="div_person_addr_card_district_id">
                            <select class="width100p" id="person_addr_card_district_id" name="person[person_addr_card_district_id]" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_addr_card_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>                    
                </div>                
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ที่อยู่ปัจจุบัน</h4>
                        <label><input type="checkbox" value="1" id="person_addr_same" name="person[person_addr_same]">ตามที่ระบุไว้ในบัตรประชาชน</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label> 
                        <input type="text" name="person[person_addr_pre_no]" id="person_addr_pre_no" placeholder="เลขที่" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="text" name="person[person_addr_pre_moo]" id="person_addr_pre_moo" placeholder="หมู่ที่" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_moo']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="person[person_addr_pre_road]" id="person_addr_pre_road" placeholder="ถนน" class="form-control person_addr_pre" value="<?php echo $person_data['person_addr_pre_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด</label>
                        <div id="div_person_addr_pre_province_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_province_id" name="person[person_addr_pre_province_id]" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($person_data['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต</label>
                        <div id="div_person_addr_pre_amphur_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_amphur_id" name="person[person_addr_pre_amphur_id]" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($person_data['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง</label>
                        <div id="div_person_addr_pre_district_id">
                            <select class="width100p person_addr_pre" id="person_addr_pre_district_id" name="person[person_addr_pre_district_id]" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($person_data['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>                    
                </div>                
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์บ้าน</label> 
                        <input type="tel" name="person[person_phone]" id="person_phone" placeholder="โทรศัพท์บ้าน" class="form-control" value="<?php echo $person_data['person_phone']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์มือถือ</label> 
                        <input type="tel" name="person[person_mobile]" id="person_mobile" placeholder="โทรศัพท์มือถือ" class="form-control" value="<?php echo $person_data['person_mobile']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">E-mail</label> 
                        <input type="email" name="person[person_email]" id="person_email" placeholder="E-mail" class="form-control" value="<?php echo $person_data['person_email']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">               
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพ</label>
                        <select class="form-control" id="career_id" name="person[career_id]">
                            <option value="">อาชีพ</option>
                            <?php if ($mst['config_career_list']): ?>
                                <?php foreach ($mst['config_career_list'] as $data): ?>
                                    <option value="<?php echo $data['career_id']; ?>" <?php if ($person_data['career_id'] == $data['career_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['career_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพอื่นๆ (ระบุ)</label> 
                        <input type="text" name="person[person_career_other]" id="person_career_other" placeholder="อาชีพอื่นๆ (ระบุ)" class="form-control" value="<?php echo $person_data['person_career_other']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลคู่สมรส</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สถานภาพ</label> 
                        <?php if ($mst['master_status_married_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['master_status_married_list'] as $data): ?>
                                    <label><input type="radio" name="person[status_married_id]" id="status_married_id_<?php echo $data['status_married_id']; ?>" value="<?php echo $data['status_married_id']; ?>" <?php if ($person_data['status_married_id'] == $data['status_married_id']): ?>checked="checked"<?php endif; ?>><?php echo $data['status_married_name']; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">คำนำหน้าชื่อ</label>
                        <select class="form-control spouse_input" id="spouse_title_id" name="spouse[title_id]">
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($spouse_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ</label>
                        <input type="text" name="spouse[person_fname]" id="spouse_fname" placeholder="ชื่อ" class="form-control spouse_input" value="<?php echo $spouse_data['person_fname']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล</label>
                        <input type="text" name="spouse[person_lname]" id="spouse_lname" placeholder="นามสกุล" class="form-control spouse_input" value="<?php echo $spouse_data['person_lname']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันเกิด (ปี-เดือน-วัน : 2017-01-01)</label>
                        <div class="input-group">
                            <input type="text" class="form-control spouse_input" placeholder="YYYY-MM-DD" id="spouse_birthdate" name="spouse[person_birthdate] spouse_input" value="<?php echo $spouse_data['person_birthdate']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อายุ</label>
                        <input type="text" class="form-control spouse_input" placeholder="อายุ" id="spouse_age" name="อายุ" value="" readonly="readonly">                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เพศ</label>
                        <?php if ($mst['master_sex_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['master_sex_list'] as $data): ?>
                                <label><input type="radio" class="spouse_input" name="spouse[sex_id]" id="spouse_sex_id_<?php echo $data['sex_id']; ?>" value="<?php echo $data['sex_id']; ?>" <?php if ($spouse_data['sex_id'] == $data['sex_id']): ?>checked="checked"<?php endif; ?> ><?php echo $data['sex_name']; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เชื้อชาติ</label>
                        <select class="form-control spouse_input" id="spouse_race_id" name="spouse[race_id]">
                            <option value="">เชื้อชาติ</option>
                            <?php if ($mst['config_race_list']): ?>
                                <?php foreach ($mst['config_race_list'] as $data): ?>
                                    <option value="<?php echo $data['race_id']; ?>" <?php if ($spouse_data['race_id'] == $data['race_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['race_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">สัญชาติ</label>
                        <select class="form-control spouse_input" id="spouse_nationality_id" name="spouse[nationality_id]">
                            <option value="">สัญชาติ</option>
                            <?php if ($mst['config_nationality_list']): ?>
                                <?php foreach ($mst['config_nationality_list'] as $data): ?>
                                    <option value="<?php echo $data['nationality_id']; ?>" <?php if ($spouse_data['nationality_id'] == $data['nationality_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['nationality_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ศาสนา</label>
                        <select class="form-control spouse_input" id="spouse_religion_id" name="spouse[religion_id]">
                            <option value="">ศาสนา</option>
                            <?php if ($mst['config_religion_list']): ?>
                                <?php foreach ($mst['config_religion_list'] as $data): ?>
                                    <option value="<?php echo $data['religion_id']; ?>" <?php if ($spouse_data['religion_id'] == $data['religion_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['religion_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่บัตรประจำตัวประชาชน</label>
                        <input type="text" name="spouse[person_thaiid]" id="spouse_thaiid" placeholder="เลขที่บัตรประจำตัวประชาชน" class="form-control spouse_input" value="<?php echo $spouse_data['person_thaiid']; ?>"  maxlength="13">
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันออกบัตร</label>
                        <div class="input-group">
                            <input type="text" class="form-control spouse_input" placeholder="YYYY-MM-DD" id="spouse_card_startdate" name="spouse[person_card_startdate]" value="<?php echo $spouse_data['person_card_startdate']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันบัตรหมดอายุ</label>
                        <div class="input-group">

                            <input type="text" class="form-control spouse_input" placeholder="YYYY-MM-DD" id="spouse_card_expiredate" name="spouse[person_card_expiredate]" value="<?php echo $spouse_data['person_card_expiredate']; ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>                
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ที่อยู่ตามทะเบียนบ้าน</h4>                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label> 
                        <input type="text" name="spouse[person_addr_card_no]" id="spouse_addr_card_no" placeholder="เลขที่" class="form-control spouse_input" value="<?php echo $spouse_data['person_addr_card_no']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="text" name="spouse[person_addr_card_moo]" id="spouse_addr_card_moo" placeholder="หมู่ที่" class="form-control spouse_input" value="<?php echo $spouse_data['person_addr_card_moo']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="spouse[person_addr_card_road]" id="spouse_addr_card_road" placeholder="ถนน" class="form-control spouse_input" value="<?php echo $spouse_data['person_addr_card_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด</label>
                        <div id="div_spouse_addr_card_province_id">
                            <select class="width100p spouse_addr_card" id="spouse_addr_card_province_id" name="spouse[person_addr_card_province_id]">
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($spouse_data['person_addr_card_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต</label>
                        <div id="div_spouse_addr_card_amphur_id">
                            <select class="width100p spouse_addr_card" id="spouse_addr_card_amphur_id" name="spouse[person_addr_card_amphur_id]">
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($spouse_data['person_addr_card_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง</label>
                        <div id="div_spouse_addr_card_district_id">
                            <select class="width100p spouse_addr_card" id="spouse_addr_card_district_id" name="spouse[person_addr_card_district_id]">
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($spouse_data['person_addr_card_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>                            
                    </div>                    
                </div>                
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ที่อยู่ปัจจุบัน</h4>
                        <label><input type="checkbox" value="1" id="spouse_addr_same" name="spouse[person_addr_same]" class="spouse_input">ตามที่ระบุไว้ในบัตรประชาชน</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่</label> 
                        <input type="text" name="spouse[person_addr_pre_no]" id="spouse_addr_pre_no" placeholder="เลขที่" class="form-control spouse_addr_pre spouse_input" value="<?php echo $spouse_data['person_addr_pre_no']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label> 
                        <input type="text" name="spouse[person_addr_pre_moo]" id="spouse_addr_pre_moo" placeholder="หมู่ที่" class="form-control spouse_addr_pre spouse_input" value="<?php echo $spouse_data['person_addr_pre_moo']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ถนน</label> 
                        <input type="text" name="spouse[person_addr_pre_road]" id="spouse_addr_pre_road" placeholder="ถนน" class="form-control spouse_addr_pre spouse_input" value="<?php echo $spouse_data['person_addr_pre_road']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด</label>
                        <div id="div_spouse_addr_pre_province_id">
                            <select class="width100p spouse_addr_pre" id="spouse_addr_pre_province_id" name="spouse[person_addr_pre_province_id]">
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($spouse_data['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต</label> 
                        <div id="div_spouse_addr_pre_amphur_id">
                            <div id="div_spouse_addr_pre_amphur_id">
                                <select class="width100p spouse_addr_pre" id="spouse_addr_pre_amphur_id" name="spouse[person_addr_pre_amphur_id]">
                                    <option value="">อำเภอ/เขต</option>
                                    <?php if ($mst['master_amphur_list']): ?>
                                        <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                            <option value="<?php echo $data['amphur_id']; ?>" <?php if ($spouse_data['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>                                
                        </div>                            
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง</label>
                        <div id="div_spouse_addr_pre_district_id">
                            <div id="div_spouse_addr_pre_district_id">
                                <select class="width100p spouse_addr_pre" id="spouse_addr_pre_district_id" name="spouse[person_addr_pre_district_id]">
                                    <option value="">ตำบล/แขวง</option>
                                    <?php if ($mst['master_district_list']): ?>
                                        <?php foreach ($mst['master_district_list'] as $data): ?>
                                            <option value="<?php echo $data['district_id']; ?>" <?php if ($spouse_data['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>                                
                        </div>                            
                    </div>                    
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์บ้าน</label> 
                        <input type="tel" name="spouse[person_phone]" id="spouse_phone" placeholder="โทรศัพท์บ้าน" class="form-control spouse_input" value="<?php echo $spouse_data['person_phone']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">โทรศัพท์มือถือ</label> 
                        <input type="tel" name="spouse[person_mobile]" id="spouse_mobile" placeholder="โทรศัพท์มือถือ" class="form-control spouse_input" value="<?php echo $spouse_data['person_mobile']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">E-mail</label> 
                        <input type="email" name="spouse[person_email]" id="spouse_email" placeholder="E-mail" class="form-control spouse_input" value="<?php echo $spouse_data['person_email']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">               
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพ</label>
                        <select class="form-control spouse_input" id="spouse_career_id" name="spouse[career_id]">
                            <option value="">อาชีพ</option>
                            <?php if ($mst['config_career_list']): ?>
                                <?php foreach ($mst['config_career_list'] as $data): ?>
                                    <option value="<?php echo $data['career_id']; ?>" <?php if ($spouse_data['career_id'] == $data['career_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['career_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อาชีพอื่นๆ (ระบุ)</label> 
                        <input type="text" name="spouse[person_career_other]" id="spouse_career_other" placeholder="อาชีพอื่นๆ (ระบุ)" class="form-control spouse_input" value="<?php echo $spouse_data['person_career_other']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลสมาชิก</h4>                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนสมาชิกในครัวเรือน (คน)</label> 
                        <input type="text" name="loan_member_amount" id="loan_member_amount" placeholder="จำนวนสมาชิกในครัวเรือน" class="form-control" value="<?php echo $loan_data['loan_member_amount']; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนแรงงานในครัวเรือน (คน)</label> 
                        <input type="text" name="loan_labor_amount" id="loan_labor_amount" placeholder="จำนวนแรงงานในครัวเรือน" class="form-control" value="<?php echo $loan_data['loan_labor_amount']; ?>">
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>

                <!--back & next page-->
                <div class="col-sm-8 right">
                    <div class="form-group form-group-loan">
                        <a href="<?php echo site_url("loan/edit_loan/{$loan_data['loan_id']}"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                        <a href="<?php echo site_url("loan/form_land"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
    /*province, amphur, district addr_card for person*/
    var person_pad_id_addr_card_list = 'province_id=person_addr_card_province_id&amphur_id=person_addr_card_amphur_id&district_id=person_addr_card_district_id';
    var person_pad_name_addr_card_list = 'province_name=person[person_addr_card_province_id]&amphur_name=person[person_addr_card_amphur_id]&district_name=person[person_addr_card_district_id]';
    var person_pad_class_addr_card_list = 'province_class=person_addr_card&amphur_class=person_addr_card&district_class=person_addr_card';

    //addr_card        
    var param_url_person_pad_addr_card = "/?" + person_pad_id_addr_card_list + "&" + person_pad_name_addr_card_list + "&" + person_pad_class_addr_card_list;
<?php if ($person_data['person_addr_card_amphur_id']): ?>
        $("#div_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_card_province_id").val() + "/" + <?php echo $person_data['person_addr_card_amphur_id'] ?> + param_url_person_pad_addr_card);
        $("#div_person_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_addr_card_amphur_id'] ?> + "/" + <?php echo $person_data['person_addr_card_district_id'] ?> + param_url_person_pad_addr_card);
<?php endif; ?>
</script>


<script type="text/javascript">
    /*province, amphur, district addr_pre for person*/
    var person_pad_id_addr_pre_list = 'province_id=person_addr_pre_province_id&amphur_id=person_addr_pre_amphur_id&district_id=person_addr_pre_district_id';
    var person_pad_name_addr_pre_list = 'province_name=person[person_addr_pre_province_id]&amphur_name=person[person_addr_pre_amphur_id]&district_name=person[person_addr_pre_district_id]';
    var person_pad_class_addr_pre_list = 'province_class=person_addr_pre&amphur_class=person_addr_pre&district_class=person_addr_pre';

    //addr_pre        
    var param_url_person_pad_addr_pre = "/?" + person_pad_id_addr_pre_list + "&" + person_pad_name_addr_pre_list + "&" + person_pad_class_addr_pre_list;
<?php if ($person_data['person_addr_pre_amphur_id']): ?>
        $("#div_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#person_addr_pre_province_id").val() + "/" + <?php echo $person_data['person_addr_pre_amphur_id'] ?> + param_url_person_pad_addr_pre);
        $("#div_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $person_data['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $person_data['person_addr_pre_district_id'] ?> + param_url_person_pad_addr_pre);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_card for spouse*/
    var spouse_pad_id_addr_card_list = 'province_id=spouse_addr_card_province_id&amphur_id=spouse_addr_card_amphur_id&district_id=spouse_addr_card_district_id';
    var spouse_pad_name_addr_card_list = 'province_name=spouse[personaddr_card_province_id]&amphur_name=spouse[person_addr_card_amphur_id]&district_name=spouse[person_addr_card_district_id]';
    var spouse_pad_class_addr_card_list = 'province_class=spouse_addr_card&amphur_class=spouse_addr_card&district_class=spouse_addr_card';

    //addr_card        
    var param_url_spouse_pad_addr_card = "/?" + spouse_pad_id_addr_card_list + "&" + spouse_pad_name_addr_card_list + "&" + spouse_pad_class_addr_card_list;
<?php if ($spouse_data['person_addr_card_amphur_id']): ?>
        $("#div_spouse_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#spouse_addr_card_province_id").val() + "/" + <?php echo $spouse_data['person_addr_card_amphur_id'] ?> + param_url_spouse_pad_addr_card);
        $("#div_spouse_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $spouse_data['person_addr_card_amphur_id'] ?> + "/" + <?php echo $spouse_data['person_addr_card_district_id'] ?> + param_url_spouse_pad_addr_card);
<?php endif; ?>
</script>


<script type="text/javascript">
    /*province, amphur, district addr_pre for spouse*/
    var spouse_pad_id_addr_pre_list = 'province_id=spouse_addr_pre_province_id&amphur_id=spouse_addr_pre_amphur_id&district_id=spouse_addr_pre_district_id';
    var spouse_pad_name_addr_pre_list = 'province_name=spouse[person_addr_pre_province_id]&amphur_name=spouse[person_addr_pre_amphur_id]&district_name=spouse[person_addr_pre_district_id]';
    var spouse_pad_class_addr_pre_list = 'province_class=spouse_addr_pre&amphur_class=spouse_addr_pre&district_class=spouse_addr_pre';

    //addr_pre        
    var param_url_spouse_pad_addr_pre = "/?" + spouse_pad_id_addr_pre_list + "&" + spouse_pad_name_addr_pre_list + "&" + spouse_pad_class_addr_pre_list;
<?php if ($spouse_data['person_addr_pre_amphur_id']): ?>
        $("#div_spouse_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#spouse_addr_pre_province_id").val() + "/" + <?php echo $spouse_data['person_addr_pre_amphur_id'] ?> + param_url_spouse_pad_addr_pre);
        $("#div_spouse_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $spouse_data['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $spouse_data['person_addr_pre_district_id'] ?> + param_url_spouse_pad_addr_pre);
<?php endif; ?>
</script>