<form id="form_loan" name="form_loan" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land/save_land5"); ?>">
    <input type="hidden" id="loan_objective_id" name="loan_objective_id" value="<?php echo $form_data['loan_objective_id']; ?>">
    <!--<input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">-->
    <input type="hidden" id="land_id" name="land_id" value="">
    <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลการประกอบอาชีพเกษตรกรรม</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <label class="control-label">ระบุประเภทของเกษตรกรรม <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_farm_for" id="loan_type_farm_for" placeholder="ระบุประเภทของเกษตรกรรม" required><?php echo $form_data['loan_type_farm_for'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="row">        
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <label class="control-label">สถานที่ <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_farm_location" id="loan_type_farm_location" placeholder="สถานที่" required><?php echo $form_data['loan_type_farm_location'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <label class="control-label">โครงการที่นำเสนอ <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_farm_project" id="loan_type_farm_project" placeholder="ระบุประเภทของเกษตรกรรม" required><?php echo $form_data['loan_type_farm_project'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="row">        
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <label class="control-label">โดยนำไปใช้เป็นค่าใช้จ่าย <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_farm_expenditure" id="loan_type_farm_expenditure" placeholder="สถานที่" required><?php echo $form_data['loan_type_farm_expenditure'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group form-group-loan">
                <label class="control-label">คาดว่าจะมีรายได้จากโครงการที่เสนอ ตั้งแต่ปีที่ <span class="asterisk">*</span></label> 
                <!--<input type="text" name="loan_type_farm_income_yearstart" id="loan_type_farm_income_yearstart" placeholder="คาดว่าจะมีรายได้จากโครงการที่เสนอ ตั้งแต่ปีที่" class="form-control" value="<?php echo $form_data['loan_type_farm_income_yearstart']; ?>" required>-->
                <select class="form-control" id="loan_type_farm_income_yearstart" name="loan_type_farm_income_yearstart" required>
                    <option value="">ตั้งแต่ปีที่</option>
                    <?php if ($mst['year_list']): ?>
                        <?php foreach ($mst['year_list'] as $data): ?>
                            <option value="<?php echo $data; ?>" <?php if ($form_data['loan_type_farm_income_yearstart'] == $data): ?>selected="selected"<?php endif; ?>><?php echo $data; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-group-loan">
                <label class="control-label">รายได้สุทธิปีละ <span class="asterisk">*</span></label> 
                <input type="text" min="0" name="loan_type_farm_income_per_year" id="loan_type_farm_income_per_year" placeholder="รายได้สุทธิปีละ" class="form-control money_right" value="<?php echo $form_data['loan_type_farm_income_per_year']; ?>" required>               
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group form-group-loan">
                <label class="control-label">เริ่มชำระคืนต้นเงินและดอกเบี้ย ตั้งแต่ปีที่ <span class="asterisk">*</span></label> 
                <!--<input type="text" name="loan_type_farm_payment_yearstart" id="loan_type_farm_payment_yearstart" placeholder="เริ่มชำระคืนต้นเงินและดอกเบี้ย ตั้งแต่ปีที่" class="form-control" value="<?php echo $form_data['loan_type_farm_payment_yearstart']; ?>" required>-->
                <select class="form-control" id="loan_type_farm_payment_yearstart" name="loan_type_farm_payment_yearstart" required>
                    <option value="">ตั้งแต่ปีที่</option>
                    <?php if ($mst['year_list']): ?>
                        <?php foreach ($mst['year_list'] as $data): ?>
                            <option value="<?php echo $data; ?>" <?php if ($form_data['loan_type_farm_payment_yearstart'] == $data): ?>selected="selected"<?php endif; ?>><?php echo $data; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group form-group-loan">
                <label class="control-label">จำนวนเงินที่ชำระ <span class="asterisk">*</span></label> 
                <input type="text" min="0" name="loan_type_farm_payment_amount" id="loan_type_farm_payment_amount" placeholder="รายได้สุทธิปีละ" class="form-control money_right" value="<?php echo $form_data['loan_type_farm_payment_amount']; ?>" required>               
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group form-group-loan">
                <label class="control-label">ภายในกำหนดเวลา <span class="asterisk">*</span></label> 
                <input type="number" min="0" name="loan_type_farm_year_amount" id="loan_type_farm_year_amount" placeholder="ภายในกำหนดเวลา" class="form-control" value="<?php echo $form_data['loan_type_farm_year_amount']; ?>" required>
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-12 right">
            <div class="form-group form-group-loan">
                <button class="btn btn-primary mr5">บันทึก</button>
                <button type="reset" class="btn btn-default">ยกเลิก</button>
            </div>
        </div>
    </div>
</form>