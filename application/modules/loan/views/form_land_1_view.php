<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">วัตถุประสงค์การขอสินเชื่อ : </label>
                    <?php echo view_value($form_data['loan_objective_name']); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">การไถ่ถอนที่ดินจาก : </label>
                    <?php echo view_value($form_data['loan_type_redeem_type_title']); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-8">
                <div class="form-group form-group-loan">
                    <label class="control-label">สาเหตุของการนำที่ดินไปจำนองหรือขายฝาก : </label>
                    <textarea class="form-control" rows="5" maxlength="200" name="loan_type_redeem_case" id="loan_type_redeem_case" placeholder="สาเหตุของการนำที่ดินไปจำนองหรือขายฝาก" required><?php echo $form_data['loan_type_redeem_case'] ?></textarea>
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ข้อมูลที่ดิน</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภท : </label>
                    <?php echo view_value($form_data['land_type_name'])?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่ : </label>
                    <?php echo view_value($form_data['land_no'])?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่ดิน : </label>
                    <?php echo view_value($form_data['land_addr_no'])?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่ : </label>
                    <?php echo view_value($form_data['land_addr_no'])?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด : </label>
                    <?php echo view_value($form_data['land_province_name'])?>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต : </label> 
                    <?php echo view_value($form_data['land_amphur_name'])?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง : </label>
                    <?php echo view_value($form_data['land_amphur_name'])?>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เนื้อที่(ไร่) : </label>
                    <?php echo view_value($form_data['land_area_rai'])?> ไร่
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เนื้อที่(งาน) : </label>
                    <?php echo view_value($form_data['land_area_ngan'])?> งาน
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เนื้อที่(ตารางวา) : </label>
                    <?php echo view_value($form_data['land_area_wah'])?> ตารางวา
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ข้อมูลเจ้าของที่ดิน</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">คำนำหน้าชื่อ</label>
                    <select class="form-control" id="redeem_owner_title_id" name="redeem_owner[title_id]" required>
                        <option value="">คำนำหน้าชื่อ</option>
                        <?php if ($mst['config_title_list']): ?>
                            <?php foreach ($mst['config_title_list'] as $data): ?>
                                <option value="<?php echo $data['title_id']; ?>" <?php if ($redeem_owner['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>     

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ชื่อ</label>
                    <input type="text" name="redeem_owner[person_fname]" id="redeem_owner_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $redeem_owner['person_fname']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">นามสกุล</label>
                    <input type="text" name="redeem_owner[person_lname]" id="redeem_owner_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $redeem_owner['person_lname']; ?>" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">วันเกิด (ปี-เดือน-วัน : 2017-01-01)</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="redeem_owner_person_birthdate" name="redeem_owner[person_birthdate]" value="<?php echo $redeem_owner['person_birthdate']; ?>" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อายุ</label>
                    <input type="text" class="form-control" placeholder="อายุ" id="redeem_owner_person_age" value="" readonly="readonly">                            
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เพศ</label>
                    <!--
                    <?php if ($mst['master_sex_list']): ?>
                        <div class="radio">
                            <?php foreach ($mst['master_sex_list'] as $data): ?>
                                <label><input type="radio" name="redeem_owner[sex_id]" id="redeem_owner_sex_id_<?php echo $data['sex_id']; ?>" value="<?php echo $data['sex_id']; ?>" <?php if ($redeem_owner['sex_id'] == $data['sex_id']): ?>checked="checked"<?php endif; ?> required><?php echo $data['sex_name']; ?><?php echo nbs(2); ?></label>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    -->
                </div>
            </div> 
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เชื้อชาติ</label>
                    <select class="form-control" id="redeem_owner_race_id" name="redeem_owner[race_id]" required>
                        <option value="">เชื้อชาติ</option>
                        <?php if ($mst['config_race_list']): ?>
                            <?php foreach ($mst['config_race_list'] as $data): ?>
                                <option value="<?php echo $data['race_id']; ?>" <?php if ($redeem_owner['race_id'] == $data['race_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['race_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">สัญชาติ</label>
                    <select class="form-control" id="redeem_owner_nationality_id" name="redeem_owner[nationality_id]" required>
                        <option value="">สัญชาติ</option>
                        <?php if ($mst['config_nationality_list']): ?>
                            <?php foreach ($mst['config_nationality_list'] as $data): ?>
                                <option value="<?php echo $data['nationality_id']; ?>" <?php if ($redeem_owner['nationality_id'] == $data['nationality_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['nationality_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ศาสนา</label>
                    <select class="form-control" id="redeem_owner_religion_id" name="redeem_owner[religion_id]" required>
                        <option value="">ศาสนา</option>
                        <?php if ($mst['config_religion_list']): ?>
                            <?php foreach ($mst['config_religion_list'] as $data): ?>
                                <option value="<?php echo $data['religion_id']; ?>" <?php if ($redeem_owner['religion_id'] == $data['religion_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['religion_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่บัตรประจำตัวประชาชน</label>
                    <input type="text" name="redeem_owner[person_thaiid]" id="redeem_owner_person_thaiid" placeholder="เลขที่บัตรประจำตัวประชาชน" class="form-control" value="<?php echo $redeem_owner['person_thaiid']; ?>" <?php if ($redeem_owner['person_id']): ?>readonly="readonly"<?php endif; ?> maxlength="13" required>
                </div>
            </div> 

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">วันออกบัตร</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="redeem_owner_person_card_startdate" name="redeem_owner[person_card_startdate]" value="<?php echo $redeem_owner['person_card_startdate']; ?>" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">วันบัตรหมดอายุ</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="YYYY-MM-DD" id="redeem_owner_person_card_expiredate" name="redeem_owner[person_card_expiredate]" value="<?php echo $redeem_owner['person_card_expiredate']; ?>" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>                
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ที่อยู่ตามทะเบียนบ้าน</h4>                        
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่</label> 
                    <input type="text" name="redeem_owner[person_addr_card_no]" id="redeem_owner_person_addr_card_no" placeholder="เลขที่" class="form-control" value="<?php echo $redeem_owner['person_addr_card_no']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label> 
                    <input type="text" name="redeem_owner[person_addr_card_moo]" id="redeem_owner_person_addr_card_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $redeem_owner['person_addr_card_moo']; ?>" maxlength="2">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label> 
                    <input type="text" name="redeem_owner[person_addr_card_road]" id="redeem_owner_person_addr_card_road" placeholder="ถนน" class="form-control" value="<?php echo $redeem_owner['person_addr_card_road']; ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด</label>
                    <div id="div_redeem_owner_person_addr_card_province_id">
                        <select class="width100p" id="redeem_owner_person_addr_card_province_id" name="redeem_owner[person_addr_card_province_id]" required>
                            <option value="">จังหวัด</option>
                            <?php if ($mst['master_province_list']): ?>
                                <?php foreach ($mst['master_province_list'] as $data): ?>
                                    <option value="<?php echo $data['province_id']; ?>" <?php if ($redeem_owner['person_addr_card_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต</label>
                    <div id="div_redeem_owner_person_addr_card_amphur_id">
                        <select class="width100p" id="redeem_owner_person_addr_card_amphur_id" name="redeem_owner[person_addr_card_amphur_id]" required>
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($redeem_owner['person_addr_card_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง</label>
                    <div id="div_redeem_owner_person_addr_card_district_id">
                        <select class="width100p" id="redeem_owner_person_addr_card_district_id" name="redeem_owner[person_addr_card_district_id]" required>
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($redeem_owner['person_addr_card_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>                    
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ที่อยู่ปัจจุบัน</h4>
                    <label><input type="checkbox" value="1" id="redeem_owner_person_addr_same" name="redeem_owner[person_addr_same]">ตามที่ระบุไว้ในบัตรประชาชน</label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่</label> 
                    <input type="text" name="redeem_owner[person_addr_pre_no]" id="redeem_owner_person_addr_pre_no" placeholder="เลขที่" class="form-control redeem_owner_person_addr_pre" value="<?php echo $redeem_owner['person_addr_pre_no']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label> 
                    <input type="text" name="redeem_owner[person_addr_pre_moo]" id="redeem_owner_person_addr_pre_moo" placeholder="หมู่ที่" class="form-control redeem_owner_person_addr_pre" value="<?php echo $redeem_owner['person_addr_pre_moo']; ?>" maxlength="2">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label> 
                    <input type="text" name="redeem_owner[person_addr_pre_road]" id="redeem_owner_person_addr_pre_road" placeholder="ถนน" class="form-control redeem_owner_person_addr_pre" value="<?php echo $redeem_owner['person_addr_pre_road']; ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด</label>
                    <div id="div_redeem_owner_person_addr_pre_province_id">
                        <select class="width100p redeem_owner_person_addr_pre" id="redeem_owner_person_addr_pre_province_id" name="redeem_owner[person_addr_pre_province_id]" required>
                            <option value="">จังหวัด</option>
                            <?php if ($mst['master_province_list']): ?>
                                <?php foreach ($mst['master_province_list'] as $data): ?>
                                    <option value="<?php echo $data['province_id']; ?>" <?php if ($redeem_owner['person_addr_pre_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต</label> 
                    <div id="div_redeem_owner_person_addr_pre_amphur_id">
                        <select class="width100p redeem_owner_person_addr_pre" id="redeem_owner_person_addr_pre_amphur_id" name="redeem_owner[person_addr_pre_amphur_id]" required>
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($redeem_owner['person_addr_pre_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง</label>
                    <div id="div_redeem_owner_person_addr_pre_district_id">
                        <select class="width100p redeem_owner_person_addr_pre" id="redeem_owner_person_addr_pre_district_id" name="redeem_owner[person_addr_pre_district_id]" required>
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($redeem_owner['person_addr_pre_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>                    
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">โทรศัพท์บ้าน</label> 
                    <input type="tel" name="redeem_owner[person_phone]" id="redeem_owner_person_phone" placeholder="โทรศัพท์บ้าน" class="form-control" value="<?php echo $redeem_owner['person_phone']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">โทรศัพท์มือถือ</label> 
                    <input type="tel" name="redeem_owner[person_mobile]" id="redeem_owner_person_mobile" placeholder="โทรศัพท์มือถือ" class="form-control" value="<?php echo $redeem_owner['person_mobile']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">E-mail</label> 
                    <input type="email" name="redeem_owner[person_email]" id="redeem_owner_person_email" placeholder="E-mail" class="form-control" value="<?php echo $redeem_owner['person_email']; ?>">
                </div>
            </div>
        </div>

        <div class="row">               
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อาชีพ</label>
                    <select class="form-control" id="redeem_owner_career_id" name="redeem_owner[career_id]">
                        <option value="">อาชีพ</option>
                        <?php if ($mst['config_career_list']): ?>
                            <?php foreach ($mst['config_career_list'] as $data): ?>
                                <option value="<?php echo $data['career_id']; ?>" <?php if ($redeem_owner['career_id'] == $data['career_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['career_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อาชีพอื่นๆ (ระบุ)</label> 
                    <input type="text" name="redeem_owner[person_career_other]" id="redeem_owner_person_career_other" placeholder="อาชีพอื่นๆ (ระบุ)" class="form-control" value="<?php echo $redeem_owner['person_career_other']; ?>">
                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ความสัมพันธ์กับผู้ขอสินเชื่อ</h4>
                </div>
            </div>
        </div>

        <div class="row">               
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ความสัมพันธ์กับผู้ขอสินเชื่อ</label>
                    <select class="form-control" id="loan_type_redeem_relationship" name="loan_type_redeem_relationship" required>
                        <option value="">ความสัมพันธ์กับผู้ขอสินเชื่อ</option>
                        <?php if ($mst['config_relationship_list']): ?>
                            <?php foreach ($mst['config_relationship_list'] as $data): ?>
                                <option value="<?php echo $data['relationship_id']; ?>" <?php if ($form_data['loan_type_redeem_relationship'] == $data['relationship_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['relationship_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>


<!--
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <button class="btn btn-primary mr5">บันทึก</button>
                    <button type="reset" class="btn btn-default">ยกเลิก</button>
                </div>
            </div>
        </div>
-->


        <hr>
        <div class="row">
            <!--back & next page-->
            <div class="col-sm-12 right">
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_person"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_doc"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>
    </div>
</div><!-- panel-body -->
</div><!-- panel -->
