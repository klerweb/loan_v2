<form id="form_loan" name="form_loan" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land/save_land3"); ?>">
    <input type="hidden" id="loan_objective_id" name="loan_objective_id" value="<?php echo $form_data['loan_objective_id']; ?>">
    <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
    <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">
    <input type="hidden" name="plaintiff[person_id]" value="<?php echo $plaintiff['person_id']; ?>">
    <input type="hidden" name="defendant[person_id]" value="<?php echo $defendant['person_id']; ?>">

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลการชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน</h4>
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลที่ดิน</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <div class="radio">
                    <?php foreach ($mst['land_buiding_detail_id_list'] as $key => $data): ?>
                        <label><input type="radio" name="land_buiding_detail_id" class="land_buiding_detail_id" id="land_buiding_detail_id_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['land_buiding_detail_id'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ประเภท <span class="asterisk">*</span></label>
                <select class="form-control" id="land_type_id" name="land_type_id" required>
                    <option value="">ประเภท</option>
                    <?php if ($mst['config_land_type_list']): ?>
                        <?php foreach ($mst['config_land_type_list'] as $data): ?>
                            <option value="<?php echo $data['land_type_id']; ?>" <?php if ($form_data['land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เลขที่ <span class="asterisk">*</span></label> 
                <input type="text" name="land_no" id="land_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['land_no']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เลขที่ดิน <span class="asterisk">*</span></label> 
                <input type="number" name="land_addr_no" id="land_addr_no" placeholder="เลขที่ดิน" class="form-control" value="<?php echo $form_data['land_addr_no']; ?>" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">หมู่ที่</label> 
                <input type="text" name="land_addr_moo" id="land_addr_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['land_addr_moo']; ?>" maxlength="2">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                <select class="width100p" id="land_addr_province_id" name="land_addr_province_id" required>
                    <option value="">จังหวัด</option>
                    <?php if ($mst['master_province_list']): ?>
                        <?php foreach ($mst['master_province_list'] as $data): ?>
                            <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>                    
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label> 
                <div id="div_land_addr_amphur_id">
                    <select class="width100p" id="land_addr_amphur_id" name="land_addr_amphur_id" required>
                        <option value="">อำเภอ/เขต</option>
                        <?php if ($mst['master_amphur_list']): ?>
                            <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                <div id="div_land_addr_district_id">
                    <select class="width100p" id="land_addr_district_id" name="land_addr_district_id" required>
                        <option value="">ตำบล/แขวง</option>
                        <?php if ($mst['master_district_list']): ?>
                            <?php foreach ($mst['master_district_list'] as $data): ?>
                                <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>                    
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เนื้อที่(ไร่) <span class="asterisk">*</span></label>
                <input type="number" min="0" name="land_area_rai" id="land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $form_data['land_area_rai']; ?>" required>                        
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เนื้อที่(งาน) <span class="asterisk">*</span></label>
                <input type="number" min="0" name="land_area_ngan" id="land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $form_data['land_area_ngan']; ?>" required>                        
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">เนื้อที่(ตารางวา) <span class="asterisk">*</span></label>
                <input type="number" min="0" name="land_area_wah" id="land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $form_data['land_area_wah']; ?>" required>                        
            </div>
        </div>
    </div>

    <?php if (!$form_data['loan_type_id']): ?>

    <hr class=" div_land_building">
        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ข้อมูลที่ดินพร้อมสิ่งปลูกสร้าง</h4>
                </div>
            </div>
        </div>


        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภทสิ่งปลูกสร้าง</label>
                    <select class="form-control" id="building_type_id" name="building_type_id" >
                        <option value="">ประเภทสิ่งปลูกสร้าง</option>
                        <?php if ($mst['config_building_type_list']): ?>
                            <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่</label> 
                    <input type="text" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" >
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label> 
                    <input type="text" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ซอย</label> 
                    <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                </div>
            </div> 

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label> 
                    <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด</label>
                    <select class="width100p" id="building_province_id" name="building_province_id">
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต</label>
                    <div id="div_building_amphur_id">
                        <select class="width100p" id="building_amphur_id" name="building_amphur_id">
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                        
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง</label>
                    <div id="div_building_district_id">
                        <select class="width100p" id="building_district_id" name="building_district_id">
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>                    
                </div>                    
            </div>
        </div>
    <?php endif; ?>

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลคำพิพากษา</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จำนวนเงินรวมทั้งสิ้น <span class="asterisk">*</span></label>
                <input type="number" min="0" name="loan_type_case_amount" id="loan_type_case_amount" placeholder="จำนวนเงินรวมทั้งสิ้น" class="form-control" value="<?php echo $form_data['loan_type_case_amount']; ?>" required>                        
            </div>
        </div>
    </div>    

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คำพิพากษา คดีหมายเลขดำที่ <span class="asterisk">*</span></label> 
                <input type="text" name="loan_type_case_black_no" id="loan_type_case_black_no" placeholder="คำพิพากษา คดีหมายเลขดำที่" class="form-control" value="<?php echo $form_data['loan_type_case_black_no']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คำพิพากษา คดีหมายเลขแดงที่ <span class="asterisk">*</span></label> 
                <input type="text" name="loan_type_case_red_no" id="loan_type_case_red_no" placeholder="คำพิพากษา คดีหมายเลขแดงที่" class="form-control" value="<?php echo $form_data['loan_type_case_red_no']; ?>" required>               
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ศาล <span class="asterisk">*</span></label> 
                <input type="text" name="loan_type_case_court" id="loan_type_case_court" placeholder="ศาล" class="form-control" value="<?php echo $form_data['loan_type_case_court']; ?>" required>               
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ลงวันที่ <span class="asterisk">*</span></label> 
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_type_case_date" name="loan_type_case_date" value="<?php echo $form_data['loan_type_case_date']; ?>" required>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>                
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ประเภทคดีความ <span class="asterisk">*</span></label>
                <div class="radio">                            
                    <label><input type="radio" name="loan_type_case_type" id="loan_type_case_type_0" value="1" checked="checked" required>แพ่ง<?php echo nbs(2); ?></label>
                    <label><input type="radio" name="loan_type_case_type" id="loan_type_case_type_1" value="2" required>อาญา<?php echo nbs(2); ?></label>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คดีระหว่างโจทก์  คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                <select class="form-control" id="plaintiff_title_id" name="plaintiff[title_id]" required>
                    <option value="">คำนำหน้าชื่อ</option>
                    <?php if ($mst['config_title_list']): ?>
                        <?php foreach ($mst['config_title_list'] as $data): ?>
                            <option value="<?php echo $data['title_id']; ?>" <?php if ($plaintiff['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>     

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                <input type="text" name="plaintiff[person_fname]" id="plaintiff_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $plaintiff['person_fname']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                <input type="text" name="plaintiff[person_lname]" id="plaintiff_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $plaintiff['person_lname']; ?>" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จำเลย</label>
                <label><input type="checkbox" value="1" name="same_loan_owner" id="same_loan_owner">เป็นคนเดียวกับผู้กู้</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คำนำหน้าชื่อ</label>
                <select class="form-control" id="defendant_title_id" name="defendant[title_id]" >
                    <option value="">คำนำหน้าชื่อ</option>
                    <?php if ($mst['config_title_list']): ?>
                        <?php foreach ($mst['config_title_list'] as $data): ?>
                            <option value="<?php echo $data['title_id']; ?>" <?php if ($defendant['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>     

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ชื่อ</label>
                <input type="text" name="defendant[person_fname]" id="defendant_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $defendant['person_fname']; ?>" >
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">นามสกุล</label>
                <input type="text" name="defendant[person_lname]" id="defendant_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $defendant['person_lname']; ?>" >
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <label class="control-label">มูลเหตุแห่งคดีความ <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_case_case" id="loan_type_case_case" placeholder="เพื่อชำระหนี้ตามคำพิพากษาฯ" required><?php echo $form_data['loan_type_case_case'] ?></textarea>
            </div>
        </div>
    </div>

    <div class="row">&nbsp;</div>

    <div class="row">
        <div class="col-sm-12 right">
            <div class="form-group form-group-loan">
                <button class="btn btn-primary mr5">บันทึก</button>
                <button type="reset" class="btn btn-default">ยกเลิก</button>
            </div>
        </div>
    </div>
</form>

<?php if ($form_data['loan_type_id']): ?>
    <hr>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลสิ่งปลูกสร้าง</h4>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped mb30">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ประเภทสิ่งปลูกสร้าง</th>                
                    <th>สถานที่ตั้ง</th>
                    <th>แก้ไข/ลบ</th>
                </tr>
            </thead>
            <?php if ($building_table): ?>
                <tbody>
                    <?php foreach ($building_table as $data): ?>
                        <tr>
                            <td><?php echo $data['building_id']; ?></td>
                            <td><?php echo $data['building_type_name']; ?></td>
                            <td><?php echo $data['address']; ?></td>
                            <td>
            <!--                                <a href="<?php echo site_url("loan/form_doc/?view=1"); ?>" class=""><span class="glyphicon glyphicon-zoom-in"></span></a>
                                &nbsp;&nbsp;-->
                                <a href="<?php echo site_url("loan/form_land/land_{$form_data['loan_objective_id']}/?edit={$form_data['loan_type_id']}&building_id={$data['building_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                &nbsp;&nbsp;
                                <a href="<?php echo site_url("loan/form_land/delete_building/{$data['building_id']}/?loan_objective_id={$form_data['loan_objective_id']}&loan_type_id={$form_data['loan_type_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>                                    
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>

        </table>
    </div><!-- table-responsive -->

    <form id="form_building" name="form_building" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land/save_building"); ?>">
        <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
        <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">
        <input type="hidden" id="loan_objective_id" name="loan_objective_id" value="<?php echo $form_data['loan_objective_id']; ?>">
        <input type="hidden" id="building_owner" name="building_owner" value="<?php echo $form_data['person_id']; ?>">
        <input type="hidden" id="building_id" name="building_id" value="<?php echo $form_data['building_id']; ?>">


        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภทสิ่งปลูกสร้าง <span class="asterisk">*</span></label>
                    <select class="form-control" id="building_type_id" name="building_type_id" required>
                        <option value="">ประเภทสิ่งปลูกสร้าง</option>
                        <?php if ($mst['config_building_type_list']): ?>
                            <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่ <span class="asterisk">*</span></label> 
                    <input type="text" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label> 
                    <input type="text" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ซอย</label> 
                    <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                </div>
            </div> 

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label> 
                    <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                    <select class="width100p" id="building_province_id" name="building_province_id" required>
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>                    
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                    <div id="div_building_amphur_id">
                        <select class="width100p" id="building_amphur_id" name="building_amphur_id" required>
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                    <div id="div_building_district_id">
                        <select class="width100p" id="building_district_id" name="building_district_id" required>
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>                    
            </div>            
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-12 right">
                <div class="form-group form-group-loan">
                    <button class="btn btn-primary mr5">บันทึก</button>
                    <button type="reset" class="btn btn-default">ยกเลิก</button>
                </div>
            </div>
        </div>
    </form>
<?php endif; ?>

<script type="text/javascript">
    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

<?php if ($form_data['land_addr_amphur_id']): ?>
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + "/" + <?php echo$form_data['land_addr_district_id'] ?> + param_url_pad_land_addr);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district for building*/
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';
    var param_url_pad_building = "/?" + pad_id_building_list + "&" + pad_name_building_list;

<?php if ($form_data['building_amphur_id']): ?>
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + "/" + <?php echo $form_data['building_amphur_id'] ?> + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['building_amphur_id'] ?> + "/" + <?php echo$form_data['building_district_id'] ?> + param_url_pad_building);
<?php endif; ?>
</script>     