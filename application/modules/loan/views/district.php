<?php $class = isset($district_class) ? $district_class : '' ?>
<?php $required = isset($district_required) ? $district_required : 'required' ?>

<select name="<?php echo $district_name ?>" id="<?php echo $district_id ?>" data-placeholder="ตำบล/แขวง" class="width100p <?php echo $class; ?>" <?php echo $required ?>>
    <option value="">ตำบล/แขวง</option>
    <?php foreach ($district as $district_data) { ?>
        <option value="<?php echo $district_data['district_id']; ?>" <?php if ($district_val == $district_data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $district_data['district_name']; ?></option>
    <?php } ?>
</select>
<script type="text/javascript">
    $("#<?php echo $district_id ?>").select2({
        minimumResultsForSearch: -1
    });
<?php if (isset($district_readonly) && $district_readonly == 'readonly'): ?>
        $("#<?php echo $district_id ?>").select2('readonly', true);
<?php endif; ?>

//zipcode_id
<?php if (isset($zipcode_id)): ?>
        zipcode_list = <?php echo $zipcode; ?>;
        $("#<?php echo $district_id ?>").select2().on("change", function (e) {
            var zicode_key = e.val;
            $("#<?php echo $zipcode_id ?>").val(zipcode_list[zicode_key]);
        });
<?php endif; ?>

</script>