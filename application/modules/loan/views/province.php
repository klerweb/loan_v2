<?php $class = isset($province_class) ? $province_class : '' ?>
<?php $required = isset($province_required) ? $province_required : 'required' ?>
<select name="<?php echo $province_name ?>" id="<?php echo $province_id ?>" data-placeholder="อำเภอ/เขต" class="width100p <?php echo $class; ?>" <?php echo $required ?>>
    <option value="">จังหวัด</option>
    <?php foreach ($province as $province_data) { ?>
        <option value="<?php echo $province_data['province_id']; ?>" <?php if ($province_val == $province_data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $province_data['province_name']; ?></option>
    <?php } ?>
</select>
<script type="text/javascript">    
//zipcode_id
<?php if (isset($zipcode_id)): ?>
        var id_list = 'province_id=<?php echo $province_id ?>&amphur_id=<?php echo $amphur_id ?>&district_id=<?php echo $district_id ?>&zipcode_id=<?php echo $zipcode_id ?>';
<?php else: ?>
        var id_list = 'province_id=<?php echo $province_id ?>&amphur_id=<?php echo $amphur_id ?>&district_id=<?php echo $district_id ?>';
<?php endif; ?>    
    
<?php if (isset($province_class) && isset($amphur_class) && isset($district_class)): ?>
        var class_list = 'province_class=<?php echo $province_class ?>&amphur_class=<?php echo $amphur_class ?>&district_class=<?php echo $district_class ?>';
<?php else: ?>
        var class_list = '';
<?php endif; ?>
    var name_list = 'province_name=<?php echo $province_name ?>&amphur_name=<?php echo $amphur_name ?>&district_name=<?php echo $district_name ?>';
    var param_url = "/?" + id_list + "&" + name_list + "&" + class_list;

    $("#<?php echo $province_id ?>").select2({
        minimumResultsForSearch: -1
    });

    $("#<?php echo $province_id ?>").change(function () {
        $("#div_<?php echo $amphur_id ?>").load(base_url + "loan/amphur/" + $("#<?php echo $province_id ?>").val() + param_url);
        $("#div_<?php echo $district_id ?>").load(base_url + "loan/district/" + param_url);
    });

<?php if (isset($province_readonly) && $province_readonly == 'readonly'): ?>
        $("#<?php echo $province_id ?>").select2('readonly', true);
<?php endif; ?>
</script>