<?php $class = isset($province_class) ? $province_class : ''?>
<select name="<?php echo $province_name?>" id="<?php echo $province_id?>" data-placeholder="อำเภอ/เขต" class="width100p <?php echo $class;?>" required>
    <option value="">จังหวัด</option>
    <?php foreach ($province as $province_data) { ?>
    <option value="<?php echo $province_data['province_id']; ?>" <?php if($province_val == $province_data['province_id']):?>selected="selected"<?php endif;?>><?php echo $province_data['province_name']; ?></option>
    <?php } ?>
</select>
<script type="text/javascript">
    var id_list = 'province_id=<?php echo $province_id?>&amphur_id=<?php echo $amphur_id?>&district_id=<?php echo $district_id?>';
    var name_list = 'province_name=<?php echo $province_name?>&amphur_name=<?php echo $amphur_name?>&district_name=<?php echo $district_name?>';
    var param_url = "/?" + id_list + "&" + name_list;

    $("#<?php echo $province_id?>").select2({
        minimumResultsForSearch: -1
    });

    $("#<?php echo $province_id?>").change(function () {
        $("#div_<?php echo $amphur_id?>").load(base_url + "loan/amphur_3/" + $("#<?php echo $province_id?>").val() + param_url);
        $("#div_<?php echo $district_id?>").load(base_url + "loan/district_3/" + param_url);
    });
</script>