<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1)</h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>

    </div>
    <div class="panel-body form-list">
        <?php if ($session_form && $session_form['status'] == 'fail'): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <ol>
            <li><a href="<?php echo site_url("loan/form_person"); ?>">ผู้ขอสินเชื่อ</a></li>
            <li><a href="<?php echo site_url("loan/form_land"); ?>">การขอสินเชื่อ</a></li>
            <li><a href="<?php echo site_url("loan/form_loan_doc"); ?>">เอกสารหลักฐานที่นำมาประกอบการยื่นคำขอสินเชื่อ</a></li>
            <li><a href="<?php echo site_url("loan/form_loan_income"); ?>">รายได้ในครัวเรือน</a></li>
            <li><a href="<?php echo site_url("loan/form_loan_expenditure"); ?>">ค่าใช้จ่ายในครัวเรือน</a></li>
            <li><a href="<?php echo site_url("loan/form_land_occupy"); ?>">ที่ดินของผู้ขอสินเชื่อและคู่สมรสถือครองทั้งหมด</a></li>            
            <li><a href="<?php echo site_url("loan/form_loan_asset"); ?>">ทรัพย์สินอื่นนอกจากที่ดินของผู้ขอสินเชื่อและคู่สมรส</a></li>
            <li><a href="<?php echo site_url("loan/form_loan_debt"); ?>">หนี้สินของผู้ขอสินเชื่อและคู่สมรส</a></li>
            <li><a href="<?php echo site_url("loan/form_loan_co"); ?>">ผู้กู้ร่วม (ถ้ามี)</a></li>
            <li><a href="<?php echo site_url("loan/form_guarantee"); ?>">หลักประกันการขอใช้สินเชื่อ</a></li>
            <li><a href="<?php echo site_url("loan/form_loan_objective"); ?>">ความประสงค์จะขอใช้สินเชื่อ</a></li>
        </ol>
    </div><!-- panel-body -->
</div><!-- panel -->