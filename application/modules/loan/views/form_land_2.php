<form id="form_loan" name="form_loan" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land/save_land2"); ?>">
    <input type="hidden" id="loan_objective_id" name="loan_objective_id" value="<?php echo $form_data['loan_objective_id']; ?>">
    <!--<input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">-->
    <input type="hidden" id="land_id" name="land_id" value="">
    <input type="hidden" id="loan_type_id" name="loan_type_id" value="<?php echo $form_data['loan_type_id']; ?>">
    <?php if ($creditor['person_id']): ?>
      <input type="hidden" name="creditor[person_id]" value="<?php echo $creditor['person_id']; ?>">
    <?php endif; ?>
    <input type="hidden" name="borrowers[person_id]" value="<?php echo $borrowers['person_id']; ?>">

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลการชำระหนี้ตามสัญญากู้ยืมเงิน</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <div class="form-group form-group-loan">
                <label class="control-label">สาเหตุของการกู้ยืมเงิน <span class="asterisk">*</span></label>
                <textarea class="form-control" rows="5" maxlength="200" name="loan_type_contract_case" id="loan_type_contract_case" placeholder="สาเหตุของการนำที่ดินไปจำนองหรือขายฝาก" required><?php echo $form_data['loan_type_contract_case'] ?></textarea>
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group form-group-loan">
                <h4>ข้อมูลการกู้ยืมเงิน</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จำนวนเงินเพื่อนำไปชำระหนี้ตามสัญญากู้ยืมเงิน (บาท)  <span class="asterisk">*</span></label>
                <input type="text" min="0" name="loan_type_contract_amount" id="loan_type_contract_amount" placeholder="จำนวนเงินเพื่อนำไปชำระหนี้ตามสัญญากู้ยืมเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_type_contract_amount']; ?>" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">สัญญากู้เงิน เลขที่ <span class="asterisk">*</span></label>
                <input type="text" name="loan_type_contract_no" id="loan_type_contract_no" placeholder="สัญญากู้เงิน เลขที่" class="form-control" value="<?php echo $form_data['loan_type_contract_no']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ลงวันที่ <span class="asterisk">*</span></label>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_type_contract_date" name="loan_type_contract_date" value="<?php echo $form_data['loan_type_contract_date']; ?>" required>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                <select class="form-control" id="creditor_title_id" name="creditor[title_id]" required>
                    <option value="">คำนำหน้าชื่อ</option>
                    <?php if ($mst['config_title_list']): ?>
                        <?php foreach ($mst['config_title_list'] as $data): ?>
                            <option value="<?php echo $data['title_id']; ?>" <?php if ($creditor['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ชื่อผู้ให้กู้ยืม <span class="asterisk">*</span></label>
                <input type="text" name="creditor[person_fname]" id="creditor_person_fname" placeholder="ชื่อผู้ให้กู้ยืม" class="form-control" value="<?php echo $creditor['person_fname']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                <input type="text" name="creditor[person_lname]" id="creditor_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $creditor['person_lname']; ?>" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ที่อยู่เลขที่ <span class="asterisk">*</span></label>
                <input type="text" name="creditor[person_addr_card_no]" id="creditor_addr_card_no" placeholder="ที่อยู่เลขที่" class="form-control" value="<?php echo $creditor['person_addr_card_no']; ?>" required>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">หมู่ที่</label>
                <input type="text" name="creditor[person_addr_card_moo]" id="creditor_addr_card_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $creditor['person_addr_card_moo']; ?>" maxlength="2">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ถนน</label>
                <input type="text" name="creditor[person_addr_card_road]" id="creditor_addr_card_road" placeholder="ถนน" class="form-control" value="<?php echo $creditor['person_addr_card_road']; ?>">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                <div id="div_creditor_addr_card_province_id">
                    <select class="width100p" id="creditor_addr_card_province_id" name="creditor[person_addr_card_province_id]" required>
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($creditor['person_addr_card_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                <div id="div_creditor_addr_card_amphur_id">
                    <select class="width100p" id="creditor_addr_card_amphur_id" name="creditor[person_addr_card_amphur_id]" required>
                        <option value="">อำเภอ/เขต</option>
                        <?php if ($mst['master_amphur_list']): ?>
                            <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                <option value="<?php echo $data['amphur_id']; ?>" <?php if ($creditor['person_addr_card_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                <div id="div_creditor_addr_card_district_id">
                    <select class="width100p" id="creditor_addr_card_district_id" name="creditor[person_addr_card_district_id]" required>
                        <option value="">ตำบล/แขวง</option>
                        <?php if ($mst['master_district_list']): ?>
                            <?php foreach ($mst['master_district_list'] as $data): ?>
                                <option value="<?php echo $data['district_id']; ?>" <?php if ($creditor['person_addr_card_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">รหัสไปรษณีย์</label>
                <input type="text" name="creditor[person_addr_card_zipcode]" id="creditor_addr_card_zipcode" placeholder="รหัสไปรษณีย์" class="form-control" value="<?php echo $creditor['person_addr_card_zipcode']; ?>" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">โทรศัพท์บ้าน</label>
                <input type="tel" pattern=".{9,9}" maxlength="9" title="9 characters" name="creditor[person_phone]" id="creditor_phone" placeholder="โทรศัพท์บ้าน" class="form-control" value="<?php echo $creditor['person_phone']; ?>">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">โทรศัพท์มือถือ</label>
                <input type="tel" pattern=".{10,10}" maxlength="10" title="10 characters" name="creditor[person_mobile]" id="creditor_mobile" placeholder="โทรศัพท์มือถือ" class="form-control" value="<?php echo $creditor['person_mobile']; ?>">
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">E-mail</label>
                <input type="email" name="person[person_email]" id="person_email" placeholder="E-mail" class="form-control" value="<?php echo $creditor['person_email']; ?>">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">กับผู้กู้ยืม</label>
                <label><input type="checkbox" value="1" name="same_loan_owner" id="same_loan_owner">เป็นคนเดียวกับผู้ขอสินเชื่อ</label>
            </div>
        </div>
    </div>

    <div class="row div_loan_owner">
        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">คำนำหน้าชื่อ</label>
                <select class="form-control" id="borrowers_title_id" name="borrowers[title_id]" >
                    <option value="">คำนำหน้าชื่อ</option>
                    <?php if ($mst['config_title_list']): ?>
                        <?php foreach ($mst['config_title_list'] as $data): ?>
                            <option value="<?php echo $data['title_id']; ?>" <?php if ($borrowers['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">ชื่อ</label>
                <input type="text" name="borrowers[person_fname]" id="borrowers_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $borrowers['person_fname']; ?>" >
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group form-group-loan">
                <label class="control-label">นามสกุล</label>
                <input type="text" name="borrowers[person_lname]" id="borrowers_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $borrowers['person_lname']; ?>" >
            </div>
        </div>
    </div>

    <?php if (!$form_data['loan_type_id']): ?>

        <hr>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ข้อมูลที่ดิน</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <div class="radio">
                        <?php foreach ($mst['land_buiding_detail_id_list'] as $key => $data): ?>
                            <label><input type="radio" name="land_buiding_detail_id" class="land_buiding_detail_id" id="land_buiding_detail_id_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['land_buiding_detail_id'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เจ้าของที่ดินคำนำหน้าชื่อ <span class="asterisk">*</span></label>
                    <select class="form-control" id="land_owner_title_id" required readonly="readonly">
                        <option value="">คำนำหน้าชื่อ</option>
                        <?php if ($mst['config_title_list']): ?>
                            <?php foreach ($mst['config_title_list'] as $data): ?>
                                <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                    <input type="text" id="land_owner_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" required readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                    <input type="text" id="land_owner_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" required readonly="readonly">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภทเอกสารสิทธิ <span class="asterisk">*</span></label>
                    <select class="form-control" id="land_type_id" name="land_type_id" required>
                        <option value="">ประเภทเอกสารสิทธิ</option>
                        <?php if ($mst['config_land_type_list']): ?>
                            <?php foreach ($mst['config_land_type_list'] as $data): ?>
                                <option value="<?php echo $data['land_type_id']; ?>" <?php if ($form_data['land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่ <span class="asterisk">*</span></label>
                    <input type="text" name="land_no" id="land_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['land_no']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่ดิน <span class="asterisk">*</span></label>
                    <input type="number" name="land_addr_no" id="land_addr_no" placeholder="เลขที่ดิน" class="form-control" value="<?php echo $form_data['land_addr_no']; ?>" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label>
                    <input type="number" name="land_addr_moo" id="land_addr_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['land_addr_moo']; ?>" maxlength="2">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                    <select class="width100p" id="land_addr_province_id" name="land_addr_province_id" required>
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                    <div id="div_land_addr_amphur_id">
                        <select class="width100p" id="land_addr_amphur_id" name="land_addr_amphur_id" required>
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                    <div id="div_land_addr_district_id">
                        <select class="width100p" id="land_addr_district_id" name="land_addr_district_id" required>
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เนื้อที่(ไร่) <span class="asterisk">*</span></label>
                    <input type="number" min="0" name="land_area_rai" id="land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $form_data['land_area_rai']; ?>" required>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เนื้อที่(งาน) <span class="asterisk">*</span></label>
                    <input type="number" min="0" max="<?php echo get_max_ngan(); ?>" name="land_area_ngan" id="land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $form_data['land_area_ngan']; ?>" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เนื้อที่(ตารางวา) <span class="asterisk">*</span></label>
                    <input type="number" min="0" max="<?php echo get_max_wa(); ?>" name="land_area_wah" id="land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $form_data['land_area_wah']; ?>" required>
                </div>
            </div>
        </div>


        <hr class=" div_land_building">
        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <h4>ข้อมูลที่ดินพร้อมสิ่งปลูกสร้าง</h4>
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เจ้าของสิ่งปลูกสร้าง</label>
                    <div class="radio">
                        <?php foreach ($mst['buiding_owner_status_list'] as $key => $data): ?>
                            <label><input type="radio" name="buiding_owner_status" class="buiding_owner_status" id="buiding_owner_status_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['buiding_owner_status'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เจ้าของสิ่งปลูกสร้างคำนำหน้าชื่อ</label>
                    <select class="form-control" id="building_owner_title_id" name="building_owner_title_id" readonly="readonly">
                        <option value="">คำนำหน้าชื่อ</option>
                        <?php if ($mst['config_title_list']): ?>
                            <?php foreach ($mst['config_title_list'] as $data): ?>
                                <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ชื่อ</label>
                    <input type="text" name="building_owner_fname" id="building_owner_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" readonly="readonly">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">นามสกุล</label>
                    <input type="text" name="building_owner_lname" id="building_owner_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" readonly="readonly">
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ประเภทสิ่งปลูกสร้าง</label>
                    <select class="form-control" id="building_type_id" name="building_type_id" >
                        <option value="">ประเภทสิ่งปลูกสร้าง</option>
                        <?php if ($mst['config_building_type_list']): ?>
                            <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">เลขที่</label>
                    <input type="number" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" >
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">หมู่ที่</label>
                    <input type="number" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อาคาร</label>
                    <input type="text" name="building_name" id="building_name" placeholder="อาคาร" class="form-control" value="<?php echo $form_data['building_name']; ?>" >
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ซอย</label>
                    <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ถนน</label>
                    <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                </div>
            </div>
        </div>

        <div class="row div_land_building">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">จังหวัด</label>
                    <select class="width100p" id="building_province_id" name="building_province_id">
                        <option value="">จังหวัด</option>
                        <?php if ($mst['master_province_list']): ?>
                            <?php foreach ($mst['master_province_list'] as $data): ?>
                                <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">อำเภอ/เขต</label>
                    <div id="div_building_amphur_id">
                        <select class="width100p" id="building_amphur_id" name="building_amphur_id">
                            <option value="">อำเภอ/เขต</option>
                            <?php if ($mst['master_amphur_list']): ?>
                                <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                    <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">ตำบล/แขวง</label>
                    <div id="div_building_district_id">
                        <select class="width100p" id="building_district_id" name="building_district_id">
                            <option value="">ตำบล/แขวง</option>
                            <?php if ($mst['master_district_list']): ?>
                                <?php foreach ($mst['master_district_list'] as $data): ?>
                                    <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col-sm-12 right">
            <div class="form-group form-group-loan">
                <button class="btn btn-primary mr5">บันทึก</button>
                <button type="reset" class="btn btn-default">ยกเลิก</button>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

<?php if ($form_data['land_addr_amphur_id']): ?>
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + "/" + <?php echo$form_data['land_addr_district_id'] ?> + param_url_pad_land_addr);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district for building*/
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';
    var param_url_pad_building = "/?" + pad_id_building_list + "&" + pad_name_building_list;

<?php if ($form_data['building_amphur_id']): ?>
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + "/" + <?php echo $form_data['building_amphur_id'] ?> + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['building_amphur_id'] ?> + "/" + <?php echo$form_data['building_district_id'] ?> + param_url_pad_building);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_card for creditor*/
    var creditor_pad_id_addr_card_list = 'province_id=creditor_person_addr_card_province_id&amphur_id=creditor_person_addr_card_amphur_id&district_id=creditor_person_addr_card_district_id';
    var creditor_pad_name_addr_card_list = 'province_name=creditor[person_addr_card_province_id]&amphur_name=creditor[person_addr_card_amphur_id]&district_name=creditor[person_addr_card_district_id]';
    var param_url_creditor_pad_addr_card = "/?" + creditor_pad_id_addr_card_list + "&" + creditor_pad_name_addr_card_list;

<?php if ($creditor['person_addr_card_amphur_id']): ?>
        $("#div_creditor_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#creditor_person_addr_card_province_id").val() + "/" + <?php echo $creditor['person_addr_card_amphur_id'] ?> + param_url_creditor_pad_addr_card);
        $("#div_creditor_person_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $creditor['person_addr_card_amphur_id'] ?> + "/" + <?php echo $creditor['person_addr_card_district_id'] ?> + param_url_creditor_pad_addr_card);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_pre for creditor*/
    var creditor_pad_id_addr_pre_list = 'province_id=creditor_person_addr_pre_province_id&amphur_id=creditor_person_addr_pre_amphur_id&district_id=creditor_person_addr_pre_district_id';
    var creditor_pad_name_addr_pre_list = 'province_name=creditor[person_addr_pre_province_id]&amphur_name=creditor[person_addr_pre_amphur_id]&district_name=creditor[person_addr_pre_district_id]';
    var creditor_pad_class_addr_pre_list = 'province_class=creditor_person_addr_pre&amphur_class=creditor_person_addr_pre&district_class=creditor_person_addr_pre';
    var param_url_creditor_pad_addr_pre = "/?" + creditor_pad_id_addr_pre_list + "&" + creditor_pad_name_addr_pre_list + "&" + creditor_pad_class_addr_pre_list;

<?php if ($creditor['person_addr_pre_amphur_id']): ?>
        $("#div_creditor_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#creditor_person_addr_pre_province_id").val() + "/" + <?php echo $creditor['person_addr_pre_amphur_id'] ?> + param_url_creditor_pad_addr_pre);
        $("#div_creditor_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $creditor['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $creditor['person_addr_pre_district_id'] ?> + param_url_creditor_pad_addr_pre);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_card for borrowers*/
    var borrowers_pad_id_addr_card_list = 'province_id=borrowers_person_addr_card_province_id&amphur_id=borrowers_person_addr_card_amphur_id&district_id=borrowers_person_addr_card_district_id';
    var borrowers_pad_name_addr_card_list = 'province_name=borrowers[person_addr_card_province_id]&amphur_name=borrowers[person_addr_card_amphur_id]&district_name=borrowers[person_addr_card_district_id]';
    var param_url_borrowers_pad_addr_card = "/?" + borrowers_pad_id_addr_card_list + "&" + borrowers_pad_name_addr_card_list;

<?php if ($borrowers['person_addr_card_amphur_id']): ?>
        $("#div_borrowers_person_addr_card_amphur_id").load(base_url + "loan/amphur/" + $("#borrowers_person_addr_card_province_id").val() + "/" + <?php echo $borrowers['person_addr_card_amphur_id'] ?> + param_url_borrowers_pad_addr_card);
        $("#div_borrowers_person_addr_card_district_id").load(base_url + "loan/district" + "/" + <?php echo $borrowers['person_addr_card_amphur_id'] ?> + "/" + <?php echo $borrowers['person_addr_card_district_id'] ?> + param_url_borrowers_pad_addr_card);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district addr_pre for borrowers*/
    var borrowers_pad_id_addr_pre_list = 'province_id=borrowers_person_addr_pre_province_id&amphur_id=borrowers_person_addr_pre_amphur_id&district_id=borrowers_person_addr_pre_district_id';
    var borrowers_pad_name_addr_pre_list = 'province_name=borrowers[person_addr_pre_province_id]&amphur_name=borrowers[person_addr_pre_amphur_id]&district_name=borrowers[person_addr_pre_district_id]';
    var borrowers_pad_class_addr_pre_list = 'province_class=borrowers_person_addr_pre&amphur_class=borrowers_person_addr_pre&district_class=borrowers_person_addr_pre';
    var param_url_borrowers_pad_addr_pre = "/?" + borrowers_pad_id_addr_pre_list + "&" + borrowers_pad_name_addr_pre_list + "&" + borrowers_pad_class_addr_pre_list;

<?php if ($borrowers['person_addr_pre_amphur_id']): ?>
        $("#div_borrowers_person_addr_pre_amphur_id").load(base_url + "loan/amphur/" + $("#borrowers_person_addr_pre_province_id").val() + "/" + <?php echo $borrowers['person_addr_pre_amphur_id'] ?> + param_url_borrowers_pad_addr_pre);
        $("#div_borrowers_person_addr_pre_district_id").load(base_url + "loan/district" + "/" + <?php echo $borrowers['person_addr_pre_amphur_id'] ?> + "/" + <?php echo $borrowers['person_addr_pre_district_id'] ?> + param_url_borrowers_pad_addr_pre);
<?php endif; ?>
</script>

<script>
    $(".buiding_owner_status").change(function () {
        var title_id = $(this).val();
        if (title_id == 0) { //ตนเอง
            $("#building_owner_title_id").val('<?php echo $person_data['title_id']; ?>');
            $("#building_owner_fname").val('<?php echo $person_data['person_fname']; ?>');
            $("#building_owner_lname").val('<?php echo $person_data['person_lname']; ?>');

            $('#building_owner_title_id').attr('readonly', true);
            $('#building_owner_fname').attr('readonly', true);
            $('#building_owner_lname').attr('readonly', true);
        } else {
            $('#building_owner_title_id').attr('readonly', false);
            $('#building_owner_fname').attr('readonly', false);
            $('#building_owner_lname').attr('readonly', false);
        }
    });
</script>

<script>
    $('#same_loan_owner').change(function () {
        if ($(this).is(":checked")) {
            $('.div_loan_owner').hide();
        } else {
            $('.div_loan_owner').show();
        }
    });
</script>

<script type="text/javascript">

$("#creditor_addr_card_province_id").change(function () {
    $("#div_creditor_addr_card_amphur_id").load(base_url + "master/amphur/" + $("#creditor_addr_card_province_id").val() +
        "?id=creditor_addr_card_amphur_id&name=creditor[person_addr_card_amphur_id]&target=div_creditor_addr_card_district_id" +
        "&target_name=creditor[person_addr_card_district_id]&target_id=creditor_addr_card_district_id&zip_id=creditor_addr_card_zipcode" );

});

<?php if ($creditor['person_addr_card_amphur_id']): ?>
        $("#div_creditor_addr_card_amphur_id").load(base_url + "master/amphur/" + $("#creditor_addr_card_province_id").val() +
            "?selected=<?php echo $creditor['person_addr_card_amphur_id'] ?>&id=creditor_addr_card_amphur_id&name=creditor[person_addr_card_amphur_id]&target=div_creditor_addr_card_district_id" +
            "&target_name=creditor[person_addr_card_district_id]&target_id=creditor_addr_card_district_id&zip_id=creditor_addr_card_zipcode" );

        $("#div_creditor_addr_card_district_id").load(base_url+"master/district/<?php echo $creditor['person_addr_card_amphur_id'] ?>" +
   		     "?selected=<?php echo $creditor['person_addr_card_district_id'] ?>&name=creditor[person_addr_card_district_id]&id=creditor_addr_card_district_id&zip_id=creditor_addr_card_zipcode" );


<?php endif; ?>


/*province, amphur, district person_land_addr for person*/



</script>
