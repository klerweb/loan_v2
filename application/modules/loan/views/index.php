
<p class="btn-newdata">
    <button type="button" class="btn btn-default btn-xs" onclick="location.href = '<?php echo site_url("loan/new_loan") ?>'">เพิ่มแบบบันทึก</button>
</p>

<?php if ($session_form && $session_form['status'] == 'success'): ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $session_form['message']; ?>
    </div>
<?php endif; ?>
<?php if ($session_form && $session_form['status'] == 'fail'): ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo $session_form['message']; ?>
    </div>
<?php endif; ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">ค้นหา</h4>
    </div>
    <div class="panel-body">
        <form id="frmSearch" name="frmSearch" class="form-inline" method="get" action="<?php echo site_url("loan"); ?>">
            <div class="form-group">
                <label class="sr-only" for="loan_code">เลขที่คำขอสินเชื่อ</label>
                <input type="text" class="form-control" id="loan_code" name="loan_code" placeholder="เลขที่คำขอสินเชื่อ" value="<?php echo $loan_code; ?>" />
            </div><!-- form-group -->
            <div class="form-group">
                <label class="sr-only" for="person_thaiid">บัตรประชาชน</label>
                <input type="text" class="form-control" id="person_thaiid" name="person_thaiid" placeholder="บัตรประชาชน" value="<?php echo $person_thaiid; ?>" />
            </div><!-- form-group -->
            <div class="form-group">
                <label class="sr-only" for="fullname">ชื่อ - นามสกุล</label>
                <input type="text" class="form-control" id="fullname" name="fullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
            </div><!-- form-group -->
            <button type="submit" class="btn btn-primary mr5">Search</button>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->

<div class="table-responsive">
    <table class="table table-striped mb30">
        <thead>
            <tr>
                <th>ลำดับที่</th>
                <th>เลขที่ขอสินเชื่อ</th>
                <th>วันที่ขอสินเชื่อ</th>
                <th>ชื่อ - นามสกุล</th>
                <th width="300">วัตถุประสงคค์การขอสินเชื่อ</th>
                <th>สถานะ</th>
                <th>จัดการ</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($result)): $i = $row_start; ?>
                <?php 
                	foreach ($result as $row): 
	                	list($y, $m, $d) = explode('-', $row['loan_date']);
	                	$row['loan_date'] = $d.'/'.$m.'/'.(intval($y)+543);
                ?>                    
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <!--<td><a href="<?php echo site_url("loan/edit_loan/{$row['loan_id']}"); ?>"><?php echo $row['loan_code']; ?></a></td>-->
                        <td><?php echo $row['loan_code']; ?></td>
                        <td><?php echo $row['loan_date']; ?></td>
                        <td><?php echo "{$row['title_name']}{$row['person_fname']} {$row['person_lname']}"; ?></td>
                        <td><?php echo $row['objective_desc']; ?></td>
                        <td><?php echo $row['loan_status_desc']; ?></td>
                        <td>
                            <a href="<?php echo site_url("loan/report/{$row['loan_id']}"); ?>" class=""><span class="fa fa-print"></span></a>
                            <?php if( $row['loan_status'] == 1):?>
                            &nbsp;
                            <a href="<?php echo site_url("loan/edit_loan/{$row['loan_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                            &nbsp;
                            <a href="<?php echo site_url("loan/delete_loan/{$row['loan_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                            <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    
    <!--<div class="title_pagination"><?php echo $title_pagination;?></div>-->
    <?php echo $pagination;?>
</div><!-- table-responsive -->