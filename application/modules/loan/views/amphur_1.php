<?php $class = isset($amphur_class) ? $amphur_class : ''?>
<select name="<?php echo $amphur_name?>" id="<?php echo $amphur_id?>" data-placeholder="อำเภอ/เขต" class="width100p <?php echo $class;?>" required>
    <option value="">อำเภอ/เขต</option>
    <?php foreach ($amphur as $amphur_data) { ?>
    <option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if($amphur_val == $amphur_data['amphur_id']):?>selected="selected"<?php endif;?>><?php echo $amphur_data['amphur_name']; ?></option>
    <?php } ?>
</select>
<script type="text/javascript">
    var id_list = 'province_id=<?php echo $province_id?>&amphur_id=<?php echo $amphur_id?>&district_id=<?php echo $district_id?>';
    var name_list = 'province_name=<?php echo $province_name?>&amphur_name=<?php echo $amphur_name?>&district_name=<?php echo $district_name?>';
    var param_url = "/?" + id_list + "&" + name_list;

    $("#<?php echo $amphur_id?>").select2({
        minimumResultsForSearch: -1
    });

    $("#<?php echo $amphur_id?>").change(function () {
        $("#div_<?php echo $district_id?>").load(base_url + "loan/district_1/" + $("#<?php echo $amphur_id?>").val() + param_url);
    });
</script>