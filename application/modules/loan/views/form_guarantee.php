<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->

        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_loan_co"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_objective"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>
        คลิ๊กที่ <a href="#"><span class="glyphicon glyphicon-plus"></span></a> เพื่อเพิ่มข้อมูล
    </div>
</div><!-- panel -->



<p class="form-list-loan-code">
    <a href="<?php echo site_url("loan/form_guarantee_land"); ?>"><span class="glyphicon glyphicon-plus"></span></a>
    &nbsp;อสังหาริมทรัพย์    
</p>
<div class="table-responsive">
    <table class="table table-striped mb30">
        <thead>
            <tr>
                <th>ลำดับที่</th>
                <th>เลขที่เอกสารสิทธิ</th>
                <th>ประเภทที่ดิน</th>
                <th>สิ่งปลูกสร้าง</th>
                <th>ชื่อผู้ถือกรรมสิทธิ์</th>
                <th>แก้ไข / ลบ</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($loan_guarantee_land_data as $key => $row): ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $row['land_no']; ?></td>
                    <td><?php echo $row['land_type_name']; ?></td>
                    <td><?php echo $row['land_buiding_detail_detail']; ?></td>
                    <td><?php echo "{$row['title_name']}{$row['person_fname']} {$row['person_lname']}"; ?></td>
                    <td>
                        <a href="<?php echo site_url("loan/form_guarantee_land/?edit={$row['loan_guarantee_land_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                        &nbsp;&nbsp;
                        <a href="<?php echo site_url("loan/form_guarantee_land/delete/{$row['loan_guarantee_land_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>                                
                    </td>
                </tr>
            <?php endforeach;
            ?>
        </tbody>
    </table>
</div><!-- table-responsive -->

<hr>
<p class="form-list-loan-code">
    <a href="<?php echo site_url("loan/form_guarantee_bond"); ?>"><span class="glyphicon glyphicon-plus"></span></a>
    &nbsp;พันธบัตรรัฐบาล    
</p>
<div class="table-responsive">
    <table class="table table-striped mb30">
        <thead>
            <tr>
                <th>ลำดับที่</th>
                <th>เจ้าของกรรมสิทธิ์</th>
                <th>เลขประจำตัวประชาชน</th>
                <th>ชนิดพันธบัตร</th>
                <th>เลขที่ต้น</th>
                <th>เลขที่ท้าย</th>
                <th>ราคา(บาท)</th>
                <th>แก้ไข / ลบ</th>
            </tr>
        </thead>
        <?php if ($loan_guarantee_bond_data): ?>
            <tbody>
                <?php foreach ($loan_guarantee_bond_data as $key => $data): ?>
                    <tr>
                        <td><?php echo $key + 1; ?></td>
                        <td><?php echo $data['loan_bond_owner']; ?></td>
                        <td><?php echo $data['loan_bond_thaiid']; ?></td>
                        <td><?php echo $data['loan_bond_type']; ?></td>
                        <td><?php echo $data['loan_bond_startnumber']; ?></td>
                        <td><?php echo $data['loan_bond_endnumber']; ?></td>
                        <td><?php echo money_num_to_str($data['loan_bond_amount']); ?></td>
                        <td>
                            <?php if ($data['loan_bond_path']): ?>
                                <a href="<?php echo site_url($data['loan_bond_path']); ?>" target="_blank"><span class="glyphicon glyphicon-file"></span></a>
                                &nbsp;&nbsp;
                            <?php endif; ?>
                            <a href="<?php echo site_url("loan/form_guarantee_bond/?edit={$data['loan_bond_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                            &nbsp;&nbsp;
                            <a href="<?php echo site_url("loan/form_guarantee_bond/delete/{$data['loan_bond_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
    </table>
</div><!-- table-responsive -->

<hr>
<p class="form-list-loan-code">
    <a href="<?php echo site_url("loan/form_guarantee_bookbank"); ?>"><span class="glyphicon glyphicon-plus"></span></a>
    &nbsp;เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์    
</p>
<div class="table-responsive">
    <table class="table table-striped mb30">
        <thead>
            <tr>
            <tr>
                <th>ลำดับที่</th>
                <th>เจ้าของบัญชี</th>
                <th>ธนาคาร/สถาบันการเงิน/สหกรณ์</th>
                <th>เลขที่บัญชี</th>
                <th>ประเภทบัญชี</th>
                <th>จำนวนเงิน(บาท)</th>
                <th>แก้ไข / ลบ</th>
            </tr>
            </tr>
        </thead>
        <?php if ($loan_guarantee_bookbank_data): ?>
            <tbody>
                <?php foreach ($loan_guarantee_bookbank_data as $key => $data): ?>
                    <tr>
                        <td><?php echo $key + 1; ?></td>
                        <td><?php echo $data['loan_bookbank_owner']; ?></td>
                        <td><?php echo $data['bank_name']; ?><?php if ($data['loan_bookbank_branch'] != ''): ?><?php echo " (สาขา {$data['loan_bookbank_branch']})"; ?><?php endif; ?></td>
                        <td><?php echo $data['loan_bookbank_no']; ?></td>
                        <td><?php echo $data['loan_bookbank_type_name']; ?></td>        
                        <td><?php echo money_num_to_str($data['loan_bookbank_amount']); ?></td>        
                        <td>
                            <?php if ($data['loan_bookbank_path']): ?>
                                <a href="<?php echo site_url($data['loan_bookbank_path']); ?>" target="_blank"><span class="glyphicon glyphicon-file"></span></a>
                                &nbsp;&nbsp;
                            <?php endif; ?>
                            <a href="<?php echo site_url("loan/form_guarantee_bookbank/?edit={$data['loan_bookbank_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                            &nbsp;&nbsp;
                            <a href="<?php echo site_url("loan/form_guarantee_bookbank/delete/{$data['loan_bookbank_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
    </table>
</div><!-- table-responsive -->

<hr>
<p class="form-list-loan-code">
    <a href="<?php echo site_url("loan/form_guarantee_bondsman"); ?>"><span class="glyphicon glyphicon-plus"></span></a>
    &nbsp;บุคคลค้ำประกัน    
</p>
<div class="table-responsive">
    <table class="table table-striped mb30">
        <thead>
            <tr>
                <th>ลำดับที่</th>
                <th>เลขบัตรประจำตัวประชาชน</th>
                <th>ชื่อ - นามสกุล</th>
                <th>รายได้ต่อเดือน (บาท)</th>
                <th>แก้ไข / ลบ</th>
            </tr>
        </thead>
        <?php if ($loan_guarantee_bondsman_data): ?>
            <tbody>
                <?php foreach ($loan_guarantee_bondsman_data as $key => $data): ?>
                    <tr>
                        <td><?php echo $key + 1; ?></td>
                        <td><?php echo $data['person_thaiid']; ?></td>                        
                        <td><?php echo "{$data['title_name']}{$data['person_fname']} {$data['person_lname']}"; ?></td>
                        <td><?php echo money_num_to_str($data['person_income_per_month']); ?></td>
                        <td>
                            <a href="<?php echo site_url("loan/form_guarantee_bondsman/?edit={$data['loan_bondsman_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                            &nbsp;&nbsp;
                            <a href="<?php echo site_url("loan/form_guarantee_bondsman/delete/{$data['loan_bondsman_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
    </table>
</div><!-- table-responsive -->
