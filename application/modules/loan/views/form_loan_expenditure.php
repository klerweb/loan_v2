<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_loan_income"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_land_occupy"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <?php if ($loan_expenditure_data || $loan_expenditure_co_data): $num_row = 0;?>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>ประเภทรายจ่าย</th>
                            <th>ชื่อกิจกรรม</th>                            
                            <th>รายจ่ายปีปัจจุบัน</th>                            
                            <th>แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($loan_expenditure_data): ?>
                            <?php foreach ($loan_expenditure_data as $key => $row): $num_row++;?>
                                <tr>
                                    <td><?php echo $num_row; ?></td>
                                    <td><?php echo $row['loan_expenditure_type_name']; ?></td>
                                    <td><?php echo $row['loan_expenditure_name']; ?></td>
                                    <td><?php echo money_num_to_str($row['loan_expenditure_amount1']); ?></td>
                                    <td>
                                        <a href="<?php echo site_url("loan/form_loan_expenditure/?edit={$row['loan_expenditure_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/form_loan_expenditure/delete/{$row['loan_expenditure_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>                                    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>

                        <?php if ($loan_expenditure_co_data): ?>
                            <?php foreach ($loan_expenditure_co_data as $key => $row): $num_row++;?>
                                <tr>
                                    <td><?php echo $num_row; ?></td>
                                    <td><?php echo $row['loan_expenditure_type_name']; ?></td>
                                    <td><?php echo $row['loan_expenditure_name']; ?></td>
                                    <td><?php echo money_num_to_str($row['loan_expenditure_amount1']); ?></td>
                                    <td>
                                        <a href="<?php echo site_url("loan/form_loan_expenditure/?edit={$row['loan_expenditure_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/form_loan_expenditure/delete/{$row['loan_expenditure_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>                                    
                                    </td>
                                </tr>
                            <?php endforeach; ?>    
                        <?php endif; ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->
        <?php endif; ?>


        <form id="form_expenditure" name="form_expenditure" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_loan_expenditure/save"); ?>">
            <input type="hidden" name="person_id" id="person_id" value="<?php echo $form_data['person_id']; ?>">
            <input type="hidden" name="loan_expenditure_id" id="loan_expenditure_id" value="<?php echo $form_data['loan_expenditure_id']; ?>">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทรายจ่าย <span class="asterisk">*</span></label>
                        <?php if ($loan_expenditure_type_data): ?>
                            <div class="radio">
                                <?php foreach ($loan_expenditure_type_data as $key => $data): ?>
                                    <label><input type="radio" name="loan_expenditure_type" class="loan_expenditure_type" id="loan_expenditure_type_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['loan_expenditure_type'] == $key): ?>checked="checked"<?php endif; ?> required><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div> 

                <div class="col-sm-8">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อกิจกรรม <span class="asterisk">*</span></label>
                        <input type="text" name="loan_expenditure_name" id="loan_expenditure_name" placeholder="ชื่อกิจกรรม" class="form-control" value="<?php echo $form_data['loan_expenditure_name']; ?>" required>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h5>รายจ่าย</h5>                        
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ปีก่อนหน้า <span class="asterisk">*</span>                            
                        </label> 
                        <input type="text" min="0" name="loan_expenditure_amount2" id="loan_expenditure_amount2" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_expenditure_amount2']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ปัจจุบัน <span class="asterisk">*</span>
                            <?php echo nbs(2); ?><input type="checkbox" value="1" id="expenditure1_same"><?php echo nbs(2); ?>ปีปัจจุบันเท่ากับปีก่อนหน้า
                        </label>
                        <input type="text" min="0" name="loan_expenditure_amount1" id="loan_expenditure_amount1" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_expenditure_amount1']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ปีถัดไป <span class="asterisk">*</span>
                            <?php echo nbs(2); ?><input type="checkbox" value="1" id="expenditure3_same"><?php echo nbs(2); ?>ปีถัดไปเท่ากับปีจจุบัน
                        </label>
                        <input type="text" min="0" name="loan_expenditure_amount3" id="loan_expenditure_amount3" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_expenditure_amount3']; ?>" required>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->
