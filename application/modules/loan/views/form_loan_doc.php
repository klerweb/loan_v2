<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : เอกสารหลักฐานที่นำมาประกอบการยื่นคำขอสินเชื่อ</h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_land"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_income"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <?php if ($loan_doc_data): ?>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>ประเภทเอกสาร</th>
                            <th>ไฟล์เอกสาร</th>                            
                            <th>รายละเอียด / ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($loan_doc_data as $key => $row): ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $row['loan_doc_type_name']; ?></td>
                                <td><?php echo $row['loan_doc_path']; ?></td>                                
                                <td>
                                    <a href="<?php echo site_url("loan/form_loan_doc/view/{$row['loan_doc_id']}"); ?>" class=""><span class="glyphicon glyphicon-zoom-in"></span></a>
                                    &nbsp;&nbsp;
                                    
                                    <?php if (isset($row['loan_doc_path']) && $row['loan_doc_path']): ?>
                                        <!--<a href="<?php echo base_url($row['loan_doc_path']); ?>" target="_bank" class=""><span class="glyphicon glyphicon-file"></span></a>-->
                                        <a href="<?php echo site_url("loan/form_loan_doc/view_file/{$row['loan_doc_id']}"); ?>" target="_bank" class=""><span class="glyphicon glyphicon-file"></span></a>
                                        &nbsp;&nbsp;
                                    <?php endif; ?>
                                        
                                    <a href="<?php echo site_url("loan/form_loan_doc/delete/{$row['loan_doc_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach;
                        ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->
        <?php endif; ?>


        <form id="form_doc" name="form_doc" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_loan_doc/save"); ?>" enctype="multipart/form-data">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $loan_data['loan_id']; ?>">
            <input type="hidden" name="view_status" id="view_status" value="<?php echo $form_data['view_status'] ?>">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทเอกสาร <span class="asterisk">*</span></label>
                        <select class="form-control" id="loan_doc_type_id" name="loan_doc_type_id" required>
                            <option value="">ประเภทเอกสาร</option>
                            <?php if ($mst['config_loan_doc_type_list']): ?>
                                <?php foreach ($mst['config_loan_doc_type_list'] as $data): ?>
                                    <option value="<?php echo $data['loan_doc_type_id']; ?>"  <?php if ($form_data['loan_doc_type_id'] == $data['loan_doc_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['loan_doc_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id="loan_doc_type_other_form">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทเอกสารอื่นๆ (ระบุ)</label>
                        <input type="text" name="loan_doc_type_other" id="loan_doc_type_other" placeholder="ประเภทเอกสารอื่นๆ (ระบุ)" class="form-control" value="<?php echo $form_data['loan_doc_type_other'] ?>">
                    </div>
                </div>

                <div class="col-sm-4" id="loan_doc_date_form">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ลงวันที่ (วัน-เดือน-ปี : <?php echo date_th_now(); ?>)</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_doc_date" name="loan_doc_date" value="<?php echo $form_data['loan_doc_date'] ?>">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4" id="loan_doc_black_no_form">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            คดีหมายเลขดำที่
                        </label>
                        <input type="text" name="loan_doc_black_no" id="loan_doc_black_no" placeholder="คดีหมายเลขดำที่" class="form-control" value="<?php echo $form_data['loan_doc_black_no'] ?>" required>
                    </div>
                </div>
                <div class="col-sm-4" id="loan_doc_red_no_form">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            คดีหมายเลขแดงที่
                        </label>
                        <input type="text" name="loan_doc_red_no" id="loan_doc_red_no" placeholder="คดีหมายเลขแดงที่" class="form-control" value="<?php echo $form_data['loan_doc_red_no'] ?>" required>
                    </div>
                </div>                
                <div class="col-sm-4" id="loan_doc_court_form">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ศาล
                        </label>
                        <input type="text" name="loan_doc_court" id="loan_doc_court" placeholder="ศาล" class="form-control" value="<?php echo $form_data['loan_doc_court'] ?>" required>
                    </div>
                </div>                
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ชื่อเอกสาร <span class="asterisk">*</span>
                        </label>
                        <input type="text" name="loan_doc_filename" id="loan_doc_filename" placeholder="ชื่อเอกสาร" class="form-control" value="<?php echo $form_data['loan_doc_filename'] ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ไฟล์เอกสาร 
                            <?php if (isset($form_data['loan_doc_path']) && $form_data['loan_doc_path']): ?>
                                <a href="<?php echo site_url("loan/form_loan_doc/view_file/{$form_data['loan_doc_id']}");?>" target="_bank" class="">[ดูไฟล์เอกสาร]</a>                            
                            <?php endif; ?>
                            <span class="asterisk">*</span>    
                        </label>
                        <input type="file" name="loan_doc_path" id="loan_doc_path" placeholder="ไฟล์เอกสาร" class="form-control" value="<?php echo $form_data['loan_doc_path'] ?>" required>                        
                    </div>
                </div>

                <div class="col-sm-4" id="loan_doc_month_form">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            จำนวนเดือน
                        </label>
                        <input type="number" min="0" name="loan_doc_month" id="loan_doc_month" placeholder="จำนวนเดือน" class="form-control" value="<?php echo $form_data['loan_doc_month'] ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <label class="control-label">รายละเอียด</label>
                        <textarea class="form-control" rows="5" maxlength="200" name="loan_doc_desc" id="loan_doc_desc" placeholder="รายละเอียด"><?php echo $form_data['loan_doc_desc'] ?></textarea>
                    </div>
                </div>
            </div>


            <hr>
            <div class="row">
                <?php if (isset($form_data['view_status']) && $form_data['view_status'] != 1): ?>
                    <div class="col-sm-12 right">
                        <div class="form-group form-group-loan">
                            <button class="btn btn-primary mr5">บันทึก</button>
                            <button type="reset" class="btn btn-default">ยกเลิก</button>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->


<script type="text/javascript">

    /*อื่นๆ = 13*/
<?php if ($form_data['loan_doc_type_id'] != 13): ?>
        $("#loan_doc_type_other_form").hide();
<?php endif; ?>

    /*ขยายระยะเวลาการขายฝาก = 12*/
<?php if ($form_data['loan_doc_type_id'] != 12): ?>
        $("#loan_doc_month_form").hide();
<?php endif; ?>

    /*5,6,7,8,9,10*/
<?php if (!in_array($form_data['loan_doc_type_id'], array(4, 5, 6, 7, 8, 9, 10))): ?>
        $("#loan_doc_date_form").hide();
<?php endif; ?>

/*8, 9 สำเนาคำฟ้อง*/
<?php if (!in_array($form_data['loan_doc_type_id'], array(8,9))): ?>
        $("#loan_doc_black_no_form").hide();
        $("#loan_doc_court_form").hide();
<?php endif; ?>
    
/*9 สำเนาคำฟ้อง*/
<?php if (!in_array($form_data['loan_doc_type_id'], array(9))): ?>
        $("#loan_doc_red_no_form").hide();
<?php endif; ?>    
    
    /*loan_doc_type_id change*/
    $("#loan_doc_type_id").change(function () {
//        $("#loan_doc_date_form").hide();
//        $("#loan_doc_date_form").show();

        var loan_doc_type_id = $(this).val();

        /*อื่นๆ = 13*/
        if (loan_doc_type_id == 13) {
            $("#loan_doc_type_other_form").show();
        } else {
            $("#loan_doc_type_other_form").hide();
        }

        /*ขยายระยะเวลาการขายฝาก = 12*/
        if (loan_doc_type_id == 12) {
            $("#loan_doc_month_form").show();
        } else {
            $("#loan_doc_month_form").hide();
        }

        /*4, 5,6,7,8,9,10*/
        if (
                loan_doc_type_id == 4 ||
                loan_doc_type_id == 5 ||
                loan_doc_type_id == 6 ||
                loan_doc_type_id == 7 ||
                loan_doc_type_id == 8 ||
                loan_doc_type_id == 9 ||
                loan_doc_type_id == 10
                ) {
            $("#loan_doc_date_form").show();
        } else {
            $("#loan_doc_date_form").hide();
        }
        
        /*8, 9 สำเนาคำฟ้อง*/
        if (
                loan_doc_type_id == 8 ||
                loan_doc_type_id == 9 
                ) {
            $("#loan_doc_black_no_form").show();
            $("#loan_doc_court_form").show();
        } else {
            $("#loan_doc_black_no_form").hide();
            $("#loan_doc_court_form").hide();
        }
        
        /*9 สำเนาคำฟ้อง*/
        if (
                loan_doc_type_id == 9
                ) {
            $("#loan_doc_red_no_form").show();
        } else {
            $("#loan_doc_red_no_form").hide();
        }

    });

</script>   