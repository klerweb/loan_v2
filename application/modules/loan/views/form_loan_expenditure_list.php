<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url($form_data['table_param']['callbackurl']); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <form id="form_expenditure" name="form_expenditure" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/save_expenditure_list/?table_param={$form_data['table_param_encode']}"); ?>">
            <input type="hidden" name="person_id" id="person_id" value="<?php echo $form_data['person_id']; ?>">
            <input type="hidden" name="loan_expenditure_id" id="loan_expenditure_id" value="<?php echo $form_data['loan_expenditure_id']; ?>">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทรายจ่าย <span class="asterisk">*</span></label>
                        <?php if ($loan_expenditure_type_data): ?>
                            <div class="radio">
                                <?php foreach ($loan_expenditure_type_data as $key => $data): ?>
                                    <label><input type="radio" name="loan_expenditure_type" class="loan_expenditure_type" id="loan_expenditure_type_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['loan_expenditure_type'] == $key): ?>checked="checked"<?php endif; ?> required><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div> 

                <div class="col-sm-8">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อกิจกรรม <span class="asterisk">*</span></label>
                        <input type="text" name="loan_expenditure_name" id="loan_expenditure_name" placeholder="ชื่อกิจกรรม" class="form-control" value="<?php echo $form_data['loan_expenditure_name']; ?>" required>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h5>รายจ่าย</h5>                        
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ปีก่อนหน้า <span class="asterisk">*</span>                            
                        </label> 
                        <input type="text" min="0" name="loan_expenditure_amount2" id="loan_expenditure_amount2" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_expenditure_amount2']; ?>" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ปัจจุบัน <span class="asterisk">*</span>
                            <?php echo nbs(2); ?><input type="checkbox" value="1" id="expenditure1_same"><?php echo nbs(2); ?>ปีปัจจุบันเท่ากับปีก่อนหน้า
                        </label>
                        <input type="text" min="0" name="loan_expenditure_amount1" id="loan_expenditure_amount1" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_expenditure_amount1']; ?>" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            ปีถัดไป <span class="asterisk">*</span>
                            <?php echo nbs(2); ?><input type="checkbox" value="1" id="expenditure3_same"><?php echo nbs(2); ?>ปีถัดไปเท่ากับปีจจุบัน
                        </label>
                        <input type="text" min="0" name="loan_expenditure_amount3" id="loan_expenditure_amount3" placeholder="จำนวนเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_expenditure_amount3']; ?>" required>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->
