<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->

        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_person"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_doc"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>
        <?php if ($session_form && $session_form['status'] == 'fail'): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <div class="table-responsive">
            <table class="table table-striped mb30">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>วัตถุประสงค์การขอสินเชื่อ</th>
                        <th>วันที่ทำรายการ</th>
                        <th>แก้ไข/ลบ</th>
                    </tr>
                </thead>

                <?php if ($table_data): ?>
                    <tbody>
                        <?php foreach ($table_data as $row): ?>
                            <tr>
                                <td><?php echo $row['loan_type_id']; ?></td>
                                <td><?php echo $row['loan_objective_name']; ?></td>
                                <td><?php echo $row['loan_type_createdate']; ?></td>
                                <td>
                                    <!--
                                    <a href="<?php echo site_url("loan/form_land/land_{$row['loan_objective_id']}_view/{$row['loan_type_id']}"); ?>" class=""><span class="glyphicon glyphicon-zoom-in"></span></a>
                                    &nbsp;&nbsp;
                                    -->
                                    <a href="<?php echo site_url("loan/form_land/land_{$row['loan_objective_id']}/?edit={$row['loan_type_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url("loan/form_land/delete/{$row['loan_type_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                <?php endif; ?>
            </table>
        </div><!-- table-responsive -->

        <?php if (isset($table_land_building) && isset($form_data['loan_type_id']) && $form_data['loan_type_id']): ?>
            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่ดินและสิ่งปลูกสร้าง (ไม่เกิน <?php echo get_max_land(); ?>)</h4>
                        <?php if (count($table_land_building) < get_max_land()): ?>
                            <a href="<?php echo site_url("loan/form_land_list/?table_land_building_param={$table_land_building_param}"); ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มข้อมูลที่ดิน</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>ที่ดิน / สิ่งปลูกสร้าง</th>
                            <th>ประเภทสิ่งปลูกสร้าง</th>
                            <th>สถานที่ตั้ง</th>
                            <th>เพิ่ม/แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <?php if (isset($table_land_building) && $table_land_building): ?>
                        <tbody>
                            <?php foreach ($table_land_building as $key_land => $land): ?>
                                <tr>
                                    <td><?php echo ($key_land + 1); ?></td>
                                    <td>ที่ดิน</td>
                                    <td>-</td>
                                    <td><?php echo $land['address']; ?></td>
                                    <td>
                                        <a href="<?php echo site_url("loan/form_building_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}"); ?>" class=""><span class="glyphicon glyphicon-plus"></span></a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/form_land_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/del_land_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>

                                <?php if (isset($land['building_table'])): ?>
                                    <?php foreach ($land['building_table'] as $key_building => $building): ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>สิ่งปลูกสร้าง</td>
                                            <td><?php echo $building['building_type_name']; ?></td>
                                            <td><?php echo $building['address']; ?></td>
                                            <td>
                                                <a href="<?php echo site_url("loan/form_building_list/?table_land_building_param={$table_land_building_param}&edit=yes&land_id={$land['land_id']}&owner_id={$land['owner_id']}&building_id={$building['building_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                                &nbsp;&nbsp;
                                                <a href="<?php echo site_url("loan/del_building_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}&building_id={$building['building_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div><!-- table-responsive -->

        <?php endif; ?>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">วัตถุประสงค์การขอสินเชื่อ</label>
                    <input type="hidden" name="curr_url" id="curr_url" value="<?php echo site_url("loan/form_land"); ?>">
                    <select class="form-control" id="loan_objective_id" name="loan_objective_id">
                        <option value="">วัตถุประสงค์การขอสินเชื่อ</option>
                        <?php if ($mst['master_loan_objective']): ?>
                            <?php foreach ($mst['master_loan_objective'] as $data): ?>
                                <option value="<?php echo $data['loan_objective_id']; ?>" <?php if ($form_data['loan_objective_id'] == $data['loan_objective_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['loan_objective_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>

        <?php if ($form_page): ?>
            <?php $this->load->view("form_{$form_page}", array('data' => '')); ?>
        <?php endif; ?>

    </div>
</div><!-- panel-body -->
</div><!-- panel -->
