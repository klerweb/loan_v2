<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->

        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_guarantee"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/edit_loan/{$loan_data['loan_id']}"); ?>"><button type="button" class="btn btn-default">กลับเมนูแบบบันทึกขอสินเชื่อ</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>


        <form id="form_loan_objective" name="form_loan_objective" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_loan_objective/save"); ?>">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จำนวนเงินทั้งสิ้น (บาท) <span class="asterisk">*</span></label>
                        <input min="0" type="text" name="loan_amount" id="loan_amount" placeholder="จำนวนเงินทั้งสิ้น (บาท)" class="form-control money_right" value="<?php echo $loan_data['loan_amount']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ระยะเวลาการผ่อนชำระ (ปี) <span class="asterisk">*</span></label>
                        <input min="0" max="30" type="number" name="loan_period_year" id="loan_period_year" placeholder="ระยะเวลาการผ่อนชำระ (ปี)" class="form-control" value="<?php echo $loan_data['loan_period_year']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นำไปใช้ประโยชน์ เพื่อการ ดังนี้ <span class="asterisk">*</span></label>
                        <textarea class="form-control" rows="5" name="loan_desc" id="loan_desc" required><?php echo $loan_data['loan_desc']; ?></textarea>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->
