<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name;?></h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <form id="form_guarantee_bond" name="form_guarantee_bond" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_guarantee_bond/save"); ?>" enctype="multipart/form-data">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $loan_data['loan_id']; ?>">
            <input type="hidden" name="loan_bond_id" id="loan_bond_id" value="<?php echo $form_data['loan_bond_id']; ?>">
            <div class="row">
<!--                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ - นามสกุล ของเจ้าของกรรมสิทธิ์</label>
                        <input type="text" name="loan_bond_owner" id="loan_bond_owner" placeholder="ชื่อ - นามสกุล ของเจ้าของกรรมสิทธิ์" class="form-control" value="<?php echo $form_data['loan_bond_owner']; ?>" required>
                    </div>
                </div>-->

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขประจำตัวประชาชน <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_thaiid" id="loan_bond_thaiid" placeholder="เลขประจำตัวประชาชน" class="form-control" value="<?php echo $form_data['loan_bond_thaiid']; ?>" maxlength="13" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขประจำตัวผู้เสียภาษี (ถ้ามี)</label>
                        <input type="text" name="loan_bond_tax" id="loan_bond_tax" placeholder="เลขประจำตัวผู้เสียภาษี" class="form-control" value="<?php echo $form_data['loan_bond_tax']; ?>" maxlength="13" readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                        <select class="form-control" id="loan_bond_title_id" name="loan_bond_title_id" required>
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($form_data['loan_bond_title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_fname" id="loan_bond_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $form_data['loan_bond_fname']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_lname" id="loan_bond_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $form_data['loan_bond_lname']; ?>" required>
                    </div>
                </div>                
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชนิดพันธบัตร <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_type" id="loan_bond_type" placeholder="ชนิดพันธบัตร" class="form-control" value="<?php echo $form_data['loan_bond_type']; ?>" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ต้น <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_startnumber" id="loan_bond_startnumber" placeholder="เลขที่ต้น" class="form-control" value="<?php echo $form_data['loan_bond_startnumber']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ท้าย <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_endnumber" id="loan_bond_endnumber" placeholder="เลขที่ท้าย" class="form-control" value="<?php echo $form_data['loan_bond_endnumber']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ราคาที่ตราไว้ <span class="asterisk">*</span></label>
                        <input type="text" min="0" name="loan_bond_amount" id="loan_bond_amount" placeholder="ราคาที่ตราไว้" class="form-control" value="<?php echo $form_data['loan_bond_amount']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">
                            แนบไฟล์
                            <?php if (isset($form_data['loan_bond_path']) && $form_data['loan_bond_path']): ?>
                                <a href="<?php echo base_url($form_data['loan_bond_path']); ?>" target="_bank" class="">[ดูไฟล์เอกสาร]</a>                            
                            <?php endif; ?>
                        </label>
                        <input type="file" name="loan_bond_path" id="loan_bond_path" placeholder="แนบไฟล์" class="form-control">
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หน้าทะเบียน <span class="asterisk">*</span></label>
                        <input type="text" name="loan_bond_regis_page" id="loan_bond_regis_page" placeholder="หน้าทะเบียน" class="form-control" value="<?php echo $form_data['loan_bond_regis_page']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">วันลงทะเบียน <span class="asterisk">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="วันลงทะเบียน (วัน/เดือน/ปี)" id="loan_bond_regis_date" name="loan_bond_regis_date" value="<?php echo $form_data['loan_bond_regis_date']; ?>" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>                                               
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>

        </form>

    </div><!-- panel-body -->
</div><!-- panel -->
