<?php $class = isset($amphur_class) ? $amphur_class : '' ?>
<?php $required = isset($amphur_required) ? $amphur_required : 'required' ?>

<select name="<?php echo $amphur_name ?>" id="<?php echo $amphur_id ?>" data-placeholder="อำเภอ/เขต" class="width100p <?php echo $class; ?>" <?php echo $required ?>>
    <option value="">อำเภอ/เขต</option>
    <?php foreach ($amphur as $amphur_data) { ?>
        <option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if ($amphur_val == $amphur_data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $amphur_data['amphur_name']; ?></option>
    <?php } ?>
</select>
<script type="text/javascript">
//zipcode_id
<?php if (isset($zipcode_id)): ?>
        var id_list = 'province_id=<?php echo $province_id ?>&amphur_id=<?php echo $amphur_id ?>&district_id=<?php echo $district_id ?>&zipcode_id=<?php echo $zipcode_id ?>';
<?php else: ?>
        var id_list = 'province_id=<?php echo $province_id ?>&amphur_id=<?php echo $amphur_id ?>&district_id=<?php echo $district_id ?>';
<?php endif; ?>


<?php if (isset($province_class) && isset($amphur_class) && isset($district_class)): ?>
        var class_list = 'province_class=<?php echo $province_class ?>&amphur_class=<?php echo $amphur_class ?>&district_class=<?php echo $district_class ?>';
<?php else: ?>
        var class_list = '';
<?php endif; ?>

<?php if (isset($province_required) && isset($amphur_required) && isset($district_required)): ?>
        var required_list = 'province_required=<?php echo $province_required ?>&amphur_required=<?php echo $amphur_required ?>&district_required=<?php echo $district_required ?>';
<?php else: ?>
        var required_list = '';
<?php endif; ?>

    var name_list = 'province_name=<?php echo $province_name ?>&amphur_name=<?php echo $amphur_name ?>&district_name=<?php echo $district_name ?>';
    var param_url = "/?" + id_list + "&" + name_list + "&" + class_list + "&" + required_list;

    $("#<?php echo $amphur_id ?>").select2({
        minimumResultsForSearch: -1
    });

    $("#<?php echo $amphur_id ?>").change(function () {
        $("#div_<?php echo $district_id ?>").load(base_url + "loan/district/" + $("#<?php echo $amphur_id ?>").val() + param_url);
    });

<?php if (isset($amphur_readonly) && $amphur_readonly == 'readonly'): ?>
        $("#<?php echo $amphur_id ?>").select2('readonly', true);
<?php endif; ?>
</script>