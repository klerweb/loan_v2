<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <div class="row">
            <div class="col-sm-7">
                <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
                <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
            </div>
            <div class="col-sm-4 right">
                <!--back & next page-->
                <div class="form-group form-group-loan">
                    <a href="<?php echo site_url("loan/form_loan_expenditure"); ?>"><button type="button" class="btn btn-default mr5">ย้อนกลับ</button></a>
                    <a href="<?php echo site_url("loan/form_loan_asset"); ?>"><button type="button" class="btn btn-default">ถัดไป</button></a>
                </div>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <?php if ($table_data): ?>
            <div class="table-responsive"  style="display:none;">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>เลขที่เอกสารสิทธิ</th>
                            <th>ประเภทที่ดิน</th>
                            <th>ชื่อผู้ถือกรรมสิทธิ์</th>
                            <th>แก้ไข / ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($table_data as $key => $row): ?>
                            <tr>
                                <td><?php echo $key + 1; ?></td>
                                <td><?php echo $row['land_no']; ?></td>
                                <td><?php echo $row['land_type_name']; ?></td>
                                <td><?php echo "{$row['title_name']}{$row['person_fname']} {$row['person_lname']}"; ?></td>
                                <td>
                                    <a href="<?php echo site_url("loan/form_land_occupy/?edit={$row['loan_land_occupy_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                    &nbsp;&nbsp;
                                    <a href="<?php echo site_url("loan/form_land_occupy/delete/{$row['loan_land_occupy_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                        <?php endforeach;
                        ?>
                    </tbody>
                </table>

                <hr>
            </div><!-- table-responsive -->
        <?php endif; ?>


        <form id="form_land_occupy" name="form_land_occupy" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_land_occupy/save"); ?>">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $form_data['loan_id']; ?>">
            <input type="hidden" name="loan_land_occupy_id" id="loan_land_occupy_id" value="<?php echo $form_data['loan_land_occupy_id']; ?>">
            <input type="hidden" name="land_owner_id" id="land_owner_id" value="<?php echo $form_data['land_owner_id']; ?>">

            <!--ข้อมูลที่ดินและสิ่งปลูกสร้าง-->
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่ดินและสิ่งปลูกสร้าง</h4>
                        <a href="<?php echo site_url("loan/form_land_list/?table_land_building_param={$table_land_building_param}"); ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มข้อมูลที่ดิน</a>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>ที่ดิน / สิ่งปลูกสร้าง</th>
                            <th>ประเภทสิ่งปลูกสร้าง</th>
                            <th>สถานที่ตั้ง</th>
                            <th>เพิ่ม/แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <?php if ($table_land_building): ?>
                        <tbody>
                            <?php foreach ($table_land_building as $key_land => $land): ?>
                                <tr>
                                    <td><?php echo ($key_land + 1); ?></td>
                                    <td>ที่ดิน</td>
                                    <td>-</td>
                                    <td><?php echo $land['address']; ?></td>
                                    <td>
                                        <a href="<?php echo site_url("loan/form_building_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}"); ?>" class=""><span class="glyphicon glyphicon-plus"></span></a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/form_land_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/del_land_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>

                                <?php if (isset($land['building_table'])): ?>
                                    <?php foreach ($land['building_table'] as $key_building => $building): ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>สิ่งปลูกสร้าง</td>
                                            <td><?php echo $building['building_type_name']; ?></td>
                                            <td><?php echo $building['address']; ?></td>
                                            <td>
                                                <a href="<?php echo site_url("loan/form_building_list/?table_land_building_param={$table_land_building_param}&edit=yes&land_id={$land['land_id']}&owner_id={$land['owner_id']}&building_id={$building['building_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>
                                                &nbsp;&nbsp;
                                                <a href="<?php echo site_url("loan/del_building_list/?table_land_building_param={$table_land_building_param}&land_id={$land['land_id']}&owner_id={$land['owner_id']}&building_id={$building['building_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
            <!--ข้อมูลที่ดินและสิ่งปลูกสร้าง-->


            <?php if (!$form_data['loan_land_occupy_id']): ?>
                <hr>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ผู้ถือกรรมสิทธิ์ <span class="asterisk">*</span></label>
                            <div class="radio">
                                <?php if ($mst['owner_list']): ?>
                                    <?php foreach ($mst['owner_list'] as $key_owner_list => $value): ?>
                                <label><input type="radio" name="land_owner" class="land_owner" id="land_owner_<?php echo $key_owner_list ?>" value="<?php echo $key_owner_list; ?>" <?php if ($key_owner_list == 1): ?>checked="checked"<?php endif; ?> required><?php echo $value; ?><?php echo nbs(2); ?></label>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="owner_other_form">
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-loan">
                                <h4>บุคคลอื่น</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-loan">
                                <label class="control-label">คำนำหน้าชื่อ <span class="asterisk">*</span></label>
                                <select class="form-control" id="title_id" name="owner_other_title_id" required="required">
                                    <option value="">คำนำหน้าชื่อ</option>
                                    <?php if ($mst['config_title_list']): ?>
                                        <?php foreach ($mst['config_title_list'] as $data): ?>
                                            <option value="<?php echo $data['title_id']; ?>" <?php if ($owner_other_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group form-group-loan">
                                <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                                <input type="text" name="owner_other_person_fname" id="owner_other_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $owner_other_data['person_fname']; ?>" required="required">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group form-group-loan">
                                <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                                <input type="text" name="owner_other_person_lname" id="owner_other_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $owner_other_data['person_lname']; ?>" required="required">
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่ดิน</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ไร่) <span class="asterisk">*</span></label>
                        <input type="number" min="0" name="land_area_rai" id="land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $form_data['land_area_rai']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(งาน) <span class="asterisk">*</span></label>
                        <input type="number" min="0" max="<?php echo get_max_ngan(); ?>" name="land_area_ngan" id="land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $form_data['land_area_ngan']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ตารางวา) <span class="asterisk">*</span></label>
                        <input type="number" min="0" max="<?php echo get_max_wa(); ?>" name="land_area_wah" id="land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $form_data['land_area_wah']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทเอกสารสิทธิ <span class="asterisk">*</span></label>
                        <select class="form-control" id="land_type_id" name="land_type_id" required>
                            <option value="">ประเภทเอกสารสิทธิ</option>
                            <?php if ($mst['config_land_type_list']): ?>
                                <?php foreach ($mst['config_land_type_list'] as $data): ?>
                                    <option value="<?php echo $data['land_type_id']; ?>" <?php if ($form_data['land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ <span class="asterisk">*</span></label>
                        <input type="text" name="land_no" id="land_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['land_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ดิน <span class="asterisk">*</span></label>
                        <input type="number" name="land_addr_no" id="land_addr_no" placeholder="ที่ตั้ง เลขที่" class="form-control" value="<?php echo $form_data['land_addr_no']; ?>" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label>
                        <input type="text" name="land_addr_moo" id="land_addr_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['land_addr_moo']; ?>" maxlength="2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <div id="div_land_addr_province_id">
                            <select class="width100p" id="land_addr_province_id" name="land_addr_province_id" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                        <div id="div_land_addr_amphur_id">
                            <select class="width100p" id="land_addr_amphur_id" name="land_addr_amphur_id" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_land_addr_district_id">
                            <select class="width100p" id="land_addr_district_id" name="land_addr_district_id" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">มูลค่าที่ดิน</label>
                        <input type="text" name="estimate_price" id="estimate_price" placeholder="ราคาประเมิณ" class="form-control money_right" value="<?php echo $form_data['estimate_price']; ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group form-group-loan">
                        <label class="control-label">การใช้ประโยชน์ในที่ดิน <span class="asterisk">*</span></label>
                        <textarea class="form-control" rows="5" maxlength="200" name="loan_land_occupy_benefit" id="loan_land_occupy_benefit" placeholder="การใช้ประโยชน์ในที่ดิน" required><?php echo $form_data['loan_land_occupy_benefit'] ?></textarea>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>
        </form>


    </div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
/*land_owner (1,2,3)*/
<?php if (in_array($form_data['land_owner'], array(1,2,3))): ?>
        $("#owner_other_form").hide();
<?php endif; ?>
    /*land_owner change*/
    $(".land_owner").change(function () {
        var land_owner = $(this).val();

        /*land_owner (1,2,3)*/
        if (
                land_owner == 1 ||
                land_owner == 2 ||
                land_owner == 3
           ) {
            $("#owner_other_form").hide();
        } else {
            $("#owner_other_form").show();
        }
    });




    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id&zipcode_id=land_addr_zipcode';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

<?php if ($form_data['land_addr_amphur_id']): ?>
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + "/" + <?php echo$form_data['land_addr_district_id'] ?> + param_url_pad_land_addr);
<?php endif; ?>
</script>
