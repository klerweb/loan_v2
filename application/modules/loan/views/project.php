<select name="project_id" id="project_id" data-placeholder="กรุณาเลือก" class="width100p" required>
	<option value="">กรุณาเลือก</option>
	<?php foreach ($project as $project_data) { ?>
		<option value="<?php echo $project_data['project_id']; ?>"><?php echo $project_data['project_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#project_id").select2({
		minimumResultsForSearch: -1
	});
</script>