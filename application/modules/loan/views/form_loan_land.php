<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : การขอสินเชื่อ</h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group form-group-loan">
                    <label class="control-label">วัตถุประสงค์การขอสินเชื่อ</label>
                    <input type="hidden" name="curr_url" id="curr_url" value="<?php echo site_url("loan/form_land"); ?>">
                    <select class="form-control" id="loan_objective_id" name="loan_objective_id">
                        <option value="">วัตถุประสงค์การขอสินเชื่อ</option>
                        <?php if ($master_loan_objective): ?>
                            <?php foreach ($master_loan_objective as $data): ?>
                                <option value="<?php echo $data['loan_objective_id']; ?>" <?php if ($form_data['loan_objective_id'] == $data['loan_objective_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['loan_objective_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
            </div>
        </div>

        <?php if ($form_page): ?>
            <?php $this->load->view("form_land_{$form_page}",array('data'=>'')); ?>
        <?php endif; ?>

    </div>
</div><!-- panel-body -->
</div><!-- panel -->
