<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">แบบบันทึกขอสินเชื่อ (บจธ 1) : <?php echo $form_name; ?></h4>
        <p class="panel-title form-list-loan-code">เลขที่คำขอสินเชื่อ : <?php echo $loan_code; ?></p>
    </div>
    <div class="panel-body">
        <?php if ($session_form && $session_form['status'] == 'success'): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $session_form['message']; ?>
            </div>
        <?php endif; ?>

        <?php if ($form_data['loan_guarantee_land_id']): ?>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลสิ่งปลูกสร้าง</h4>
                        <a href="<?php echo site_url("loan/form_building_list/?land_id={$form_data['land_id']}&table_land_building_param={$table_land_building_param}"); ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;เพิ่มข้อมูลสิ่งปลูกสร้าง</a>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped mb30">
                    <thead>
                        <tr>
                            <th>ลำดับที่</th>
                            <th>ประเภทสิ่งปลูกสร้าง</th>
                            <th>สถานที่ตั้ง</th>
                            <th>แก้ไข/ลบ</th>
                        </tr>
                    </thead>
                    <?php if ($building_table): ?>
                        <tbody>
                            <?php foreach ($building_table as $data): ?>
                                <tr>
                                    <td><?php echo $data['building_id']; ?></td>
                                    <td><?php echo $data['building_type_name']; ?></td>
                                    <td><?php echo $data['address']; ?></td>
                                    <td>
            <!--                                <a href="<?php echo site_url("loan/form_doc/?view=1"); ?>" class=""><span class="glyphicon glyphicon-zoom-in"></span></a>
                                        &nbsp;&nbsp;-->

                                        <a href="<?php echo site_url("loan/form_building_list/?land_id={$form_data['land_id']}&edit=yes&owner_id={$person_data['person_id']}&building_id={$data['building_id']}&table_land_building_param={$table_land_building_param}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>

                                        <!--<a href="<?php echo site_url("loan/form_guarantee_land/?edit={$form_data['loan_guarantee_land_id']}&building_id={$data['building_id']}"); ?>" class="btn_confirm_edit"><span class="glyphicon glyphicon-edit"></span></a>-->
                                        &nbsp;&nbsp;
                                        <a href="<?php echo site_url("loan/form_guarantee_land/delete_building/{$data['building_id']}/?loan_guarantee_land_id={$form_data['loan_guarantee_land_id']}"); ?>" class="btn_confirm_del"><span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php endif; ?>

                </table>
            </div><!-- table-responsive -->

            <!--
            <form id="form_building" name="form_building" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_guarantee_land/save_building"); ?>">
                <input type="hidden" id="loan_guarantee_land_id" name="loan_guarantee_land_id" value="<?php echo $form_data['loan_guarantee_land_id']; ?>">
                <input type="hidden" id="land_id" name="land_id" value="<?php echo $form_data['land_id']; ?>">
                <input type="hidden" id="building_owner" name="building_owner" value="<?php echo $form_data['person_id']; ?>">
                <input type="hidden" id="building_id" name="building_id" value="<?php echo $form_data['building_id']; ?>">

                <?php if (!$form_data['building_id']): ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group form-group-loan">
                                <label class="control-label">เจ้าของสิ่งปลูกสร้าง</label>
                                <div class="radio">
                                    <?php foreach ($mst['buiding_owner_status_list'] as $key => $data): ?>
                                        <label><input type="radio" name="buiding_owner_status" class="buiding_owner_status" id="buiding_owner_status_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['buiding_owner_status'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เจ้าของสิ่งปลูกสร้างคำนำหน้าชื่อ</label>
                            <select class="form-control" id="building_owner_title_id" name="building_owner_title_id" readonly="readonly">
                                <option value="">คำนำหน้าชื่อ</option>
                                <?php if ($mst['config_title_list']): ?>
                                    <?php foreach ($mst['config_title_list'] as $data): ?>
                                        <option value="<?php echo $data['title_id']; ?>" <?php if ($form_data['building_owner_title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ชื่อ</label>
                            <input type="text" name="building_owner_fname" id="building_owner_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $form_data['building_owner_fname']; ?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">นามสกุล</label>
                            <input type="text" name="building_owner_lname" id="building_owner_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $form_data['building_owner_lname']; ?>" readonly="readonly">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ประเภทสิ่งปลูกสร้าง</label>
                            <select class="form-control" id="building_type_id" name="building_type_id" required>
                                <option value="">ประเภทสิ่งปลูกสร้าง</option>
                                <?php if ($mst['config_building_type_list']): ?>
                                    <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                        <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เลขที่</label>
                            <input type="text" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" required>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">หมู่ที่</label>
                            <input type="text" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อาคาร</label>
                            <input type="text" name="building_name" id="building_name" placeholder="อาคาร" class="form-control" value="<?php echo $form_data['building_name']; ?>" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ซอย</label>
                            <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ถนน</label>
                            <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                            <select class="width100p" id="building_province_id" name="building_province_id" required>
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                            <div id="div_building_amphur_id">
                                <select class="width100p" id="building_amphur_id" name="building_amphur_id" required>
                                    <option value="">อำเภอ/เขต</option>
                                    <?php if ($mst['master_amphur_list']): ?>
                                        <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                            <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                            <div id="div_building_district_id">
                                <select class="width100p" id="building_district_id" name="building_district_id" required>
                                    <option value="">ตำบล/แขวง</option>
                                    <?php if ($mst['master_district_list']): ?>
                                        <?php foreach ($mst['master_district_list'] as $data): ?>
                                            <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="row">
                    <div class="col-sm-12 right">
                        <div class="form-group form-group-loan">
                            <button class="btn btn-primary mr5">บันทึก</button>
                            <button type="reset" class="btn btn-default">ยกเลิก</button>
                        </div>
                    </div>
                </div>
            </form>
            -->

            <hr>
        <?php endif; ?>


        <form id="form_guarantee_land" name="form_guarantee_land" class="form-horizontal form-bordered" method="post" action="<?php echo site_url("loan/form_guarantee_land/save"); ?>" enctype="multipart/form-data">
            <input type="hidden" name="loan_id" id="loan_id" value="<?php echo $form_data['loan_id']; ?>">
            <input type="hidden" name="land_id" id="land_id" value="<?php echo $form_data['land_id']; ?>">
            <input type="hidden" name="loan_guarantee_land_id" id="loan_guarantee_land_id" value="<?php echo $form_data['loan_guarantee_land_id']; ?>">
            <input type="hidden" name="loan_type_id" id="loan_type_id" value="">
            <input type="hidden" name="land_owner" id="land_owner" value="<?php echo $loan_model['person_id'] ?>">
            <input type="hidden" name="land_owner_id" id="land_owner_id" value="<?php echo $form_data['land_owner_id'] ?>">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลที่ดิน</h4>
                    </div>
                </div>
            </div>

            <?php if (!$form_data['loan_guarantee_land_id']): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-loan">
                            <div class="radio">
                                <?php foreach ($mst['land_buiding_detail_id_list'] as $key => $data): ?>
                                    <label><input type="radio" name="land_buiding_detail_id" class="land_buiding_detail_id" id="land_buiding_detail_id_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['land_buiding_detail_id'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เจ้าของที่ดินคำนำหน้าชื่อ <span class="asterisk">*</span></label>
                        <select class="form-control" id="land_owner_title_id" required readonly="readonly">
                            <option value="">คำนำหน้าชื่อ</option>
                            <?php if ($mst['config_title_list']): ?>
                                <?php foreach ($mst['config_title_list'] as $data): ?>
                                    <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ชื่อ <span class="asterisk">*</span></label>
                        <input type="text" id="land_owner_person_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" required readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">นามสกุล <span class="asterisk">*</span></label>
                        <input type="text" id="land_owner_person_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" required readonly="readonly">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ประเภทเอกสารสิทธิ <span class="asterisk">*</span></label>
                        <select class="form-control" id="land_type_id" name="land_type_id" required>
                            <option value="">ประเภทเอกสารสิทธิ</option>
                            <?php if ($mst['config_land_type_list']): ?>
                                <?php foreach ($mst['config_land_type_list'] as $data): ?>
                                    <option value="<?php echo $data['land_type_id']; ?>" <?php if ($form_data['land_type_id'] == $data['land_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['land_type_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ <span class="asterisk">*</span></label>
                        <input type="text" name="land_no" id="land_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['land_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เลขที่ดิน <span class="asterisk">*</span></label>
                        <input type="number" name="land_addr_no" id="land_addr_no" placeholder="เลขที่ดิน" class="form-control" value="<?php echo $form_data['land_addr_no']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">หมู่ที่</label>
                        <input type="text" name="land_addr_moo" id="land_addr_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['land_addr_moo']; ?>" maxlength="2">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">จังหวัด <span class="asterisk">*</span></label>
                        <select class="width100p" id="land_addr_province_id" name="land_addr_province_id" required>
                            <option value="">จังหวัด</option>
                            <?php if ($mst['master_province_list']): ?>
                                <?php foreach ($mst['master_province_list'] as $data): ?>
                                    <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['land_addr_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">อำเภอ/เขต <span class="asterisk">*</span></label>
                        <div id="div_land_addr_amphur_id">
                            <select class="width100p" id="land_addr_amphur_id" name="land_addr_amphur_id" required>
                                <option value="">อำเภอ/เขต</option>
                                <?php if ($mst['master_amphur_list']): ?>
                                    <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                        <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['land_addr_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ตำบล/แขวง <span class="asterisk">*</span></label>
                        <div id="div_land_addr_district_id">
                            <select class="width100p" id="land_addr_district_id" name="land_addr_district_id" required>
                                <option value="">ตำบล/แขวง</option>
                                <?php if ($mst['master_district_list']): ?>
                                    <?php foreach ($mst['master_district_list'] as $data): ?>
                                        <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['land_addr_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ไร่) <span class="asterisk">*</span></label>
                        <input type="number" min="0" name="land_area_rai" id="land_area_rai" placeholder="เนื้อที่(ไร่)" class="form-control" value="<?php echo $form_data['land_area_rai']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(งาน) <span class="asterisk">*</span></label>
                        <input type="number" min="0" max="<?php echo get_max_ngan(); ?>" name="land_area_ngan" id="land_area_ngan" placeholder="เนื้อที่(งาน)" class="form-control" value="<?php echo $form_data['land_area_ngan']; ?>" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">เนื้อที่(ตารางวา) <span class="asterisk">*</span></label>
                        <input type="number" min="0" max="<?php echo get_max_wa(); ?>" name="land_area_wah" id="land_area_wah" placeholder="เนื้อที่(ตารางวา)" class="form-control" value="<?php echo $form_data['land_area_wah']; ?>" required>
                    </div>
                </div>
            </div>

            <?php if (!$form_data['loan_guarantee_land_id']): ?>

                <hr class=" div_land_building">
                <div class="row div_land_building">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <h4>ข้อมูลที่ดินพร้อมสิ่งปลูกสร้าง</h4>
                        </div>
                    </div>
                </div>

                <div class="row div_land_building">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เจ้าของสิ่งปลูกสร้าง</label>
                            <div class="radio">
                                <?php foreach ($mst['buiding_owner_status_list'] as $key => $data): ?>
                                    <label><input type="radio" name="buiding_owner_status" class="buiding_owner_status" id="buiding_owner_status_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['buiding_owner_status'] == $key): ?>checked="checked"<?php endif; ?>><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row div_land_building">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เจ้าของสิ่งปลูกสร้างคำนำหน้าชื่อ</label>
                            <select class="form-control" id="building_owner_title_id" name="building_owner_title_id" readonly="readonly">
                                <option value="">คำนำหน้าชื่อ</option>
                                <?php if ($mst['config_title_list']): ?>
                                    <?php foreach ($mst['config_title_list'] as $data): ?>
                                        <option value="<?php echo $data['title_id']; ?>" <?php if ($person_data['title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ชื่อ</label>
                            <input type="text" name="building_owner_fname" id="building_owner_fname" placeholder="ชื่อ" class="form-control" value="<?php echo $person_data['person_fname']; ?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">นามสกุล</label>
                            <input type="text" name="building_owner_lname" id="building_owner_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $person_data['person_lname']; ?>" readonly="readonly">
                        </div>
                    </div>
                </div>

                <div class="row div_land_building">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ประเภทสิ่งปลูกสร้าง</label>
                            <select class="form-control" id="building_type_id" name="building_type_id" >
                                <option value="">ประเภทสิ่งปลูกสร้าง</option>
                                <?php if ($mst['config_building_type_list']): ?>
                                    <?php foreach ($mst['config_building_type_list'] as $data): ?>
                                        <option value="<?php echo $data['building_type_id']; ?>" <?php if ($form_data['building_type_id'] == $data['building_type_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['building_type_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เลขที่</label>
                            <input type="text" name="building_no" id="building_no" placeholder="เลขที่" class="form-control" value="<?php echo $form_data['building_no']; ?>" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">หมู่ที่</label>
                            <input type="text" name="building_moo" id="building_moo" placeholder="หมู่ที่" class="form-control" value="<?php echo $form_data['building_moo']; ?>" maxlength="2">
                        </div>
                    </div>
                </div>

                <div class="row div_land_building">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อาคาร</label>
                            <input type="text" name="building_name" id="building_name" placeholder="อาคาร" class="form-control" value="<?php echo $form_data['building_name']; ?>" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ซอย</label>
                            <input type="text" name="building_soi" id="building_soi" placeholder="ซอย" class="form-control" value="<?php echo $form_data['building_soi']; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ถนน</label>
                            <input type="text" name="building_road" id="building_road" placeholder="ถนน" class="form-control" value="<?php echo $form_data['building_road']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row div_land_building">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">จังหวัด</label>
                            <select class="width100p" id="building_province_id" name="building_province_id">
                                <option value="">จังหวัด</option>
                                <?php if ($mst['master_province_list']): ?>
                                    <?php foreach ($mst['master_province_list'] as $data): ?>
                                        <option value="<?php echo $data['province_id']; ?>" <?php if ($form_data['building_province_id'] == $data['province_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['province_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">อำเภอ/เขต</label>
                            <div id="div_building_amphur_id">
                                <select class="width100p" id="building_amphur_id" name="building_amphur_id">
                                    <option value="">อำเภอ/เขต</option>
                                    <?php if ($mst['master_amphur_list']): ?>
                                        <?php foreach ($mst['master_amphur_list'] as $data): ?>
                                            <option value="<?php echo $data['amphur_id']; ?>" <?php if ($form_data['building_amphur_id'] == $data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['amphur_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ตำบล/แขวง</label>
                            <div id="div_building_district_id">
                                <select class="width100p" id="building_district_id" name="building_district_id">
                                    <option value="">ตำบล/แขวง</option>
                                    <?php if ($mst['master_district_list']): ?>
                                        <?php foreach ($mst['master_district_list'] as $data): ?>
                                            <option value="<?php echo $data['district_id']; ?>" <?php if ($form_data['building_district_id'] == $data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['district_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลการมีภาระผูกพัน</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <?php if ($mst['loan_guarantee_land_bind_type_list']): ?>
                            <div class="radio">
                                <?php foreach ($mst['loan_guarantee_land_bind_type_list'] as $key => $data): ?>
                                    <label><input type="radio" class="bind_type" name="loan_guarantee_land_bind_type" id="loan_guarantee_land_bind_type_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php if ($form_data['loan_guarantee_land_bind_type'] == $key): ?>checked="checked"<?php endif; ?> ><?php echo $data; ?><?php echo nbs(2); ?></label>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div id="loan_guarantee_land_form">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">คำนำหน้าชื่อ</label>
                            <select class="form-control" id="loan_guarantee_land_title_id" name="loan_guarantee_land_title_id" >
                                <option value="">คำนำหน้าชื่อ</option>
                                <?php if ($mst['config_title_list']): ?>
                                    <?php foreach ($mst['config_title_list'] as $data): ?>
                                        <option value="<?php echo $data['title_id']; ?>" <?php if ($form_data['loan_guarantee_land_title_id'] == $data['title_id']): ?>selected="selected"<?php endif; ?>><?php echo $data['title_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">ชื่อผู้รับจำนำ</label>
                            <input type="text" name="loan_guarantee_land_fname" id="loan_guarantee_land_fname" placeholder="ชื่อผู้รับจำนำ" class="form-control" value="<?php echo $form_data['loan_guarantee_land_fname']; ?>" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">นามสกุล</label>
                            <input type="text" name="loan_guarantee_land_lname" id="loan_guarantee_land_lname" placeholder="นามสกุล" class="form-control" value="<?php echo $form_data['loan_guarantee_land_lname']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">วงเงิน (บาท)</label>
                            <input type="text" name="loan_guarantee_land_bind_amount" id="loan_guarantee_land_bind_amount" placeholder="วงเงิน (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_guarantee_land_bind_amount']; ?>" >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">โดยการผ่อนชำระกับ</label>
                            <input type="text" name="loan_guarantee_land_relief" id="loan_guarantee_land_relief" placeholder="โดยการผ่อนชำระ" class="form-control" value="<?php echo $form_data['loan_guarantee_land_relief']; ?>" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">เดือนละ (บาท)</label>
                            <input type="text" name="loan_guarantee_land_relief_permonth" id="loan_guarantee_land_relief_permonth" placeholder="เดือนละ (บาท)" class="form-control money_right" value="<?php echo $form_data['loan_guarantee_land_relief_permonth']; ?>" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group form-group-loan">
                            <label class="control-label">วันครบกำหนดผ่อนชำระ  (วัน-เดือน-ปี : <?php echo date_th_now(); ?>)</label>
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="ปฎิทินไทย (วัน/เดือน/ปี)" id="loan_guarantee_land_relief_enddate" name="loan_guarantee_land_relief_enddate" value="<?php echo $form_data['loan_guarantee_land_relief_enddate']; ?>">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <h4>ข้อมูลการใช้ประโยชน์ที่ดิน</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-10">
                    <div class="form-group form-group-loan">
                        <?php if ($mst['loan_guarantee_land_benefit_list']): ?>
                            <div class="checkbox">
                              <?php
                              $loan_guarantee_land_benefit = explode(',', $form_data ['loan_guarantee_land_benefit']);
                              ?>
                                <?php foreach ($mst['loan_guarantee_land_benefit_list'] as $key => $data): ?>
                                    <label><input type="checkbox" name="loan_guarantee_land_benefit[]" class="loan_guarantee_land_benefit"
                                      value="<?php echo $key; ?>"  <?php if (in_array($key, $loan_guarantee_land_benefit)) { ?>
                                          checked="checked" <?php } ?> > <?php echo $data; ?><?php echo nbs(2); ?></label>
                                    <br>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group form-group-loan">
                        <label class="control-label">ระบุ</label>
                        <input type="text" name="loan_guarantee_land_benefit_other" id="loan_guarantee_land_benefit_other" placeholder="ระบุ" class="form-control" value="<?php echo $form_data['loan_guarantee_land_benefit_other']; ?>" >
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-sm-12 right">
                    <div class="form-group form-group-loan">
                        <button class="btn btn-primary mr5">บันทึก</button>
                        <button type="reset" class="btn btn-default">ยกเลิก</button>
                    </div>
                </div>
            </div>


        </form>


    </div><!-- panel-body -->
</div><!-- panel -->

<script type="text/javascript">
    /*province, amphur, district for land_addr*/
    var pad_id_land_addr_list = 'province_id=land_addr_province_id&amphur_id=land_addr_amphur_id&district_id=land_addr_district_id';
    var pad_name_land_addr_list = 'province_name=land_addr_province_id&amphur_name=land_addr_amphur_id&district_name=land_addr_district_id';
    var param_url_pad_land_addr = "/?" + pad_id_land_addr_list + "&" + pad_name_land_addr_list;

<?php if ($form_data['land_addr_amphur_id']): ?>
        $("#div_land_addr_amphur_id").load(base_url + "loan/amphur/" + $("#land_addr_province_id").val() + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + param_url_pad_land_addr);
        $("#div_land_addr_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['land_addr_amphur_id'] ?> + "/" + <?php echo$form_data['land_addr_district_id'] ?> + param_url_pad_land_addr);
<?php endif; ?>
</script>

<script type="text/javascript">
    /*province, amphur, district for building*/
    var pad_id_building_list = 'province_id=building_province_id&amphur_id=building_amphur_id&district_id=building_district_id';
    var pad_name_building_list = 'province_name=building_province_id&amphur_name=building_amphur_id&district_name=building_district_id';
    var param_url_pad_building = "/?" + pad_id_building_list + "&" + pad_name_building_list;

<?php if ($form_data['building_amphur_id']): ?>
        $("#div_building_amphur_id").load(base_url + "loan/amphur/" + $("#building_province_id").val() + "/" + <?php echo $form_data['building_amphur_id'] ?> + param_url_pad_building);
        $("#div_building_district_id").load(base_url + "loan/district" + "/" + <?php echo $form_data['building_amphur_id'] ?> + "/" + <?php echo$form_data['building_district_id'] ?> + param_url_pad_building);
<?php endif; ?>
</script>

<script>
<?php if (!$form_data['building_id']): ?>
        $(".buiding_owner_status").change(function () {
            var title_id = $(this).val();
            if (title_id == 0) { //ตนเอง
                $("#building_owner_title_id").val('<?php echo $person_data['title_id']; ?>');
                $("#building_owner_fname").val('<?php echo $person_data['person_fname']; ?>');
                $("#building_owner_lname").val('<?php echo $person_data['person_lname']; ?>');

                $('#building_owner_title_id').attr('readonly', true);
                $('#building_owner_fname').attr('readonly', true);
                $('#building_owner_lname').attr('readonly', true);
            } else {
                $("#building_owner_title_id").val('');
                $("#building_owner_fname").val('');
                $("#building_owner_lname").val('');

                $('#building_owner_title_id').attr('readonly', false);
                $('#building_owner_fname').attr('readonly', false);
                $('#building_owner_lname').attr('readonly', false);
            }
        });
<?php else: ?>
        $('#building_owner_title_id').attr('readonly', false);
        $('#building_owner_fname').attr('readonly', false);
        $('#building_owner_lname').attr('readonly', false);
<?php endif; ?>



<?php if ($form_data['loan_guarantee_land_bind_type'] != 1): ?>
        $("#loan_guarantee_land_form").hide();
<?php endif; ?>

    /*bind_type change*/
    $(".bind_type").change(function () {
        var bind_type = $(this).val();
        /*1*/
        if (bind_type == 1) {
            $("#loan_guarantee_land_form").show();
        } else {
            $("#loan_guarantee_land_form").hide();
        }
    });
//
// <?php if (!in_array($form_data['loan_guarantee_land_benefit'],array(3,4))): ?>
//         $('#loan_guarantee_land_benefit_other').attr('readonly', true);
//         $('#loan_guarantee_land_benefit_other').val('');
// <?php else: ?>
//         $('#loan_guarantee_land_benefit_other').attr('readonly', false);
// <?php endif; ?>

    /*loan_guarantee_land_benefit change*/
    $(".loan_guarantee_land_benefit").click(function () {
        var loan_guarantee_land_benefit = $(this).val();
      
        /*3,4*/
        if (
                loan_guarantee_land_benefit == 3 ||
                loan_guarantee_land_benefit == 4
            ) {
            $('#loan_guarantee_land_benefit_other').attr('readonly', false);
        } else {
            $('#loan_guarantee_land_benefit_other').attr('readonly', true);
            $('#loan_guarantee_land_benefit_other').val('');
        }
    });



</script>
