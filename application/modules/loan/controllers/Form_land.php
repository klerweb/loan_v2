<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_land extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกการขอสินเชื่อ';
    private $form_name = 'การขอสินเชื่อ';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_type_model');
        $this->load->model('loan_model');
        $this->load->model('land_model');
        $this->load->model('land_owner_model');
        $this->load->model('building_model');
        $this->load->model('person_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();

        //check_has_formland
        $this->check_has_formland();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //page_form
        $form_page = $this->uri->segment(4);

        //form_data
        $form_data['loan_objective_id'] = '';

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //loan_type
        $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

        //mst
        $mst = array(
            'master_loan_objective' => $this->mst_model->get_all('loan_objective')
        );

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'table_data' => $table_data,
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'mst' => $mst,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'content' => $this->parser->parse('form_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function land_1() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //sum_loan_amount
        $this->loan_lib->sum_loan_amount($loan_model['person_id'], 1);

        //page_form
        $form_page = $this->uri->segment(3);

        //$form_data
        $form_data = array_merge($this->loan_lib->land_fields(), $this->loan_lib->building_fields());
        $form_data['loan_id'] = $loan_id;
        $form_data['form_page'] = $form_page;
        $form_data['loan_objective_id'] = 1;
        $form_data['land_id'] = '';
        $form_data['loan_type_redeem_amount'] = money_num_to_str(0);
        $form_data['loan_type_redeem_relationship'] = 6; //ตนเอง
        $form_data['buiding_owner_status'] = 0; //ตนเอง
        //redeem_owner
        $person_id = $loan_model['person_id'];
        if ($person_id) {
            $redeem_owner = $this->loan_lib->person_detail($person_id);
            $person_data = $this->loan_lib->person_detail($person_id);
        } else {
            $redeem_owner = $this->loan_lib->person_fields();
            $person_data = $this->loan_lib->person_fields();
        }

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';

        //get
        $get = $this->input->get();
        if ($get) {
            if (isset($get['edit'])) {
                $loan_type_model = $this->loan_type_model->get_by_id($get['edit']);
                $loan_type_model['loan_type_redeem_amount'] = money_num_to_str($loan_type_model['loan_type_redeem_amount']);
                $land_model = $this->land_model->get_row_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));

                $form_data = array_merge($loan_type_model, $land_model, $this->loan_lib->building_fields());
                $form_data['loan_id'] = $loan_id;
                $form_data['form_page'] = $form_page;
                $form_data['land_id'] = $land_model ? $land_model['land_id'] : '';

                //$redeem_owner
                $redeem_owner = $this->person_model->get_by_id($loan_type_model['loan_type_redeem_owner']);
                $redeem_owner['person_birthdate'] = den_to_dth($redeem_owner['person_birthdate']);
                $redeem_owner['person_card_startdate'] = den_to_dth($redeem_owner['person_card_startdate']);
                $redeem_owner['person_card_expiredate'] = den_to_dth($redeem_owner['person_card_expiredate']);

                //table_land_building
                $land_model = $this->land_model->get_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                if ($land_model) {
                    $table_land_building = array();
                    foreach ($land_model as $key_land => $land) {
                        $table_land_building[$key_land] = $land;
                        $land_full_detail = $this->loan_lib->land_full_detail($land);
                        $table_land_building[$key_land]['address'] = $land_full_detail['address'];
                        $table_land_building[$key_land]['owner_id'] = $loan_model['person_id'];

                        $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));
                        if ($building_model) {
                            foreach ($building_model as $key_building => $building) {
                                $building_full_detail = $this->loan_lib->building_full_detail($building);
                                $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                                $building_model[$key_building]['address'] = $building_full_detail['address'];
                            }

                            $table_land_building[$key_land]['building_table'] = $building_model;
                        }
                    }
                }

                //$table_land_building_param
                $table_land_building_data = array(
                    'callbackurl' => base64_encode("loan/form_land/land_1/?edit={$get['edit']}"),
                    'owner_id' => $loan_model['person_id'],
                    'save_loan_guarantee_land' => 1,
                    'loan_type_id' => $loan_type_model['loan_type_id'],
                );
                //echo '<pre>';
                //print_r($table_land_building_data);
                //echo '</pre>';

                $table_land_building_param = json_encode($table_land_building_data);
                $table_land_building_param = base64_encode($table_land_building_param);
            }
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //loan_type
        $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

        //mst
        $mst = array(
            //config & master for person
            'config_title_list' => $this->mst_model->get_all('title'),
            'config_race_list' => $this->mst_model->get_all('race'),
            'config_nationality_list' => $this->mst_model->get_all('nationality'),
            'config_religion_list' => $this->mst_model->get_all('religion'),
            'config_career_list' => $this->mst_model->get_all('career'),
            'master_sex_list' => $this->mst_model->get_all('sex'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            'master_status_married_list' => $this->mst_model->get_all('status_married'),
            //config & master for land
            'config_relationship_list' => $this->mst_model->get_all('relationship'),
            'config_land_type' => $this->mst_model->get_all('land_type'),
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
            //config & master for building
            'config_building_type_list' => $this->mst_model->get_all('building_type'),
            'land_buiding_detail_id_list' => $this->land_model->get_land_buiding_detail_id_list(),
            'buiding_owner_status_list' => $this->land_model->get_buiding_owner_status_list(),
        );

        //redeem_type
        $redeem_type_data = $this->loan_type_model->get_redeem_type();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'person_data' => $person_data,
            'table_data' => $table_data,
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'mst' => $mst,
            'redeem_owner' => $redeem_owner,
            'redeem_type_data' => $redeem_type_data,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'content' => $this->parser->parse('form_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function land_1_view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //page_form
        $form_page = $this->uri->segment(3);
        $loan_type_id = $this->uri->segment(4);

        //loan_type_model
        $loan_type_model = $this->loan_type_model->get_by_id($loan_type_id);
        $loan_type_model['loan_type_redeem_type_title'] = $this->loan_type_model->redeem_type[$loan_type_model['loan_type_redeem_type']];

        //land_model
        $land_model = $this->land_model->get_row_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
        $land_model = $this->land_model->get_view($land_model['land_id']);

        //loan_objective
        $loan_objective = $this->mst_model->get_by_opt_table('master_loan_objective', 'loan_objective_id', $loan_type_model['loan_objective_id']);

        //$redeem_owner
        $redeem_owner = $this->person_model->get_by_id($loan_type_model['loan_type_redeem_owner']);


        exit;
        //building
        $building_table = $this->building_model->get_by_opt(array('land_id' => $land_model['land_id']));

        if ($building_table) {
            foreach ($building_table as $key => $data) {
                $building_full_detail = $this->loan_lib->building_full_detail($data);
                $building_table[$key]['building_type_name'] = $building_full_detail['building_type_name'];
                $building_table[$key]['address'] = $building_full_detail['address'];
            }
        }

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'ดูข้อมูลการขอสินเขื่อ';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}"),
            'การขอสินเชื่อ' => site_url("loan/form_land")
        );

        //$form_data
        $form_data = array_merge($loan_type_model, $land_model, $this->loan_lib->building_fields(), $loan_objective);
        $form_data['loan_id'] = $loan_id;
        $form_data['form_page'] = $form_page;
        $form_data['land_id'] = $land_model ? $land_model['land_id'] : '';

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'redeem_owner' => $redeem_owner,
            'building_table' => $building_table,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'content' => $this->parser->parse('form_land_1_view', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
        /*
          exit;



          $form_data = array_merge($loan_type_model, $land_model, $this->loan_lib->building_fields());
          $form_data['loan_id'] = $loan_id;
          $form_data['form_page'] = $form_page;
          $form_data['land_id'] = $land_model ? $land_model['land_id'] : '';

          //$redeem_owner
          $redeem_owner = $this->person_model->get_by_id($loan_type_model['loan_type_redeem_owner']);

          //building
          $building_table = $this->building_model->get_by_opt(array('land_id' => $land_model['land_id']));

          if ($building_table) {
          foreach ($building_table as $key => $data) {
          $building_full_detail = $this->loan_lib->building_full_detail($data);
          $building_table[$key]['building_type_name'] = $building_full_detail['building_type_name'];
          $building_table[$key]['address'] = $building_full_detail['address'];
          }
          }


          //loan_data
          $loan_data = $this->loan_model->get_by_id($loan_id);

          //loan_type
          $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

          //mst
          $mst = array(
          //config & master for person
          'config_title_list' => $this->mst_model->get_all('title'),
          'config_race_list' => $this->mst_model->get_all('race'),
          'config_nationality_list' => $this->mst_model->get_all('nationality'),
          'config_religion_list' => $this->mst_model->get_all('religion'),
          'config_career_list' => $this->mst_model->get_all('career'),
          'master_sex_list' => $this->mst_model->get_all('sex'),
          'master_district_list' => array(),
          'master_amphur_list' => array(),
          'master_province_list' => $this->mst_model->get_table_location_all('province'),
          'master_status_married_list' => $this->mst_model->get_all('status_married'),
          //config & master for land
          'config_relationship_list' => $this->mst_model->get_all('relationship'),
          'config_land_type' => $this->mst_model->get_all('land_type'),
          'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
          'config_land_type_list' => $this->mst_model->get_all('land_type'),
          //config & master for building
          'config_building_type_list' => $this->mst_model->get_all('building_type'),
          );

          //redeem_type
          $redeem_type_data = $this->loan_type_model->get_redeem_type();

          //session_form
          if (isset($_SESSION['session_form'])) {
          $session_form = $_SESSION['session_form'];
          $this->session->unset_userdata('session_form');
          } else {
          $session_form = array();
          }

          //form_name
          $form_name = $this->form_name;

          //data_menu & data_breadcrumb
          $data_menu['menu'] = $this->menu;

          //data_breadcrumb
          $data_breadcrumb['menu'] = array(
          $this->menu_name => site_url('loan'),
          $this->title => site_url("loan/edit_loan/{$loan_id}")
          );

          //data_content
          $data_content = array(
          'form_name' => $form_name,
          'loan_code' => $loan_model['loan_code'],
          'loan_data' => $loan_data,
          'table_data' => $table_data,
          'form_data' => $form_data,
          'form_page' => $form_page,
          'session_form' => $session_form,
          'mst' => $mst,
          'redeem_owner' => $redeem_owner,
          'redeem_type_data' => $redeem_type_data,
          'building_table' => $building_table,
          );

          $data = array(
          'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
          'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
          'title' => $form_name,
          'css_other' => array('modules_loan.css'),
          'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
          'content' => $this->parser->parse('form_land', $data_content, TRUE)
          );

          $this->parser->parse('main', $data);
         */
    }

    public function save_land1() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            //redeem_owner
            //$redeem_owner_id = $this->loan_lib->person_land_save($post['redeem_owner'], $post['redeem_owner']['person_id']);
            $redeem_owner_id = $loan_model['person_id'];

            //loan_type
            $loan_type = array(
                'loan_id' => $loan_id,
                'loan_objective_id' => $post['loan_objective_id'],
                'loan_type_redeem_type' => $post['loan_type_redeem_type'],
                'loan_type_redeem_case' => $post['loan_type_redeem_case'],
                'loan_type_redeem_owner' => $redeem_owner_id,
                'loan_type_redeem_relationship' => $post['loan_type_redeem_relationship'],
                'loan_type_redeem_amount' => money_str_to_num($post['loan_type_redeem_amount']),
            );

            $loan_type_model = $this->loan_type_model->save($loan_type, $post['loan_type_id']);
            $loan_type_id = $loan_type_model['id'];

            if (!$post['loan_type_id']) {
                //land
                $land = array(
                    'loan_type_id' => $loan_type_id,
                    'land_type_id' => $post['land_type_id'],
                    'land_no' => $post['land_no'],
                    'land_addr_no' => $post['land_addr_no'],
                    'land_addr_moo' => $post['land_addr_moo'],
                    'land_addr_district_id' => $post['land_addr_district_id'],
                    'land_addr_amphur_id' => $post['land_addr_amphur_id'],
                    'land_addr_province_id' => $post['land_addr_province_id'],
                    'land_area_rai' => $post['land_area_rai'],
                    'land_area_ngan' => $post['land_area_ngan'],
                    'land_area_wah' => $post['land_area_wah'],
                    'land_buiding_detail_id' => isset($post['land_buiding_detail_id']) ? $post['land_buiding_detail_id'] : '',
                	'land_estimate_price' => $post['land_estimate_price']
                );
                $land_model = $this->land_model->save($land, $post['land_id']);
                $land_id = $land_model['id'];

                //land_buiding
                if ($post['land_buiding_detail_id'] == 1) {
                    if (!$post['loan_type_id']) {
                        $building = array(
                            'land_id' => $land_id,
                            'building_owner_title_id' => $post['building_owner_title_id'],
                            'building_owner_fname' => $post['building_owner_fname'],
                            'building_owner_lname' => $post['building_owner_lname'],
                            'building_no' => $post['building_no'],
                            'building_moo' => $post['building_moo'],
                            'building_name' => $post['building_name'],
                            'building_soi' => $post['building_soi'],
                            'building_road' => $post['building_road'],
                            'building_district_id' => $post['building_district_id'],
                            'building_amphur_id' => $post['building_amphur_id'],
                            'building_province_id' => $post['building_province_id'],
                            'building_type_id' => $post['building_type_id'],
                        	'building_estimate_price' => $post['building_estimate_price']
                        );

                        $building_model = $this->building_model->save($building);
                        $building_id = $building_model['id'];
                    }
                }

                //loan_guarantee_land
                $this->load->model('loan_guarantee_land_model');
                $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_row_by_opt(array('land_id' => $land_id));

                if (!$loan_guarantee_land_model) {
                    $post_loan_guarantee_land_model = array(
                        'loan_id' => $loan_id,
                        'land_id' => $land_id
                    );
                    $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model);
                    $loan_guarantee_land_id = $loan_guarantee_land_model['id'];
                }

                //land_owner
                $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                if (!$land_owner_model) {
                    $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                    $land_owner_id = $land_owner_model['id'];
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_land/land_1/?edit={$loan_type_id}");
    }

    public function land_2() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //sum_loan_amount
        $this->loan_lib->sum_loan_amount($loan_model['person_id'], 2);

        //page_form
        $form_page = $this->uri->segment(3);

        //$form_data
        $form_data = array_merge($this->loan_lib->land_fields(), $this->loan_lib->building_fields());
        $form_data['loan_id'] = $loan_id;
        $form_data['form_page'] = $form_page;
        $form_data['loan_objective_id'] = 2;
        $form_data['person_id'] = $loan_model['person_id'];
        $form_data['loan_type_contract_amount'] = money_num_to_str(0);
        $form_data['buiding_owner_status'] = 0; //ตนเอง
        //person_data
        $person_id = $loan_model['person_id'];
        if ($person_id) {
            $person_data = $this->loan_lib->person_detail($person_id);
        } else {
            $person_data = $this->loan_lib->person_fields();
        }

        //creditor
        $creditor = $this->loan_lib->person_fields();

        //borrowers
        $borrowers = $this->loan_lib->person_fields();

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';

        //get
        $get = $this->input->get();
        if ($get) {
            if (isset($get['edit'])) {
                $loan_type_model = $this->loan_type_model->get_by_id($get['edit']);
                $land_model = $this->land_model->get_row_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                $loan_type_model['loan_type_contract_date'] = den_to_dth($loan_type_model['loan_type_contract_date']);
                $loan_type_model['loan_type_contract_amount'] = money_num_to_str($loan_type_model['loan_type_contract_amount']);

                $form_data = array_merge($loan_type_model, $land_model, $this->loan_lib->building_fields());
                $form_data['loan_id'] = $loan_id;
                $form_data['form_page'] = $form_page;
                $form_data['person_id'] = $loan_model['person_id'];

                //$redeem_owner
                $creditor = $this->person_model->get_by_id($loan_type_model['loan_type_contract_creditor']);

                //borrowers
                $borrowers = $this->person_model->get_by_id($loan_type_model['loan_type_contract_borrowers']);

                //table_land_building
                $land_model = $this->land_model->get_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                if ($land_model) {
                    $table_land_building = array();
                    foreach ($land_model as $key_land => $land) {
                        $table_land_building[$key_land] = $land;
                        $land_full_detail = $this->loan_lib->land_full_detail($land);
                        $table_land_building[$key_land]['address'] = $land_full_detail['address'];
                        $table_land_building[$key_land]['owner_id'] = $loan_model['person_id'];
                        $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));
                        if ($building_model) {
                            foreach ($building_model as $key_building => $building) {
                                $building_full_detail = $this->loan_lib->building_full_detail($building);
                                $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                                $building_model[$key_building]['address'] = $building_full_detail['address'];
                            }

                            $table_land_building[$key_land]['building_table'] = $building_model;
                        }
                    }
                }

                //$table_land_building_param
                $table_land_building_data = array(
                    'callbackurl' => base64_encode("loan/form_land/land_2/?edit={$get['edit']}"),
                    'owner_id' => $loan_model['person_id'],
                    'save_loan_guarantee_land' => 1,
                    'loan_type_id' => $loan_type_model['loan_type_id'],
                );
                $table_land_building_param = json_encode($table_land_building_data);
                $table_land_building_param = base64_encode($table_land_building_param);
            }
        }else{
          $creditor['person_id']=null;
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //loan_type
        $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

        //mst
        $mst = array(
            //config & master for person
            'config_title_list' => $this->mst_model->get_all('title'),
            'config_race_list' => $this->mst_model->get_all('race'),
            'config_nationality_list' => $this->mst_model->get_all('nationality'),
            'config_religion_list' => $this->mst_model->get_all('religion'),
            'config_career_list' => $this->mst_model->get_all('career'),
            'master_sex_list' => $this->mst_model->get_all('sex'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            'master_status_married_list' => $this->mst_model->get_all('status_married'),
            //config & master for land
            'config_relationship_list' => $this->mst_model->get_all('relationship'),
            'config_land_type' => $this->mst_model->get_all('land_type'),
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
            //config & master for building
            'config_building_type_list' => $this->mst_model->get_all('building_type'),
            'land_buiding_detail_id_list' => $this->land_model->get_land_buiding_detail_id_list(),
            'buiding_owner_status_list' => $this->land_model->get_buiding_owner_status_list(),
        );

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'person_data' => $person_data,
            'table_data' => $table_data,
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'mst' => $mst,
            'creditor' => $creditor,
            'borrowers' => $borrowers,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_land2() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            //insert creditor
            $person_saved = $this->person_model->save($post['creditor'], !empty($post['creditor']['person_id'])?$post['creditor']['person_id']:null);
            $creditor_id = $person_saved['id'];
          //  $creditor_id = $this->loan_lib->person_land2_save($post['creditor'], $post['creditor']['person_id']);

            //insert borrowers
            if (isset($post['same_loan_owner']) && $post['same_loan_owner'] == 1) {
//                $person_model = $this->person_model->get_by_id($loan_model['person_id']);
//                $post['borrowers']['title_id'] = $person_model['title_id'];
//                $post['borrowers']['person_fname'] = $person_model['person_fname'];
//                $post['borrowers']['person_lname'] = $person_model['person_lname'];

                $borrowers_id = $loan_model['person_id'];
            } else {
                $borrowers_id = $this->loan_lib->person_land2_save($post['borrowers'], $post['borrowers']['person_id']);
            }


            //insert loan_type
            $loan_type = array(
                'loan_id' => $loan_id,
                'loan_objective_id' => $post['loan_objective_id'],
                'loan_type_contract_case' => $post['loan_type_contract_case'],
                'loan_type_contract_no' => $post['loan_type_contract_no'],
                'loan_type_contract_date' => dth_to_den($post['loan_type_contract_date']),
                'loan_type_contract_creditor' => $creditor_id,
                'loan_type_contract_borrowers' => $borrowers_id,
                'loan_type_contract_amount' => money_str_to_num($post['loan_type_contract_amount']),
            );

            $loan_type_model = $this->loan_type_model->save($loan_type, $post['loan_type_id']);
            $loan_type_id = $loan_type_model['id'];

            if (!$post['loan_type_id']) {
                //land
                $land = array(
                    'loan_type_id' => $loan_type_id,
                    'land_type_id' => $post['land_type_id'],
                    'land_no' => $post['land_no'],
                    'land_addr_no' => $post['land_addr_no'],
                    'land_addr_moo' => $post['land_addr_moo'],
                    'land_addr_district_id' => $post['land_addr_district_id'],
                    'land_addr_amphur_id' => $post['land_addr_amphur_id'],
                    'land_addr_province_id' => $post['land_addr_province_id'],
                    'land_area_rai' => $post['land_area_rai'],
                    'land_area_ngan' => $post['land_area_ngan'],
                    'land_area_wah' => $post['land_area_wah'],
                    'land_buiding_detail_id' => isset($post['land_buiding_detail_id']) ? $post['land_buiding_detail_id'] : '',
                	'land_estimate_price' => $post['land_estimate_price']
                );
                $land_model = $this->land_model->save($land, $post['land_id']);
                $land_id = $land_model['id'];

                //land_buiding
                if ($post['land_buiding_detail_id'] == 1) {
                    if (!$post['loan_type_id']) {
                        $building = array(
                            'land_id' => $land_id,
                            'building_owner_title_id' => $post['building_owner_title_id'],
                            'building_owner_fname' => $post['building_owner_fname'],
                            'building_owner_lname' => $post['building_owner_lname'],
                            'building_no' => $post['building_no'],
                            'building_moo' => $post['building_moo'],
                            'building_name' => $post['building_name'],
                            'building_soi' => $post['building_soi'],
                            'building_road' => $post['building_road'],
                            'building_district_id' => $post['building_district_id'],
                            'building_amphur_id' => $post['building_amphur_id'],
                            'building_province_id' => $post['building_province_id'],
                            'building_type_id' => $post['building_type_id'],
                        	'building_estimate_price' => $post['building_estimate_price']
                        );

                        $building_model = $this->building_model->save($building);
                        $building_id = $building_model['id'];
                    }
                }

                //loan_guarantee_land
                $this->load->model('loan_guarantee_land_model');
                $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_row_by_opt(array('land_id' => $land_id));

                if (!$loan_guarantee_land_model) {
                    $post_loan_guarantee_land_model = array(
                        'loan_id' => $loan_id,
                        'land_id' => $land_id
                    );
                    $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model);
                    $loan_guarantee_land_id = $loan_guarantee_land_model['id'];
                }

                //land_owner
                $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                if (!$land_owner_model) {
                    $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                    $land_owner_id = $land_owner_model['id'];
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }
//echo "person_id : {$loan_model['person_id']}<br>";
//echo "loan_type_id : {$loan_type_id}<br>";
//echo "land_id : {$land_id}<br>";
//if(isset($building_id)) echo "building_id : {$building_id}<br>";
//if(isset($loan_guarantee_land_id)) echo "loan_guarantee_land_id : {$loan_guarantee_land_id}<br>";
//if(isset($land_owner_id)) echo "land_owner_id : {$land_owner_id}<br>";
//exit;

        redirect("loan/form_land/land_2/?edit={$loan_type_id}");
    }

    public function land_3() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //sum_loan_amount
        $this->loan_lib->sum_loan_amount($loan_model['person_id'], 3);

        //page_form
        $form_page = $this->uri->segment(3);

        //$form_data
        $form_data = array_merge($this->loan_lib->land_fields(), $this->loan_lib->building_fields());
        $form_data['loan_id'] = $loan_id;
        $form_data['form_page'] = $form_page;
        $form_data['loan_objective_id'] = 3;
        $form_data['person_id'] = $loan_model['person_id'];
        $form_data['loan_type_case_amount'] = money_num_to_str(0);
        $form_data['buiding_owner_status'] = 0; //ตนเอง
        //person_data
        $person_id = $loan_model['person_id'];
        if ($person_id) {
            $person_data = $this->loan_lib->person_detail($person_id);
        } else {
            $person_data = $this->loan_lib->person_fields();
        }

        //plaintiff
        $plaintiff = $this->loan_lib->person_fields();

        //defendant
        $defendant = $this->loan_lib->person_fields();

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';

        //get
        $get = $this->input->get();
        if ($get) {
            if (isset($get['edit'])) {
                $loan_type_model = $this->loan_type_model->get_by_id($get['edit']);
                $land_model = $this->land_model->get_row_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                $loan_type_model['loan_type_case_date'] = den_to_dth($loan_type_model['loan_type_case_date']);
                $loan_type_model['loan_type_case_amount'] = money_num_to_str($loan_type_model['loan_type_case_amount']);

                $form_data = array_merge($loan_type_model, $land_model, $this->loan_lib->building_fields());
                $form_data['loan_id'] = $loan_id;
                $form_data['form_page'] = $form_page;
                $form_data['person_id'] = $loan_model['person_id'];

                //$plaintiff
                $plaintiff = $this->person_model->get_by_id($loan_type_model['loan_type_case_plaintiff_id']);
                       // echo '<pre>plaintiff';
                       // print_r($plaintiff);
                       // echo '</pre>';
                //$defendant
                $defendant = $this->person_model->get_by_id($loan_type_model['loan_type_case_defendant_id']);

                //table_land_building
                $land_model = $this->land_model->get_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                if ($land_model) {
                    $table_land_building = array();
                    foreach ($land_model as $key_land => $land) {
                        $table_land_building[$key_land] = $land;
                        $land_full_detail = $this->loan_lib->land_full_detail($land);
                        $table_land_building[$key_land]['address'] = $land_full_detail['address'];
                        $table_land_building[$key_land]['owner_id'] = $loan_model['person_id'];
                        $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));
                        if ($building_model) {
                            foreach ($building_model as $key_building => $building) {
                                $building_full_detail = $this->loan_lib->building_full_detail($building);
                                $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                                $building_model[$key_building]['address'] = $building_full_detail['address'];
                            }

                            $table_land_building[$key_land]['building_table'] = $building_model;
                        }
                    }
                }

                //$table_land_building_param
                $table_land_building_data = array(
                    'callbackurl' => base64_encode("loan/form_land/land_3/?edit={$get['edit']}"),
                    'owner_id' => $loan_model['person_id'],
                    'save_loan_guarantee_land' => 1,
                    'loan_type_id' => $loan_type_model['loan_type_id'],
                );
                $table_land_building_param = json_encode($table_land_building_data);
                $table_land_building_param = base64_encode($table_land_building_param);
            }
        }else{
          $plaintiff['person_id'] = null;
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //loan_type
        $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

        //mst
        $mst = array(
            'config_title_list' => $this->mst_model->get_all('title'),
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            //config & master for land
            'config_relationship_list' => $this->mst_model->get_all('relationship'),
            'config_land_type' => $this->mst_model->get_all('land_type'),
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
            'config_building_type_list' => $this->mst_model->get_all('building_type'),
            'land_buiding_detail_id_list' => $this->land_model->get_land_buiding_detail_id_list(),
            'buiding_owner_status_list' => $this->land_model->get_buiding_owner_status_list(),
        );

        //case_type
        $case_type_data = $this->loan_type_model->get_case_type();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'person_data' => $person_data,
            'table_data' => $table_data,
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'mst' => $mst,
            'case_type_data' => $case_type_data,
            'plaintiff' => $plaintiff,
            'defendant' => $defendant,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_land3() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
//echo '<pre>';
//print_r($post);
//echo '</pre>';
//exit;
        if ($post) {
            //insert plaintiff
            //TODO save
            $person_saved = $this->person_model->save($post['plaintiff'], !empty($post['plaintiff']['person_id'])?$post['plaintiff']['person_id']:null);
            $plaintiff_id = $person_saved['id'];

            //$plaintiff_id = $this->loan_lib->person_land2_save($post['plaintiff'], $post['plaintiff']['person_id']);

            //insert defendant
            if (isset($post['same_loan_owner']) && $post['same_loan_owner'] == 1) {
//                $person_model = $this->person_model->get_by_id($loan_model['person_id']);
//                $post['defendant']['title_id'] = $person_model['title_id'];
//                $post['defendant']['person_fname'] = $person_model['person_fname'];
//                $post['defendant']['person_lname'] = $person_model['person_lname'];

                $defendant_id = $loan_model['person_id'];
            } else {
                $defendant_id = $this->loan_lib->person_land2_save($post['defendant'], $post['defendant']['person_id']);
            }


            //insert loan_type
            $loan_type = array(
                'loan_id' => $loan_id,
                'loan_objective_id' => $post['loan_objective_id'],
                'loan_type_case_black_no' => $post['loan_type_case_black_no'],
                'loan_type_case_red_no' => $post['loan_type_case_red_no'],
                'loan_type_case_court' => $post['loan_type_case_court'],
                'loan_type_case_date' => dth_to_den($post['loan_type_case_date']),
                'loan_type_case_type' => strval($post['loan_type_case_type']),
                'loan_type_case_plaintiff_id' => $plaintiff_id,
                'loan_type_case_defendant_id' => $defendant_id,
                'loan_type_case_case' => $post['loan_type_case_case'],
                'loan_type_case_comment' => $post['loan_type_case_comment'],
                'loan_type_case_amount' => money_str_to_num($post['loan_type_case_amount']),
            );
            $loan_type_model = $this->loan_type_model->save($loan_type, $post['loan_type_id']);
            $loan_type_id = $loan_type_model['id'];

            if (!$post['loan_type_id']) {
                //land
                $land = array(
                    'loan_type_id' => $loan_type_id,
                    'land_type_id' => $post['land_type_id'],
                    'land_no' => $post['land_no'],
                    'land_addr_no' => $post['land_addr_no'],
                    'land_addr_moo' => $post['land_addr_moo'],
                    'land_addr_district_id' => $post['land_addr_district_id'],
                    'land_addr_amphur_id' => $post['land_addr_amphur_id'],
                    'land_addr_province_id' => $post['land_addr_province_id'],
                    'land_area_rai' => $post['land_area_rai'],
                    'land_area_ngan' => $post['land_area_ngan'],
                    'land_area_wah' => $post['land_area_wah'],
                    'land_buiding_detail_id' => isset($post['land_buiding_detail_id']) ? $post['land_buiding_detail_id'] : '',
                	'land_estimate_price' => $post['land_estimate_price']
                );
                $land_model = $this->land_model->save($land, $post['land_id']);
                $land_id = $land_model['id'];

                //land_buiding
                if ($post['land_buiding_detail_id'] == 1) {
                    if (!$post['loan_type_id']) {
                        $building = array(
                            'land_id' => $land_id,
                            'building_owner_title_id' => $post['building_owner_title_id'],
                            'building_owner_fname' => $post['building_owner_fname'],
                            'building_owner_lname' => $post['building_owner_lname'],
                            'building_no' => $post['building_no'],
                            'building_moo' => $post['building_moo'],
                            'building_name' => $post['building_name'],
                            'building_soi' => $post['building_soi'],
                            'building_road' => $post['building_road'],
                            'building_district_id' => $post['building_district_id'],
                            'building_amphur_id' => $post['building_amphur_id'],
                            'building_province_id' => $post['building_province_id'],
                            'building_type_id' => $post['building_type_id'],
                        	'building_estimate_price' => $post['building_estimate_price']
                        );

                        $building_model = $this->building_model->save($building);
                        $building_id = $building_model['id'];
                    }
                }

                //loan_guarantee_land
                $this->load->model('loan_guarantee_land_model');
                $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_row_by_opt(array('land_id' => $land_id));

                if (!$loan_guarantee_land_model) {
                    $post_loan_guarantee_land_model = array(
                        'loan_id' => $loan_id,
                        'land_id' => $land_id
                    );
                    $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model);
                    $loan_guarantee_land_id = $loan_guarantee_land_model['id'];
                }

                //land_owner
                $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                if (!$land_owner_model) {
                    $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                    $land_owner_id = $land_owner_model['id'];
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

//echo "person_id : {$loan_model['person_id']}<br>";
//echo "loan_type_id : {$loan_type_id}<br>";
//echo "land_id : {$land_id}<br>";
//if(isset($building_id)) echo "building_id : {$building_id}<br>";
//if(isset($loan_guarantee_land_id)) echo "loan_guarantee_land_id : {$loan_guarantee_land_id}<br>";
//if(isset($land_owner_id)) echo "land_owner_id : {$land_owner_id}<br>";
//exit;

        redirect("loan/form_land/land_3/?edit={$loan_type_id}");
    }

    public function land_4() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //sum_loan_amount
        $this->loan_lib->sum_loan_amount($loan_model['person_id'], 4);

        //page_form
        $form_page = $this->uri->segment(3);

        //$form_data
        $form_data = array_merge($this->loan_lib->land_fields(), $this->loan_lib->building_fields());
        $form_data['loan_id'] = $loan_id;
        $form_data['form_page'] = $form_page;
        $form_data['loan_objective_id'] = 4;
        $form_data['person_id'] = $loan_model['person_id'];
        $form_data['loan_type_sale_amount'] = money_num_to_str(0);
        $form_data['loan_type_sale_relationship'] = 6; //ตนเอง
        $form_data['buiding_owner_status'] = 0; //ตนเอง
        $form_data['loan_type_sale_type'] = 1; //ตนเอง
        //person_data
        $person_id = $loan_model['person_id'];
        if ($person_id) {
            $person_data = $this->loan_lib->person_detail($person_id);
        } else {
            $person_data = $this->loan_lib->person_fields();
        }

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';

        //get
        $get = $this->input->get();
        if ($get) {
            if (isset($get['edit'])) {
                $loan_type_model = $this->loan_type_model->get_by_id($get['edit']);
                $land_model = $this->land_model->get_row_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                $loan_type_model['loan_type_sale_date'] = den_to_dth($loan_type_model['loan_type_sale_date']);
                $loan_type_model['loan_type_sale_amount'] = money_num_to_str($loan_type_model['loan_type_sale_amount']);

                $form_data = array_merge($loan_type_model, $land_model, $this->loan_lib->building_fields());
                $form_data['loan_id'] = $loan_id;
                $form_data['form_page'] = $form_page;
                $form_data['person_id'] = $loan_model['person_id'];

                //table_land_building
                $land_model = $this->land_model->get_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                if ($land_model) {
                    $table_land_building = array();
                    foreach ($land_model as $key_land => $land) {
                        $table_land_building[$key_land] = $land;
                        $land_full_detail = $this->loan_lib->land_full_detail($land);
                        $table_land_building[$key_land]['address'] = $land_full_detail['address'];
                        $table_land_building[$key_land]['owner_id'] = $loan_model['person_id'];

                        $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));
                        if ($building_model) {
                            foreach ($building_model as $key_building => $building) {
                                $building_full_detail = $this->loan_lib->building_full_detail($building);
                                $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                                $building_model[$key_building]['address'] = $building_full_detail['address'];
                            }

                            $table_land_building[$key_land]['building_table'] = $building_model;
                        }
                    }
                }

                //$table_land_building_param
                $table_land_building_data = array(
                    'callbackurl' => base64_encode("loan/form_land/land_4/?edit={$get['edit']}"),
                    'owner_id' => $loan_model['person_id'],
                    'save_loan_guarantee_land' => 1,
                    'loan_type_id' => $loan_type_model['loan_type_id'],
                );
                $table_land_building_param = json_encode($table_land_building_data);
                $table_land_building_param = base64_encode($table_land_building_param);
            }
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //loan_type
        $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

        //mst
        $mst = array(
            'config_title_list' => $this->mst_model->get_all('title'),
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            //config & master for land
            'config_relationship_list' => $this->mst_model->get_all('relationship'),
            'config_land_type' => $this->mst_model->get_all('land_type'),
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
            'config_building_type_list' => $this->mst_model->get_all('building_type'),
            'land_buiding_detail_id_list' => $this->land_model->get_land_buiding_detail_id_list(),
            'buiding_owner_status_list' => $this->land_model->get_buiding_owner_status_list(),
        );

        //sale_type
        $sale_type_data = $this->loan_type_model->get_sale_type();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'person_data' => $person_data,
            'table_data' => $table_data,
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'mst' => $mst,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
            'sale_type_data' => $sale_type_data,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_land4() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
//echo '<pre>';
//print_r($post);
//echo '</pre>';
//exit;
        if ($post) {
            //insert loan_type
            $loan_type = array(
                'loan_id' => $loan_id,
                'loan_objective_id' => $post['loan_objective_id'],
                'loan_type_sale_type' => $post['loan_type_sale_type'],
                'loan_type_sale_relationship' => $post['loan_type_sale_relationship'],
                'loan_type_sale_date' => dth_to_den($post['loan_type_sale_date']),
                'loan_type_sale_amount' => money_str_to_num($post['loan_type_sale_amount']),
            );
            $loan_type_model = $this->loan_type_model->save($loan_type, $post['loan_type_id']);
            $loan_type_id = $loan_type_model['id'];

            if (!$post['loan_type_id']) {
                //land
                $land = array(
                    'loan_type_id' => $loan_type_id,
                    'land_type_id' => $post['land_type_id'],
                    'land_no' => $post['land_no'],
                    'land_addr_no' => $post['land_addr_no'],
                    'land_addr_moo' => $post['land_addr_moo'],
                    'land_addr_district_id' => $post['land_addr_district_id'],
                    'land_addr_amphur_id' => $post['land_addr_amphur_id'],
                    'land_addr_province_id' => $post['land_addr_province_id'],
                    'land_area_rai' => $post['land_area_rai'],
                    'land_area_ngan' => $post['land_area_ngan'],
                    'land_area_wah' => $post['land_area_wah'],
                    'land_buiding_detail_id' => isset($post['land_buiding_detail_id']) ? $post['land_buiding_detail_id'] : '',
                	'land_estimate_price' => $post['land_estimate_price']
                );
                $land_model = $this->land_model->save($land, $post['land_id']);
                $land_id = $land_model['id'];

                //land_buiding
                if ($post['land_buiding_detail_id'] == 1) {
                    if (!$post['loan_type_id']) {
                        $building = array(
                            'land_id' => $land_id,
                            'building_owner_title_id' => $post['building_owner_title_id'],
                            'building_owner_fname' => $post['building_owner_fname'],
                            'building_owner_lname' => $post['building_owner_lname'],
                            'building_no' => $post['building_no'],
                            'building_moo' => $post['building_moo'],
                            'building_name' => $post['building_name'],
                            'building_soi' => $post['building_soi'],
                            'building_road' => $post['building_road'],
                            'building_district_id' => $post['building_district_id'],
                            'building_amphur_id' => $post['building_amphur_id'],
                            'building_province_id' => $post['building_province_id'],
                            'building_type_id' => $post['building_type_id'],
                        	'building_estimate_price' => $post['building_estimate_price']
                        );

                        $building_model = $this->building_model->save($building);
                        $building_id = $building_model['id'];
                    }
                }

                //loan_guarantee_land
                $this->load->model('loan_guarantee_land_model');
                $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_row_by_opt(array('land_id' => $land_id));

                if (!$loan_guarantee_land_model) {
                    $post_loan_guarantee_land_model = array(
                        'loan_id' => $loan_id,
                        'land_id' => $land_id
                    );
                    $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model);
                    $loan_guarantee_land_id = $loan_guarantee_land_model['id'];
                }

                //land_owner
                $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                if (!$land_owner_model) {
                    $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                    $land_owner_id = $land_owner_model['id'];
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

//echo "person_id : {$loan_model['person_id']}<br>";
//echo "loan_type_id : {$loan_type_id}<br>";
//echo "land_id : {$land_id}<br>";
//if(isset($building_id)) echo "building_id : {$building_id}<br>";
//if(isset($loan_guarantee_land_id)) echo "loan_guarantee_land_id : {$loan_guarantee_land_id}<br>";
//if(isset($land_owner_id)) echo "land_owner_id : {$land_owner_id}<br>";
//exit;

        redirect("loan/form_land/land_4/?edit={$loan_type_id}");
    }

    public function land_5() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //sum_loan_amount
        $this->loan_lib->sum_loan_amount($loan_model['person_id'], 5);

        //page_form
        $form_page = $this->uri->segment(3);

        //$form_data
        $form_data = array_merge($this->loan_lib->land_fields(), $this->loan_lib->building_fields());
        $form_data['loan_id'] = $loan_id;
        $form_data['form_page'] = $form_page;
        $form_data['loan_objective_id'] = 5;

        //get
        $get = $this->input->get();
        if ($get) {
            if (isset($get['edit'])) {
                $loan_type_model = $this->loan_type_model->get_by_id($get['edit']);
                $land_model = array('land_id' => '');

                $form_data = array_merge($loan_type_model, $land_model);
                $form_data['loan_id'] = $loan_id;
                $form_data['form_page'] = $form_page;
            }
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //loan_type
        $table_data = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id, 'loan_type_status' => $this->loan_type_model->_status_active));

        //$year_list
        $year = date('Y');
        for ($i = 0; $i < 5; $i++) {
            $year_list[$year] = $year;
            $year = $year + 1;
        }

        //mst
        $mst = array(
            'master_loan_objective' => $this->mst_model->get_all('loan_objective'),
            'year_list' => $year_list,
        );

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'table_data' => $table_data,
            'form_data' => $form_data,
            'form_page' => $form_page,
            'session_form' => $session_form,
            'mst' => $mst,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'content' => $this->parser->parse('form_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_land5() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            //insert loan_type
            $loan_type = array(
                'loan_id' => $loan_id,
                'loan_objective_id' => $post['loan_objective_id'],
                'loan_type_farm_for' => $post['loan_type_farm_for'],
                'loan_type_farm_location' => $post['loan_type_farm_location'],
                'loan_type_farm_project' => $post['loan_type_farm_project'],
                'loan_type_farm_expenditure' => $post['loan_type_farm_expenditure'],
                'loan_type_farm_income_yearstart' => $post['loan_type_farm_income_yearstart'],
                'loan_type_farm_income_per_year' => money_str_to_num($post['loan_type_farm_income_per_year']),
                'loan_type_farm_payment_yearstart' => $post['loan_type_farm_payment_yearstart'],
                'loan_type_farm_payment_amount' => money_str_to_num($post['loan_type_farm_payment_amount']),
                'loan_type_farm_year_amount' => $post['loan_type_farm_year_amount']
            );

            $loan_type_model = $this->loan_type_model->save($loan_type, $post['loan_type_id']);
            $loan_type_id = $loan_type_model['id'];

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_land/land_5/?edit={$loan_type_id}");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete
        if (isset($id)) {
            /*
            //delete land_model
            $land_model = $this->land_model->get_by_opt($opt = array('loan_type_id' => $id));
            if ($land_model) {
                foreach ($land_model as $land) {
                    //delete building
                    $this->building_model->delete_opt($opt = array('land_id' => $land['land_id']));
                }
            }
            $this->land_model->delete_opt($opt = array('loan_type_id' => $id));
            */

            //delete loan_type_model
            $this->loan_type_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_land');
    }

    public function save_building() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {
            //$building_owner
            $building_owner = $this->person_model->get_by_id($post['building_owner']);

            $building = array(
                'land_id' => $post['land_id'],
                'building_owner_title_id' => $building_owner['title_id'],
                'building_owner_fname' => $building_owner['person_fname'],
                'building_owner_lname' => $building_owner['person_lname'],
                'building_no' => $post['building_no'],
                'building_moo' => $post['building_moo'],
                'building_soi' => $post['building_soi'],
                'building_road' => $post['building_road'],
                'building_district_id' => $post['building_district_id'],
                'building_amphur_id' => $post['building_amphur_id'],
                'building_province_id' => $post['building_province_id'],
                'building_type_id' => $post['building_type_id'],
            	'building_estimate_price' => $post['building_estimate_price']
            );
            $this->building_model->save($building, $post['building_id']);

            //land
            $land = array(
                'land_buiding_detail_id' => 1,
            );

            $land_model = $this->land_model->save($land, $post['land_id']);


            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan/form_land/land_{$post['loan_objective_id']}/?edit={$post['loan_type_id']}");
        }

        redirect("loan/form_land");
    }

    public function delete_building() {
        $id = $this->uri->segment(4);

        //get
        $get = $this->input->get();

        //delete Loan_income by id
        if (isset($id)) {
            $this->building_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_land/land_{$get['loan_objective_id']}/?edit={$get['loan_type_id']}");
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

    public function check_has_formland() {
        //get
        $get = $this->input->get();

        if (isset($get['edit'])) {
            return TRUE;
        }

        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //page_form
        $form_page = $this->uri->segment(3);

        switch ($form_page) {
            case 'land_1': $loan_objective_id = 1;
                break;

            case 'land_2': $loan_objective_id = 2;
                break;

            case 'land_3': $loan_objective_id = 3;
                break;

            case 'land_4': $loan_objective_id = 4;
                break;

            case 'land_5': $loan_objective_id = 5;
                break;

            default : return TRUE;
                break;
        }

        $opt = array(
            'loan_id' => $loan_id,
            'loan_type_status' => $this->loan_type_model->_status_active
        );

        //$opt['not_loan_objective_id'] = $loan_objective_id;
        $loan_type_model = $this->loan_type_model->get_by_opt($opt);
        if ($loan_type_model) {
            if ($loan_objective_id == 5) {
                foreach ($loan_type_model as $data) {
                    if ($data['loan_objective_id'] == 5) {
                        //session_form
                        $session_form = array(
                            'status' => 'fail',
                            'message' => 'ไม่สามารถเพิ่มการขอสินเชื่อได้.'
                        );
                        $this->session->set_userdata('session_form', $session_form);
                        redirect('loan/form_land');
                    }
                }
            } else {
                foreach ($loan_type_model as $data) {
                    if ($data['loan_objective_id'] != 5) {
                        //session_form
                        $session_form = array(
                            'status' => 'fail',
                            'message' => 'ไม่สามารถเพิ่มการขอสินเชื่อได้.'
                        );
                        $this->session->set_userdata('session_form', $session_form);
                        redirect('loan/form_land');
                    }
                }
            }
        }

        return TRUE;
    }

}
