<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_loan_income extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'รายได้ในครัวเรือน';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_income_model');
        $this->load->model('loan_co_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];
        $person_id = $loan_model['person_id'];


        //form_data
        $form_data = array(
            'loan_activity_id' => '',
            'loan_activity_type' => '',
            'loan_activity_name' => '',
            'loan_activity_income1' => money_num_to_str(0),
            'loan_activity_income2' => money_num_to_str(0),
            'loan_activity_income3' => money_num_to_str(0),
            'loan_activity_expenditure1' => money_num_to_str(0),
            'loan_activity_expenditure2' => money_num_to_str(0),
            'loan_activity_expenditure3' => money_num_to_str(0),
            'loan_activity_self_rai' => 0,
            'loan_activity_self_ngan' => 0,
            'loan_activity_self_wah' => 0,
            'loan_activity_renting_rai' => 0,
            'loan_activity_renting_ngan' => 0,
            'loan_activity_renting_wah' => 0,
            'loan_activity_approve_rai' => 0,
            'loan_activity_approve_ngan' => 0,
            'loan_activity_approve_wah' => 0,
            'person_id' => $person_id,
        );

        //$get
        $get = $this->input->get();
        if ($get) {
            //edit Loan_income by id
            if (isset($get['edit'])) {
                $form_data = $this->loan_income_model->get_by_id($get['edit']);
                $form_data['loan_activity_income1'] = money_num_to_str($form_data['loan_activity_income1']);
                $form_data['loan_activity_income2'] = money_num_to_str($form_data['loan_activity_income2']);
                $form_data['loan_activity_income3'] = money_num_to_str($form_data['loan_activity_income3']);
                $form_data['loan_activity_expenditure1'] = money_num_to_str($form_data['loan_activity_expenditure1']);
                $form_data['loan_activity_expenditure2'] = money_num_to_str($form_data['loan_activity_expenditure2']);
                $form_data['loan_activity_expenditure3'] = money_num_to_str($form_data['loan_activity_expenditure3']);
                $form_data['person_id'] = $form_data['person_id'];
            }
        }

        //loan_income
        $loan_income_data = $this->loan_income_model->get_by_opt(array('person_id' => $person_id));
        
        //loan_income from loan_co
        $loan_co_model = $this->loan_co_model->get_by_opt(array('loan_id' => $loan_id));
        $loan_income_co_data = array();
        if($loan_co_model){
            foreach($loan_co_model as $data){
                $loan_income_co_model = $this->loan_income_model->get_by_opt(array('person_id' => $data['person_id']));
                if($loan_income_co_model){
                    foreach($loan_income_co_model as $data_income_co){
                        $loan_income_co_data[] = $data_income_co;
                    }
                }               
            }
        }
     
        //$loan_activity_type_data
        $loan_activity_type_data = $this->loan_income_model->get_loan_activity_type();

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'loan_income_data' => $loan_income_data,
            'loan_income_co_data' => $loan_income_co_data,
            'loan_activity_type_data' => $loan_activity_type_data,
            'form_data' => $form_data,
            'session_form' => $session_form
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_income.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_loan_income', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {
            $post['loan_activity_income1'] = money_str_to_num($post['loan_activity_income1']);
            $post['loan_activity_income2'] = money_str_to_num($post['loan_activity_income2']);
            $post['loan_activity_income3'] = money_str_to_num($post['loan_activity_income3']);
            $post['loan_activity_expenditure1'] = money_str_to_num($post['loan_activity_expenditure1']);
            $post['loan_activity_expenditure2'] = money_str_to_num($post['loan_activity_expenditure2']);
            $post['loan_activity_expenditure3'] = money_str_to_num($post['loan_activity_expenditure3']);
            
            $post['loan_activity_self_rai'] = $post['loan_activity_self_rai']?money_str_to_num($post['loan_activity_self_rai']):0;
            $post['loan_activity_self_ngan'] = $post['loan_activity_self_ngan']?money_str_to_num($post['loan_activity_self_ngan']):0;
            $post['loan_activity_self_wah'] = $post['loan_activity_self_wah']?money_str_to_num($post['loan_activity_self_wah']):0;
            $post['loan_activity_renting_rai'] = $post['loan_activity_renting_rai']?money_str_to_num($post['loan_activity_renting_rai']):0;
            $post['loan_activity_renting_ngan'] = $post['loan_activity_renting_ngan']?money_str_to_num($post['loan_activity_renting_ngan']):0;
            $post['loan_activity_renting_wah'] = $post['loan_activity_renting_wah']?money_str_to_num($post['loan_activity_renting_wah']):0;
            $post['loan_activity_approve_rai'] = $post['loan_activity_approve_rai']?money_str_to_num($post['loan_activity_approve_rai']):0;
            $post['loan_activity_approve_ngan'] = $post['loan_activity_approve_ngan']?money_str_to_num($post['loan_activity_approve_ngan']):0;
            $post['loan_activity_approve_wah'] = $post['loan_activity_approve_wah']?money_str_to_num($post['loan_activity_approve_wah']):0;

            if ($post['loan_activity_id']) {
                //update loan_income
                $id = $post['loan_activity_id'];
                unset($post['loan_activity_id']);
                unset($post['person_id']);

                $loan_income_model = $this->loan_income_model->save($post, $id);
                $loan_activity_id = $loan_income_model['id'];
            } else {
                unset($post['loan_activity_id']);

                //insert loan_income
                $loan_income_model = $this->loan_income_model->save($post);
                $loan_activity_id = $loan_income_model['id'];
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_loan_income");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete Loan_income by id
        if (isset($id)) {
            $this->loan_income_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_loan_income');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
