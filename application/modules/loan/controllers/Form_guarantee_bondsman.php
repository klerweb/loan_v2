<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_guarantee_bondsman extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $page_name = 'หลักประกันการขอใช้สินเชื่อ';
    private $form_name = 'บุคคลค้ำประกัน';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('person_model');
        $this->load->model('loan_guarantee_bondsman_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //config & master
        $mst = array(
            'config_title_list' => $this->mst_model->get_all('title'),
            'config_race_list' => $this->mst_model->get_all('race'),
            'config_nationality_list' => $this->mst_model->get_all('nationality'),
            'config_religion_list' => $this->mst_model->get_all('religion'),
            'config_career_list' => $this->mst_model->get_all('career'),
            'config_relationship_list' => $this->mst_model->get_all('relationship'),
            'config_bondsman_type_list' => $this->mst_model->get_all('bondsman_type'),
            'master_sex_list' => $this->mst_model->get_all('sex'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
        );

        //person_data
        $person_data = $this->loan_lib->person_fields();

        //$form_data
        $form_data['bondsman_type'] = '';
        $form_data['loan_bondsman_id'] = '';

        //$get
        $get = $this->input->get();
        if ($get) {
            //edit Loan_income by id
            if (isset($get['edit'])) {
                $form_data = $this->loan_guarantee_bondsman_model->get_by_id($get['edit']);

                $person_data = $this->person_model->get_by_id($form_data['person_id']);
                $person_data['person_birthdate'] = den_to_dth($person_data['person_birthdate']);
                $person_data['person_card_startdate'] = den_to_dth($person_data['person_card_startdate']);
                $person_data['person_card_expiredate'] = den_to_dth($person_data['person_card_expiredate']);
                $person_data['person_cardid_emp_startdate'] = den_to_dth($person_data['person_cardid_emp_startdate']);
                $person_data['person_cardid_emp_expiredate'] = den_to_dth($person_data['person_cardid_emp_expiredate']);
            }
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}"),
            $this->page_name => site_url("loan/form_guarantee"),
        );

        $loan_person_data =  $this->person_model->get_by_id($loan_data['person_id']);
        $loan_spouse_data =  $this->person_model->get_by_id($loan_data['loan_spouse']);
        
        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'person_data' => $person_data,
            'loan_data' => $loan_data,
        	'loan_person_data' => $loan_person_data,
        	'loan_spouse_data' => $loan_spouse_data,
            'session_form' => $session_form,
            'mst' => $mst,
            'form_data' => $form_data,
        );
        
        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_guarantee_bondsman.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_guarantee_bondsman', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            //person_save
            $person_id = $this->loan_lib->person_save($post['person'], $post['person']['person_id']);

            if ($post['loan_bondsman_id']) {
                $loan_bondsman_id = $post['loan_bondsman_id'];
                
                $loan_guarantee_bondsman_data = array(
                    'bondsman_type' => $post['bondsman_type'],
                );
                
                $loan_guarantee_bondsman_model = $this->loan_guarantee_bondsman_model->save($loan_guarantee_bondsman_data, $loan_bondsman_id);
            } else {
                $loan_guarantee_bondsman_data = array(
                    'loan_id' => $loan_id,
                    'person_id' => $person_id,
                    'loan_bondsman_status' => 1,
                    'bondsman_type' => $post['bondsman_type'],
                );
                //insert loan_guarantee_bondsman
                $loan_guarantee_bondsman_model = $this->loan_guarantee_bondsman_model->get_by_opt(array('loan_id' => $loan_id, 'person_id' => $person_id));
                if (!$loan_guarantee_bondsman_model && ($loan_model['person_id'] != $person_id)) {
                    $loan_guarantee_bondsman_model = $this->loan_guarantee_bondsman_model->save($loan_guarantee_bondsman_data);
                    $loan_bondsman_id = $loan_guarantee_bondsman_model['id'];
                }                    
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_guarantee");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete loan_guarantee_bondsman_model by id
        if (isset($id)) {
            $this->loan_guarantee_bondsman_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_guarantee');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
