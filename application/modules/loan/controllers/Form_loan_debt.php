<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_loan_debt extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'หนี้สินของผู้ขอสินเชื่อและคู่สมรส';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_debt_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //form_data
        $form_data = array(
            'loan_id' => $loan_id,
            'loan_debt_id' => '',
            'loan_debt_name' => '',
            'loan_debt_amount' => money_num_to_str(0),
            'loan_debt_amount_start' => money_num_to_str(0),
            'loan_debt_amount_remain' => money_num_to_str(0),
            'loan_debt_date_start' => '',
            'loan_debt_date_end' => '',
            'loan_debt_owner_type' => 1,
            'loan_debt_owner_id' => $loan_model['person_id'],
            'loan_debt_interest' => money_num_to_str(0)
        );

        //$get
        $get = $this->input->get();
        if ($get) {
            //edit Loan_asset by id
            if (isset($get['edit'])) {
                $loan_debt_edit = $this->loan_debt_model->get_by_id($get['edit']);

                //form_data
                $form_data = array(
                    'loan_id' => $loan_id,
                    'loan_debt_id' => $loan_debt_edit['loan_debt_id'],
                    'loan_debt_name' => $loan_debt_edit['loan_debt_name'],
                    'loan_debt_amount' => money_num_to_str($loan_debt_edit['loan_debt_amount']),
                    'loan_debt_amount_start' => money_num_to_str($loan_debt_edit['loan_debt_amount_start']),
                    'loan_debt_amount_remain' => money_num_to_str($loan_debt_edit['loan_debt_amount_remain']),
                    'loan_debt_date_start' => den_to_dth($loan_debt_edit['loan_debt_date_start']),
                    'loan_debt_date_end' => den_to_dth($loan_debt_edit['loan_debt_date_end']),
                    'loan_debt_owner_type' => $loan_debt_edit['loan_debt_owner_type'],
                    'loan_debt_owner_id' => $loan_debt_edit['loan_debt_owner_id'],
                    'loan_debt_interest' => money_num_to_str($loan_debt_edit['loan_debt_interest'])
                );
            }
        }

        //loan_debt
        $loan_debt_data = $this->loan_debt_model->get_by_opt(array('loan_id' => $loan_id));
        
        //loan_debt_owner_type
        $mst = array(
            'debt_owner_type_list' => $this->loan_debt_model->get_debt_owner_type($loan_model)
        );

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'loan_debt_data' => $loan_debt_data,
            'session_form' => $session_form,
            'form_data' => $form_data,
            'mst' => $mst
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_debt.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_loan_debt', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {
            $post['loan_debt_amount_start'] = money_str_to_num($post['loan_debt_amount_start']);
            $post['loan_debt_amount_remain'] = money_str_to_num($post['loan_debt_amount_remain']);
            $post['loan_debt_amount'] = money_str_to_num($post['loan_debt_amount']);            
            $post['loan_debt_date_start'] = dth_to_den($post['loan_debt_date_start']);
            $post['loan_debt_date_end'] = dth_to_den($post['loan_debt_date_end']);
            $post['loan_debt_interest'] = money_str_to_num($post['loan_debt_interest']);            
            $post['loan_debt_owner_type'] = $post['loan_debt_owner_type'];
            $post['loan_debt_owner_id'] = $post['loan_debt_owner_type'] == 1 ? $loan_model['person_id'] : $loan_model['loan_spouse'];

            if ($post['loan_debt_id']) {
                //update loan_debt
                $id = $post['loan_debt_id'];
                unset($post['loan_debt_id']);

                $loan_debt_model = $this->loan_debt_model->save($post, $id);
                $loan_debt_id = $loan_debt_model['id'];
            } else {
                unset($post['loan_debt_id']);

                //insert loan_debt
                $loan_debt_model = $this->loan_debt_model->save($post);
                $loan_debt_id = $loan_debt_model['id'];
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_loan_debt");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete loan_debt by id
        if (isset($id)) {
            $this->loan_debt_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_loan_debt');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
