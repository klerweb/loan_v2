<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_guarantee_bond extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $page_name = 'หลักประกันการขอใช้สินเชื่อ';
    private $form_name = 'หลักประกันการใช้สินเชื่อด้วยพันธบัตรรัฐบาล';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('person_model');
        $this->load->model('loan_guarantee_bond_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //form_data
        $form_data = array(
            'loan_bond_id' => '',
            'loan_bond_owner' => '',
            'loan_bond_thaiid' => '',
            'loan_bond_tax' => '',
            'loan_bond_type' => '',
            'loan_bond_startnumber' => '',
            'loan_bond_endnumber' => '',
            'loan_bond_amount' => '',
            'loan_bond_path' => '',
            'loan_bond_title_id' => '',
            'loan_bond_fname' => '',
            'loan_bond_lname' => '',
            'loan_bond_regis_page' => '',
            'loan_bond_regis_date' => '',
        );

        //config & master
        $mst = array(
            'config_title_list' => $this->mst_model->get_all('title'),
        );

        //$get
        $get = $this->input->get();
        if ($get) {
            //edit Loan_income by id
            if (isset($get['edit'])) {
                $form_data = $this->loan_guarantee_bond_model->get_by_id($get['edit']);
                $form_data['loan_bond_amount'] = money_num_to_str($form_data['loan_bond_amount']);
                $form_data['loan_bond_regis_page'] = $form_data['loan_bond_regis_page'];
                $form_data['loan_bond_regis_date'] = den_to_dth($form_data['loan_bond_regis_date']);
            }
        } else {
            //person_data
            $person_data = $this->person_model->get_by_id($loan_model['person_id']);
            
            $form_data['loan_bond_thaiid'] = $person_data['person_thaiid'];
            $form_data['loan_bond_tax'] = $person_data['person_thaiid'];
            $form_data['loan_bond_title_id'] = $person_data['title_id'];
            $form_data['loan_bond_fname'] = $person_data['person_fname'];
            $form_data['loan_bond_lname'] = $person_data['person_lname'];
            $form_data['loan_bond_amount'] = money_num_to_str(0);            
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}"),
            $this->page_name => site_url("loan/form_guarantee"),
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'form_data' => $form_data,
            'session_form' => $session_form,
            'mst' => $mst,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_guarantee_bond.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_guarantee_bond', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {
            $post['loan_bond_amount'] = money_str_to_num($post['loan_bond_amount']);
            $post['loan_bond_regis_date'] = dth_to_den($post['loan_bond_regis_date']);
            
            $check_file = (isset($_FILES['loan_bond_path']) && $_FILES['loan_bond_path']['size'] > 0);
            if ($check_file) {
                $uploads_path = $this->config->item('uploads_loan_guarantee_path');
//                $config['upload_path'] = "assets/uploads/loan_doc/{$loan_id}";
                $config['upload_path'] = $uploads_path;
                $config['allowed_types'] = '*';
                $config['max_size'] = 0;
                $extension_file = explode('.', $_FILES['loan_bond_path']['name']);
                $config['file_name'] = date('YmdHis') . md5(date('YmdHis')) . '.' . $extension_file[1];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('loan_bond_path');
                $upload_data = $this->upload->data();
                $loan_bond_path = $uploads_path . $config['file_name'];
                $post['loan_bond_path'] = $loan_bond_path;
            }

            $tite_model = $this->mst_model->get_by_id('title', $post['loan_bond_title_id']);
            if ($tite_model) {
                $tite_name = $tite_model['title_name'];
            } else {
                $tite_name = '';
            }
            $post['loan_bond_owner'] = "{$tite_name}{$post['loan_bond_fname']} {$post['loan_bond_lname']}";

            if ($post['loan_bond_id']) {
                //update loan_guarantee_bond_model
                $loan_bond_id = $post['loan_bond_id'];
                unset($post['loan_bond_id']);
                $loan_guarantee_bond_model = $this->loan_guarantee_bond_model->save($post, $loan_bond_id);
            } else {
                //insert loan_guarantee_bond_model        
                unset($post['loan_bond_id']);
                $loan_guarantee_bond_model = $this->loan_guarantee_bond_model->save($post);
                $loan_bond_id = $loan_guarantee_bond_model['id'];
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_guarantee");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete loan_guarantee_bond_model by id
        if (isset($id)) {
            //delete file
            $loan_guarantee_bond_model = $this->loan_guarantee_bond_model->get_by_id($id);
            @unlink($loan_guarantee_bond_model['loan_bond_path']);

            //delete data
            $this->loan_guarantee_bond_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_guarantee');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
