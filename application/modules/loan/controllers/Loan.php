<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'รายการขอสินเชื่อ';

    function __construct() {
        parent::__construct();

        $this->load->model('loan_model');
        $this->load->model('loan_type_model');
        $this->load->model('loan_guarantee_bond_model');
        $this->load->model('loan_guarantee_bondsman_model');
        $this->load->model('loan_guarantee_bookbank_model');
        $this->load->model('loan_guarantee_land_model');
        $this->load->model('person_model');
        $this->load->model('mst_model');
        $this->load->model('building_model');
        $this->load->model('land_owner_model');
        $this->load->model('loan_income_model');
        $this->load->model('loan_expenditure_model');
        $this->load->model('loan_co_model');
        $this->load->model('loan_debt_model');
        $this->load->model('loan_doc_model');
        $this->load->model('loan_income_model');
        $this->load->model('loan_land_occupy_model');
        $this->load->model('loan_asset_model');
        $this->load->model('land_model');
        $this->load->model('status_married/status_married_model');
        $this->load->model('land_type/land_type_model');
        $this->load->model('district/district_model');
        $this->load->model('amphur/amphur_model');
        $this->load->model('province/province_model');
        $this->load->model('employee/employee_model');

#        $this->load->model('land_owner_model');
//        $this->load->model('building_model');
//        $this->load->model('loan_expenditure_model');
//
        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');
    }

    public function index() {
        //unset session_loan
        $this->session->unset_userdata('session_loan');

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //search
        $person_thaiid = '';
        $fullname = '';
        $loan_code = '';
        $page_param = '?search=1';

        if (!empty($_GET['loan_code'])) {
            $loan_code = $_GET['loan_code'];
            $page_param .= "&loan_code={$loan_code}";
        }

        if (!empty($_GET['person_thaiid'])) {
            $person_thaiid = $_GET['person_thaiid'];
            $page_param .= "&person_thaiid={$person_thaiid}";
        }

        if (!empty($_GET['fullname'])) {
            $fullname = $_GET['fullname'];
            $page_param .= "&fullname={$fullname}";
        }

        //offset & limit
        $page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
        $get_per_page = isset($_per_page) ? get_per_page($_per_page) : get_per_page();
        $option_q = array();
        $option_numrows = array();
        $option_limit['offset'] = $page == 1 ? 0 : ($page - 1) * $get_per_page;
        $option_limit['limit'] = $get_per_page;

        //result
        $result = $this->loan_model->search($option_limit, $loan_code, $person_thaiid, $fullname);
        $result_numrows = $this->loan_model->search_numrows($loan_code, $person_thaiid, $fullname);

        if ($result) {
            foreach ($result as $key => $res) {
                $result[$key]['objective_desc'] = '';
                $loan_type_model = $this->loan_type_model->get_by_opt(array('loan_id' => $res['loan_id']));
                if ($loan_type_model) {
                    foreach ($loan_type_model as $loan_type) {
                        $result[$key]['objective_desc'] .= ", {$loan_type['loan_objective_name']}";
                    }
                }

                $result[$key]['objective_desc'] = substr($result[$key]['objective_desc'], 1);
                $result[$key]['loan_status_desc'] = $res['loan_status'] ? $this->loan_model->_status_desc[$res['loan_status']] : '';
            }
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan')
        );



        //pagination
        $total_rows = $result_numrows;
        $config_pagination = config_pagination();
        $config_pagination['base_url'] = base_url("loan{$page_param}");
        $config_pagination['total_rows'] = $total_rows;
        $config_pagination['per_page'] = $get_per_page;

        $this->pagination->initialize($config_pagination);
        $pagination = $this->pagination->create_links();
        $title_pagination = title_pagination($page, $get_per_page, $total_rows);

        $data_content = array(
            'loan_code' => $loan_code,
            'person_thaiid' => $person_thaiid,
            'fullname' => $fullname,
            'result' => $result,
            'session_form' => $session_form,
            'pagination' => $pagination,
            'title_pagination' => $title_pagination,
            'row_start' => $option_limit['offset'] + 1
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js'),
            'title' => $this->form_name,
            'content' => $this->parser->parse('index', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function new_loan() {
        if (!isset($_SESSION['session_loan'])) {
            //insert new loan
            $insert_data = array();
            $loan_model = $this->loan_model->save($insert_data);
            $loan_id = $loan_model['id'];

            $session_loan = array(
                'loan_model' => $this->loan_model->get_by_id($loan_id)
            );

            $this->session->set_userdata('session_loan', $session_loan);

            redirect("loan/edit_loan/{$loan_id}");
        }

        redirect("loan");
    }

    public function edit_loan() {
        $loan_id = $this->uri->segment(3);
        $loan_model = $this->loan_model->get_by_id($loan_id);
        $session_loan = array(
            'loan_model' => $loan_model
        );
        $this->session->set_userdata('session_loan', $session_loan);

        //loan_model
        $loan_model = $_SESSION['session_loan']['loan_model'];

        //check_loan_co_age_60
        $check_loan_co_age_60 = $this->loan_lib->check_loan_co_age_60($loan_model['person_id'], $loan_model['loan_id']);
        if ($check_loan_co_age_60 === FALSE) {
            echo '<script>alert("อายุเกิน 60ปี ต้องมีผู้กู้ร่วม");</script>';
        }

        //get_status_list
        $status_list = $this->loan_model->get_status_list();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'แบบบันทึกขอสินเชื่อ';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan')
        );

        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'session_form' => $session_form,
            'loan_model' => $loan_model,
            'status_list' => $status_list,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js'),
            'content' => $this->parser->parse('form_list', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function delete_loan($loan_id) {
//        $this->load->model('land_model');
//        $this->load->model('land_owner_model');
//        $this->load->model('building_model');
//        building
//        building_assess
//        building_recode
//        land
//        land_assess
//        land_inspector
//        land_latitude
//        land_owner
//        land_recode

        $this->load->model('loan_guarantee_land_model');
        $this->load->model('loan_land_occupy_model');
        $this->load->model('loan_type_model');
        $this->load->model('loan_model');

        $this->load->model('loan_co_model');
        $this->load->model('loan_debt_model');
        $this->load->model('loan_doc_model');
        $this->load->model('loan_guarantee_bond_model');
        $this->load->model('loan_guarantee_bondsman_model');
        $this->load->model('loan_guarantee_bookbank_model');
        $this->load->model('loan_asset_model');

        $loan_model = $this->loan_model->get_by_id($loan_id);
        if (in_array($loan_model['loan_status'], array(2))) {
            //session_form
            $session_form = array(
                'status' => 'fail',
                'message' => 'ไม่สามารถลบข้อมูลได้.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan");
        }

        $this->loan_co_model->delete_by_loan($loan_id);
        $this->loan_debt_model->delete_by_loan($loan_id);
        $this->loan_doc_model->delete_by_loan($loan_id);
        $this->loan_guarantee_bond_model->delete_by_loan($loan_id);
        $this->loan_guarantee_bondsman_model->delete_by_loan($loan_id);
        $this->loan_guarantee_bookbank_model->delete_by_loan($loan_id);
        $this->loan_asset_model->delete_by_loan($loan_id);

        /* loan_guarantee_land */
        $this->loan_guarantee_land_model->delete_by_loan($loan_id);

        /* loan_land_occupy */
        $this->loan_land_occupy_model->delete_by_loan($loan_id);

        /* loan_type */
        $this->loan_type_model->delete_by_loan($loan_id);

        /* loan */
        $this->loan_model->delete($loan_id);

        //session_form
        $session_form = array(
            'status' => 'success',
            'message' => 'ลบข้อมูลเรียบร้อย.'
        );
        $this->session->set_userdata('session_form', $session_form);

        redirect("loan");
    }

    public function cancel_loan($loan_id) {
        $status_cancel = 0;

//        $this->load->model('land_model');
//        $this->load->model('land_owner_model');
//        $this->load->model('building_model');
//        building
//        building_assess
//        building_recode
//        land
//        land_assess
//        land_inspector
//        land_latitude
//        land_owner
//        land_recode

        $this->load->model('loan_guarantee_land_model');
        $this->load->model('loan_land_occupy_model');
        $this->load->model('loan_type_model');
        $this->load->model('loan_model');

        $this->load->model('loan_co_model');
        $this->load->model('loan_debt_model');
        $this->load->model('loan_doc_model');
        $this->load->model('loan_guarantee_bond_model');
        $this->load->model('loan_guarantee_bondsman_model');
        $this->load->model('loan_guarantee_bookbank_model');
        $this->load->model('loan_asset_model');

        $loan_model = $this->loan_model->get_by_id($loan_id);
        if (in_array($loan_model['loan_status'], array(2))) {
            //session_form
            $session_form = array(
                'status' => 'fail',
                'message' => 'ไม่สามารถแก้ไขข้อมูลได้.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan");
        }

        $this->loan_co_model->update_status_by_loan($loan_id, $status_cancel);
        $this->loan_debt_model->update_status_by_loan($loan_id, $status_cancel);
        $this->loan_doc_model->update_status_by_loan($loan_id, $status_cancel);
        $this->loan_guarantee_bond_model->update_status_by_loan($loan_id, $status_cancel);
        $this->loan_guarantee_bondsman_model->update_status_by_loan($loan_id, $status_cancel);
        $this->loan_guarantee_bookbank_model->update_status_by_loan($loan_id, $status_cancel);
        $this->loan_asset_model->update_status_by_loan($loan_id, $status_cancel);

        /* loan_guarantee_land */
        $this->loan_guarantee_land_model->update_status_by_loan($loan_id, $status_cancel);

        /* loan_land_occupy */
        $this->loan_land_occupy_model->update_status_by_loan($loan_id, $status_cancel);

        /* loan_type */
        $this->loan_type_model->update_status_by_loan($loan_id, $status_cancel);

        /* loan */
        $this->loan_model->update_status_by_loan($loan_id, $status_cancel);

        //session_form
        $session_form = array(
            'status' => 'success',
            'message' => 'แก้ไขข้อมูลเรียบร้อย.'
        );
        $this->session->set_userdata('session_form', $session_form);

        redirect("loan");
    }

    public function success_loan($loan_id) {
        $status_success = 2;

        $this->load->model('loan_model');
        $loan_model = $this->loan_model->get_by_id($loan_id);
        if (in_array($loan_model['loan_status'], array(2))) {
            //session_form
            $session_form = array(
                'status' => 'fail',
                'message' => 'ไม่สามารถแก้ไขข้อมูลได้.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan");
        }

        /* loan */
        $this->loan_model->save(array('loan_status' => $status_success), $loan_id);

        //session_form
        $session_form = array(
            'status' => 'success',
            'message' => 'แก้ไขข้อมูลเรียบร้อย.'
        );
        $this->session->set_userdata('session_form', $session_form);

        redirect("loan");
    }

    public function draf_loan($loan_id) {
        $status_draf = 1;

        $this->load->model('loan_model');
        $loan_model = $this->loan_model->get_by_id($loan_id);
        if (in_array($loan_model['loan_status'], array(2))) {
            //session_form
            $session_form = array(
                'status' => 'fail',
                'message' => 'ไม่สามารถแก้ไขข้อมูลได้.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan");
        }

        /* loan */
        $this->loan_model->save(array('loan_status' => $status_draf), $loan_id);

        //session_form
        $session_form = array(
            'status' => 'success',
            'message' => 'แก้ไขข้อมูลเรียบร้อย.'
        );
        $this->session->set_userdata('session_form', $session_form);

        redirect("loan");
    }

    public function form_guarantee() {
        //check_page_permission
        $this->check_page_permission();

        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];
        $opt = array('loan_id' => $loan_id);

        //loan_guarantee_land
        $loan_guarantee_land_data = $this->loan_guarantee_land_model->get_table_data($opt);

        //loan_guarantee_bond
        $loan_guarantee_bond_data = $this->loan_guarantee_bond_model->get_by_opt($opt);

        //loan_guarantee_bookbank
        $loan_guarantee_bookbank_data = $this->loan_guarantee_bookbank_model->report_list($opt);

        //loan_guarantee_bondsman
        $loan_guarantee_bondsman_data = $this->loan_guarantee_bondsman_model->report_list($opt);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'หลักประกันการขอใช้สินเชื่อ';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'session_form' => $session_form,
            'loan_guarantee_land_data' => $loan_guarantee_land_data,
            'loan_guarantee_bond_data' => $loan_guarantee_bond_data,
            'loan_guarantee_bookbank_data' => $loan_guarantee_bookbank_data,
            'loan_guarantee_bondsman_data' => $loan_guarantee_bondsman_data,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_guarantee', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function check_page_permission() {
        $page = $this->uri->segment(2);
        $loan_id = $_SESSION['session_loan']['loan_model']['loan_id'];
        $person_id = $_SESSION['session_loan']['loan_model']['person_id'];

        if (!isset($_SESSION['session_loan'])) {
            redirect("loan");
        } elseif ($page != 'form_person' && !$person_id) {

            //session_form
            $session_form = array(
                'status' => 'fail',
                'message' => 'ไม่พบข้อมูลผู้ขอสินเชื่อ.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan/edit_loan/{$loan_id}");
        }
    }

    public function search_person() {
        $get = $this->input->get();
        $person_model = $this->person_model->get_row_by_opt(array('person_thaiid' => $get['person_thaiid']));
        $person_model['person_birthdate'] = den_to_dth($person_model['person_birthdate']);
        $person_model['person_card_startdate'] = den_to_dth($person_model['person_card_startdate']);
        $person_model['person_card_expiredate'] = den_to_dth($person_model['person_card_expiredate']);

        echo json_encode($person_model);
        exit;
    }

    public function form_sample() {
        //check_page_permission
        $this->check_page_permission();

        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {
            //$this->loan_model->save($post, $loan_id);
            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan/form_objective");
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //data_content
        $data_content = array(
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'session_form' => $session_form
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array(),
            'title' => 'แบบบันทึกขอสินเชื่อ',
            'content' => $this->parser->parse('form_objective', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function province($id = '') {
        $data = $this->input->get();
        $data['province'] = $this->mst_model->get_table_location_all('province');
        $data['province_val'] = $id;

//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';

        $this->parser->parse('province', $data);
    }

    public function amphur($province = '', $id = '') {
        if ($province == '') {
            $data = $this->input->get();
            $data['amphur'] = array();
        } else {
            $data = $this->input->get();

            $this->load->model('amphur/amphur_model', 'amphur');
            $data['amphur'] = $this->amphur->getByProvince($province);
            $data['amphur_val'] = $id;
        }
//echo '<pre>';
//print_r($data);
//echo '</pre>';

        $this->parser->parse('amphur', $data);
    }

    public function district($amphur = '', $id = '') {
        if ($amphur == '') {
            $data = $this->input->get();
            $data['district'] = array();
        } else {
            $data = $this->input->get();

            $this->load->model('district/district_model', 'district');
            $data['district'] = $this->district->getByAmphur($amphur);
            $data['district_val'] = $id;
        }

        $data['zipcode'] = json_encode($this->mst_model->get_all_zipcode('zipcode'));

        $this->parser->parse('district', $data);
    }

    public function province_2($id = '') {
        $data = $this->input->get();
        $data['province'] = $this->mst_model->get_table_location_all('province');
        $data['province_val'] = $id;

//        echo '<pre>';
//        print_r($data);
//        echo '</pre>';

        $this->parser->parse('province_2', $data);
    }

    public function amphur_2($province = '', $id = '') {
        if ($province == '') {
            $data = $this->input->get();
            $data['amphur'] = array();
        } else {
            $data = $this->input->get();

            $this->load->model('amphur/amphur_model', 'amphur');
            $data['amphur'] = $this->amphur->getByProvince($province);
            $data['amphur_val'] = $id;
        }
//echo '<pre>';
//print_r($data);
//echo '</pre>';

        $this->parser->parse('amphur_2', $data);
    }

    public function district_2($amphur = '', $id = '') {
        if ($amphur == '') {
            $data = $this->input->get();
            $data['district'] = array();
        } else {
            $data = $this->input->get();

            $this->load->model('district/district_model', 'district');
            $data['district'] = $this->district->getByAmphur($amphur);
            $data['district_val'] = $id;
        }
//echo '<pre>';
//print_r($data);
//echo '</pre>';

        $this->parser->parse('district_2', $data);
    }

    public function province_opt($id = '') {
        $data = $this->input->get();
        $province = $this->mst_model->get_table_location_all('province');
//        echo '<option value="" selected>จังหวัด</option>';
//
//        if (!empty($result)) {
//            foreach ($result as $value) {
//                echo '<option value="' . $value['province_id'] . '">' . $value['province_name'] . '</option>';
//            }
//        } else {
//            return false;
//        }


        $class = isset($data['province_class']) ? $data['province_class'] : '';
        $required = isset($data['province_required']) ? $data['province_required'] : 'required';

        $sel = "";
        $sel .="<select name=\"{$data['province_name']}\" id=\"{$data['province_id']}\" data-placeholder=\"อำเภอ/เขต\" class=\"width100p {$class}\" {$required}>";
        $sel .="<option value=\"\">จังหวัด</option>";


        foreach ($province as $province_data) {
            $selected = $id == $province_data['province_id'] ? "selected=\"selected\"" : '';
            $sel .= "<option value=\"{$province_data['province_id']}\" {$selected}>{$province_data['province_name']}</option>";
        }
        $sel .= "</select>";

        $script = "<script type=\"text/javascript\">";
        $script .= "var id_list = 'province_id={$data['province_id']}&amphur_id={$data['amphur_id']}&district_id={$data['district_id']}';";
        $script .= "var name_list = 'province_name={$data['province_name']}&amphur_name={$data['amphur_name']}&district_name={$data['district_name']}';";
        $script .= "var param_url = \"/?\" + id_list + \"&\" + name_list;";
        $script .= "$(\"#{$data['province_id']}\").select2({minimumResultsForSearch: -1});";
        $script .= "$(\"#{$data['province_id']}\").change(function () {";
        $script .= "$(\"#div_{$data['amphur_id']}\").load(base_url + \"loan/amphur/\" + $(\"#{$data['province_id']}\").val() + param_url);";
        $script .= "$(\"#div_{district_id }\").load(base_url + \"loan/district/\" + param_url);";
        $script .= "});";
        if (isset($data['province_readonly']) && $data['province_readonly'] == 'readonly') {
            $script .= "$(\"#{$data['province_id']}\").select2('readonly', true);";
        }
        $script .= "</script>";


        echo $sel . $script;
        exit;
    }

    public function decode_table_param($table_param) {
        $table_param = json_decode(base64_decode($table_param), TRUE);
        $table_param['callbackurl'] = base64_decode($table_param['callbackurl']);

        return $table_param;
    }

    ############ manage land

    public function form_land_list() {
        $get = $this->input->get();

      	$table_land_building_param = $this->decode_table_param($get['table_land_building_param']);
        //echo '<pre>';
        //print_r($table_land_building_param);
        //echo '</pre>';
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //$land_model
        if (isset($get['land_id']) && $get['land_id']) {
        	$land_model = $this->land_model->get_by_id($get['land_id']);
        } else {
            $land_model = $this->loan_lib->land_fields();
        }
        $land_model['loan_type_id'] = $table_land_building_param['loan_type_id'];
        //$form_data
        $form_data = array_merge($land_model, $this->loan_lib->building_fields());
        //echo '<pre>';
      //  print_r($land_model);
      //  echo '</pre>';
        $form_data['loan_id'] = $loan_id;
        $form_data['buiding_owner_status'] = 0; //ตนเอง
        $form_data['table_land_building_param'] = $table_land_building_param;
        $form_data['table_land_building_param_encode'] = $get['table_land_building_param'];


        $person_id = isset($get['owner_id'])?$get['owner_id']:$loan_model['person_id'];

        if ($person_id) {
            $person_data = $this->loan_lib->person_detail($person_id);
        } else {
            $person_data = $this->loan_lib->person_fields();
        }

        //mst
        $mst = array(
            //config & master for person
            'config_title_list' => $this->mst_model->get_all('title'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            'config_building_type_list' => $this->mst_model->get_all('building_type'),
            'land_buiding_detail_id_list' => $this->land_model->get_land_buiding_detail_id_list(),
            'buiding_owner_status_list' => $this->land_model->get_buiding_owner_status_list(),
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
        );

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'ข้อมูลที่ดิน';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'form_data' => $form_data,
            'person_data' => $person_data,
            'session_form' => $session_form,
            'mst' => $mst,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'content' => $this->parser->parse('form_land_list', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_land_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_land_building_param = $this->decode_table_param($get['table_land_building_param']);

        //$post
        $post = $this->input->post();

        //land
        $land = array(
          	'land_type_id' => $post['land_type_id'],
            'land_no' => $post['land_no'],
            'land_addr_no' => $post['land_addr_no'],
            'land_addr_moo' => $post['land_addr_moo'],
            'land_addr_district_id' => $post['land_addr_district_id'],
            'land_addr_amphur_id' => $post['land_addr_amphur_id'],
            'land_addr_province_id' => $post['land_addr_province_id'],
            'land_area_rai' => $post['land_area_rai'],
            'land_area_ngan' => $post['land_area_ngan'],
            'land_area_wah' => $post['land_area_wah'],
            'land_buiding_detail_id' => 0,
        	'land_estimate_price' => $post['land_estimate_price']
        );

        if(!empty($post['loan_type_id'])) $land['loan_type_id'] = $post['loan_type_id'];
        $land_model = $this->land_model->save($land, $post['land_id']);
        $land_id = $land_model['id'];

        if ($land_id) {
            //land_owner
            $land_owner_opt = array('land_id' => $land_id, 'person_id' => $post['owner_id']);
            $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
            if (!$land_owner_model) {
                $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                $land_owner_id = $land_owner_model['id'];
            }

            //loan_guarantee_land
            if (isset($table_land_building_param['save_loan_guarantee_land']) && $table_land_building_param['save_loan_guarantee_land'] == 1) {
                $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_row_by_opt(array('land_id' => $land_id));

                if (!$loan_guarantee_land_model) {
                    $post_loan_guarantee_land_model = array(
                        'loan_id' => $loan_id,
                        'land_id' => $land_id
                    );
                    $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model);
                    $loan_guarantee_land_id = $loan_guarantee_land_model['id'];
                }
            }
        }

        redirect($table_land_building_param['callbackurl']);
    }

    public function del_land_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_land_building_param = $this->decode_table_param($get['table_land_building_param']);

        //land
        $land = array(
            'loan_type_id' => NULL,
            'land_buiding_detail_id' => 0,
        );
        $land_model = $this->land_model->save($land, $get['land_id']);

        redirect($table_land_building_param['callbackurl']);
    }

    ############ manage building

    public function form_building_list() {
        $get = $this->input->get();
        $table_land_building_param = $this->decode_table_param($get['table_land_building_param']);

        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //$land_model
        if (isset($get['land_id']) && $get['land_id']) {
            $land_model = $this->land_model->get_by_id($get['land_id']);
        } else {
            $land_model = $this->loan_lib->land_fields();
        }

        //$building_model
        if (isset($get['building_id']) && $get['building_id']) {
            $building_model = $this->building_model->get_by_id($get['building_id']);
        } else {
            $building_model = $this->loan_lib->building_fields();
        }



  	    $person_model = $this->loan_lib->person_detail($loan_model['person_id']);

        // echo '<pre>';
        // print_r($building_model);
        //   print_r($person_model);
        // echo '</pre>';

        //$form_data
        $form_data = array_merge($land_model, $building_model);
        $form_data['loan_id'] = $loan_id;
        $form_data['table_land_building_param'] = $table_land_building_param;
        $form_data['table_land_building_param_encode'] = $get['table_land_building_param'];
        $person_id = $get['owner_id'];
        if (isset($get['edit'])) {
          $form_data['buiding_owner_status'] = ($person_model['person_fname']==$building_model['building_owner_fname']
            && $person_model['person_lname']==$building_model['building_owner_lname'])?0:1;

          $form_data['building_estimate_price'] = $building_model['building_estimate_price'];
          //Temporary set person_data from building_owner
          $person_data = $this->loan_lib->person_fields();
          $person_data['person_id'] = $person_id;
          $person_data['title_id'] = $building_model['building_owner_title_id'];
          $person_data['person_fname'] = $building_model['building_owner_fname'];
          $person_data['person_lname'] = $building_model['building_owner_lname'];

        }else{
          $form_data['buiding_owner_status'] = 0;
          $form_data['building_estimate_price'] = 0;
          if ($person_id) {
              $person_data = $this->loan_lib->person_detail($person_id);
          } else {
              $person_data = $this->loan_lib->person_fields();
          }
        }




        //mst
        $mst = array(
            //config & master for person
            'config_title_list' => $this->mst_model->get_all('title'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            'config_building_type_list' => $this->mst_model->get_all('building_type'),
            'land_buiding_detail_id_list' => $this->land_model->get_land_buiding_detail_id_list(),
            'buiding_owner_status_list' => $this->land_model->get_buiding_owner_status_list(),
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
        );

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'ข้อมูลสิ่งปลูกสร้าง';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'form_data' => $form_data,
            'person_data' => $person_data,
            'session_form' => $session_form,
            'mst' => $mst,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land.js'),
            'content' => $this->parser->parse('form_building_list', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_building_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_land_building_param = $this->decode_table_param($get['table_land_building_param']);

        //$post
        $post = $this->input->post();

        $building = array(
            'land_id' => $post['land_id'],
            'building_owner_title_id' => $post['building_owner_title_id'],
            'building_owner_fname' => $post['building_owner_fname'],
            'building_owner_lname' => $post['building_owner_lname'],
            'building_no' => $post['building_no'],
            'building_moo' => $post['building_moo'],
            'building_name' => $post['building_name'],
            'building_soi' => $post['building_soi'],
            'building_road' => $post['building_road'],
            'building_district_id' => $post['building_district_id'] ? $post['building_district_id'] : 0,
            'building_amphur_id' => $post['building_amphur_id'] ? $post['building_amphur_id'] : 0,
            'building_province_id' => $post['building_province_id'] ? $post['building_province_id'] : 0,
            'building_type_id' => $post['building_type_id'] ? $post['building_type_id'] : 0,
        	'building_estimate_price' => $post['building_estimate_price']? $post['building_estimate_price'] : 0
        );

        $building_model = $this->building_model->save($building, $post['building_id']);
        $building_id = $building_model['id'];

        //land
        $land = array(
            'land_buiding_detail_id' => 1,
        );
        $land_model = $this->land_model->save($land, $post['land_id']);

        redirect($table_land_building_param['callbackurl']);
    }

    public function del_building_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_land_building_param = $this->decode_table_param($get['table_land_building_param']);

        //building_model
        $this->building_model->delete($get['building_id']);

        redirect($table_land_building_param['callbackurl']);
    }

    ############ manage income

    public function form_income_list() {
        $get = $this->input->get();
        $table_param = $this->decode_table_param($get['table_param']);

        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //form_data
        $form_data = array(
            'loan_activity_id' => '',
            'loan_activity_type' => '',
            'loan_activity_name' => '',
            'loan_activity_income1' => money_num_to_str(0),
            'loan_activity_income2' => money_num_to_str(0),
            'loan_activity_income3' => money_num_to_str(0),
            'loan_activity_expenditure1' => money_num_to_str(0),
            'loan_activity_expenditure2' => money_num_to_str(0),
            'loan_activity_expenditure3' => money_num_to_str(0),
            'loan_activity_self_rai' => '',
            'loan_activity_self_ngan' => '',
            'loan_activity_self_wah' => '',
            'loan_activity_renting_rai' => '',
            'loan_activity_renting_ngan' => '',
            'loan_activity_renting_wah' => '',
            'loan_activity_approve_rai' => '',
            'loan_activity_approve_ngan' => '',
            'loan_activity_approve_wah' => '',
        );

        if (isset($get['edit'])) {
            $form_data = $this->loan_income_model->get_by_id($get['edit']);
            $form_data['loan_activity_income1'] = money_num_to_str($form_data['loan_activity_income1']);
            $form_data['loan_activity_income2'] = money_num_to_str($form_data['loan_activity_income2']);
            $form_data['loan_activity_income3'] = money_num_to_str($form_data['loan_activity_income3']);
            $form_data['loan_activity_expenditure1'] = money_num_to_str($form_data['loan_activity_expenditure1']);
            $form_data['loan_activity_expenditure2'] = money_num_to_str($form_data['loan_activity_expenditure2']);
            $form_data['loan_activity_expenditure3'] = money_num_to_str($form_data['loan_activity_expenditure3']);
        }

        //$form_data
        $form_data['loan_id'] = $loan_id;
        $form_data['table_param'] = $table_param;
        $form_data['table_param_encode'] = $get['table_param'];
        $form_data['person_id'] = $table_param['owner_id'];


        $person_id = $table_param['owner_id'];
        //loan_income
        $loan_income_data = $this->loan_income_model->get_by_opt(array('person_id' => $person_id));
        $loan_activity_type_data = $this->loan_income_model->get_loan_activity_type();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'รายได้ในครัวเรือน';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'form_data' => $form_data,
            'session_form' => $session_form,
            'loan_income_data' => $loan_income_data,
            'loan_activity_type_data' => $loan_activity_type_data,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_income.js'),
            'content' => $this->parser->parse('form_income_list', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_income_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_param = $this->decode_table_param($get['table_param']);

        //$post
        $post = $this->input->post();

        if ($post) {

            $post['loan_activity_income1'] = money_str_to_num($post['loan_activity_income1']);
            $post['loan_activity_income2'] = money_str_to_num($post['loan_activity_income2']);
            $post['loan_activity_income3'] = money_str_to_num($post['loan_activity_income3']);
            $post['loan_activity_expenditure1'] = money_str_to_num($post['loan_activity_expenditure1']);
            $post['loan_activity_expenditure2'] = money_str_to_num($post['loan_activity_expenditure2']);
            $post['loan_activity_expenditure3'] = money_str_to_num($post['loan_activity_expenditure3']);

            $post['loan_activity_self_rai'] = $post['loan_activity_self_rai'] ? money_str_to_num($post['loan_activity_self_rai']) : 0;
            $post['loan_activity_self_ngan'] = $post['loan_activity_self_ngan'] ? money_str_to_num($post['loan_activity_self_ngan']) : 0;
            $post['loan_activity_self_wah'] = $post['loan_activity_self_wah'] ? money_str_to_num($post['loan_activity_self_wah']) : 0;
            $post['loan_activity_renting_rai'] = $post['loan_activity_renting_rai'] ? money_str_to_num($post['loan_activity_renting_rai']) : 0;
            $post['loan_activity_renting_ngan'] = $post['loan_activity_renting_ngan'] ? money_str_to_num($post['loan_activity_renting_ngan']) : 0;
            $post['loan_activity_renting_wah'] = $post['loan_activity_renting_wah'] ? money_str_to_num($post['loan_activity_renting_wah']) : 0;
            $post['loan_activity_approve_rai'] = $post['loan_activity_approve_rai'] ? money_str_to_num($post['loan_activity_approve_rai']) : 0;
            $post['loan_activity_approve_ngan'] = $post['loan_activity_approve_ngan'] ? money_str_to_num($post['loan_activity_approve_ngan']) : 0;
            $post['loan_activity_approve_wah'] = $post['loan_activity_approve_wah'] ? money_str_to_num($post['loan_activity_approve_wah']) : 0;

            if ($post['loan_activity_id']) {
                //update loan_income
                $id = $post['loan_activity_id'];
                unset($post['loan_activity_id']);
                unset($post['person_id']);

                $loan_income_model = $this->loan_income_model->save($post, $id);
                $loan_activity_id = $loan_income_model['id'];
            } else {
                unset($post['loan_activity_id']);

                //insert loan_income
                $loan_income_model = $this->loan_income_model->save($post);
                $loan_activity_id = $loan_income_model['id'];
            }
        }


        redirect($table_param['callbackurl']);
    }

    public function del_income_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_param = $this->decode_table_param($get['table_param']);

        //loan_income_model
        $this->loan_income_model->delete($get['loan_activity_id']);

        redirect($table_param['callbackurl']);
    }

    ############ manage expenditure

    public function form_expenditure_list() {
        $get = $this->input->get();
        $table_param = $this->decode_table_param($get['table_param']);

        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //form_data
        $form_data = array(
            'loan_expenditure_id' => '',
            'loan_expenditure_type' => '',
            'loan_expenditure_name' => '',
            'loan_expenditure_amount1' => money_num_to_str(0),
            'loan_expenditure_amount2' => money_num_to_str(0),
            'loan_expenditure_amount3' => money_num_to_str(0)
        );

        if (isset($get['edit'])) {
            $form_data = $this->loan_expenditure_model->get_by_id($get['edit']);
            $form_data['loan_expenditure_amount1'] = money_num_to_str($form_data['loan_expenditure_amount1']);
            $form_data['loan_expenditure_amount2'] = money_num_to_str($form_data['loan_expenditure_amount2']);
            $form_data['loan_expenditure_amount3'] = money_num_to_str($form_data['loan_expenditure_amount3']);
        }

        //$form_data
        $form_data['loan_id'] = $loan_id;
        $form_data['table_param'] = $table_param;
        $form_data['table_param_encode'] = $get['table_param'];
        $form_data['person_id'] = $table_param['owner_id'];

        $person_id = $table_param['owner_id'];
        //loan_expenditure
        $loan_expenditure_data = $this->loan_expenditure_model->get_by_opt(array('person_id' => $person_id));
        $loan_expenditure_type_data = $this->loan_expenditure_model->get_loan_expenditure_type();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'ค่าใช้จ่ายในครัวเรือน';

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );


        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'form_data' => $form_data,
            'session_form' => $session_form,
            'loan_expenditure_data' => $loan_expenditure_data,
            'loan_expenditure_type_data' => $loan_expenditure_type_data,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_expenditure.js'),
            'content' => $this->parser->parse('form_loan_expenditure_list', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save_expenditure_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_param = $this->decode_table_param($get['table_param']);

        //$post
        $post = $this->input->post();

        if ($post) {
            $post['loan_expenditure_amount1'] = money_str_to_num($post['loan_expenditure_amount1']);
            $post['loan_expenditure_amount2'] = money_str_to_num($post['loan_expenditure_amount2']);
            $post['loan_expenditure_amount3'] = money_str_to_num($post['loan_expenditure_amount3']);

            if ($post['loan_expenditure_id']) {
//                echo '1';
//                exit;
                //update loan_expenditure
                $id = $post['loan_expenditure_id'];
                unset($post['loan_expenditure_id']);
                unset($post['person_id']);

                $loan_expenditure_model = $this->loan_expenditure_model->save($post, $id);
                $loan_expenditure_id = $loan_expenditure_model['id'];
            } else {
                unset($post['loan_expenditure_id']);

                //insert loan_expenditure
                $loan_expenditure_model = $this->loan_expenditure_model->save($post);
                $loan_expenditure_id = $loan_expenditure_model['id'];

                if ($loan_expenditure_id && $post['loan_expenditure_type'] == $this->loan_expenditure_model->loan_expenditure_type_other) {
                    $post_loan_debt['loan_id'] = $loan_id;
                    $post_loan_debt['loan_debt_name'] = $post['loan_expenditure_name'];
                    $post_loan_debt['loan_debt_amount'] = $post['loan_expenditure_amount1'];

                    $loan_debt_model = $this->loan_debt_model->save($post_loan_debt);
                    $loan_debt_id = $loan_debt_model['id'];
                }
            }
        }


        redirect($table_param['callbackurl']);
    }

    public function del_expenditure_list() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        $get = $this->input->get();
        $table_param = $this->decode_table_param($get['table_param']);

        //loan_expenditure_model
        $this->loan_expenditure_model->delete($get['id']);

        redirect($table_param['callbackurl']);
    }

    ############# public function pdf($id)

    /* report list */

    public function report($loan_id) {
        $loan_id = $this->uri->segment(3);
        $loan_model = $this->loan_model->get_by_id($loan_id);
        $session_loan = array(
            'loan_model' => $loan_model
        );
        $this->session->set_userdata('session_loan', $session_loan);

        //loan_model
        $loan_model = $_SESSION['session_loan']['loan_model'];

        //get_status_list
        $status_list = $this->loan_model->get_status_list();

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = 'รายงานแบบบันทึกขอสินเชื่อ';
        $this->title = $form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => '#'
        );

        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'session_form' => $session_form,
            'loan_model' => $loan_model,
            'status_list' => $status_list,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js'),
            'content' => $this->parser->parse('report_list', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    function pdf_land_list($land_list) {
        $htmlcontent = '';

        foreach ($land_list as $key => $data) {
            $num_land = $key + 1;
            $htmlcontent .='<table width="100%">';
            $htmlcontent1 ='
			<tr>
				<td width="60%">' . multi_space(28) . num2Thai("{$num_land}&nbsp;)&nbsp;") . 'ที่ดิน&nbsp;' . $this->pdf_land_type($data['land_type_id']) . '</td>
                                <td width="5%">&nbsp;</td>
                                <td width="15%">เลขที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_no'])) . multi_space(1) . '</u></td>
                                <td width="15%">เลขที่ดิน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_addr_no'])) . multi_space(1) . '</u></td>
			</tr>
            ';
            $htmlcontent .='
			<tr>
				<td width="75%">' . multi_space(28) . num2Thai("{$num_land}&nbsp;)&nbsp;") . 'ที่ดิน&nbsp;' . $this->pdf_land_type($data['land_type_id']) . '</td>
                                <td width="12%">เลขที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_no'])) . multi_space(1) . '</u></td>
                                <td width="12%">เลขที่ดิน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_addr_no'])) . multi_space(1) . '</u></td>
			</tr>
            ';
            $htmlcontent .='
			<tr>
                                <td width="100%">ตำบล/แขวง&nbsp;<u class="udotted">' . multi_space(1) . view_value($data['land_addr_district_name']) . multi_space(1) . '</u>'
                    . multi_space(2) . 'อำเภอ/เขต&nbsp;<u class="udotted">' . multi_space(1) . view_value($data['land_addr_amphur_name']) . multi_space(1) . '</u>'
                    . multi_space(2) . 'จังหวัด&nbsp;<u class="udotted">' . multi_space(1) . view_value($data['land_addr_province_name']) . multi_space(1) . '</u></td>
			</tr>
            ';

            $htmlcontent .='
			<tr>
                                <td width="50%">จำนวนเนื้อที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_area_rai'])) . multi_space(1) . '</u> ไร่'
                    . multi_space(2) . '<u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_area_ngan'])) . multi_space(1) . '</u> งาน'
                    . multi_space(2) . '<u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_area_wah'])) . multi_space(1) . '</u> ตารางวา
                                </td>
			</tr>
            ';

            //land_buiding
            if ($data['building_list']) {
                foreach ($data['building_list'] as $key_building => $data_building) {
                    $num_building = $key_building + 1;
                    //$htmlcontent .='<table width="100%">';
                    $htmlcontent .='
			<tr>
				<td width="100%">'.multi_space(28) . num2Thai("{$num_land}.{$num_building}&nbsp;)&nbsp;") . 'สิ่งปลูกสร้าง ประเภท </td>
			</tr>
                    ';
                    $htmlcontent .= $this->pdf_building_type($data_building['building_type_id']);

                    $htmlcontent .='
			<tr>
				<td width="20%">สิ่งปลูกสร้าง เลขที่&nbsp;<u class="udotted">' . multi_space(1) . view_value(num2Thai($data_building['building_no'])) . multi_space(1) . '</u></td>
				<td width="20%">อาคาร&nbsp;<u class="udotted">' . multi_space(1) . view_value(num2Thai($data_building['building_name'])) . multi_space(1) . '</u></td>
				<td width="20%">หมู่ที่&nbsp;<u class="udotted">' . multi_space(1) . view_value(num2Thai($data_building['building_moo'])) . multi_space(1) . '</u></td>
				<td width="20%">ถนน&nbsp;<u class="udotted">' . multi_space(1) . view_value(num2Thai($data_building['building_road'])) . multi_space(1) . '</u></td>
			</tr>
                    ';

                    $htmlcontent .='
			<tr>
				<td width="25%">ตำบล/แขวง&nbsp;<u class="udotted">' . multi_space(1) . view_value($data_building['building_district_name']) . multi_space(1) . '</u></td>
				<td width="25%">อำเภอ/เขต&nbsp;<u class="udotted">' . multi_space(1) . view_value($data_building['building_amphur_name']) . '</u></td>
				<td width="25%">จังหวัด&nbsp;<u class="udotted">' . multi_space(1) . view_value($data_building['building_province_name']) . '</u></td>
			</tr>
                    ';

                    //$htmlcontent .='</table>';
                }
            }

            $htmlcontent .='</table>';

            $htmlcontent .='<table width="100%"><tr><td width="100%"></td></tr></table>';
        }

        return $htmlcontent;
    }

    function pdf_land_type($land_type_id) {
        //config_land_type
        $config_land_type = $this->mst_model->get_all('land_type');

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        $htmlcontent = '<table width="100%"><tr>';
        foreach ($config_land_type as $key => $data) {
            if ($data['land_type_id'] == $land_type_id) {
                $htmlcontent .= '<td>' . $html_checkbox . '&nbsp;' . $data['land_type_name'] . '</td>';
            } else {
                $htmlcontent .= '<td>' . $html_uncheckbox . '&nbsp;' . $data['land_type_name'] . '</td>';
            }
        }
        $htmlcontent .='</tr></table>';
        return $htmlcontent;
    }

    function pdf_building_type($building_type_id) {
        //config_building_type
        $config_building_type = $this->mst_model->get_all('building_type');

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        $htmlcontent = '';
//        $htmlcontent .= '<table width="100%">';
        $htmlcontent .= '<tr>';
        foreach ($config_building_type as $key => $data) {
            if ($data['building_type_id'] == $building_type_id) {
                $htmlcontent_res = '<td width="20%">' . $html_checkbox . '&nbsp;' . $data['building_type_name'] . '</td>';
            } else {
                $htmlcontent_res = '<td width="20%">' . $html_uncheckbox . '&nbsp;' . $data['building_type_name'] . '</td>';
            }

            if ($key % 3 == 0 && $key > 0) {
                $htmlcontent .= $htmlcontent_res;
                $htmlcontent .= '</tr><tr>';
            } else {
                $htmlcontent .= $htmlcontent_res;
            }
        }
        $htmlcontent .= '</tr>';
//        $htmlcontent .='</table>';
        return $htmlcontent;
    }

    function pdf_case_type($case_type) {
        //case_type
        $case_type_list = $this->loan_type_model->get_case_type();

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        $htmlcontent = '';
        $htmlcontent .= '<table width="100%">';
        $htmlcontent .= '<tr>';
        foreach ($case_type_list as $key => $data) {
            if ($key == $case_type) {
                $htmlcontent_res = '<td width="10%">' . $html_checkbox . '&nbsp;' . $data . '</td>';
            } else {
                $htmlcontent_res = '<td width="10%">' . $html_uncheckbox . '&nbsp;' . $data . '</td>';
            }

            if ($key % 3 == 0 && $key > 0) {
                $htmlcontent .= $htmlcontent_res;
                $htmlcontent .= '</tr><tr>';
            } else {
                $htmlcontent .= $htmlcontent_res;
            }
        }
        $htmlcontent .= '</tr>';
        $htmlcontent .='</table>';
        return $htmlcontent;
    }

    function pdf_redeem_type($redeem_type_id) {
        //redeem_type
        $redeem_type_list = $this->loan_type_model->get_redeem_type();

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        $htmlcontent = '';
        $htmlcontent .= '<table width="100%">';
        $htmlcontent .= '<tr>';
        foreach ($redeem_type_list as $key => $data) {
            if ($key == $redeem_type_id) {
                $htmlcontent_res = '<td width="20%">' . $html_checkbox . '&nbsp;' . $data . '</td>';
            } else {
                $htmlcontent_res = '<td width="20%">' . $html_uncheckbox . '&nbsp;' . $data . '</td>';
            }

            if ($key % 3 == 0 && $key > 0) {
                $htmlcontent .= $htmlcontent_res;
                $htmlcontent .= '</tr><tr>';
            } else {
                $htmlcontent .= $htmlcontent_res;
            }
        }
        $htmlcontent .= '</tr>';
        $htmlcontent .='</table>';
        return $htmlcontent;
    }

    function pdf_land_bind_type($loan_guarantee_land_bind_type) {
        //land_bind_type
        $land_bind_type_list = $this->loan_guarantee_land_model->get_loan_guarantee_land_bind_type_list();

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        $htmlcontent = '';
        $htmlcontent .= '<table width="100%">';
        $htmlcontent .= '<tr>';
        foreach ($land_bind_type_list as $key => $data) {
            if ($key == $loan_guarantee_land_bind_type) {
                $htmlcontent_res = '<td width="50%">' . $html_checkbox . '&nbsp;' . $data . '</td>';
            } else {
                $htmlcontent_res = '<td width="50%">' . $html_uncheckbox . '&nbsp;' . $data . '</td>';
            }

            if ($key % 3 == 0 && $key > 0) {
                $htmlcontent .= $htmlcontent_res;
                $htmlcontent .= '</tr><tr>';
            } else {
                $htmlcontent .= $htmlcontent_res;
            }
        }
        $htmlcontent .= '</tr>';
        $htmlcontent .='</table>';
        return $htmlcontent;
    }

    function pdf_loan_guarantee_land_benefit($loan_guarantee_land_benefit, $loan_guarantee_land_benefit_other) {
        //loan_guarantee_land_benefit_list
        $loan_guarantee_land_benefit_list = $this->loan_guarantee_land_model->get_loan_guarantee_land_benefit_list();

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        $htmlcontent = '';
        $htmlcontent .= '<table width="100%">';
        foreach ($loan_guarantee_land_benefit_list as $key => $data) {
            if ($key == $loan_guarantee_land_benefit) {
                if ($key == 4) {
                    $other = '<u class="udotted">'. $loan_guarantee_land_benefit_other .multi_space(1). '</u>';
                } else {
                    $other = multi_dot(10);
                }

                $htmlcontent_res = '<td width="100%">' .multi_space(28) . $html_checkbox . '&nbsp;' . $data . multi_space(2) . $other . '</td>';
            } else {
                if ($key == 4) {
                    $other = multi_dot(10);
                } else {
                    $other = '';
                }

                $htmlcontent_res = '<td width="100%">' .multi_space(28). $html_uncheckbox . '&nbsp;' . $data . $other . '</td>';
            }

            $htmlcontent .= '<tr>';
            $htmlcontent .= $htmlcontent_res;
            $htmlcontent .= '</tr>';
        }

        $htmlcontent .='</table>';
        return $htmlcontent;
    }

    public function pdf() {
        $filename = 'non_loan_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->AddPage();

        $htmlcontent1 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๒</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>';
        $css = "<style>"
                . ".logo{"
                . "float:left;"
                . "}"
                . ".detail-box{"
                . ""
                . "}"
                . "</style>";
        $htmlcontent = "{$css}"
                . "<div class='logo'><img src='assets\images\logo_invoice.png' style='width:80px;'></div>"
                . "<div class='detail-box'>"
                . "<div class='title'>สถาบันบริหารจัดการธนาคารที่ดิน</div>"
                . "<div class='detail'>"
                . "ทีอยู่<br>"
                . "เบอร์ติดต่อ<br>"
                . "</div>"
                . "</div>";
//        echo $htmlcontent;
//        exit;
        ob_start();
        $this->load->library('tcpdf');

        $filename = 'test_loan_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->AddPage();

        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 5, 10, true);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        $pdf->Output($filename . '.pdf', 'I');
    }

    /* บจธ.สช. ๑ */
    public function report_loan($loan_id)
    {
    	include_once('word/class/tbs_class.php');
    	include_once('word/class/tbs_plugin_opentbs.php');

    	$loan_id = $this->uri->segment(3);

    	$TBS = new clsTinyButStrong;
    	$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

    	$loan_model = $this->loan_model->get_by_id($loan_id);
    	$loan_model['loan_amount_thai'] = num2wordsThai($loan_model['loan_amount']);
    	$loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);

    	$person_model = $this->loan_lib->person_detail($loan_model['person_id']);
      $status_marrieds = $this->status_married_model->all();
      $land_type5 = $this->land_type_model->getById(5);


    	if ($loan_model['loan_spouse'])
    	{
    		$spouse_model = $this->loan_lib->person_detail($loan_model['loan_spouse']);
    	}
    	else
    	{
    		$spouse_model = array(
    				'title_name' => ' - ',
    				'person_fname' => ' - ',
    				'person_lname' => ' - ',
    				'person_age' => ' - ',
    				'career_name' => ' - ',
    				'person_addr_pre_no' => ' - ',
    				'person_addr_pre_moo' => ' - ',
    				'person_addr_pre_road' => ' - ',
    				'person_addr_pre_district_name' => ' - ',
    				'person_addr_pre_amphur_name' => ' - ',
    				'person_addr_pre_province_name' => ' - ',
    				'person_phone' => ' - ',
    				'person_mobile' => ' - ',
    				'person_email' => ' - ',
    		);
    	}
      $employee = $this->employee_model->getByUserId(get_uid_login());
      //echo '<pre>';
      //print_r($employee);
    	//echo '</pre>';

      $GLOBALS['user_name'] = $employee?($employee['employee_fname'].' '.$employee['employee_lname']):get_ufullname_login();

      $GLOBALS['position_name'] = $employee?$employee['position_name']:' ';
    	$GLOBALS['loan_location'] = view_value(num2Thai($loan_model['loan_location']));
    	$GLOBALS['loan_datefull'] = view_value(num2Thai($loan_model['loan_datefull']));
    	$GLOBALS['loan_amount'] = view_value(num2Thai($loan_model['loan_amount']));
    	$GLOBALS['loan_amount_text'] = view_value(num2Thai($loan_model['loan_amount_thai']));
    	$GLOBALS['loan_period_year'] = view_value(num2Thai($loan_model['loan_period_year']));
    	$GLOBALS['loan_desc'] = view_value(num2Thai(nl2br($loan_model['loan_desc'])));
    	$GLOBALS['personname'] = $person_model['title_name'].$person_model['person_fname'].' '.$person_model['person_lname'];
    	$GLOBALS['person_birthdate_thai'] = view_value(num2Thai($person_model['person_birthdate_thai']));
    	$GLOBALS['person_age'] = view_value(num2Thai($person_model['person_age']));
    	$GLOBALS['person_sex_list'] = view_value($person_model['sex_name']);
    	$GLOBALS['person_race'] = view_value($person_model['race_name']);
    	$GLOBALS['person_nationality'] = view_value($person_model['nationality_name']);
    	$GLOBALS['person_religion'] = view_value($person_model['religion_name']);
    	$GLOBALS['person_thaiid'] = view_value(num2Thai(formatThaiid($person_model['person_thaiid'])));
    	$GLOBALS['person_card_startdate_thai'] = view_value(num2Thai($person_model['person_card_startdate_thai']));
    	$GLOBALS['person_card_expiredate_thai'] = view_value(num2Thai($person_model['person_card_expiredate_thai']));
    	$GLOBALS['person_addr_card_no'] = num2Thai($person_model['person_addr_card_no']);
    	$GLOBALS['person_addr_card_moo'] = view_value(num2Thai($person_model['person_addr_card_moo']));
    	$GLOBALS['person_addr_card_road'] = view_value(num2Thai($person_model['person_addr_card_road']));
    	$GLOBALS['person_addr_card_district_name'] = view_value(num2Thai($person_model['person_addr_card_district_name']));
    	$GLOBALS['person_addr_card_amphur_name'] = view_value(num2Thai($person_model['person_addr_card_amphur_name']));
    	$GLOBALS['person_addr_card_province_name'] = view_value(num2Thai($person_model['person_addr_card_province_name']));
      $GLOBALS['person_pre_1'] = $person_model['person_addr_same'] ? '(√)' : '()';
      $GLOBALS['person_pre_2'] = $person_model['person_addr_same'] ? '()' : '(√)';
    	$GLOBALS['person_pre_card_no'] = view_value(num2Thai($person_model['person_addr_pre_no']));
    	$GLOBALS['person_pre_card_moo'] = view_value(num2Thai($person_model['person_addr_pre_moo']));
    	$GLOBALS['person_pre_card_road'] = view_value(num2Thai($person_model['person_addr_pre_road']));
    	$GLOBALS['person_pre_card_district_name'] = view_value(num2Thai($person_model['person_addr_pre_district_name']));
    	$GLOBALS['person_pre_card_amphur_name'] = view_value(num2Thai($person_model['person_addr_pre_amphur_name']));
    	$GLOBALS['person_pre_card_province_name'] = view_value(num2Thai($person_model['person_addr_pre_province_name']));
    	$GLOBALS['person_phone'] = view_value(num2Thai($person_model['person_phone']));
    	$GLOBALS['person_mobile'] = view_value(num2Thai($person_model['person_mobile']));
    	$GLOBALS['person_email'] = view_value($person_model['person_email']);
    	$GLOBALS['person_land_no'] = view_value(num2Thai($person_model['person_land_no']));
    	$GLOBALS['person_land_area'] = empty($person_model['person_land_area_rai'])? ' - ' :
            view_value(num2Thai($person_model['person_land_area_rai'].' - '.$person_model['person_land_area_ngan'].' - '.$person_model['person_land_area_wah']));
      $person_land_district = $this->district_model->getById($person_model['person_land_addr_district_id']);
      $GLOBALS['person_land_district'] = view_value($person_land_district['district_name']);
      $person_land_amphur = $this->amphur_model->getById($person_model['person_land_addr_amphur_id']);
    	$GLOBALS['person_land_amphur'] = view_value($person_land_amphur['amphur_name']);
      $person_land_province = $this->province_model->getById($person_model['person_land_addr_province_id']);
    	$GLOBALS['person_land_province'] = view_value($person_land_province['province_name']);
    	$GLOBALS['check_career_farm'] = '(√)';
    	$GLOBALS['check_career_other'] = '( )';
    	$GLOBALS['person_career_name'] = view_value($person_model['career_name'].$person_model['person_career_other']);
    	$GLOBALS['person_career_other_name'] = '';

      if($person_model['person_address_type'] == 1){
          $GLOBALS['person_address_type1'] = '√';
      }else{
          $GLOBALS['person_address_type1'] = ' ';
      }
      if($person_model['person_address_type'] == 2){
          $GLOBALS['person_address_type2'] = '√';
      }else{
          $GLOBALS['person_address_type2'] = ' ';
      }
      $GLOBALS['person_address_type_other'] = ' - ';
      if($person_model['person_address_type'] == 3){
          $GLOBALS['person_address_type3'] = '√';
          $GLOBALS['person_address_type_other'] = $person_model['person_address_type_other']?$person_model['person_address_type_other']:' ';
      }else{
          $GLOBALS['person_address_type3'] = ' ';
          $GLOBALS['person_address_type_other'] = ' - ';
      }

      if($person_model['person_land_type_id'] == 1){
          $GLOBALS['person_land_type1'] = '√';
      }else{
          $GLOBALS['person_land_type1'] = ' ';
      }

      if($person_model['person_land_type_id'] == 2){
          $GLOBALS['person_land_type2'] = '√';
          $GLOBALS['person_land_no2'] = view_value(num2Thai($person_model['person_land_no']));
          $GLOBALS['person_land_area2'] = empty($person_model['person_land_area_rai'])? ' - ' :
                view_value(num2Thai($person_model['person_land_area_rai'].' - '.$person_model['person_land_area_ngan'].' - '.$person_model['person_land_area_wah']));

      }else{
          $GLOBALS['person_land_type2'] = ' ';
          $GLOBALS['person_land_no2'] = ' - ';
          $GLOBALS['person_land_area2'] = ' - ';
      }

      if($person_model['person_land_type_id'] == 3){
          $GLOBALS['person_land_type3'] = '√';
          $GLOBALS['person_land_no3'] = view_value(num2Thai($person_model['person_land_no']));
          $GLOBALS['person_land_area3'] = empty($person_model['person_land_area_rai'])? ' - ' :
                view_value(num2Thai($person_model['person_land_area_rai'].' - '.$person_model['person_land_area_ngan'].' - '.$person_model['person_land_area_wah']));
      }else{
          $GLOBALS['person_land_type3'] = ' ';
          $GLOBALS['person_land_no3'] = ' - ';
          $GLOBALS['person_land_area3'] = ' - ';
      }

      if($person_model['person_land_type_id'] == 5){
          $GLOBALS['person_land_type5'] = '√';
          $GLOBALS['person_land_no5'] = view_value(num2Thai($person_model['person_land_no']));
          $GLOBALS['person_land_area5'] = empty($person_model['person_land_area_rai'])? ' - ' :
                view_value(num2Thai($person_model['person_land_area_rai'].' - '.$person_model['person_land_area_ngan'].' - '.$person_model['person_land_area_wah']));
          $GLOBALS['person_land_type5_name']  = $land_type5?$land_type5['land_type_name']:' - ';
      }else{
          $GLOBALS['person_land_type5'] = ' ';
          $GLOBALS['person_land_no5'] = ' - ';
          $GLOBALS['person_land_area5'] = ' - ';
          $GLOBALS['person_land_type5_name']  = '';
      }


      $status_text = '';
      foreach ($status_marrieds as $key => $value) {
         if($value['status_married_id'] == $person_model['status_married_id']){
           $status_text .= '(√) '.$value['status_married_name'].'  ';
         }else{
           $status_text .= '( ) '.$value['status_married_name'].'  ';
         }
      }

      $GLOBALS['person_married_list'] = view_value($status_text);

    	$GLOBALS['spouse_name'] = $spouse_model['title_name'] . $spouse_model['person_fname'] . ' ' . $spouse_model['person_lname'];
    	$GLOBALS['spouse_age'] = view_value(num2Thai($spouse_model['person_age']));
    	$GLOBALS['spouse_career'] = view_value($spouse_model['career_name']);
    	$GLOBALS['spouse_addr_card_no'] = view_value(num2Thai($spouse_model['person_addr_pre_no']));
    	$GLOBALS['spouse_addr_card_moo'] = view_value(num2Thai($spouse_model['person_addr_pre_moo']));
    	$GLOBALS['spouse_addr_card_road'] = view_value(num2Thai($spouse_model['person_addr_pre_road']));
    	$GLOBALS['spouse_addr_card_district_name'] = view_value(num2Thai($spouse_model['person_addr_pre_district_name']));
    	$GLOBALS['spouse_addr_card_amphur_name'] = view_value(num2Thai($spouse_model['person_addr_pre_amphur_name']));
    	$GLOBALS['spouse_addr_card_province_name'] = view_value(num2Thai($spouse_model['person_addr_pre_province_name']));
    	$GLOBALS['spouse_mobile'] = view_value(num2Thai($spouse_model['person_mobile']));
    	$GLOBALS['spouse_phone'] = view_value(num2Thai($spouse_model['person_phone']));
    	$GLOBALS['spouse_email'] = view_value(num2Thai($spouse_model['person_email']));
    	$GLOBALS['loan_member_amount'] = view_value(num2Thai($loan_model['loan_member_amount']));
    	$GLOBALS['loan_labor_amount'] = view_value(num2Thai($loan_model['loan_labor_amount']));

    	$loan_type_model = $this->loan_lib->loan_type_by_loan_id($loan_id);

    	$config_land_type = $this->mst_model->get_all('land_type');

    	$html_land_type = '';
    	foreach ($config_land_type as $key_land_type => $data_land_type)
    	{
    		$html_land_type .= '( ) '.$data_land_type['land_type_name'].'  ';
    	}

    	$config_building_type = $this->mst_model->get_all('building_type');

    	$html_building = '';
    	foreach ($config_building_type as $key_building => $data_building)
    	{
    		$html_building .= '( ) '.$data_building['building_type_name'].'  ';
    	}

    	$case_type_list = $this->loan_type_model->get_case_type();

    	$type_3_type = '';
    	//foreach ($case_type_list as $key_case => $data_case)
    	//{
    	//	$type_3_type .= '( ) '.$data_case.' ';
    	//}

    	$type_1_check = '( )';
    	$type_1_type = ' - ';
    	$type_1_case = ' - ';

    	$type_2_check = '( )';
    	$building_2_type = $html_building;
    	$type_2_objective = ' - ';

    	$type_3_check = '( )';
    	$building_3_type = $html_building;

    	$type_4_check = '( )';

    	$type_5_check = '( )';
    	$type_5_objective = ' - ';
    	$type_5_for = ' - ';
    	$type_5_location = ' - ';
    	$type_5_project = ' - ';
    	$type_5_expenditure = ' - ';
    	$type_5_income_year = ' - ';
    	$type_5_income_per = ' - ';
    	$type_5_payment_year = ' - ';
    	$type_5_payment_amount = ' - ';
    	$type_5_year_amount = ' - ';

      $land1  = array();
      $land_building1  = array();

      $land2  = array();
      $land_building2  = array();

      $land3  = array();
      $land_building3  = array();

      $land4  = array();
      $land_building4  = array();

      //echo '<pre>';
   	  //print_r($loan_type_model);
    	//echo '</pre>';
    	foreach ($loan_type_model as $key => $data)
    	{

    		if ($data['loan_objective_id']==1)
    		{
    			$type_1_check = '(√)';

    			$redeem_type_list = $this->loan_type_model->get_redeem_type();

    			$type_1_type_check = '';
    			$type_1_type = '';
    			foreach ($redeem_type_list as $key_redeem => $data_redeem)
    			{
    				if ($key_redeem == $data['loan_type_redeem_type'])
    				{
    					$type_1_type .= '(√) '.$data_redeem.' ';
    				}
    				else
    				{
    					$type_1_type .= '( ) '.$data_redeem.' ';
    				}
    			}

    			$type_1_case = view_value(num2Thai(nl2br($data['loan_type_redeem_case'])));


          foreach ($data['land_list'] as $key => $value) {
            $html_land_type_check = '';
            foreach ($config_land_type as $key_land_type => $data_land_type)
            {
              if ($data_land_type['land_type_id'] == $value['land_type_id'])
              {
                $html_land_type_check .= '(√) '.$data_land_type['land_type_name'].'  ';
              }
              else
              {
                $html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
              }
            }
            if(empty($value['building_list'][0]))
            {
              $land1[] = array(
              'land_type' =>  $html_land_type_check,
              'land_no' => view_value(num2Thai($value['land_no'])),
              'land_addr_no' =>  view_value(num2Thai($value['land_addr_no'])),
              'land_district' => view_value($value['land_addr_district_name']),
              'land_amphur' => view_value($value['land_addr_amphur_name']),
              'land_province' => view_value($value['land_addr_province_name']),
              'land_rai' => view_value(num2Thai($value['land_area_rai'])),
              'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
              'land_wah' => view_value(num2Thai($value['land_area_wah'])),
              'type_amount' => view_value(num2Thai($data['loan_type_redeem_amount'])),
              'type_amount_text' => view_value($data['loan_type_redeem_amount_thai']),
              'type_owner' => view_value($data['loan_type_redeem_owner_fullname']),
              'type_age' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_age'])),
              'type_home_no' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_addr_pre_no'])),
              'type_home_moo' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_addr_pre_moo'])),
              'type_home_road' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_road']),
              'type_home_district' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_district_name']),
              'type_home_amphur' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_amphur_name']),
              'type_home_province' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_province_name']),
              'type_phone' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_phone'])),
              'type_mobile' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_mobile'])),
              'type_relation' => view_value($data['relationship_name'])
            );

            }else  {
              $html_building = '';
              foreach ($config_building_type as $key_building => $data_building)
              {
                if ($data_building['building_type_id'] == $value['building_list'][0]['building_type_id'])
                {
                  $html_building .= '(√) '.$data_building['building_type_name'].'  ';
                }
                else
                {
                  $html_building .= '( ) '.$data_building['building_type_name'].'  ';
                }
              }

              // echo "<pre>";
              // print_r($value['building_list']);
              // echo "</pre>";
              $land_building1[] = array(
                'building_type' => $html_building,
                'land_type' => $html_land_type_check,
                'land_no' => view_value(num2Thai($value['land_no'])),
                'land_addr_no' => view_value(num2Thai($value['land_addr_no'])),
                'land_district' => view_value($value['land_addr_district_name']),
                'land_amphur' => view_value($value['land_addr_amphur_name']),
                'land_province' => view_value($value['land_addr_province_name']),
                'land_rai' => view_value(num2Thai($value['land_area_rai'])),
                'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
                'land_wah' => view_value(num2Thai($value['land_area_wah'])),
                'building_no' => view_value(num2Thai($value['building_list'][0]['building_no'])),
                'building_name' => view_value(num2Thai($value['building_list'][0]['building_name'])),
                'building_moo' => view_value(num2Thai($value['building_list'][0]['building_moo'])),
                'building_road' => view_value(num2Thai($value['building_list'][0]['building_road'])),
                'building_district' => view_value(isset($value['building_list'][0]['building_district_name'])?$value['building_list'][0]['building_district_name']:''),
                'building_amphur' => view_value($value['building_list'][0]['building_amphur_name']),
                'building_province' => view_value($value['building_list'][0]['building_province_name']),
                'type_amount' => view_value(num2Thai($data['loan_type_redeem_amount'])),
                'type_amount_text' => view_value($data['loan_type_redeem_amount_thai']),
                'type_owner' => view_value($data['loan_type_redeem_owner_fullname']),
                'type_age' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_age'])),
                'type_home_no' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_addr_pre_no'])),
                'type_home_moo' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_addr_pre_moo'])),
                'type_home_road' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_road']),
                'type_home_district' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_district_name']),
                'type_home_amphur' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_amphur_name']),
                'type_home_province' => view_value($data['loan_type_redeem_owner_info']['person_addr_pre_province_name']),
                'type_phone' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_phone'])),
                'type_mobile' => view_value(num2Thai($data['loan_type_redeem_owner_info']['person_mobile'])),
                'type_relation' => view_value($data['relationship_name'])
              );

            }

          }
    		}
    		else if ($data['loan_objective_id'] == 2)
    		{
    			$type_2_check = '(√)';

    			$type_objective = $data['loan_objective_name'];

    			$html_land_type_check = '';
    			foreach ($config_land_type as $key_land_type => $data_land_type)
    			{
            if(!empty($data['land_list'])){
        				if ($data_land_type['land_type_id'] == $data['land_list'][0]['land_type_id'])
        				{
        					$html_land_type_check .= '(√) '.$data_land_type['land_type_name'].'  ';
        				}
        				else
        				{
        					$html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
        				}
              }else{
                $html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
              }
    			}
          foreach ($data['land_list'] as $key => $value) {
            $html_land_type_check = '';
            foreach ($config_land_type as $key_land_type => $data_land_type)
            {
              if ($data_land_type['land_type_id'] == $value['land_type_id'])
              {
                $html_land_type_check .= '(√) '.$data_land_type['land_type_name'].'  ';
              }
              else
              {
                $html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
              }
            }
            if(empty($value['building_list'][0]))
            {

              $land2[] = array(
              'land_type' =>  $html_land_type_check,
              'land_no' => view_value(num2Thai($value['land_no'])),
              'land_addr_no' =>  view_value(num2Thai($value['land_addr_no'])),
              'land_district' => view_value($value['land_addr_district_name']),
              'land_amphur' => view_value($value['land_addr_amphur_name']),
              'land_province' => view_value($value['land_addr_province_name']),
              'land_rai' => view_value(num2Thai($value['land_area_rai'])),
              'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
              'land_wah' => view_value(num2Thai($value['land_area_wah'])),
              'type_amount' => view_value(num2Thai($data['loan_type_contract_amount'])),
              'type_amount_text' => view_value($data['loan_type_contract_amount_thai']),
      				'type_no' => view_value($data['loan_type_contract_no']),
      				'type_date' => view_value(num2Thai(thai_display_date($data['loan_type_contract_date']))),
      				'type_creditor' => view_value($data['loan_type_contract_creditor_fullname']),
      				'type_borrowers' => view_value($data['loan_type_contract_borrowers_fullname']),
              'type_objective' => view_value($type_objective)

            );

            }else  {
              $html_building = '';
              foreach ($config_building_type as $key_building => $data_building)
              {
                if ($data_building['building_type_id'] == $value['building_list'][0]['building_type_id'])
                {
                  $html_building .= '(√) '.$data_building['building_type_name'].'  ';
                }
                else
                {
                  $html_building .= '( ) '.$data_building['building_type_name'].'  ';
                }
              }

              $land_building2[] = array(
                'building_type' => $html_building,
                'land_type' => $html_land_type_check,
                'land_no' => view_value(num2Thai($value['land_no'])),
                'land_addr_no' => view_value(num2Thai($value['land_addr_no'])),
                'land_district' => view_value($value['land_addr_district_name']),
                'land_amphur' => view_value($value['land_addr_amphur_name']),
                'land_province' => view_value($value['land_addr_province_name']),
                'land_rai' => view_value(num2Thai($value['land_area_rai'])),
                'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
                'land_wah' => view_value(num2Thai($value['land_area_wah'])),
                'building_no' => view_value(num2Thai($value['building_list'][0]['building_no'])),
                'building_name' => view_value(num2Thai($value['building_list'][0]['building_name'])),
                'building_moo' => view_value(num2Thai($value['building_list'][0]['building_moo'])),
                'building_road' => view_value(num2Thai($value['building_list'][0]['building_road'])),
                'building_district' => view_value($value['building_list'][0]['building_district_name']),
                'building_amphur' => view_value($value['building_list'][0]['building_amphur_name']),
                'building_province' => view_value($value['building_list'][0]['building_province_name']),
                'type_amount' => view_value(num2Thai($data['loan_type_contract_amount'])),
                'type_amount_text' => view_value($data['loan_type_contract_amount_thai']),
                'type_no' => view_value($data['loan_type_contract_no']),
        				'type_date' => view_value(num2Thai(thai_display_date($data['loan_type_contract_date']))),
        				'type_creditor' => view_value($data['loan_type_contract_creditor_fullname']),
        				'type_borrowers' => view_value($data['loan_type_contract_borrowers_fullname']),
                'type_objective' => view_value($type_objective)

              );

            }

          }

    		}
    		else if ($data['loan_objective_id'] == 3)
    		{
    			$type_3_check = '(√)';

    			$type_objective = $data['loan_objective_name'];

    			$type_3_type_check = '';
    			foreach ($case_type_list as $key_case => $data_case)
    			{
    				if($key_case == $data['loan_type_case_type'])
    				{
    					$type_3_type_check .= '(√) '.$data_case.' ';
    				}
    				else
    				{
    					$type_3_type_check .= '( ) '.$data_case.' ';
    				}
    			}


          foreach ($data['land_list'] as $key => $value) {
            $html_land_type_check = '';
            foreach ($config_land_type as $key_land_type => $data_land_type)
            {
              if ($data_land_type['land_type_id'] == $value['land_type_id'])
              {
                $html_land_type_check .= '(√) '.$data_land_type['land_type_name'].'  ';
              }
              else
              {
                $html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
              }
            }

            if(empty($value['building_list'][0]))
            {

              $land3[] = array(
              'land_type' =>  $html_land_type_check,
              'land_no' => view_value(num2Thai($value['land_no'])),
              'land_addr_no' =>  view_value(num2Thai($value['land_addr_no'])),
              'land_district' => view_value($value['land_addr_district_name']),
              'land_amphur' => view_value($value['land_addr_amphur_name']),
              'land_province' => view_value($value['land_addr_province_name']),
              'land_rai' => view_value(num2Thai($value['land_area_rai'])),
              'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
              'land_wah' => view_value(num2Thai($value['land_area_wah'])),
              'type_black' => view_value(num2Thai($data['loan_type_case_black_no'])),
      				'type_red' => view_value(num2Thai($data['loan_type_case_red_no'])),
      				'type_court' => view_value(num2Thai($data['loan_type_case_court'])),
      				'type_date' => view_value(num2Thai(thai_display_date($data['loan_type_case_date']))),
              'type_type' => $type_3_type_check,
              'type_plaintiff' => view_value(num2Thai($data['loan_type_case_plaintiff_fullname'])),
      				'type_defendant' => view_value(num2Thai($data['loan_type_case_defendant_fullname'])),
      				'type_comment' => view_value(num2Thai(nl2br($data['loan_type_case_comment']))),
      				'type_amount' => view_value(num2Thai($data['loan_type_case_amount'])),
      				'type_amount_text' => view_value($data['loan_type_case_amount_thai']),
              'type_objective' => view_value($type_objective)

            );

            }else  {
              $html_building = '';
              foreach ($config_building_type as $key_building => $data_building)
              {
                if ($data_building['building_type_id'] == $value['building_list'][0]['building_type_id'])
                {
                  $html_building .= '(√) '.$data_building['building_type_name'].'  ';
                }
                else
                {
                  $html_building .= '( ) '.$data_building['building_type_name'].'  ';
                }
              }


              $land_building3[] = array(
                'building_type' => $html_building,
                'land_type' => $html_land_type_check,
                'land_no' => view_value(num2Thai($value['land_no'])),
                'land_addr_no' => view_value(num2Thai($value['land_addr_no'])),
                'land_district' => view_value($value['land_addr_district_name']),
                'land_amphur' => view_value($value['land_addr_amphur_name']),
                'land_province' => view_value($value['land_addr_province_name']),
                'land_rai' => view_value(num2Thai($value['land_area_rai'])),
                'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
                'land_wah' => view_value(num2Thai($value['land_area_wah'])),
                'building_no' => view_value(num2Thai($value['building_list'][0]['building_no'])),
                'building_name' => view_value(num2Thai($value['building_list'][0]['building_name'])),
                'building_moo' => view_value(num2Thai($value['building_list'][0]['building_moo'])),
                'building_road' => view_value(num2Thai($value['building_list'][0]['building_road'])),
                'building_district' => view_value($value['building_list'][0]['building_district_name']),
                'building_amphur' => view_value($value['building_list'][0]['building_amphur_name']),
                'building_province' => view_value($value['building_list'][0]['building_province_name']),
                'type_black' => view_value(num2Thai($data['loan_type_case_black_no'])),
        				'type_red' => view_value(num2Thai($data['loan_type_case_red_no'])),
        				'type_court' => view_value(num2Thai($data['loan_type_case_court'])),
        				'type_date' =>  view_value(num2Thai(thai_display_date($data['loan_type_case_date']))),
                'type_type' => $type_3_type_check,
                'type_plaintiff' => view_value(num2Thai($data['loan_type_case_plaintiff_fullname'])),
        				'type_defendant' => view_value(num2Thai($data['loan_type_case_defendant_fullname'])),
        				'type_comment' => view_value(num2Thai(nl2br($data['loan_type_case_comment']))),
        				'type_amount' => view_value(num2Thai($data['loan_type_case_amount'])),
        				'type_amount_text' => view_value($data['loan_type_case_amount_thai']),
                'type_objective' => view_value($type_objective)

              );

            }

          }

    		}
    		else if ($data['loan_objective_id'] == 4)
    		{
    			$type_4_check = '(√)';

    			$type_objective = $data['loan_objective_name'];

          foreach ($data['land_list'] as $key => $value) {
            $html_land_type_check = '';
            foreach ($config_land_type as $key_land_type => $data_land_type)
            {
              if ($data_land_type['land_type_id'] == $value['land_type_id'])
              {
                $html_land_type_check .= '(√) '.$data_land_type['land_type_name'].'  ';
              }
              else
              {
                $html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
              }
            }
            if(empty($value['building_list'][0]))
            {

              $land4[] = array(
              'land_type' =>  $html_land_type_check,
              'land_no' => view_value(num2Thai($value['land_no'])),
              'land_addr_no' =>  view_value(num2Thai($value['land_addr_no'])),
              'land_district' => view_value($value['land_addr_district_name']),
              'land_amphur' => view_value($value['land_addr_amphur_name']),
              'land_province' => view_value($value['land_addr_province_name']),
              'land_rai' => view_value(num2Thai($value['land_area_rai'])),
              'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
              'land_wah' => view_value(num2Thai($value['land_area_wah'])),
	            'type_date' =>  view_value(num2Thai(thai_display_date($data['loan_type_sale_date']))),
      				'type_amount' => view_value(num2Thai($data['loan_type_sale_amount'])),
      				'type_amount_text' => view_value($data['loan_type_sale_amount_thai']),
              'type_objective' => view_value($type_objective)

            );

            }else  {
              $html_building = '';
              foreach ($config_building_type as $key_building => $data_building)
              {
                if ($data_building['building_type_id'] == $value['building_list'][0]['building_type_id'])
                {
                  $html_building .= '(√) '.$data_building['building_type_name'].'  ';
                }
                else
                {
                  $html_building .= '( ) '.$data_building['building_type_name'].'  ';
                }
              }

              $land_building4[] = array(
                'building_type' => $html_building,
                'land_type' => $html_land_type_check,
                'land_no' => view_value(num2Thai($value['land_no'])),
                'land_addr_no' => view_value(num2Thai($value['land_addr_no'])),
                'land_district' => view_value($value['land_addr_district_name']),
                'land_amphur' => view_value($value['land_addr_amphur_name']),
                'land_province' => view_value($value['land_addr_province_name']),
                'land_rai' => view_value(num2Thai($value['land_area_rai'])),
                'land_ngan' => view_value(num2Thai($value['land_area_ngan'])),
                'land_wah' => view_value(num2Thai($value['land_area_wah'])),
                'building_no' => view_value(num2Thai($value['building_list'][0]['building_no'])),
                'building_name' => view_value(num2Thai($value['building_list'][0]['building_name'])),
                'building_moo' => view_value(num2Thai($value['building_list'][0]['building_moo'])),
                'building_road' => view_value(num2Thai($value['building_list'][0]['building_road'])),
                'building_district' => view_value($value['building_list'][0]['building_district_name']),
                'building_amphur' => view_value($value['building_list'][0]['building_amphur_name']),
                'building_province' => view_value($value['building_list'][0]['building_province_name']),
                'type_date' =>view_value(num2Thai(thai_display_date($data['loan_type_sale_date']))),
        				'type_amount' => view_value(num2Thai($data['loan_type_sale_amount'])),
        				'type_amount_text' => view_value($data['loan_type_sale_amount_thai']),
                'type_objective' => view_value($type_objective)

              );

            }

          }


    		}
    		else if ($data['loan_objective_id'] == 5)
    		{
    			$type_5_check = '(√)';

    			$type_5_objective = $data['loan_objective_name'];
    			$type_5_for = view_value(num2Thai(nl2br($data['loan_type_farm_for'])));
    			$type_5_location = view_value(num2Thai(nl2br($data['loan_type_farm_location'])));
    			$type_5_project = view_value(num2Thai(nl2br($data['loan_type_farm_project'])));
    			$type_5_expenditure = view_value(num2Thai(nl2br($data['loan_type_farm_expenditure'])));
    			$type_5_income_year = view_value(num2Thai($data['loan_type_farm_income_yearstart']));
    			$type_5_income_per = view_value(num2Thai($data['loan_type_farm_income_per_year']));
    			$type_5_payment_year = view_value(num2Thai($data['loan_type_farm_payment_yearstart']));
    			$type_5_payment_amount = view_value(num2Thai($data['loan_type_farm_payment_amount']));
    			$type_5_year_amount = view_value(num2Thai($data['loan_type_farm_year_amount']));
    		}
    	}

    	$GLOBALS['type_1_check'] = $type_1_check;
    	$GLOBALS['type_1_type'] = $type_1_type;
    	$GLOBALS['type_1_case'] = $type_1_case;

    	$GLOBALS['type_2_check'] = $type_2_check;
    	$GLOBALS['type_2_objective'] = $type_2_objective;
    	$GLOBALS['type_3_check'] = $type_3_check;
    	$GLOBALS['type_4_check'] = $type_4_check;

    	$GLOBALS['type_5_check'] = $type_5_check;
    	$GLOBALS['type_5_objective'] = $type_5_objective;
    	$GLOBALS['type_5_for'] = $type_5_for;
    	$GLOBALS['type_5_location'] = $type_5_location;
    	$GLOBALS['type_5_project'] = $type_5_project;
    	$GLOBALS['type_5_expenditure'] = $type_5_expenditure;
    	$GLOBALS['type_5_income_year'] = $type_5_income_year;
    	$GLOBALS['type_5_income_per'] = $type_5_income_per;
    	$GLOBALS['type_5_payment_year'] = $type_5_payment_year;
    	$GLOBALS['type_5_payment_amount'] = $type_5_payment_amount;
    	$GLOBALS['type_5_year_amount'] = $type_5_year_amount;

    	$loan_doc_model = $this->loan_doc_model->get_by_opt(array('loan_id' => $loan_id));

    	$check_doc_1 = '( )'; //บัตรประชาชน
    	$check_doc_2 = '( )'; //ทะเบียนบ้าน
    	$check_doc_3 = '( )'; //สิทธิในที่ดิน
    	$check_doc_4 = '( )'; //กู้ยืมเงิน
    	$check_doc_5 = '( )'; //จำนอง
    	$check_doc_6 = '( )'; //ขายฝาก
    	$check_doc_7 = '( )'; //เช่าที่ดิน
    	$check_doc_8 = '( )'; //คำฟ้อง
    	$check_doc_9 = '( )'; //คำพิพากษา
    	$check_doc_10 = '( )'; //คำสั่งของกรมบังคับคดี
    	$check_doc_11 = '( )'; //ประเมินราคาที่ดิน
    	$check_doc_12 = '( )'; //ขยายระยะเวลา
    	$check_doc_13 = '( )'; //อื่นๆ

    	$doc_3_desc = ' - ';
    	$doc_4_date = ' - ';
    	$doc_5_date = ' - ';
    	$doc_6_date = ' - ';
    	$doc_7_date = ' - ';
    	$doc_8_black = ' - ';
    	$doc_8_court = ' - ';
    	$doc_8_date = ' - ';
    	$doc_9_black = ' - ';
    	$doc_9_red = ' - ';
    	$doc_9_court = ' - ';
    	$doc_9_date = ' - ';
    	$doc_10_date = ' - ';
    	$doc_13_desc = ' - ';

    	if($loan_doc_model)
    	{
    		foreach ($loan_doc_model as $loan_doc)
    		{
    			if($loan_doc['loan_doc_type_id']=='1')
    			{
    				$check_doc_1 = '(√)';
    			}
    			else if($loan_doc['loan_doc_type_id']=='2')
    			{
    				$check_doc_2 = '(√)';
    			}
    			else if($loan_doc['loan_doc_type_id']=='3')
    			{
    				$check_doc_3 = '(√)';
            $doc_3_desc = num2Thai($loan_doc['loan_doc_desc']);
    			}
    			else if($loan_doc['loan_doc_type_id']=='4')
    			{
    				$check_doc_4 = '(√)';
    				$doc_4_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
    			}
    			else if($loan_doc['loan_doc_type_id']=='5')
    			{
    				$check_doc_5 = '(√)';
    				$doc_5_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
    			}
    			else if($loan_doc['loan_doc_type_id']=='6')
    			{
    				$check_doc_6 = '(√)';
    				$doc_6_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
    			}
    			else if($loan_doc['loan_doc_type_id']=='7')
    			{
    				$check_doc_7 = '(√)';
    				$doc_7_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
    			}
    			else if($loan_doc['loan_doc_type_id']=='8')
    			{
    				$check_doc_8 = '(√)';
    				$doc_8_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
            $doc_8_black = num2Thai($loan_doc['loan_doc_black_no']);
          	$doc_8_court = num2Thai($loan_doc['loan_doc_court']);
    			}
    			else if($loan_doc['loan_doc_type_id']=='9')
    			{
    				$check_doc_9 = '(√)';
    				$doc_9_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
            $doc_9_black = num2Thai($loan_doc['loan_doc_black_no']);
          	$doc_9_red = num2Thai($loan_doc['loan_doc_red_no']);
          	$doc_9_court = num2Thai($loan_doc['loan_doc_court']);
    			}
    			else if($loan_doc['loan_doc_type_id']=='10')
    			{
    				$check_doc_10 = '(√)';
    				$doc_10_date = view_value(num2Thai(thai_display_date($loan_doc['loan_doc_date'])));
    			}
    			else if($loan_doc['loan_doc_type_id']=='11')
    			{
    				$check_doc_11 = '(√)';
    			}
    			else if($loan_doc['loan_doc_type_id']=='12')
    			{
    				$check_doc_12 = '(√)';
    			}
    			else if($loan_doc['loan_doc_type_id']=='13')
    			{
    				$check_doc_13 = '(√)';
            $doc_13_desc = num2Thai($loan_doc['loan_doc_type_other']);
    			}
    		}
    	}

    	$GLOBALS['check_doc_1'] = $check_doc_1;
    	$GLOBALS['check_doc_2'] = $check_doc_2;
    	$GLOBALS['check_doc_3'] = $check_doc_3;
    	$GLOBALS['check_doc_4'] = $check_doc_4;
    	$GLOBALS['check_doc_5'] = $check_doc_5;
    	$GLOBALS['check_doc_6'] = $check_doc_6;
    	$GLOBALS['check_doc_7'] = $check_doc_7;
    	$GLOBALS['check_doc_8'] = $check_doc_8;
    	$GLOBALS['check_doc_9'] = $check_doc_9;
    	$GLOBALS['check_doc_10'] = $check_doc_10;
    	$GLOBALS['check_doc_11'] = $check_doc_11;
    	$GLOBALS['check_doc_12'] = $check_doc_12;
    	$GLOBALS['check_doc_13'] = $check_doc_13;

    	$GLOBALS['doc_3_desc'] = $doc_3_desc;
    	$GLOBALS['doc_4_date'] = $doc_4_date;
    	$GLOBALS['doc_5_date'] = $doc_5_date;
    	$GLOBALS['doc_6_date'] = $doc_6_date;
    	$GLOBALS['doc_7_date'] = $doc_7_date;
    	$GLOBALS['doc_8_black'] = $doc_8_black;
    	$GLOBALS['doc_8_court'] = $doc_8_court;
    	$GLOBALS['doc_8_date'] = $doc_8_date;
    	$GLOBALS['doc_9_black'] = $doc_9_black;
    	$GLOBALS['doc_9_red'] = $doc_9_red;
    	$GLOBALS['doc_9_court'] = $doc_9_court;
    	$GLOBALS['doc_9_date'] = $doc_9_date;
    	$GLOBALS['doc_10_date'] = $doc_10_date;
    	$GLOBALS['doc_13_desc'] = $doc_13_desc;

    	$loan_income_model = $this->loan_lib->loan_income_report($loan_model['person_id']);

    	$y_curr = date('Y') + 542;

    	$data_income = array(
    			'1' => array(),
    			'2' => array(),
    			'3' => array()
    		);

    	if($loan_income_model)
    	{
    		foreach ($loan_income_model[$y_curr] as $key => $data)
    		{
    			if($data)
    			{
    				foreach ($data as $key_income2 => $data_income2)
    				{
    					$income_id = count($data_income[$key]) + 1;
    					$data_income[$key][] = array(
					    			'income_id' => num2Thai($income_id),
					    			'income_name' => view_value($data_income2['loan_activity_name']),
					    			'income_amount' => view_value(num2Thai($data_income2['loan_activity_income'])),
                    'loan_income_amount' => $data_income2['loan_activity_income_num'],
					    	);
    				}
    			}
    		}
    	}

    	$income =  array(
    			'income_id' => num2Thai(1),
    			'income_name' => ' - ',
    			'income_amount' => ' - ',
          'loan_income_amount' => 0
    	);

    	if(empty($data_income['1'])) $data_income['1'][] = $income;
    	if(empty($data_income['2'])) $data_income['2'][] = $income;
    	if(empty($data_income['3'])) $data_income['3'][] = $income;

    	$GLOBALS['income_year'] = view_value(num2Thai($y_curr));

    	$loan_expenditure_model = $this->loan_lib->loan_expenditure_report($loan_model['person_id']);


    	$data_expenditure = array(
    			'1' => array(),
    			'2' => array(),
    			'3' => array(),
    			'4' => array()
    	);


    	if($loan_expenditure_model)
    	{
    		foreach ($loan_expenditure_model[$y_curr] as $key => $data)
    		{
    			if($data)
    			{
    				foreach ($data as $key_expenditure2 => $data_expenditure2)
    				{
    					$expenditure_id = count($data_expenditure[$key]) + 1;
    					$data_expenditure[$key][] = array(
    							'expenditure_id' => num2Thai($expenditure_id),
    							'expenditure_name' => view_value($data_expenditure2['loan_expenditure_name']),
    							'expenditure_amount' => view_value(num2Thai($data_expenditure2['loan_expenditure_amount'])),
                  'loan_expenditure_amount' => $data_expenditure2['loan_expenditure_amount_num'],

    					);
    				}
    			}
    		}
    	}

    	$expenditure =  array(
    			'expenditure_id' => num2Thai(1),
    			'expenditure_name' => ' - ',
    			'expenditure_amount' => ' - ',
          'loan_expenditure_amount' => 0
    	);

    	if(empty($data_expenditure['1'])) $data_expenditure['1'][] = $expenditure;
    	if(empty($data_expenditure['2'])) $data_expenditure['2'][] = $expenditure;
    	if(empty($data_expenditure['3'])) $data_expenditure['3'][] = $expenditure;
    	if(empty($data_expenditure['4'])) $data_expenditure['4'][] = $expenditure;

    	$GLOBALS['expenditure_year'] = view_value(num2Thai($y_curr));


      //land by loaner and spouse + land occupy
      $loan_model = $_SESSION['session_loan']['loan_model'];

      $owner_id = $loan_model['person_id'];
      $spouse_id = $loan_model['loan_spouse'];
      $lands = array();
      $land_owner_model = $this->land_owner_model->get_by_opt(array('person_id' => $owner_id));
      if ($land_owner_model) {

          foreach ($land_owner_model as $key_land => $land_owner) {
              $person = $this->loan_lib->person_detail($land_owner['person_id']);
              $land = $this->land_model->get_by_id($land_owner['land_id']);
              $land_detail = $this->loan_lib->land_detail($land);
              $benefit = $land_detail['land_benefit']? $land_detail['land_benefit']:' - ';
              $loan_land_occupy_model = $this->loan_land_occupy_model->getByLoanIdAndLandId($loan_id,$land['land_id']);
              if($loan_land_occupy_model){
                $benefit = view_value(num2Thai($loan_land_occupy_model['loan_land_occupy_benefit']));
              }
              $lands[] = array(
              'owner' => view_value(num2Thai($person['person_fullname'])),
              'area' => view_value(num2Thai($land['land_area_rai'])).' - '.view_value(num2Thai($land['land_area_ngan'])).' - '
                  .view_value(num2Thai($land['land_area_wah'])),
              'land_type' =>  view_value(num2Thai($land_detail['land_type_name'])),
              'land_no' => view_value(num2Thai($land['land_no'])),
              'address' =>  'หมู่ที่ '.view_value(num2Thai($land_detail['land_addr_moo'])).' ตำบล '
                    .view_value(num2Thai($land_detail['land_addr_district_name'])).' อำเภอ '
                    .view_value(num2Thai($land_detail['land_addr_amphur_name'])).' จังหวัด '.
                      view_value(num2Thai($land_detail['land_addr_province_name'])),
              'benefit' => $benefit
            );
          }
      }
      $land_owner_model = $this->land_owner_model->get_by_opt(array('person_id' => $spouse_id));
      if ($land_owner_model) {

          foreach ($land_owner_model as $key_land => $land_owner) {
              $person = $this->loan_lib->person_detail($land_owner['person_id']);
              $land = $this->land_model->get_by_id($land_owner['land_id']);
              $land_detail = $this->loan_lib->land_detail($land);
              $benefit = ' - ';
              $loan_land_occupy_model = $this->loan_land_occupy_model->getByLoanIdAndLandId($loan_id,$land['land_id']);
              if($loan_land_occupy_model){
                $benefit = view_value(num2Thai($loan_land_occupy_model['loan_land_occupy_benefit']));
              }
              $lands[] = array(
              'id' => $land['land_id'],
              'owner' => view_value(num2Thai($person['person_fullname'])),
              'area' => view_value(num2Thai($land['land_area_rai'])).' - '.view_value(num2Thai($land['land_area_ngan'])).' - '
                  .view_value(num2Thai($land['land_area_wah'])),
              'land_type' =>  view_value(num2Thai($land_detail['land_type_name'])),
              'land_no' => view_value(num2Thai($land['land_no'])),
              'address' =>  'หมู่ที่ '.view_value(num2Thai($land_detail['land_addr_moo'])).' ตำบล '
                    .view_value(num2Thai($land_detail['land_addr_district_name'])).' อำเภอ '
                    .view_value(num2Thai($land_detail['land_addr_amphur_name'])).' จังหวัด '.
                      view_value(num2Thai($land_detail['land_addr_province_name'])),
              'benefit' => $benefit
            );
          }
      }



    //	$loan_land_occupy_model = $this->loan_lib->loan_land_occupy_report($loan_id);


    	$data_occupy = array();
    	if($lands)
    	{
    		foreach ($lands as $key => $data)
    		{
    			$data_occupy[] = array(
            'occupy_id' => num2Thai($key + 1),
            'occupy_owner' => $data['owner'],
            'occupy_area' => $data['area'],
            'land_type' => $data['land_type'],
            'land_no' => $data['land_no'],
            'occupy_address' => $data['address'],
            'occupy_benefit' => $data['benefit']
    					/*'occupy_id' => num2Thai($key + 1),
    					'occupy_owner' => view_value(num2Thai($data['land_detail']['owner_fullname'])),
    					'occupy_area' => view_value(num2Thai($data['land_detail']['land_area_rai'])).' - '.view_value(num2Thai($data['land_detail']['land_area_ngan'])).' - '.view_value(num2Thai($data['land_detail']['land_area_wah'])),
    					'land_type' => view_value(num2Thai($data['land_detail']['land_type_name'])),
    					'land_no' => view_value(num2Thai($data['land_detail']['land_no'])),
    					'occupy_address' => view_value(num2Thai($data['land_detail']['land_addr_no'])).' '.view_value(num2Thai($data['land_detail']['land_addr_moo'])).' '.view_value(num2Thai($data['land_detail']['land_addr_district_name'])).' '.view_value(num2Thai($data['land_detail']['land_addr_amphur_name'])).' '.view_value(num2Thai($data['land_detail']['land_addr_province_name'])),
    					'occupy_benefit' => view_value(num2Thai($data['loan_land_occupy_benefit']))*/
    				);
    		}
    	}
    	else
    	{
    		$data_occupy[] = array(
    				'occupy_id' => num2Thai(1),
    				'occupy_owner' => ' - ',
    				'occupy_area' => ' - ',
    				'land_type' => ' - ',
    				'land_no' => ' - ',
    				'occupy_address' => ' - ',
    				'occupy_benefit' => ' - '
    		);
    	}

    	$loan_asset_model = $this->loan_lib->loan_asset_report($loan_id);

    	$data_asset = array();

    	if ($loan_asset_model)
    	{
        $GLOBALS['has_asset'] = '';
    		foreach ($loan_asset_model as $key => $data)
    		{
    			$data_asset[] = array(
    					'asset_id' => num2Thai($key + 1),
    					'asset_name' => view_value(num2Thai($data['loan_asset_name'])),
    					'asset_amount' => view_value(num2Thai(money_num_to_str($data['loan_asset_amount']))),
    					'asset_quantity' => view_value(num2Thai($data['loan_asset_quantity'])).' '.num2Thai($data['loan_asset_uom_name'])
    			);
    		}
    	}
    	else
    	{
        $GLOBALS['has_asset'] = ' - ';

    	}

    	$loan_co_model = $this->loan_lib->loan_co_report($loan_id);

    	$data_co = array();
    	if($loan_co_model)
    	{
    		foreach ($loan_co_model as $key => $data)
    		{
    			$data_co[] = array(
    					'co_id' => num2Thai($key + 1),
    					'co_fullname' => view_value($data['person_detail']['person_fullname']),
    					'co_birthdate' => view_value(num2Thai($data['person_detail']['person_birthdate_thai'])),
    					'co_age' => view_value(num2Thai($data['person_detail']['person_age'])),
    					'co_sex' => view_value(num2Thai($data['person_detail']['sex_name'])),
    					'co_race' => view_value(num2Thai($data['person_detail']['race_name'])),
    					'co_nationality' => view_value(num2Thai($data['person_detail']['nationality_name'])),
    					'co_religion' => view_value(num2Thai($data['person_detail']['religion_name'])),
    					'co_thaiid' => view_value(num2Thai(formatThaiid($data['person_detail']['person_thaiid']))),
    					'co_card_start' => view_value(num2Thai($data['person_detail']['person_card_startdate_thai'])),
    					'co_card_expire' => view_value(num2Thai($data['person_detail']['person_card_expiredate_thai'])),
    					'co_type_card' => '(√)',
    					'co_type_pre' => '( )',
    					'co_card_no' => view_value(num2Thai($data['person_detail']['person_addr_card_no'])),
    					'co_card_moo' => view_value(num2Thai($data['person_detail']['person_addr_card_moo'])),
    					'co_card_road' => view_value(num2Thai($data['person_detail']['person_addr_card_road'])),
    					'co_card_district' => view_value(num2Thai($data['person_detail']['person_addr_card_district_name'])),
    					'co_card_amphur' => view_value(num2Thai($data['person_detail']['person_addr_card_amphur_name'])),
    					'co_card_province' => view_value(num2Thai($data['person_detail']['person_addr_card_province_name'])),
    					'co_pre_no' => view_value(num2Thai($data['person_detail']['person_addr_pre_no'])),
    					'co_pre_moo' => view_value(num2Thai($data['person_detail']['person_addr_pre_moo'])),
    					'co_pre_road' => view_value(num2Thai($data['person_detail']['person_addr_pre_road'])),
    					'co_pre_district' => view_value(num2Thai($data['person_detail']['person_addr_pre_district_name'])),
    					'co_pre_amphur' => view_value(num2Thai($data['person_detail']['person_addr_pre_amphur_name'])),
    					'co_pre_province' => view_value(num2Thai($data['person_detail']['person_addr_pre_province_name'])),
    					'co_phone' => view_value(num2Thai($data['person_detail']['person_phone'])),
    					'co_mobile' => view_value(num2Thai($data['person_detail']['person_mobile'])),
    					'co_email' => view_value(num2Thai($data['person_detail']['person_email'])),
    					'co_career' => view_value(num2Thai($data['person_detail']['career_name'])),
              'co_career_other' => view_value(num2Thai($data['person_detail']['person_career_other'])),
    					'co_income' => view_value(num2Thai(money_num_to_str($data['person_detail']['person_income_per_month']))),
    					'co_expenditure' => view_value(num2Thai(money_num_to_str($data['person_detail']['person_expenditure_per_month']))),
    					'co_relation' => view_value(num2Thai($data['relationship_name']))
    			);
    		}
    	}
    	else
    	{
    		$data_co[] = array(
    				'co_id' => num2Thai(1),
    				'co_fullname' => ' - ',
    				'co_birthdate' => ' - ',
    				'co_age' => ' - ',
    				'co_sex' => ' - ',
    				'co_race' => ' - ',
    				'co_nationality' => ' - ',
    				'co_religion' => ' - ',
    				'co_thaiid' => ' - ',
    				'co_card_start' => ' - ',
    				'co_card_expire' => ' - ',
            'co_type_card' => '( )',
            'co_type_pre' => '( )',
    				'co_card_no' => ' - ',
    				'co_card_moo' => ' - ',
    				'co_card_road' => ' - ',
    				'co_card_district' => ' - ',
    				'co_card_amphur' => ' - ',
    				'co_card_province' => ' - ',
    				'co_pre_no' => ' - ',
    				'co_pre_moo' => ' - ',
    				'co_pre_road' => ' - ',
    				'co_pre_district' => ' - ',
    				'co_pre_amphur' => ' - ',
    				'co_pre_province' => ' - ',
    				'co_phone' => ' - ',
    				'co_mobile' => ' - ',
    				'co_email' => ' - ',
    				'co_career' => ' - ',
            'co_career_other' => ' - ',
    				'co_income' => ' - ',
    				'co_expenditure' => ' - ',
    				'co_relation' => ' - '
    		);
    	}

    	$loan_debt_model = $this->loan_lib->loan_debt_report($loan_id);

    	$data_debt = array();
    	if($loan_debt_model)
    	{
    		$GLOBALS['check_debt_yes'] = '(√)';
    		$GLOBALS['check_debt_no'] = '( )';

    		foreach ($loan_debt_model as $key => $data)
    		{
    			$data_debt[] = array(
    					'debt_id' => num2Thai($key + 1),
    					'debt_name' => num2Thai($data['loan_debt_name']),
    					'debt_amount_start' => num2Thai($data['loan_debt_amount_start']),
    					'debt_amount_remain' => num2Thai($data['loan_debt_amount_remain']),
    					'debt_amount' => num2Thai($data['loan_debt_amount']),
    					'debt_start' => num2Thai($data['loan_debt_date_start']),
    					'debt_end' => num2Thai($data['loan_debt_date_end'])
    			);
    		}
    	}
    	else
    	{
    		$GLOBALS['check_debt_yes'] = '( )';
    		$GLOBALS['check_debt_no'] = '(√)';
    	}

    	$loan_guarantee_land_model = $this->loan_lib->loan_guarantee_land_report($loan_id);
    	$GLOBALS['check_land'] = $loan_guarantee_land_model? '(√)' : '( )';

    	$data_land = array();
    	if($loan_guarantee_land_model)
    	{
    		foreach ($loan_guarantee_land_model as $key => $data)
    		{
    			if ($data['loan_guarantee_land_bind_type']=='1')
    			{
    				$check_mortgage_no = '( )';
    				$check_mortgage_yes = '(√)';
    				$mortgage_owner = view_value(num2Thai($data['loan_guarantee_land_fullname']));
    				$mortgage_amount = view_value(num2Thai($data['loan_guarantee_land_bind_amount']));
    			}
    			else
    			{
    				$check_mortgage_no = '(√)';
    				$check_mortgage_yes = '( )';
    				$mortgage_owner = ' - ';
    				$mortgage_amount = ' - ';
    			}

    			if ($data['loan_guarantee_land_bind_type']=='1')
    			{
    				$check_pay_no = '( )';
    				$check_pay_yes = '(√)';
    				$pay_relief = view_value(num2Thai($data['loan_guarantee_land_relief']));
    				$pay_peymonth = view_value(num2Thai($data['loan_guarantee_land_relief_permonth']));
    				$pay_enddate = view_value(num2Thai($data['loan_guarantee_land_relief_enddate']));
    			}
    			else
    			{
    				$check_pay_no = '(√)';
    				$check_pay_yes = '( )';
    				$pay_relief = ' - ';
    				$pay_peymonth = ' - ';
    				$pay_enddate = ' - ';
    			}
          //
          // echo '<pre>';
        	// print_r($data);
        	// echo '</pre>';
          $loan_guarantee_land_benefit = explode(",", $data['loan_guarantee_land_benefit']);
    			$list_land = array(
    					'land_id' => num2Thai($key + 1),
    					'check_mortgage_no' => $check_mortgage_no,
    					'check_mortgage_yes' => $check_mortgage_yes,
    					'mortgage_owner' => $mortgage_owner,
    					'mortgage_amount' => $mortgage_amount,
    					'check_pay_no' => $check_pay_no,
    					'check_pay_yes' => $check_pay_yes,
    					'pay_relief' => $pay_relief,
    					'pay_permonth' => $pay_peymonth,
    					'pay_enddate' => $pay_enddate,
              'land_benefit' => $data['loan_guarantee_land_benefit'],
    					'check_farm' =>  in_array(1,$loan_guarantee_land_benefit)?'(√)':'( )',
    					'check_live' => in_array(2,$loan_guarantee_land_benefit)?'(√)':'( )',
    					'check_no' => in_array(3,$loan_guarantee_land_benefit)?'(√)':'( )',
    					'check_yes' =>  in_array(4,$loan_guarantee_land_benefit)?'(√)':'( )',
    					'yes_desc' => $data['loan_guarantee_land_benefit_other']?$data['loan_guarantee_land_benefit_other']:' - '
    			);

    			$config_land_type = $this->mst_model->get_all('land_type');

    			$html_land_type_check = '';
    			$html_land_type = '';
    			foreach ($config_land_type as $key_land_type => $data_land_type)
    			{
    				if ($data_land_type['land_type_id'] == $data['land_list']['land_type_id'])
    				{
    					$html_land_type_check .= '(√) '.$data_land_type['land_type_name'].'  ';
    				}
    				else
    				{
    					$html_land_type_check .= '( ) '.$data_land_type['land_type_name'].'  ';
    				}

    				$html_land_type .= '( ) '.$data_land_type['land_type_name'].'  ';
    			}

    			$config_building_type = $this->mst_model->get_all('building_type');

    			$html_building = '';
    			foreach ($config_building_type as $key_building => $data_building)
    			{
    				$html_building .= '( ) '.$data_building['building_type_name'].'  ';
    			}

    			if(empty($data['land_list']['building_list'][0]))
    			{
    				$list_land['land_1_type'] = $html_land_type_check;
    				$list_land['land_1_no'] = view_value(num2Thai($data['land_list']['land_no']));
    				$list_land['land_1_addr_no'] = view_value(num2Thai($data['land_list']['land_addr_no']));
    				$list_land['land_1_district'] = view_value($data['land_list']['land_addr_district_name']);
    				$list_land['land_1_amphur'] = view_value($data['land_list']['land_addr_amphur_name']);
    				$list_land['land_1_province'] = view_value($data['land_list']['land_addr_province_name']);
    				$list_land['land_1_rai'] = view_value(num2Thai($data['land_list']['land_area_rai']));
    				$list_land['land_1_ngan'] = view_value(num2Thai($data['land_list']['land_area_ngan']));
    				$list_land['land_1_wah'] = view_value(num2Thai($data['land_list']['land_area_wah']));
    				$list_land['land_2_type'] = $html_land_type;
    				$list_land['land_2_no'] = ' - ';
    				$list_land['land_2_addr_no'] = ' - ';
    				$list_land['land_2_district'] = ' - ';
    				$list_land['land_2_amphur'] = ' - ';
    				$list_land['land_2_province'] = ' - ';
    				$list_land['land_2_rai'] = ' - ';
    				$list_land['land_2_ngan'] = ' - ';
    				$list_land['land_2_wah'] = ' - ';
    				$list_land['building_type'] = $html_building;
    				$list_land['building_no'] = ' - ';
    				$list_land['building_name'] = ' - ';
    				$list_land['building_moo'] = ' - ';
    				$list_land['building_road'] = ' - ';
    				$list_land['building_district'] = ' - ';
    				$list_land['building_amphur'] = ' - ';
    				$list_land['building_province'] = ' - ';
    			}
    			else
    			{
    				$html_building = '';
    				foreach ($config_building_type as $key_building => $data_building)
    				{
    					if ($data_building['building_type_id'] == $data['land_list']['building_list'][0]['building_type_id'])
    					{
    						$html_building .= '(√) '.$data_building['building_type_name'].'  ';
    					}
    					else
    					{
    						$html_building .= '( ) '.$data_building['building_type_name'].'  ';
    					}
    				}

    				$list_land['land_1_type'] = $html_land_type;
    				$list_land['land_1_no'] = ' - ';
    				$list_land['land_1_addr_no'] = ' - ';
    				$list_land['land_1_district'] = ' - ';
    				$list_land['land_1_amphur'] = ' - ';
    				$list_land['land_1_province'] = ' - ';
    				$list_land['land_1_rai'] = ' - ';
    				$list_land['land_1_ngan'] = ' - ';
    				$list_land['land_1_wah'] = ' - ';
    				$list_land['land_2_type'] = $html_land_type_check;
    				$list_land['land_2_no'] = view_value(num2Thai($data['land_list']['land_no']));
    				$list_land['land_2_addr_no'] = view_value(num2Thai($data['land_list']['land_addr_no']));
    				$list_land['land_2_district'] = view_value($data['land_list']['land_addr_district_name']);
    				$list_land['land_2_amphur'] = view_value($data['land_list']['land_addr_amphur_name']);
    				$list_land['land_2_province'] = view_value($data['land_list']['land_addr_province_name']);
    				$list_land['land_2_rai'] = view_value(num2Thai($data['land_list']['land_area_rai']));
    				$list_land['land_2_ngan'] = view_value(num2Thai($data['land_list']['land_area_ngan']));
    				$list_land['land_2_wah'] = view_value(num2Thai($data['land_list']['land_area_wah']));
    				$list_land['building_type'] = $html_building;
    				$list_land['building_no'] = view_value(num2Thai($data['land_list']['building_list'][0]['building_no']));
    				$list_land['building_name'] = view_value(num2Thai($data['land_list']['building_list'][0]['building_name']));
    				$list_land['building_moo'] = view_value(num2Thai($data['land_list']['building_list'][0]['building_moo']));
    				$list_land['building_road'] = view_value(num2Thai($data['land_list']['building_list'][0]['building_road']));
    				$list_land['building_district'] = view_value($data['land_list']['building_list'][0]['building_district_name']);
    				$list_land['building_amphur'] = view_value($data['land_list']['building_list'][0]['building_amphur_name']);
    				$list_land['building_province'] = view_value($data['land_list']['building_list'][0]['building_province_name']);
    			}

    			$data_land[] = $list_land;
    		}
    	}
    	else
    	{
    		$data_land[] = array(
    				'land_id' => num2Thai(1),
    				'check_mortgage_no' => '( )',
    				'check_mortgage_yes' => '( )',
    				'mortgage_owner' => ' - ',
    				'mortgage_amount' => ' - ',
    				'check_pay_no' => '( )',
    				'check_pay_yes' => '( )',
    				'pay_relief' => ' - ',
    				'pay_permonth' => ' - ',
    				'pay_enddate' => ' - ',
    				'check_farm' => '( )',
    				'check_live' => '( )',
    				'check_no' => '( )',
    				'check_yes' => '( )',
    				'yes_desc' => ' - ',
    				'land_1_type' => $html_land_type,
    				'land_1_no' => ' - ',
    				'land_1_addr_no' => ' - ',
    				'land_1_district' => ' - ',
    				'land_1_amphur' => ' - ',
    				'land_1_province' => ' - ',
    				'land_1_rai' => ' - ',
    				'land_1_ngan' => ' - ',
    				'land_1_wah' => ' - ',
    				'land_2_type' => $html_land_type,
    				'land_2_no' => ' - ',
    				'land_2_addr_no' => ' - ',
    				'land_2_district' => ' - ',
    				'land_2_amphur' => ' - ',
    				'land_2_province' => ' - ',
    				'land_2_rai' => ' - ',
    				'land_2_ngan' => ' - ',
    				'land_2_wah' => ' - ',
    				'building_type' => $html_building,
    				'building_no' => ' - ',
    				'building_name' => ' - ',
    				'building_moo' => ' - ',
    				'building_road' => ' - ',
    				'building_district' => ' - ',
    				'building_amphur' => ' - ',
    				'building_province' => ' - '
    		);
    	}

    	$loan_guarantee_bond_model = $this->loan_lib->loan_guarantee_bond_report($loan_id);
    	$GLOBALS['check_bond'] = $loan_guarantee_bond_model? '(√)' : '( )';

    	$data_bond = array();
    	if($loan_guarantee_bond_model)
    	{
    		foreach ($loan_guarantee_bond_model as $key => $data)
    		{
    			$data_bond[] = array(
    						'bond_id' => num2Thai($key + 1),
    						'bond_owner' => num2Thai($data['loan_bond_owner']),
    						'bond_thaiid' => num2Thai(formatThaiid($data['loan_bond_thaiid'])),
    						'bond_type' => num2Thai($data['loan_bond_type']),
    						'bond_start' => num2Thai($data['loan_bond_startnumber']),
    						'bond_end' => num2Thai($data['loan_bond_endnumber']),
    						'bond_amount' => num2Thai($data['loan_bond_amount']),
    						'bond_amount_text' => num2Thai($data['loan_bond_amount_thai'])
    					);
    		}
    	}
    	else
    	{
    		$data_bond[] = array(
    				'bond_id' => num2Thai(1),
    				'bond_owner' => ' - ',
    				'bond_thaiid' => ' - ',
    				'bond_type' => ' - ',
    				'bond_start' => ' - ',
    				'bond_end' => ' - ',
    				'bond_amount' => ' - ',
    				'bond_amount_text' => ' - '
    		);
    	}

    	$loan_guarantee_bookbank_model = $this->loan_lib->loan_guarantee_bookbank_report($loan_id);
    	$GLOBALS['check_bookbank'] = $loan_guarantee_bookbank_model? '(√)' : '( )';

    	$data_bookbank = array();
    	if($loan_guarantee_bookbank_model)
    	{
    		foreach ($loan_guarantee_bookbank_model as $key => $data)
    		{
    			$data_bookbank[] = array(
    					'bookbank_id' => num2Thai($key + 1),
    					'bookbank_owner' => view_value(num2Thai($data['loan_bookbank_owner'])),
    					'bookbank_bank' => view_value(num2Thai($data['bank_name'])),
    					'bookbank_branch' => num2Thai($data['loan_bookbank_branch']),
    					'bookbank_no' => view_value(num2Thai($data['loan_bookbank_no'])),
    					'bookbank_type' => view_value(num2Thai($data['loan_bookbank_type_name'])),
    					'bookbank_date' => view_value(num2Thai($data['loan_bookbank_date'])),
    					'bookbank_amount' => view_value(num2Thai($data['loan_bookbank_amount'])),
    					'bookbank_amount_text' => view_value(num2Thai($data['loan_bookbank_amount_thai']))
    			);
    		}
    	}
    	else
    	{
    		$data_bookbank[] = array(
    				'bookbank_id' => num2Thai(1),
    				'bookbank_owner' => ' - ',
    				'bookbank_bank' => ' - ',
    				'bookbank_branch' => ' - ',
    				'bookbank_no' => ' - ',
    				'bookbank_type' => ' - ',
    				'bookbank_date' => ' - ',
    				'bookbank_amount' => ' - ',
    				'bookbank_amount_text' => ' - '
    		);
    	}

    	$loan_guarantee_bondsman_model = $this->loan_lib->loan_guarantee_bondsman_report($loan_id);
    	$GLOBALS['check_bondsman'] = $loan_guarantee_bondsman_model? '(√)' : '( )';
      $bondsman_amount = count($loan_guarantee_bondsman_model);

    	$GLOBALS['bondsman_amount'] = $bondsman_amount>0?num2Thai(count($loan_guarantee_bondsman_model)):' - ';

    	$data_bondsman = array();
    	if($loan_guarantee_bondsman_model)
    	{
    		foreach ($loan_guarantee_bondsman_model as $key => $data)
    		{
    			if($data['person_detail']['person_loan_other_check']=='1')
    			{
    				$other_check_yes = '(√)';
    				$other_check_no = '( )';

    				$other_amount = view_value(num2Thai($data['person_detail']['person_guarantee_other_amount']));
    				$other_year = view_value(num2Thai($data['person_detail']['person_guarantee_other_amount_per_year']));
    			}
    			else
    			{
    				$other_check_yes = '( )';
    				$other_check_no = '(√)';

    				$other_amount = ' - ';
    				$other_year = ' - ';
    			}

    			if ($data['person_detail']['person_guarantee_other_amount_check']=='1')
    			{
    				$guarantee_check_yes = '(√)';
    				$guarantee_check_no = '( )';

    				$guarantee_amount = view_value(num2Thai($data['person_detail']['person_guarantee_other_amount']));
    			}
    			else
    			{
    				$guarantee_check_yes = '( )';
    				$guarantee_check_no = '(√)';

    				$guarantee_amount = ' - ';
    			}

    			$data_bondsman[] = array(
    					'bondsman_id' => num2Thai($key + 1),
    					'bondsman_fullname' => view_value(num2Thai($data['person_detail']['person_fullname'])),
    					'bondsman_age' => view_value(num2Thai($data['person_detail']['person_age'])),
    					'bondsman_pre_no' => view_value(num2Thai($data['person_detail']['person_addr_pre_no'])),
    					'bondsman_pre_moo' => view_value(num2Thai($data['person_detail']['person_addr_pre_moo'])),
    					'bondsman_pre_road' => view_value(num2Thai($data['person_detail']['person_addr_pre_road'])),
    					'bondsman_district' => view_value(num2Thai($data['person_detail']['person_addr_pre_district_name'])),
    					'bondsman_amphur' => view_value(num2Thai($data['person_detail']['person_addr_pre_amphur_name'])),
    					'bondsman_province' => view_value(num2Thai($data['person_detail']['person_addr_pre_province_name'])),
    					'bondsman_phone' => view_value(num2Thai($data['person_detail']['person_phone'])),
    					'bondsman_mobile' => view_value(num2Thai($data['person_detail']['person_mobile'])),
    					'bondsman_email' => view_value(num2Thai($data['person_detail']['person_email'])),
    					'bondsman_career' => view_value(num2Thai($data['person_detail']['career_name'])),
    					'bondsman_position' => view_value(num2Thai($data['person_detail']['person_position'])),
    					'bondsman_job_desc' => view_value(num2Thai($data['person_detail']['person_job_desc'])),
    					'bondsman_type_business' => view_value(num2Thai($data['person_detail']['person_type_business'])),
    					'bondsman_belong' => view_value(num2Thai($data['person_detail']['person_belong'])),
    					'bondsman_ministry' => view_value(num2Thai($data['person_detail']['person_ministry'])),
    					'bondsman_workplace' => view_value(num2Thai($data['person_detail']['person_workplace'])),
    					'bondsman_cardid_emp' => view_value(num2Thai($data['person_detail']['person_cardid_emp'])),
    					'bondsman_card_start' => view_value(num2Thai($data['person_detail']['person_cardid_emp_startdate'])),
    					'bondsman_card_expire' => view_value(num2Thai($data['person_detail']['person_cardid_emp_expiredate'])),
    					'bondsman_income_per_month' => view_value(num2Thai($data['person_detail']['person_income_per_month'])),
    					'bondsman_income_per_year' => view_value(num2Thai($data['person_detail']['person_income_per_year'])),
    					'bondsman_other_check_yes' => $other_check_yes,
    					'bondsman_other_check_no' => $other_check_no,
    					'bondsman_other_amount' => $other_amount,
    					'bondsman_other_year' => $other_year,
    					'bondsman_guarantee_check_yes' => $guarantee_check_yes,
    					'bondsman_guarantee_check_no' => $guarantee_check_no,
    					'bondsman_guarantee_amount' => $guarantee_amount
    			);
    		}
    	}
    	else
    	{
    		$data_bondsman[] = array(
    				'bondsman_id' => num2Thai(1),
    				'bondsman_fullname' => ' - ',
    				'bondsman_age' => ' - ',
    				'bondsman_pre_no' => ' - ',
    				'bondsman_pre_moo' => ' - ',
    				'bondsman_pre_road' => ' - ',
    				'bondsman_district' => ' - ',
    				'bondsman_amphur' => ' - ',
    				'bondsman_province' => ' - ',
    				'bondsman_phone' => ' - ',
    				'bondsman_mobile' => ' - ',
    				'bondsman_email' => ' - ',
    				'bondsman_career' => ' - ',
    				'bondsman_position' => ' - ',
    				'bondsman_job_desc' => ' - ',
    				'bondsman_type_business' => ' - ',
    				'bondsman_belong' => ' - ',
    				'bondsman_ministry' => ' - ',
    				'bondsman_workplace' => ' - ',
    				'bondsman_cardid_emp' => ' - ',
    				'bondsman_card_start' => ' - ',
    				'bondsman_card_expire' => ' - ',
    				'bondsman_income_per_month' => ' - ',
    				'bondsman_income_per_year' => ' - ',
    				'bondsman_other_check_yes' => '( )',
    				'bondsman_other_check_no' => '( )',
    				'bondsman_other_amount' => ' - ',
    				'bondsman_other_year' => ' - ',
    				'bondsman_guarantee_check_yes' => '( ) ',
    				'bondsman_guarantee_check_no' => '( )',
    				'bondsman_guarantee_amount' => ' - '
    		);
    	}

    	$template = 'word/template/1_loan.docx';
    	$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).
/*
    	echo '<pre>';
    	print_r($data_income['2']);
    	echo '</pre>';
*/
      $income1_sum = 0.0;
      foreach ($data_income['1'] as $key => $value) {
         $income1_sum += $value['loan_income_amount'];
      }

      $GLOBALS['income1_sum'] = $income1_sum?num2Thai(money_num_to_str($income1_sum)).' บาท':' - ';
    	$TBS->MergeBlock('income1', $data_income['1']);

      $income2_sum = 0.0;
      foreach ($data_income['2'] as $key => $value) {
         $income2_sum += $value['loan_income_amount'];
      }

      $GLOBALS['income2_sum'] = $income2_sum?num2Thai(money_num_to_str($income2_sum)).' บาท':' - ';
    	$TBS->MergeBlock('income2', $data_income['2']);

      $income3_sum = 0.0;
      foreach ($data_income['3'] as $key => $value) {
         $income3_sum += $value['loan_income_amount'];
      }

      $GLOBALS['income3_sum'] = $income3_sum?num2Thai(money_num_to_str($income3_sum)).' บาท':' - ';

    	$TBS->MergeBlock('income3', $data_income['3']);
      $expenditure1_sum = 0.0;
      foreach ($data_expenditure['1'] as $key => $value) {
         $expenditure1_sum += $value['loan_expenditure_amount'];
      }

      $GLOBALS['expenditure1_sum'] = $expenditure1_sum?num2Thai(money_num_to_str($expenditure1_sum)).' บาท':' - ';
    	$TBS->MergeBlock('expenditure1', $data_expenditure['1']);
      $expenditure2_sum = 0.0;
      foreach ($data_expenditure['2'] as $key => $value) {
         $expenditure2_sum += $value['loan_expenditure_amount'];
      }
      $GLOBALS['expenditure2_sum'] = $expenditure2_sum?num2Thai(money_num_to_str($expenditure2_sum)).' บาท':' - ';
    	$TBS->MergeBlock('expenditure2', $data_expenditure['2']);
      $expenditure3_sum = 0.0;
      foreach ($data_expenditure['3'] as $key => $value) {
         $expenditure3_sum += $value['loan_expenditure_amount'];
      }

      $GLOBALS['expenditure3_sum'] = $expenditure3_sum?num2Thai(money_num_to_str($expenditure3_sum)).' บาท':' - ';
    	$TBS->MergeBlock('expenditure3', $data_expenditure['3']);
      $expenditure4_sum = 0.0;
      foreach ($data_expenditure['4'] as $key => $value) {
         $expenditure4_sum += $value['loan_expenditure_amount'];
      }
      $GLOBALS['expenditure4_sum'] = $expenditure4_sum?num2Thai(money_num_to_str($expenditure4_sum)).' บาท':' - ';
    	$TBS->MergeBlock('expenditure4', $data_expenditure['4']);
    	$TBS->MergeBlock('occupy', $data_occupy);
    	$TBS->MergeBlock('asset', $data_asset);
    	$TBS->MergeBlock('debt', $data_debt);
    	$TBS->MergeBlock('co', $data_co);
    	$TBS->MergeBlock('land', $data_land);
      $html_empty_land_type = '';
      foreach ($config_land_type as $key_land_type => $data_land_type)
      {
        $html_empty_land_type .= '( ) '.$data_land_type['land_type_name'].'  ';
      }
      $html_empty_building_type = '';
      foreach ($config_building_type as $key_building => $data_building)
      {
          $html_empty_building_type .= '( ) '.$data_building['building_type_name'].'  ';
      }
      if(empty($land1)){
        $land1[] = array(
        'building_type' => $html_empty_building_type,
        'land_type' => $html_empty_land_type,
        'land_no' => ' - ',
        'land_addr_no' => ' - ',
        'land_district' => ' - ',
        'land_amphur' => ' - ',
        'land_province' => ' - ',
        'land_rai' => ' - ',
        'land_ngan' => ' - ',
        'land_wah' => ' - ',
        'building_no' => ' - ',
        'building_name' => ' - ',
        'building_moo' => ' - ',
        'building_road' => ' - ',
        'building_district' => ' - ',
        'building_amphur' => ' - ',
        'building_province' => ' - ',
        'type_amount' => ' - ',
        'type_amount_text' => ' - ',
        'type_owner' => ' - ',
        'type_age' => ' - ',
        'type_home_no' => ' - ',
        'type_home_moo' => ' - ',
        'type_home_road' => ' - ',
        'type_home_district' => ' - ',
        'type_home_amphur' => ' - ',
        'type_home_province' => ' - ',
        'type_phone' => ' - ',
        'type_mobile' => ' - ',
        'type_relation' => ' - ');
      }

      $TBS->MergeBlock('land1', $land1);
      if(empty($land_building1)){
        $land_building1[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' =>  ' - ',
          'land_addr_no' =>  ' - ',
          'land_district' =>  ' - ',
          'land_amphur' => ' - ',
          'land_province' =>  ' - ',
          'land_rai' =>  ' - ',
          'land_ngan' =>  ' - ',
          'land_wah' =>  ' - ',
          'building_no' =>  ' - ',
          'building_name' =>  ' - ',
          'building_moo' =>  ' - ',
          'building_road' =>  ' - ',
          'building_district' =>  ' - ',
          'building_amphur' =>  ' - ',
          'building_province' =>  ' - ',
          'type_amount' =>  ' - ',
          'type_amount_text' =>  ' - ',
          'type_owner' =>  ' - ',
          'type_age' =>  ' - ',
          'type_home_no' =>  ' - ',
          'type_home_moo' =>  ' - ',
          'type_home_road' =>  ' - ',
          'type_home_district' =>  ' - ',
          'type_home_amphur' => ' - ',
          'type_home_province' =>  ' - ',
          'type_phone' =>  ' - ',
          'type_mobile' =>  ' - ',
          'type_relation' => ' - '
        );
      }
      $TBS->MergeBlock('land_building1', $land_building1);
      if(empty($land2)){
        $land2[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' => ' - ',
          'land_addr_no' =>  ' - ',
          'land_district' => ' - ',
          'land_amphur' => ' - ',
          'land_province' => ' - ',
          'land_rai' => ' - ',
          'land_ngan' => ' - ',
          'land_wah' => ' - ',
          'type_amount' => ' - ',
          'type_amount_text' => ' - ',
          'type_no' => ' - ',
          'type_date' => ' - ',
          'type_creditor' => ' - ',
          'type_borrowers' => ' - ',
          'type_objective' => ' - '
        );
      }
      $TBS->MergeBlock('land2', $land2);
      if(empty($land_building2)){
        $land_building2[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' => ' - ',
          'land_addr_no' => ' - ',
          'land_district' => ' - ',
          'land_amphur' => ' - ',
          'land_province' => ' - ',
          'land_rai' => ' - ',
          'land_ngan' => ' - ',
          'land_wah' => ' - ',
          'building_no' => ' - ',
          'building_name' => ' - ',
          'building_moo' => ' - ',
          'building_road' => ' - ',
          'building_district' => ' - ',
          'building_amphur' => ' - ',
          'building_province' => ' - ',
          'type_amount' => ' - ',
          'type_amount_text' => ' - ',
          'type_no' => ' - ',
          'type_date' => ' - ',
          'type_creditor' => ' - ',
          'type_borrowers' => ' - ',
          'type_objective' => ' - '
        );
      }
      $TBS->MergeBlock('land_building2', $land_building2);
      if(empty($land3)){
        $land3[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' => ' - ',
          'land_addr_no' =>  ' - ',
          'land_district' => ' - ',
          'land_amphur' => ' - ',
          'land_province' => ' - ',
          'land_rai' => ' - ',
          'land_ngan' => ' - ',
          'land_wah' => ' - ',
          'type_amount' => ' - ',
          'type_amount_text' => ' - ',
          'type_black' => ' - ',
          'type_red' => ' - ',
          'type_court' => ' - ',
          'type_date' =>' - ',
          'type_type' => ' - ',
          'type_plaintiff' => ' - ',
          'type_defendant' => ' - ',
          'type_comment' => ' - ',
          'type_objective' => ' - '
        );
      }
      $TBS->MergeBlock('land3', $land3);
      if(empty($land_building3)){


        $land_building3[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' => ' - ',
          'land_addr_no' => ' - ',
          'land_district' => ' - ',
          'land_amphur' => ' - ',
          'land_province' => ' - ',
          'land_rai' => ' - ',
          'land_ngan' => ' - ',
          'land_wah' => ' - ',
          'building_no' => ' - ',
          'building_name' => ' - ',
          'building_moo' => ' - ',
          'building_road' => ' - ',
          'building_district' => ' - ',
          'building_amphur' => ' - ',
          'building_province' => ' - ',
          'type_amount' => ' - ',
          'type_amount_text' => ' - ',
          'type_black' => ' - ',
          'type_red' => ' - ',
          'type_court' => ' - ',
          'type_date' => ' - ',
          'type_type' => ' - ',
          'type_plaintiff' => ' - ',
          'type_defendant' => ' - ',
          'type_comment' => ' - ',
          'type_objective' => ' - '
        );
      }
      $TBS->MergeBlock('land_buiding3', $land_building3);
      if(empty($land4)){
        $land4[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' => ' - ',
          'land_addr_no' =>  ' - ',
          'land_district' => ' - ',
          'land_amphur' => ' - ',
          'land_province' => ' - ',
          'land_rai' => ' - ',
          'land_ngan' => ' - ',
          'land_wah' => ' - ',
          'type_amount' => ' - ',
          'type_amount_text' => ' - ',
          'type_date' =>' - ',
          'type_objective' => ' - '
        );
      }
      $TBS->MergeBlock('land4', $land4);
      if(empty($land_building4)){
        $land_building4[] = array(
          'building_type' => $html_empty_building_type,
          'land_type' => $html_empty_land_type,
          'land_no' => ' - ',
          'land_addr_no' => ' - ',
          'land_district' => ' - ',
          'land_amphur' => ' - ',
          'land_province' => ' - ',
          'land_rai' => ' - ',
          'land_ngan' => ' - ',
          'land_wah' => ' - ',
          'building_no' => ' - ',
          'building_name' => ' - ',
          'building_moo' => ' - ',
          'building_road' => ' - ',
          'building_district' => ' - ',
          'building_amphur' => ' - ',
          'building_province' => ' - ',
          'type_amount' => ' - ',
          'type_amount_text' => ' - ',
          'type_date' => ' - ',
          'type_objective' => ' - '

        );
      }
      $TBS->MergeBlock('land_building4', $land_building4);
    	$TBS->MergeBlock('bond', $data_bond);
    	$TBS->MergeBlock('bookbank', $data_bookbank);
    	$TBS->MergeBlock('bondsman', $data_bondsman);
    	//$TBS->PlugIn(OPENTBS_DEBUG_XML_CURRENT);

    	$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
    	$output_file_name = 'loan_'.date('Y-m-d').'.docx';
      $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
      $TBS->Show(OPENTBS_FILE, $temp_file);

      $this->send_download($temp_file,$output_file_name);
    	//$TBS->Show(OPENTBS_DOWNLOAD, $output_file_name);


    }


    public function send_download($temp_file,$file) {
        $basename = basename($file);
        $length   = sprintf("%u", filesize($temp_file));

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $basename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $length);
        ob_clean();
        flush();
        set_time_limit(0);
        readfile($temp_file);
        exit();
    }


    public function report_loan_guarantee_bond_word($loan_id){

        $this->load->model('analysis/loan_analysis_model', 'loan_analysis');
        	include_once('word/class/tbs_class.php');
    		include_once('word/class/tbs_plugin_opentbs.php');

    		$TBS = new clsTinyButStrong;
    		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $interest_rate = '-';
        $analysis = $this->loan_analysis->getByLoan($loan_id);
    		//$this->load->model('payment_schedule_model');
      //  $payment_schedule_model = $this->payment_schedule_model->get_by_opt($opt = array('loan_id' => $loan_id));
            // echo "<pre>";
            // print_r($analysis);
            // echo "</pre>";
            if ($analysis) {
                $interest_rate = $analysis['loan_analysis_interest'];
            } else {
                $interest_rate = '-';
            }
            $loan_model = $this->loan_model->get_by_id($loan_id);
            $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);
            $person_model = $this->loan_lib->person_detail($loan_model['person_id']);
            $loan_guarantee_bond_model = $this->loan_lib->loan_guarantee_bond_report($loan_id);

            $sum_amount = 0;
            foreach ($loan_guarantee_bond_model as $key => $data) {
            	$sum_amount = $sum_amount + $data['loan_bond_amount_normal'];
            }
            $sum_amount_thai = num2wordsThai(money_num_to_str($sum_amount));
            $sum_amount = num2Thai(money_num_to_str($sum_amount));
    		// Retrieve the user name to display
    		$GLOBALS['loan_location'] = num2Thai($loan_model['loan_location']);
    		$GLOBALS['loan_datefull'] = num2Thai($loan_model['loan_datefull']);
    		$GLOBALS['personname'] = num2Thai($person_model['person_fullname']);
    		$GLOBALS['person_pre_card_no'] = num2Thai($person_model['person_addr_pre_no']);
    		$GLOBALS['person_pre_card_moo'] = num2Thai($person_model['person_addr_pre_moo']);
    		$GLOBALS['person_pre_card_road'] = num2Thai($person_model['person_addr_pre_road']);
    		$GLOBALS['person_pre_card_district_name'] = num2Thai($person_model['person_addr_pre_district_name']);
    		$GLOBALS['person_pre_card_amphur_name'] = num2Thai($person_model['person_addr_pre_amphur_name']);
    		$GLOBALS['person_pre_card_province_name'] = num2Thai($person_model['person_addr_pre_province_name']);
    		$GLOBALS['person_phone'] = num2Thai($person_model['person_phone']);
    		$GLOBALS['sum_amount'] = num2Thai($sum_amount);
    		$GLOBALS['sum_amount_text'] = $sum_amount_thai;
    		$GLOBALS['interest_rate'] = num2Thai($interest_rate);

    		$template = 'word/template/9_1_loan.docx';
    		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

    		$datax = array();
            foreach ($loan_guarantee_bond_model as $data) {
                $datax[] = array(
    	            'loan_bond_type'=> num2Thai($data['loan_bond_type']),
    	            'loan_bond_startnumber'=> num2Thai(money_num_to_str($data['loan_bond_startnumber'])),
                	'loan_bond_endnumber'=> num2Thai(money_num_to_str($data['loan_bond_endnumber'])),
    	            'loan_bond_regis_page'=> num2Thai($data['loan_bond_regis_page']),
    	            'loan_bond_regis_date'=> num2Thai(thai_display_date_short($data['loan_bond_regis_date'])),
    	            'loan_bond_amount'=> num2Thai($data['loan_bond_amount'])
                );
    		}
    		$TBS->MergeBlock('g', $datax);

    		// Delete comments
    		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
    		$output_file_name = 'loan_9_'.$loan_id.'_'.date('Y-m-d').'.docx';
        $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
    	  $TBS->Show(OPENTBS_FILE, $temp_file);
       	$this->send_download($temp_file,$output_file_name);
    }


    public function report_loan_bak($loan_id) {
        $loan_id = $this->uri->segment(3);
        $logo_invoice = 'assets\images\logo_invoice.png';

        ########################### Data ###########################
        //loan_model
        $loan_model = $this->loan_model->get_by_id($loan_id);

        //person_model
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);

        //spouse_model
        if ($loan_model['loan_spouse']) {
            $spouse_model = $this->loan_lib->person_detail($loan_model['loan_spouse']);
        } else {
            $spouse_model = array(
                'title_name' => '-',
                'person_fname' => '-',
                'person_lname' => '-',
                'person_age' => '-',
                'career_name' => '-',
                'person_addr_pre_no' => '-',
                'person_addr_pre_moo' => '-',
                'person_addr_pre_road' => '-',
                'person_addr_pre_district_name' => '-',
                'person_addr_pre_amphur_name' => '-',
                'person_addr_pre_province_name' => '-',
                'person_phone' => '-',
                'person_mobile' => '-',
                'person_email' => '-',
            );
        }

        //loan_type
        $loan_type_model = $this->loan_lib->loan_type_by_loan_id($loan_id);

        //loan_doc
        $loan_doc_model = $this->loan_doc_model->get_by_opt(array('loan_id' => $loan_id));

        //loan_income_model
        $loan_income_model = $this->loan_lib->loan_income_report($loan_model['person_id']);

        //loan_expenditure_model
        $loan_expenditure_model = $this->loan_lib->loan_expenditure_report($loan_model['person_id']);

        //loan_land_occupy_model
        $loan_land_occupy_model = $this->loan_lib->loan_land_occupy_report($loan_id);

        //loan_asset_model
        $loan_asset_model = $this->loan_lib->loan_asset_report($loan_id);

        //loan_debt_model
        $loan_debt_model = $this->loan_lib->loan_debt_report($loan_id);

        //loan_co_model
        $loan_co_model = $this->loan_lib->loan_co_report($loan_id);

        //loan_guarantee_land_model
        $loan_guarantee_land_model = $this->loan_lib->loan_guarantee_land_report($loan_id);

        //loan_guarantee_bond_model
        $loan_guarantee_bond_model = $this->loan_lib->loan_guarantee_bond_report($loan_id);

        //loan_guarantee_bookbank_model
        $loan_guarantee_bookbank_model = $this->loan_lib->loan_guarantee_bookbank_report($loan_id);

        //loan_guarantee_bondsman_model
        $loan_guarantee_bondsman_model = $this->loan_lib->loan_guarantee_bondsman_report($loan_id);

        //loan_objective
        $loan_model['loan_amount_thai'] = num2wordsThai($loan_model['loan_amount']);
        $loan_model['loan_amount'] = num2Thai(money_num_to_str($loan_model['loan_amount']));

        //config_land_type
        //$config_land_type = $this->mst_model->get_all('land_type');
        //config_building_type
        //$config_building_type = $this->mst_model->get_all('building_type');
        //master_sex
        $master_sex = $this->mst_model->get_all('sex');

        //master_status_married
        $master_status_married = $this->mst_model->get_all('status_married');

        //redeem_type
        //$redeem_type = $this->loan_type_model->get_redeem_type();
        //case_type
//        $case_type = $this->loan_type_model->get_case_type();
        //config_relationship
        $config_relationship = $this->mst_model->get_all('relationship');

        //bind_type
        $bind_type = $this->loan_guarantee_land_model->get_loan_guarantee_land_bind_type_list();

        //land_benefit
        $land_benefit = $this->loan_guarantee_land_model->get_loan_guarantee_land_benefit_list();


        ########################### Report ###########################
        //tcpdf
        $this->load->library('tcpdf');

        $filename = "loan_{$loan_id}_" . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        ############################################ Page set1
        // add a page
        $pdf->AddPage();

        $htmlcontent_gap = '<table width="100%"><tr><td width="100%"></td></tr></table>';

        //$htmlcontent
        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๑</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบบันทึกการขอสินเชื่อ ตามข้อบังคับสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)<br/>
					ว่าด้วย การให้สินเชื่อเพื่อป้องกันและแก้ไขปัญหาการสูญเสียสิทธิในที่ดินเกษตรกรรม พ.ศ. ๒๕๕๙
				</td>
			</tr>
		</table>';

        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';
        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">เขียนที่&nbsp;&nbsp;<u class="udotted">' . multi_space(1) . view_value(num2Thai($loan_model['loan_location'])) . multi_space(1) . '</u></td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่&nbsp;&nbsp;<u class="udotted">' . multi_space(1) . view_value(num2Thai($loan_model['loan_datefull'])) . multi_space(1) . '</u></td>
			</tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';

        /* $person_model */
        if ($person_model) {
            $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                    <td width="100%">' . multi_space(20) . 'ข้าพเจ้า <u class="udotted"> ' . multi_space(1) . $person_model['title_name'] . $person_model['person_fname'] . '&nbsp;&nbsp;' . $person_model['person_lname'] . multi_space(1) . '</u></td>
                            </tr>
                            <tr>
                                    <td width="35%">วันเกิด <u class="udotted"> ' . multi_space(1) . view_value(num2Thai($person_model['person_birthdate_thai'])) . multi_space(1) . '</u></td>
                                    <td width="15%">อายุ <u class="udotted"> ' . multi_space(1) . view_value(num2Thai($person_model['person_age'])) . multi_space(1) . '</u> ปี</td>
                                    <td width="15%">เพศ <u class="udotted"> ' . multi_space(1) . view_value($person_model['sex_name']) . multi_space(1) . '</u></td>
                                    <td width="20%">เชื้อชาติ <u class="udotted"> ' . multi_space(1) . view_value($person_model['race_name']) . multi_space(1) . '</u></td>
                            </tr>
                            <tr>
                                    <td width="20%">สัญชาติ <u class="udotted"> ' . multi_space(1) . view_value($person_model['nationality_name']) . multi_space(1) . '</u></td>
                                    <td width="20%">ศาสนา <u class="udotted"> ' . multi_space(1) . view_value($person_model['religion_name']) . multi_space(1) . '</u></td>
                                    <td width="60%">เลขประจำตัวประชาชน <u class="udotted"> ' . multi_space(1) . view_value(num2Thai($person_model['person_thaiid'])) . multi_space(1) . '</u></td>
                            </tr>
                            <tr>
                                    <td width="100%">วันออกบัตร <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_card_startdate_thai'])) . multi_space(1) . '</u>' . 'วันบัตรหมดอายุ <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_card_expiredate_thai'])) . multi_space(1) . '</u></td>
                            </tr>
                            <tr>
                                    <td width="100%">ที่อยู่ตามบัตรประจำตัวประชาชน เลขที่<u class="udotted">' . multi_space(1) . num2Thai($person_model['person_addr_card_no']) . multi_space(1) .
                    '</u> หมู่ที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_moo'])) . multi_space(1) .
                    '</u> ถนน<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_road'])) . multi_space(1) .
                    '</u> ตำบล/แขวง<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_district_name'])) . multi_space(1) .
                    '</u> อำเภอ/เขต<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_amphur_name'])) . multi_space(1) .
                    '</u> จังหวัด<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_province_name'])) . multi_space(1) .
                    '</u></td>
                            </tr>

                            <tr>
                                    <td width="100%">ที่อยู่ปัจจุบัน เลขที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_no'])) . multi_space(1) .
                    '</u> หมู่ที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_moo'])) . multi_space(1) .
                    '</u> ถนน<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_road'])) . multi_space(1) .
                    '</u> ตำบล/แขวง<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_district_name'])) . multi_space(1) .
                    '</u> อำเภอ/เขต<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_amphur_name'])) . multi_space(1) .
                    '</u> จังหวัด<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_province_name'])) . multi_space(1) .
                    '</u></td>
                            </tr>
                            <tr>
                                    <td width="30%">โทรศัพท์บ้าน <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_phone'])) . multi_space(1) . '</u></td>
                                    <td width="30%">โทรศัพท์มือถือ <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_mobile'])) . multi_space(1) . '</u></td>
                                    <td width="30%">E-mail <u class="udotted">' . multi_space(1) . view_value($person_model['person_email']) . multi_space(1) . '</u></td>
                            </tr>
                            <tr>
                                    <td width="30%">อาชีพ <u class="udotted">' . multi_space(1) . view_value($person_model['career_name']) . multi_space(1) . '</u></td>
                                    <td width="30%">สถานภาพ <u class="udotted">' . multi_space(1) . view_value($person_model['status_married_name']) . multi_space(1) . '</u></td>
                            </tr>
                            </table>';
        }

        /* $spouse_model */
        if ($spouse_model) {
            $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                    <td width="50%">ชื่อ-นามสกุล ของคู่สมรส <u class="udotted">' . multi_space(1) . $spouse_model['title_name'] . $spouse_model['person_fname'] . '&nbsp;&nbsp;' . $spouse_model['person_lname'] . multi_space(1) . '</u></td>
                                    <td width="20%">อายุ <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_age'])) . multi_space(1) . '</u>  ปี</td>
                                    <td width="30%">อาชีพ <u class="udotted">' . multi_space(1) . view_value($spouse_model['career_name']) . multi_space(1) . '</u></td>
                            </tr>
                            <tr>
                                    <td width="100%">ที่อยู่ เลขที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_addr_pre_no'])) . multi_space(1) .
                    '</u> หมู่ที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_addr_pre_moo'])) . multi_space(1) .
                    '</u> ถนน <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_addr_pre_road'])) . multi_space(1) .
                    '</u> ตำบล/แขวง <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_addr_pre_district_name'])) . multi_space(1) .
                    '</u> อำเภอ/เขต <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_addr_pre_amphur_name'])) . multi_space(1) .
                    '</u> จังหวัด <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_addr_pre_province_name'])) . multi_space(1) .
                    '</u></td>
                            </tr>
                            <tr>
                                    <td width="30%">โทรศัพท์บ้าน <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_phone'])) . multi_space(1) . '</u></td>
                                    <td width="30%">โทรศัพท์มือถือ <u class="udotted">' . multi_space(1) . view_value(num2Thai($spouse_model['person_mobile'])) . multi_space(1) . '</u></td>
                                    <td width="30%">E-mail <u class="udotted">' . multi_space(1) . view_value($spouse_model['person_email']) . multi_space(1) . '</u></td>
                            </tr>
                    </table>';
        }

        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="40%">จำนวนสมาชิกในครัวเรือน <u class="udotted">' . multi_space(1) . view_value(num2Thai($loan_model['loan_member_amount'])) . multi_space(1) . '</u> คน</td>
				<td width="40%">จำนวนแรงงานในครัวเรือน <u class="udotted">' . multi_space(1) . view_value(num2Thai($loan_model['loan_labor_amount'])) . multi_space(1) . '</u> คน</td>
                        </tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">' . multi_space(20) . 'ข้าพเจ้าขอให้ถ้อยคำต่อ ' . multi_dot(50) . ' ตำแหน่ง ' . multi_dot(50) . '</td>
			</tr>
                        <tr>
				<td width="100%">ด้วยความสัตย์จริง และขอให้รายละเอียดประกอบการพิจารณาในการแสดงความประสงค์ขอรับสินเชื่อ<br/>
                                ตามข้อบังคับสถาบันบริหารจัดการธนาคารที่ (องค์การมหาชน) ว่าด้วย การให้สินเชื่อเพื่อป้องกันและแก้ไขปัญหา<br/>
                                การสูญเสียสิทธิในที่ดินเกษตรกรรม พ.ศ. ๒๕๕๙ ดังต่อไปนี้
                                </td>
			</tr>
		</table>';

        /* loan_type_model */
        if ($loan_type_model) {
            $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%"></td>
			</tr>
		</table>';
            $htmlcontent .= '<div style="page-break-before: always;"> </div>';
            $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">' . multi_space(20) . '๑. ก©ารขอสินเชื่อ</td>
			</tr>
		</table>';

            foreach ($loan_type_model as $key => $data) {
                //loan_objective_id == 1
                if ($data['loan_objective_id'] == 1) {
                    $htmlcontent .= $htmlcontent_gap;
                    $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">' . multi_space(24) . '() ' . $data['loan_objective_name'] . 'จาก ' . $this->pdf_redeem_type($data['loan_type_redeem_type']) . '</td>
			</tr>
                    </table>';

                    $htmlcontent .='<table width="100%">
			<tr>
                            <td width="100%">' . multi_space(28) . 'สาเหตุของการนำที่ดินไปจำนองหรือขายฝาก</td>
			</tr>
                        <tr>
                            <td width="100%"><u class="udotted">' . multi_space(1) . view_value(num2Thai(nl2br($data['loan_type_redeem_case']))) . multi_space(1) . '</u></td>
			</tr>
                    </table>';

                    //land_list
                    if ($data['land_list']) {
                        $htmlcontent .= $this->pdf_land_list($data['land_list']);
                    }

                    $htmlcontent .='<table width="100%">
			<tr>
                            <td width="100%">จำนวนเงินที่นำไปจำนองหรือขายฝาก <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_redeem_amount'])) . multi_space(1) . '</u> บาท ( <u class="udotted">' . multi_space(1) . view_value($data['loan_type_redeem_amount_thai']) . multi_space(1) . '</u> )</td>
			</tr>
                        <tr>
                            <td width="100%">ชื่อ - นามสกุลของเจ้าของที่ดินที่นำไปจำนองหรือขายฝาก <u class="udotted">' . multi_space(1) . view_value($data['loan_type_redeem_owner_fullname']) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
                            <td width="100%">อายุ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_redeem_owner_info']['person_age'])) . multi_space(1) . '</u> ปี ที่อยู่ปัจจุบัน เลขที่<u class="udotted">' . view_value(num2Thai($data['loan_type_redeem_owner_info']['person_addr_pre_no'])) . multi_space(1) . '</u>  หมู่ที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_redeem_owner_info']['person_addr_pre_moo'])) . multi_space(1) . '</u>  ถนน<u class="udotted">' . multi_space(1) . view_value($data['loan_type_redeem_owner_info']['person_addr_pre_road']) . multi_space(1) . '</u>  ตำบล/แขวง<u class="udotted">' . multi_space(1) . view_value($data['loan_type_redeem_owner_info']['person_addr_pre_district_name']) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
                            <td width="100%">อำเภอ/เขต<u class="udotted">' . multi_space(1) . view_value($data['loan_type_redeem_owner_info']['person_addr_pre_amphur_name']) . multi_space(1) . '</u>  จังหวัด<u class="udotted">' . multi_space(1) . view_value($data['loan_type_redeem_owner_info']['person_addr_pre_province_name']) . multi_space(1) . '</u>  โทรศัพท์บ้าน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_redeem_owner_info']['person_phone'])) . multi_space(1) . '</u>  โทรศัพท์มือถือ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_redeem_owner_info']['person_mobile'])) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
                            <td width="100%">โดยมีความสัมพันธ์กับผู้ขอสินเชื่อ <u class="udotted">' . multi_space(1) . view_value($data['relationship_name']) . multi_space(1) . '</u></td>
			</tr>
                    </table>';
                }
                //loan_objective_id == 2
                elseif ($data['loan_objective_id'] == 2) {
                    $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">'.multi_space(24).'() ' . $data['loan_objective_name'] . '</td>
			</tr>
                    </table>';

                    //land_list
                    if ($data['land_list']) {
                        $htmlcontent .= $this->pdf_land_list($data['land_list']);
                    }

                    $htmlcontent .='<table width="100%">
			<tr>
				<td width="100%">จำนวนเงินเพื่อนำไปชำระหนี้ตามสัญญากู้ยืมเงิน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_contract_amount'])) . multi_space(1) . '</u> บาท ( <u class="udotted">' . multi_space(1) . view_value($data['loan_type_contract_amount_thai']) . multi_space(1) . '</u> )</td>
			</tr>
                        <tr>
				<td width="100%">สํญญาเงินกู้ยืม เลขที่ <u class="udotted">' . multi_space(1) . view_value($data['loan_type_contract_no']) . multi_space(1) . '</u> ลงวันที่ <u class="udotted">' . multi_space(1) . view_value(thai_display_date($data['loan_type_contract_date'])) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
				<td width="100%">ระหว่าง ผู้ให้กู้ยืม <u class="udotted">' . multi_space(1) . view_value($data['loan_type_contract_creditor_fullname']) . multi_space(1) . '</u> กับ <u class="udotted">' . multi_space(1) . view_value($data['loan_type_contract_borrowers_fullname']) . multi_space(1) . '</u> ผู้กู้ยืม</td>
			</tr>
                    </table>';

                    $htmlcontent .= $htmlcontent_gap;
                }
                //loan_objective_id == 3
                elseif ($data['loan_objective_id'] == 3) {
                    $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">' . multi_space(24) . '() ' . $data['loan_objective_name'] . '</td>
			</tr>
                    </table>';

                    //land_list
                    if ($data['land_list']) {
                        $htmlcontent .= $this->pdf_land_list($data['land_list']);
                    }

                    $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">คำพิพากษา คดีหมายเลขดำที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_case_black_no'])) . multi_space(1) . '</u>  คดีหมายเลขแดงที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_case_red_no'])) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
                            <td width="20%">ศาล <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_case_court'])) . multi_space(1) . '</u></td>
                            <td width="80%">วันที่ <u class="udotted">' . multi_space(1) . view_value(thai_display_date($data['loan_type_case_date'])) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
                            <td width="100%">ประเภทคดีความ ' . $this->pdf_case_type($data['loan_type_case_type']) . '</td>
			</tr>
                        <tr>
                            <td width="40%">คดีระหว่าง <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_case_plaintiff_fullname'])) . multi_space(1) . '</u> โจทก์</td>
                            <td width="40%"><u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_case_defendant_fullname'])) . multi_space(1) . '</u> จำเลย</td>
			</tr>
                        <tr>
                            <td width="100%">มูลเหตุแห่งคดีความ</td>
			</tr>
                        <tr>
                            <td width="100%"><u class="udotted">' . multi_space(1) . view_value(num2Thai(nl2br($data['loan_type_case_comment']))) . multi_space(1) . '</u></td>
			</tr>
                    </table>';

                    $htmlcontent .= $htmlcontent_gap;
                }
                //loan_objective_id == 4
                elseif ($data['loan_objective_id'] == 4) {
                    $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">' . multi_space(24) . '() ' . $data['loan_objective_name'] . '</td>
			</tr>
                    </table>';

                    //land_list
                    if ($data['land_list']) {
                        $htmlcontent .= $this->pdf_land_list($data['land_list']);
                    }

                    $htmlcontent .='<table width="100%">
			<tr>
				<td width="100%">เพื่อซื้อที่ดินที่ถูกขายทอดตลาดหรือหลุดขายฝากไปแล้ว เป็นจำนวนเงิน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_sale_amount'])) . multi_space(1) . '</u> บาท</td>
			</tr>
                        <tr>
				<td width="100%">( <u class="udotted">' . multi_space(1) . view_value($data['loan_type_sale_amount_thai']) . multi_space(1) . '</u> ) ที่ดินได้ถูกขายทอดตลาดหรือหลุดขายฝากไปแล้ว</td>
			</tr>
                        <tr>
				<td width="100%">เมื่อวันที่ <u class="udotted">' . multi_space(1) . view_value($data['loan_type_sale_date']) . multi_space(1) . '</u></td>
			</tr>
                    </table>';

                    $htmlcontent .= $htmlcontent_gap;
                }
                //loan_objective_id == 5
                elseif ($data['loan_objective_id'] == 5) {
                    $htmlcontent .= $htmlcontent_gap;
                    $htmlcontent .='<table width="100%">
			<tr>
				<td width="100%">'.multi_space(24).'() ' . $data['loan_objective_name'] . '</td>
			</tr>
                    </table>';

                    $htmlcontent .='<table width="100%">
			<tr>
				<td width="100%">'.multi_space(28).'โดยขอสินเชื่อเพื่อไปประกอบอาชีพเกษตรกรรม (ระบุประเภทของเกษตรกรรม)</td>
			</tr>
                        <tr>
				<td width="100%"><u class="udotted">' . multi_space(1) . view_value(num2Thai(nl2br($data['loan_type_farm_for']))) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
				<td width="100%">สถานที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai(nl2br($data['loan_type_farm_location']))) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
				<td width="100%">โครงการที่นำเสนอ (ระบุรายละเอียด หรือตามที่ระบุในเอกสารแนบ)</td>
			</tr>
                        <tr>
				<td width="100%"><u class="udotted">' . multi_space(1) . view_value(num2Thai(nl2br($data['loan_type_farm_project']))) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
				<td width="100%">โดยนำไปใช้เป็นค่าใช้จ่าย ดังนี้</td>
			</tr>
                        <tr>
				<td width="100%"><u class="udotted">' . multi_space(1) . view_value(num2Thai(nl2br($data['loan_type_farm_expenditure']))) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
				<td width="100%">คาดว่าจะมีรายได้จากโครงการที่เสนอ ตั้งแต่ปีที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_farm_income_yearstart'])) . multi_space(1) . '</u>  ของโครงการ รายได้สุทธิ ปีละ <u class="udotted">  ' . multi_space(1) . view_value(num2Thai($data['loan_type_farm_income_per_year'])) . multi_space(1) . '</u> บาท</td>
			</tr>
                        <tr>
				<td width="100%">และจะเริ่มชำระคืนต้นเงินและดอกเบี้ย ตั้งแต่ปีที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_farm_payment_yearstart'])) . '</u> เป็นเงินจำนวน <u class="udotted">  ' . multi_space(1) . view_value(num2Thai($data['loan_type_farm_payment_amount'])) . multi_space(1) . '</u> บาท</td>
			</tr>
                        <tr>
				<td width="100%">โดยคาดว่าจะชำระเสร็จสิ้น ภายในกำหนดเวลา <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_type_farm_year_amount'])) . multi_space(1) . '</u> ปี</td>
			</tr>
                    </table>';
                }
            }
        }

        /* $loan_doc_model */
        $htmlcontent_loan_doc = '';
        if ($loan_doc_model) {
            foreach ($loan_doc_model as $loan_doc) {
                if ($loan_doc['loan_doc_date']) {
                    $loan_doc_date = 'ลงวันที่ <u class="udotted">' . multi_space(1) . view_value(thai_display_date($loan_doc['loan_doc_date'])) . multi_space(1) . '</u>';
                } else {
                    $loan_doc_date = '';
                }

                $htmlcontent_loan_doc .= ''
                        . '<tr>'
                        . '<td width="100%">'.multi_space(24).'' . $html_checkbox . ' ' . $loan_doc['loan_doc_type_name'] . multi_space(3) . $loan_doc_date . '</td>'
                        . '</tr>';
            }
        }

        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(20).'๒. เอกสารหลักฐานที่นำมาประกอบการยื่นคำขอสินเชื่อ</td>
			</tr>
                        ' . $htmlcontent_loan_doc . '
		</table>';

        /* $loan_income_model */
        $y_curr = date('Y') + 543;
        if ($loan_income_model) {
            $loan_income[1] = '';
            $loan_income[2] = '';
            $loan_income[3] = '';

            foreach ($loan_income_model[$y_curr] as $key_income => $data_income) {
                $loan_income[$key_income] = '';
                if ($data_income) {
                    foreach ($data_income as $key_income2 => $data_income2) {
                        $num_row = $key_income2 + 1;
                        $loan_income[$key_income] .= ''
                                . '<tr>'
                                . '<td width="100%">'.multi_space(28).'' . num2Thai($num_row) . ') <u class="udotted">' . multi_space(1) . view_value($data_income2['loan_activity_name']) . multi_space(1) . '</u>'
                                . multi_space(3) . 'เป็นเงิน <u class="udotted">' . multi_space(3) . view_value(num2Thai($data_income2['loan_activity_income'])) . multi_space(3) . '</u>บาท</td>'
                                . '</tr>';
                    }
                }
            }

            $htmlcontent .= $htmlcontent_gap;
            $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๓. รายได้ในครัวเรือนของข้าพเจ้า ในรอบปี <u class="udotted">' . multi_space(1) . view_value(num2Thai($y_curr)) . multi_space(1) . '</u> ที่ผ่านมา</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'๓.๑ รายได้จากการทำการเกษตร (ระบุกิจกรรม)</td>
                            </tr>
                            ' . $loan_income[1] . '
                            <tr>
                                <td width="100%">'.multi_space(24).'๓.๒ รายได้นอกการทำการเกษตร (ระบุกิจกรรม)</td>
                            </tr>
                            ' . $loan_income[2] . '
                            <tr>
                                <td width="100%">'.multi_space(24).'๓.๓ รายได้อื่นๆ (ระบุกิจกรรม)</td>
                            </tr>
                            ' . $loan_income[3] . '
                    </table>';
        } else {
            $htmlcontent .= $htmlcontent_gap;
            $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๓. รายได้ในครัวเรือนของข้าพเจ้า ในรอบปี <u class="udotted">' . multi_space(1) . view_value(num2Thai($y_curr)) . multi_space(1) . '</u> ที่ผ่านมา</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'๓.๑ รายได้จากการทำการเกษตร (ระบุกิจกรรม)</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'๓.๒ รายได้นอกการทำการเกษตร (ระบุกิจกรรม)</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'๓.๓ รายได้อื่นๆ (ระบุกิจกรรม)</td>
                            </tr>
                    </table>';
        }

        /* $loan_expenditure_model */
        $y_curr = date('Y') + 543;
        if ($loan_expenditure_model) {
            $loan_expenditure[1] = '';
            $loan_expenditure[2] = '';
            $loan_expenditure[3] = '';
            $loan_expenditure[4] = '';

            foreach ($loan_expenditure_model[$y_curr] as $key_expenditure => $data_expenditure) {
                $loan_expenditure[$key_expenditure] = '';
                if ($data_expenditure) {
                    foreach ($data_expenditure as $key_expenditure2 => $data_expenditure2) {
                        $num_row = $key_expenditure2 + 1;
                        $loan_expenditure[$key_expenditure] .= ''
                                . '<tr>'
                                . '<td width="100%">'.multi_space(28).'' . num2Thai($num_row) . ') <u class="udotted">' . multi_space(1) . view_value($data_expenditure2['loan_expenditure_name']) . multi_space(1) . '</u>'
                                . multi_space(3) . 'เป็นเงิน <u class="udotted">' . multi_space(3) . view_value(num2Thai($data_expenditure2['loan_expenditure_amount'])) . multi_space(3) . '</u> บาท</td>'
                                . '</tr>';
                    }
                }
            }

            $htmlcontent .= $htmlcontent_gap;
            $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๔. ค่าใช้จ่ายในครัวเรือนของข้าพเจ้า ในรอบปี <u class="udotted">' . multi_space(1) . view_value(num2Thai($y_curr)) . multi_space(1) . '</u> ที่ผ่านมา</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายในการลงทุนทกำการเษตร</td>
                            </tr>
                            ' . $loan_expenditure[1] . '
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายในการลงทุนนอกการเกษตร</td>
                            </tr>
                            ' . $loan_expenditure[2] . '
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายในครัวเรือน</td>
                            </tr>
                            ' . $loan_expenditure[3] . '
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายอื่นๆ (ระบุ)</td>
                            </tr>
                            ' . $loan_expenditure[4] . '
                    </table>';
        } else {
            $htmlcontent .= $htmlcontent_gap;
            $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๔. ค่าใช้จ่ายในครัวเรือนของข้าพเจ้า ในรอบปี <u class="udotted">' . multi_space(1) . view_value(num2Thai($y_curr)) . multi_space(1) . '</u> ที่ผ่านมา</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายในการลงทุนทกำการเษตร</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายในการลงทุนนอกการเกษตร</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายในครัวเรือน</td>
                            </tr>
                            <tr>
                                <td width="100%">'.multi_space(24).'ค่าใช้จ่ายอื่นๆ (ระบุ)</td>
                            </tr>
                    </table>';
        }

        /* $loan_land_occupy_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๕. ที่ดินที่ข้าพเจ้าและคู่สมรสถือครองทั้งหมด มีดังนี้</td>
                            </tr>
                    </table>';
        if ($loan_land_occupy_model) {
            foreach ($loan_land_occupy_model as $key => $data) {
                $num_row = $key + 1;

                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(24).'แปลที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($num_row)) . multi_space(1) . '</u> ผู้ถือกรรมสิทธิ์ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['owner_fullname'])) . multi_space(1) . '</u>
                            เนื้อที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_area_rai'])) . '-' . view_value(num2Thai($data['land_detail']['land_area_ngan'])) . '-' . view_value(num2Thai($data['land_detail']['land_area_wah'])) . multi_space(1) . '</u> ไร่</td>
			</tr>
                        <tr>
                            <td width="100%">หนังสือแสดงสิทธิในที่ดินประเภท <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_type_name'])) . multi_space(1) . '</u> เลขที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_no'])) . multi_space(1) . '</u></td>
			</tr>
                        <tr>
                            <td width="100%">ตั้งอยู่ที่ เลขที่ดิน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_addr_no'])) . multi_space(1) .
                        '</u> หมู่ที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_addr_moo'])) . multi_space(1) .
                        '</u> ตำบล/แขวง <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_addr_district_name'])) . multi_space(1) .
                        '</u> อำเภอ/เขต <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_addr_amphur_name'])) . multi_space(1) .
                        '</u> จังหวัด <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['land_detail']['land_addr_province_name'])) . multi_space(1) .
                        '</u></td>
                        </tr>
                        <tr>
                            <td width="100%">การใช้ประโยชน์ในที่ดิน <u class="udotted">' . view_value(num2Thai($data['loan_land_occupy_benefit'])) . '</u> </td>
			</tr>
		</table>';
            }
        }

        /* $loan_asset_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๖. ข้าพเจ้าและคู่สมรสมีทรัพย์สินอื่นนอกจากที่ดิน (เช่น สิ่งปลูกสร้าง เครื่องจักรกล ฯลฯ) ดังนี้</td>
                            </tr>
                    </table>';
        if ($loan_asset_model) {
            $htmlcontent .='<table width="100%">';
            foreach ($loan_asset_model as $key => $data) {
                $num_row = $key + 1;
                $htmlcontent .='<tr>'
                        . '<td width="100%">'.multi_space(24)
                        . num2Thai($num_row) . ') '
                        . '<u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_asset_name'])) . multi_space(1) . '</u>' . multi_space(1)
                        . 'เป็นเงิน <u class="udotted">' . multi_space(1) . view_value(num2Thai(money_num_to_str($data['loan_asset_amount']))) . multi_space(1) . '</u> บาท  '
                        . 'จำนวน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_asset_quantity'])) . multi_space(1) . '</u>' . multi_space(1) . num2Thai($data['loan_asset_uom_name'])
                        . '</td>'
                        . '</tr>';
            }
            $htmlcontent .='</table>';
        }

        /* $loan_debt_model */
        if ($loan_debt_model) {
            $check2 = $html_checkbox;
            $check1 = $html_uncheckbox;
        } else {
            $check2 = $html_uncheckbox;
            $check1 = $html_checkbox;
        }

        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(20).'๗. ข้าพเจ้าและคู่สมรสมีหนี้สิน ดังนี้</td>
			</tr>
                        <tr>
                            <td width="100%">'.multi_space(24).'' . $check1 . ' ไม่มี</td>
			</tr>
                        <tr>
                            <td width="100%">'.multi_space(24).'' . $check2 . ' มี (ระบุ)</td>
			</tr>
		</table>';

        if ($loan_debt_model) {
            $htmlcontent .='<table width="100%" border="1" cellpadding="0" cellspacing="0">';
            $htmlcontent .='
                <tr>
                    <th width="7%">ลำดับ</th>
                    <th width="15%">รายการ<br/>หนี้สิน</th>
                    <th width="15%">หนี้<br/>เริ่มต้น</th>
                    <th width="15%">หนี้<br/>คงเหลือ</th>
                    <th width="15%">จำนวนหนี้ที่<br/>ชำระต่อปี</th>
                    <th width="15%">เป็นหนี้<br/>เมื่อ</th>
                    <th width="15%">สิ้นสุด<br/>เมื่อ</th>
                </tr>
            ';

            foreach ($loan_debt_model as $key => $data) {
                $num_row = $key + 1;
                $htmlcontent .='
                    <tr>
                        <td>' . num2Thai($num_row) . '</td>
                        <td>' . num2Thai($data['loan_debt_name']) . '</td>
                        <td>' . num2Thai($data['loan_debt_amount_start']) . '</td>
                        <td>' . num2Thai($data['loan_debt_amount_remain']) . '</td>
                        <td>' . num2Thai($data['loan_debt_amount']) . '</td>
                        <td>' . num2Thai($data['loan_debt_date_start']) . '</td>
                        <td>' . num2Thai($data['loan_debt_date_end']) . '</td>
                    </tr>
                ';
            }

            $htmlcontent .='<table>';
        }

        /* $loan_co_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                            <tr>
                                <td width="100%">'.multi_space(20).'๘. ผู้กู้ร่วม (ถ้ามี)</td>
                            </tr>
                    </table>';


        if ($loan_co_model) {

            foreach ($loan_co_model as $key => $data) {
                $num_row = $key + 1;
                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(24).'๘.' . num2Thai($num_row) . ' ชื่อ - นามสกุล <u class="udotted">' . multi_space(1) . view_value($data['person_detail']['person_fullname']) . multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">
                                วันเกิด (วัน/เดือน/ปี) <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_birthdate_thai'])) . multi_space(1) . '</u>
                                อายุ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_age'])) . ' ปี' . multi_space(1) . '</u>
                                เพศ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['sex_name'])) . multi_space(1) . '</u>
                                เชื้อชาติ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['race_name'])) . multi_space(1) . '</u>
                            </td>
			</tr>
                        <tr>
                            <td width="100%">
                                สัญชาติ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['nationality_name'])) . multi_space(1) . '</u>
                                ศาสนา <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['religion_name'])) . multi_space(1) . '</u>
                                เลขบัตรประจำตัวประชาชน <u class="udotted">' . multi_space(1). view_value(formatThaiid(num2Thai($data['person_detail']['person_thaiid']))) . multi_space(1) . '</u>
                            </td>
			</tr>
                        <tr>
                            <td width="100%">
                                วันออกบัตร <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_card_startdate_thai'])) . multi_space(1) . '</u>
                                วันบัตรหมดอายุ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_card_expiredate_thai'])) . multi_space(1) . '</u>
                            </td>
			</tr>
                        <tr>
                            <td width="100%">
                                ที่อยู่ตามบัตรประชาชน เลขที่ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_addr_card_no'])) . multi_space(1) . '</u>
                                หมู่ที่ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_addr_card_moo'])) . multi_space(2) . '</u>
                                ถนน <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_addr_card_road'])) . multi_space(2) . '</u>
                                ตำบล/แขวง <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_addr_card_district_name'])) . multi_space(1) . '</u>
                                อำเภอ/เขต <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_addr_card_amphur_name'])) . multi_space(1) . '</u>
                                จังหวัด <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_addr_card_province_name'])) . multi_space(2) . '</u>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                โทรศัพท์บ้าน <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_phone'])) . multi_space(2) . '</u>
                                โทรศัพท์มือถือ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_mobile'])) . multi_space(2) . '</u>
                                E-mail <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['person_email'])) . multi_space(2) . ' </u>
                            </td>
			</tr>
                        <tr>
                            <td width="100%">
                                อาชีพ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['person_detail']['career_name'])) . multi_space(2) . '</u>
                                รายได้ต่อเดือน <u class="udotted">' . multi_space(1). view_value(num2Thai(money_num_to_str($data['person_detail']['person_income_per_month']))) . multi_space(2) . '</u>บาท' . '
                                รายจ่ายต่อเดือน <u class="udotted">' . multi_space(1). view_value(num2Thai(money_num_to_str($data['person_detail']['person_expenditure_per_month']))) . multi_space(2) . '</u>บาท' . '
                            </td>
			</tr>
                        <tr>
                            <td width="100%">
                                ความสัมพันธ์กับผู้ขอสินเชื่อ <u class="udotted">' . multi_space(1). view_value(num2Thai($data['relationship_name'])) . multi_space(2) . '</u>
                            </td>
			</tr>
                        <tr>
                            <td width="100%">&nbsp;</td>
			</tr>
		</table>';
            }
        }

        /* loan_guarantee */
        $check_loan_guarantee_land = $loan_guarantee_land_model ? $html_checkbox : $html_uncheckbox;
        $check_loan_guarantee_bond = $loan_guarantee_bond_model ? $html_checkbox : $html_uncheckbox;
        $check_loan_guarantee_bookbank = $loan_guarantee_bookbank_model ? $html_checkbox : $html_uncheckbox;
        $check_loan_guarantee_bondsman = $loan_guarantee_bondsman_model ? $html_checkbox : $html_uncheckbox;

        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(20).'๙. ในการขอรับสินเชื่อข้าพเจ้า ขอเสนอหลักประกันการใช้สินเชื่อ ดังนี้</td>
			</tr>
                        <tr>
                            <td width="100%">'.multi_space(24).'' . $check_loan_guarantee_land . ' อสังหาริมทรัพย์</td>
			</tr>
                        <tr>
                            <td width="100%">'.multi_space(24).'' . $check_loan_guarantee_bond . ' พันธบัตรรัฐบาล</td>
			</tr>
                        <tr>
                            <td width="100%">'.multi_space(24).'' . $check_loan_guarantee_bookbank . ' เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์</td>
			</tr>
                        <tr>
                            <td width="100%">'.multi_space(24).'' . $check_loan_guarantee_bondsman . ' บุคคลค้ำประกัน</td>
			</tr>
		</table>';

        /* $loan_guarantee_land_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(24).'๙.๑ อสังหาริมทรัพย์</td>
			</tr>
		</table>';
        if ($loan_guarantee_land_model) {
            foreach ($loan_guarantee_land_model as $key => $data) {
                $land_list[0] = $data['land_list'];
                $htmlcontent .= $this->pdf_land_list($land_list);

                //loan_guarantee_land_bind
                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="60%">'.multi_space(28). $this->pdf_land_bind_type($data['loan_guarantee_land_bind_type']) . '</td>
                            <td width="40%">โดยการจำนอง กับ <u class="udotted">' .multi_space(1). view_value(num2Thai($data['loan_guarantee_land_fullname'])) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">วงเงิน <u class="udotted">' .multi_space(1). view_value(num2Thai($data['loan_guarantee_land_bind_amount'])) .multi_space(1). '</u>บาท</td>
			</tr>
		</table>';

                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="60%">' .multi_space(28) . $this->pdf_land_bind_type($data['loan_guarantee_land_bind_type']) . '</td>
                            <td width="40%">โดยการผ่อนชำระ กับ <u class="udotted">' .multi_space(1). view_value(num2Thai($data['loan_guarantee_land_relief'])) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">เดือนละ <u class="udotted">' .multi_space(1). view_value(num2Thai($data['loan_guarantee_land_relief_permonth'])) .multi_space(1). '</u>บาท' . multi_space(4) . 'วันครบกำหนดผ่อนชำระ <u class="udotted">' .multi_space(1). view_value(num2Thai($data['loan_guarantee_land_relief_enddate'])) .multi_space(1). '</u></td>
			</tr>
		</table>';

                $htmlcontent .= $this->pdf_loan_guarantee_land_benefit($data['loan_guarantee_land_benefit'], $data['loan_guarantee_land_benefit_other']);
            }
        }

        /* $loan_guarantee_bond_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(24).'๙.๒ ชนิดพันธบัตรรัฐบาล</td>
			</tr>
		</table>';
        if ($loan_guarantee_bond_model) {
            foreach ($loan_guarantee_bond_model as $key => $data) {
                $num_row = $key + 1;
                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(28) . num2Thai($num_row) . ') ชื่อ - นามสกุลของเจ้าของกรรมสิทธิ์ <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_owner']) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">เลขประจำตัวประชาชน <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_thaiid']) .multi_space(1). '</u>' . multi_space(4) . 'เลขประจำตัวผู้เสียภาษี (ถ้ามี) <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_thaiid']) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">ชนิดพันธบัตร <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_type']) . '</u>' . multi_space(4) .
                        'เลขที่ต้น <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_startnumber']) .multi_space(1). '</u>' . multi_space(4) .
                        'เลขที่ท้าย <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_endnumber']) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">ราคาที่ตราไว้ <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_amount']) . multi_space(1) .
                        '</u>บาท  ( <u class="udotted">' . multi_space(1) . num2Thai($data['loan_bond_amount_thai']) . multi_space(1) . '</u>)</td>
			</tr>
		</table>';
            }
        }

        /* $loan_guarantee_bookbank_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(24).'๙.๓ เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์</td>
			</tr>
		</table>';
        if ($loan_guarantee_bookbank_model) {

            foreach ($loan_guarantee_bookbank_model as $key => $data) {
                $num_row = $key + 1;
                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(28) . num2Thai($num_row) . ') ชื่อ - นามสกุลของเจ้าของบัญชี <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_bookbank_owner'])) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">ธนาคารหรือสถาบันการเงินหรือสหกรณ์ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['bank_name'])) .multi_space(1). '</u>' . multi_space(4) .
                        'สาขา (ถ้ามี)' . multi_space(1) . num2Thai($data['loan_bookbank_branch']) . '</td>
			</tr>
                        <tr>
                            <td width="100%">เลขที่บัญชี <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_bookbank_no'])) .multi_space(1). '</u>' . multi_space(4) .
                        'ประเภทของการฝาก <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_bookbank_type_name'])) .multi_space(1). '</u>' . multi_space(4) .
                        'ซึ่งมียอดเงินฝาก ณ วันที่ <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_bookbank_date'])) .multi_space(1). '</u></td>
			</tr>
                        <tr>
                            <td width="100%">จำนวนเงิน <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_bookbank_amount'])) . multi_space(1) .
                        '</u> บาท  ( <u class="udotted">' . multi_space(1) . view_value(num2Thai($data['loan_bookbank_amount_thai'])) . multi_space(1) . '</u> )</td>
			</tr>
		</table>';
            }
        }


        /* $loan_guarantee_bondsman_model */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(24).'๙.๔ บุคคลค้ำประกัน จำนวน ' . num2Thai(count($loan_guarantee_bondsman_model)) . ' คน มีรายละเอียดดังต่อไปนี้</td>
                        </tr>
                </table>';
        if ($loan_guarantee_bondsman_model) {
            foreach ($loan_guarantee_bondsman_model as $key => $data) {
                $num_row = $key + 1;

                if ($data['person_detail']['person_loan_other_check'] == 1) {
                    $loan_other_check0 = $html_uncheckbox;
                    $loan_other_check1 = $html_checkbox;
                } else {
                    $loan_other_check0 = $html_checkbox;
                    $loan_other_check1 = $html_uncheckbox;
                }

                if ($data['person_detail']['person_guarantee_other_amount_check'] == 1) {
                    $guarantee_other_check0 = $html_uncheckbox;
                    $guarantee_other_check1 = $html_checkbox;
                } else {
                    $guarantee_other_check0 = $html_checkbox;
                    $guarantee_other_check1 = $html_uncheckbox;
                }

                if($key > 0){
                    $htmlcontent .= $htmlcontent_gap;
                }

                $htmlcontent .='<table width="100%">
                        <tr>
                            <td width="100%">'.multi_space(28) . num2Thai($num_row) . ') ชื่อ - นามสกุลของผู้ค้ำประกัน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_fullname'])) .
                        '</u> อายุ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_age'])) . '</u> ปี</td>
                        </tr>
                        <tr>
                            <td width="100%">ที่อยู่ปัจจุบัน เลขที่ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_addr_pre_no'])) .
                        '</u> หมู่ที่ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_addr_pre_moo'])) .
                        '</u> ถนน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_addr_pre_road'])) .
                        '</u> ตำบล/แขวง <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_addr_pre_district_name'])) .
                        '</u> อำเภอ/เขต <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_addr_pre_amphur_name'])) .
                        '</u> จังหวัด <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_addr_pre_province_name'])) . '</u></td>
                        </tr>
                        <tr>
                            <td width="100%">โทรศัพท์บ้าน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_phone'])) .
                        '</u> โทรศัพท์มือถือ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_mobile'])) .
                        '</u> E-mail <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_email'])) .
                        '</u> อาชีพ <u class="udotted">' . view_value(num2Thai($data['person_detail']['career_name'])) .
                        '</u> ตำแหน่ง <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_position'])) .
                        '</u> ลักษณะงาน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_job_desc'])) .
                        '</u> </td>
                        </tr>
                        <tr>
                            <td width="100%">ลักษณะธุรกิจ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_type_business'])) .
                        '</u> สังกัด <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_belong'])) .
                        '</u> กระทรวง <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_ministry'])) .
                        '</u> สถานที่ทำงาน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_workplace'])) . '</u></td>
                        </tr>
                        <tr>
                            <td width="100%">ถือบัตรข้าราชการ/บัตรประจำตัวประชาชน เลขที่ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_cardid_emp'])) .
                        '</u> วันออกบัตร <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_cardid_emp_startdate'])) . '</u></td>
                        </tr>
                        <tr>
                            <td width="100%">วันบัตรหมดอายุ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_cardid_emp_expiredate'])) .
                        '</u> เงินเดือน/ค่าจ้าง/รายได้ต่อเดือน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_income_per_month'])) . '</u> บาท</td>
                        </tr>
                        <tr>
                            <td width="100%">หรือมีรายได้ต่อปี <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_income_per_year'])) . '</u> บาท</td>
                        </tr>

                        <tr>
                            <td width="100%">' .multi_space(28). $loan_other_check0 . ' ไม่มีภาระผูกพันที่ต้องชำระหนี้เงินกู้อื่น</td>
                        </tr>
                        <tr>
                            <td width="100%">' .multi_space(28). $loan_other_check1 . ' มีภาระผูกพันที่ต้องชำระหนี้เงินกู้อื่น ทั้งต้นเงินหรือดอกเบี้ย เดือนละ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_guarantee_other_amount'])) . '</u> บาท</td>
                        </tr>
                        <tr>
                            <td width="100%"> หรือปีละ <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_guarantee_other_amount_per_year'])) . '</u> บาท</td>
                        </tr>

                        <tr>
                            <td width="100%">' .multi_space(28). $guarantee_other_check0 . ' ไม่มีภาระผูกพันที่ต้องชำระหนี้เงินกู้อื่น</td>
                        </tr>
                        <tr>
                            <td width="100%">' .multi_space(28). $guarantee_other_check1 . ' มีภาระผูกพันที่ต้องชำระหนี้เงินกู้อื่น จำนวนเงิน <u class="udotted">' . view_value(num2Thai($data['person_detail']['person_guarantee_other_amount'])) . '</u> บาท</td>
                        </tr>
                </table>';
            }
        }

        /* loan_objective */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                    <tr>
                        <td width="100%">'.multi_space(20).'๑๐. ข้าพเจ้ามีความประสงค์จะขอใช้สินเชื่อจากสถาบันบริหารจัดการธนาคารที่ (องค์การมหาชน)</td>
                    </tr>
                    <tr>
                        <td width="100%">เป็นจำนวนเงินทั้งสิ้น <u class="udotted">' .multi_space(1). view_value(num2Thai($loan_model['loan_amount'])) .multi_space(1). '</u> บาท ( <u class="udotted">' .multi_space(1). view_value(num2Thai($loan_model['loan_amount_thai'])) .multi_space(1). '</u> )  ระยะเวลาการผ่อนชำระ <u class="udotted">' .multi_space(1). view_value(num2Thai($loan_model['loan_period_year'])) .multi_space(1). '</u> ปี </td>
                    </tr>
                    <tr>
                        <td width="100%">โดยจะนำไปใช้ประโยชน์ เพื่อการดังนี้ <u class="udotted">' .multi_space(1). view_value(num2Thai(nl2br($loan_model['loan_desc']))) .multi_space(1). '</u></td>
                    </tr>
                    <tr>
                        <td width="100%">ข้าพเจ้าขอรับรองว่า ถ้อยคำที่ให้ต่อเจ้าหน้าที่เป็นความจริงทุกประการ และเจ้าหน้าที่ได้อ่านให้ฟังแล้ว รับรองว่าถูกค้องจึงลงลายมือชื่อไว้เป็นหลักฐาน</td>
                    </tr>
            </table>';

        /* ลงชื่อ */
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .= $htmlcontent_gap;
        $htmlcontent .='<table width="100%">
                    <tr>
                        <td width="100%">' . multi_space(80) . 'ลงชื่อ ' . multi_dot(60) . ' ผู้ให้ถ้อยคำ</td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="100%">' . multi_space(87) . '(' . multi_dot(61) . ')</td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr><td width="100%"></td></tr>

                    <tr>
                        <td width="100%">' . multi_space(80) . 'ลงชื่อ ' . multi_dot(60) . ' ผู้บันทึก</td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="100%">' . multi_space(87) . '(' . multi_dot(61) . ')</td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="100%">' . multi_space(87) . 'ตำแหน่ง ' . multi_dot(47) . ' </td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr><td width="100%"></td></tr>

                    <tr>
                        <td width="100%">' . multi_space(80) . 'ลงชื่อ ' . multi_dot(60) . ' พยาน</td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="100%">' . multi_space(87) . '(' . multi_dot(61) . ')</td>
                    </tr>
            </table>';


        ob_start();
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 5, 10, true);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        ############################################ Page set1
        //Output
        $pdf->Output($filename . '.pdf', 'I');
    }

    /* บจธ.สช. ๖ */

    public function report_loan_guarantee_bondsman($loan_id) {
        $loan_id = $this->uri->segment(3);
        $logo_invoice = 'assets\images\logo_invoice.png';

        ########################### Data ###########################
        //loan_model
        $loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);

        //person_model
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);

        //loan_guarantee_bondsman_model
        $loan_guarantee_bondsman_model = $this->loan_lib->loan_guarantee_bondsman_report($loan_id);

        ########################### Report ###########################
        //tcpdf
        $this->load->library('tcpdf');

        $filename = "loan_{$loan_id}_" . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        ############################################ Page set1
        // add a page
        $pdf->AddPage();

        $htmlcontent_gap = '<table width="100%"><tr><td width="100%"></td></tr></table>';

        //$htmlcontent
        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๖</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					หนังสือค้ำประกัน
				</td>
			</tr>
		</table>';

        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';
        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">ทำที่<u class="udotted">&nbsp;&nbsp;' . view_value(num2Thai($loan_model['loan_location'])) . multi_space(2) . '</u></td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่<u class="udotted">&nbsp;&nbsp;' . view_value(num2Thai($loan_model['loan_datefull'])) . multi_space(2) . '</u></td>
			</tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';

        if ($loan_guarantee_bondsman_model) {
            foreach ($loan_guarantee_bondsman_model as $key => $data) {
                $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2">' . multi_space(20) . 'ข้าพเจ้า <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_fullname'])) . multi_space(2) . '</u>' . ' เลขประจำตัวประชาชน <u class="udotted">' . multi_space(2) . view_value(num2Thai(formatThaiid($data['person_detail']['person_thaiid']))) . multi_space(2) . '</u> </td>
			</tr>
			<tr>
				<td width="100%">เป็น <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['career_name'])) . multi_space(2) . '</u>  ตำแหน่ง <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_position'])) . multi_space(2) . '</u> สังกัด <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_belong'])) . multi_space(2) . '</u>    ได้รับเงินเดือน/ค่าจ้าง เดือนละ <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_income_per_month'])) . multi_space(2) . '</u> บาท</td>
			</tr>
			<tr>
				<td colspan="2">ที่อยู่ เลขที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_addr_pre_no'])) . multi_space(2) . '</u> หมู่ที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_addr_pre_moo'])) . multi_space(2) . '</u>  ถนน <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_addr_pre_road'])) . multi_space(2) . '</u>  ตำบล/แขวง <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_addr_pre_district_name'])) . multi_space(2) . '</u>  อำเภอ/เขต <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_addr_pre_amphur_name'])) . multi_space(2) . '</u> </td>
			</tr>
                        <tr>
				<td colspan="2">จังหวัด <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_addr_pre_province_name'])) . multi_space(2) . '</u> โทรศัพท์ <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['person_detail']['person_phone'])) . multi_space(2) . '</u>  </td>
			</tr>
		</table>';
            }
        } else {
            $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2">' . multi_space(20) . 'ข้าพเจ้า ............... เลขประจำตัวประชาชน  ...............</td>
			</tr>
			<tr>

				<td width="100%">เป็น
					<table width="80%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="assets\icon\checkbox_check.png"> ข้าราชการ</td>
						<td><img src="assets\icon\checkbox_uncheck.png"> พนักงานรัฐวิสาหกิจ</td>
						<td><img src="assets\icon\checkbox_uncheck.png"> อื่น ๆ ...............</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">ตำแหน่ง ............... สังกัด ...............    ได้รับเงินเดือน/ค่าจ้าง เดือนละ      ............... บาท</td>
			</tr>
			<tr>
				<td colspan="2">ที่อยู่ เลขที่ ............... หมู่ที่...............  ถนน ...............  ตำบล/แขวง............... </td>
			</tr>
			<tr>
				<td colspan="2">อำเภอ/เขต ............... จังหวัด ............... โทรศัพท์ ...............  </td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2">' . multi_space(20) . 'ข้าพเจ้า ............... เลขประจำตัวประชาชน  ...............</td>
			</tr>
			<tr>

				<td width="100%">เป็น
					<table width="80%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="assets\icon\checkbox_check.png"> ข้าราชการ</td>
						<td><img src="assets\icon\checkbox_uncheck.png"> พนักงานรัฐวิสาหกิจ</td>
						<td><img src="assets\icon\checkbox_uncheck.png"> อื่น ๆ ...............</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">ตำแหน่ง ............... สังกัด ...............    ได้รับเงินเดือน/ค่าจ้าง เดือนละ      ............... บาท</td>
			</tr>
			<tr>
				<td colspan="2">ที่อยู่ เลขที่ ............... หมู่ที่...............  ถนน ...............  ตำบล/แขวง............... </td>
			</tr>
			<tr>
				<td colspan="2">อำเภอ/เขต ............... จังหวัด ............... โทรศัพท์ ...............  </td>
			</tr>
		</table>
                ';
        }

        $htmlcontent .='
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>ขอทำหนังสือค้ำประกันให้ไว้ต่อสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.) ซึ่งต่อไปนี้เรียกว่า “บจธ.” เพื่อเป็นหลักฐานดังต่อไปนี้</td>
			</tr>
			<tr>
				<td>' . multi_space(20) . 'ข้อ ๑. ตามที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(2) . '</u> ได้ทำหนังสือกู้เงิน ' . multi_dot(70) . ' </td>
			</tr>
			<tr>
				<td>ลงวันที่ ' . multi_dot(50) . ' วัตถุประสงค์ <u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_desc'])) . multi_space(2) . '</u> ให้ไว้ต่อ บจธ. วงเงินกู้ไม่เกิน <u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_amount'])) . multi_space(2) . '</u> บาท ( <u class="udotted">' . multi_space(2) . view_value(num2wordsThai($loan_model['loan_amount'])) . multi_space(2) . '</u> )</td>
			</tr>
			<tr>
				<td>กำหนดชำระเสร็จสิ้นภายในวันที่ ' . multi_dot(50) . ' </td>
			</tr>
			<tr>
				<td>ข้าพเจ้ายินยอมค้ำประกันเพื่อชำระหนี้ตามหนังสือดังกล่าวนั้น ในจำนวนต้นเงิน <u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_amount'])) . multi_space(2) . '</u> บาท</td>
			</tr>
			<tr>
				<td>รวมทั้งดอกเบี้ย ค่าธรรมเนียม ค่าสินเชื่อ ค่าสินไหมทดแทน ค่าปรับ และค่าอุปกรณ์อื่น ๆ เกี่ยวกับเงินกู้รายนี้ ให้แก่ บจธ. จนเต็มจำนวน</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๒.ข้าพเจ้าทราบข้อผูกพันของผู้กู้ตามที่กำหนดไว้ในหนังสือกู้เงินในข้อ ๑ ข้าพเจ้ายินยอมค้ำประกัน การปฏิบัติตามข้อผูกพันนั้น</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๓.	เมื่อปรากฏว่า ผู้กู้ไม่ชำระหนี้ ซึ่งข้าพเจ้าค้ำประกันไว้นี้ และ บจธ. ได้แจ้งเรื่องให้ข้าพเจ้าทราบ
ข้าพเจ้ายินยอมรับผิดชำระหนี้ให้แก่ บจธ. แทนผู้กู้ โดยมิพักใช้สิทธิของผู้ค้ำประกันตามข้อ ๖๘๗ แห่งประมวลกฎหมายแพ่งและพาณิชย์แต่ประการใด บจธ. จะเรียกชำระหนี้นั้น พร้อมทั้งดอกเบี้ยจากผู้ค้ำประกันคนใดคนหนึ่ง หรือหลายคนในบรรดาผู้ค้ำประกันโดยสิ้นเชิงหรือบางส่วนก็ได้ ตามแต่ บจธ. จะเลือกบรรดาผู้ค้ำประกัน
ยังคงผูกพันอยู่จนกว่าหนี้ดังกล่าวจะได้ชำระเสร็จ</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๔.	หากจะมีการกระทำอย่างใดอย่างหนึ่งของ บจธ. เป็นเหตุให้ข้าพเจ้าไม่อาจเข้าไปรับช่วงได้ทั้งหมด หรือแต่บางส่วนในสิทธิใดๆ อันได้ให้แก่ บจธ. ก่อนหรือในขณะหรือภายหลังที่ข้าพเจ้าทำหนังสือค้ำประกัน ข้าพเจ้าก็ไม่หลุดพ้นจากความรับผิดในการค้ำประกัน</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๕.	ข้าพเจ้าตกลงให้ บจธ. บอกกล่าวข้าพเจ้าไปยังที่อยู่ที่แจ้งไว้กับ บจธ. หรือตามที่ข้าพเจ้าแจ้ง เพิ่มเติมเป็นหนังสือให้ บจธ. ทราบ หาก บจธ. ได้บอกกล่าวข้าพเจ้าตามที่อยู่ที่แจ้งเพิ่มเติมเป็นหนังสือ ให้ถือว่าข้าพเจ้า ได้รับทราบคำบอกกล่าวโดยชอบแล้ว</td>
			</tr>
			<tr>
				<td><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าพเจ้าเข้าใจข้อความในหนังสือค้ำประกันหนี้โดยตลอดแล้ว จึงลงลายมือชื่อไว้เป็นสำคัญต่อหน้าพยาน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ค้ำประกัน </td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ค้ำประกัน </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
		</table>';

        ob_start();
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 5, 10, true);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        ############################################ Page set1
        //Output
        $pdf->Output($filename . '.pdf', 'I');
    }

	public function report_loan_guarantee_bondsman_word($loan_id){

    	include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);
        $loan_guarantee_bondsman_model = $this->loan_lib->loan_guarantee_bondsman_report($loan_id);

		// Retrieve the user name to display
		$GLOBALS['loan_location'] = num2Thai($loan_model['loan_location']);
		$GLOBALS['loan_datefull'] = num2Thai($loan_model['loan_datefull']);
		$GLOBALS['person_fullname'] = num2Thai($person_model['person_fullname']);
		$GLOBALS['loan_desc'] = num2Thai($loan_model['loan_desc']);
		$GLOBALS['loan_amount'] = num2Thai($loan_model['loan_amount']);
		$GLOBALS['loan_amount_text'] = num2wordsThai($loan_model['loan_amount']);

		$template = 'word/template/6_1_loan.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		$datax = array();
        foreach ($loan_guarantee_bondsman_model as $data) {
            $datax[] = array(
	            'person_fullname'=> num2Thai($data['person_detail']['person_fullname']),
	            'person_thaiid'=> num2Thai(formatThaiid($data['person_detail']['person_thaiid'])),
            	'person_career'=> num2Thai($data['person_detail']['career_name']),
              'career_other'=> num2Thai($data['person_detail']['career_other']),
	            'person_position'=> num2Thai($data['person_detail']['person_position']),
	            'person_belong'=> num2Thai($data['person_detail']['person_belong']),
	            'person_income_per_month'=> num2Thai($data['person_detail']['person_income_per_month']),
	            'person_addr_pre_no'=> num2Thai($data['person_detail']['person_addr_pre_no']),
	            'person_addr_pre_moo'=> num2Thai($data['person_detail']['person_addr_pre_moo']),
	            'person_addr_pre_road'=> num2Thai($data['person_detail']['person_addr_pre_road']),
	            'person_addr_pre_district_name'=> num2Thai($data['person_detail']['person_addr_pre_district_name']),
	            'person_addr_pre_amphur_name'=> num2Thai($data['person_detail']['person_addr_pre_amphur_name']),
	            'person_addr_pre_province_name'=> num2Thai($data['person_detail']['person_addr_pre_province_name']),
            	'person_phone'=> num2Thai($data['person_detail']['person_phone'])
            );
		}
		$TBS->MergeBlock('g', $datax);

		// Delete comments
		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'loan_6_'.$loan_id.'_'.date('Y-m-d').'.docx';
    $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);
    }

    /* บจธ.สช. ๗ */

    public function report_spouse_agree($loan_id) {
        $loan_id = $this->uri->segment(3);
        $logo_invoice = 'assets\images\logo_invoice.png';

        ########################### Data ###########################
        //loan_model
        $loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);

        //person_model
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);

        //spouse_model
        if ($loan_model['loan_spouse']) {
            $spouse_model = $this->loan_lib->person_detail($loan_model['loan_spouse']);
        } else {
            $spouse_model = array(
                'title_name' => '-',
                'person_fname' => '-',
                'person_lname' => '-',
                'person_age' => '-',
                'career_name' => '-',
                'person_addr_pre_no' => '-',
                'person_addr_pre_moo' => '-',
                'person_addr_pre_road' => '-',
                'person_addr_pre_district_name' => '-',
                'person_addr_pre_amphur_name' => '-',
                'person_addr_pre_province_name' => '-',
                'person_phone' => '-',
                'person_mobile' => '-',
                'person_email' => '-',
            );
        }

        ########################### Report ###########################
        //tcpdf
        $this->load->library('tcpdf');

        $filename = "loan_{$loan_id}_" . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        ############################################ Page set1
        // add a page
        $pdf->AddPage();

        $htmlcontent_gap = '<table width="100%"><tr><td width="100%"></td></tr></table>';

        //$htmlcontent
        $htmlcontent = '
<style> .nline{border-bottom:1px dotted black;}</style>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๗</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบหนังสือยินยอมของคู่สมรส
				</td>
			</tr>
		</table>';

        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';
        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">ทำที่&nbsp;&nbsp;<u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_location'])) . multi_space(2) . '</u></td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่&nbsp;&nbsp;<u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_datefull'])) . multi_space(2) . '</u></td>
			</tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>' . multi_space(20) . 'ข้าพเจ้า <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_fullname'])) . multi_space(2) . '</u> อายุ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_age'])) . multi_space(2) . '</u> ปี</td>
			</tr>
			<tr>
				<td>ที่อยู่ปัจจุบัน เลขที่ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_addr_pre_no'])) . multi_space(2) . '</u> หมู่ที่ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_addr_pre_moo'])) . multi_space(2) . '</u> ถนน <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_addr_pre_road'])) . multi_space(2) . '</u>  ตำบล/แขวง <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_addr_pre_district_name'])) . multi_space(2) . '</u> อำเภอ/เขต <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_addr_pre_amphur_name'])) . multi_space(2) . '</u> จังหวัด <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_addr_pre_province_name'])) . multi_space(2) . '</u></td>
			</tr>
			<tr>
				<td>เลขประจำตัวประชาชน <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai(formatThaiid($spouse_model['person_thaiid']))) . multi_space(2) . '</u> วันออกบัตร <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_card_startdate'])) . multi_space(2) . '</u> บัตรหมดอายุ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($spouse_model['person_card_expiredate'])) . multi_space(2) . '</u></td>
			</tr>
			<tr>
				<td>ซึ่งเป็นสามี/ภรรยาโดยชอบด้วยกฎหมายของ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(2) . '</u> ขอทำหนังสือฉบับนี้ขึ้นไว้เพื่อ</td>
			</tr>
			<tr>
				<td>เป็นหลักฐานแสดงว่าข้าพเจ้าได้รับทราบและยินยอมให้ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(2) . '</u>  สามี/ภรรยาของข้าพเจ้า เป็นผู้มีอำนาจในการทำนิติกรรม/สัญญา ' . multi_dot(40) . '</td>
			</tr>
			<tr>
				<td>' . multi_space(20) . 'ทั้งนี้ การใดที่ <u style="border-bottom:1px dotted">' . multi_space(2) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(2) . '</u> สามี/ภรรยาของข้าพเจ้าได้กระทำไปในขอบอำนาจของหนังสือยินยอมนี้ ให้มีผลผูกพันข้าพเจ้าเสมือนหนึ่งว่าข้าพเจ้าได้เข้าร่วมกระทำด้วย</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เพื่อเป็นหลักฐานในการนี้  ข้าพเจ้าจึงลงลายมือชื่อไว้เป็นสำคัญต่อหน้าพยาน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"> </td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้ให้ความยินยอม </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
		</table>
                <br><br><br><br><br>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าพเจ้าคู่สัญญาขอรับรองว่า ผู้ให้ความยินยอมข้างต้นเป็นคู่สมรสของข้าพเจ้า ลายมือชื่อ
ผู้ให้ความยินยอมเป็นลายมือชื่อจริงและผู้ให้ความยินยอม ยินยอมให้ทำนิติกรรม/สัญญา' . multi_dot(20) . 'ได้
หากเกิดความเสียหายอย่างใดๆ ขึ้น ข้าพเจ้าขอเป็นผู้รับผิดชอบและใช้ข้อความนี้ยันข้าพเจ้าได้ทั้งในคดีแพ่ง
และในคดีอาญา</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้รับรอง </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
		</table>';

        ob_start();
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 5, 10, true);
//        $pdf->writeHTML($htmlcontent, true, 0, true, true);
        $pdf->writeHTML($htmlcontent, 0);

        ############################################ Page set1
        //Output
        $pdf->Output($filename . '.pdf', 'I');
    }

    public function report_spouse_agree_word($loan_id){

    	include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

    	$loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);
        $spouse_model = $this->loan_lib->person_detail($loan_model['loan_spouse']);

		// Retrieve the user name to display
		$GLOBALS['loan_location'] = num2Thai($loan_model['loan_location']);
		$GLOBALS['loan_datefull'] = num2Thai($loan_model['loan_datefull']);
		$GLOBALS['spouse_fullname'] = num2Thai($spouse_model['person_fullname']);
		$GLOBALS['spouse_age'] = num2Thai($spouse_model['person_age']);
		$GLOBALS['spouse_addr_pre_no'] = num2Thai($spouse_model['person_addr_pre_no']);
		$GLOBALS['spouse_addr_pre_moo'] = num2Thai($spouse_model['person_addr_pre_moo']);
		$GLOBALS['spouse_addr_pre_soi'] = num2Thai($spouse_model['person_addr_pre_road']);
		$GLOBALS['spouse_addr_pre_road'] = num2Thai($spouse_model['person_addr_pre_road']);
		$GLOBALS['spouse_addr_pre_district_name'] = num2Thai($spouse_model['person_addr_pre_district_name']);
		$GLOBALS['spouse_addr_pre_amphur_name'] = num2Thai($spouse_model['person_addr_pre_amphur_name']);
		$GLOBALS['spouse_addr_pre_province_name'] = num2Thai($spouse_model['person_addr_pre_province_name']);
		$GLOBALS['spouse_thaiid'] = num2Thai(formatThaiid($spouse_model['person_thaiid']));
		$GLOBALS['spouse_card_startdate'] = num2Thai($spouse_model['person_card_startdate']);
		$GLOBALS['spouse_card_expiredate'] = num2Thai($spouse_model['person_card_expiredate']);
		$GLOBALS['person_fullname'] = num2Thai($person_model['person_fullname']);

		$template = 'word/template/7_1_loan.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		// Delete comments
		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'loan_7_'.$loan_id.'_'.date('Y-m-d').'.docx';
    $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);
    }

    /* บจธ.สช. ๘ */

    public function report_loan_guarantee_bookbank($loan_id) {
        $loan_id = $this->uri->segment(3);
        $logo_invoice = 'assets\images\logo_invoice.png';

        ########################### Data ###########################
        //loan_model
        $loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);

        //person_model
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);

        //$loan_guarantee_bookbank_model
        $loan_guarantee_bookbank_model = $this->loan_lib->loan_guarantee_bookbank_report($loan_id);

        ########################### Report ###########################
        //tcpdf
        $this->load->library('tcpdf');

        $filename = "loan_{$loan_id}_" . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        ############################################ Page set1
        // add a page
        $pdf->AddPage();

        $htmlcontent_gap = '<table width="100%"><tr><td width="100%"></td></tr></table>';

        //$htmlcontent
        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					สัญญาขอนำเงินฝากมาเป็นหลักประกันในการชำระหนี้
				</td>
			</tr>
		</table>';

        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">เขียนที่&nbsp;&nbsp;<u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_location'])) . multi_space(2) . '</u> </td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่&nbsp;&nbsp;<u class="udotted">' . multi_space(2) . view_value(num2Thai($loan_model['loan_datefull'])) . multi_space(2) . '</u> </td>
			</tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2">' . multi_space(20) . 'ข้าพเจ้า <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(2) . '</u> วันเกิด (วัน/เดือน/ปี) <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_birthdate_thai'])) . multi_space(2) . '</u> </td>
			</tr>
			<tr>
				<td colspan="2">อายุ <u class="udotted">' . multi_space(1) . num2Thai($person_model['person_age']) . multi_space(1) . '</u> ปี    เพศ <u class="udotted">' . multi_space(1) . num2Thai($person_model['sex_name']) . multi_space(1) . '</u>' .
                '  เชื้อชาติ <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['race_name'])) . multi_space(1) . '</u>  สัญชาติ <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['nationality_name'])) . multi_space(1) . '</u>  ศาสนา <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['religion_name'])) . multi_space(1) . '</u>' .
                '  เลขประจำตัวประชาชน <u class="udotted">' . multi_space(1) . view_value(num2Thai(formatThaiid($person_model['person_thaiid']))) . multi_space(1) . ' </u>
                                </td>
			</tr>
			<tr>
				<td colspan="2">วันออกบัตร <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_card_startdate_thai'])) . multi_space(2) . '</u>  บัตรหมดอายุ <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_card_expiredate_thai'])) . multi_space(2) . '</u></td>
			</tr>
			<tr>
				<td colspan="2">ที่อยู่ตามบัตรประจำตัวประชาชน เลขที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_no'])) . multi_space(1) . '</u> หมู่ที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_moo'])) . multi_space(1) . '</u> ถนน<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_road'])) . multi_space(1) . '</u>' .
                ' ตำบล/แขวง<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_district_name'])) . multi_space(1) . '</u> อำเภอ/เขต<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_amphur_name'])) . multi_space(1) . '</u> จังหวัด<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_card_province_name'])) . multi_space(1) . '</u></td>
			</tr>
			<tr>
				<td colspan="2">ที่อยู่ปัจจุบัน เลขที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_addr_pre_no'])) . multi_space(2) . '</u> หมู่ที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_addr_pre_moo'])) . multi_space(2) . '</u> ถนน <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_addr_pre_road'])) . multi_space(2) . '</u>' .
                ' ตำบล/แขวง <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_addr_pre_district_name'])) . multi_space(2) . '</u>  อำเภอ/เขต <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_addr_pre_amphur_name'])) . multi_space(2) . '</u> จังหวัด <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_addr_pre_province_name'])) . multi_space(2) . '</u> </td>
			</tr>
			<tr>
				<td colspan="2">โทรศัพท์บ้าน <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_phone'])) . multi_space(2) . '</u>  โทรศัพท์มือถือ <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_mobile'])) . multi_space(2) . '</u> E-mail <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_email'])) . multi_space(2) . '</u> </td>
			</tr>
        ';

        if ($loan_guarantee_bookbank_model) {
            $sum_amount = 0;
            foreach ($loan_guarantee_bookbank_model as $key => $data) {
                $sum_amount = $sum_amount + money_str_to_num($data['loan_bookbank_amount_normal']);
                $htmlcontent .='
                                <tr>
                                        <td colspan="2"></td>
                                </tr>
                                <tr>
                                        <td colspan="2">โดยข้าพเจ้าเป็นเจ้าของเงินฝากในบัญชีเงินฝากของธนาคาร สถาบันการเงิน หรือสหกรณ์ <u class="udotted">' . view_value(num2Thai($data['bank_name'])) . '</u> </td>
                                </tr>
                                <tr>
                                        <td colspan="2">ประเภท <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['loan_bookbank_type_name'])) . multi_space(2) . '</u> บัญชีเงินฝากเลขที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['loan_bookbank_no'])) . multi_space(2) . '</u> </td>
                                </tr>
                                <tr>
                                        <td colspan="2">ซึ่งมียอดเงินฝาก ณ วันที่ <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['loan_bookbank_date'])) . multi_space(2) . '</u> จำนวนเงิน <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['loan_bookbank_amount'])) . multi_space(2) . '</u> บาท  ( <u class="udotted">' . multi_space(2) . view_value(num2Thai($data['loan_bookbank_amount_thai'])) . multi_space(2) . '</u> ) </td>
                                </tr>
                ';
            }

            $sum_amount = money_num_to_str($sum_amount);
        } else {
            $sum_amount = money_num_to_str(0);
        }

        $htmlcontent .='
                        <tr>
                                <td colspan="2">' . multi_space(20) . 'ข้าพเจ้าขอนำเงินฝากจำนวน <u class="udotted">' . multi_space(2) . view_value(num2Thai($sum_amount)) . multi_space(2) . '</u> บาท ( <u class="udotted">' . multi_space(2) . view_value(num2Thai(num2wordsThai($sum_amount))) . multi_space(2) . '</u> ) </td>
                        </tr>
                        <tr>
                                <td colspan="2">ซึ่งฝากอยู่ในบัญชีดังกล่าวมาเป็นหลักประกันในการชำระหนี้ของนาย/นาง/นางสาว <u class="udotted">' . multi_space(2) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(2) . '</u> </td>
                        </tr>
                        <tr>
                                <td colspan="2">ซึ่งเป็นลูกหนี้ที่มีหนี้ต่อสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.) ตามคำขอสินเชื่อ <u class="udotted">' . multi_space(2) . num2Thai($loan_model['loan_desc']) . multi_space(2) . '</u> </td>
                        </tr>
			<tr>
				<td colspan="2">และสัญญากู้ยืมเงินเลขที่ ' . multi_dot(40) . ' ลงวันที่ ................. เดือน ................  พ.ศ. ................
                                โดยมีข้อตกลงว่าเพื่อประโยชน์แห่งการขอนำเงินฝากมาเป็นหลักประกันในการชำระหนี้ ข้าพเจ้ายินยอมให้ บจธ. ดำเนินการ โดยประการใดๆ เช่น ทำหนังสือหรือข้อตกลงกับธนาคารหรือสถาบันการเงินหรือสหกรณ์ข้างต้น ซึ่งเป็นผู้รับฝากเงิน ของข้าพเจ้า
                                เพื่อดำเนินการพักเงินในบัญชีเงินฝาก (Hold) ในจำนวนเงินตามสัญญานำเงินฝากมาเป็นหลักประกัน ในการชำระหนี้ฉบับนี้ รวมทั้งห้ามมิให้ข้าพเจ้าถอนเงินจำนวนข้างต้นออกจากบัญชีเงินฝากของข้าพเจ้า
                                จนกว่าหนี้ระหว่างลูกหนี้กับ บจธ. จะชำระหมดสิ้นไป และในกรณีหากลูกหนี้ผิดนัดไม่ชำระหนี้แก่ บจธ.มาตามสัญญากู้ยืมเงินดังกล่าว ข้าพเจ้ายินยอมให้ผู้รับฝากเงินดำเนินการใดๆ เพื่อนำไปชำระหนี้เต็มจำนวนที่ค้างชำระ รวมตลอดจนดอกเบี้ย
                                ค่าภาระติดพันอันเป็นอุปกรณ์แห่งหนี้รายหนี้ด้วย</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" colspan="2"></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"> </td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... เจ้าของเงินฝาก </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
		</table>';
        ob_start();
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 5, 10, true);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        ############################################ Page set1
        //Output
        $pdf->Output($filename . '.pdf', 'I');
    }

    public function report_loan_guarantee_bookbank_word($loan_id){

    	include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);
        $loan_guarantee_bookbank_model = $this->loan_lib->loan_guarantee_bookbank_report($loan_id);

        $sum_amount = 0;
        foreach ($loan_guarantee_bookbank_model as $data) {
        	$sum_amount = $sum_amount + money_str_to_num($data['loan_bookbank_amount_normal']);
		}
		$sum_amount = money_num_to_str($sum_amount);

		$person_sex_list = $person_model['sex_id'] == '1' ? '( √ ) ชาย ' : '( ) ชาย';
        $person_sex_list .= $person_model['sex_id'] == '2' ? '( √ ) หญิง' : '( ) หญิง';
    // echo "<pre>";print_r($person_model);echo "</pre>";
		// Retrieve the user name to display
		$GLOBALS['loan_location'] = num2Thai($loan_model['loan_location']);
		$GLOBALS['loan_datefull'] = num2Thai($loan_model['loan_datefull']);
		$GLOBALS['person_fullname'] = num2Thai($person_model['person_fullname']);
		$GLOBALS['person_birthdate_thai'] = num2Thai($person_model['person_birthdate_thai']);
		$GLOBALS['person_age'] = num2Thai($person_model['person_age']);
		$GLOBALS['person_sex_list'] = $person_sex_list;
		$GLOBALS['person_race'] = num2Thai($person_model['race_name']);
		$GLOBALS['person_nationality'] = num2Thai($person_model['nationality_name']);
		$GLOBALS['person_religion'] = num2Thai($person_model['religion_name']);
		$GLOBALS['person_thaiid'] = num2Thai(formatThaiid($person_model['person_thaiid']));
		$GLOBALS['person_card_startdate_thai'] = num2Thai($person_model['person_card_startdate_thai']);
		$GLOBALS['person_card_expiredate_thai'] = num2Thai($person_model['person_card_expiredate_thai']);
		$GLOBALS['person_addr_card_no'] = num2Thai($person_model['person_addr_card_no']);
		$GLOBALS['person_addr_card_moo'] = num2Thai($person_model['person_addr_card_moo']);
		$GLOBALS['person_addr_card_road'] = num2Thai($person_model['person_addr_card_road']);
		$GLOBALS['person_addr_card_district_name'] = num2Thai($person_model['person_addr_card_district_name']);
		$GLOBALS['person_addr_card_amphur_name'] = num2Thai($person_model['person_addr_card_amphur_name']);
		$GLOBALS['person_addr_card_province_name'] = num2Thai($person_model['person_addr_card_province_name']);
		$GLOBALS['person_pre_card_no'] = num2Thai($person_model['person_addr_pre_no']);
		$GLOBALS['person_pre_card_moo'] = num2Thai($person_model['person_addr_pre_moo']);
		$GLOBALS['person_pre_card_road'] = num2Thai($person_model['person_addr_pre_road']);
		$GLOBALS['person_pre_card_district_name'] = num2Thai($person_model['person_addr_pre_district_name']);
		$GLOBALS['person_pre_card_amphur_name'] = num2Thai($person_model['person_addr_pre_amphur_name']);
		$GLOBALS['person_pre_card_province_name'] = num2Thai($person_model['person_addr_pre_district_name']);
		$GLOBALS['person_phone'] = num2Thai($person_model['person_phone']);
		$GLOBALS['person_mobile'] = num2Thai($person_model['person_mobile']);
		$GLOBALS['person_email'] = num2Thai($person_model['person_email']);
		$GLOBALS['sum_amount'] = num2Thai($sum_amount);
		$GLOBALS['sum_amount_thai'] = num2Thai(num2wordsThai($sum_amount));
		$GLOBALS['person_fullname'] = num2Thai($person_model['person_fullname']);
		$GLOBALS['loan_desc'] = num2Thai($loan_model['loan_desc']);

		$template = 'word/template/8_1_loan.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		$datax = array();
        foreach ($loan_guarantee_bookbank_model as $data) {
            $datax[] = array(
	            'bank_name'=> num2Thai($data['bank_name']),
	            'loan_bookbank_type_name'=> num2Thai($data['loan_bookbank_type_name']),
            	'loan_bookbank_no'=> num2Thai($data['loan_bookbank_no']),
	            'loan_bookbank_date'=> num2Thai($data['loan_bookbank_date']),
	            'loan_bookbank_amount'=> num2Thai($data['loan_bookbank_amount']),
	            'loan_bookbank_amount_thai'=> num2Thai($data['loan_bookbank_amount_thai'])
            );
		}
		$TBS->MergeBlock('g', $datax);

		// Delete comments
		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'loan_8_'.$loan_id.'_'.date('Y-m-d').'.docx';
    $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);
    }


    /* บจธ.สช. ๙ */

    public function report_loan_guarantee_bond($loan_id) {
        $loan_id = $this->uri->segment(3);
        $logo_invoice = 'assets\images\logo_invoice.png';

        $this->load->model('payment_schedule_model');
        $payment_schedule_model = $this->payment_schedule_model->get_by_opt($opt = array('loan_id' => $loan_id));
        if ($payment_schedule_model) {
            $interest_rate = $payment_schedule_model['interest_rate'];
        } else {
            $interest_rate = '-';
        }

        ########################### Data ###########################
        //loan_model
        $loan_model = $this->loan_model->get_by_id($loan_id);
        $loan_model['loan_amount'] = money_num_to_str($loan_model['loan_amount']);

        //person_model
        $person_model = $this->loan_lib->person_detail($loan_model['person_id']);

        //$loan_guarantee_bond_model
        $loan_guarantee_bond_model = $this->loan_lib->loan_guarantee_bond_report($loan_id);

        ########################### Report ###########################
        //tcpdf
        $this->load->library('tcpdf');

        $filename = "loan_{$loan_id}_" . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        #### include ####
        $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
        $html_uncheckbox = '<img src="assets\icon\checkbox_uncheck.png">';

        ############################################ Page set1
        // add a page
        $pdf->AddPage();

        $htmlcontent_gap = '<table width="100%"><tr><td width="100%"></td></tr></table>';

        //$htmlcontent
        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๙</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบข้อตกลงจำนำสิทธิในพันธบัตรเป็นหลักประกัน
				</td>
			</tr>
		</table>';

        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';
        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">เขียนที่&nbsp;<sup>ข้อ/<u>๑</u></sup>&nbsp;&nbsp;<u class="udotted">' . multi_space(1) . num2Thai($loan_model['loan_location']) . multi_space(1) . '</u></td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่&nbsp;<sup>ข้อ/<u>๒</u></sup>&nbsp;&nbsp;<u class="udotted">' . multi_space(1) . num2Thai($loan_model['loan_datefull']) . multi_space(1) . '</u></td>
			</tr>
		</table>';
        $htmlcontent .='<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>';

        $htmlcontent .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2">' . multi_space(20) . 'ข้อตกลงทำขึ้นระหว่าง ข้าพเจ้า <sup>ข้อ/<u>๓</u></sup> <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(1) . '</u> </td>
			</tr>
			<tr>
				<td colspan="2">ที่อยู่ เลขที่ <sup>ข้อ/<u>๔</u></sup> <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_no'])) . multi_space(1) . '</u> หมู่ที่<u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_moo'])) . multi_space(1) . '</u> ถนน <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_road'])) . multi_space(1) . '</u>' .
                ' ตำบล/แขวง <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_district_name'])) . multi_space(1) . '</u> อำเภอ/เขต <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_amphur_name'])) . multi_space(1) . '</u> จังหวัด <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_addr_pre_province_name'])) . multi_space(1) . '</u> </td>
			</tr>
			<tr>
				<td colspan="2">โทรศัพท์ <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_phone'])) . multi_space(1) . '</u> </td>
			</tr>
			<tr>
				<td colspan="2">ซึ่งในแบบข้อตกลงนี้ เรียกว่า “ผู้จำนำ” ฝ่ายหนึ่ง</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กับ<u> สถาบันบริหารจัดการจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.) เลขที่ ๔๖๙ ถนนนครสวรรค์ แขวงสวนจิตรลดา เขตดุสิต กรุงเทพมหานคร โทร. ๐ ๒๖๒๙ ๘๘๘๔</u> โดย <sup>ข้อ/<u>๕</u></sup>................................................................. ผู้รับมอบอำนาจ ซึ่งในแบบข้อตกลงนี้ เรียกว่า “ผู้รับจำนำ” ฝ่ายหนึ่ง</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;โดยทั้งสองฝ่ายขอทำความตกลงกันดังต่อไปนี้</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑. ผู้จำนำซึ่งเป็นผู้มีกรรมสิทธิ์ในพันธบัตร ดังนี้</td>
			</tr>
		</table>
                ';

        $htmlcontent .= '
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #000;">
			<tr>
				<td width="25%" style="border: 1px solid #000;text-align:center;">ชนิดพันธบัตร</td>
				<td width="15%" style="border: 1px solid #000;text-align:center;">พันธบัตรเลขที่ต้น</td>
				<td width="15%" style="border: 1px solid #000;text-align:center;">พันธบัตรเลขที่ท้าย</td>
				<td width="15%" style="border: 1px solid #000;text-align:center;">หน้าทะเบียน</td>
				<td width="15%" style="border: 1px solid #000;text-align:center;">ลงวันที่</td>
				<td width="15%" style="border: 1px solid #000;text-align:center;">จำนวนเงิน(บาท)</td>
			</tr>
			<tr>
				<td width="25%" style="border: 1px solid #000;text-align:center;"><sup>ข้อ/<u>๖</u></sup></td>
				<td width="15%" style="border: 1px solid #000;text-align:center;"><sup>ข้อ/<u>๗</u></sup></td>
				<td width="15%" style="border: 1px solid #000;text-align:center;"><sup>ข้อ/<u>๘</u></sup></td>
				<td width="15%" style="border: 1px solid #000;text-align:center;"><sup>ข้อ/<u>๙</u></sup></td>
				<td width="15%" style="border: 1px solid #000;text-align:center;"><sup>ข้อ/<u>๑๐</u></sup></td>
				<td width="15%" style="border: 1px solid #000;text-align:center;"><sup>ข้อ/<u>๑๑</u></sup></td>
			</tr>
                        ';

        if ($loan_guarantee_bond_model) {
            $sum_amount = 0;
            foreach ($loan_guarantee_bond_model as $key => $data) {
                $sum_amount = $sum_amount + $data['loan_bond_amount_normal'];
                $htmlcontent .= '
                                <tr>
                                        <td width="25%" style="border: 1px solid #000;text-align:center;">' . view_value(num2Thai($data['loan_bond_type'])) . '</td>
                                        <td width="15%" style="border: 1px solid #000;text-align:center;">' . view_value(num2Thai(money_num_to_str($data['loan_bond_startnumber']))) . '</td>
                                        <td width="15%" style="border: 1px solid #000;text-align:center;">' . view_value(num2Thai(money_num_to_str($data['loan_bond_endnumber']))) . '</td>
                                        <td width="15%" style="border: 1px solid #000;text-align:center;">' . view_value(num2Thai($data['loan_bond_regis_page'])) . '</td>
                                        <td width="15%" style="border: 1px solid #000;text-align:center;">' . view_value(num2Thai(thai_display_date_short($data['loan_bond_regis_date']))) . '</td>
                                        <td width="15%" style="border: 1px solid #000;text-align:center;">' . view_value(num2Thai($data['loan_bond_amount'])) . '</td>
                                </tr>
                ';
            }


            $sum_amount_thai = num2wordsThai(money_num_to_str($sum_amount));
            $sum_amount = num2Thai(money_num_to_str($sum_amount));
        } else {
            $sum_amount = num2Thai(money_num_to_str(0));
            $sum_amount_thai = '';
        }

        $htmlcontent .= '</table>';

        $htmlcontent .= '
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ขอตกลงจำนำสิทธิในพันธบัตรดังกล่าวไว้กับ บจธ. เพื่อเป็นประกันการชาระหนี้เงินกู้ทุกราย ตามเอกสารการกู้ซึ่ง <sup>ข้อ/<u>๑๒</u></sup> <u class="udotted">' . multi_space(1) . view_value(num2Thai($person_model['person_fullname'])) . multi_space(1) . '</u> ได้ทาให้ไว้ต่อในเวลานี้ หรือในเวลาใดเวลาหนึ่งในภายหน้า รวมจำนวนต้นเงินที่กู้ยืมขั้นสูงสุด <sup>ข้อ/<u>๑๓</u></sup> <u class="udotted">' . multi_space(1) . view_value(num2Thai($sum_amount)) . multi_space(1) . '</u> บาท ( <u class="udotted">' . multi_space(1) . view_value(num2Thai($sum_amount_thai)) . multi_space(1) . '</u> ) และอุปกรณ์แห่งหนี้รายนี้</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๒. ผู้รับจำนำตกลงรับจำนำสิทธิในพันธบัตรตามข้อ ๑</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๓. ผู้จำนำตกลงให้ บจธ. ดำเนินการเกี่ยวกับดอกเบี้ยพันธบัตร เมื่อมีการชำระดอกเบี้ย พันธบัตรดังกล่าว โดยให้นำเข้าบัญชีเงินฝากของ บจธ. ที่ธนาคาร <sup>ข้อ/<u>๑๔</u></sup> ............... สาขา ............... บัญชีประเภท ............... บัญชีเลขที่ ............... เพื่อ  <sup>ข้อ/<u>๑๕</u></sup> ............... หากมีเงินเหลือให้นำเข้าบัญชีเงินฝาก ของผู้จำนำ นาย/นาง/นางสาว <sup>ข้อ/<u>๑๖</u></sup> ............... ที่ธนาคาร............... สาขา ............... บัญชีประเภท ............... บัญชีเลขที่ ...............</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๔. ผู้จำตกลงว่า หากมีการชำระต้นเงินตามพันธบัตรกับดอกเบี้ยตามพันธบัตร (ถ้ามี) ก่อนที่ บจธ. จะได้รับชำระหนี้เงินกู้ ซึ่งใช้การจำนำสิทธิในพันธบัตรตามข้อ ๑ เป็นประกันเสร็จสิ้น ให้ บจธ. เป็นผู้รับเงินและนำมาหักชำระหนี้เงินกู้ที่ใช้พันธบัตรตามข้อ ๑ เป็นประกันซึ่งถึงกำหนดชำระแล้วได้ ถ้ามีเงินเหลือจากการชำระหนี้ หรือหนี้เงินกู้ซึ่งจำนำสิทธิในพันธบัตรตามข้อ ๑ เป็นประกันยังไม่ถึงกาหนด ให้ บจธ. นำเงินเข้าบัญชีเงินฝากของผู้จำนำ นาย/นาง/นางสาว <sup>ข้อ/<u>๑๗</u></sup> ............... ที่ธนาคาร ............... สาขา ............... บัญชีประเภท............... บัญชีเลขที่ ...............</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๕. ถึงแม้จะมีการกระทำอย่างใดอย่างหนึ่งของ บจธ. เป็นเหตุให้ผู้จำนำไม่อาจเข้ารับช่วงได้ทั้งหมดหรือแต่บางส่วนในสิทธิใดๆ อันได้ให้ไว้แก่ บจธ. ก่อนหรือในขณะ หรือในภายหน้าที่ผู้จำนำเข้าทำข้อตกลงนี้ ผู้จำนำก็ไม่หลุดพ้นจากความรับผิดในการจำนำรายนี้แต่ประการใดเลย</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๖. ต่อไปในภายหน้า ถ้าข้อบังคับของ บจธ. กำหนดอัตราดอกเบี้ยเงินกู้ของผู้จำนำ และหรือผู้กู้ได้ยืมจาก บจธ. สูงขึ้นกว่าอัตราที่กำหนดไว้ในหนังสือสัญญากู้ยืมเงิน ผู้จำนำตกลงยินยอมชำระดอกเบี้ยในอัตราที่กำหนดขึ้นใหม่ นับแต่วันที่ใช้ข้อบังคับเป็นต้นไป โดย บจธ. ไม่จำต้องบอกกล่าวล่วงหน้า</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ในกรณีที่ผู้จำนำและหรือผู้กู้ไม่ชำระคืนเงินกู้ทั้งหมดหรือบางส่วนภายในเวลาที่กำหนดไว้ในสัญญากู้ ผู้จำนำยินยอมชำระดอกเบี้ยเงินกู้สาหรับเงินกู้ส่วนที่มิได้ชำระตามกำหนดนั้นให้แก่ บจธ. เพิ่มขึ้นอีก <br>ในอัตราร้อยละ ' . num2Thai($interest_rate) . ' ต่อปี จากต้นเงินที่ค้างชำระ นับถัดจากวันสิ้นกำหนดชำระในหนังสือกู้เงินเป็นต้นไป จนกว่าจะชำระ เงินกู้ยืมเสร็จเรียบร้อย อย่างไรก็ดี ให้ดอกเบี้ยรวมกันแล้ว ต้องไม่เกินอัตราสูงสุดตามที่กฎหมายกำหนดไว้</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๗. ให้ถือว่าข้อตกลงนี้เป็นส่วนหนึ่งของหนังสือขอจำนำสิทธิในพันธบัตรตามระเบียบ ของธนาคารแห่งประเทศไทย</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อตกลงนี้ ทำขึ้น ๓ ฉบับ มีข้อความตรงกันทุกประการ ทุกฝ่ายได้ตรวจข้อตกลงนี้ และเข้าใจข้อความ โดยตลอดแล้ว จึงได้ลงลายมือชื่อหรือพิมพ์ลายนิ้วมือให้ไว้เป็นสาคัญต่อหน้าพยาน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"> </td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้จำนำ </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"> </td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... ผู้รับจำนำ </td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">ลงชื่อ ......................................... พยาน</td>
				</tr>
				<tr>
					<td width="50%" style="text-align:left;"></td>
					<td width="50%" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(.................................................)</td>
				</tr>
				<tr>
					<td width="100%" colspan="2"><br/><br/></td>
				</tr>
		</table>
		<br pagebreak="true"/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบข้อตกลงจำนำสิทธิในพันธบัตรเป็นหลักประกัน
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					.........................................
				</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑. <sup>ข้อ/<u>๑</u></sup> ระบุสถานที่ที่จัดทำแบบข้อตกลงจำนำสิทธิในพันธบัตรเป็นประกัน เช่น บจธ. เป็นต้น</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๒. <sup>ข้อ/<u>๒</u></sup> ระบุวัน เดือน ปี ที่จัดทำแบบข้อตกลงจำนำสิทธิในพันธบัตรเป็นประกัน</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๓. <sup>ข้อ/<u>๓</u></sup> ระบุชื่อเจ้าของพันธบัตรผู้จำนำ</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๔. <sup>ข้อ/<u>๔</u></sup> ระบุที่อยู่ของเจ้าของพันธบัตรผู้จำนำ</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๕. <sup>ข้อ/<u>๕</u></sup> ระบุชื่อเจ้าหน้าที่ บจธ. ซึ่งเป็นผู้ได้รับมอบอำนาจของ บจธ. ในการดำเนินการจำนำของ บจธ. ซึ่งต้องไปจดทะเบียนรับจำนำสิทธิในพันธบัตรเป็นประกัน</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๖. <sup>ข้อ/<u>๖</u></sup> ระบุชนิดของพันธบัตรตามที่ปรากฏในพันธบัตร เช่น ชนิดดอกเบี้ยทบต้น (ครั้งที่ ๑) ชนิดดอกเบี้ยร้อยละเจ็ดจุดสองห้า หรือพันธบัตรออมทรัพย์ช่วยชาติ ปี ๒๕๔๕ อายุ ๑๐ ปี เป็นต้น</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๗. <sup>ข้อ/<u>๗</u>/<u>๘</u>/<u>๗</u> และ/<u>๑๐</u></sup> ระบุตามพันธบัตรที่จำนำ</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๘. <sup>ข้อ/<u>๑๑</u></sup> ระบุจำนวนเงินตามพันธบัตรที่จำนำ</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๙. <sup>ข้อ/<u>๑๒</u></sup> กรณีเจ้าของพันธบัตรจำนำพันธบัตรเป็นประกันหนี้ของตนเองให้ระบุ “ผู้จำนำ” กรณีผู้อื่นจำนำพันธบัตรเป็นประกันหนี้ของผู้กู้ ให้ระบุชื่อผู้กู้เป็นลูกหนี้</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑๐. <sup>ข้อ/<u>๑๓</u></sup> ระบุเท่ากับจำนวนเงินตามพันธบัตรส่วนที่จำนำเป็นประกันทั้งหมด เช่น พันธบัตรส่วนที่จำนำ ทั้งหมดมีมูลค่า ๒๐๐,๐๐๐ บาทให้ระบุจำนวน ๒๐๐,๐๐๐ บาท</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑๑. <sup>ข้อ/<u>๑๔</u></sup> กรณีผู้กู้ยืมใช้พันธบัตรของตนเองหรือของคู่สมรสจำนำเป็นประกันหนี้ของตนเอง และเป็นพันธบัตรที่มีการจ่ายดอกเบี้ยทุกปี เมื่อมีการชำระดอกเบี้ยพันธบัตร ให้ระบุให้นำเข้าบัญชีเงินฝากของ บจธ. เช่น ระบุว่า “บจธ. ที่ธนาคารกรุงไทย จำกัด สาขาทำเนียบรัฐบาล บัญชีประเภทกระแสรายวัน บัญชีเลขที่ x xxxx xxxxx” เป็นต้น กรณีผู้อื่นที่มิใช่คู่สมรสของผู้กู้ยืมจำนำพันธบัตรเป็นประกันหนี้ของผู้กู้ยืม ซึ่ง บจธ.ไม่ได้นำดอกเบี้ยพันธบัตรมาหักชาระหนี้ เงินกู้ ให้สอบถามจากเจ้าของพันธบัตร และระบุตามที่สอบถามได้</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กรณีเป็นพันธบัตรชนิดดอกเบี้ยทบต้น ไม่ต้องดำเนินการบันทึก เนื่องจากไม่มีการรับดอกเบี้ยประจำปี</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑๑. <sup>ข้อ/<u>๑๕</u></sup> กรณีผู้กู้ใช้พันธบัตรของตนเองหรือของคู่สมรสจำนำเป็นประกันหนี้ของตนเอง และเป็นพันธบัตรที่มีการจ่ายดอกเบี้ยทุกปี ให้ระบุ “เพื่อจัดสรรชำระหนี้เงินกู้ซึ่งใช้การจำนำสิทธิในพันธบัตร ตามข้อ ๑ เป็นประกัน”</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กรณีที่ผู้อื่นซึ่งมิใช่คู่สมรสของผู้กู้จำนำพันธบัตรเป็นประกันหนี้ของผู้กู้ยืม หรือกรณีเป็นพันธบัตรชนิดดอกเบี้ยทบต้น ให้บันทึกเครื่องหมาย “-”</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑๒. <sup>ข้อ/<u>๑๖</u></sup> ระบุบัญชีที่เจ้าของพันธบัตรได้เปิดไว้ ในเฉพาะกรณีที่ผู้กู้ยืมใช้พันธบัตรของตนเอง หรือคู่สมรสจำนำเป็นประกันหนี้ของตนเอง</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กรณีผู้อื่นซึ่งมิใช่คู่สมรสของผู้กู้ยืมจำนำพันธบัตรเป็นประกันหนี้ของผู้กู้ยืม หรือกรณี เป็นพันธบัตรชนิดดอกเบี้ยทบต้น ให้บันทึกเครื่องหมาย “-”</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อ ๑๓. <sup>ข้อ/<u>๑๗</u></sup> ให้ระบุบัญชีที่เจ้าของพันธบัตรได้เปิดไว้ในทุกกรณีไม่ว่าผู้กู้ยืมจะใช้พันธบัตรของตนเองหรือของผู้อื่นมาจำนำเป็นประกัน และไม่ว่าเป็นพันธบัตรชนิดใด</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					.........................................
				</td>
			</tr>
		</table>';

        ob_start();
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 5, 10, true);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        ############################################ Page set1
        //Output
        $pdf->Output($filename . '.pdf', 'I');
    }

}
