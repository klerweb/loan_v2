<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_land_occupy extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'ที่ดินของผู้ขอสินเชื่อและคู่สมรสถือครองทั้งหมด';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_land_occupy_model');
        $this->load->model('land_owner_model');
        $this->load->model('land_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];
        $person_id = $loan_model['person_id'];
        $loan_spouse = $loan_model['loan_spouse'];

        //$form_data
        $form_data = array(
            'loan_land_occupy_id' => '',
            'loan_id' => $loan_id,
            'land_id' => '',
            'land_owner_id' => '',
            'land_type_id' => '',
            'land_no' => '',
            'land_area_rai' => 0,
            'land_area_ngan' => 0,
            'land_area_wah' => 0,
            'land_addr_no' => '',
            'land_addr_moo' => '',
            'land_addr_district_id' => '',
            'land_addr_amphur_id' => '',
            'land_addr_province_id' => '',
            'land_buiding_detail_id' => '',
            'land_owner' => 1,
            'loan_land_occupy_benefit' => '',
            'estimate_price' => money_num_to_str(0),
            'person' => $person_id,
            'spouse' => $loan_spouse
        );

        //$owner_other_data
        $owner_other_data = $this->loan_lib->person_fields();

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';

        //$table_land_building_param
        $table_land_building_data = array(
            'callbackurl' => base64_encode("loan/form_land_occupy"),
          //  'owner_id' => $loan_model['person_id'],
            'save_loan_guarantee_land' => 0,
            'loan_type_id' => '',
        );
        $table_land_building_param = json_encode($table_land_building_data);
        $table_land_building_param = base64_encode($table_land_building_param);

        //table_land_building
        $table_land_building1 = array();
        $land_owner_model = $this->land_owner_model->get_by_opt(array('person_id' => $loan_model['person_id']));
        if ($land_owner_model) {

            foreach ($land_owner_model as $key_land => $land_owner) {
                $land = $this->land_model->get_by_id($land_owner['land_id']);

                if ($land) {
                    $table_land_building1[$key_land] = $land;
                    $land_full_detail = $this->loan_lib->land_full_detail($land);
                    $table_land_building1[$key_land]['owner_id'] =  $loan_model['person_id'];
                    $table_land_building1[$key_land]['address'] = $land_full_detail['address'];

                    $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));

                    if ($building_model) {
                        foreach ($building_model as $key_building => $building) {
                            $building_full_detail = $this->loan_lib->building_full_detail($building);
                            $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                            $building_model[$key_building]['address'] = $building_full_detail['address'];
                        }

                        $table_land_building1[$key_land]['building_table'] = $building_model;
                    }
                }
            }
        }
        // echo '<pre>';
        // print_r($table_land_building1);
        // echo '</pre>';
        $land_owner_model2 = $this->land_owner_model->get_by_opt(array('person_id' => $loan_model['loan_spouse']));
        $table_land_building2 = array();
        if ($land_owner_model2) {

            foreach ($land_owner_model2 as $key_land => $land_owner) {
                $land = $this->land_model->get_by_id($land_owner['land_id']);

                if ($land) {
                    $table_land_building2[$key_land] = $land;
                    $land_full_detail = $this->loan_lib->land_full_detail($land);
                    $table_land_building2[$key_land]['address'] = $land_full_detail['address'];
                    $table_land_building2[$key_land]['owner_id'] =  $loan_model['loan_spouse'];
                    $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));

                    if ($building_model) {
                        foreach ($building_model as $key_building => $building) {
                            $building_full_detail = $this->loan_lib->building_full_detail($building);
                            $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                            $building_model[$key_building]['address'] = $building_full_detail['address'];
                        }

                        $table_land_building2[$key_land]['building_table'] = $building_model;
                    }
                }
            }
        }

        $table_land_building =  array_merge ( $table_land_building1 , $table_land_building2 );
        // echo '<pre>....';
        // print_r($table_land_building);
        // echo '</pre>';
        //
        //$get
        $get = $this->input->get();
        if ($get) {
            //edit
            if (isset($get['edit'])) {
                //loan_land_occupy_model
                $loan_land_occupy_model = $this->loan_land_occupy_model->get_by_id($get['edit']);
                $loan_land_occupy_id = $loan_land_occupy_model['loan_land_occupy_id'];
                $land_id = $loan_land_occupy_model['land_id'];
                $loan_land_occupy_model['estimate_price'] = money_num_to_str($loan_land_occupy_model['estimate_price']);

                //land_model
                $land_model = $this->land_model->geat_by_id($land_id);
                $land_type_id = $land_model['land_type_id'];

                //land_owner_model
                $land_owner_model = $this->land_owner_model->get_row_by_opt(array('land_id' => $land_id));
                $land_owner = $land_owner_model['person_id'];
                $land_owner_id = $land_owner_model['land_owner_id'];

                //$form_data
                $form_data = array(
                    'loan_land_occupy_id' => $loan_land_occupy_id,
                    'loan_land_occupy_benefit' => $loan_land_occupy_model['loan_land_occupy_benefit'],
//                    'estimate_price' => money_num_to_str($loan_land_occupy_model['estimate_price']),
                    'estimate_price' => $loan_land_occupy_model['estimate_price'],
                    'loan_id' => $loan_id,
                    'land_id' => $land_id,
                    'land_owner_id' => $land_owner_id,
                    'land_type_id' => $land_type_id,
                    'land_no' => $land_model['land_no'],
                    'land_area_rai' => $land_model['land_area_rai'],
                    'land_area_ngan' => $land_model['land_area_ngan'],
                    'land_area_wah' => $land_model['land_area_wah'],
                    'land_addr_no' => $land_model['land_addr_no'],
                    'land_addr_moo' => $land_model['land_addr_moo'],
                    'land_addr_district_id' => $land_model['land_addr_district_id'],
                    'land_addr_amphur_id' => $land_model['land_addr_amphur_id'],
                    'land_addr_province_id' => $land_model['land_addr_province_id'],
                    'land_buiding_detail_id' => $land_model['land_buiding_detail_id'],
                    'land_owner' => $land_owner,
                    'person' => $person_id,
                    'spouse' => $loan_spouse
                );

                //$table_land_building_param
                $table_land_building_data = array(
                    'callbackurl' => base64_encode("loan/form_land_occupy/?edit={$get['edit']}"),
                    'owner_id' => $loan_model['person_id'],
                    'save_loan_guarantee_land' => 0,
                    'loan_type_id' => '',
                );
                $table_land_building_param = json_encode($table_land_building_data);
                $table_land_building_param = base64_encode($table_land_building_param);
            }
        }

        //config & master
        $mst['config_land_type_list'] = $this->mst_model->get_all('land_type');
        $mst['master_district_list'] = array();
        $mst['master_amphur_list'] = array();
        $mst['master_province_list'] = $this->mst_model->get_table_location_all('province');
        $mst['owner_list'] = $this->loan_land_occupy_model->get_owner_list();
        $mst['config_title_list'] = $this->mst_model->get_all('title');

        if (!$loan_model['loan_spouse']) {
            unset($mst['owner_list'][2]);
            unset($mst['owner_list'][3]);
            unset($mst['owner_list'][5]);
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //table_data
        $opt = array('loan_id' => $loan_id);
        $table_data = $this->loan_land_occupy_model->get_table_data($opt);


        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'session_form' => $session_form,
            'form_data' => $form_data,
            'table_data' => $table_data,
            'mst' => $mst,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
            'owner_other_data' => $owner_other_data,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_land_occupy.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_land_occupy', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            if ($post['land_id']) {
                //update land_model & land_owner_model
                $land_id = $this->loan_lib->land_occupy_save($post);
            } else {
                //insert land_model & land_owner_model
                $land_id = $this->loan_lib->land_occupy_save($post);

                //insert loan_land_occupy_model
                $post_loan_land_occupy_model = array(
                    'loan_land_occupy_benefit' => $post['loan_land_occupy_benefit'],
                  //  'estimate_price' => $post['estimate_price'],
                    'loan_id' => $post['loan_id'],
                    'land_id' => $land_id,
                );
                $loan_land_occupy_model = $this->loan_land_occupy_model->save($post_loan_land_occupy_model);
                $loan_land_occupy_id = $loan_land_occupy_model['id'];

                //land_owner
                /* 1 => 'ผู้ขอ', 2 => 'คู่สมรส', 3 => 'ผู้ขอกับคู่สมรส', 4 => 'ผู้ขอกับบุคคลอื่น', 5 => 'คู่สมรสกับบุคคลอื่น' */
                if ($post['land_owner'] == 1) {
                    //person_id
                    $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                    $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                    if (!$land_owner_model) {
                        $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                        $land_owner_id = $land_owner_model['id'];
                    }
                } elseif ($post['land_owner'] == 2) {
                    //spouse
                    if ($loan_model['loan_spouse']) {
                        $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['loan_spouse']);
                        $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                        if (!$land_owner_model) {
                            $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                            $land_owner_id = $land_owner_model['id'];
                        }
                    }
                } elseif ($post['land_owner'] == 3) {
                    //person_id
                    $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                    $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                    if (!$land_owner_model) {
                        $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                        $land_owner_id = $land_owner_model['id'];
                    }

                    //spouse
                    if ($loan_model['loan_spouse']) {
                        $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['loan_spouse']);
                        $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                        if (!$land_owner_model) {
                            $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                            $land_owner_id = $land_owner_model['id'];
                        }
                    }
                } elseif ($post['land_owner'] == 4) {
                    //person
                    $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['person_id']);
                    $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                    if (!$land_owner_model) {
                        $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                        $land_owner_id = $land_owner_model['id'];
                    }

                    //owner_other
                    $owner_other = array(
                        'title_id' => $post['owner_other_title_id'],
                        'person_fname' => $post['owner_other_person_fname'],
                        'person_lname' => $post['owner_other_person_lname']
                    );
                    $owner_other_id = $this->loan_lib->person_land2_save($owner_other);

                    $land_owner_opt = array('land_id' => $land_id, 'person_id' => $owner_other_id);
                    $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                    if (!$land_owner_model) {
                        $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                        $land_owner_id = $land_owner_model['id'];
                    }
                } elseif ($post['land_owner'] == 5) {
                    //spouse
                    if ($loan_model['loan_spouse']) {
                        $land_owner_opt = array('land_id' => $land_id, 'person_id' => $loan_model['loan_spouse']);
                        $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                        if (!$land_owner_model) {
                            $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                            $land_owner_id = $land_owner_model['id'];
                        }
                    }

                    //owner_other
                    $owner_other = array(
                        'title_id' => $post['owner_other_title_id'],
                        'person_fname' => $post['owner_other_person_fname'],
                        'person_lname' => $post['owner_other_person_lname']
                    );
                    $owner_other_id = $this->loan_lib->person_land2_save($owner_other);

                    $land_owner_opt = array('land_id' => $land_id, 'person_id' => $owner_other_id);
                    $land_owner_model = $this->land_owner_model->get_row_by_opt($land_owner_opt);
                    if (!$land_owner_model) {
                        $land_owner_model = $this->land_owner_model->save($land_owner_opt);
                        $land_owner_id = $land_owner_model['id'];
                    }
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_land_occupy");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete Loan_income by id
        if (isset($id)) {
            //delete loan_land_occupy_model
            $this->loan_land_occupy_model->delete($id);

//            $loan_land_occupy_model = $this->loan_land_occupy_model->get_by_id($id);
//            $loan_land_occupy_id = $loan_land_occupy_model['loan_land_occupy_id'];
//            $land_id = $loan_land_occupy_model['land_id'];
            //delete Land_model
//            $this->land_model->delete($land_id);
            //delete loan_land_occupy_model
//            $this->loan_land_occupy_model->delete($id);
            //delete land_owner_model
//            $this->land_owner_model->delete_opt(array('land_id' => $land_id));
            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_land_occupy');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
