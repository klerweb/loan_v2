<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_loan_objective extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'ความประสงค์จะขอใช้สินเชื่อ';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_type_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);
        $loan_data['loan_period_year'] = $loan_data['loan_period_year'] ? $loan_data['loan_period_year'] : 30;

        //loan_desc
        if (!$loan_data['loan_desc']) {
            //objective_desc
            $objective_desc = '';
            $loan_type_model = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id));
            if ($loan_type_model) {
                foreach ($loan_type_model as $loan_type) {
                    $objective_desc .= ", {$loan_type['loan_objective_name']}";
                }
            }
            $objective_desc = substr($objective_desc, 1);

            $loan_data['loan_desc'] = $objective_desc;
        }

        //loan_amount
        if ($loan_data['loan_amount'] == 0) {
            $money = 0;
            $loan_type_model = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id));
            if ($loan_type_model) {
                foreach ($loan_type_model as $loan_type) {
                    if ($loan_type['loan_objective_id'] == 1) {
                        $money = $money + $loan_type['loan_type_redeem_amount'];
                    } elseif ($loan_type['loan_objective_id'] == 2) {
                        $money = $money + $loan_type['loan_type_contract_amount'];
                    } elseif ($loan_type['loan_objective_id'] == 3) {
                        $money = $money + $loan_type['loan_type_case_amount'];
                    } elseif ($loan_type['loan_objective_id'] == 4) {
                        $money = $money + $loan_type['loan_type_sale_amount'];
                    } elseif ($loan_type['loan_objective_id'] == 5) {
                        $money = $money + $loan_type['loan_type_farm_payment_amount'] * $loan_type['loan_type_farm_year_amount'];
                    }
                }
            }

            $loan_data['loan_amount'] = money_num_to_str($money);
        } else {
            $loan_data['loan_amount'] = money_num_to_str($loan_data['loan_amount']);
        }


        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'session_form' => $session_form
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_objective.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_loan_objective', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {
            $post['loan_amount'] = money_str_to_num($post['loan_amount']);
            $this->loan_model->save($post, $loan_id);

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_loan_objective");
    }

}
