<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_loan_doc extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'เอกสารหลักฐานที่นำมาประกอบการยื่นคำขอสินเชื่อ';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_doc_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //form_data
        $form_data = array(
            'loan_doc_id' => '',
            'loan_doc_type_id' => '',
            'loan_doc_type_other' => '',
            'loan_doc_desc' => '',
            'loan_doc_path' => '',
            'view_status' => '',
            'loan_doc_date' => '',
            'loan_doc_filename' => '',
            'loan_doc_month' => '',
            'loan_doc_black_no' => '',
            'loan_doc_red_no' => '',
            'loan_doc_court' => ''
        );

        //config & master
        $mst = array(
            'config_loan_doc_type_list' => $this->mst_model->get_all('loan_doc_type'),
        );

        //loan_doc
        $loan_doc_data = $this->loan_doc_model->get_by_opt(array('loan_id' => $loan_id));

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'loan_doc_data' => $loan_doc_data,
            'form_data' => $form_data,
            'mst' => $mst,
            'session_form' => $session_form
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_doc.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_loan_doc', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {



        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {

            if ($post['view_status'] == 1) {
                redirect("loan/form_loan_doc");
                exit;
            }

            //upload file
            $loan_doc_path = '';

            $check_file = (isset($_FILES['loan_doc_path']) && $_FILES['loan_doc_path']['size'] > 0);
            if ($check_file) {
                $uploads_path = $this->config->item('uploads_loan_doc_path');
//                $config['upload_path'] = "assets/uploads/loan_doc/{$loan_id}";
                $config['upload_path'] = $uploads_path;
                $config['allowed_types'] = '*';
                $config['max_size'] = 0;
                $extension_file = explode('.', $_FILES['loan_doc_path']['name']);

                //file_name
                $config['file_name'] = date('YmdHis') . md5(date('YmdHis')) . '.' . end($extension_file);
//                if ($post['loan_doc_filename']) {
//                    $config['file_name'] = $post['loan_doc_filename'] . '.' . end($extension_file);
//                } else {
//                    $config['file_name'] = date('YmdHis') . md5(date('YmdHis')) . '.' . end($extension_file);
//                }

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('loan_doc_path');
                $upload_data = $this->upload->data();
                $loan_doc_path = $uploads_path . $config['file_name'];
            }

            //insert loan_doc
            unset($post['view_status']);

            $post['loan_doc_path'] = $loan_doc_path;
            $post['loan_doc_date'] = $this->loan_lib->check_val_null(dth_to_den($post['loan_doc_date']));
            $post['loan_doc_month'] = $this->loan_lib->check_val_null(dth_to_den($post['loan_doc_month']));
            $loan_doc_model = $this->loan_doc_model->save($post);
            $loan_doc_id = $loan_doc_model['id'];

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_loan_doc");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        if (isset($id)) {
            //delete file
            $loan_doc_model = $this->loan_doc_model->get_by_id($id);
            @unlink($loan_doc_model['loan_doc_path']);

            //delete data
            $this->loan_doc_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_loan_doc');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);

        $form_data = $this->loan_doc_model->get_by_id($id);
        $form_data['view_status'] = 1;

        //config & master
        $mst = array(
            'config_loan_doc_type_list' => $this->mst_model->get_all('loan_doc_type'),
        );

        //loan_doc
        $loan_doc_data = $this->loan_doc_model->get_by_opt(array('loan_id' => $loan_id));

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //data_content
        $data_content = array(
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'loan_doc_data' => $loan_doc_data,
            'form_data' => $form_data,
            'mst' => $mst,
            'session_form' => $session_form
        );

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}"),
            'เอกสารหลักฐานที่นำมาประกอบการยื่นคำขอสินเชื่อ' => site_url("loan/form_loan_doc")
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            '$data_breadcrumb' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js'),
            'title' => 'แบบบันทึกขอสินเชื่อ',
            'content' => $this->parser->parse('form_loan_doc', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function view_file() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
        $form_data = $this->loan_doc_model->get_by_id($id);

        //file_exists
        if($form_data && file_exists($form_data['loan_doc_path'])){
            //extension
            $file_parts = pathinfo($form_data['loan_doc_path']);
            $extension = $file_parts['extension'];

            $this->load->helper('download');
            $data = file_get_contents($form_data['loan_doc_path']);
            $name = $form_data['loan_doc_filename'] . ".{$extension}";
            force_download($name, $data); 
        }else{
            echo 'File not found.';
        }                   
    }

}
