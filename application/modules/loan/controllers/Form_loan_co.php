<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_loan_co extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'ผู้กู้ร่วม (ถ้ามี)';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('person_model');
        $this->load->model('loan_co_model');
        $this->load->model('loan_model');
        $this->load->model('land_owner_model');
        $this->load->model('land_model');
        $this->load->model('loan_income_model');
        $this->load->model('loan_expenditure_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //config & master
        $mst = array(
            'config_title_list' => $this->mst_model->get_all('title'),
            'config_race_list' => $this->mst_model->get_all('race'),
            'config_nationality_list' => $this->mst_model->get_all('nationality'),
            'config_religion_list' => $this->mst_model->get_all('religion'),
            'config_career_list' => $this->mst_model->get_all('career'),
            'config_relationship_list' => $this->mst_model->get_all('relationship'),
            'master_sex_list' => $this->mst_model->get_all('sex'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            'master_status_married_list' => $this->mst_model->get_all('status_married'),
        );

        //loan_co
        $loan_co_data = $this->loan_co_model->report_list(array('loan_id' => $loan_id));

        //borrower_data
        $borrower_data = $this->person_model->get_by_id($loan_model['person_id']);

        //person_data
        $person_data = $this->loan_lib->person_fields();
        $form_data = array(
            'loan_co_id' => '',
            'loan_id' => '',
            'person_id' => '',
            'relationship_id' => '',
        );

        //loan_data
        $loan_data = $loan_model;

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';
        $table_param = '';

        //table_income
        $loan_income_co_data = array();
        $loan_income_person_data = array();
        //table_expenditure
        $loan_expenditure_co_data = array();
        $loan_expenditure_person_data = array();

        //get
        $get = $this->input->get();
        if ($get) {
            if (isset($get['edit'])) {
                $loan_co_model = $this->loan_co_model->get_by_id($get['edit']);
                $person_data = $this->person_model->get_by_id($loan_co_model['person_id']);

                $person_data['person_income_per_month'] = money_num_to_str($person_data['person_income_per_month']);
                $person_data['person_expenditure_per_month'] = money_num_to_str($person_data['person_expenditure_per_month']);
                $person_data['person_thaiid_format'] = formatThaiid($person_data['person_thaiid']);
                $person_data['person_birthdate'] = den_to_dth($person_data['person_birthdate']);
                $person_data['person_card_startdate'] = den_to_dth($person_data['person_card_startdate']);
                $person_data['person_card_expiredate'] = den_to_dth($person_data['person_card_expiredate']);


                $form_data = $loan_co_model;

                //$table_land_building_param
                $table_land_building_data = array(
                    'callbackurl' => base64_encode("loan/form_loan_co/?edit={$get['edit']}"),
                    'owner_id' => $loan_co_model['person_id'],
                    'save_loan_guarantee_land' => 0,
                    'loan_type_id' => '',
                );
                $table_land_building_param = json_encode($table_land_building_data);
                $table_land_building_param = base64_encode($table_land_building_param);

                //$table_param
                $table_param_data = array(
                    'callbackurl' => base64_encode("loan/form_loan_co/?edit={$get['edit']}"),
                    'owner_id' => $loan_co_model['person_id'],
                    'save_loan_guarantee_land' => 0,
                    'loan_type_id' => '',
                );
                $table_param = json_encode($table_param_data);
                $table_param = base64_encode($table_param);

                //table_land_building
                $land_owner_model = $this->land_owner_model->get_by_opt(array('person_id' => $loan_co_model['person_id']));
                //$land_model = $this->land_model->get_by_opt(array('loan_type_id' => $loan_type_model['loan_type_id']));
                if ($land_owner_model) {
                    $table_land_building = array();
                    foreach ($land_owner_model as $key_land => $land_owner) {
                        $land = $this->land_model->get_by_id($land_owner['land_id']);
                        $table_land_building[$key_land] = $land;
                        $land_full_detail = $this->loan_lib->land_full_detail($land);
                        $table_land_building[$key_land]['address'] = $land_full_detail['address'];

                        $building_model = $this->building_model->get_by_opt(array('land_id' => $land['land_id']));
                        if ($building_model) {
                            foreach ($building_model as $key_building => $building) {
                                $building_full_detail = $this->loan_lib->building_full_detail($building);
                                $building_model[$key_building]['building_type_name'] = $building_full_detail['building_type_name'];
                                $building_model[$key_building]['address'] = $building_full_detail['address'];
                            }

                            $table_land_building[$key_land]['building_table'] = $building_model;
                        }
                    }
                }

                //table_loan_income
                $loan_income_co_data = $this->loan_income_model->get_by_opt(array('person_id' => $loan_co_model['person_id']));
                $loan_income_person_data = $this->loan_income_model->get_by_opt(array('person_id' => $loan_model['person_id']));

                //table_loan_expenditure
                $loan_expenditure_co_data = $this->loan_expenditure_model->get_by_opt(array('person_id' => $loan_co_model['person_id']));
                $loan_expenditure_person_data = $this->loan_expenditure_model->get_by_opt(array('person_id' => $loan_model['person_id']));
            }
        }

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );
        
        $loan_person_data =  $this->person_model->get_by_id($loan_data['person_id']);
        $loan_spouse_data =  $this->person_model->get_by_id($loan_data['loan_spouse']);

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'person_data' => $person_data,
            'loan_data' => $loan_data,
        	'loan_person_data' => $loan_person_data,
        	'loan_spouse_data' => $loan_spouse_data,
            'loan_co_data' => $loan_co_data,
            'session_form' => $session_form,
            'mst' => $mst,
            'form_data' => $form_data,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
            'table_param' => $table_param,
            'loan_income_co_data' => $loan_income_co_data,
            'loan_income_person_data' => $loan_income_person_data,
            'loan_expenditure_co_data' => $loan_expenditure_co_data,
            'loan_expenditure_person_data' => $loan_expenditure_person_data,
            'borrower_data' => $borrower_data,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_co.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_loan_co', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            //person_save
            $person_id = $this->loan_lib->person_save($post['person'], $post['person']['person_id']);

            if (isset($post['loan_co_id']) && $post['loan_co_id']) {
                //update loan_co
                $loan_co_id = $post['loan_co_id'];
                
                $loan_co_data = array(
                    'loan_id' => $loan_id,
                    'person_id' => $person_id,
                    'relationship_id' => $post['relationship_id'] ? $post['relationship_id'] : '',
                );

                $loan_co_model = $this->loan_co_model->save($loan_co_data, $loan_co_id);                
            } else {
                //insert loan_co
                $loan_co_model = $this->loan_co_model->get_by_opt(array('loan_id' => $loan_id, 'person_id' => $person_id));

                if (!$loan_co_model && $loan_model['person_id'] != $person_id) {
                    $loan_co_data = array(
                        'loan_id' => $loan_id,
                        'person_id' => $person_id,
                        'relationship_id' => $post['relationship_id'] ? $post['relationship_id'] : '',
                    );

                    $loan_co_model = $this->loan_co_model->save($loan_co_data);
                    $loan_co_id = $loan_co_model['id'];
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan/form_loan_co");
        }
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete loan_co by id
        if (isset($id)) {
            $this->loan_co_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_loan_co');
    }

}
