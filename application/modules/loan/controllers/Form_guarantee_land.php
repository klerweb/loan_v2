<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_guarantee_land extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $page_name = 'หลักประกันการขอใช้สินเชื่อ';
    private $form_name = 'อสังหาริมทรัพย์';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_guarantee_land_model');
        $this->load->model('person_model');
        $this->load->model('land_model');
        $this->load->model('land_owner_model');
        $this->load->model('building_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];
        $person_id = $loan_model['person_id'];

        //form_data
        $form_data = array(
            'loan_guarantee_land_id' => '',
            'loan_id' => $loan_id,
            'land_id' => '',
            'land_owner_id' => '',
            'land_type_id' => '',
            'land_no' => '',
            'land_area_rai' => '',
            'land_area_ngan' => '',
            'land_area_wah' => '',
            'land_addr_no' => '',
            'land_addr_moo' => '',
            'land_addr_district_id' => '',
            'land_addr_amphur_id' => '',
            'land_addr_province_id' => '',
            'land_buiding_detail_id' => '',
            'building_id' => '',
            'building_owner' => '',
            'building_type_id' => '',
            'building_no' => '',
            'building_moo' => '',
            'building_name' => '',
            'building_soi' => '',
            'building_road' => '',
            'building_district_id' => '',
            'building_amphur_id' => '',
            'building_province_id' => '',
            //bind
            'loan_guarantee_land_bind_type' => '',
            'loan_guarantee_land_title_id' => '',
            'loan_guarantee_land_fname' => '',
            'loan_guarantee_land_lname' => '',
            'loan_guarantee_land_bind_amount' => money_num_to_str(0),
            'loan_guarantee_land_relief' => '',
            'loan_guarantee_land_relief_permonth' => money_num_to_str(0),
            'loan_guarantee_land_relief_enddate' => '',
            'loan_guarantee_land_benefit' => '',
            'loan_guarantee_land_benefit_other' => '',
        );

        $form_data['buiding_owner_status'] = 0; //ตนเอง
        //person_data
        $person_id = $loan_model['person_id'];
        if ($person_id) {
            $person_data = $this->loan_lib->person_detail($person_id);
        } else {
            $person_data = $this->loan_lib->person_fields();
        }

        $form_data['building_owner_title_id'] = $person_data['title_id'];
        $form_data['building_owner_fname'] = $person_data['person_fname'];
        $form_data['building_owner_lname'] = $person_data['person_lname'];

        //building
        $building_table = array();

        //table_land_building
        $table_land_building = array();
        $table_land_building_param = '';

        //$get
        $get = $this->input->get();
        if ($get) {
            //edit Loan_income by id
            if (isset($get['edit'])) {
                //loan_guarantee_land_model
                $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_by_id($get['edit']);
                $loan_guarantee_land_id = $loan_guarantee_land_model['loan_guarantee_land_id'];
                $land_id = $loan_guarantee_land_model['land_id'];

                //land_model
                $land_model = $this->land_model->get_by_id($land_id);

                //land_owner_model
                $land_owner_model = $this->land_owner_model->get_row_by_opt(array('land_id' => $land_id));

                //$form_data
                $form_data = array(
                    'loan_guarantee_land_id' => $loan_guarantee_land_id,
                    'loan_id' => $loan_id,
                    'land_id' => $land_id,
                    'land_owner_id' => $land_owner_model['land_owner_id'],
                    'land_type_id' => $land_model['land_type_id'],
                    'land_no' => $land_model['land_no'],
                    'land_area_rai' => $land_model['land_area_rai'],
                    'land_area_ngan' => $land_model['land_area_ngan'],
                    'land_area_wah' => $land_model['land_area_wah'],
                    'land_addr_no' => $land_model['land_addr_no'],
                    'land_addr_moo' => $land_model['land_addr_moo'],
                    'land_addr_district_id' => $land_model['land_addr_district_id'],
                    'land_addr_amphur_id' => $land_model['land_addr_amphur_id'],
                    'land_addr_province_id' => $land_model['land_addr_province_id'],
                    'land_buiding_detail_id' => $land_model['land_buiding_detail_id'],
                    //building
                    'person_id' => $person_id,
                    'building_id' => '',
                    'building_owner' => '',
                    'building_type_id' => '',
                    'building_no' => '',
                    'building_moo' => '',
                    'building_name' => '',
                    'building_soi' => '',
                    'building_road' => '',
                    'building_district_id' => '',
                    'building_amphur_id' => '',
                    'building_province_id' => '',
                	   'building_estimate_price' => '',
                    //bind
                    'loan_guarantee_land_bind_type' => $loan_guarantee_land_model['loan_guarantee_land_bind_type'],
                    'loan_guarantee_land_title_id' => $loan_guarantee_land_model['loan_guarantee_land_title_id'],
                    'loan_guarantee_land_fname' => $loan_guarantee_land_model['loan_guarantee_land_fname'],
                    'loan_guarantee_land_lname' => $loan_guarantee_land_model['loan_guarantee_land_lname'],
                    'loan_guarantee_land_bind_amount' => money_num_to_str($loan_guarantee_land_model['loan_guarantee_land_bind_amount']),
                    'loan_guarantee_land_relief' => $loan_guarantee_land_model['loan_guarantee_land_relief'],
                    'loan_guarantee_land_relief_permonth' => money_num_to_str($loan_guarantee_land_model['loan_guarantee_land_relief_permonth']),
                    'loan_guarantee_land_relief_enddate' => $loan_guarantee_land_model['loan_guarantee_land_relief_enddate'] ? den_to_dth($loan_guarantee_land_model['loan_guarantee_land_relief_enddate']) : '',
                    'loan_guarantee_land_benefit' => $loan_guarantee_land_model['loan_guarantee_land_benefit'],
                    'loan_guarantee_land_benefit_other' => $loan_guarantee_land_model['loan_guarantee_land_benefit_other'],
                );

                $form_data['buiding_owner_status'] = 0; //ตนเอง
                //building
                $building_table = $this->building_model->get_by_opt(array('land_id' => $land_model['land_id']));

                if ($building_table) {
                    foreach ($building_table as $key => $data) {
                        $building_full_detail = $this->loan_lib->building_full_detail($data);
                        $building_table[$key]['building_type_name'] = $building_full_detail['building_type_name'];
                        $building_table[$key]['address'] = $building_full_detail['address'];
                    }
                }

                if (isset($get['building_id'])) {
                    $building_model = $this->building_model->get_by_id($get['building_id']);
                    $form_data = array_merge($form_data, $building_model);
                } else {
                    $form_data['building_owner_title_id'] = $person_data['title_id'];
                    $form_data['building_owner_fname'] = $person_data['person_fname'];
                    $form_data['building_owner_lname'] = $person_data['person_lname'];
                }
            }

            //$table_land_building_param
            $table_land_building_data = array(
                'callbackurl' => base64_encode("loan/form_guarantee_land/?edit={$get['edit']}"),
              //  'owner_id' => $loan_model['person_id'],
                'save_loan_guarantee_land' => 1,
                'loan_type_id' => '',
            );
            $table_land_building_param = json_encode($table_land_building_data);
            $table_land_building_param = base64_encode($table_land_building_param);
        }

        //config & master
        $mst['config_land_type_list'] = $this->mst_model->get_all('land_type');
        $mst['config_building_type_list'] = $this->mst_model->get_all('building_type');
        $mst['master_district_list'] = array();
        $mst['master_amphur_list'] = array();
        $mst['master_province_list'] = $this->mst_model->get_table_location_all('province');
        $mst['land_buiding_detail_id_list'] = $this->land_model->get_land_buiding_detail_id_list();
        $mst['config_title_list'] = $this->mst_model->get_all('title');
        $mst['loan_guarantee_land_bind_type_list'] = $this->loan_guarantee_land_model->get_loan_guarantee_land_bind_type_list();
        $mst['loan_guarantee_land_benefit_list'] = $this->loan_guarantee_land_model->get_loan_guarantee_land_benefit_list();
        $mst['buiding_owner_status_list'] = $this->land_model->get_buiding_owner_status_list();

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}"),
            $this->page_name => site_url("loan/form_guarantee"),
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'session_form' => $session_form,
            'form_data' => $form_data,
            'mst' => $mst,
            'building_table' => $building_table,
            'loan_model' => $loan_model,
            'person_data' => $person_data,
            'table_land_building' => $table_land_building,
            'table_land_building_param' => $table_land_building_param,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_guarantee_land.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_guarantee_land', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            //save land
            if(isset($post['land_id']) && $post['land_id']){
                $land_model = $this->land_model->get_by_id($post['land_id']);
                $post['loan_type_id'] = $land_model['loan_type_id'] ? $land_model['loan_type_id'] : NULL;
            }else{
                $post['loan_type_id'] = NULL;
            }


            $land_id = $this->loan_lib->land_save($post);

            if ($land_id) {
                //insert loan_guarantee_land_model
                $post_loan_guarantee_land_model = array(
                    'loan_id' => $post['loan_id'],
                    'land_id' => $land_id,
                    'loan_guarantee_land_bind_type' => $post['loan_guarantee_land_bind_type'],
                    'loan_guarantee_land_benefit' => empty($post['loan_guarantee_land_benefit']) ? '' : join(",", $post['loan_guarantee_land_benefit']),
                    'loan_guarantee_land_benefit_other' => $post['loan_guarantee_land_benefit_other'],
                );

                if (isset($post['loan_guarantee_land_bind_type']) && $post['loan_guarantee_land_bind_type'] == 1) {
                    $post_loan_guarantee_land_model['loan_guarantee_land_title_id'] = $post['loan_guarantee_land_title_id'];
                    $post_loan_guarantee_land_model['loan_guarantee_land_fname'] = $post['loan_guarantee_land_fname'];
                    $post_loan_guarantee_land_model['loan_guarantee_land_lname'] = $post['loan_guarantee_land_lname'];
                    $post_loan_guarantee_land_model['loan_guarantee_land_bind_amount'] = money_str_to_num($post['loan_guarantee_land_bind_amount']);
                    $post_loan_guarantee_land_model['loan_guarantee_land_relief'] = $post['loan_guarantee_land_relief'];
                    $post_loan_guarantee_land_model['loan_guarantee_land_relief_permonth'] = money_str_to_num($post['loan_guarantee_land_relief_permonth']);
                    $post_loan_guarantee_land_model['loan_guarantee_land_relief_enddate'] = $post['loan_guarantee_land_relief_enddate'] ? dth_to_den($post['loan_guarantee_land_relief_enddate']) : NULL;
                } else {
                    $post_loan_guarantee_land_model['loan_guarantee_land_title_id'] = NULL;
                    $post_loan_guarantee_land_model['loan_guarantee_land_fname'] = NULL;
                    $post_loan_guarantee_land_model['loan_guarantee_land_lname'] = NULL;
                    $post_loan_guarantee_land_model['loan_guarantee_land_bind_amount'] = NULL;
                    $post_loan_guarantee_land_model['loan_guarantee_land_relief'] = NULL;
                    $post_loan_guarantee_land_model['loan_guarantee_land_relief_permonth'] = NULL;
                    $post_loan_guarantee_land_model['loan_guarantee_land_relief_enddate'] = NULL;
                }

                $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model, $post['loan_guarantee_land_id']);
                $loan_guarantee_land_id = $loan_guarantee_land_model['id'];
            }

            //land_buiding
            if (isset($post['land_buiding_detail_id']) && $post['land_buiding_detail_id'] == 1) {
                if (!$post['loan_guarantee_land_id']) {
                    //$building_owner
                    $building_owner = $this->person_model->get_by_id($loan_model['person_id']);

                    $building = array(
                        'land_id' => $land_id,
                        'building_owner_title_id' => $building_owner['title_id'],
                        'building_owner_fname' => $building_owner['person_fname'],
                        'building_owner_lname' => $building_owner['person_lname'],
                        'building_no' => $post['building_no'],
                        'building_moo' => $post['building_moo'],
                        'building_name' => $post['building_name'],
                        'building_soi' => $post['building_soi'],
                        'building_road' => $post['building_road'],
                        'building_district_id' => $post['building_district_id'],
                        'building_amphur_id' => $post['building_amphur_id'],
                        'building_province_id' => $post['building_province_id'],
                        'building_type_id' => $post['building_type_id'],
                    	'building_estimate_price' => $post['building_estimate_price']
                    );

                    $this->building_model->save($building);
                }
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );

            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_guarantee");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete loan_guarantee_land_model by id
        if (isset($id)) {
            $loan_guarantee_land_model = $this->loan_guarantee_land_model->get_by_id($id);
            $loan_guarantee_land_id = $loan_guarantee_land_model['loan_guarantee_land_id'];
            $land_id = $loan_guarantee_land_model['land_id'];

            //delete Land_model
            //$this->land_model->delete($land_id);
            //delete loan_guarantee_land_model
            $this->loan_guarantee_land_model->delete($id);

            //delete land_owner_model
            //$this->land_owner_model->delete_opt(array('land_id' => $id));
            //delete building
            //$this->building_model->delete_opt(array('land_id' => $land_id));
            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_guarantee');
    }

    public function save_building() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            $building = array(
                'land_id' => $post['land_id'],
                'building_owner_title_id' => $post['building_owner_title_id'],
                'building_owner_fname' => $post['building_owner_fname'],
                'building_owner_lname' => $post['building_owner_lname'],
                'building_no' => $post['building_no'],
                'building_moo' => $post['building_moo'],
                'building_name' => $post['building_name'],
                'building_soi' => $post['building_soi'],
                'building_road' => $post['building_road'],
                'building_district_id' => $post['building_district_id'],
                'building_amphur_id' => $post['building_amphur_id'],
                'building_province_id' => $post['building_province_id'],
                'building_type_id' => $post['building_type_id'],
            	'building_estimate_price' => $post['building_estimate_price']
            );

            $this->building_model->save($building, $post['building_id']);


            //land
            $land = array(
                'land_buiding_detail_id' => 1,
            );

            $land_model = $this->land_model->save($land, $post['land_id']);


            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan/form_guarantee_land/?edit={$post['loan_guarantee_land_id']}");
        }

        redirect("loan/form_guarantee");
    }

    public function delete_building() {
        $id = $this->uri->segment(4);

        //get
        $get = $this->input->get();

        //delete Loan_income by id
        if (isset($id)) {
            $this->building_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_guarantee_land/?edit={$get['loan_guarantee_land_id']}");
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
