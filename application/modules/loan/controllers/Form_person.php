<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_person extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'ผู้ขอสินเชื่อ';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('person_model');
        $this->load->model('loan_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];
        $loan_model['loan_date'] = den_to_dth($loan_model['loan_date']);
        $loan_model['loan_location'] = $loan_model['loan_location'] ? $loan_model['loan_location'] : 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)';

        //config & master
        $mst = array(
            'config_land_type_list' => $this->mst_model->get_all('land_type'),
            'config_title_list' => $this->mst_model->get_all('title'),
            'config_race_list' => $this->mst_model->get_all('race'),
            'config_nationality_list' => $this->mst_model->get_all('nationality'),
            'config_religion_list' => $this->mst_model->get_all('religion'),
            'config_career_list' => $this->mst_model->get_all('career'),
            'master_sex_list' => $this->mst_model->get_all('sex'),
            'master_district_list' => array(),
            'master_amphur_list' => array(),
            'master_province_list' => $this->mst_model->get_table_location_all('province'),
            'master_status_married_list' => $this->mst_model->get_all('status_married'),
            'person_address_type' => $this->person_model->get_person_address_type(),
        );

        //person_data
        $person_id = $loan_model['person_id'];
        if ($person_id) {
            $person_data = $this->person_model->get_by_id($person_id);
            $person_data['person_birthdate'] = den_to_dth($person_data['person_birthdate']);
            $person_data['person_card_startdate'] = den_to_dth($person_data['person_card_startdate']);
            $person_data['person_card_expiredate'] = den_to_dth($person_data['person_card_expiredate']);
        } else {
            $person_data = $this->loan_lib->person_fields();
        }

        //spouse_data
        $spouse_id = $loan_model['loan_spouse'];
        if ($spouse_id) {
            $spouse_data = $this->person_model->get_by_id($spouse_id);
            $spouse_data['person_birthdate'] = den_to_dth($spouse_data['person_birthdate']);
            $spouse_data['person_card_startdate'] = den_to_dth($spouse_data['person_card_startdate']);
            $spouse_data['person_card_expiredate'] = den_to_dth($spouse_data['person_card_expiredate']);
        } else {
            $spouse_data = $this->loan_lib->person_fields();
        }

        //loan_data
        $loan_data = $loan_model;

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}"),
        );

        //data_content
        $data_content = array(
            'loan_code' => $loan_model['loan_code'],
            'form_name' => $form_name,
            'person_data' => $person_data,
            'spouse_data' => $spouse_data,
            'loan_data' => $loan_data,
            'session_form' => $session_form,
            'mst' => $mst,
            'loan_model' => $loan_model,
        );

        $data = array(
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/form_person.js', 'modules_loan/loan.js'),
            'content' => $this->parser->parse('form_person', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function project($year='')
    {
    	if($year=='')
    	{
    		$data['project'] = array();
    	}
    	else
    	{
    		$this->load->model('project/project_model', 'project');

    		$data['project'] = $this->project->getByYear($year);
    	}

    	$this->parser->parse('project', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();

        if ($post) {
            $person_id = $this->loan_lib->person_save($post['person'], $post['person_id']);

            if ($post['spouse']['person_fname'] && $post['spouse']['person_lname']) {
                $spouse_id = $this->loan_lib->spouse_save($post['spouse'], $post['person'], $post['spouse_id']);
            } else {
                $spouse_id = 0;
            }

            /* update loan */
            $loan_data = array(
                'person_id' => $person_id,
                'loan_spouse' => $spouse_id,
                'loan_member_amount' => $post['loan_member_amount'],
                'loan_labor_amount' => $post['loan_labor_amount'],
                'loan_location' => $post['loan_location'],
            	'loan_year' => $post['loan_year'],
            	'project_id' => $post['project_id'],
                'loan_date' => dth_to_den($post['loan_date']),
            );

            $this->loan_model->save($loan_data, $loan_id);

            //update session_loan
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);

            redirect("loan/form_person");
        }
    }

}
