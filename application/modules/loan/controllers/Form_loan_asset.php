<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Form_loan_asset extends MY_Controller {

    private $menu = 'loan';
    private $menu_name = 'สินเชื่อ';
    private $title = 'แบบบันทึกขอสินเชื่อ';
    private $form_name = 'ทรัพย์สินอื่นนอกจากที่ดินของผู้ขอสินเชื่อและคู่สมรส';

    function __construct() {
        parent::__construct();

        //model
        $this->load->model('mst_model');
        $this->load->model('loan_model');
        $this->load->model('loan_asset_model');

        //helper
        $this->load->helper('loan_helper');

        //library
        $this->load->library('loan_lib');

        //check_page_permission
        check_page_permission();
    }

    public function index() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //form_data
        $form_data = array(
            'loan_id' => $loan_id,
            'loan_asset_id' => '',
            'loan_asset_name' => '',
            'loan_asset_amount' => money_num_to_str(0),
            'loan_asset_quantity' => 0,
            'loan_asset_uom' => '',
        );

        //config & master
        $mst = array(
            'config_uom_list' => $this->mst_model->get_all('uom')
        );
   
        //$get
        $get = $this->input->get();
        if ($get) {
            //edit Loan_asset by id
            if (isset($get['edit'])) {
                $loan_asset_edit = $this->loan_asset_model->get_by_id($get['edit']);

                //form_data
                $form_data = array(
                    'loan_id' => $loan_id,
                    'loan_asset_id' => $loan_asset_edit['loan_asset_id'],
                    'loan_asset_name' => $loan_asset_edit['loan_asset_name'],
                    'loan_asset_amount' => money_num_to_str($loan_asset_edit['loan_asset_amount']),
                    'loan_asset_quantity' => $loan_asset_edit['loan_asset_quantity'],
                    'loan_asset_uom' => $loan_asset_edit['loan_asset_uom'],
                );
            }
        }

        //loan_asset
        $loan_asset_data = $this->loan_asset_model->get_by_opt(array('loan_id' => $loan_id));

        //loan_data
        $loan_data = $this->loan_model->get_by_id($loan_id);

        //session_form
        if (isset($_SESSION['session_form'])) {
            $session_form = $_SESSION['session_form'];
            $this->session->unset_userdata('session_form');
        } else {
            $session_form = array();
        }

        //form_name
        $form_name = $this->form_name;

        //data_menu & data_breadcrumb
        $data_menu['menu'] = $this->menu;

        //data_breadcrumb
        $data_breadcrumb['menu'] = array(
            $this->menu_name => site_url('loan'),
            $this->title => site_url("loan/edit_loan/{$loan_id}")
        );

        //data_content
        $data_content = array(
            'form_name' => $form_name,
            'loan_code' => $loan_model['loan_code'],
            'loan_data' => $loan_data,
            'loan_asset_data' => $loan_asset_data,
            'session_form' => $session_form,
            'form_data' => $form_data,
            'mst' => $mst,
        );

        $data = array(
            'css_other' => array('modules_loan.css'),
            'js_other' => array('modules_loan/loan.js', 'modules_loan/form_loan_asset.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'title' => $form_name,
            'content' => $this->parser->parse('form_loan_asset', $data_content, TRUE)
        );

        $this->parser->parse('main', $data);
    }

    public function save() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //$post
        $post = $this->input->post();
        if ($post) {        
            $post['loan_asset_amount'] = money_str_to_num($post['loan_asset_amount']);
            if ($post['loan_asset_id']) {
                //update loan_income
                $id = $post['loan_asset_id'];
                unset($post['loan_asset_id']);

                $loan_asset_model = $this->loan_asset_model->save($post, $id);
                $loan_asset_id = $loan_asset_model['id'];
            } else {
                unset($post['loan_asset_id']);

                //insert loan_asset
                $loan_asset_model = $this->loan_asset_model->save($post);
                $loan_asset_id = $loan_asset_model['id'];
            }

            //update session
            $session_loan['loan_model'] = $this->loan_model->get_by_id($loan_id);
            $this->session->set_userdata('session_loan', $session_loan);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'บันทึกข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect("loan/form_loan_asset");
    }

    public function delete() {
        $id = $this->uri->segment(4);

        //delete Loan_asset by id
        if (isset($id)) {
            $this->loan_asset_model->delete($id);

            //session_form
            $session_form = array(
                'status' => 'success',
                'message' => 'ลบข้อมูลสำเร็จ.'
            );
            $this->session->set_userdata('session_form', $session_form);
        }

        redirect('loan/form_loan_asset');
    }

    public function view() {
        //loan_model from session
        $loan_model = $_SESSION['session_loan']['loan_model'];
        $loan_id = $loan_model['loan_id'];

        //id
        $id = $this->uri->segment(4);
    }

}
