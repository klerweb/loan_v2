<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_model extends CI_Model {

    private $table = 'loan';
    private $table_person = 'person';
    private $table_config_title = 'config_title';
    private $_status_success = '2';
    private $_status_draf = '1';
    private $_status_cancel = '0';
    private $_col_id;
    private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    public $_status_desc = array('ยกเลิก', 'แบบร่าง', 'เรียบร้อย');

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        if(check_permission_isowner('loan')) $this->db->where($this->_col_createby, get_uid_login());
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        if(check_permission_isowner('loan')) $this->db->where($this->_col_createby, get_uid_login());
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }
    
    public function get_row_by_opt($opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        if(check_permission_isowner('loan')) $this->db->where($this->_col_createby, get_uid_login());
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        $result['loan_datefull'] = thai_display_date($result['loan_date']);
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if($query->row_array()){
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        }else{
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
        	$array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_draf;
            
            //not null
            $array['loan_date'] = date("Y-m-d");
            $array['loan_amount'] = 0;
            $array['loan_period_year'] = 0;
            $array['person_id'] = 0;
            $array['loan_member_amount'] = 0;
            $array['loan_labor_amount'] = 0;
            $array['loan_dept_amount'] = 0;
            $array['loan_residence_type'] = 0;

            //insert
            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();

            //gen loan_code
            $array['loan_code'] = self::gen_loan_code($id);
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        } else {
        	$array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");

            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }

    public function search($option_limit, $loan_code, $person_thaiid = '', $fullname = '') {
        $where = '';
        if ($loan_code != '') {
            $where .= "OR {$this->table}.loan_code LIKE '%{$loan_code}%' ";
        }

        if ($person_thaiid != '') {
            $where .= "OR {$this->table_person}.person_thaiid LIKE '%" . $person_thaiid . "%' ";
        }

        if ($fullname != '') {
            $where .= "OR {$this->table_person}.person_fname LIKE '%" . $fullname
                    . "%' OR {$this->table_person}.person_lname LIKE '%" . $fullname . "%'";
        }

        if(isset($option_limit)){         
            $this->db->limit($option_limit['limit'], $option_limit['offset']);
        }
        
        if ($where != '') {
            $where = substr($where, 3);
            $this->db->where("(" . $where . ")", NULL, FALSE);
        }


        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_person, "loan.person_id = {$this->table_person}.person_id", "LEFT");
        $this->db->join($this->table_config_title, "{$this->table_person}.title_id = {$this->table_config_title}.title_id", "LEFT");
        if(check_permission_isowner('loan')) $this->db->where($this->_col_createby, get_uid_login());
        $this->db->order_by('loan_id','desc');
                
        $query = $this->db->get();
        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function search_numrows($loan_code, $person_thaiid = '', $fullname = ''){
        $where = '';
        if ($loan_code != '') {
            $where .= "OR {$this->table}.loan_code LIKE '%{$loan_code}%' ";
        }

        if ($person_thaiid != '') {
            $where .= "OR {$this->table_person}.person_thaiid LIKE '%" . $person_thaiid . "%' ";
        }

        if ($fullname != '') {
            $where .= "OR {$this->table_person}.person_fname LIKE '%" . $fullname
                    . "%' OR {$this->table_person}.person_lname LIKE '%" . $fullname . "%'";
        }

        if ($where != '') {
            $where = substr($where, 3);
            $this->db->where("(" . $where . ")", NULL, FALSE);
        }
        
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_person, "loan.person_id = {$this->table_person}.person_id", "LEFT");
        $this->db->join($this->table_config_title, "{$this->table_person}.title_id = {$this->table_config_title}.title_id", "LEFT");
        if(check_permission_isowner('loan')) $this->db->where($this->_col_createby, get_uid_login());

        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }


    public function gen_loan_code($id) {
        return "LOAN-{$id}";
    }
    
    public function get_status_list(){
        return $this->_status_desc;
    }
    
    public function update_status_by_loan($loan_id, $status) {
    	$array[$this->_col_updateby] = get_uid_login();
        $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
        $array[$this->_col_status] = $status;
        $this->db->where('loan_id', $loan_id);
        $this->db->update($this->table, $array);
    }
    
    public function sum_amount($person_id){
        $this->db->select_sum('loan_amount');
        $this->db->where('person_id', $person_id); 
        $query = $this->db->get($this->table);
        $result = $query->row_array();
        
        return $result;
    }

}

?>