<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class land_model extends CI_Model {

    private $table = 'land';
    private $table_province = 'master_province';
    private $table_amphur = 'master_amphur';
    private $table_district = 'master_district';
    private $table_land_type = 'config_land_type';
    private $_col_id;
    private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    private $_status_active = '1';
    private $_status_cancel = '0';
    private $reservation = array(
        '1' => 'อยู่',
        '2' => 'ไม่อยู่',
        '3' => 'คาบ',
    );
    private $reservation_map = array(
        '1' => 'ภูมิประเทศ',
        '2' => 'ภพถ่ายทางอากาศ',
    );
    private $land_buiding_detail_id_list = array(
        0 => 'ที่ดิน',
        1 => 'ที่ดินพร้อมสิ่งปลูกสร้าง'
    );
    private $land_buiding_detail_id_list2 = array(
        3 => 'ข้อมูลที่ดินเดียวกับที่นำไปจำนอง',
        0 => 'ที่ดิน',
        1 => 'ที่ดินพร้อมสิ่งปลูกสร้าง'
    );
    private $buiding_owner_status_list = array(
        0 => 'ตนเอง',
        1 => 'ผู้อื่น'
    );

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if (isset($opt['person_id'])) {
            $this->db->where('person_id', $opt['person_id']);
        }
        if (isset($opt['loan_type_id'])) {
            $this->db->where('loan_type_id', $opt['loan_type_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        if (isset($opt['loan_type_id'])) {
            $this->db->where('loan_type_id', $opt['loan_type_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if ($query->row_array()) {
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        } else {
            $result = 1;
        }

        return $result;
    }

    public function checkDuplicate($opt) {
        $this->db->where('land_no', $opt['land_no']);
        $this->db->where('land_addr_no', $opt['land_addr_no']);
        $this->db->where('land_addr_moo', $opt['land_addr_moo']);
        $this->db->where('land_addr_district_id', $opt['land_addr_district_id']);
        $this->db->where('land_addr_amphur_id', $opt['land_addr_amphur_id']);
        $this->db->where('land_addr_province_id', $opt['land_addr_province_id']);

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->order_by('land_id', 'desc');
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
        	$array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            if (isset($array['land_buiding_detail_id'])) {
                $array['land_buiding_detail_id'] = $array['land_buiding_detail_id'] ? $array['land_buiding_detail_id'] : 0;
            }

            //checkDuplicate
            $result_dup = $this->checkDuplicate($array);

            if (!$result_dup) {
                $this->db->insert($this->table, $array);
                $id = $this->db->insert_id();
            } else {
                $id = $result_dup['land_id'];
                $array[$this->_col_updateby] = get_uid_login();
            	$array[$this->_col_updatedate] = date("Y-m-d H:i:s");
                $this->db->where($this->_col_id, $id);
                $this->db->update($this->table, $array);
            }
        } else {
            $array[$this->_col_updatedate] = date("Y-m-d");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }

    public function delete_opt($opt = array()) {
        if ($opt) {
            if (isset($opt['loan_type_id'])) {
                $this->db->where('loan_type_id', $opt['loan_type_id']);
            }
            $this->db->delete($this->table);
        }

        return FALSE;
    }

    public function get_reservation() {
        return $this->reservation;
    }

    public function get_reservation_map() {
        return $this->reservation_map;
    }

    public function get_land_buiding_detail_id_list() {
        return $this->land_buiding_detail_id_list;
    }

    public function get_land_buiding_detail_id_list2() {
        return $this->land_buiding_detail_id_list2;
    }

    public function get_buiding_owner_status_list() {
        return $this->buiding_owner_status_list;
    }

    public function get_view($id) {
        $this->db->select("{$this->table}.*, {$this->table_province}.province_name as land_province_name, "
                . "{$this->table_amphur}.amphur_name as land_amphur_name, {$this->table_district}.district_name as land_district_name, "
                . "{$this->table_land_type}.land_type_name");
        $this->db->from($this->table);
        //จังหวัด master_province
        $this->db->join($this->table_province, "{$this->table}.land_addr_province_id = {$this->table_province}.province_id", 'LEFT');
        //อำเภอ master_amphur
        $this->db->join($this->table_amphur, "{$this->table}.land_addr_amphur_id = {$this->table_amphur}.amphur_id", 'LEFT');
        //ตำบล master_district
        $this->db->join($this->table_district, "{$this->table}.land_addr_district_id = {$this->table_district}.district_id", 'LEFT');
        //land_type
        $this->db->join($this->table_land_type, "{$this->table}.land_type_id = {$this->table_land_type}.land_type_id", 'LEFT');

        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

}

?>