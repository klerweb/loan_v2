<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Building_model extends CI_Model {

    private $table = 'building';
    private $_col_id;
    private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    private $_status_active = '1';
    private $_status_cancel = '0';
    private $building_state = array(
        '1' => 'building_state1',
        '2' => 'building_state2',
        '3' => 'building_state3',
        '4' => 'building_state4',
        '5' => 'building_state5',
    );
    private $building_quality = array(
        '1' => 'building_quality1',
        '2' => 'building_quality2',
        '3' => 'building_quality3',
    );
    private $building_style = array(
        '1' => 'style1',
        '2' => 'style2',
        '3' => 'style3',
    );
    private $building_skill = array(
        '1' => 'skill1',
        '2' => 'skill2',
        '3' => 'skill3',
    );

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if (isset($opt['land_id'])) {
            $this->db->where('land_id', $opt['land_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();
        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if($query->row_array()){
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        }else{
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
            $array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();
        } else {
        	$array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }
    
    public function delete_opt($opt = array()) {
        if ($opt) {
            if (isset($opt['land_id'])) {
                $this->db->where('land_id', $opt['land_id']);
            }
            $this->db->delete($this->table);
        }

        return FALSE;
    }

    public function get_building_state() {
        return $this->building_state;
    }

    public function get_building_quality() {
        return $this->building_quality;
    }
    
    public function get_building_style() {
        return $this->building_style;
    }

    public function get_building_skill() {
        return $this->building_skill;
    }

    public function get_building_type_by_id($building_type_id){
        $this->db->select("*");
        $this->db->from('config_building_type');
        $this->db->where('building_type_id', $building_type_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
    
    public function get_district($id){
        $this->db->select("*");
        $this->db->from('master_district');
        $this->db->where('district_id', $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
    
    public function get_amphur($id){
        $this->db->select("*");
        $this->db->from('master_amphur');
        $this->db->where('amphur_id', $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
    
    public function get_province($id){
        $this->db->select("*");
        $this->db->from('master_province');
        $this->db->where('province_id', $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
}


?>