<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_guarantee_land_model extends CI_Model {

    private $table = 'loan_guarantee_land';
    private $perfix_table = 'loan_guarantee_land';
    private $table_loan = 'loan';
    private $table_land = 'land';
    private $table_config_land_type = 'config_land_type';
    private $table_person = 'person';
    private $table_config_title = 'config_title';
    private $table_land_owner = 'land_owner';
    private $_col_id;
    private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    private $_status_active = '1';
    private $_status_cancel = '0';
    public $loan_guarantee_land_bind_type = array(0 => 'ไม่มีภาระผูกพัน', 1 => 'มีภาระผูกพัน ');
    private $loan_guarantee_land_benefit = array(
        1 => 'เป็นที่ดินสำหรับใช้ทำเกษตรกรรม',
        2 => 'เป็นที่ดินสำหรับการอยู่อาศัย',
        3 => 'ไม่ได้นำไปจำนอง ขายฝาก หรือใช้ประกันหนี้กับสถาบันการเงิน หรือบุคคลอื่น',
        4 => 'นำไปจำนอง ขายฝาก หรือใช้ประกันหนี้กับสถาบันการเงิน หรือบุคคลอื่น (ระบุ)',
    );

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->perfix_table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->perfix_table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where('loan_id', $opt['loan_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        if (isset($opt['land_id'])) {
            $this->db->where('land_id', $opt['land_id']);
        }
        
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if ($query->row_array()) {
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        } else {
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
            $array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();
        } else {
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }

    public function delete_by_loan($loan_id) {
        $this->db->delete($this->table, array('loan_id' => $loan_id));
    }

    public function get_table_data1($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where("{$this->table}.loan_id", $opt['loan_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_loan, "{$this->table}.loan_id = {$this->table_loan}.loan_id", 'LEFT');
        $this->db->join($this->table_land, "{$this->table}.land_id = {$this->table_land}.land_id", 'LEFT');
        $this->db->join($this->table_config_land_type, "{$this->table_land}.land_type_id = {$this->table_config_land_type}.land_type_id", 'LEFT');
        $this->db->join($this->table_land_owner, "{$this->table_land}.land_id = {$this->table_land_owner}.land_id", 'LEFT');
        $this->db->join($this->table_person, "{$this->table_land_owner}.person_id = {$this->table_person}.person_id", 'LEFT');
        $this->db->join($this->table_config_title, "{$this->table_person}.title_id = {$this->table_config_title}.title_id", "LEFT");
        $query = $this->db->get();
        echo $this->db->last_query();
        exit;
        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        if ($result) {
            foreach ($result as $key => $data) {
                $result[$key]['land_buiding_detail_detail'] = $data['land_buiding_detail_id'] == 1 ? 'มี' : 'ไม่มี';
            }
        }

        return $result;
    }

    public function get_table_data($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where("{$this->table}.loan_id", $opt['loan_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_loan, "{$this->table}.loan_id = {$this->table_loan}.loan_id", 'LEFT');
        $this->db->join($this->table_land, "{$this->table}.land_id = {$this->table_land}.land_id", 'LEFT');
        $this->db->join($this->table_config_land_type, "{$this->table_land}.land_type_id = {$this->table_config_land_type}.land_type_id", 'LEFT');
        $this->db->join($this->table_land_owner, "{$this->table_land}.land_id = {$this->table_land_owner}.land_id AND {$this->table_loan}.person_id = {$this->table_land_owner}.person_id", 'LEFT');
        $this->db->join($this->table_person, "{$this->table_land_owner}.person_id = {$this->table_person}.person_id", 'LEFT');
        $this->db->join($this->table_config_title, "{$this->table_person}.title_id = {$this->table_config_title}.title_id", "LEFT");        
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        if ($result) {
            foreach ($result as $key => $data) {
                $result[$key]['land_buiding_detail_detail'] = $data['land_buiding_detail_id'] == 1 ? 'มี' : 'ไม่มี';
            }
        }

        return $result;
    }

    public function update_status_by_loan($loan_id, $status) {
        $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
        $array[$this->_col_status] = $status;
        $this->db->where('loan_id', $loan_id);
        $this->db->update($this->table, $array);
    }
    
    public function get_loan_guarantee_land_bind_type_list(){
        return $this->loan_guarantee_land_bind_type;
    }
    
    public function get_loan_guarantee_land_benefit_list(){
        return $this->loan_guarantee_land_benefit;
    }
            
            

}

?>