<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_type_model extends CI_Model {


    private $table = 'loan_type';
    private $table_mst_objective = 'master_loan_objective';
    private $table_relationship = 'config_relationship';
    private $_col_id;
    private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    public $_status_active = '1';
    public $_status_cancel = '0';
    public $redeem_type = array(
        '1' => 'การจำนอง',
        '2' => 'การขายฝาก',
    );
    public $case_type = array(
        '1' => 'แพ่ง',
        '2' => 'อาญา',
    );
    public $sale_type = array(
        '1' => 'เพื่อซื้อที่ดินหลุดขายฝาก',
        '2' => 'เพื่อซื้อที่ดินขายทอดตลาด',
    );

//21:40 Songja@Ipad mini 1. เพื่อซื้อที่ดินหลุดขายฝาก
//2. เพื่อซื้อที่ดินขายทอดตลาด
//21:42 Songja@Ipad mini loan_type_sale_type


    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_mst_objective, "{$this->table}.loan_objective_id = {$this->table_mst_objective}.loan_objective_id", "LEFT");
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where('loan_type.loan_id', $opt['loan_id']);
        }

        if (isset($opt['loan_type_status'])) {
            $this->db->where('loan_type.loan_type_status', $opt['loan_type_status']);
        }

        if (isset($opt['loan_objective_id'])) {
            $this->db->where('loan_type.loan_objective_id', $opt['loan_objective_id']);
        }

        if (isset($opt['not_loan_objective_id'])) {
            $this->db->where('loan_type.loan_objective_id <> ', $opt['not_loan_objective_id'], FALSE);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_mst_objective, "{$this->table}.loan_objective_id = {$this->table_mst_objective}.loan_objective_id", "LEFT");
        $this->db->join($this->table_relationship, "{$this->table}.loan_type_redeem_relationship = {$this->table_relationship}.relationship_id", "LEFT");
        $this->db->order_by($this->table.'.loan_objective_id','asc');
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();
        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_mst_objective, "{$this->table}.loan_objective_id = {$this->table_mst_objective}.loan_objective_id", "LEFT");
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_mst_objective, "{$this->table}.loan_objective_id = {$this->table_mst_objective}.loan_objective_id", "LEFT");
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if($query->row_array()){
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        }else{
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
            $array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();
        } else {
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }

    public function delete_by_loan($loan_id) {
        $this->db->delete($this->table, array('loan_id' => $loan_id));
    }

    public function get_redeem_type() {
        return $this->redeem_type;
    }

    public function get_case_type() {
        return $this->case_type;
    }

    public function get_sale_type() {
        return $this->sale_type;
    }

    public function update_status_by_loan($loan_id, $status) {
        $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
        $array[$this->_col_status] = $status;
        $this->db->where('loan_id', $loan_id);
        $this->db->update($this->table, $array);
    }

    public function get_view($id) {
        $this->db->select("{$this->table}.*"
//        . ", addr_pre_province.province_name as person_addr_pre_province_name"
//                . ", addr_pre_amphur.amphur_name as person_addr_pre_amphur_name"
//                . ", addr_pre_district.district_name as person_addr_pre_district_name"
//                . ", addr_card_province.province_name as person_addr_card_province_name"
//                . ", addr_card_amphur.amphur_name as person_addr_pre_card_name"
//                . ", addr_card_district.district_name as person_addr_card_district_name"
//                . ", config_title.title_name"
//                . ", sex.sex_name, race.race_name, nationality.nationality_name, religion.religion_name"
//                . ", career.career_name"
//                . ", status_married.status_married_name"
                . "");
        $this->db->from($this->table);

        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
}

?>
