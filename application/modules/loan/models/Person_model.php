<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Person_model extends CI_Model {

    private $table = 'person';
    private $table_province = 'master_province';
    private $table_amphur = 'master_amphur';
    private $table_district = 'master_district';
    private $table_sex = 'master_sex';
    private $table_race = 'config_race';
    private $table_nationality = 'config_nationality';
    private $table_religion = 'config_religion';
    private $table_career = 'config_career';
    private $table_status_married = 'master_status_married';

    private $table_land_type = 'config_land_type';
    private $config_title = 'config_title';
    private $_col_id;
     private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    private $_status_active = '1';
    private $_status_cancel = '0';
    private $person_address_type = array(
        1 => 'มีที่อยู่อาศัยเป็นของตน',
        2 => 'ไม่มีที่อยู่อาศัยเป็นของตนเอง',
        3 => 'อาศัยอยู่กับผู้อื่น'
    );

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if (isset($opt['person_thaiid'])) {
            $this->db->where('person_thaiid', $opt['person_thaiid']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        if (isset($opt['person_thaiid'])) {
            $this->db->where('person_thaiid', $opt['person_thaiid']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->order_by($this->_col_id,'desc');
        $this->db->where('person_status', $this->_status_active);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if ($query->row_array()) {
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        } else {
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
    //  echo '<pre>';
    //  print_r($array);
      //  print_r($id);
    //  echo '</pre>';
        if (!$id) {
            $array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();
        } else {
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }

    public function get_view($id) {
        $this->db->select("{$this->table}.*"
        . ", addr_pre_province.province_name as person_addr_pre_province_name"
                . ", addr_pre_amphur.amphur_name as person_addr_pre_amphur_name"
                . ", addr_pre_district.district_name as person_addr_pre_district_name"
                . ", addr_card_province.province_name as person_addr_card_province_name"
                . ", addr_card_amphur.amphur_name as person_addr_card_amphur_name"
                . ", addr_card_district.district_name as person_addr_card_district_name"
                . ", config_title.title_name"
                . ", sex.sex_name, race.race_name, nationality.nationality_name, religion.religion_name"
                . ", career.career_name"
                . ", status_married.status_married_name"
                . "");
        $this->db->from($this->table);

        //คำนำหน้าชื่อ
        $this->db->join("{$this->config_title} config_title", "{$this->table}.title_id = config_title.title_id", 'LEFT');
        //เพศ
        $this->db->join("{$this->table_sex} sex", "{$this->table}.sex_id = sex.sex_id", 'LEFT');
        //เชื่อชาติ
        $this->db->join("{$this->table_race} race", "{$this->table}.race_id = race.race_id", 'LEFT');
        //สัญชาติ
        $this->db->join("{$this->table_nationality} nationality", "{$this->table}.nationality_id = nationality.nationality_id", 'LEFT');
        //ศาสนา
        $this->db->join("{$this->table_religion} religion", "{$this->table}.religion_id = religion.religion_id", 'LEFT');
        //จังหวัด master_province
        $this->db->join("{$this->table_province} addr_pre_province", "{$this->table}.person_addr_pre_province_id = addr_pre_province.province_id", 'LEFT');
        $this->db->join("{$this->table_province} addr_card_province", "{$this->table}.person_addr_card_province_id = addr_card_province.province_id", 'LEFT');
        //อำเภอ master_amphur
        $this->db->join("{$this->table_amphur} addr_pre_amphur", "{$this->table}.person_addr_pre_amphur_id = addr_pre_amphur.amphur_id", 'LEFT');
        $this->db->join("{$this->table_amphur} addr_card_amphur", "{$this->table}.person_addr_card_amphur_id = addr_card_amphur.amphur_id", 'LEFT');
        //ตำบล master_district
        $this->db->join("{$this->table_district} addr_pre_district", "{$this->table}.person_addr_pre_district_id = addr_pre_district.district_id", 'LEFT');
        $this->db->join("{$this->table_district} addr_card_district", "{$this->table}.person_addr_card_district_id = addr_card_district.district_id", 'LEFT');
        //อาชีพ
        $this->db->join("{$this->table_career} career", "{$this->table}.career_id = career.career_id", 'LEFT');
        //ความสัมพันธ์ผู้ขอสินเชื่อ
        $this->db->join("{$this->table_status_married} status_married", "{$this->table}.status_married_id = status_married.status_married_id", 'LEFT');

        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    public function get_person_address_type(){
        return $this->person_address_type;
    }
}

?>
