<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mst_model extends CI_Model {

    private $table = array(
        'bank' => 'config_bank',
        'buiding_detail_land' => 'config_buiding_detail_land',
        'building_part' => 'config_building_part',
        'building_part_sub' => 'config_building_part_sub',
        'building_type' => 'config_building_type',
        'career' => 'config_career',
        'irigation' => 'config_irigation',
        'land_type' => 'config_land_type',
        'loan_doc_type' => 'config_loan_doc_type',
        'loan_objective' => 'config_loan_objective',
        'location_detail_land' => 'config_location_detail_land',
        'nationality' => 'config_nationality',
        'non_loan_case' => 'config_non_loan_case',
        'non_loan_objective' => 'config_non_loan_objective',
        'product' => 'config_product',
        'race' => 'config_race',
        'relationship' => 'config_relationship',
        'religion' => 'config_religion',
        'title' => 'config_title',
        'type_quality_land' => 'config_type_quality_land',
        'loan_objective' => 'master_loan_objective',
        'sex' => 'master_sex',
        'status_life' => 'master_status_life',
        'status_married' => 'master_status_married',
    	'building_type' => 'config_building_type',
    	'uom' => 'config_uom',
    	'bondsman_type' => 'config_bondsman_type',
    	'zipcode' => 'master_zipcode',
    );
    
    private $table_location = array(
        'amphur' => 'master_amphur',
        'district' => 'master_district',
        'province' => 'master_province',
        'geo' => 'master_geo',
    );
    private $_status_active = '1';
    private $_status_cancel = '0';

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    public function get_all($table_name) {
        $this->db->select("*");
        $this->db->from($this->table[$table_name]);
        $this->db->where("{$table_name}_status", (string) $this->_status_active);
        $this->db->order_by("{$table_name}_id",'ASC');

        $query = $this->db->get();
        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }    
    public function get_by_id($table_name, $id){
        $this->db->select("*");
        $this->db->from($this->table[$table_name]);
        $this->db->where("{$table_name}_id", $id);
        
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
    
    public function get_by_opt_table($table_name, $table_name_id, $id){
        $this->db->select("*");
        $this->db->from($table_name);
        $this->db->where($table_name_id, $id);
        
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
    
    public function get_table_location_all($table_name) {
        $this->db->select("*");
        $this->db->from($this->table_location[$table_name]);
        
        if($table_name == 'province'){
            $this->db->order_by("province_name", "asc");
        }elseif($table_name == 'amphur'){
            $this->db->order_by("amphur_name", "asc");
        }elseif($table_name == 'district'){
            $this->db->order_by("district_name", "asc");
        }

        $query = $this->db->get();
        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }
    public function get_table_location_by_id($table_name, $id){
        $this->db->select("*");
        $this->db->from($this->table_location[$table_name]);
        $this->db->where("{$table_name}_id", $id);
        
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }
    
    public function get_all_zipcode(){
        $this->db->select("*");
        $this->db->from($this->table['zipcode']);
        $this->db->order_by("zipcode_id",'ASC');

        $query = $this->db->get();
        $result_list = $query->num_rows() != 0 ? $query->result_array() : array();
        $result = array();
        if($result_list){
            foreach($result_list as $key => $data){
                $result[$data['district_id']] = $data['zipcode'];
            }
        }

        return $result;
    }

}

?>