<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_guarantee_bond_model extends CI_Model {

    private $table = 'loan_guarantee_bond';
    private $perfix_table = 'loan_bond';
    private $_col_id;
     private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    private $_status_active = '1';
    private $_status_cancel = '0';
    

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->perfix_table . '_id';
        $this->_col_createby = $this->perfix_table . '_createby';
        $this->_col_createdate = $this->perfix_table . '_createdate';
        $this->_col_updateby = $this->perfix_table . '_updateby';
        $this->_col_updatedate = $this->perfix_table . '_updatedate';
        $this->_col_status = $this->perfix_table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if(isset($opt['loan_id'])){
            $this->db->where('loan_id', $opt['loan_id']);
        }
        
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if($query->row_array()){
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        }else{
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
            $array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();
        } else {
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }
    
    public function delete_by_loan($loan_id) {
        $this->db->delete($this->table, array('loan_id' => $loan_id));
    }
    
    public function update_status_by_loan($loan_id, $status) {
        $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
        $array[$this->_col_status] = $status;
        $this->db->where('loan_id', $loan_id);
        $this->db->update($this->table, $array);
    }

}

?>