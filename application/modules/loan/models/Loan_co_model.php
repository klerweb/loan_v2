<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_co_model extends CI_Model {

    private $table = 'loan_co';
    private $table_person = 'person';
    private $table_config_relationship = 'config_relationship';
    private $table_config_title = 'config_title';
    private $_col_id;
    private $_col_createby;
    private $_col_createdate;
    private $_col_updateby;
    private $_col_updatedate;
    private $_col_status;
    private $_status_active = '1';
    private $_status_cancel = '0';

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        //default value of columns
        $this->_col_id = $this->table . '_id';
        $this->_col_createby = $this->table . '_createby';
        $this->_col_createdate = $this->table . '_createdate';
        $this->_col_updateby = $this->table . '_updateby';
        $this->_col_updatedate = $this->table . '_updatedate';
        $this->_col_status = $this->table . '_status';
    }

    public function get_all() {
        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_by_opt($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where('loan_id', $opt['loan_id']);
        }
        if (isset($opt['person_id'])) {
            $this->db->where('person_id', $opt['person_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function get_row_by_opt($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where('loan_id', $opt['loan_id']);
        }
        if (isset($opt['person_id'])) {
            $this->db->where('person_id', $opt['person_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : array();

        return $result;
    }

    public function get_by_id($id, $opt = array()) {
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->_col_id, $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
    }

    private function get_max_id() {
        $this->db->select_max($this->_col_id);
        $query = $this->db->get($this->table);
        if ($query->row_array()) {
            $result = $query->row_array();
            $result = $result[$this->_col_id] + 1;
        } else {
            $result = 1;
        }

        return $result;
    }

    public function save($array, $id = '') {
        if (!$id) {
            $array[$this->_col_createby] = get_uid_login();
            $array[$this->_col_createdate] = date("Y-m-d H:i:s");
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $array[$this->_col_status] = $this->_status_active;

            $this->db->insert($this->table, $array);
            $id = $this->db->insert_id();
        } else {
            $array[$this->_col_updateby] = get_uid_login();
            $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
            $this->db->where($this->_col_id, $id);
            $this->db->update($this->table, $array);
        }

        $num_row = $this->db->affected_rows();

        return array('id' => $id, 'rows' => $num_row);
    }

    public function delete($id) {
        $this->db->delete($this->table, array($this->_col_id => $id));
    }

    public function delete_by_loan($loan_id) {
        $this->db->delete($this->table, array('loan_id' => $loan_id));
    }

    public function report_list($opt = array()) {
        if (isset($opt['loan_id'])) {
            $this->db->where('loan_id', $opt['loan_id']);
        }

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_person, "{$this->table}.person_id = {$this->table_person}.person_id", 'LEFT');
        $this->db->join($this->table_config_relationship, "{$this->table}.relationship_id = {$this->table_config_relationship}.relationship_id", 'LEFT');
        $this->db->join($this->table_config_title, "{$this->table_person}.title_id = {$this->table_config_title}.title_id", "LEFT");

        $query = $this->db->get();
        $result = $query->num_rows() != 0 ? $query->result_array() : array();

        return $result;
    }

    public function update_status_by_loan($loan_id, $status) {
        $array[$this->_col_updatedate] = date("Y-m-d H:i:s");
        $array[$this->_col_status] = $status;
        $this->db->where('loan_id', $loan_id);
        $this->db->update($this->table, $array);
    }

}

?>