<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nationality extends MY_Controller
{
	private $title_page = 'สัญชาติ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('nationality_model', 'nationality');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->nationality->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_nationality/nationality.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('nationality', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('nationality')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มสัญชาติ';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('nationality_model', 'nationality');
			$data_content['data'] = $this->nationality->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มสัญชาติ';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขสัญชาติ';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_nationality/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('nationality_model', 'nationality');
		
		$array['nationality_name'] = $this->input->post('txtNationalityName');
		$array['nationality_status'] = '1';
		
		$id = $this->nationality->save($array, $this->input->post('txtNationalityId'));
		
		if($this->input->post('txtNationalityId')=='')
		{
			$data = array(
					'alert' => 'บันทึกสัญชาติเรียบร้อยแล้ว',
					'link' => site_url('nationality'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขสัญชาติเรียบร้อยแล้ว',
					'link' => site_url('nationality'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('nationality_model', 'nationality');
		$this->nationality->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกสัญชาติเรียบร้อยแล้ว',
				'link' => site_url('nationality'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['nationality_name'] = '';
		
		return $data;
	}
}