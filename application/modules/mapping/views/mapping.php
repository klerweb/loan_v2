<p class="nomargin" style="margin-bottom:20px; text-align:right;">
	<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo base_url()?>mapping/form'">เพิ่มข้อมูล</button>
</p>
<div class="table-responsive">
	<table class="table table-striped mb30">
		<thead>
			<tr>
            	<th>#</th>
                <th>เมนู</th>
                <th>รายละเอียด</th>
                <th>สถานะ</th>
                <th></th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if(!empty($result))
			{
				$i = 1;
				foreach ($result as $row)
				{
		?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $row['module_name']; ?></td>
				<td><?php echo $row['module_desc']; ?></td>
				<td><?php echo $row['module_status'] == '1' ? 'Active' : 'Cancel'; ?></td>
           		<td><a href="<?php echo base_url().'mapping/form/'.$row['module_id']?>" data-toggle="tooltip" title="Edit" class="delete-row tooltips" data-original-title="Edit"><i class="fa fa-edit"></i></a></td>
           	</tr>
		<?php
				}	
			} 
		?>
		</tbody>
	</table>
</div><!-- table-responsive -->