<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo !empty($id) ? 'แก้ไข' : 'เพิ่ม';?>ข้อมูล</h4>
	</div>
	<div class="panel-body">
    <form id="frm1" name="frm1" method="post" action="<?php echo base_url().'mapping/save'?>" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">ชื่อเมนู</label>
	    </div>
		<div class="col-sm-3 form-group">
	    	<input type="text" name="module_name" id="module_name" class="form-control" value="<?php echo @$result['module_name'];?>" <?php echo $id ? 'disabled':'';?>>
		</div>  
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">รายละเอียด</label>
	    </div>  
		<div class="col-sm-3 form-group">
	        <input type="text" name="module_desc" id="module_desc" class="form-control" value="<?php echo @$result['module_desc'];?>" required>
		</div>                                                                            
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">สถานะ</label>
	    </div>
	    <div class="col-sm-3 form-group">
	        	<div class="row" >
	            	<div class="col-sm-6">
	            		<input type="radio" id="module_status1" name="module_status" value="1" <?php echo @$result['module_status']!='0' ? 'checked="checked"' : ''?>>
		            	<label>Active</label>
	                </div>
	                <div class="col-sm-6">
	            		<input type="radio" id="module_status0" name="module_status" value="0" <?php echo @$result['module_status']=='0' ? 'checked="checked"' : ''?>>
		            	<label>Cancel</label>
	                </div>
	       		</div>
		 </div>
	 </div>
	 <input type="hidden" name="module_id" value="<?php echo $id;?>">
	</form>
	</div><!-- panel-body -->
	<div class="panel-footer right">
         <button id="btnSubmit" class="btn btn-primary">บันทึก</button>
	</div><!-- panel-footer -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>
<script>
        jQuery(document).ready(function () {     

        	var response;
            $.validator.addMethod(
                "check_dup", 
                function(value, element) {
                    $.ajax({
                        type: "POST",
                        url: base_url+"mapping/check_dup",
                        data: "name="+value,
                        dataType:"html",
                        success: function(msg)
                        {
                            response = ( msg == true ) ? true : false;
                        }
                     });
                    return response;
                },
                "Name is Already !"
            );
             
        	$("#frm1").validate({
        		rules: {
        	        "module_name":{
        	        	//check_dup:true,
        	        	required: true
        	        },
        		},
        		highlight: function(element) {
        			$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        		},
        		unhighlight: function(element) {
        	        $(element).closest('.form-group').removeClass('has-error');
        	    },
        		errorElement: 'span',
        	    errorClass: 'help-block',
        	    errorPlacement: function(error, element) {
        	        if(element.parent('.input-group').length) {
        	            error.insertAfter(element.parent());
        	        } else {
        	            error.insertAfter(element);
        	        }
        	    },
        		success: function(element) {
        			$(element).closest(".form-group").removeClass("has-error");
        		}		
        	});

        	jQuery('#btnSubmit').click(function (e) {
            	$('#frm1').submit();
            });
        });
</script>
