<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapping extends MY_Controller 
{
	private $title = 'รายการเมนู';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('mapping_model');
	}
	
	public function index()
	{		
		$data_menu['menu'] = 'user';
		$data_breadcrumb['menu'] = array('ผู้ใช้งาน' =>'#');
		
		$data_content = array(
			'result' => $this->mapping_model->search()
		);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('mapping', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'user';
		$data_breadcrumb['menu'] = array('ผู้ใช้งาน' =>'#',$this->title => site_url('user'));
		
		$data_content = array(
				'id' => $id,
				'result' => $id ? $this->mapping_model->get_table_by_id($id) : array()
			);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$result = $this->mapping_model->save($_POST,$_POST['module_id']);
		if($result ['rows'] > 0)
		{
			redirect_url(base_url().'mapping','บันทึกข้อมูลเรียบร้อยแล้ว');
		}else{
			redirect_url(base_url().'mapping','ไม่สามารถบันทึกข้อมูลได้');
		}
	}
	
	public function check_dup()
	{
		$result = $this->mapping_model->check_dup($_POST['name']);
		if(!empty($result)){
			echo false;
		}else{
			echo true;
		}
	}
	
}
