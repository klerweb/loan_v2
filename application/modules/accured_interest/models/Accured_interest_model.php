<?php
class accured_interest_model extends CI_Model{

	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	function load_show_accured($day)
	{
		$data_non_payment1="";
		$date_pay="";
		$array_pay="";
		
		$this->db->order_by('date_payout','asc');
		$check_array=array('date_payout<='=>$day,'status'=>'N','loan_pay_number'=>1);
		$this->db->where($check_array);
		$query_data_non_payment=$this->db->get('payment_schedule');
		if($query_data_non_payment->num_rows()>0)
		{
			foreach($query_data_non_payment->result() as $data_non_payment)
			{
					$check_null_loan_contract=array('loan_contract_id'=>$data_non_payment->loan_contract_id,'loan_contract_no!='=>null);
					$this->db->where($check_null_loan_contract);
					$query_check_loan_contract=$this->db->get('loan_contract');
					if($query_check_loan_contract->num_rows()>0)
					{
						$array_pay=$array_pay.$data_non_payment->loan_contract_id.",";
					}
			}
			
			$datas=explode(",",$array_pay); //array ที่ต้องการตรวจสอบ
			$check=array(); //array สำหรับการตรวจสอบ
			for ($i=0; $i<count($datas); $i++) {
				
				$b=1;
				for($j=0;$j<$i;$j++)
				{
					if($datas[$i]==$datas[$j]) $b=0;
					
				}
				if($b==1)
				{
					$check[]=$datas[$i];
				}
			}
			
			
			$this->load->model('accured_interest_model', 'accured_interest_m');
			
			for ($i=0;$i<count($check)-1;$i++)
			{
				$data_non_payment1=$data_non_payment1.$this->accured_interest_m->check_non_pay($check[$i],$day)."\\";
			}
			
		}
		
		$array_pay="";
		$this->db->order_by('date_payout','asc');
		$check_array=array('date_payout<='=>$day,'status'=>'D');
		$this->db->where($check_array);
		$query_data_non_payment=$this->db->get('payment_schedule');
		if($query_data_non_payment->num_rows()>0)
		{
			foreach($query_data_non_payment->result() as $data_non_payment)
			{
				$check_null_loan_contract=array('loan_contract_id'=>$data_non_payment->loan_contract_id,'loan_contract_no!='=>null);
					$this->db->where($check_null_loan_contract);
					$query_check_loan_contract=$this->db->get('loan_contract');
					if($query_check_loan_contract->num_rows()>0)
					{
						$array_pay=$array_pay.$data_non_payment->loan_contract_id.",";		
					}
				
			}
			$datas=explode(",",$array_pay); //array ที่ต้องการตรวจสอบ
			$check=array(); //array สำหรับการตรวจสอบ
			for ($i=0; $i<count($datas); $i++) {
				
				$b=1;
				for($j=0;$j<$i;$j++)
				{
					if($datas[$i]==$datas[$j]) $b=0;
					
				}
				if($b==1)
				{
					$check[]=$datas[$i];
				}
			}
			$this->load->model('accured_interest_model', 'accured_interest_m');
			
			for ($i=0;$i<count($check)-1;$i++)
			{
				$data_non_payment1=$data_non_payment1.$this->accured_interest_m->check_pay($check[$i],$day)."\\";
			}
			
		}
		if($data_non_payment1=="")
		{
			return $day."\\"."no data";
		}
		else
		{
			return $day."\\".$data_non_payment1;
		}

	}
function check_pay($data,$day)
{
		$data_non_payment1="";
		$date_pay="";
		$interest_rate="";
		$this->db->order_by('date_payout','asc');
		$this->db->limit(1);
		$check_array=array('status'=>'D','loan_contract_id'=>$data);
		$this->db->where($check_array);
		$query_data_non_payment=$this->db->get('payment_schedule');
		$data_non_payment=$query_data_non_payment->result();
		
		//---------------------
				if($data_non_payment[0]->interest_accrued==null)
				{
					$inter_acc=0;
				}
				else
				{
					$inter_acc=$data_non_payment[0]->interest_accrued;
				}
				$check1=array('loan_contract_id'=>$data,'loan_pay_number'=>$data_non_payment[0]->loan_pay_number);
				$this->db->where($check1);
				$this->db->limit(1);
				$query_check_last_pay=$this->db->get('loan_invoice');
				$check_last_pay=$query_check_last_pay->result();
				$this->db->where('invoice_no',$check_last_pay[0]->invoice_no);
				$this->db->order_by('date_transfer','DESC');
				$this->db->limit(1);
				$query_get_last_pay=$this->db->get('loan_payment');
				$get_last_pay=$query_get_last_pay->result();
				$date_pay=$get_last_pay[0]->date_transfer;
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_id=$this->db->get('loan_contract');
				$data_loan_id=$query_data_loan_id->result();
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_balance=$this->db->get('loan_balance');
				$data_loan_balance=$query_data_loan_balance->result();
				$this->db->where('loan_id',$data_loan_id[0]->loan_id);
				$query_data_person_id=$this->db->get('loan');
				$data_person_id=$query_data_person_id->result();
				$this->db->where('person_id',$data_person_id[0]->person_id);
				$query_data_detail_person=$this->db->get('person');
				$data_detail_person=$query_data_detail_person->result();
				$this->db->where('title_id',$data_detail_person[0]->title_id);
				$query_data_title=$this->db->get('config_title');
				
				$data_title=$query_data_title->result();
				$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$data_non_payment[0]->date_payout.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_non_payment[0]->interest_rate.",".$data_loan_id[0]->loan_contract_date.",".$inter_acc.",".$data.","."check_pay";
				//----------------------------
		return $data_non_payment1;

}
function check_non_pay($data,$day)
{
	
		$data_non_payment1="";
		$date_pay="";
		$interest_rate="";
		$this->db->order_by('date_payout','asc');
		$this->db->limit(1);
		$check_array=array('date_payout<='=>$day,'status'=>'N','loan_contract_id'=>$data);
		$this->db->where($check_array);
		$query_data_non_payment=$this->db->get('payment_schedule');
		$data_non_payment=$query_data_non_payment->result();
		
		
				$query_data_payment=$this->db->query("select * from payment_schedule where (status='P' or status='D') AND loan_contract_id=$data");
				if ($query_data_payment->num_rows()>0)
				{
					$data_payment=$query_data_payment->result();
					$date_pay=$data_payment[0]->date_payout;
					
				}
				else
				{
					$this->db->where('loan_contract_id',$data);
					$query_data_loan_id1=$this->db->get('loan_contract');
					$data_loan_id1=$query_data_loan_id1->result();
					$date_pay=$data_loan_id1[0]->loan_contract_date;
				}
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_id=$this->db->get('loan_contract');
				$data_loan_id=$query_data_loan_id->result();
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_balance=$this->db->get('loan_balance');
				$data_loan_balance=$query_data_loan_balance->result();
				$this->db->where('loan_id',$data_loan_id[0]->loan_id);
				$query_data_person_id=$this->db->get('loan');
				$data_person_id=$query_data_person_id->result();
				$this->db->where('person_id',$data_person_id[0]->person_id);
				$query_data_detail_person=$this->db->get('person');
				$data_detail_person=$query_data_detail_person->result();
				$this->db->where('title_id',$data_detail_person[0]->title_id);
				$query_data_title=$this->db->get('config_title');
				
				$data_title=$query_data_title->result();
				$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$data_non_payment[0]->date_payout.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_non_payment[0]->interest_rate.",".$data_loan_id[0]->loan_contract_date.",".'0'.",".$data.","."check_non_pay";
				//----------------------------
		return $data_non_payment1;
}
function search_show_accured($day,$day_last,$condition)
	{
		$data_non_payment1="";
		$date_pay="";
		$array_pay="";
		
		
		if($condition==1) { $check_array=array('date_payout<='=>$day_last,'status'=>'N','loan_pay_number'=>1);
		$this->db->order_by('date_payout','asc');
		//else { $check_array=array('date_payout>='=>$day,'date_payout<='=>$day_last,'status'=>'N') ;}
		$this->db->where($check_array);
		$query_data_non_payment=$this->db->get('payment_schedule');
		
		if($query_data_non_payment->num_rows()>0)
		{
			foreach($query_data_non_payment->result() as $data_non_payment)
			{
					$check_null_loan_contract=array('loan_contract_id'=>$data_non_payment->loan_contract_id,'loan_contract_no!='=>null);
					$this->db->where($check_null_loan_contract);
					$query_check_loan_contract=$this->db->get('loan_contract');
					if($query_check_loan_contract->num_rows()>0)
					{
						$array_pay=$array_pay.$data_non_payment->loan_contract_id.",";	
					}
			}
			$datas=explode(",",$array_pay); //array ที่ต้องการตรวจสอบ
			$check=array(); //array สำหรับการตรวจสอบ
			for ($i=0; $i<count($datas); $i++) {
				
				$b=1;
				for($j=0;$j<$i;$j++)
				{
					if($datas[$i]==$datas[$j]) $b=0;
					
				}
				if($b==1)
				{
					$check[]=$datas[$i];
				}
			}
			
			$this->load->model('accured_interest_model', 'accured_interest_m');
			
			for ($i=0;$i<count($check)-1;$i++)
			{
				if($this->accured_interest_m->check_non_pay1($check[$i],$day,$day_last,$condition)!=null)
				{
					$data_non_payment1=$data_non_payment1.$this->accured_interest_m->check_non_pay1($check[$i],$day,$day_last,$condition)."\\";
				}
			}
				
		}
		}
		$array_pay="";
		if($condition==2)
		{
			$chek_null_contract=array('loan_contract_no!='=>null);
			$this->db->select('loan_contract_id');
			$this->db->where($chek_null_contract);
			$query_load_loan_contract=$this->db->get('loan_contract');
			$load_contract=array();
			foreach($query_load_loan_contract->result() as $data_loan_contract)
			{
				$load_contract[]=$data_loan_contract->loan_contract_id;
			}
			$check_array=array('date_transfer>='=>$day,'date_transfer<='=>$day_last);
			$this->db->distinct('invoice_no');
			$this->db->order_by('date_transfer','desc');
			$this->db->where($check_array);
			$query_data_non_payment=$this->db->get('loan_payment');
			$check_data_con2=array();
			$count_data_con2=0;
			foreach($query_data_non_payment->result() as $data_con2)
			{
				if($count_data_con2==0)
				{
					$check_data_con2[$count_data_con2][0]=$data_con2->invoice_no;
					$check_data_con2[$count_data_con2][1]=$data_con2->date_transfer;
					$count_data_con2=$count_data_con2+1;
				}
				else
				{
					$b=1;
					for($i=0;$i<count($check_data_con2);$i++)
					{
						if($check_data_con2[$i][0]==$data_con2->invoice_no)
						{
							$b=0;
						}
					}
					if($b==1)
					{
						$check_data_con2[$count_data_con2][0]=$data_con2->invoice_no;
						$check_data_con2[$count_data_con2][1]=$data_con2->date_transfer;
						$count_data_con2=$count_data_con2+1;
					}
				}
				
			}
			$chk_l_contract=array();
			for($i=0;$i<count($check_data_con2);$i++)
			{
				$this->db->where('invoice_no',$check_data_con2[$i][0]);
				$query_get_loan_contract=$this->db->get('loan_invoice');
				$get_loan_contract=$query_get_loan_contract->result();
				$this->db->where('loan_contract_id',$get_loan_contract[0]->loan_contract_id);
				$query_get_loan=$this->db->get('loan_contract');
				$get_loan=$query_get_loan->result();
				$this->db->where('loan_id',$get_loan[0]->loan_id);
				$query_get_person_id=$this->db->get('loan');
				$get_person_id=$query_get_person_id->result();
				$this->db->where('person_id',$get_person_id[0]->person_id);
				$query_get_person_detail=$this->db->get('person');
				$get_person_detail=$query_get_person_detail->result();
				$this->db->where('title_id',$get_person_detail[0]->title_id);
				$query_get_title_name=$this->db->get('config_title');
				$get_title_name=$query_get_title_name->result();
				$this->db->where('loan_contract_id',$get_loan_contract[0]->loan_contract_id);
				$query_get_loan_amount=$this->db->get('loan_balance');
				$get_loan_amount=$query_get_loan_amount->result();
				$chk_l_contract[]=$get_loan_contract[0]->loan_contract_id;
				$data_non_payment1=$data_non_payment1.$get_title_name[0]->title_name." ".$get_person_detail[0]->person_fname." ".$get_person_detail[0]->person_lname.",".$day_last.",".$check_data_con2[$i][1].",".$get_loan[0]->loan_contract_amount.",".$get_loan_amount[0]->loan_amount.","."3.00".",".$get_loan[0]->loan_contract_date.",".'0'.","."check_non_pay2"."\\";
			}
			$check_loan_contract=array('loan_contract_no!='=>null,'loan_contract_date<='=>$day);
			$this->db->select('loan_contract_no,loan_contract_id');
			$this->db->where($check_loan_contract);
			$query_get_loan_contract_no=$this->db->get('loan_contract');
			if($query_get_loan_contract_no->num_rows()>0)
			{
				$keep_loan_contract=array();
				$count_data_con2=0;
				foreach($query_get_loan_contract_no->result() as $data_k_l)
				{
					$b=1;
					for($i=0;$i<count($chk_l_contract);$i++)
					{
						if($chk_l_contract[$i]==$data_k_l->loan_contract_id)
						{
							$b=0;
						}
					}
					if($b==1)
					{
						$keep_loan_contract[$count_data_con2]=$data_k_l->loan_contract_id;
						$count_data_con2=$count_data_con2+1;
					}
				}
				for($i=0;$i<count($keep_loan_contract);$i++)
				{
					$this->db->where('loan_contract_id',$keep_loan_contract[$i]);
					$query_get_loan=$this->db->get('loan_contract');
					$get_loan=$query_get_loan->result();
					$this->db->where('loan_id',$get_loan[0]->loan_id);
					$query_get_person_id=$this->db->get('loan');
					$get_person_id=$query_get_person_id->result();
					$this->db->where('person_id',$get_person_id[0]->person_id);
					$query_get_person_detail=$this->db->get('person');
					$get_person_detail=$query_get_person_detail->result();
					$this->db->where('title_id',$get_person_detail[0]->title_id);
					$query_get_title_name=$this->db->get('config_title');
					$get_title_name=$query_get_title_name->result();
					$this->db->where('loan_contract_id',$keep_loan_contract[$i]);
					$query_get_loan_amount=$this->db->get('loan_balance');
					$get_loan_amount=$query_get_loan_amount->result();
					$chk_l_contract1[]=$keep_loan_contract[$i];
					$data_non_payment1=$data_non_payment1.$get_title_name[0]->title_name." ".$get_person_detail[0]->person_fname." ".$get_person_detail[0]->person_lname.",".$day_last.",".$day.",".$get_loan[0]->loan_contract_amount.",".$get_loan_amount[0]->loan_amount.","."3.00".",".$get_loan[0]->loan_contract_date.",".'0'.","."check_non_pay2"."\\";
				}
			}
			$check_loan_contract_more=array('loan_contract_no!='=>null,'loan_contract_date>='=>$day);
			$this->db->select('loan_contract_no,loan_contract_id');
			$this->db->where($check_loan_contract_more);
			$query_get_loan_contract_no_more=$this->db->get('loan_contract');
			if($query_get_loan_contract_no_more->num_rows()>0)
			{
				$keep_loan_contract1=array();
				$count_data_con2=0;
				foreach($query_get_loan_contract_no_more->result() as $data_k_l)
				{
					$b=1;
					for($i=0;$i<count($chk_l_contract1);$i++)
					{
						if($chk_l_contract1[$i]==$data_k_l->loan_contract_id)
						{
							$b=0;
						}
					}
					for($j=0;$j<count($chk_l_contract);$j++)
					{
						if($chk_l_contract[$i]==$data_k_l->loan_contract_id)
						{
							$b=0;
						}

					}
					if($b==1)
					{
						$keep_loan_contract1[$count_data_con2]=$data_k_l->loan_contract_id;
						$count_data_con2=$count_data_con2+1;
					}
				}
				for($i=0;$i<count($keep_loan_contract1);$i++)
				{
					$this->db->where('loan_contract_id',$keep_loan_contract[$i]);
					$query_get_loan=$this->db->get('loan_contract');
					$get_loan=$query_get_loan->result();
					$this->db->where('loan_id',$get_loan[0]->loan_id);
					$query_get_person_id=$this->db->get('loan');
					$get_person_id=$query_get_person_id->result();
					$this->db->where('person_id',$get_person_id[0]->person_id);
					$query_get_person_detail=$this->db->get('person');
					$get_person_detail=$query_get_person_detail->result();
					$this->db->where('title_id',$get_person_detail[0]->title_id);
					$query_get_title_name=$this->db->get('config_title');
					$get_title_name=$query_get_title_name->result();
					$this->db->where('loan_contract_id',$keep_loan_contract[$i]);
					$query_get_loan_amount=$this->db->get('loan_balance');
					$get_loan_amount=$query_get_loan_amount->result();
					$chk_l_contract[]=$keep_loan_contract[$i];
					$data_non_payment1=$data_non_payment1.$get_title_name[0]->title_name." ".$get_person_detail[0]->person_fname." ".$get_person_detail[0]->person_lname.",".$day_last.",".$get_loan[0]->loan_contract_date.",".$get_loan[0]->loan_contract_amount.",".$get_loan_amount[0]->loan_amount.","."3.00".",".$get_loan[0]->loan_contract_date.",".'0'.","."check_non_pay2"."\\";
				}
			}
			
			return $day_last."\\".$data_non_payment1;
			/*$check_array=array('date_payout<='=>$day);
			
			$this->db->where($check_array);
			$query_data_non_payment=$this->db->get('payment_schedule');
			
			if($query_data_non_payment->num_rows()>0)
			{
				foreach($query_data_non_payment->result() as $data_non_payment)
				{
					$check_null_loan_contract=array('loan_contract_id'=>$data_non_payment->loan_contract_id,'loan_contract_no!='=>null);
					$this->db->where($check_null_loan_contract);
					$query_check_loan_contract=$this->db->get('loan_contract');
					if($query_check_loan_contract->num_rows()>0)
					{
						$array_pay=$array_pay.$data_non_payment->loan_contract_id.",";	
					}
				
				}
				
				$datas=explode(",",$array_pay); //array ที่ต้องการตรวจสอบ
				$check=array(); //array สำหรับการตรวจสอบ
				for ($i=0; $i<count($datas); $i++) {
				
					$b=1;
					for($j=0;$j<$i;$j++)
					{
						if($datas[$i]==$datas[$j]) $b=0;
					
					}
					if($b==1)
					{
						$check[]=$datas[$i];
					}
				}
				
				$this->load->model('accured_interest_model', 'accured_interest_m');
			
				for ($i=0;$i<count($check)-1;$i++)
				{
					
					if($this->accured_interest_m->check_non_pay2($check[$i],$day,$day_last,$condition)!="none" and $this->accured_interest_m->check_non_pay2($check[$i],$day,$day_last,$condition)!=null)
					{
						$data_non_payment1=$data_non_payment1.$this->accured_interest_m->check_non_pay2($check[$i],$day,$day_last,$condition)."\\";
					}
					
				}
				
				
			}*/

		}
		
		$array_pay="";
		
		$this->db->order_by('date_payout','asc');
		if($condition==1) { $check_array=array('date_payout<='=>$day_last,'status'=>'D');}
		else { $check_array=array('date_payout>='=>$day,'date_payout<='=>$day_last,'status'=>'D') ;}
		$this->db->where($check_array);
		$query_data_non_payment=$this->db->get('payment_schedule');
		if($query_data_non_payment->num_rows()>0)
		{
			foreach($query_data_non_payment->result() as $data_non_payment)
			{
					$check_null_loan_contract=array('loan_contract_id'=>$data_non_payment->loan_contract_id,'loan_contract_no!='=>null);
					$this->db->where($check_null_loan_contract);
					$query_check_loan_contract=$this->db->get('loan_contract');
					if($query_check_loan_contract->num_rows()>0)
					{
						$array_pay=$array_pay.$data_non_payment->loan_contract_id.",";	
					}
				
			}

			$datas=explode(",",$array_pay); //array ที่ต้องการตรวจสอบ
			$check=array(); //array สำหรับการตรวจสอบ
			for ($i=0; $i<count($datas); $i++) {
				
				$b=1;
				for($j=0;$j<$i;$j++)
				{
					if($datas[$i]==$datas[$j]) $b=0;
					
				}
				if($b==1)
				{
					$check[]=$datas[$i];
				}
			}
			$this->load->model('accured_interest_model', 'accured_interest_m');
			
			for ($i=0;$i<count($check)-1;$i++)
			{
				if($this->accured_interest_m->check_pay1($check[$i],$day,$day_last,$condition)!=null)
				{
					$data_non_payment1=$data_non_payment1.$this->accured_interest_m->check_pay1($check[$i],$day,$day_last,$condition)."\\";
				}
			}
	
		}
		
		if($data_non_payment1=="")
		{
			return $day_last."\\"."no data";
		}
		else
		{
			if($condition==2)
			{
				$check_l_data=explode("\\",$data_non_payment1);
				$check_last_data=array();
				for ($i=count($check_l_data)-2; $i>=0; $i--) {
					$check_l1_data=explode(",",$check_l_data[$i]);
					if($i==count($check_l_data)-2)
					{
						$check_last_data[]=$check_l_data[$i];
					}
					else
					{
						$b=1;
						for($j=0;$j<count($check_last_data);$j++)
						{
							$check_l2_data=explode(",",$check_last_data[$j]);
							if($check_l1_data[8]==$check_l2_data[8]) $b=0;
						}
						if($b==1)
						{
					
							$check_last_data[]=$check_l_data[$i];
						}
					}
				}
				
				$d_convert_data="";
				for($k=0;$k<count($check_last_data);$k++)
				{
					$d_convert_data=$d_convert_data.$check_last_data[$k]."\\";
				}
				return $day_last."\\".$d_convert_data;
			}
			else if($condition==1)
			{
				return $day_last."\\".$data_non_payment1;
			}
			
		}


	}
	function check_non_pay2($data,$day,$day_last,$condition)
{
	$data_non_payment1="";
	$date_pay="";
	$interest_rate="";
		$this->db->order_by('date_payout','asc');
		$this->db->limit(1);
	if($condition==1)
	{
			$check_array=array('date_payout<='=>$day_last,'status'=>'N','loan_contract_id'=>$data);
	}
	else
	{
		$check_array=array('date_payout>='=>$day,'date_payout<='=>$day_last,'status'=>'N','loan_contract_id'=>$data);
	}
				$this->db->where($check_array);
				$query_data_non_payment=$this->db->get('payment_schedule');
				$data_non_payment=$query_data_non_payment->result();
				
				
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_id=$this->db->get('loan_contract');
				$data_loan_id=$query_data_loan_id->result();
				if($data_loan_id[0]->loan_contract_date!=null)
				{
					$this->db->where('loan_contract_id',$data);
					$query_data_loan_balance=$this->db->get('loan_balance');
					$data_loan_balance=$query_data_loan_balance->result();
					$this->db->where('loan_id',$data_loan_id[0]->loan_id);
					$query_data_person_id=$this->db->get('loan');
					$data_person_id=$query_data_person_id->result();
					$this->db->where('person_id',$data_person_id[0]->person_id);
					$query_data_detail_person=$this->db->get('person');
					$data_detail_person=$query_data_detail_person->result();
					$this->db->where('title_id',$data_detail_person[0]->title_id);
					$query_data_title=$this->db->get('config_title');
				
					$data_title=$query_data_title->result();
				
					if($condition==1)
					{
					
						$query_get_last_payment=$this->db->query("select * from payment_schedule where (status='P' or status='D') AND loan_contract_id=$data order by loan_pay_number desc limit 1");
						if ($query_get_last_payment->num_rows()>0)
						{
							$get_last_payment=$query_get_last_payment->result();
							$check_array=array('loan_contract_id'=>$data,'loan_pay_number'=>$get_last_payment[0]->loan_pay_number);
							$this->db->where($check_array);
							$this->db->limit(1);
							$query_get_invoice=$this->db->get('loan_invoice');
							$get_invoice=$query_get_invoice->result();
							$this->db->where('invoice_no',$get_invoice[0]->invoice_no);
							$this->db->order_by('date_transfer','DESC');
							$this->db->limit(1);
							$query_get_last_date=$this->db->get('loan_payment');
							$get_last_date=$query_get_last_date->result();
							$date_pay=$get_last_date[0]->date_transfer;
						}
						else
						{
							$date_pay=$data_loan_id[0]->loan_contract_date;
						}
						$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$day.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.","."3.00".",".$data_loan_id[0]->loan_contract_date.",".'0'.","."check_non_pay2";
					}
					else
					{
					//$date_pay=$day;
						$query_get_last_payment=$this->db->query("select * from payment_schedule where (status='P' or status='D') AND loan_contract_id=$data order by loan_pay_number desc limit 1");
						if ($query_get_last_payment->num_rows()>0)
						{
							$get_last_payment=$query_get_last_payment->result();
							$check_array=array('loan_contract_id'=>$data,'loan_pay_number'=>$get_last_payment[0]->loan_pay_number);
							$this->db->where($check_array);
							$this->db->limit(1);
							$query_get_invoice=$this->db->get('loan_invoice');
							$get_invoice=$query_get_invoice->result();
							$this->db->where('invoice_no',$get_invoice[0]->invoice_no);
							$this->db->order_by('date_transfer','DESC');
							$this->db->limit(1);
							$query_get_last_date=$this->db->get('loan_payment');
							$get_last_date=$query_get_last_date->result();
							$arrDate1 = explode("-",$day);
							$arrDate2 = explode("-",$get_last_date[0]->date_transfer);
							$timStmp1 = mktime(0,0,0,$arrDate1[1],$arrDate1[2],$arrDate1[0]);
							$timStmp2 = mktime(0,0,0,$arrDate2[1],$arrDate2[2],$arrDate2[0]);
							if ($timStmp1 == $timStmp2) {
								$date_pay=$day;
								$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$day.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.","."3.00".",".$data_loan_id[0]->loan_contract_date.",".'0'.",".$data.","."NonPay1";
							} else if ($timStmp1 > $timStmp2) {
								$date_pay=$day;
								$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$day.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.","."3.00".",".$data_loan_id[0]->loan_contract_date.",".'0'.",".$data.","."NonPay2";
							} else if ($timStmp1 < $timStmp2) {
								//$date_pay=$get_last_date[0]->date_transfer;
								$arrDate1 = explode("-",$day_last);
								$arrDate2 = explode("-",$get_last_date[0]->date_transfer);
								$timStmp1 = mktime(0,0,0,$arrDate1[1],$arrDate1[2],$arrDate1[0]);
								$timStmp2 = mktime(0,0,0,$arrDate2[1],$arrDate2[2],$arrDate2[0]);
								if ($timStmp1 > $timStmp2) {
									$date_pay=$get_last_date[0]->date_transfer;
									$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$day.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.","."3.00".",".$data_loan_id[0]->loan_contract_date.",".'0'.",".$data.","."check_non_pay22";
								}
								else
								{
									return "none";
								}
							}
						
						}
						else
						{
							$date_pay=$day;
							$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$day.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.","."3.00".",".$data_loan_id[0]->loan_contract_date.",".'0'.",".$data.","."check_non_pay23";
						}
				
					}
					return $data_non_payment1;
					}	
					else
					{	
						return ;
					}
				//----------------------------
		
}
function check_non_pay1($data,$day,$day_last,$condition)
{
	$data_non_payment1="";
	$date_pay="";
	$interest_rate="";
		$this->db->order_by('date_payout','asc');
		$this->db->limit(1);
	if($condition==1)
	{
			$check_array=array('date_payout<='=>$day_last,'status'=>'N','loan_contract_id'=>$data);
	}
	else
	{
		$check_array=array('date_payout>='=>$day,'date_payout<='=>$day_last,'status'=>'N','loan_contract_id'=>$data);
	}
				$this->db->where($check_array);
				$query_data_non_payment=$this->db->get('payment_schedule');
				$data_non_payment=$query_data_non_payment->result();
				
				
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_id=$this->db->get('loan_contract');
				$data_loan_id=$query_data_loan_id->result();
				if($data_loan_id[0]->loan_contract_date!=null){
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_balance=$this->db->get('loan_balance');
				$data_loan_balance=$query_data_loan_balance->result();
				$this->db->where('loan_id',$data_loan_id[0]->loan_id);
				$query_data_person_id=$this->db->get('loan');
				$data_person_id=$query_data_person_id->result();
				$this->db->where('person_id',$data_person_id[0]->person_id);
				$query_data_detail_person=$this->db->get('person');
				$data_detail_person=$query_data_detail_person->result();
				$this->db->where('title_id',$data_detail_person[0]->title_id);
				$query_data_title=$this->db->get('config_title');
				
				$data_title=$query_data_title->result();
				
				if($condition==1)
				{
					
					$query_get_last_payment=$this->db->query("select * from payment_schedule where (status='P' or status='D') AND loan_contract_id=$data order by loan_pay_number desc limit 1");
					if ($query_get_last_payment->num_rows()>0)
					{
						$get_last_payment=$query_get_last_payment->result();
						$check_array=array('loan_contract_id'=>$data,'loan_pay_number'=>$get_last_payment[0]->loan_pay_number);
						$this->db->where($check_array);
						$this->db->limit(1);
						$query_get_invoice=$this->db->get('loan_invoice');
						$get_invoice=$query_get_invoice->result();
						$this->db->where('invoice_no',$get_invoice[0]->invoice_no);
						$this->db->order_by('date_transfer','DESC');
						$this->db->limit(1);
						$query_get_last_date=$this->db->get('loan_payment');
						$get_last_date=$query_get_last_date->result();
						$date_pay=$get_last_date[0]->date_transfer;
					}
					else
					{
						$date_pay=$data_loan_id[0]->loan_contract_date;
					}
				}
				else
				{
					$date_pay=$day;
				}
				$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$data_non_payment[0]->date_payout.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_non_payment[0]->interest_rate.",".$data_loan_id[0]->loan_contract_date.",".'0'.",".$data.","."check_non_pay1";
				//----------------------------
		return $data_non_payment1;
				}
				else
				{
					return ;
				}
}
function check_pay1($data,$day,$day_last,$condition)
{
	$data_non_payment1="";
	$date_pay="";
	$interest_rate="";
	$this->db->order_by('date_payout','asc');
	$this->db->limit(1);
	if($condition==1)
	{
			$check_array=array('date_payout<='=>$day_last,'status'=>'D','loan_contract_id'=>$data);
	}
	else
	{
		$check_array=array('date_payout>='=>$day,'date_payout<='=>$day_last,'status'=>'D','loan_contract_id'=>$data);
	}
				$this->db->where($check_array);
				$query_data_non_payment=$this->db->get('payment_schedule');
				$data_non_payment=$query_data_non_payment->result();
				if($data_non_payment[0]->interest_accrued==null)
				{
					$inter_acc=0;
				}
				else
				{
					$inter_acc=$data_non_payment[0]->interest_accrued;
				}
					$check_array=array('loan_contract_id'=>$data,'loan_pay_number'=>$data_non_payment[0]->loan_pay_number);
					$this->db->where($check_array);
					$this->db->limit(1);
					$query_get_invoice=$this->db->get('loan_invoice');
					$get_invoice=$query_get_invoice->result();
					$this->db->where('invoice_no',$get_invoice[0]->invoice_no);
					$this->db->order_by('date_transfer','DESC');
					$this->db->limit(1);
					$query_get_last_date=$this->db->get('loan_payment');
					$get_last_date=$query_get_last_date->result();
					$date_pay=$get_last_date[0]->date_transfer;
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_id=$this->db->get('loan_contract');
				$data_loan_id=$query_data_loan_id->result();
				$this->db->where('loan_contract_id',$data);
				$query_data_loan_balance=$this->db->get('loan_balance');
				$data_loan_balance=$query_data_loan_balance->result();
				$this->db->where('loan_id',$data_loan_id[0]->loan_id);
				$query_data_person_id=$this->db->get('loan');
				$data_person_id=$query_data_person_id->result();
				$this->db->where('person_id',$data_person_id[0]->person_id);
				$query_data_detail_person=$this->db->get('person');
				$data_detail_person=$query_data_detail_person->result();
				$this->db->where('title_id',$data_detail_person[0]->title_id);
				$query_data_title=$this->db->get('config_title');
				$data_title=$query_data_title->result();
				if($condition==2)
				{
					if($date_pay<$day)
					{
						$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$data_non_payment[0]->date_payout.",".$day.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_non_payment[0]->interest_rate.",".$data_loan_id[0]->loan_contract_date.",".$inter_acc.",".$data.","."check_pay1";
					}
					else
					{
						$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$data_non_payment[0]->date_payout.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_non_payment[0]->interest_rate.",".$data_loan_id[0]->loan_contract_date.",".$inter_acc.",".$data.","."check_pay1";
					}
				}
				else
				{
					$data_non_payment1=$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$data_non_payment[0]->date_payout.",".$date_pay.",".$data_loan_id[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_non_payment[0]->interest_rate.",".$data_loan_id[0]->loan_contract_date.",".$inter_acc.",".$data.","."check_pay1";
				}
				//----------------------------
		return $data_non_payment1;
}
}

?>