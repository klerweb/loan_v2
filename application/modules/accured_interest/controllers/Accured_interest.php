<?php
class accured_interest extends MY_Controller{
	private $title_page = 'ดอกเบี้ยค้างรับลูกหนี้สินเชื่อ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('accured_interest_model', 'accured_interest_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงานทางการเงิน' =>'#');
		$date=explode("-",date("Y-m-d"));
		$data_content = array(
				'date_search'=>$date[2]."/".$date[1]."/".$date[0],
				'show_accured_interest' => $this->accured_interest_m->load_show_accured(date("Y-m-d")),
				'condition_check'=>1
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_finance_disburse/form.js','modules_finance_disburse/jquery.table2excel.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('show_accured_interest', $data_content, TRUE)
		);
		$this->parser->parse('main', $data);
	}
	public function search_accured_interest()
	{
		$this->load->model('accured_interest_model', 'accured_interest_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงานทางการเงิน' =>'#');
		if($_POST['ddlStatus']==0 or $_POST['ddlStatus']==2 or $_POST['ddlStatus']==4 or $_POST['ddlStatus']==6 or $_POST['ddlStatus']==7 or $_POST['ddlStatus']==9 or $_POST['ddlStatus']==11)
		{
			$date=31;
			$month=intval($_POST['ddlStatus'])+1;
		}
		else if($_POST['ddlStatus']==3 or $_POST['ddlStatus']==5 or $_POST['ddlStatus']==8 or $_POST['ddlStatus']==10)
		{
			$date=30;
			$month=intval($_POST['ddlStatus'])+1;
		}
		else
		{
			$month=intval($_POST['ddlStatus'])+1;
			if($_POST['ddlTitle']%4==0)
			{
				$date=29;
			}
			else
			{
				$date=28;
			}
		}
		
		$data_content = array(
				'date_search'=>$date."/".$month."/".$_POST['ddlTitle'],
				'show_accured_interest' => $this->accured_interest_m->search_show_accured($_POST['ddlTitle']."-".$month."-"."01",$_POST['ddlTitle']."-".$month."-".$date,$_POST['ddlObjective']),
			'condition_check'=>$_POST['ddlObjective']
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_finance_disburse/form.js','modules_finance_disburse/jquery.table2excel.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('show_accured_interest', $data_content, TRUE)
		);
		$this->parser->parse('main', $data);
	}
}
?>