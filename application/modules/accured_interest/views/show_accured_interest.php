
<script type="text/javascript">
$(document).ready(function(){
	
});
function exportToExcel(tableID){
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6' style='height: 75px; text-align: center; width: 250px'>";
    var textRange; var j=0;
    tab = document.getElementById(tableID); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {

        tab_text=tab_text;

        tab_text=tab_text+tab.rows[j].innerHTML.toUpperCase()+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text= tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); //remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer

    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write( 'sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"sudhir123.txt");
    }

    else {
       sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
    
    return (sa);
}
function fncSubmit()
{
	if ($("#ddlStatus").val()==-1)
	 {
		 alert("กรุณาเลือกเดือนที่ต้องการ");
		 return false;
	 }
	 else if ($("#ddlTitle").val()==0)
	 {
		 alert("กรุณาเลือกปีที่ต้องการ");
		 return false;
	 }
	 else if ($("#ddlObjective").val()==0)
	 {
		 alert("กรุณาเลือกเงื่อนไข");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}

</script>
<?php

function cal_date($fdate,$ldate)
{
	//-------------------cal date
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);                //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$u_y=date("Y", $mage)-1970;
	$u_m=date("m",$mage)-1;
	$u_d=date("d",$mage)-1;
	$show_date="";
	if($u_y==0){$u_y="";} else {$u_y=$u_y." ปี";}
	if($u_m==0){$u_m="";} else {$u_m=$u_m." เดือน";}
	if($u_d==0){$u_d="";} else {$u_d=$u_d." วัน";}
 return "  $u_y $u_m $u_d";
 
//---------------end caldate	
}
function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return round($result);
}

function cal_real_date($fdate,$ldate)
{
	$first_date=explode("-",$fdate);
	$last_date=explode("-",$ldate);
	$different=$last_date[0]-$first_date[0];
	$count_date="";
	
	for($i=0;$i<=$different;$i++)
	{
		if($i==0)
		{
			$ldate1=$first_date[0]+$i;
			
				if($ldate1>2036)
				{
					if($ldate1%4==0)
					{
						if($different==0)
						{
							$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2016"."-"."12"."-"."31";
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}					
					}
					else
					{
						if($different==0)
						{
							$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2017"."-"."12"."-"."31";
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}					
					}
					
				}
				else
				{
					if($different==0)
					{
						$count_date=$count_date+cal_date1($fdate,$ldate);
					}
					else
					{
						$ldate2=$first_date[0]."-"."12"."-"."31";
						$ldate3=$first_date[0]."-".$first_date[1]."-".$first_date[2];
						$count_date=$count_date+cal_date1($ldate3,$ldate2);
					}
				}	
		}
		else if($i>0 && $i<$different)
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".'12'."-".'31';
					$ldate3="2016"."-".'01'."-".'01';
					
				}
				else
				{
					$ldate2="2017"."-".'12'."-".'31';
					$ldate3="2017"."-".'01'."-".'01';
					
				}
			}
			else
			{
				$ldate2=$ldate1."-".'12'."-".'31';
				$ldate3=$ldate1."-".'01'."-".'01';

			}
			$count_date=$count_date+cal_date1($ldate3,$ldate2);
			
		}
		else if ($i==$different )
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2016"."-".'01'."-".'01';
					$count_date=$count_date+cal_date1($ldate3,$ldate2);	
				}
				else
				{
					$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2017"."-".'01'."-".'01';
					$count_date=$count_date+cal_date1($ldate3,$ldate2);	
				}
			}
			else
			{
				$ldate3=$ldate1."-".'01'."-".'01';
				$count_date=$count_date+cal_date1($ldate3,$ldate);	
			}
		}
	}
	return $count_date+$different;
	
}
$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
$month1 = array("ม.ค.", "ก.พ.", "มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('accured_interest/search_accured_interest'); ?>" onSubmit="JavaScript:return fncSubmit();">
					<div class="form-group">
						<label class="control-label"></label>
								<div class="input-group">
									<select name="ddlStatus" id="ddlStatus" data-placeholder="เดือน" class="width100p" style="width: 100px" required>
									<option value=-1>เดือน</option>
									<?php
									for ($array_month=0;$array_month<count($month);$array_month++)
										{ 
										?><option value=<?php echo $array_month; if($array_month+1==date("m")){ echo ' selected="selected"';}?>>  <?php echo $month[$array_month]; ?></option>
							<?php }
							?>
							</select> 
								</div>
					</div><!-- form-group -->
					<div class="form-group">
						<label class="control-label"></label>
								<div class="input-group">
									<select name="ddlTitle" id="ddlTitle" data-placeholder="ปี" class="width100p" style="width: 100px" required>
									<option value=0>ปี</option>
									<?php
									for ($array_year=date("Y")-1;$array_year<date("Y")+10;$array_year++)
										{ 
										?><option value=<?php echo $array_year; if($array_year==date("Y")){ echo ' selected="selected"';}?>>  <?php echo intval($array_year)+543; ?></option>
							<?php }
							?>
							</select> 
								</div>
					</div><!-- form-group -->
					<div class="form-group">
						<label class="control-label"></label>
								<div class="input-group">
									<select name="ddlObjective" id="ddlObjective" data-placeholder="เงื่อนไข" class="width100p" style="width: 150px" required>
									<option value=0>เงื่อนไข</option>
									<option value=1> แบบสะสม</option>
									<option value=2> แบบรายเดือน</option>
							
							</select> 
								</div>
					</div><!-- form-group -->
					
					<button type="submit" class="btn btn-primary mr5">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<?php 

$split_data=explode("\\",$show_accured_interest); ?>
<?php //--------------------------------?>
<center><table width="100%" height="100%" border="0" id="show_card_debtor" name="show_card_debtor">
  <tr>
    <td colspan="17"><center>รายละเอียดดอกเบี้ยค้างรับลูกหนี้สินเชื่อ <?php if($condition_check==1) { echo "(แบบสะสม)";} 
	else
	{
		echo "(รายเดือน)";
	}
	?></center></td>
  </tr>
  <tr>
    <td colspan="17"><center><?php
	$split_date=explode("-",$split_data[0]);
	echo "ณ วันที่ ".intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	?>
	</center></td>
  </tr>
  
  <tr>
  <td colspan="17"><br></td>
  </tr>
  <?php
  if($split_data[1]!="no data"){
$total=0;
?>
  <tr><td colspan="17"><center><table width="100%" height="100%" border="1" id="show_c_debtor" name="show_c_debtor">
  <tr>
    <td colspan="1" style="font-sizne: 10px; text-align:center;">ลำดับที่</td>
    <td colspan="3" style="font-sizne: 10px; text-align:center;">ลูกหนี้</td>
	<td colspan="2" style="font-sizne: 10px; text-align:center;">วันที่เริ่มสัญญา</td>
    
	<td colspan="3" style="font-sizne: 10px; text-align:center;">เงินต้น</td>
	<td colspan="1" style="font-sizne: 10px; text-align:center;">อัตราดอกเบี้ย</td>
	<td colspan="2" style="font-sizne: 10px; text-align:center;">วันที่เริ่มคิดดอกเบี้ย</td>
	<td colspan="2" style="font-sizne: 10px; text-align:center;">วันที่สิ้นสุดการคิดดอกเบี้ย</td>
	<td colspan="1" style="font-sizne: 10px; text-align:center;">จำนวน(วัน)</td>
	<td colspan="2" style="font-sizne: 10px; text-align:center;">ดอกเบี้ย (บาท)</td>
	</tr>
	<?php 
	for($j=1;$j<count($split_data)-1;$j++)
	{
	$split_data2=explode(",",$split_data[$j]);
	
	?>
	<tr>
	<td colspan="1" style="font-sizne: 10px; text-align:center;"><?php echo $j;?></td>
    <td colspan="3" style="font-sizne: 10px; text-align:left;"><?php echo $split_data2[0];?></td>
   <td colspan="2" style="font-sizne: 10px; text-align:center;"><?php 
	$split_date=explode("-",$split_data2[6]);
	echo intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	?></td>
	<td colspan="3" style="font-sizne: 10px; text-align:right;"><?php echo number_format($split_data2[4],2);?></td>
	<td colspan="1" style="font-sizne: 10px; text-align:right;"><?php echo $split_data2[5]."%";?></td>
	 <td colspan="2" style="font-sizne: 10px; text-align:center;"><?php 
	if($condition_check==1) { $split_date=explode("-",$split_data2[2]);
	echo intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	}
	else { 
		$split_date_f=explode("-",$split_data2[2]); echo intval($split_date_f[2])." ".$month[intval($split_date_f[1])-1]." พ.ศ. ".(intval($split_date_f[0])+543);
		}
	

	?></td>
	
	<td colspan="2" style="font-sizne: 10px; text-align:center;"><?php 
	$split_date=explode("-",$split_data[0]);
	echo intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	?></td>
	<td colspan="1" style="font-sizne: 10px; text-align:center;"><?php 
	 //$count_date=cal_real_date($split_data2[2],$split_data[0]);
	if($condition_check==1) { $split_date=explode("-",$split_data2[2]);
		$count_date=cal_real_date($split_data2[2],$split_data[0])+1;
	}
	else { 
		$split_date_f=explode("-",$split_data2[2]); 
			$count_date=cal_real_date($split_date_f[0]."-".$split_date_f[1]."-".$split_date_f[2],$split_data[0])+1;
		}
		echo $count_date;
	?></td>
	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php
	
	/*if($condition_check==1) { $split_date=explode("-",$split_data2[2]);
		$count_date=cal_real_date($split_data2[2],$split_data[0])+1;
	}
	else { 
		$split_date_f=explode("-",$split_data[0]); 
			$count_date=cal_real_date($split_date_f[0]."-".$split_date_f[1]."-1",$split_data[0])+1;
		}*/
	$interest=$split_data2[4]*($split_data2[5]/36500)*$count_date;
	$ex = explode('.',$interest);
			if(count($ex)==2) {$s = substr($ex[1],0,2);
			$interest=$ex[0].".".$s;}
			$total=$total+$interest+$split_data2[7];
	echo number_format($interest+$split_data2[7],2);
	?>
</td>
	</tr>
	<?php } 
if($split_data[1]!="no data"){
	?>
	<tr>
	<td colspan="15" style="font-sizne: 10px; text-align:center;">รวม</td>
	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total,2); ?></td>
	</tr>
	<?php
}
	?>
 </table> </td></tr>
  </tr>
</table>
</center>
<br />
<center><input name="btn_to_excel1" type="button" id="btn_to_excel1" value="ส่งออกรายละเอียดดอกเบี้ยค้างรับลูกหนี้สินเชื่อ"  onclick="exportToExcel('show_card_debtor')" /></input></center>
<br>
<?php } ?>