<script type="text/javascript">
$(document).ready(function(){
	
});
function exportToExcel(tableID){
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6' style='height: 75px; text-align: center; width: 250px'>";
    var textRange; var j=0;
    tab = document.getElementById(tableID); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {

        tab_text=tab_text;

        tab_text=tab_text+tab.rows[j].innerHTML.toUpperCase()+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text= tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); //remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer

    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write( 'sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"sudhir123.txt");
    }

    else {
       sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
    
    return (sa);
}
function fncSubmit()
{
	if ($("#txtDate").val()=="")
	 {
		 alert("กรุณาเลือกวันที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}

</script>
<?php

function cal_date($fdate,$ldate)
{
	//-------------------cal date
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);                //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$u_y=date("Y", $mage)-1970;
	$u_m=date("m",$mage)-1;
	$u_d=date("d",$mage)-1;
	$show_date="";
	if($u_y==0){$u_y="";} else {$u_y=$u_y." ปี";}
	if($u_m==0){$u_m="";} else {$u_m=$u_m." เดือน";}
	if($u_d==0){$u_d="";} else {$u_d=$u_d." วัน";}
 return "  $u_y $u_m $u_d";
 
//---------------end caldate	
}
function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return floor($result);
	
	
	
}
$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
$month1 = array("ม.ค.", "ก.พ.", "มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('accured_interest/search_accured_interest'); ?>" onSubmit="JavaScript:return fncSubmit();">
					<div class="form-group">
						<label class="control-label"><span class="asterisk"></span></label>
								<div class="input-group">
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo $date_search;?>" readonly="readonly"/>
									<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
					</div><!-- form-group -->
					
					<button type="submit" class="btn btn-primary mr5">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<?php 

$split_data=explode("\\",$show_accured_interest); ?>
<?php //--------------------------------?>
<center><table width="100%" height="100%" border="0" id="show_card_debtor" name="show_card_debtor">
  <tr>
    <td colspan="17"><center>รายละเอียดดอกเบี้ยค้างรับลูกหนี้สินเชื่อ</center></td>
  </tr>
  <tr>
    <td colspan="17"><center><?php
	$split_date=explode("-",$split_data[0]);
	echo "ณ วันที่ ".intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0]));
	?>
	</center></td>
  </tr>
  
  <tr>
  <td colspan="17"><br></td>
  </tr>
  <?php
  if($split_data[1]!="no data"){
$total=0;
?>
  <tr><td colspan="17"><center><table width="100%" height="100%" border="1" id="show_c_debtor" name="show_c_debtor">
  <tr>
    <td colspan="1"><center>ลำดับที่</center></td>
    <td colspan="3"><center>ลูกหนี้</center></td>
    <td colspan="2"><center>ชำระวันที่</center></td>
	<td colspan="3"><center>เงินต้น</center></td>
	<td colspan="1"><center>อัตราดอกเบี้ย</center></td>
	<td colspan="2"><center>วันที่เริ่ม</center></td>
	<td colspan="2"><center>วันที่สิ้นสุด</center></td>
	<td colspan="1"><center>จำนวน(วัน)</center></td>
	<td colspan="2"><center>ดอกเบี้ย (บาท)</center></td>
	</tr>
	<?php 
	for($j=1;$j<count($split_data)-1;$j++)
	{
	$split_data2=explode(",",$split_data[$j]);
	?>
	<tr>
	<td colspan="1"><p align="center"><?php echo $j;?></p></td>
    <td colspan="3"><p align="center"><?php echo $split_data2[0];?></center></td>
    <td colspan="2"><p align="center"><?php 
	$split_date=explode("-",$split_data2[1]);
	echo intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	?></p></td>
	<td colspan="3"><p align="right"><?php echo number_format($split_data2[4],2);?></p></td>
	<td colspan="1"><p align="right"><?php echo $split_data2[5]."%";?></p></td>
	<td colspan="2"><p align="center"><?php 
	$split_date=explode("-",$split_data2[2]);
	echo intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	?></p></td>
	<td colspan="2"><p align="center"><?php 
	$split_date=explode("-",$split_data[0]);
	echo intval($split_date[2])." ".$month[intval($split_date[1])-1]." พ.ศ. ".(intval($split_date[0])+543);
	?></p></td>
	<td colspan="1"><p align="center"><?php echo cal_date1($split_data2[2],$split_data[0]);
	?></p></td>
	<td colspan="2"><p align="right"><?php
	$count_date=cal_date1($split_data2[2],$split_data[0]);
	
	$interest=$split_data2[4]*($split_data2[5]/36500)*$count_date;
	$total=$total+round($interest,2);
	echo number_format(round($interest,2),2);
	?></p>
</td>
	</tr>
	<?php } 
if($split_data[1]!="no data"){
	?>
	<tr>
	<td colspan="15"><p align="center">รวม</p></td>
	<td colspan="2"><p align="right"><?php echo number_format($total,2); ?></p></td>
	</tr>
	<?php
}
	?>
 </table> </td></tr>
  </tr>
</table>
</center>
<br />
<center><input name="btn_to_excel1" type="button" id="btn_to_excel1" value="ส่งออกรายละเอียดดอกเบี้ยค้างรับลูกหนี้สินเชื่อ"  onclick="exportToExcel('show_card_debtor')" /></input></center>
<br>
<?php } ?>