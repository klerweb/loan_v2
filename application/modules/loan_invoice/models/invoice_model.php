<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class invoice_model extends CI_Model {

    //Table Model
    private $table_loan = 'loan';
    private $table_contract = 'loan_contract';
    private $table_invoice = 'loan_invoice';
    private $table_person = 'person';
    private $table_balance = 'loan_balance';
    private $table_payment = 'payment_schedule';
    private $table_loan_payment = 'loan_payment';
    

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }
    
    function query($loan_id=''){
    	$sql = "SELECT ps.date_payout, 
  ps.loan_pay_number, 
  ps.money_pay, 
  ps.status, 
  ps.isinvgenerate, 
  ps.interest_rate, 
  ps.loan_contract_id, 
  ps.loan_id, 
  ps.payment_interest, 
  lp.principle, 
  lp.loan_amount_balance, 
  lp.interest_accrued, 
  la.loan_analysis_length, 
  lb.loan_amount, 
  ps.money_pay - COALESCE(lp.principle, 0.00::double precision) AS pay_amount, 
   CASE 
    WHEN ps.status = 'D'::bpchar THEN lp.interest 
    ELSE ps.payment_interest 
   END AS interest, 
  ps.money_pay - COALESCE(lp.principle, 0.00::double precision) + 
   CASE 
    WHEN ps.status = 'D'::bpchar THEN lp.interest_accrued 
    ELSE ps.payment_interest 
   END AS pay, 
  inv.printed, 
        (ps.date_payout  + INTERVAL '-30 day')::date as  setdisplay, 
        EXTRACT(YEAR from (ps.date_payout) )::integer as setdisplay_year 
    FROM payment_schedule ps 
   LEFT JOIN loan_invoice inv ON ps.loan_contract_id = inv.loan_contract_id AND ps.loan_pay_number = inv.loan_pay_number 
   LEFT JOIN ( SELECT mpay.invoice_no, 
    mpay.com_code, 
    mpay.money_transfer, 
    mpay.date_transfer, 
    mpay.createdby, 
    mpay.createddate, 
    mpay.updateby, 
    mpay.updatedate, 
    mpay.payment_id, 
    mpay.payment_no, 
    mpay.principle, 
    mpay.interest, 
    mpay.loan_amount_balance, 
    mpay.interest_accrued 
      FROM loan_payment mpay 
     WHERE mpay.payment_id = (( SELECT max(spay.payment_id) AS max 
        FROM loan_payment spay 
       WHERE mpay.invoice_no::text = spay.invoice_no::text))) lp ON inv.invoice_no::text = lp.invoice_no::text 
   JOIN loan_analysis la ON la.loan_id = ps.loan_id 
   JOIN loan_balance lb ON ps.loan_contract_id = lb.loan_contract_id 
         where ps.loan_id = ".$loan_id;
    	return $sql;
    	
    }

    function getby_loanid( $loan_id = -1,$year = '') {
                
        /*$this->db->select('*');
        $this->db->from("select * from (".$this->query($loan_id).") as tb_invoice)");
        $this->db->join($this->table_contract, "{$this->table_contract}.loan_contract_id=tb_invoice.loan_contract_id", 'inner');
        $this->db->join($this->table_loan, "{$this->table_contract}.loan_id={$this->table_loan}.loan_id", 'inner');
        $this->db->join($this->table_person, "{$this->table_loan}.person_id={$this->table_person}.person_id", 'inner');
        
        if($year != ''){
            //$year = date('Y');
            $this->db->where("tb_invoice.setdisplay_year",$year);
            $this->db->or_where("(tb_invoice.setdisplay_year < {$year} and tb_invoice.status in ('D','N'))");
        //$this->db->where_in("",array('D','N'));
        }
        
        
        $this->db->order_by('loan_pay_number');
        $result = $this->db->get();*/

        $sql = "select * from (".$this->query($loan_id).") as tb_invoice  
        INNER JOIN loan_contract ON loan_contract.loan_contract_id=tb_invoice.loan_contract_id 
        INNER JOIN loan ON loan_contract.loan_id =loan.loan_id 
        INNER JOIN person ON loan.person_id=person.person_id ORDER BY loan_pay_number";
         $result = $this->db->query($sql);
        //print($this->db->last_query());
        return $result->result_array();
    }
    function get_distinct_year($loan_id = -1) {
    	//$this->db->select('*');
        //$this->db->from("(select distinct setdisplay_year as year_en,(setdisplay_year+543) year_th from FN_InvoiceByloanId(" . $loan_id . ")) as tb_invoice");
        
        //$this->db->order_by('1');
        //$result = $this->db->get();
        $sql = "(select distinct setdisplay_year as year_en,(setdisplay_year+543) year_th from (".$this->query($loan_id).")  as tb_invoice) order by year_th";
        $result = $this->db->query($sql);
        return $result->result_array();
    }
    

    function get_by_contract_id($contract_id) {
        $this->db->select('*');
        $this->db->from($this->table_invoice);
        $this->db->join($this->table_contract, "{$this->table_contract}.loan_contract_id={$this->table_invoice}.loan_contract_id", 'inner');
        $this->db->join($this->table_loan, "{$this->table_contract}.loan_id={$this->table_loan}.loan_id", 'inner');
        $this->db->join($this->table_person, "{$this->table_loan}.person_id={$this->table_person}.person_id", 'inner');
        $this->db->order_by('loan_pay_number');

        if (!empty($contract_id)) {
            $this->db->where("{$this->table_contract}.loan_contract_id", $contract_id);
        }

        $result = $this->db->get();
        //print($this->db->last_query());
        return $result->result_array();
    }

    function get_payment($contract_id) {
        $this->db->select("*");
        $this->db->from($this->table_balance);
        $this->db->where("loan_contract_id", $contract_id);
        $result = $this->db->get();
        return $result->row_array();
    }

    function get_distinct_contract_no($contract_no, $person_card_id, $person_name) {
        $this->db->select("distinct on ({$this->table_contract}.loan_contract_id) *");
        $this->db->from($this->table_invoice);
        $this->db->join($this->table_contract, "{$this->table_contract}.loan_contract_id={$this->table_invoice}.loan_contract_id", 'inner');
        $this->db->join($this->table_loan, "{$this->table_contract}.loan_id={$this->table_loan}.loan_id", 'inner');
        $this->db->join($this->table_person, "{$this->table_loan}.person_id={$this->table_person}.person_id", 'inner');
        $this->db->join("{$this->table_payment}","{$this->table_payment}.loan_id={$this->table_contract}.loan_id"); 
        $this->db->where_in("{$this->table_payment}.status",array('D','N'));
        if (!empty($contract_no)) {
            $this->db->where("{$this->table_contract}.loan_contract_no", $contract_no);
        }
        if (!empty($person_card_id)) {
            $this->db->where("{$this->table_person}.person_thaiid", $person_card_id);
        }
        if (!empty($person_name)) {            
            $this->db->like("{$this->table_person}.person_fname", trim($person_name),'both');
			$this->db->or_like("{$this->table_person}.person_lname", trim($person_name),'both');
        }

        $result = $this->db->get();

        return $result->result_array();
    }

    function get_interest($contract_id) {
        $this->db->select('*');
        $this->db->from($this->table_payment);
        $this->db->where('loan_contract_id', $contract_id);
        $result = $this->db->get();
        return $result->row_array();
    }

    function save_print($invoice_id) {
        $this->db->set('printed', '1');
        $this->db->where('invoice_id', $invoice_id);
        $this->db->update($this->table_invoice);
    }
	
	
	function get_land($loan_id)
	{
		$this->db->select('*,
		(select province_name as land_province_name from master_province where province_id=land.land_addr_province_id),
		(select amphur_name as land_amphur_name from master_amphur where amphur_id=land.land_addr_amphur_id),
		(select district_name as land_district_name from master_district where district_id=land.land_addr_district_id),
		(select land_type_name from config_land_type where land_type_id=land.land_type_id)');
		$this->db->from("land");
		$this->db->join("loan_guarantee_land", "land.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->where("loan_guarantee_land.loan_id", $loan_id);
		$query = $this->db->get();
	    //echo $this->db->last_query();
		$result = $query->num_rows()!=0? $query->result_array() : array();	
		return $result;
	}
	
	function get_analysis($loan_id)
	{
		$this->db->select('*');
		$this->db->from("loan_analysis");
		$this->db->where("loan_id", $loan_id);
		$query = $this->db->get();
	    //echo $this->db->last_query();
		$result = $query->num_rows()!=0? $query->row_array() : array();	
		return $result;
	}
    function get_paymentschdule($contract_id){
			$this->db->select('*');
			$this->db->from("payment_schedule");
			$this->db->where('loan_contract_id',$contract_id);
			$result = $this->db->get();
			return $result->result_array();
	}	
}
