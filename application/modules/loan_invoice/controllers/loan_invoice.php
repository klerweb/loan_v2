<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class loan_invoice extends MY_Controller {

    private $title = 'รายการใบแจ้งชำระหนี้';
    private $status_bill = array('C' => 'ยกเลิก', 'N' => 'ปกติ', 'P' => 'จ่ายแล้ว', 'D' => 'จ่ายบางส่วน', 'E' => 'ปิดบัญชี');
    private $status_option = array(''=>'ทั้งหมด','A' => 'ปกติ/จ่ายบางส่วน', 'P' => 'จ่ายแล้ว','C' => 'ยกเลิก','E' => 'ปิดบัญชี');


    function __construct() {
        parent::__construct();
        $this->load->helper('date_helper');
        $this->load->helper('land_helper');
        $this->load->model('invoice_model');
        $this->load->model('person/person_model');
        $this->load->model('title/title_model');
        $this->load->model('province/province_model');
        $this->load->model('district/district_model');
        $this->load->model('amphur/amphur_model');

        $this->load->library('pagination');
        $this->load->helper('loan_helper');
    }

    public function index() {

        $page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
        $get_per_page = isset($_per_page) ? get_per_page($_per_page) : get_per_page();



        $arr_invoice = array();
        //if ($this->input->post()) {
        $contract_no = $this->input->post('contract_id');
        $person_thaiid = $this->input->post('person_card_id');
        $person_name = $this->input->post('person_name');
        $arr_invoice = $this->invoice_model->get_distinct_contract_no($contract_no, $person_thaiid, $person_name);

        if (!empty($arr_invoice)) {
            //get address
            foreach ($arr_invoice as $key => $val) {

                $province_name = $this->province_model->getById($val['person_addr_card_province_id']);
                $distinct_name = $this->district_model->getById($val['person_addr_card_district_id']);
                $amphur_name = $this->amphur_model->getById($val['person_addr_card_amphur_id']);


                $arr_invoice[$key]['province_name'] = $province_name['province_name'];
                $arr_invoice[$key]['district_name'] = $distinct_name['district_name'];
                $arr_invoice[$key]['amphur_name'] = $amphur_name['amphur_name'];
            }
        }
        //}

         //split page
        $arr_page_data = array_chunk($arr_invoice, $get_per_page, false);

//config pagging
        $total_rows = count($arr_invoice);
        $config_pagination = config_pagination();
        $config_pagination['base_url'] = current_url();
        $config_pagination['total_rows'] = $total_rows;
        $config_pagination['per_page'] = $get_per_page;

        $this->pagination->initialize($config_pagination);
        $pagination = $this->pagination->create_links();
        $title_pagination = title_pagination($page, $get_per_page, $total_rows);

        $data_on_page = array();
        if (count($arr_page_data)) {
            $data_on_page = $arr_page_data[$page - 1];
        }

        $data_menu['menu'] = 'account';
        $data_breadcrumb['menu'] = array('การเงิน' => '#');
        /*
          print('<pre>');
          print_r($arr_invoice);
          print('</pre>');
         */
        $arr_data = array('data' => $data_on_page,
                'pagging' => $pagination,
                'pagging_row' => $get_per_page,
            );

        $data = array(
            'title' => $this->title,
            'css_other' => array('modules_loan.css'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'content' => $this->parser->parse('forms', $arr_data, TRUE)
        );
        $this->parser->parse('main', $data);
    }

    public function list_invoice($loan_id) {


        $this->session->unset_userdata('ss_invoice');

        $arr_invoice = array();
        $arr_person = array();



        if (!empty($loan_id)) {
            $select_year = '';
            if ($this->input->post('ddl_year')) {
                $select_year = $this->input->post('ddl_year');
            }


            $arr_year = $this->invoice_model->get_distinct_year($loan_id);
            foreach ($arr_year as $key => $y) {
                $arr_year[$key]['select'] = "";
                if ($y['year_en'] == $select_year)
                    $arr_year[$key]['select'] = "selected = 'selected' ";
            }
            $status_paid = true; //สถานะจ่ายครบในแต่ละงวด
             //search by status
            $status = $this->input->post('ddl_paid');

            $arr_invoice = $this->invoice_model->getby_loanid($loan_id, $select_year);

            if (!empty($arr_invoice)) {
                foreach ($arr_invoice as $key => $val) {

                    //set status print
                    $arr_invoice[$key]['display_iconprint'] = false;
                    if ($val['setdisplay_year'] <= date('Y')) {
                        //if($val['status'] == 'N' || $val['status'] == 'D' ){
                        $arr_invoice[$key]['display_iconprint'] = true;
                        //}
                    }
                    if ($status_paid && (trim($val['status']) == 'D' || trim($val['status']) == 'N' )) {
                        $status_paid = false;
                    }
                    $arr_invoice[$key]['paid_status'] = $val['status'];
                    $arr_invoice[$key]['status_ext'] = $this->status_bill[trim($val['status'])];

                    $arr_invoice[$key]['css_print'] = $val['printed'] == "1" ? 'printed' : '';
                    $arr_invoice[$key]['css_print_tip'] = $val['printed'] == "1" ? 'print แล้ว' : 'รอ print';


                    //get amount
                    //$principle = !empty($val['principle'] ) ? $val['principle'] : 0.00;
                    //$interest = !empty($val['interest'] ) ? $val['interest'] : 0.00;
                    //$arr_invoice[$key]['pay_amount'] = $principle + $interest;
                    $amount = $this->invoice_model->get_payment($val['loan_contract_id']);
                    if (!empty($amount)) {
                        $arr_invoice[$key]['loan_balance'] = $amount['loan_amount'];
                    } else {
                        $arr_invoice[$key]['loan_balance'] = "0.00";
                    }
                }

                $arr_person = $this->person_model->getById($arr_invoice[0]['person_id']);
                $title = $this->title_model->getById($arr_person['title_id']);
                $arr_person['title_name'] = $title['title_name'];

                $province_name = $this->province_model->getById($arr_person['person_addr_card_province_id']);
                $distinct_name = $this->district_model->getById($arr_person['person_addr_card_district_id']);
                $amphur_name = $this->amphur_model->getById($arr_person['person_addr_card_amphur_id']);


                $arr_person['province_name'] = $province_name['province_name'];
                $arr_person['district_name'] = $distinct_name['district_name'];
                $arr_person['amphur_name'] = $amphur_name['amphur_name'];


            } else {
               // redirect('loan_invoice');
            }
        }


        $data_menu['menu'] = 'account';
        $data_breadcrumb['menu'] = array('การเงิน' => '#', 'รายการใบแจ้งชำระหนี้' => base_url() . 'loan_invoice');
        /*
          print('<pre>');
          print_r($arr_invoice);
          print('</pre>');
         */
        $arr_data = array(
            'data' => $arr_invoice,
            'person' => $arr_person,
            'year' => $arr_year,
            'status_paid' => $status_paid,
            'status_option' =>$this->status_option,
            'where_status' => $this->input->post('ddl_paid')

        );
        $this->session->set_userdata('ss_invoice', $arr_data);

        /*echo "<pre>";
        print_r($arr_data['data']);
        echo "</pre>";*/

        $data = array(
            'title' => $this->title,
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'content' => $this->parser->parse('list_invoice', $arr_data, TRUE)
        );
        $this->parser->parse('main', $data);
    }

    function pdf() {
        ob_start();
        $this->load->library('tcpdf');

        $filename = 'loan_invoice_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->AddPage();

        $htmlcontent = '';

        if ($this->session->has_userdata('ss_invoice')) {
            //find invoice_id
            $arr = $this->session->userdata('ss_invoice');

            //print_r($arr);
            //$this->load->model('invoice_model');
            //$this->invoice_model->save_print($invoice_id);
            $bal = 0.00;

            $start_period = '';
            $start_paid='';
            $end_paid='';
            $end_period = '';
            $paid_date = '';
            $count = 0;
            $html_invoice ='';
            foreach ($arr['data'] as $val) {
                if (trim($val['status']) == 'D' || trim($val['status']) == 'N') {
                    if ($start_period == ''){
                        $start_period = $val['loan_pay_number'];
                        $start_paid =  $val['date_payout'];
                    }


                    $end_period = $val['loan_pay_number'];
                    $paid_date = $val['date_payout'];
                    $end_paid = $val['date_payout'];

                    $count++;
                    $bal += $val['pay'];
                    $bl_loan_amount = $val['loan_balance'] - $bal;
                    $html_invoice .= '<tr>
                                                <td style="border: solild 1px #aaa;" width="30%">' . num2Thai(thai_display_date($val['date_payout'])) . '</td>
                                                <td style="border: solild 1px #aaa;text-align:center" width="10%">' . num2Thai($val['loan_pay_number']) . '</td>
                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($bl_loan_amount, 2)) . '</td>
                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['interest'], 2)) . '</td>
                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['pay'], 2)). '</td>
                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['pay_amount'], 2)). '</td>
                                        </tr>';
                }
            }

            $interest = $arr['data'][0]['loan_analysis_length'] == 1 ? 'รายเดือน' : $arr['data'][0]['loan_analysis_length'] == 6 ? 'ราย ๖ เดือน' : 'รายปี';
            $payment = $this->invoice_model->get_paymentschdule($arr['data'][0]['loan_contract_id']);

            $land = $this->invoice_model->get_land($arr['data'][0]['loan_id']);
            $html_land = '';
            if (!empty($land)) {
                foreach ($land as $rows) {
                    $html_land .= num2Thai($rows['land_type_name'] . ' เลขที่ ' . $rows['land_no'] . ' ตำบล' . $rows['land_district_name'] . ' อำเภอ' . $rows['land_amphur_name'] . ' จังหวัด' . $rows['land_province_name'] . ' จำนวนเนื้อที่ ' . $rows['land_area_rai'] . '-' . $rows['land_area_ngan'] . '-' . $rows['land_area_wah'] . ' ไร่  ');
                }
            }

            $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
		<td width="80%">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="text-align:right; color:#aaa;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 10px;"></td>
				</tr>
			</table>
			<hr />
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="45%"></td>
		<td width="55%">เขียนที่&nbsp;&nbsp;สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
	</tr>
	<tr>
		<td width="45%"></td>
		<td width="55%">วันที่&nbsp;&nbsp;' . num2Thai(thai_display_date($paid_date)) . '</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="7%">เรื่อง </td>
		<td width="93%">ขอแจ้งกำหนดการชำระหนี้ </td>
	</tr>
	<tr>
		<td width="7%">เรียน</td>
		<td width="93%">' . $arr['person']['title_name'] . $arr['person']['person_fname'] . ' ' . $arr['person']['person_lname'] . '</td>
	</tr>
	<tr>
		<td width="7%">อ้างถึง </td>
		<td width="93%">หนังสือกู้ยืมเงิน ตามสัญญาเลขที่ ' . num2Thai($arr['data'][0]['loan_contract_no']) . ' ลงวันที่ ' . num2Thai(thai_display_date($arr['data'][0]['loan_contract_date'])) . '</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="13%">สิ่งที่ส่งมาด้วย </td>
		<td width="87%">๑.ใบแจ้งการชำระหนี้ </td>
	</tr>
	<tr>
		<td width="13%"> </td>
		<td>๒.ตัวอย่างการเขียนใบนำฝากเงิน ณ ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ตามที่สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) หรือ บจธ. ได้อนุมัติให้สินเชื่อ (เงินกู้) แก่ท่านเป็นจำนวนเงิน ' . num2Thai(number_format($arr['data'][0]['loan_contract_amount'], 2)) . ' บาท (' . num2wordsThai($arr['data'][0]['loan_contract_amount']) . ') กำหนดให้ชำระหนี้ คืนเงินต้นและดอกเบี้ยเป็นงวด' . $interest . ' รวมจำนวน ' . num2Thai(count($payment)) . ' งวด เริ่มงวดแรกตั้งแต่วันที่ ' . num2Thai(thai_display_date($start_paid)) . ' ถึง งวดสุดท้ายวันที่ ' . num2Thai(thai_display_date($end_paid)) . '  ซึ่งในการนี้ ท่านได้นำ ' . $html_land . ' เพื่อจำนองไว้เป็น หลักค้ำประกันเงินกู้ รายละเอียดปรากฏตามหนังสือที่อ้างถึง นั้น</td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อโปรดดำเนินการชำระหนี้ต่อไปด้วย จักขอบคุณมาก </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="50%" style="text-align:left;"> </td>
			<td width="50%" style="text-align:center;">ขอแสดงความนับถือ</td>
		</tr>
		<tr>
			<td width="50%" style="text-align:left;"><br/></td>
			<td width="50%" style="text-align:center;"></td>
		</tr>
		<tr>
			<td width="50%" style="text-align:left;"> </td>
			<td width="50%" style="text-align:center;">(นางราตรี  พยนต์รักษ์)</td>
		</tr>
		<tr>
			<td width="50%" style="text-align:left;"></td>
			<td width="50%" style="text-align:center;">ผู้อำนวยการกองการเงินและบัญชี</td>
		</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="text-align:left;">กองการเงินและบัญชี </td>
				</tr>
				<tr>
					<td style="text-align:left;">โทร. ๐ ๒๒๗๘ ๑๖๔๘ </td>
				</tr>
				<tr>
					<td style="text-align:left;">โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
				</tr>
			</table>
';


            $pdf->writeHTML($htmlcontent, true, 0, true, true);
            $pdf->AddPage();

            $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    		<tr>
			    			<td  colspan="2" style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
										<td width="80%">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
												</tr>
											</table>
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="100%">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐ โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
												</tr>
											</table>
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="100%" style="font-size: 10px;"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td  colspan="2" style="border: solild 1px #aaa; text-align:center;">ใบแจ้งหนี้ชำระงวดที่  ' .num2Thai($start_period. '-' . $end_period). '</td>
			    		</tr>
			    		<tr>
			    			<td width="60%" style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    					 <tr>
					                    <td width="25%">ชื้อผู้กู้</td>
					                    <td>' . $arr['person']['title_name'] . $arr['person']['person_fname'] . ' ' . $arr['person']['person_lname'] . '</td>
					                </tr>
					                 <tr>
					                    <td width="25%">ที่อยู่</td>
					                    <td>' . show_address_master('person_addr_card', $arr['person']) . '</td>
					                </tr>
			    				</table>
			    			</td>
			    			<td rowspan="2" width="40%" style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    					 <tr>
					                    <td width="35%">วันที่</td>
					                    <td>' . num2Thai(thai_display_date($paid_date) ). '</td>
					                </tr>
					                <tr>
					                    <td width="35%">เลขที่สัญญา</td>
					                    <td>' . num2Thai($arr['data'][0]['loan_contract_no']) . '</td>
					                </tr>
					                <tr>
					                    <td width="35%">ดอกเบี้ย</td>
					                    <td>ร้อยละต่อ ' . num2Thai($arr['data'][0]['interest_rate']). '  &nbsp;' . $interest . ' </td>
					                </tr>
					                <tr>
					                    <td width="35%">จำนวนงวดชำระ</td>
					                    <td>' .num2Thai($count ). ' งวด</td>
					                </tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    					 <tr>
					                    <td width="25%">การนำส่งเงิน</td>
					                    <td>โอนผ่าน ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร<br/>สาขาบางเขน</td>
					                </tr>
					                 <tr>
					                    <td width="25%">ชื่อบัญชี</td>
					                    <td>สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)<br/>ประเภทเงินฝากออมทรัพย์ เลขที่ 020082094471</td>
					                </tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="2"><br/></td>
			    		</tr>
			    	</table>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="7%">เรื่อง </td>
					<td width="93%">ขอแจ้งกำหนดการชำระหนี้ </td>
				</tr>
				<tr>
					<td width="7%">เรียน</td>
					<td width="93%">' . $arr['person']['title_name'] . $arr['person']['person_fname'] . ' ' . $arr['person']['person_lname'] . '</td>
				</tr>
				<tr>
					<td width="7%">อ้างถึง </td>
					<td width="93%">หนังสือกู้ยืมเงิน ตามสัญญาเลขที่ ' . num2Thai($arr['data'][0]['loan_contract_no']) . ' ลงวันที่ ' . num2Thai(thai_display_date($arr['data'][0]['loan_contract_date'])) . '</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="13%">สิ่งที่ส่งมาด้วย </td>
					<td width="87%">๑.ใบแจ้งการชำระหนี้ </td>
				</tr>
				<tr>
					<td width="13%"> </td>
					<td>๒.ตัวอย่างการเขียนใบนำฝากเงิน ณ ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร </td>
				</tr>
			</table>

			    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
			    		<tr>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="30%">วัน/เดือน/ปี<br/>ที่ชำระ</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="10%">งวด</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">เงินต้น<br/>(บาท)</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">ดอกเบี้ย<br/>(บาท)</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">ยอดชำระทั้งสิ้น <br/>(บาท)</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">เงินต้นคงเหลือ (บาท)</td>
			    		</tr>
			    		' . $html_invoice . '
			    	</table>
			    	<br/><br/>
			    	<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    		 <tr>
			    		 	<td width="5%">ทั้งนี้ </td>
			    		 	<td width="95%"> ท่านต้องเป็นผู้รับภาระค่าธรรมเนียมในการโอนเงินที่ธนาคารการเกษตรและสหกรณ์การเกษตรเรียกเก็บเพิ่มอีกจำนวน 10 บาท (สิบบาทถ้วน)</td>
			    		 </tr>
			  ';
        }


        $pdf->SetFont('thsarabun', '', 16);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        $pdf->Output($filename . '.pdf', 'I');
    }

    function word_invoice()
    {
    	include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $arr = $this->session->userdata('ss_invoice');

    	$year_selected = '';
		foreach ($arr['year'] as $year) {
			if($year['select']){
				$year_selected = $year['year_en'];
			}
		}

		$x_paid_date = '-';
		$x_pay_number = '-';
		$x_date_payment = '-';
		$x_principle = '-';
		$x_interest = '-';
		$x_sum = '-';
		$x_sum_text = '-';

        $start_paid='';
        $end_paid='';
        $paid_date = '';
        foreach ($arr['data'] as $val) {
                if (trim($val['status']) == 'D' || trim($val['status']) == 'N') {
                    if ($start_paid == ''){
                        $start_paid =  $val['date_payout'];
                        $x_paid_date = date('Y-m-d',strtotime($start_paid.' - 30 days'));
                    }
                    $end_paid = $val['date_payout'];
                    $paid_date = $val['date_payout'];
                }
                if(trim($val['paid_status']) == 'D'){
                	$val['pay_amount'] = $val['principle'];
                	$val['pay'] = $val['money_pay'];
                }
                if($val['setdisplay_year'] == $year_selected){
                	$x_paid_date = date('Y-m-d',strtotime($val['date_payout'].' - 30 days'));
                	$x_pay_number = num2Thai($val['loan_pay_number']);
                	$x_date_payment = num2Thai(thai_display_date($val['date_payout']));
                	$x_principle = num2Thai(number_format($val['pay_amount'], 2));
                	$x_interest = num2Thai(number_format($val['interest'], 2));
                	$x_sum = num2Thai(number_format($val['pay'], 2));
                	$x_sum_text = num2wordsThai($val['pay']);
                }
       }

       $interest = $arr['data'][0]['loan_analysis_length'] == 1 ? 'รายเดือน' : $arr['data'][0]['loan_analysis_length'] == 6 ? 'ราย ๖ เดือน' : 'รายปี';
       $payment = $this->invoice_model->get_paymentschdule($arr['data'][0]['loan_contract_id']);

       $land = $this->invoice_model->get_land($arr['data'][0]['loan_id']);
       $html_land = '';
       $html_land_area = '';
       if (!empty($land)) {
           foreach ($land as $rows) {
           		$html_land = num2Thai($rows['land_type_name'] . ' เลขที่ ' . $rows['land_no'] . ' เลขที่ดิน ' . $rows['land_addr_no'] .  ' ตำบล' . $rows['land_district_name'] . ' อำเภอ' . $rows['land_amphur_name'] . ' จังหวัด' . $rows['land_province_name']);
           		$html_land_area  = num2Thai($rows['land_area_rai'] . '-' . $rows['land_area_ngan'] . '-' . $rows['land_area_wah']);
           }
        }

		$GLOBALS['paid_location'] = 'บจธ./';
		$GLOBALS['paid_date'] = num2Thai(thai_display_date($x_paid_date));
		$GLOBALS['person_fullname'] = $arr['person']['title_name'] . $arr['person']['person_fname'] . ' ' . $arr['person']['person_lname'];
		$GLOBALS['loan_contract_no'] = num2Thai($arr['data'][0]['loan_contract_no']);
		$GLOBALS['loan_contract_date'] = num2Thai(thai_display_date($arr['data'][0]['loan_contract_date']));
		$GLOBALS['loan_contract_amount'] = num2Thai(number_format($arr['data'][0]['loan_contract_amount'], 2));
		$GLOBALS['loan_contract_amount_text'] = num2wordsThai($arr['data'][0]['loan_contract_amount']);
		$GLOBALS['interest'] = $interest;
		$GLOBALS['count_payment'] = num2Thai(count($payment));
		$GLOBALS['start_paid'] = num2Thai(thai_display_date($start_paid));
		$GLOBALS['end_paid'] = num2Thai(thai_display_date($end_paid));
		$GLOBALS['land'] = $html_land;
		$GLOBALS['land_area'] = $html_land_area;

		$GLOBALS['x_pay_number'] = $x_pay_number;
		$GLOBALS['x_date_payment'] = $x_date_payment;
		$GLOBALS['x_principle'] = $x_principle;
		$GLOBALS['x_interest'] = $x_interest;
		$GLOBALS['x_sum'] = $x_sum;
		$GLOBALS['x_sum_text'] = $x_sum_text;

		$template = 'word/template/loan_invoice.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'loan_invoice_'.date('Y-m-d').'.docx';
    $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
	  $TBS->Show(OPENTBS_FILE, $temp_file);
   	$this->send_download($temp_file,$output_file_name);
    }

    public function send_download($temp_file,$file) {
          $basename = basename($file);
          $length   = sprintf("%u", filesize($temp_file));

          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="' . $basename . '"');
          header('Content-Transfer-Encoding: binary');
          header('Connection: Keep-Alive');
          header('Expires: 0');
          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
          header('Pragma: public');
          header('Content-Length: ' . $length);
          ob_clean();
          flush();
          set_time_limit(0);
          readfile($temp_file);
          exit();
      }
	function pdf_invoice() {
        ob_start();
        $this->load->library('tcpdf');

        $filename = 'loan_invoice_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->AddPage();

        $htmlcontent = '';

        if ($this->session->has_userdata('ss_invoice')) {
            $arr = $this->session->userdata('ss_invoice');

        $year_selected = '';
		foreach ($arr['year'] as $year) {
			if($year['select']){
				$year_selected = $year['year_en'];
			}
		}

            $bal = 0.00;
            $bl_loan_amount = 0.00;

            $start_period = '';
            $start_paid='';
            $end_paid='';
            $end_period = '';
            $paid_date = '';
            $count = 0;
            $html_invoice ='';
            $x_paid_date = '';
            foreach ($arr['data'] as $val) {
                if (trim($val['status']) == 'D' || trim($val['status']) == 'N') {
                    if ($start_period == ''){
                        $start_period = $val['loan_pay_number'];
                        $start_paid =  $val['date_payout'];
                    }
                    $end_period = $val['loan_pay_number'];
                    $paid_date = $val['date_payout'];
                    $end_paid = $val['date_payout'];

                    $count++;
                	if(trim($val['paid_status']) == 'N' || trim($val['paid_status']) == 'D'){ //calcurate balance
                       $bal += $val['pay_amount'];
                       $bl_loan_amount = $val['loan_balance'] - $bal;
                    }
                    /* update by pairin */
                    if(trim($val['paid_status']) == 'D'){
                     	$val['pay_amount'] = $val['principle'];
                    	$val['pay'] = $val['money_pay'];
                    	$bl_loan_amount = $val['loan_amount_balance'];
                    }
	                if($val['setdisplay_year'] == $year_selected){
	                	$start_period = $val['loan_pay_number'];
	                	$x_paid_date = $val['date_payout'];
	                	$html_invoice .= '<tr>
	                                                <td style="border: solild 1px #aaa;" width="30%">' . num2Thai(thai_display_date($val['date_payout'])) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:center" width="10%">' . num2Thai($val['loan_pay_number']) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['pay_amount'], 2)) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['interest'], 2)) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['pay'], 2)). '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($bl_loan_amount, 2)). '</td>
	                                        </tr>';
	                }else{
	                	$x_paid_date = $start_paid;
	                	$html_invoice .= '<tr>
	                                                <td style="border: solild 1px #aaa;" width="30%">' . num2Thai(thai_display_date($val['date_payout'])) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:center" width="10%">' . num2Thai($val['loan_pay_number']) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['pay_amount'], 2)) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['interest'], 2)) . '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($val['pay'], 2)). '</td>
	                                                <td style="border: solild 1px #aaa;text-align:right" width="15%">' . num2Thai(number_format($bl_loan_amount, 2)). '</td>
	                                        </tr>';
	                }

                }
            }

            $interest = $arr['data'][0]['loan_analysis_length'] == 1 ? 'ต่อเดือน' : $arr['data'][0]['loan_analysis_length'] == 6 ? 'ต่อ ๖ เดือน' : 'ต่อปี';
            $payment = $this->invoice_model->get_paymentschdule($arr['data'][0]['loan_contract_id']);


            $land = $this->invoice_model->get_land($arr['data'][0]['loan_id']);
            $html_land = '';
            if (!empty($land)) {
                foreach ($land as $rows) {
                    $html_land .= num2Thai($rows['land_type_name'] . ' เลขที่ ' . $rows['land_no'] . ' ตำบล' . $rows['land_district_name'] . ' อำเภอ' . $rows['land_amphur_name'] . ' จังหวัด' . $rows['land_province_name'] . ' จำนวนเนื้อที่ ' . $rows['land_area_rai'] . '-' . $rows['land_area_ngan'] . '-' . $rows['land_area_wah'] . ' ไร่  ');
                }
            }

            $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    		<tr>
			    			<td  colspan="2" style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
										<td width="80%">
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
												</tr>
											</table>
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="100%">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐ โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
												</tr>
											</table>
											<table width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td width="100%" style="font-size: 10px;"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td  colspan="2" style="border: solild 1px #aaa; text-align:center;">ใบแจ้งหนี้ชำระงวดที่  ' .num2Thai($start_period). '</td>
			    		</tr>
			    		<tr>
			    			<td width="55%" style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    					 <tr>
					                    <td width="25%">ชื้อผู้กู้</td>
					                    <td>' . $arr['person']['title_name'] . $arr['person']['person_fname'] . ' ' . $arr['person']['person_lname'] . '</td>
					                </tr>
					                 <tr>
					                    <td width="25%">ที่อยู่</td>
					                    <td>' . show_address_master('person_addr_card', $arr['person']) . '</td>
					                </tr>
			    				</table>
			    			</td>
			    			<td rowspan="2" width="45%" style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    					 <tr>
					                    <td width="35%">วันที่</td>
					                    <td>' . num2Thai(thai_display_date($x_paid_date) ). '</td>
					                </tr>
					                <tr>
					                    <td width="35%">เลขที่สัญญา</td>
					                    <td>' . num2Thai($arr['data'][0]['loan_contract_no']) . '</td>
					                </tr>
					                <tr>
					                    <td width="35%">ดอกเบี้ย</td>
					                    <td>ร้อยละ  ' . num2Thai($arr['data'][0]['interest_rate']). '  &nbsp;' . $interest . ' </td>
					                </tr>
					                <tr>
					                    <td width="35%">จำนวนงวดชำระ</td>
					                    <td>' .num2Thai($count ). ' งวด</td>
					                </tr>
					                <tr>
					                    <td width="35%">วงเงินกู้</td>
					                    <td>' .num2Thai(number_format($arr['data'][0]['loan_contract_amount'], 2)). ' บาท</td>
					                </tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td style="border: solild 1px #aaa;">
			    				<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    					 <tr>
					                    <td width="25%">การนำส่งเงิน</td>
					                    <td>โอนผ่าน ธนาคารเพื่อการเกษตรและ<br/>สหกรณ์การเกษตร สาขาบางเขน</td>
					                </tr>
					                 <tr>
					                    <td width="25%">ชื่อบัญชี</td>
					                    <td>สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)<br/>ประเภทเงินฝากออมทรัพย์ เลขที่ 020082094471</td>
					                </tr>
			    				</table>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td colspan="2"><br/></td>
			    		</tr>
			    	</table>
			    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
			    		<tr>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="30%">วัน/เดือน/ปี<br/>ที่ชำระ</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="10%">งวดที่</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">เงินต้น<br/>(บาท)</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">ดอกเบี้ย<br/>(บาท)</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">ยอดชำระทั้งสิ้น <br/>(บาท)</td>
			    			<td style="border: solild 1px #aaa; text-align:center;" width="15%">เงินต้นคงเหลือ (บาท)</td>
			    		</tr>
			    		' . $html_invoice . '
			    	</table>
			    	<br/><br/>
			    	<table width="100%" border="0" cellpadding="0" cellspacing="0">
			    		 <tr>
			    		 	<td width="5%">ทั้งนี้ </td>
			    		 	<td width="95%"> ท่านต้องเป็นผู้รับภาระค่าธรรมเนียมในการโอนเงินที่ธนาคารการเกษตรและสหกรณ์การเกษตรเรียกเก็บเพิ่มอีกจำนวน <br/>10 บาท (สิบบาทถ้วน)</td>
			    		 </tr>
			       </table>
			       <table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="50%" style="text-align:left;"><br/></td>
							<td width="50%" style="text-align:center;"></td>
						</tr>
						<tr>
							<td width="50%" style="text-align:left;"> </td>
							<td width="50%" style="text-align:center;">(นางราตรี  พยนต์รักษ์)</td>
						</tr>
						<tr>
							<td width="50%" style="text-align:left;"></td>
							<td width="50%" style="text-align:center;">ผู้อำนวยการกองการเงินและบัญชี</td>
						</tr>
				</table>
			  ';
        }


        $pdf->SetFont('thsarabun', '', 16);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        $pdf->Output($filename . '.pdf', 'I');
    }

    function print_form($invoice_id) {

        if ($this->session->has_userdata('ss_invoice')) {
            //find invoice_id
            $arr = $this->session->userdata('ss_invoice');
            $arr_invoice = array();
            foreach ($arr['data'] as $key => $val) {
                if ($val['invoice_id'] == $invoice_id) {
                    $val['date_expire_payment'] = thai_display_date($val['date_expire_payment']);
                    $val['principle'] = number_format($val['principle'], 2);
                    $val['interest'] = number_format($val['interest'], 2);
                    //$val['pay_amount'] = number_format($val['pay_amount'],2);
                    //$val['loan_balance'] = number_format($val['loan_balance'],2);
                    $arr_invoice = $val;
                    $arr_invoice['count'] = count($arr['data']);
                }
            }
            //print_r($arr_invoice);
            $this->load->model('invoice_model');
            $data = array(
                'person' => $arr['person'],
                'data' => $arr_invoice,
                'list_data' => $arr,
                'interest' => $this->invoice_model->get_interest($arr_invoice['loan_contract_id'])
            );
            $this->load->model('invoice_model');
            $this->invoice_model->save_print($invoice_id);

            $this->load->view('print', $data);
        }
    }

}
