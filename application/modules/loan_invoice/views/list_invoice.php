<form action="" name="frm_inv" method="post">
<style>
    .printed{
        color:red;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-1"><label>ชื่อผู้กู้</label></div>
            <div class="col-sm-4"><?php echo $person['title_name'] . $person['person_fname'] . ' ' . $person['person_lname']; ?></div>
            <div class="col-sm-2"><label>เลขที่สัญญา</label></div>
            <div class="col-sm-4"><?php echo $data[0]['loan_contract_no']; ?></div>
        </div>
        <div class="row">
            <div class="col-sm-1"><label>ที่อยู่</label></div>
            <div class="col-sm-10"><?php echo show_address_master('person_addr_card', $person); ?></div>

        </div>
        <div class="row">
            <div class="col-sm-1"><label>วงเงินกู้</label></div>
            <div class="col-sm-4"><?php echo isset($data[0]['loan_contract_amount']) ? number_format($data[0]['loan_contract_amount'], 2) : 0.00; ?></div>
            <div class="col-sm-1">บาท</div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label>ใบแจ้งหนี้ปี</label>
                <select class="form-control" name="ddl_year" id="select_year">
                    <option value=''>--กรุณาเลือก--</option>
                    <?php if(isset($year)){ 
                        foreach($year as $y){
                        ?>
                    <option value="<?php echo $y['year_en']; ?>" <?php echo $y['select']?>><?php echo $y['year_th']; ?></option>
                    <?php 
                        }
                        } ?>
                </select>
            </div>
            <div class="col-sm-3">
                <label>สถานะชำระ</label>
                <select name="ddl_paid" id="ddl_paid"  class="form-control" >
                    <?php 
                       $get_paid = $where_status; 
                       
                        foreach($status_option as $key => $val){
                            $default = '';
                            if($key == $get_paid)
                                $default = "selected='selected'";
                            
                    ?>
                    <option value="<?php echo $key;?>" <?php echo $default; ?>><?php echo $val; ?></option>
                    <?php 
                        }
                    ?>
                </select>
            </div>
            
            <div class="col-sm-6 text-right align-bottom">
                <?php if($status_paid) { ?>
                <label> <a href="#" class="btn btn-primary">
                    จ่ายแล้ว
                </a></label>
                <?php }else{ ?>
                <label> &nbsp;</label>
                <a href="<?php echo site_url("loan_invoice/word_invoice/0");?>" class="btn btn-primary" target="_bank">
                <i class="fa fa-print" aria-hidden="true"></i> Print หนังสือแจ้งหนี้
                </a>
                <label> &nbsp;</label>
                <a href="<?php echo site_url("loan_invoice/pdf_invoice/0");?>" class="btn btn-primary" target="_bank">
                <i class="fa fa-print" aria-hidden="true"></i> Print ใบแจ้งหนี้
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="panel-body">


        <div class="table-responsive">
            <table class="table mb30">
                <thead>
                    <tr>
                        <th>วันเดือนปี ที่ชำระ</th>
                        <th>งวดที่</th>
                        <th>เงินต้น</th>
                        <th>ดอกเบี้ย</th>
                        <th>ยอดชำระทั้งสิ้น</th>
                        <th>เงินต้นคงเหลือ</th>
                        <th>สถานะชำระ</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($data)) {
                        $bal = 0.00;
                        $bl_loan_amount = 0.00;
                        $render = false;
                        foreach ($data as $val) {
                           $render = false;
                           if(empty($where_status)) $render= true;
                           if($where_status=='A' && (trim($val['paid_status']) == 'N' ||  trim($val['paid_status']) =='D')) $render= true;
                           if($where_status==trim($val['paid_status'])) $render= true;
                           
                            if($render){
                            //$bl_loan_amount = $val['loan_balance'];
                            if(trim($val['paid_status']) == 'N' || trim($val['paid_status']) == 'D'){ //calcurate balance
                                $bal += $val['pay_amount'];
                                $bl_loan_amount = $val['loan_balance'] - $bal;
                            }
                             /* update by pairin */
                            if(trim($val['paid_status']) == 'D'){
                                $val['pay_amount'] = $val['principle'];
                                $val['pay'] = $val['money_pay'];
                                $bl_loan_amount = $val['loan_amount_balance'];
                            }
                            ?>
                            <tr>
                                <td><?php echo thai_display_date($val['date_payout']); ?></td>
                                <td><?php echo $val['loan_pay_number']; ?></td>
		     		            <td><?php echo number_format($val['pay_amount'], 2); ?></td>
                                <td><?php echo number_format($val['interest'], 2); ?></td>
                                <td><?php echo number_format($val['pay'], 2); ?></td>
                                <td><?php echo number_format($bl_loan_amount, 2); ?></td>
                               
                                <td><?php echo $val['status_ext'];?></td>
                                    <!--
                        <?php if($val['display_iconprint']== true) { ?>
                                    <a href="<?php echo site_url("loan_invoice/print_form/"); ?>" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="<?php echo $val['css_print_tip']; ?>" target="_bank">
                                        <i class="fa fa-print <?php echo $val['css_print']; ?>" aria-hidden="true"></i>
                                        <?php echo $val['setdisplay_year'].'/'.$val['status'];?>
                                    </a>
                            <?php }  ?>
                                </td> -->
                            </tr>
                            <?php
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- div -->
    </div><!-- panel body -->
</div>
</form>
<script>
    $('#select_year').change(function(){
        $('form').submit();
    });
    $('#ddl_paid').change(function(){
        $('form').submit();
    });
</script>



