
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link href="<?php echo base_url(); ?>assets/css/style.default.css" rel="stylesheet">
        <style>

            table{
                margin-top:5%;
                margin-left:5%;
                margin-right:5%;
                width:90%;
                font-size:12px;
            }
            table tr td{
                /*border:1px solid #ccc;*/
                line-height:25px;
                padding:5px;
            }
            tr.bd-all td{
                border:1px solid #ccc;
            }
            .bd-top {
                border-top:1px solid #ccc;
            }
            .bd-left {
                border-left:1px solid #ccc;
            }
            .bd-right {
                border-right:1px solid #ccc;
            }
            .bd-bottom {
                border-bottom:1px solid #ccc;
            }
            .bd-none{
                border-color:#fff;
            }
            .text-top{vertical-align: text-top;}
            /*
            .text-center{ text-align:center;}
            .text-right{ text-align:right;}
            .text-bold{font-weight:bold;}
            .text-nowrap{
                    word-wrap: break-word;
            }
            */
        </style>
    </head>
    <body>
        <table>
            <tbody>

                <tr class="bd-top bd-left bd-right">
                    <td rowspan="3"><img src="<?php echo base_url() ?>assets/images/logo_invoice.png" width="80" height="80"/></td>
                    <td colspan="5">สถาบันบริหารจัดการธนาคารที่ดิน&nbsp; (องค์การมหาชน)</td>
                </tr>
                <tr class="bd-right">
                    <td colspan="5">เลขที่ 210 สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจศิริ ถนนพหลโยธิน เขตพญาไท</td>
                </tr>
                <tr class="bd-right">
                    <td colspan="5">กรุงเพมหานคร 10400 โทร.  0 2278 1648 โทรสาร 0 2278 1148</td>
                </tr>
                <tr class="bd-top bd-left bd-right bd-bottom">
                    <td colspan="6" class="text-center text-bold">ใบแจ้งหนี้ชำระงวดที่  <?php echo $data['loan_pay_number']; ?></td>
                </tr>
                <!--detail-->
                <tr class="bd-left">
                    <td>ชื้อผู้กู้</td>
                    <td colspan="3" class="bd-right "><?php echo $person['title_name'] . $person['person_fname'] . ' ' . $person['person_lname']; ?></td>
                    <td>วันที่</td>
                    <td class="bd-right"><?php echo $data['date_expire_payment']; ?></td>
                </tr>
                <tr class="bd-left">
                    <td class="bd-bottom text-top">ที่อยู่</td>
                    <td colspan="3" class="bd-right bd-bottom" ><?php echo show_address_master('person_addr_card', $person); ?></td>
                    <td class="bd-none text-top">เลขที่สัญญา</td>
                    <td class="bd-right text-top"><?php echo $data['loan_contract_no']; ?></td>
                </tr>
                <tr class="bd-left">
                    <td class="text-bold text-nowrap">การนำส่งเงิน</td>
                    <td class="text-bold bd-right"  colspan="3">โอนผ่าน ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร </td>
                    <td>ดอกเบี้ย</td>
                    <td class="bd-right">ร้อยละ <?php echo $interest['interest_rate']; ?> ต่อปี</td>
                </tr>
                <tr class="bd-left">
                    <td></td>
                    <td class="text-bold bd-right"  colspan="3">สาขาบางเขน</td>
                    <td>จำนวนงวดชำระ</td>
                    <td  class="bd-right"><?php echo $data['count']; ?>  งวด</td>
                </tr>
                <tr class="bd-left">
                    <td class="text-bold">ชื่อบัญชี</td>
                    <td class="text-bold bd-right" colspan="3">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
                    <td></td>
                    <td  class="bd-right"></td>
                </tr>
                <tr class="bd-left">
                    <td class="bd-bottom"></td>
                    <td class="text-bold bd-bottom bd-right" colspan="3">ประเภทเงินฝากออมทรัพย์ เลขที่ 020082094471</td>
                    <td class="bd-bottom"></td>
                    <td class="bd-bottom bd-right"></td>
                </tr>
                <tr style="height:30px;">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>



                <tr class="bd-all">
                    <td class="text-center" width="1%">วัน/เดือน/ปี ที่ชำระ</td>
                    <td class="text-center">งวด</td>
                    <td class="text-center">เงินต้น (บาท)</td>
                    <td class="text-center">ดอกเบี้ย (บาท)</td>
                    <td class="text-center">ยอดชำระทั้งสิ้น <br/>(บาท)</td>
                    <td class="text-center">เงินต้นคงเหลือ (บาท)</td>
                </tr>
                <!--detail -->
                <?php
                foreach ($list_data['data'] as $val) {
                    //if ($val['status'] == 'D' || $val['status'] == 'N') {
                        ?>
                        <tr class="bd-all">
                            <td class="text-center"><?php echo $val['date_expire_payment']; ?></td>
                            <td class="text-center"><?php echo $val['loan_pay_number']; ?></td>
                            <td class="text-right"><?php echo $val['principle']; ?></td>
                            <td class="text-right"><?php echo $val['interest']; ?></td>
                            <td class="text-right"><?php echo $val['pay_amount']; ?></td>
                            <td class="text-right"><?php echo $val['loan_balance']; ?></td>
                        </tr>
                        <?php
                    //}
                }
                ?>
                <tr>
                    <td colspan="6">ทั้งนี้ ท่านต้องเป็นผู้รับภาระค่าธรรมเนียมในการโอนเงินที่ธนาคารการเกษตรและสหกรณ์การเกษตร</td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-left:30px;">เรียกเก็บเพิ่มอีกจำนวน 10 บาท (สิบบาทถ้วน)</td>
                </tr>
                <tr>
                    <td colspan="4"></td>
                    <td style="padding-top:40px" colspan="2">
                        <div class="text-center textnowrap">
                            <div>นางราตรี  พยนต์รักษ์</div>
                            <div>ผู้อำนวยการกองการเงินและบัญชี</div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <script>
            //window.print();
        </script>
    </body>	
</html>