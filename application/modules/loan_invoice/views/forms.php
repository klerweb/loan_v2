<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-btns">
            <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
        </div><!-- panel-btns -->
        <h4 class="panel-title">ค้นหา</h4>
    </div>
    <div class="panel-body">
        <form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url("loan_invoice"); ?>">
			<div class="form-group">
                <label class="sr-only" for="loan_code">เลขที่สัญญา</label>
                <input type="text" class="form-control" id="contract_id" name="contract_id" placeholder="เลขที่สัญญา" value="" />
            </div>
			<div class="form-group">
                <label class="sr-only" for="loan_code">เลขที่บัตรประชาชน</label>
                <input type="text" class="form-control" id="person_card_id" name="person_card_id" placeholder="เลขที่บัตรประชาชน" value="" />
            </div>
            <div class="form-group">
                <label class="sr-only" for="person_name">ชื่อ-นามสกุล</label>
                <input type="text" class="form-control input-xs" id="person_name" name="person_name" placeholder="ชื่อ-นามสกุล" value="" />
            </div>
           
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div><!-- panel-body -->
</div><!-- panel -->

<div class="row">
	<div class="col-md-12">
		<!-- BASIC WIZARD -->
		<form method="post" id="valWizard" class="panel-wizard">
			<div class="tab-pane">
					<!--<div class="row">
						<div class="col-sm-12 right">
							<button id="btnAddCo" name="btnAddCo" type="button" class="btn btn-primary" onClick="location.href='<?php echo base_url()?>land/form'">เพิ่มแบบบันทึก</button>
						</div>
					</div>
					-->
					<div class="col-md-12">
	  
					  <div class="table-responsive">
					  <table class="table mb30">
						<thead>
						  <tr>
							<th>#</th>
							<th>เลขที่สัญญา</th>
							<th>ชื่อผู้กู้</th>
							<th>เลขที่บัตรประชาชน</th>
							<th>ที่อยู่</th>
							<th>รายละเอียด</th>
						  </tr>
						</thead>
						<tbody>
						<?php 
						if(count($data) > 0){
							$i = 1;
							foreach($data as $val){
						?>
						  <tr>
							<td><?php echo $i;?></td>
							<td><?php echo $val['loan_contract_no'];?></td>
							<td><?php echo $val['person_fname'].' '.$val['person_lname'];?></td>
							<td><?php echo $val['person_thaiid'];?></td>
							<td><?php echo show_address_master('person_addr_card',$val);?></td>
							<td><a href="<?php echo site_url("loan_invoice/list_invoice/".$val['loan_id']); ?>" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="ดูรายการ"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></td>
						  </tr>			  
						 <?php
								$i++;
								}
							}else if($this->input->post()){
								
						?>
							
							<tr>
								<td colspan="6"><div class="alert alert-danger text-center" role="alert">ไม่พบข้อมูล</div></td>
							</tr>
							
						<?php
							}
						?>
						</tbody>
					  </table>
                                              <?= $pagging; ?>
					  </div><!-- table-responsive -->
					 
					</div>
			</div><!-- tab-pane -->
		</form><!-- panel-wizard -->
	</div><!-- col-md-12 -->
</div><!-- row -->
<!-- content -->
