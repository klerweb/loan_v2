<ul class="breadcrumb">
	<li><a href="<?php echo site_url('dashboard'); ?>"><i class="glyphicon glyphicon-home"></i></a></li>
	<?php foreach ($menu as $page => $link) {  ?>
		<li><a href="<?php echo $link; ?>"><?php echo $page; ?></a></li>
	<?php } ?>
</ul>