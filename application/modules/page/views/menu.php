<h5 class="leftpanel-title">Navigation</h5>
<ul class="nav nav-pills nav-stacked">
	<li <?php if($menu=='dashboard') echo 'class="active"'; ?>><a href="<?php echo base_url()?>dashboard"><i class="fa fa-home"></i> <span>หน้าหลัก</span></a></li>
	
	<?php if(check_permission_isread('loan')){?>
	<li class="parent<?php if($menu=='loan') echo ' active'; ?>"><a href="#"><i class="fa fa-edit"></i> <span>สินเชื่อ</span></a>
		<ul class="children">
			<li><a href="<?php echo base_url()?>loan">รายการสินเชื่อ (บจธ 1)</a></li>
			<li><a href="<?php echo base_url()?>non_loan">รายการสินเชื่อ (บจธ 2)</a></li>
			<li><a href="<?php echo base_url()?>land">รายการแบบบันทึกการตรวจสอบที่ดิน</a></li>
			<li><a href="<?php echo base_url()?>building">รายการแบบบันทึกการตรวจสอบประเมินราคาสิ่งปลูกสร้าง</a></li>
			<li><a href="<?php echo base_url()?>summary">สรุปการประเมินราคาทรัพย์สิน</a></li>
			<li><a href="<?php echo base_url()?>analysis">พิจารณาการขอสินเชื่อ</a></li>
            <li><a href="<?php echo base_url()?>approve">รายงานขออนุมัติคำขอ</a></li>
			
		</ul>
	</li>
	<?php } ?>
	
	<?php if(check_permission_isread('law')){?>
	<li <?php if($menu=='law') echo 'class="active"'; ?>><a href="<?php echo base_url()?>forms"><i class="fa fa-check-square-o"></i> <span>นิติกรรม</span></a></li>
	<?php } ?>
	
	<?php if(check_permission_isread('debt')){?>
	<li class="parent<?php if($menu=='debt') echo ' active'; ?>"><a href="#"><i class="fa fa-sign-out"></i> <span>แบบปรับโครงสร้างหนี้</span></a>
		<ul class="children">
            <li><a href="<?php echo base_url()?>delay">ผ่อนผัน</a></li>
			<li><a href="<?php echo base_url()?>extend">ขยายระยะเวลา</a></li>
			<li><a href="<?php echo base_url()?>debt">ปรับโครงสร้างหนี้</a></li>
		</ul>
	</li>
	<?php } ?>
	<?php if(check_permission_isread('account')){?>
	<li class="parent<?php if($menu=='account') echo ' active'; ?>"><a href="#"><i class="fa fa-money"></i> <span>การเงิน</span></a>
		<ul class="children">
			<li><a href="<?php echo base_url()?>finance_disburse">เบิกจ่ายเงินกู้</a></li>
			<li><a href="<?php echo base_url()?>loan_invoice">ใบแจ้งหนี้</a></li>
            <li><a href="<?php echo base_url()?>loan_payment">ชำระหนี้เงินกู้</a></li>
		</ul>
	</li>
	<?php } ?>
	<?php if(check_permission_isread('report_m')){?>
	<li class="parent<?php if($menu=='report_m') echo ' active'; ?>"><a href="#"><i class="fa fa-credit-card"></i> <span>รายงานทางการเงิน</span></a>
		<ul class="children">
			<li><a href="<?php echo base_url()?>report_debtor">รายงานการ์ดลูกหนี้</a></li>
            <li><a href="<?php echo base_url()?>accured_interest">รายงานดอกเบี้ยค้างรับ</a></li>
			<li><a href="http://122.155.187.190:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports%2FLB&reportUnit=%2Freports%2FLB%2FRegisDebtorsCredit&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">ทะเบียนคุมลูกหนี้</a></li>
			<li><a href="http://122.155.187.190:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports%2FLB&reportUnit=%2Freports%2FLB%2FContractReg&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">ทะเบียนคุมสัญญา</a></li>
			<li><a href="http://122.155.187.190:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports%2FLB&reportUnit=%2Freports%2FLB%2FInvoiceReg&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">ทะเบียนคุมใบแจ้งหนี้</a></li>
			<li><a href="http://122.155.187.190:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports%2FLB&reportUnit=%2Freports%2FLB%2FOverDue&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">รายงานลูกหนี้ผิดนัดชำระ</a></li>
			<li><a href="http://122.155.187.190:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports%2FLB&reportUnit=%2Freports%2FLB%2FLoanPayment&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">รายงานการจ่ายเงินสินเชื่อ</a></li>
			<li><a href="http://122.155.187.190:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2Freports%2FLB&reportUnit=%2Freports%2FLB%2FDailyPayment&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">รายงานการรับชำระสินเชื่อประจำวัน</a></li>
		</ul>
	</li>
	<?php } ?>
	<?php if(check_permission_isread('report')){?>
	<!-- <li class="parent<?php if($menu=='report') echo ' active'; ?>"><a href="#"><i class="fa fa-cube"></i> <span>รายงาน</span></a>
		<ul class="children">
			<li><a href="#">รายงานขออนุมัติคำขอ</a></li>
			<li><a href="#">รายงานสรุปผลการพิจารณาคำขอสินเชื่อ</a></li>
			<li><a href="#">รายงานลูกหนี้รายสัญญา</a></li>
			<li><a href="#">รายงานสรุปผลการพิจารณาคำขอสินเชื่อ</a></li>
		</ul>
	</li> -->
	<?php } ?>
	
	<?php if(check_permission_isread('setting')){?>
	<li class="parent<?php if($menu=='setting') echo ' active'; ?>"><a href="#"><i class="fa fa-gear"></i> <span>ตั้งค่า</span></a>
		<ul class="children">
			<li><a href="<?php echo base_url()?>title">คำนำหน้า</a></li>
			<li><a href="<?php echo base_url()?>race">เชื้อชาติ</a></li>
			<li><a href="<?php echo base_url()?>nationality">สัญชาติ</a></li>
			<li><a href="<?php echo base_url()?>religion">ศาสนา</a></li>
			<li><a href="<?php echo base_url()?>career">อาชีพ</a></li>
			<li><a href="<?php echo base_url()?>relationship">ความสัมพันธ์</a></li>
			<li><a href="<?php echo base_url()?>land_type">ประเภทที่ดิน</a></li>
			<li><a href="<?php echo base_url()?>building_part">ส่วนต่างๆ ของสิ่งปลูกสร้าง</a></li>
			<li><a href="<?php echo base_url()?>non_loan_objective">วัตถุประสงค์ (กรณีไม่รับคำขอ)</a></li>
			<li><a href="<?php echo base_url()?>non_loan_case">สาเหตุ (กรณีไม่รับคำขอ)</a></li>
			<li><a href="<?php echo base_url()?>bank">ธนาคาร</a></li>
			<li><a href="<?php echo base_url()?>bank_payment">ธนาคารสำหรับรับชำระหนี้</a></li>
			<li><a href="<?php echo base_url()?>project">โครงการ</a></li>
		</ul>
	</li>
	<?php } ?>
	
	<?php if(check_permission_isread('user')){?>
	<li class="parent<?php if($menu=='user') echo ' active'; ?>"><a href="#"><i class="fa fa-user"></i> <span>ผู้ใช้งาน</span></a>
		<ul class="children">
        	<li><a href="<?php echo base_url()?>user">พนักงาน</a></li>
            <li><a href="<?php echo base_url()?>role">สิทธิการใช้งาน</a></li>  
            <li><a href="<?php echo base_url()?>mapping">รายการเมนู</a></li>                             
    	</ul>
	</li>
	<?php } ?>  
   <?php if(check_permission_isread('leasing')){?>    
    <li  class="parent  <?php if($menu=='leasing') echo ' active'; ?>"><a href="#"><i class="fa fa-check-square-o"></i> <span>เช่าที่ดิน</span></a> 
         <ul class="children">
            <li><a href="<?php echo base_url()?>leasing"> สัญญาเช่าซื้อที่ดิน</a></li>
            <li><a href="<?php echo base_url()?>leasing/report_invoice">รายงานการ์ดลูกหนี้</a></li>
            <li><a href="<?php echo base_url()?>leasing/report_invoice2">ใบแจ้งหนี้</a></li>  
            <li><a href="http://10.10.10.41:8081/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=%2Freports%2FLB%2FContractHirePurchaseReg&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">ทะเบียนคุมเลขที่สัญญาเช่าซื้อ</li> 
            <li><a href="http://10.10.10.41:8081/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=%2Freports%2FLB%2FDailyPaymentHirePurchase&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">รายงานการรับชำระเช่าซื้อประจำวัน</li> 
            <li><a href="http://10.10.10.41:8081/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=%2Freports%2FLB%2FInvoiceHirePurchaseReg&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">ทะเบียนคุมใบแจ้งหนี้เช่าซื้อ</li> 
            <li><a href="http://10.10.10.41:8081/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=%2Freports%2FLB%2FLoanPaymentHirePurchase&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">รายงานการจ่ายเงินเช่าซื้อ</li> 
            <li><a href="http://10.10.10.41:8081/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=%2Freports%2FLB%2FOverDueHirePurchase&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">รายงานลูกหนี้เช่าซื้อผิดนัดชำระ</li> 
            <li><a href="http://10.10.10.41:8081/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=%2Freports%2FLB%2FRegisDebtorHirePurchaseCredit&standAlone=true&output=xlsx&decorate=no&j_username=rptusr&j_password=rptpwd">ทะเบียบคุมลูกหนี้เช่าซื้อ</li>                            
        </ul>
    </li>
    <?php }?>   
    
</ul>


 




