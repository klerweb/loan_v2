<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debt extends MY_Controller
{
	private $menu = 'แบบปรับโครงสร้างหนี้';
	private $title = 'แบบปรับโครงสร้างหนี้';
	private $message;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('debt_model');
		$this->load->model('loan/mst_model');
		$this->message = array('200'=> 'บันทึกข้อมูลเรียบร้อย');
	}
	
	public function index()
	{	
		$loan_code = '';
		$loan_contract_no = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan_code = $_POST['txtLoan'];
		if(!empty($_POST['loan_contract_no'])) $loan_contract_no = $_POST['loan_contract_no'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_menu['menu'] = 'debt';
		$data_breadcrumb['menu'] = array($this->menu =>'#');
		
		$data_content = array(
				'loan_code' => $loan_code,
				'loan_contract_no' => $loan_contract_no,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->debt_model->search_debt($loan_code, $loan_contract_no, $thaiid, $fullname)
		);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('debt', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	function form($loan_id='',$loan_contract_id='',$debt_requestdate='',$status='')
	{
		$data_menu['menu'] = 'debt';
		$data_breadcrumb['menu'] = array(
				$this->menu =>'#',
				$this->title => site_url('debt')
			);
			
		$data_content = array(
			'status'=>$status,			
			'message'=>!empty($this->message[$status]) ? $this->message[$status] : '-',
			'loan_id' => $loan_id > 0 ? $loan_id : '',
			'loan_contract_id' => $loan_contract_id > 0 ? $loan_contract_id :'',
			'debt_requestdate' => $debt_requestdate > 0 ? $debt_requestdate :''
		);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	function search()
	{
		$loan_code = '';
		$loan_contract_no = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan_code = $_POST['txtLoan'];
		if(!empty($_POST['loan_contract_no'])) $loan_contract_no = $_POST['loan_contract_no'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_content = array(
				'loan_code' => $loan_code,
				'loan_contract_no' => $loan_contract_no,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->debt_model->search_debt_invoice($loan_code, $loan_contract_no, $thaiid, $fullname)
		);	
		$this->parser->parse('form_list', $data_content);
	}
	
	function form_add($loan_id='',$loan_contract_id='',$debt_requestdate='')
	{		
		$this->load->model('delay/delay_model');
		$this->load->model('extend/extend_model');
		
		$delay_data = $this->delay_model->get_loan_delay($loan_id,$loan_contract_id);		
		$extend_data = $this->extend_model->get_loan_extend($loan_id,$loan_contract_id);
		$payment_data = $this->debt_model->get_payment_schedule($loan_contract_id);
		
		if((count($delay_data) == 2 && count($extend_data) == 3)){
			$this->load->model('person/person_model');
			$this->load->model('province/province_model');
			$this->load->model('amphur/amphur_model');
			$this->load->model('district/district_model');
			
			$loan_data = $this->debt_model->get_loan_by_id($loan_id);
			$person_data = $this->person_model->getById($loan_data['person_id']);
			$loan_type_data = $this->debt_model->get_loan_type_by_loan_id($loan_id);
			$loan_contract_data = $this->debt_model->get_loan_contract_by_loan_id($loan_id);
			$loan_balance_data = $this->debt_model->get_loan_balance_by_loan_contract_id($loan_contract_id);
			$payment_behide_data = $this->debt_model->get_payment_schedule_behide_by_loan_contract_id($loan_contract_id);
			$invoice_data = $this->debt_model->get_loan_invoice_by_loan_contract_id($loan_contract_id,$payment_data[0]['loan_pay_number']);
			$debt_data = $this->debt_model->get_loan_debt_restructuring($loan_id,$loan_contract_id,$debt_requestdate);
			
			$data_content = array(
				'loan_id' => $loan_id,
				'loan_contract_id' => $loan_contract_id,
				'debt_requestdate' => $debt_requestdate,
				'loan_data' => $loan_data,
				'person_data' => $person_data,
				'loan_type_data' => $loan_type_data,
				'loan_contract_data' => $loan_contract_data,
				'loan_balance_data' => $loan_balance_data,
				'payment_data' => $payment_data,
				'payment_behide_data' => $payment_behide_data,
				'invoice_data' => $invoice_data,
				'debt_data' => $debt_data,
				'sex' => $this->mst_model->get_all('sex'),
				'race' => $this->mst_model->get_all('race'),
				'nationality' => $this->mst_model->get_all('nationality'),
				'religion' => $this->mst_model->get_all('religion'),			
				'province_card' => $this->province_model->all(),
				'amphur_card' => $this->amphur_model->getByProvince($person_data['person_addr_card_province_id']),
				'district_card' => $this->district_model->getByAmphur($person_data['person_addr_card_amphur_id']),
				'province_pre' => $this->province_model->all(),
				'amphur_pre' => $this->amphur_model->getByProvince($person_data['person_addr_pre_province_id']),
				'district_pre' => $this->district_model->getByAmphur($person_data['person_addr_pre_amphur_id']),
			);
		
			$this->parser->parse('form_add', $data_content);
		}else{
			redirect_url(base_url().'debt','ขออภัยค่ะ รายการสินเชื่อ ไม่เข้าเงื่อนไขที่จะขอปรับโครงสร้างหนี้');
			exit(0);
		}
	}
	
	function save()
	{		
		$this->db = $this->load->database('default', TRUE);	
		
		list($d, $m, $y) = explode('/', $_POST['debt_date']);		
		$_POST['debt_date'] = (intval($y)-543).'-'.$m.'-'.$d;
		
		$loan_id = $_POST['loan_id'];
		$loan_contract_id = $_POST['loan_contract_id'];
		
		$_POST['debt_amt'] = str_replace(",","",$_POST['debt_amt']);
		$_POST['debt_payamt'] = str_replace(",","",$_POST['debt_payamt']);
		$_POST['loan_amount'] = str_replace(",","",$_POST['loan_amount']);
		$_POST['princle_pay'] = str_replace(",","",$_POST['princle_pay']);
		$_POST['interest_pay'] = str_replace(",","",$_POST['interest_pay']);
		$_POST['behind_number'] = str_replace(",","",$_POST['behind_number']);
		$_POST['behind_money'] = str_replace(",","",$_POST['behind_money']);
						
		$debt_data = array(
				'loan_id'=> $loan_id,
				'loan_contract_id' => $loan_contract_id,
				'debt_requestdate'=> date($this->config->item('log_date_format')),
				'debt_amt'=> $_POST['debt_amt'],
				'debt_interest'=> $_POST['debt_interest'],
				'debt_period'=> $_POST['debt_period'],
				'debt_type_period'=> $_POST['debt_type_period'],
				'debt_payamt'=> $_POST['debt_payamt'],
				'debt_reason'=> $_POST['debt_reason'],
				'createdby'=> get_uid_login(),		
				'createdate'=> date($this->config->item('log_date_format')),
				'updatedby'=> get_uid_login(),	
				'updateddate'=> date($this->config->item('log_date_format')),
				'debt_location'=> $_POST['debt_location'],	
				'debt_date'=> $_POST['debt_date'],	
				'loan_amount'=> $_POST['loan_amount'],		
				'princle_pay'=> $_POST['princle_pay'],		
				'interest_pay'=> $_POST['interest_pay'],		
				'behind_number'=> $_POST['behind_number'],		
				'behind_money'=> $_POST['behind_money'],
		);
		$this->debt_model->save_debt('loan_debt_restructuring',$debt_data);
			
		$payment_data['status'] = 'C';
		$where['loan_contract_id'] = $loan_contract_id;
		$this->debt_model->update_payment_schedule('payment_schedule',$payment_data,$where);
				
		$this->db->query("update loan_contract set loan_contract_status = '5' where loan_id = '$loan_id' ");		
		
		$this->db->query("update loan set loan_status = '5' where loan_id = '$loan_id' ");		
		$data_loan = $this->debt_model->get_data_by_tablename('loan',$loan_id,array('loan_status'=>'1'));
		if(!empty($data_loan)){
			$loan_id_new = $this->debt_model->save('loan',$data_loan);
				
			$this->db->query("update loan_guarantee_land set loan_guarantee_land_status = '5' where loan_id = '$loan_id' ");
			$query_gland = $this->db->query("select loan_guarantee_land_id,land_id from loan_guarantee_land where loan_id = '$loan_id' and loan_guarantee_land_status = '5'");
			if(!empty($query_gland)){
				foreach ($query_gland->result() as $row_gland)
				{			
					   $loan_guarantee_land_id = $row_gland->loan_guarantee_land_id;
				       $land_id = $row_gland->land_id;
				       	       
				       $this->db->query("update land set land_status = '5' where land_id = '$land_id' ");  
				       $data_land = $this->debt_model->get_data_by_tablename('land',$land_id,array('land_status'=>'2'));
				       if(!empty($data_loan)){
							$land_id_new = $this->debt_model->save('land',$data_land);
							
				       		$data_gland = $this->debt_model->get_data_by_tablename('loan_guarantee_land',$loan_guarantee_land_id,array('loan_guarantee_land_status'=>'1','loan_id'=>$loan_id_new,'land_id'=>$land_id_new));
							if(!empty($data_gland)){
								$this->debt_model->save('loan_guarantee_land',$data_gland);
							}
							
							$this->db->query("update land_assess set land_assess_status = '5' where land_id = '$land_id' ");
							$data_land_assess = $this->debt_model->get_data_list_by_tablename('land_assess',"land_id = '$land_id'",array('land_assess_status'=>'1','land_id'=>$land_id_new));
				       		if(!empty($data_land_assess)){
				       			foreach ($data_land_assess as $rows) {
				       				$this->debt_model->save('land_assess',$rows);
				       			}								
				       		}
				       		
				       		$this->db->query("update land_record set land_record_status = '5' where land_id = '$land_id' ");	
				       		$data_land_record = $this->debt_model->get_data_list_by_tablename('land_record',"land_id = '$land_id'",array('land_record_status'=>'1','land_id'=>$land_id_new));
				       		if(!empty($data_land_record)){
				       			foreach ($data_land_record as $rows) {
				       				$this->debt_model->save('land_record',$rows);
				       			}								
				       		}
				       		
				       	    $this->db->query("update building set building_status = '5' where land_id = '$land_id' ");				       	    
				       		$query_building = $this->db->query("select building_id from building where land_id = '$land_id' and building_status = '5'");
							if(!empty($query_building)){
				       			foreach ($query_building->result() as $row_building) {
				       				$building_id = $row_building->building_id;
				       				
				       				$data_building = $this->debt_model->get_data_by_tablename('building',$building_id,array('building_status'=>'2','land_id'=>$land_id_new));
									if(!empty($data_building)){
										$building_id_new = $this->debt_model->save('building',$data_building);
										
										$this->db->query("update building_assess set building_assess_status = '5' where building_id = '$building_id' ");
						       			$data_building_assess = $this->debt_model->get_data_list_by_tablename('building_assess',"building_id = '$building_id'",array('building_assess_status'=>'1','building_id'=>$building_id_new));
							       		if(!empty($data_building_assess)){
							       			foreach ($data_building_assess as $rows) {
							       				$this->debt_model->save('building_assess',$rows);
							       			}								
							       		}
					       				
								        $this->db->query("update building_record set building_record_status = '5' where building_id = '$building_id' ");	
					       				$data_building_record = $this->debt_model->get_data_list_by_tablename('building_record',"building_id = '$building_id'",array('building_record_status'=>'1','building_id'=>$building_id_new));
							       		if(!empty($data_building_record)){
							       			foreach ($data_building_record as $rows) {
							       				$this->debt_model->save('building_record',$rows);
							       			}								
							       		}	
									}
				       			}								
				       		}
				       }
				}
			}				
		}		
		
		redirect(base_url().'debt/form/0/0/0/200');
	}
}