<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debt_model extends CI_Model
{	
	private $table = 'loan_extend';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	function search_debt($loan_code='', $loan_contract_no='', $thaiid='',$fullname='')
	{
		$where = '';
		if($loan_code!='') $where .= "OR loan.loan_code LIKE '%".$loan_code."%' ";
		if($loan_contract_no!='') $where .= "OR loan_contract.loan_contract_no LIKE '%".$loan_contract_no."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("loan.loan_code,config_title.title_name,person.person_fname,person.person_lname,loan_contract.loan_contract_no,
				loan_debt_restructuring.loan_id,loan_debt_restructuring.loan_contract_id,loan_debt_restructuring.debt_requestdate,loan_debt_restructuring.debt_amt");
		$this->db->from("loan_debt_restructuring");
		$this->db->join("loan_contract", "loan_contract.loan_contract_id = loan_debt_restructuring.loan_contract_id", "LEFT");		
		$this->db->join("loan_balance", "loan_balance.loan_contract_id = loan_contract.loan_contract_id", "LEFT");		
		$this->db->join("loan", "loan.loan_id = loan_contract.loan_id", "LEFT");		
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan.loan_status", "5");
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	function search_debt_invoice($loan_code='', $loan_contract_no='', $thaiid='',$fullname='')
	{ 
		$where = '';
		if($loan_code!='') $where .= "OR loan.loan_code LIKE '%".$loan_code."%' ";
		if($loan_contract_no!='') $where .= "OR loan_contract.loan_contract_no LIKE '%".$loan_contract_no."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("loan.loan_id,loan_contract.loan_contract_id,config_title.title_name,person.person_fname,person.person_lname, 
				loan_contract.loan_contract_no,loan_contract.loan_contract_amount,loan_balance.loan_amount,loan_invoice.date_expire_payment");
		$this->db->from("loan_invoice");
		$this->db->join("loan_contract", "loan_contract.loan_contract_id = loan_invoice.loan_contract_id", "LEFT");		
		$this->db->join("loan_balance", "loan_balance.loan_contract_id = loan_contract.loan_contract_id", "LEFT");		
		$this->db->join("loan", "loan.loan_id = loan_contract.loan_id", "LEFT");		
		$this->db->join("person", "loan.person_id = person.person_id", "LEFT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where('loan_invoice.printed', '1');
		$this->db->where('(current_date) - (loan_invoice.date_expire_payment) >', '90');
		$this->db->where("loan.loan_status", "2");
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
		
	function get_loan_by_id($id)
	{
		$this->db->select("*");
        $this->db->from('loan');
        $this->db->where('loan_id', $id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_type_by_loan_id($loan_id)
	{
		$this->db->select("*");
        $this->db->from('loan_type');
        $this->db->join("master_loan_objective", "master_loan_objective.loan_objective_id = loan_type.loan_objective_id", "LEFT");		
        $this->db->where('loan_id', $loan_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_contract_by_loan_id($loan_id)
	{
		$this->db->select("*");
        $this->db->from('loan_contract');
        $this->db->where('loan_id', $loan_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_balance_by_loan_contract_id($loan_contract_id)
	{
		$this->db->select("*");
        $this->db->from('loan_balance');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $query = $this->db->get();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_payment_schedule($loan_contract_id,$date_payout=null){
		$this->db->select("*");
        $this->db->from('payment_schedule');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $this->db->where_in('status', array('N','P'));
        if($date_payout)$this->db->where('date_trunc(\'day\',date_payout) =', $date_payout);
        $this->db->order_by("loan_pay_number", "asc");
        $this->db->order_by("date_payout", "asc");
       // $this->db->limit(1);
        $query = $this->db->get();
        
       // echo $this->db->last_query();

        $result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
	
	function get_payment_schedule_behide_by_loan_contract_id($loan_contract_id){
		$this->db->select("count(*) as sum_behide_number,sum(money_pay) as sum_behide_money");
        $this->db->from('payment_schedule');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $this->db->where('status', 'N');
        $this->db->where('isinvgenerate', '1');
        $this->db->where('date_trunc(\'day\',date_payout) <', date('Y-m-d'));
        $query = $this->db->get();
        
        //echo $this->db->last_query();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_invoice_by_loan_contract_id($loan_contract_id,$loan_pay_number){
		$this->db->select("*");
        $this->db->from('loan_invoice');
        $this->db->where('loan_contract_id', $loan_contract_id);
        $this->db->where('loan_pay_number', $loan_pay_number);
        $query = $this->db->get();
        
      //  echo $this->db->last_query();

        $result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_loan_debt_restructuring($loan_id,$loan_contract_id,$debt_requestdate)
	{
		$this->db->select("*");
        $this->db->from('loan_debt_restructuring');
        $this->db->where('loan_id', $loan_id);
        if($loan_contract_id)$this->db->where('loan_contract_id', $loan_contract_id);
        if($debt_requestdate)$this->db->where('date_trunc(\'day\',debt_requestdate)', $debt_requestdate);
        $this->db->order_by("debt_requestdate", "asc");
        $query = $this->db->get();
		//echo $this->db->last_query();
        $result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
	
	function save_debt($table,$array)
	{
		$this->db->set($array);
		$this->db->insert($table);
		//$id = $this->db->insert_id();		
		$num_row = $this->db->affected_rows();	
		//echo $this->db->last_query();	
		//return array('id' => $id, 'rows' => $num_row);
		return array('rows' => $num_row);
	}
	
	function save($table,$array)
	{
		$this->db->set($array);
		$this->db->insert($table);
		$id = $this->db->insert_id();		
		//$num_row = $this->db->affected_rows();	
		//echo $this->db->last_query();	
		//return array('id' => $id, 'rows' => $num_row);
		//return array('rows' => $num_row);
		return $id;
	}
	
	function update_payment_schedule($table,$array,$where)
	{
		if (!empty($where)){
			foreach ($where as $key => $row) {
				$this->db->where($key, $row);
			}		
			$this->db->update($table, $array);
			//echo $this->db->last_query();
		}
	}
	
	function get_col_by_tablename($table_name)
	{
		$query = $this->db->query("SELECT column_name FROM information_schema.columns WHERE table_name = '$table_name'");
		$result = $query->result_array();
		$cols = array();
		foreach ($result as $row) {
			if($row['column_name'] != $table_name.'_id')$cols[] = $row['column_name'];
		}
		$col = implode(",",$cols);
		return $col;
	}
	
	function get_data_by_tablename($table_name,$table_id,$select_field)
	{
		$col_insert = $this->get_col_by_tablename($table_name);
		$col_select = $col_insert;
		if (!empty($select_field)){
			foreach ($select_field as $field => $value) {
				$col_select = str_replace($field,"'".$value."' as $field",$col_select);
			}
		}
		$query = $this->db->query("select $col_select  from $table_name where {$table_name}_id='$table_id' and {$table_name}_status='5' ");
		$result = $query->num_rows() != 0 ? $query->row_array() : null;
        return $result;
	}
	
	function get_data_list_by_tablename($table_name,$where_id,$select_field)
	{
		$col_insert = $this->get_col_by_tablename($table_name);
		$col_select = $col_insert;
		if (!empty($select_field)){
			foreach ($select_field as $field => $value) {
				$col_select = str_replace($field,"'".$value."' as $field",$col_select);
			}
		}
		$query = $this->db->query("select $col_select  from $table_name where $where_id and {$table_name}_status='5' ");
		$result = $query->num_rows() != 0 ? $query->result_array() : null;
        return $result;
	}
}