<form id="frmReligion" name="frmReligion" action="<?php echo base_url().'religion/save'; ?>" method="post">
	<input type="hidden" id="txtReligionId" name="txtReligionId" value="<?php echo $id; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-religion">ศาสนา</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">ศาสนา <span class="asterisk">*</span></label>
								<input type="text" name="txtReligionName" id="txtReligionName" class="form-control" value="<?php echo $data['religion_name']; ?>" maxlength="100" required />
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>