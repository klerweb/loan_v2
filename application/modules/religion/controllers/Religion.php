<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Religion extends MY_Controller
{
	private $title_page = 'ศาสนา';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('religion_model', 'religion');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->religion->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_religion/religion.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('religion', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('religion')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มศาสนา';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('religion_model', 'religion');
			$data_content['data'] = $this->religion->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มศาสนา';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขศาสนา';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_religion/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('religion_model', 'religion');
		
		$array['religion_name'] = $this->input->post('txtReligionName');
		$array['religion_status'] = '1';
		
		$id = $this->religion->save($array, $this->input->post('txtReligionId'));
		
		if($this->input->post('txtReligionId')=='')
		{
			$data = array(
					'alert' => 'บันทึกศาสนาเรียบร้อยแล้ว',
					'link' => site_url('religion'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขศาสนาเรียบร้อยแล้ว',
					'link' => site_url('religion'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('religion_model', 'religion');
		$this->religion->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกศาสนาเรียบร้อยแล้ว',
				'link' => site_url('religion'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['religion_name'] = '';
		
		return $data;
	}
}