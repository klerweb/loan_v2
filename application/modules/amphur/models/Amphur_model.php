<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amphur_model extends CI_Model
{
	private $table = 'master_amphur';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByProvince($province)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("province_id", $province);
		$this->db->order_by("amphur_name", "asc");
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("amphur_id", $id);
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function get_by_id($id){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("amphur_id",$id);
		$query = $this->db->get();
		
		$result = $query->row_array();
		
		return $result;
	}
}
?>