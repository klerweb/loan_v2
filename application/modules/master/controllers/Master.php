<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function person_search()
    {
        $this->load->model('person/person_model', 'person');

        $q = $this->input->get('q');
        $data = $this->person->search($q);

        echo json_encode($data);
    }

    public function amphur($province='')
    {
        $data = $this->input->get();
        if ($province=='') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');

            $data['amphur'] = $this->amphur->getByProvince($province);
        }

        $this->parser->parse('amphur', $data);
    }

    public function district($amphur='')
    {
        $data = $this->input->get();
        if ($amphur=='') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');

            $data['district'] = $this->district->getByAmphur($amphur);
        }

        $this->parser->parse('district', $data);
    }
		public function zip($district='')
		{
				$data = $this->input->get();
				if ($district=='') {
						$data['zip'] = '';
				} else {
						$this->load->model('district/district_model', 'district');
						$data['zip'] = $this->district->getZip($district);
				}

				$this->parser->parse('zip', $data);
		}

}
