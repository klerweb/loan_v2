<?php $selected = isset($selected)?$selected:''; ?>
<select name="<?php echo $name ?>" id="<?php echo $id ?>" data-placeholder="กรุณาเลือก" class="width100p" required>
	<option value="">กรุณาเลือก</option>
	<?php foreach ($district as $district_data) {
    ?>
		<option value="<?php echo $district_data['district_id']; ?>" <?php if ($selected == $district_data['district_id']): ?>selected="selected"<?php endif; ?>><?php echo $district_data['district_name']; ?></option>
	<?php
} ?>
</select>
<script type="text/javascript">

	$("#<?php echo $id ?>").select2({
			minimumResultsForSearch: -1
	});
	<?php if (isset($zip_id)): ?>
	$("#<?php echo $id ?>").change(function(){
		$.ajax({
		  type: 'GET',
		  url: base_url+"master/zip/"+$("#<?php echo $id ?>").val(),
		  async: false,
		  success: function (data) {
		    $('#<?php echo $zip_id ?>').val(data);
		  }
		});
	});
	<?php endif; ?>
</script>
