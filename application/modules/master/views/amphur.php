<?php $selected = isset($selected)?$selected:''; ?>

<select name="<?php echo $name ?>" id="<?php echo $id ?>" data-placeholder="กรุณาเลือก" class="width100p" required>
	<option value="">กรุณาเลือก</option>
	<?php foreach ($amphur as $amphur_data) { ?>
		<option value="<?php echo $amphur_data['amphur_id']; ?>" <?php if ($selected == $amphur_data['amphur_id']): ?>selected="selected"<?php endif; ?>><?php echo $amphur_data['amphur_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#<?php echo $id ?>").select2({
		minimumResultsForSearch: -1
	});

	$("#<?php echo $id ?>").change(function(){
		$("#<?php echo $target ?>").load(base_url+"master/district/"+$("#<?php echo $id ?>").val() +
		 "?name=<?php echo $target_name ?>&id=<?php echo $target_id ?>&zip_id=<?php echo $zip_id ?>" );
	});
</script>
