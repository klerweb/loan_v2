<?php
class report_debtor extends MY_Controller{
	private $title_page = 'รายงานการ์ดลูกหนี้';
	function __construct()
	{
		parent::__construct();
	}



	public function index()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงาน' =>'#');
		$data_content = array(
				
				'report_card_debtor' => $this->report_card_debtor_m->load_card_debtor()
				
		);
		
		$data = array(
				'title' => $this->title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('load_card_debtor', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_all_loan_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_loan_payment=$this->report_card_debtor_m->search_payment();
		echo $data_loan_payment;
	}
	public function receipt_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data['show_receipt_payment']=$this->report_card_debtor_m->show_receipt_payment($this->uri->segment(3));
		$this->load->view('receipt_payment_print',$data);
	}
	public function payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		
		$data_content = array(
				'bank_payment' => $this->report_card_debtor_m>load_bank_payment_active(),
				'loan_wait_payment'=>$this->report_card_debtor_m->show_w_payment()
		);
		$title_page="ใบแจ้งหนี้ที่ยังไม่มีการชำะระเงิน";
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('show_wait_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function show_one_person_loan_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		$data_content = array(
				'loan_one_payment' => $this->report_card_debtor_m->loan_one_payment($this->uri->segment(3))
		);
		
		$data = array(
				'title' => $this->title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('show_one_person_loan_payments', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	} 
	
	public function edit_receive_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		$data_content = array(
				'bank_payment' => $this->report_card_debtor_m->load_bank_payment_active(),
				'load_payment'=>$this->report_card_debtor_m->loan_one_payment($this->uri->segment(3))
				
		);
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		$title_page = 'ข้อมูลการรับชำระหนี้เงินกู้';
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('form_edit_receive_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function save_receive_payment() //ติดค้างอยู่
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$this->title_page="ใบแจ้งหนี้ที่ยังไม่ได้รับชำระเงินกู้";
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment/payment'));
		$data_content = array(
				'bank_payment' => $this->report_card_debtor_m->load_bank_payment_active(),
				'load_payment'=>$this->report_card_debtor_m->load_data_invoice($this->uri->segment(3))
		);
		
		$title_page = 'การรับชำระหนี้เงินกู้';
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('receive_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function show_cal_date()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงาน' =>'#',$this->title_page => site_url('Report_debtor'));
		$data_content = array(
				
		);
		$title_page = 'ประมาณการลูกหนี้';
		$data = array(
				'title' => $title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				'js_other' => array('modules_finance_disburse/jquery.table2excel.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('1', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function estimate_card_debtors()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงาน' =>'#',$this->title_page => site_url('Report_debtor'));
		$data_content = array(
				'load_debtor' => $this->report_card_debtor_m->show_c_debtor_estimate($this->uri->segment(3))
		);
		$title_page = 'ประมาณการลูกหนี้';
		$data = array(
				'title' => $title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				'js_other' => array('modules_finance_disburse/jquery.table2excel.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('show_card_debtor_estimate', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function real_card_debtors()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงาน' =>'#',$this->title_page => site_url('Report_debtor'));
		$data_content = array(
				'load_debtor' => $this->report_card_debtor_m->show_c_debtor($this->uri->segment(3))
		);
		$title_page = 'การ์ดลูกหนี้';
		$data = array(
				'title' => $title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				'js_other' => array('modules_finance_disburse/jquery.table2excel.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('show_card_debtor_real', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function check_bank_payment_active()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_bank=$this->report_card_debtor_m->load_bank_payment_active();
		echo $data_bank;

	}
	public function check_bank_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_bank=$this->report_card_debtor_m->all_bank_payment();
		echo $data_bank;
		
	}
	public function delete_loan_payment()
	{
		$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_delete_disburse=$this->report_card_debtor_m->delete_l_payment();
		echo $status_delete_disburse;
		
	}
	public function save_loan_pay()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_save_disburse=$this->report_card_debtor_m->save_l_pay();
		echo $status_save_disburse;
	}
	public function confirm_loan_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_confirm_disburse=$this->report_card_debtor_m->confirm_l_pay();
		echo $status_confirm_disburse;
	}
	public function search_card_debtor()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงาน' =>'#',$this->title_page => site_url('Report_debtor'));
		$data_content = array(
				'report_card_debtor' => $this->report_card_debtor_m->search_l_payment()
		);
		
		$data = array(
				'title' => $this->title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('load_card_debtor', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_wait_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'report_m'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('รายงาน' =>'#',$this->title_page => site_url('Report_debtor'));
		$data_content = array(
				'bank_payment' => $this->loan_payment_m->load_bank_payment_active(),
				'loan_wait_payment'=>$this->loan_payment_m->search_w_payment()
		);
		$title_page="ใบแจ้งหนี้ที่ยังไม่มีการชำะระเงิน";
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('show_wait_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function edit_loan_payment()
	{
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_edit_payment=$this->loan_payment_m->edit_l_payment();
		echo $status_edit_payment;
	}
	
}
?>