<?php
class report_card_debtor_model extends CI_Model{
function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}

	function load_bank_payment()
	{
		$query_bank_payment=$this->db->get('bank_loan_payment');
		return $query_bank_payment->result();
	}
	function search_l_payment()
	{
		$data="";
		if($_POST['id_card_search']!="")
		{
			$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->check_id_card_l($_POST['id_card_search']);
		}
		if($_POST['name_search']!="")
		{
			$split_name=explode(" ",$_POST['name_search']);
			if (count($split_name)>2)
			{
				$split_name[1]=$split_name[1]." ".$split_name[2];
				$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->search_name_in_person($split_name[0],$split_name[1]);
			}
			else if (count($split_name)==1)
			{
				$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->search_name1_in_person($_POST['name_search']);
			}
			else
			{
				$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			$data=$data.$this->report_card_debtor_m->search_name_in_person($split_name[0],$split_name[1]);
			}
		}
		
		if($_POST['contact_id']!="")
		{
			$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->search_contract_no1($_POST['contact_id']);
		}
		return $data;
		
	}
	function search_contract_no($invoice_no)
	{	
		
		$data_loan_pay="";
		$sqlit_data=explode("\\",$invoice_no);
		for($i=0;$i< count($sqlit_data);$i++)
		{	
				$split_data2=explode(",",$sqlit_data[$i]);
				$this->db->where('invoice_no',$split_data2[0]);
				$query_data_loan_payment=$this->db->get('loan_payment');
				if($query_data_loan_payment->num_rows()>0)
				{
					foreach($query_data_loan_payment->result() as $data_loan_payment)
					{
						$this->db->where('loan_id',$split_data2[8]);
						$query_data_loan=$this->db->get('loan');
						$data_loan=$query_data_loan->result();
						$this->db->where('person_id',$split_data2[10]);
						$query_data_person=$this->db->get('person');
						$data_person=$query_data_person->result();
						$this->db->where('title_id',$data_person[0]->title_id);
						$query_data_title=$this->db->get('config_title');
						$data_title=$query_data_title->result();
						$this->db->where('loan_contract_id',$split_data2[9]);
						$query_data_loan_balance=$this->db->get('loan_balance');
						$data_loan_balance=$query_data_loan_balance->result();
						//-----------------------
						$data_loan_pay=$data_loan_pay.$data_loan_payment->payment_no.",".$split_data2[1].",".$split_data2[7].",".$split_data2[6].",".$split_data2[2].",".$split_data2[3].",".$data_loan_payment->com_code.",".$split_data2[4].",".$split_data2[5].",".$split_data2[6].",".$data_loan_payment->money_transfer.",".$data_loan_payment->date_transfer."\\";
					}
		}
		}
		
		if ($data_loan_pay!="")
		{
			return $data_loan_pay;
		}
		else
		{
			return "no data";
		}
}
function search_contract_no1($invoice_no)
	{	
		$data_array_loan_payment="";
		$this->db->where('loan_contract_no',$invoice_no);
		$query_data_loan_contract=$this->db->get('loan_contract');
		if($query_data_loan_contract->num_rows()>0)
		{
			$data_loan_contract=$query_data_loan_contract->result();
			$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
			$query_data_loan=$this->db->get('loan');
			$data_loan=$query_data_loan->result();

			$this->db->where('person_id',$data_loan[0]->person_id);
			$query_data_person=$this->db->get('person');
			$data_person=$query_data_person->result();
			$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			return $this->report_card_debtor_m->check_id_card_l($data_person[0]->person_thaiid);
			}
			else
			{
				return "no data";
			}
}
function search_name_in_person($name,$lname)
{
	$data_person_id="";
	$check_state=array('person_fname'=>$name,'person_lname'=>$lname);
	
	$this->db->where($check_state);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person_id);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		return $this->report_card_debtor_m->check_id_card_l($data_person1);
	}
	else
	{
		return "no data";
	}
}
function search_name1_in_person($name)
{
	$data_person_id="";
	$this->db->where('person_fname',$name);
	$this->db->or_where('person_lname',$name);
	$query_data_person=$this->db->get('person');
	
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person_id);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		
		$this->load->model('report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		return $this->report_card_debtor_m->check_id_card_l($data_person1);
		
	}
	else
	{
		return "no data";
	}
}
function check_id_card_l($id_card)
{
	
	$data_array_loan_payment="";
	$data_invoice="";
	$data_loan_connect="";
	$split_data=explode(",",$id_card);
	
	for($i=0;$i<count($split_data);$i++)
	{
		$this->db->where('person_thaiid',$split_data[$i]);
		$query_data_person=$this->db->get('person');
		if($query_data_person->num_rows()>0)
		{
			foreach($query_data_person->result() as $data_person)
			{
				$this->db->where('person_id',$data_person->person_id);
				$query_data_loan=$this->db->get('loan');
				
				if($query_data_loan->num_rows()>0)
				{
					foreach($query_data_loan->result() as $data_loan)
					{
						$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no!='=>null);
						$this->db->where($check_data);
						$query_data_loan_contract=$this->db->get('loan_contract');
						if($query_data_loan_contract->num_rows()>0)
						{
							foreach($query_data_loan_contract->result() as $data_loan_contract)
							{
								$this->db->where('loan_contract_no',$data_loan_contract->loan_contract_no);
								$query_data_loan_contract=$this->db->get('loan_contract');
								if($query_data_loan_contract->num_rows()>0)
								{
									$this->db->order_by('invoice_no','asc');
									$this->db->where('loan_contract_id',$data_loan_contract->loan_contract_id);
									$query_data_loan_invoice=$this->db->get('loan_invoice');
									
									if($query_data_loan_invoice->num_rows()>0)
									{
										foreach($query_data_loan_invoice->result() as $data_loan_invoice)
										{
											$this->db->where('loan_contract_id',$data_loan_contract->loan_contract_id);
											$query_data_loan_balance=$this->db->get('loan_balance');
											$data_loan_balance=$query_data_loan_balance->result();
											$this->db->where('title_id',$data_person->title_id);
											$query_data_title=$this->db->get('config_title');
											$data_title=$query_data_title->result();
											$this->db->where('loan_contract_id',$data_loan_contract->loan_contract_id);
											$query_data_loan_balance=$this->db->get('loan_balance');
											$data_loan_balance=$query_data_loan_balance->result();
											$this->db->where('invoice_no',$data_loan_invoice->invoice_no);
											$query_data_loan_payment=$this->db->get('loan_payment');
											if($query_data_loan_payment->num_rows()>0)
											{
													$have_pay=1;
											}
											else
											{
												$have_pay=0;
											}
											$query_data_last_payment=$this->db->query("select date_payout from payment_schedule where (status='N' or status='D' or status='P' or status='CL') AND loan_contract_id=$data_loan_contract->loan_contract_id order by loan_pay_number desc limit 1");
											$data_last_payment=$query_data_last_payment->result();
											$this->db->where('title_id',$data_person->title_id);
											$query_title=$this->db->get('config_title');
											$data_title=$query_title->result();
											$data_array_loan_payment=$data_array_loan_payment.$data_loan_contract->loan_contract_no.",".$data_title[0]->title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_contract->loan_contract_amount.",".$data_loan_contract->loan_contract_date.",".$data_last_payment[0]->date_payout.",".$have_pay.",".$data_loan_contract->loan_contract_id."\\";
										}
										
									}
								}

							}
						}

					}
					
				}
			}
			
		}
	}
	return $data_array_loan_payment;	
}
function show_c_debtor($loan_contract_id)
{
	$data_l_payment_schedule="";
	$this->db->where('loan_contract_id',$loan_contract_id);
	$query_db_get_loan_contract_no=$this->db->get('loan_contract');
	$db_get_loan_contract_no=$query_db_get_loan_contract_no->result();
	$this->db->where('loan_contract_no',$db_get_loan_contract_no[0]->loan_contract_no);
	$query_data_loan_contract=$this->db->get('loan_contract');
	$data_loan_contract=$query_data_loan_contract->result();
	$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
	$query_data_loan_type_objective=$this->db->get('loan_type');
	$data_loan_type_id=$query_data_loan_type_objective->result();
	$data_loan_type_name="";
		if($data_loan_type_id[0]->loan_objective_id==1)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="เพื่อไถ่ถอนการจำนอง";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="เพื่อไถ่ถอนที่ดินจากการขายฝาก";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==2)
		{
			$data_loan_type_name="เพื่อชำระหนี้ตามสัญญาเงินกู้";
		}
		else if($data_loan_type_id[0]->loan_objective_id==3)
		{
			$data_loan_type_name="เพื่อชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน";
		}
		else if($data_loan_type_id[0]->loan_objective_id==4)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="เพื่อซื้อที่ดินที่หลุดขายฝากไปแล้ว ไม่เกิน 5 ปี";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="เพื่อซื้อที่ดินที่ถูกขายทอดตลาด";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==5)
		{
			$data_loan_type_name="เพื่อการประกอบอาชีพเกษตรกรรม";
		}
	
	$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
	$query_data_loan=$this->db->get('loan');
	$data_loan=$query_data_loan->result();
	$this->db->where('person_id',$data_loan[0]->person_id);
	$query_data_person=$this->db->get('person');
	$data_person=$query_data_person->result();
	$this->db->where('title_id',$data_person[0]->title_id);
	$query_data_title=$this->db->get('config_title');
	$data_title=$query_data_title->result();
	$string=$data_loan_contract[0]->loan_contract_id;
	$query_max_loan_pay_number=$this->db->query("SELECT DISTINCT loan_pay_number FROM payment_schedule where loan_contract_id='$string'");
	$max_loan_pay_number=$query_max_loan_pay_number->result();
	
	if($query_max_loan_pay_number->num_rows()>0)
	{
		for($i=1;$i<=$query_max_loan_pay_number->num_rows();$i++)
		{
			$query_data_payment_schedule=$this->db->query("select * from payment_schedule where (status='P' or status='D') and loan_contract_id=$string and loan_pay_number=$i");
			if($query_data_payment_schedule->num_rows()>0)
			{
				
				$data_loan_payment_sc=$query_data_payment_schedule->result();
				$m_pay=0;
				$check_loan_invoice=array('loan_contract_id'=>$string,'loan_pay_number'=>$i);
				$this->db->where($check_loan_invoice);
				$query_data_loan_invoice=$this->db->get('loan_invoice');
				
				if($query_data_loan_invoice->num_rows()>0)
				{
					$data_loan_invoice=$query_data_loan_invoice->result();
					$invoice_number=$data_loan_invoice[0]->invoice_no;
					$this->db->where('invoice_no',$invoice_number);
							$query_data_loan_payment= $this->db->get('loan_payment');
							if($query_data_loan_payment->num_rows()>0)
							{
								foreach($query_data_loan_payment->result() as $data_pay)
								{
									$data_l_payment_schedule=$data_l_payment_schedule.$i.",".$data_loan_payment_sc[0]->date_payout.",".$data_loan_payment_sc[0]->money_pay.",".$data_loan_payment_sc[0]->interest_rate.",".$data_pay->principle.",".$data_pay->interest.",".$data_pay->loan_amount_balance.",".$data_pay->interest_accrued."\\";
								}
							}
							
				}
				
			}
		}
		$query_date_last_payout=$this->db->query("select date_payout from payment_schedule where (status='CL' or status='N' or status='D' or status='P') AND loan_contract_id=$loan_contract_id AND loan_pay_number=$query_max_loan_pay_number->num_rows");
		$date_last_payout=$query_date_last_payout->result();
		$data_l_payment_schedule=$query_max_loan_pay_number->num_rows().",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_loan_contract[0]->loan_id.",".$data_loan_type_name.",".$db_get_loan_contract_no[0]->loan_contract_no.",".$data_loan_contract[0]->loan_contract_date.",".$data_loan_contract[0]->loan_contract_amount.","."3.00%".",".$date_last_payout[0]->date_payout."/*/".$data_l_payment_schedule;
		return $data_l_payment_schedule;
	}	
}
function show_c_debtor_estimate($loan_contract_id)
{
	$data_l_payment_schedule="";
	$this->db->where('loan_contract_id',$loan_contract_id);
	$query_db_get_loan_contract_no=$this->db->get('loan_contract');
	$db_get_loan_contract_no=$query_db_get_loan_contract_no->result();
	$this->db->where('loan_contract_no',$db_get_loan_contract_no[0]->loan_contract_no);
	$query_data_loan_contract=$this->db->get('loan_contract');
	$data_loan_contract=$query_data_loan_contract->result();
	$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
	$query_data_loan_type_objective=$this->db->get('loan_type');
	$data_loan_type_id=$query_data_loan_type_objective->result();
	$data_loan_type_name="";
		if($data_loan_type_id[0]->loan_objective_id==1)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="เพื่อไถ่ถอนการจำนอง";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="เพื่อไถ่ถอนที่ดินจากการขายฝาก";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==2)
		{
			$data_loan_type_name="เพื่อชำระหนี้ตามสัญญาเงินกู้";
		}
		else if($data_loan_type_id[0]->loan_objective_id==3)
		{
			$data_loan_type_name="เพื่อชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน";
		}
		else if($data_loan_type_id[0]->loan_objective_id==4)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="เพื่อซื้อที่ดินที่หลุดขายฝากไปแล้ว ไม่เกิน 5 ปี";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="เพื่อซื้อที่ดินที่ถูกขายทอดตลาด";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==5)
		{
			$data_loan_type_name="เพื่อการประกอบอาชีพเกษตรกรรม";
		}
	
	$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
	$query_data_loan=$this->db->get('loan');
	$data_loan=$query_data_loan->result();
	$this->db->where('person_id',$data_loan[0]->person_id);
	$query_data_person=$this->db->get('person');
	$data_person=$query_data_person->result();
	$this->db->where('title_id',$data_person[0]->title_id);
	$query_data_title=$this->db->get('config_title');
	$data_title=$query_data_title->result();
	$string=$data_loan_contract[0]->loan_contract_id;
	$query_max_loan_pay_number=$this->db->query("SELECT DISTINCT loan_pay_number FROM payment_schedule where loan_contract_id='$string' order by loan_pay_number asc");
	if($query_max_loan_pay_number->num_rows()>0)
	{
		for($i=1;$i<=$query_max_loan_pay_number->num_rows();$i++)
		{
			$query_data_payment_schedule=$this->db->query("select * from payment_schedule where (status='P' or status='N' or status='D' or status='CL') and loan_contract_id='$string' and loan_pay_number='$i'");
			$data_payment_schedule=$query_data_payment_schedule->result();
			$m_pay=0;
			$data_l_payment_schedule=$data_l_payment_schedule.$i.",".$data_payment_schedule[0]->date_payout.",".$data_payment_schedule[0]->money_pay.",".$data_payment_schedule[0]->interest_rate."\\";
		}
		$data_l_payment_schedule=$query_max_loan_pay_number->num_rows().",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_loan_contract[0]->loan_id.",".$data_loan_type_name.",".$db_get_loan_contract_no[0]->loan_contract_no.",".$data_loan_contract[0]->loan_contract_date.",".$data_loan_contract[0]->loan_contract_amount.","."3.00%"."/*/".$data_l_payment_schedule;
		return $data_l_payment_schedule;
	}
	
}

	function count_page_loan_payment()
	{
		$query = $this->db->get('loan_payment');
		if ($query->num_rows()>0)
		{
			
			$page=floor($query->num_rows()/10);
			
			if ($query->num_rows()%9>0)
			{
				$page=$page+1;
			}
			return $page;
		}
		else
		{
			return "no data";
		}
	}
	function loan_one_payment($data_id_payment)
	{
		$data_payment="";
		$data_array_loan_payment="";
		$this->db->where('payment_no',$data_id_payment);
		$query_loan_payment = $this->db->get('loan_payment');
		if ($query_loan_payment->num_rows()>0)
		{
			foreach($query_loan_payment->result() as $data_loan_payment)
			{
				$this->db->where('invoice_no',$data_loan_payment->invoice_no);
				$query_loan_invoice=$this->db->get('loan_invoice');
				foreach($query_loan_invoice->result() as $data_loan_invoice)
				{
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_loan_contact=$this->db->get('loan_contract');
					foreach ($query_loan_contact->result() as $data_loan_contact)
					{
						$this->db->where('loan_contract_id',$data_loan_contact->loan_contract_id);
						$query_loan_balance=$this->db->get('loan_balance');
						
						foreach($query_loan_balance->result() as $data_loan_balance)
						{
						$this->db->where('loan_id',$data_loan_contact->loan_id);
						$query_loan=$this->db->get('loan');
						
						foreach ($query_loan->result() as $data_loan)
						{
							$this->db->where('person_id',$data_loan->person_id);
							$query_person=$this->db->get('person');
							
							foreach($query_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_title=$this->db->get('config_title');
								
								foreach($query_title->result() as $data_title)
								{
									$data_title_name=$data_title->title_name;
								}
								$data_payment=$data_payment.$data_loan_payment->payment_no.",".$data_loan_payment->invoice_no.",".$data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_balance->loan_amount.",".$data_loan_invoice->loan_pay_number.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_payment->com_code.",".$data_loan_payment->money_transfer.",".$data_loan_payment->date_transfer;

							}
						}
						}
					}
			}
			
			
		}
		return $data_payment;
		}
		else
		{
			return "no data";
		}
	}
	function load_card_debtor()
	{
		$data_array_loan_payment="";
		$data_title_name="";
		$have_pay="";
		$this->db->where('loan_contract_no!=',null);
		$this->db->order_by('loan_contract_id','DESC');
		$query_loan_contract=$this->db->get('loan_contract');
		if($query_loan_contract->num_rows()>0)
		{
			foreach($query_loan_contract->result() as $data_loan_contract)
			{
				$query_data_loan_payment=$this->db->query("select * from loan_invoice where loan_contract_id=$data_loan_contract->loan_contract_id and (loan_invoice.invoice_no IN (select invoice_no from loan_payment))");
				if($query_data_loan_payment->num_rows()>0)
				{
					$have_pay=1;
				}
				else
				{
					$have_pay=0;
				}
				$query_data_last_payment=$this->db->query("select date_payout from payment_schedule where (status='N' or status='D' or status='P' or status='CL') AND loan_contract_id=$data_loan_contract->loan_contract_id order by loan_pay_number desc limit 1");
				$data_last_payment=$query_data_last_payment->result();
				$this->db->where('loan_id',$data_loan_contract->loan_id);
				$query_loan=$this->db->get('loan');
				$data_loan_person_id=$query_loan->result();
				$this->db->where('person_id',$data_loan_person_id[0]->person_id);
				$query_person=$this->db->get('person');
				$data_person=$query_person->result();
				$this->db->where('title_id',$data_person[0]->title_id);
				$query_title=$this->db->get('config_title');
				$data_title=$query_title->result();
				$data_array_loan_payment=$data_array_loan_payment.$data_loan_contract->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_loan_contract->loan_contract_amount.",".$data_loan_contract->loan_contract_date.",".$data_last_payment[0]->date_payout.",".$have_pay.",".$data_loan_contract->loan_contract_id."\\";
			}
			return $data_array_loan_payment;
		}
		else
		{
			return "no data";
		}
	}
	function check_loan_payment()
	{
		
		$data_title_name="";
	  	$this->db->where("invoice_no", $_POST['id_invoice']);
		$query=$this->db->get('loan_invoice');
		if ($query->num_rows() > 0 )
        { 
			
			foreach($query->result() as $data_invoice)
			{
				$this->db->where('loan_contract_id',$data_invoice->loan_contract_id);
				$query_loan_contact=$this->db->get('loan_contract');
				
				foreach($query_loan_contact->result() as $data_loan_contact)
				{
				$this->db->where("loan_id",$data_loan_contact->loan_id );
				$query1=$this->db->get('loan');
				
				foreach($query1->result() as $data_loan)
				{
					$this->db->where("person_id",$data_loan->person_id);
					$query2=$this->db->get('person');
					
					foreach($query2->result() as $data_person)
					{
						$this->db->where('title_id',$data_person->title_id);
						$query_get_title_name=$this->db->get('config_title');
						foreach($query_get_title_name->result() as $data_get_title_name)
						{
							$data_title_name=$data_get_title_name->title_name;
						}
						$this->db->where("loan_contract_id",$data_invoice->loan_contract_id);
						$query3=$this->db->get('loan_balance');

						foreach($query3->result() as $data_loan_banlance)
						{
							$this->db->where("invoice_no",$_POST['id_invoice']);
							$query4=$this->db->get('loan_payment');
							if($query4->num_rows()>0)
							{
								foreach($query4->result() as $data_payment)
								{
									$My_Multiple_Statements_Array = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'loan_pay_number' => $data_invoice->loan_pay_number);
									$this->db->where($My_Multiple_Statements_Array);
									$query_data_payment_schedule=$this->db->get('payment_schedule');
									
									foreach($query_data_payment_schedule->result() as $data_payment_schedule)
									{
										$this->db->select_sum('money_transfer', 'Amount_money_tran');
										$this->db->where("invoice_no",$_POST['id_invoice']);
										$query_sum_money=$this->db->get('loan_payment'); //รวมเงินที่จ่ายมาแต่ละบิล
										$result_sum_money=$query_sum_money->result();
										$My_Multiple_Statements_Array1 = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'status' => "C");
									$this->db->where($My_Multiple_Statements_Array1);
									$query_data_payment_status=$this->db->get('payment_schedule');
									if ($query_data_payment_status->num_rows()>0)
									{
									echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.",".$data_payment->com_code.",".$data_payment->money_transfer.",".$data_payment->date_transfer.","."ยกเลิก".",".$result_sum_money[0]->Amount_money_tran.",".$data_loan_banlance->status_loan."\\"; //แจ้งการเรียกเก็บเงินกรณีบิลยกเลิกแล้ว จะไม่สามารถจ่ายเงินได้อีก
									}
									else if ($query_data_payment_status->num_rows()==0)
									{
											echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.",".$data_payment->com_code.",".$data_payment->money_transfer.",".$data_payment->date_transfer.","."ปกติ".",".$result_sum_money[0]->Amount_money_tran.",".$data_loan_banlance->status_loan."\\"; // กรณียังสถานะของบิลอื่นยังเป็นปกติ
									}
									}
								}
							}
							else
							{
								$My_Multiple_Statements_Array = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'loan_pay_number' => $data_invoice->loan_pay_number);
									$this->db->where($My_Multiple_Statements_Array);
									$query_data_payment_schedule=$this->db->get('payment_schedule');
									
									foreach($query_data_payment_schedule->result() as $data_payment_schedule)
									{
										$My_Multiple_Statements_Array1 = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'status' => "C");
									$this->db->where($My_Multiple_Statements_Array1);
									$query_data_payment_status=$this->db->get('payment_schedule');
									if ($query_data_payment_status->num_rows()>0)
									{
										echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.","." ".","." ".","." ".","."ยกเลิก".","."0".",".$data_loan_banlance->status_loan."\\";  //แจ้งการเรียกเก็บเงินกรณีบิลยกเลิกแล้ว จะไม่สามารถจ่ายเงินได้อีก
									}
									else if($query_data_payment_status->num_rows()==0)
									{
										echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.","." ".","." ".","." ".","."ปกติ".","."0".",".$data_loan_banlance->status_loan."\\"; // กรณียังสถานะของบิลอื่นยังเป็นปกติ
									}
								
									}
							}
						}
					}
				}
				}
				
			}
		}
		else
		{
			echo "ไม่มีเลขใบแจ้งหนี้ที่ต้องการค้นหา";
		}
	}
	function load_bank_payment_active()
	{
		$data_bank_show="";
		$this->db->where("status_com_code","1");
		$query=$this->db->get('bank_loan_payment');
		if($query->num_rows()>0)
		{
			foreach($query->result() as $bank_active)
			{
				$this->db->where('bank_id',$bank_active->bank_id);
				$query_bank_name=$this->db->get('config_bank');
				foreach($query_bank_name->result() as $data_bank_name)
				{
					$data_bank_show=$data_bank_show.$bank_active->com_code.",".$bank_active->account_name.",".$data_bank_name->bank_name."\\";
				}
			}
			return $data_bank_show;
		}
		else
		{
			return "ไม่มี";
		}
	
		
	}
	function all_bank_payment()
	{
		$data_bank_show="";
		$query=$this->db->get('bank_loan_payment');
		if($query->num_rows()>0)
		{
			foreach($query->result() as $bank_active)
			{
				$this->db->where('bank_id',$bank_active->bank_id);
				$query_bank_name=$this->db->get('config_bank');
				foreach($query_bank_name->result() as $data_bank_name)
				{
					$data_bank_show=$data_bank_show.$bank_active->com_code.",".$bank_active->account_name.",".$data_bank_name->bank_name."\\";
				}
			}
			return $data_bank_show;
		}
		else
		{
			return "ไม่มี";
		}
	}
	function delete_l_payment()
	{
		if($this->db->delete("loan_payment","invoice_id=".$_POST['id_invoice']))
		{
				//return "success";
				$data = array('status_pay'=>0);
				$this->db->where("invoice_id",$_POST['id_invoice']);
				if ($this->db->update("loan_invoice",$data))
				{
					return "success";
				}
				else
				{	
					return "fail";
				}
		}
		else
		{	
			return "fail";
		}
	}
	function save_l_pay()
	{
		$this->db->order_by('payment_id','asc');
		$this->db->limit(0);
		$query = $this->db->get('loan_payment');
		$id_new;
		if ($query->num_rows()>0)
		{
			foreach($query->result() as $id_pay)
			{
					$id_new=($id_pay->payment_id)+1;
			}
		}
		else
		{
			$id_new=1;
		}
		$date_tran=explode("/",$_POST['date_tranfer']);
				$data_tran_u=(intval($date_tran[2])-543)."-".$date_tran[1]."-".$date_tran[0];
				$date_pay_check=$date_tran[2].$date_tran[1];
					$query_data_count_pay_no=$this->db->query("select payment_no from loan_payment where cast(payment_no as varchar(20)) like '%$date_pay_check%' order by payment_no desc limit 1");
					if($query_data_count_pay_no->num_rows()>0)
					{
						$data_count_pay_no=$query_data_count_pay_no->result();
						$new_pay_no=($data_count_pay_no[0]->payment_no)+1;
					}
					else
					{
						$new_pay_no=$date_pay_check."001";
					}
				
		
		$data_insert_payment=array(
			'payment_id'=>$id_new,
			'invoice_no'=>$_POST['invoice_no'],
			'com_code'=>$_POST['bank'],
			'money_transfer'=>$_POST['money_transfer'],
			'date_transfer'=>$data_tran_u,
			'createddate'=>date("Y-m-d"),
			'updatedate'=>date("Y-m-d"),
			'payment_no'=>$new_pay_no
		);
		$this->db->where('invoice_no',$_POST['invoice_no']);
		$query_data_loan_invoice=$this->db->get('loan_invoice');
		
		$data_loan_invoice=$query_data_loan_invoice->result();
		$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
		$query_data_loan_balance=$this->db->get('loan_balance');
		
		$data_loan_balance=$query_data_loan_balance->result();
		$data_loan_amount=$data_loan_balance[0]->loan_amount;
		$data_loan_principle=$data_loan_invoice[0]->principle;
		$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
		$query_data_loan_contact=$this->db->get('loan_contract');
		$data_loan_contract=$query_data_loan_contact->result();
		$My_Multiple_load_status_payment = array('loan_contract_id'=> $data_loan_invoice[0]->loan_contract_id, 'loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number,'status'=>'P');
		$this->db->where($My_Multiple_load_status_payment);
		$query_status_payment=$this->db->get('payment_schedule');
		
		if ($query_status_payment->num_rows()>0)
		{
			$data_loan_interest=0;
		}
		else{
			$data_loan_interest=$data_loan_invoice[0]->interest; }
		$data_money_transfer=$_POST['money_transfer'];
	
		if($data_money_transfer<$data_loan_interest)
		{
			$total= $data_loan_amount+($data_loan_interest-$data_money_transfer);
			$data_update_loan_balance=array('loan_amount'=>$total,'updatedate'=>date("Y-m-d"));
		}
		else if ($data_money_transfer==$data_loan_interest)
		{
			$data_update_loan_balance=array('loan_amount'=>$data_loan_amount,'updatedate'=>date("Y-m-d"));
		}
		else if($data_money_transfer<$data_loan_interest+$data_loan_principle)
		{
			$total= $data_loan_amount-($data_money_transfer-$data_loan_interest);
			
				$data_update_loan_balance=array('loan_amount'=>$total,'updatedate'=>date("Y-m-d"));
		}
		else if($data_money_transfer>$data_loan_interest+$data_loan_principle)
		{
			$total= $data_loan_amount-(($data_money_transfer- ($data_loan_interest+$data_loan_principle))+$data_loan_principle);
			if($total>0)
			{
				$data_update_loan_balance=array('loan_amount'=>$total,'updatedate'=>date("Y-m-d"));
			}
			else
			{
				$data_update_loan_balance=array('loan_amount'=>0,'status_loan'=>'E','updatedate'=>date("Y-m-d"));
			}
		}
		else if($data_money_transfer==$data_loan_interest+$data_loan_principle)
		{
			$total= $data_loan_amount-$data_loan_principle;
			if($total>0)
			{
				$data_update_loan_balance=array('loan_amount'=>$total,'updatedate'=>date("Y-m-d"));
			}
			else
			{
				$data_update_loan_balance=array('loan_amount'=>0,'status_loan'=>'E','updatedate'=>date("Y-m-d"));
			}
		}
		$data_update_payment_schedule=array('status'=>'P','updatedate'=>date("Y-m-d"));
		$My_Multiple_update_payment_schedule = array('loan_contract_id'=> $data_loan_invoice[0]->loan_contract_id, 'loan_pay_number'=> $data_loan_invoice[0]->loan_pay_number);
		
		if($this->db->insert('loan_payment',$data_insert_payment))
		{
			
			$this->db->where($My_Multiple_update_payment_schedule);
			
			if($this->db->update('payment_schedule',$data_update_payment_schedule))
			{
				
				$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
				if ($this->db->update('loan_balance',$data_update_loan_balance))
				{
					if($total<=0)
					{
						$My_Multiple_load_status_payment = array('loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id,'status'=>'N');
						
						$array_update_status_schedule=array('status'=>'C','updatedate'=>date("Y-m-d"));
						$this->db->where($My_Multiple_load_status_payment);
						$query_get_all_status_schedul=$this->db->get('payment_schedule');
						if($query_get_all_status_schedul->num_rows()>0)
						{
							$My_Multiple_load_status_payment = array('loan_contract_id'=> $data_loan_invoice[0]->loan_contract_id,'status'=>'N');
							$array_update_status_schedule=array('status'=>'C','updatedate'=>date("Y-m-d"));
							$this->db->where($My_Multiple_load_status_payment);
							if($this->db->update('payment_schedule',$array_update_status_schedule))
							{
								return "success";
							}
							else
							{
								return "fail";
							}
						}
						else
						{
							return "success";
						}
						
					}
					else
					{
						return "success";
					}
					
				}
				else
				{
					return "fail";
				}
			}
			else
				{
					return "fail";
				}
		}
		else
		{
			return "fail";
		} 
	}
	function confirm_l_pay()
	{
		$location="assets/pic_check/";
		$pieces_file = explode("\\", $_POST['file']);
		$type_file=explode(".",$pieces_file[count($pieces_file)-1]);
		$this->db->order_by('id_disbursements','asc');
		$this->db->limit(0);
		$query = $this->db->get('loan_disbursements');
		$id_new;
		if ($query->num_rows()>0)
		{
			foreach($query->result() as $id_dis)
			{
					$id_new=($id_dis->id_disbursements)+1;
			}
		}
		else
		{
			$id_new=1;
		}

		$data_insert=array(
				'id_disbursements'=>$id_new,
				'loan_id'=>$_POST['id'],
				'id_transaction'=>$_POST['id_tran'],
				'id_check'=>$_POST['id_check'],
				'bank_number'=>$_POST['bank'],
				'bank_branch_name'=>$_POST['branch_bank'],
				'date_transfer'=>$_POST['date_tran'],
				'pic_check'=>"check_loan_".$_POST['id'].".".$type_file[1],
				'status_save'=>0
				
			);

		
		if ($this->db->insert('loan_disbursements',$data_insert))
		{
				return "success";
		}
		else
		{
			return "fail";
		}
	}
	function edit_l_payment()
	{
		
		$this->db->where('invoice_no',$_POST['invoice_no']);
		$query_loan_invoice=$this->db->get('loan_invoice');
		$data_loan_invoice=$query_loan_invoice->result();
		$str=str_replace(",","",$_POST['money_transfer']);
		preg_match('/[[:digit:]]+\.?[[:digit:]]*/', $str, $regs);  
		$new_m=doubleval($regs[0]);  
		$old_m=doubleval($_POST['old_money_transfer']);
		$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
		$query_loan_balance=$this->db->get('loan_balance');
		$data_loan_balance=$query_loan_balance->result();
		$old_loan_amount=$data_loan_balance[0]->loan_amount;
		
		if($old_m>$new_m)
		{
			$total=$old_m-$new_m;
			$old_loan_amount=$old_loan_amount+$total;
		}
		else if($old_m==$new_m)
		{
			$old_loan_amount=$old_loan_amount;
		}
		else if($old_m<$new_m)
		{
			
			$total=$new_m-$old_m;
			$old_loan_amount=$old_loan_amount+$total;
		}
			$data_update_balance=array('loan_amount'=>$old_loan_amount,'updatedate'=>date("Y-m-d"));
			$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
			if($this->db->update('loan_balance',$data_update_balance))
			{
				$date_tran=explode("/",$_POST['date_transfer']);
				$data_tran_u=(intval($date_tran[2])-543)."-".$date_tran[1]."-".$date_tran[0];
				$date_pay_check=$date_tran[2].$date_tran[1];
				$old_d_t=explode("/",$_POST['date_transfer']);
				$new_d_t=explode("/",$_POST['old_date_transfer']);
				$old_d_t1=$old_d_t[1]."/".$old_d_t[2];
				$new_d_t1=$new_d_t[1]."/".$new_d_t[2];
				
				if($old_d_t1!=$new_d_t1)
				{
					$query_data_count_pay_no=$this->db->query("select payment_no from loan_payment where cast(payment_no as varchar(20)) like '%$date_pay_check%' order by payment_no desc limit 1");
					if($query_data_count_pay_no->num_rows()>0)
					{
						$data_count_pay_no=$query_data_count_pay_no->result();
						$new_pay_no=($data_count_pay_no[0]->payment_no)+1;
					}
					else
					{
						$new_pay_no=$date_pay_check."001";
					}
				}
				if($old_d_t1==$new_d_t1)
				{
					$this->db->where('payment_no',$_POST['payment_no']);
					$update_loan_payment=array('com_code'=>$_POST['bank'],'updatedate'=>date("Y-m-d"),'money_transfer'=>$_POST['money_transfer'],'date_transfer'=>$data_tran_u);
					if($this->db->update('loan_payment',$update_loan_payment))
					{
						return "success";
					}
					else
					{
						return "ไม่สามารถบันทึกได้";
					}
				}
				else
				{
					$this->db->where('payment_no',$_POST['payment_no']);
					
					$update_loan_payment=array('com_code'=>$_POST['bank'],'updatedate'=>date("Y-m-d"),'money_transfer'=>$_POST['money_transfer'],'date_transfer'=>$data_tran_u,'payment_no'=>$new_pay_no);
					if($this->db->update('loan_payment',$update_loan_payment))
					{
						return "success";
					}
					else
					{
						return "ไม่สามารถบันทึกได้";
					}
					
				}
			}
			else
			{
				return "ไม่สามารถบันทึกได้";
			}
	}
	function show_w_payment()
	{
		$data_wait_payment="";
		$date_year="";
		$month_search=date("m");
		$year_search=date("Y");
		if(intval($month_search)>=12)
		{
			$date_year=((intval($year_search)+1)."-"."01"."-"."01");
		}
		else
		{
			if(intval($month_search)<10)
			{
				$date_year=intval($year_search)."-0".(intval($month_search)+1)."-"."01";
			}
			else
			{
				$date_year=intval($year_search)."-".(intval($month_search)+1)."-"."01";
			}
		}
		$query_loan_invoice=$this->db->query("SELECT * FROM loan_invoice WHERE date_expire_payment<='$date_year' AND NOT loan_invoice.invoice_no IN (SELECT invoice_no FROM loan_payment) order by date_expire_payment desc");
		if($query_loan_invoice->num_rows()>0)
		{
		foreach($query_loan_invoice->result() as $data_loan_invoice)
		{
			$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
			$query_data_loan_contract=$this->db->get('loan_contract');
			$data_loan_contract=$query_data_loan_contract->result();
			$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
			$query_data_loan_balance=$this->db->get('loan_balance');
			$data_loan_balance=$query_data_loan_balance->result();
			$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
			$query_data_loan_id=$this->db->get('loan');
			$data_loan_id=$query_data_loan_id->result();
			$this->db->where('person_id',$data_loan_id[0]->person_id);
			$query_data_person=$this->db->get('person');
			$data_person=$query_data_person->result();
			$this->db->where('title_id',$data_person[0]->title_id);
			$query_data_title=$this->db->get('config_title');
			$data_title=$query_data_title->result();
			$data_wait_payment=$data_wait_payment.$data_loan_invoice->invoice_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_invoice->date_expire_payment."\\";

		}
		return $data_wait_payment;
		}
		else
		{
			return "no data";
		}

	}
	function search_w_payment()
	{
		$data="";
		if($_POST['id_card_search']!="")
		{
			$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->check_w_id_card_l1($_POST['id_card_search']);
		}
		if($_POST['name_search']!="")
		{
			$split_name=explode(" ",$_POST['name_search']);
			if (count($split_name)>2)
			{
				$split_name[1]=$split_name[1]." ".$split_name[2];
				$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->search_w_name_in_person($split_name[0],$split_name[1]);
			}
			else if (count($split_name)==1)
			{
				$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->search_w_name1_in_person($_POST['name_search']);
			}
			else
			{
				$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			$data=$data.$this->report_card_debtor_m->search_w_name_in_person($split_name[0],$split_name[1]);
			}
		}
		
		if($_POST['contact_id']!="")
		{
			$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 $data=$data.$this->report_card_debtor_m->search_w_contract_no($_POST['contact_id']."\\");
		}
		return $data;
		
	}
	function search_w_contract_no($invoice_no)
	{	
		if($invoice_no!="no data")
		{
		$data_wait_payment="";
		$sqlit_data=explode("\\",$invoice_no);
		for($i=0;$i<count($sqlit_data)-1;$i++)
		{
			$this->db->where('invoice_no',$sqlit_data[$i]);
			$query_data_loan_invoice=$this->db->get('loan_invoice');
			if($query_data_loan_invoice->num_rows()>0)
			{
			$data_loan_invoice=$query_data_loan_invoice->result();
			$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
			$query_data_loan_contract=$this->db->get('loan_contract');
			$data_loan_contract=$query_data_loan_contract->result();
			$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
			$query_data_loan_balance=$this->db->get('loan_balance');
			$data_loan_balance=$query_data_loan_balance->result();
			$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
			$query_data_loan_id=$this->db->get('loan');
			$data_loan_id=$query_data_loan_id->result();
			$this->db->where('person_id',$data_loan_id[0]->person_id);
			$query_data_person=$this->db->get('person');
			$data_person=$query_data_person->result();
			$this->db->where('title_id',$data_person[0]->title_id);
			$query_data_title=$this->db->get('config_title');
			$data_title=$query_data_title->result();
			$data_wait_payment=$data_wait_payment.$data_loan_invoice[0]->invoice_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_invoice[0]->principle.",".$data_loan_invoice[0]->interest.",".$data_loan_invoice[0]->date_expire_payment."\\";
			}
		}
			if($data_wait_payment!="")
			{
				return $data_wait_payment;
			}
			else
			{
				return "no data";
			}
		}
		else
		{
			return "no data";
		}
		
}
function search_w_name_in_person($name,$lname)
{
	$data_person_id="";
	$check_state=array('person_fname'=>$name,'person_lname'=>$lname);
	
	$this->db->where($check_state);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			 //$data_person_id=$data_person_idใ$this->loan_payment_m->check_id_card_l($data_person->person_thaiid);
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		return $this->report_card_debtor_m->check_w_id_card_l($data_person_id);
	}
	else
	{
		return "no data";
	}
}
function search_w_name1_in_person($name)
{
	$data_person_id="";
	$this->db->where('person_fname',$name);
	$this->db->or_where('person_lname',$name);
	$query_data_person=$this->db->get('person');
	
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		return $this->report_card_debtor_m->check_w_id_card_l($data_person_id);
	}
	else
	{
		return "no data";
	}
}
function check_w_id_card_l1($id_card)
{
	$data_invoice="";
	$data="";
	$data_person_array_id="";
	$this->db->where('person_thaiid',$id_card);
	$query_data_person=$this->db->get('person');
	foreach($query_data_person->result() as $data_person)
	{
		$data_person_array_id=$data_person_array_id.$data_person->person_id.",";
	}
	$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person_array_id);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		$split_data_card=explode(",",$data_person1);
		for( $i=0;$i<count($split_data_card)-1;$i++)
		{
			$this->db->where('person_id',$split_data_card[$i]);
			$query_data_loan=$this->db->get('loan');
			if($query_data_loan->num_rows()>0)
			{
				foreach($query_data_loan->result() as $data_loan)
				{
					$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no!='=>null);
					$this->db->where($check_data);
					$query_data_loan_contract=$this->db->get('loan_contract');
					if($query_data_loan_contract->num_rows()>0)
					{
						foreach($query_data_loan_contract->result() as $data_loan_contract)
						{
							//----------------------------------------------------------
					
							//----------------------------------------------------------
							$this->db->where('loan_contract_no',$data_loan_contract->loan_contract_no);
							$query_data_loan_contract=$this->db->get('loan_contract');
							if($query_data_loan_contract->num_rows()>0)
							{
								$data_loan_contract=$query_data_loan_contract->result();
								$data_loan_contract_id=$data_loan_contract[0]->loan_contract_id;
								$query_data_loan_invoice=$this->db->query("SELECT * FROM loan_invoice WHERE loan_contract_id=$data_loan_contract_id AND NOT loan_invoice.invoice_no IN (SELECT invoice_no FROM loan_payment)");
								if($query_data_loan_invoice->num_rows()>0)
								{
									foreach($query_data_loan_invoice->result() as $data_loan_invoice)
									{
										$data_invoice=$data_invoice.$data_loan_invoice->invoice_no."\\";
									}
								}
								
							}
						}
			}
		}
	}
		}
		if($data_invoice!="")
		{
			$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			return $this->report_card_debtor_m->search_w_contract_no($data_invoice);
		}
		else
		{
			return "no data";
		}
		
}
function check_w_id_card_l($id_card)
{
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$id_card);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		$data_invoice="";
		$data="";
		$data_person_array_id="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$this->db->where('person_thaiid',$check_data_card[$l]);
			$query_data_person=$this->db->get('person');
			foreach($query_data_person->result() as $data_person)
			{
				$data_person_array_id=$data_person_array_id.$data_person->person_id.",";
			}
		}
		
		$split_data_card=explode(",",$data_person_array_id);
		
		for( $i=0;$i<count($split_data_card)-1;$i++)
		{
			$this->db->where('person_id',$split_data_card[$i]);
			$query_data_loan=$this->db->get('loan');
			if($query_data_loan->num_rows()>0)
			{
				foreach($query_data_loan->result() as $data_loan)
				{
					$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no!='=>null);
					$this->db->where($check_data);
					$query_data_loan_contract=$this->db->get('loan_contract');
					if($query_data_loan_contract->num_rows()>0)
					{
						foreach($query_data_loan_contract->result() as $data_loan_contract)
						{
							//----------------------------------------------------------
					
							//----------------------------------------------------------
							$this->db->where('loan_contract_no',$data_loan_contract->loan_contract_no);
							$query_data_loan_contract=$this->db->get('loan_contract');
							if($query_data_loan_contract->num_rows()>0)
							{
								$data_loan_contract=$query_data_loan_contract->result();
								$data_loan_contract_id=$data_loan_contract[0]->loan_contract_id;
								$query_data_loan_invoice=$this->db->query("SELECT * FROM loan_invoice WHERE loan_contract_id=$data_loan_contract_id AND NOT loan_invoice.invoice_no IN (SELECT invoice_no FROM loan_payment)");
								if($query_data_loan_invoice->num_rows()>0)
								{
									foreach($query_data_loan_invoice->result() as $data_loan_invoice)
									{
										$data_invoice=$data_invoice.$data_loan_invoice->invoice_no."\\";
									}
								}
							}
						}
			}
		}
	}
		}
		if($data_invoice!="")
		{
			$this->load->model('Report_card_debtor_model', 'report_card_debtor_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
			return $this->report_card_debtor_m->search_w_contract_no($data_invoice);
		}
		else
		{
			return "no data";
		}
}
function load_data_invoice($invoice_id)
	{
	$data_payment="";
				$this->db->where('invoice_no',$invoice_id);
				$query_loan_invoice=$this->db->get('loan_invoice');
				foreach($query_loan_invoice->result() as $data_loan_invoice)
				{
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_loan_contact=$this->db->get('loan_contract');
					foreach ($query_loan_contact->result() as $data_loan_contact)
					{
						$this->db->where('loan_contract_id',$data_loan_contact->loan_contract_id);
						$query_loan_balance=$this->db->get('loan_balance');
						
						foreach($query_loan_balance->result() as $data_loan_balance)
						{
						$this->db->where('loan_id',$data_loan_contact->loan_id);
						$query_loan=$this->db->get('loan');
						
						foreach ($query_loan->result() as $data_loan)
						{
							$this->db->where('person_id',$data_loan->person_id);
							$query_person=$this->db->get('person');
							
							foreach($query_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_title=$this->db->get('config_title');
								
								foreach($query_title->result() as $data_title)
								{
									$data_title_name=$data_title->title_name;
								}
								$data_payment=$data_payment.$invoice_id.",".$data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_balance->loan_amount.",".$data_loan_invoice->loan_pay_number.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest."\\";

							}
						}
						}
					}
			}
			return $data_payment;
	}
	function show_receipt_payment($payment_no)
	{
		$data_receipt_payment="";
		$this->db->where('payment_no',$payment_no);
		$query_data_loan_payment=$this->db->get('loan_payment');
		if($query_data_loan_payment->num_rows()>0)
		{
			$data_loan_payment=$query_data_loan_payment->result();
			$this->db->where('invoice_no',$data_loan_payment[0]->invoice_no);
			$query_data_loan_invoice=$this->db->get('loan_invoice');
			if($query_data_loan_invoice->num_rows()>0)
			{
				$data_loan_invoice=$query_data_loan_invoice->result();
				$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
				$query_data_loan_contract=$this->db->get('loan_contract');
				if($query_data_loan_contract->num_rows()>0)
				{
					$data_loan_contract=$query_data_loan_contract->result();
					$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
					$query_data_loan_type=$this->db->get('loan_type');
					$data_loan_type=$query_data_loan_type->result();
					$this->db->where('loan_objective_id',$data_loan_type[0]->loan_objective_id);
					$query_data_loan_objective=$this->db->get('master_loan_objective');
					$data_loan_objective=$query_data_loan_objective->result();
					$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
					$query_data_loan_balance=$this->db->get('loan_balance');
					$data_loan_balance=$query_data_loan_balance->result();
					$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
					$query_data_person=$this->db->get('loan');
					if($query_data_person->num_rows()>0)
					{
						$data_person=$query_data_person->result();
						
						$this->db->where('person_id',$data_person[0]->person_id);
						$query_data_detail_person=$this->db->get('person');
						$data_detail_person=$query_data_detail_person->result();
						$this->db->where('title_id',$data_detail_person[0]->title_id);
						$query_data_title=$this->db->get('config_title');
						$data_title=$query_data_title->result();
						$this->db->where('district_id',$data_detail_person[0]->person_addr_card_district_id);
						$query_data_district=$this->db->get('master_district');
						$data_district=$query_data_district->result();
						$this->db->where('amphur_id',$data_detail_person[0]->person_addr_card_amphur_id);
						$query_data_amphur=$this->db->get('master_amphur');
						$data_amphur=$query_data_amphur->result();
						$this->db->where('province_id',$data_detail_person[0]->person_addr_card_province_id);
						$query_data_province=$this->db->get('master_province');
						$data_province=$query_data_province->result();
						$address="";
						$tel="";
						if($data_detail_person[0]->person_addr_card_no!=null){
								$address=$address.$data_detail_person[0]->person_addr_card_no." ";
						}
						if($data_detail_person[0]->person_addr_card_moo!=null){
							$address=$address."หมู่ที่ ".$data_detail_person[0]->person_addr_card_moo." ";
						}
						if($data_detail_person[0]->person_addr_card_road!=null){
							$address=$address."หมู่ที่ ".$data_detail_person[0]->person_addr_card_road." ";
						}
						if($data_detail_person[0]->person_phone!=null){
							$tel=$tel."โทร. ".$data_detail_person[0]->person_phone." ";
						}
						if($data_detail_person[0]->person_mobile!=null){
							$tel=$tel."มือถือ. ".$data_detail_person[0]->person_mobile." ";
						}
						$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
						$query_data__loan_object_id=$this->db->get('loan_type');
						$data__loan_object_id=$query_data__loan_object_id->result();
						$this->db->where('loan_objective_id',$data__loan_object_id[0]->loan_objective_id);
						$query_data_objective_name=$this->db->get('master_loan_objective');
						$data_objective_name=$query_data_objective_name->result();
						$check_payment=array('status'=>'P','loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id,'loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number);
						$this->db->where($check_payment);
						$query_data_interest_rate=$this->db->get('payment_schedule');
						$data_interest_rate=$query_data_interest_rate->result();
						$data_receipt_payment=$data_receipt_payment.$payment_no.",".$data_loan_payment[0]->date_transfer.",".$data_objective_name[0]->loan_objective_name.",".$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$address." ".$data_detail_person[0]->person_addr_card_road.","." ตำบล".$data_district[0]->district_name." อำเภอ".$data_amphur[0]->amphur_name." จังหวัด".$data_province[0]->province_name.",".$tel.",".$data_loan_payment[0]->date_transfer.",".$data_loan_contract[0]->loan_contract_no.",".$data_loan_contract[0]->loan_id.",".$data_interest_rate[0]->interest_rate.",".$data_loan_invoice[0]->loan_pay_number.",".$data_loan_payment[0]->money_transfer.",".$data_loan_invoice[0]->principle.",".$data_loan_invoice[0]->interest.",".$data_loan_balance[0]->loan_amount;
						return $data_receipt_payment;
					}
					else
					{
						return "no data";
					}
					
				}
				else
				{
					return "no data";
				}
			}
			else
			{
				return "no data";
			}
		}
		else
		{
			return "no data";
		}
	}
}
?>