<br />

<script type="text/javascript">
$(document).ready(function(){
	
});
function exportToExcel(tableID){
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6' style='height: 75px; text-align: center; width: 250px'>";
    var textRange; var j=0;
    tab = document.getElementById(tableID); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {

        tab_text=tab_text;

        tab_text=tab_text+tab.rows[j].innerHTML.toUpperCase()+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text= tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); //remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer

    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write( 'sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"sudhir123.txt");
    }

    else {
       sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
    
    return (sa);
}
</script>
<?php

function cal_date($fdate,$ldate)
{
	//-------------------cal date
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);                //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$u_y=date("Y", $mage)-1970;
	$u_m=date("m",$mage)-1;
	$u_d=date("d",$mage)-1;
	$show_date="";
	if($u_y==0){$u_y="";} else {$u_y=$u_y." ปี";}
	if($u_m==0){$u_m="";} else {$u_m=$u_m." เดือน";}
	if($u_d==0){$u_d="";} else {$u_d=$u_d." วัน";}
 return "  $u_y $u_m $u_d";
 
//---------------end caldate	
}
function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return round($result);
}

function cal_real_date($fdate,$ldate)
{
	$first_date=explode("-",$fdate);
	$last_date=explode("-",$ldate);
	$different=$last_date[0]-$first_date[0];
	$count_date="";
	
	for($i=0;$i<=$different;$i++)
	{
		if($i==0)
		{
			$ldate1=$first_date[0]+$i;
			
				if($ldate1>2036)
				{
					if($ldate1%4==0)
					{
						if($different==0)
						{
							$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2016"."-"."12"."-"."31";
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}					
					}
					else
					{
						if($different==0)
						{
							$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2017"."-"."12"."-"."31";
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}					
					}
					
				}
				else
				{
					if($different==0)
					{
						$count_date=$count_date+cal_date1($fdate,$ldate);
					}
					else
					{
						$ldate2=$first_date[0]."-"."12"."-"."31";
						$ldate3=$first_date[0]."-".$first_date[1]."-".$first_date[2];
						$count_date=$count_date+cal_date1($ldate3,$ldate2);
					}
				}	
		}
		else if($i>0 && $i<$different)
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".'12'."-".'31';
					$ldate3="2016"."-".'01'."-".'01';
					
				}
				else
				{
					$ldate2="2017"."-".'12'."-".'31';
					$ldate3="2017"."-".'01'."-".'01';
					
				}
			}
			else
			{
				$ldate2=$ldate1."-".'12'."-".'31';
				$ldate3=$ldate1."-".'01'."-".'01';

			}
			$count_date=$count_date+cal_date1($ldate3,$ldate2);
			
		}
		else if ($i==$different )
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2016"."-".'01'."-".'01';
					$count_date=$count_date+cal_date1($ldate3,$ldate2);	
				}
				else
				{
					$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2017"."-".'01'."-".'01';
					$count_date=$count_date+cal_date1($ldate3,$ldate2);	
				}
			}
			else
			{
				$ldate3=$ldate1."-".'01'."-".'01';
				$count_date=$count_date+cal_date1($ldate3,$ldate);	
			}
		}
	}
	return $count_date+$different;
	
}
$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
$month1 = array("ม.ค.", "ก.พ.", "มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
$split_data=explode("/*/",$load_debtor);
	
	$split_data1=explode(",",$split_data[0]);
	?>
<center><table width="100%" height="100%" border="0" id="show_card_debtor1" name="show_card_debtor1">
  <tr>
    <td colspan="14"><center>ประมาณการการ์ดลูกหนี้รายตัว (จ่ายเงินค่างวดจริง) </center></td>
  </tr>
  <tr>
    <td colspan="14"> </td>
  </tr>
  <tr>
    <td colspan="4">ชื่อ: <u><?php echo $split_data1[1]; ?></u></td>
    <td colspan="3">รหัสลูกหนี้: <u><?php echo $split_data1[2]; ?></u></td>
    <td colspan="7">ประเภทสินชื่อ: <u><?php echo $split_data1[3]; ?></u></td>
  </tr>
  <tr>
    <td colspan="4">สัญญาเลขที่: <u><?php echo $split_data1[4]; ?></u></td>
    <td colspan="4">วันที่: <u>
	<?php 
	$split_day=explode("-",$split_data1[5]);
	echo $split_day[2]." ".$month[intval($split_day[1])-1]." ".(intval($split_day[0])+543);
	?>
    </u></td>
    <td colspan="3">วงเงินกู้: <u><?php echo number_format($split_data1[6]); ?></u></td>
    <td colspan="3">อัตราดอกเบี้ย:  <u><?php echo $split_data1[7]; ?></u> ปี</td>
  </tr>
  <tr>
    <td colspan="4">วันที่สิ้นสุดสัญญา: <u>
	 
    <?php
		$day_split=explode("-",$split_data1[8]);
		$split_data2=explode("\\",$split_data[1]);
		$split_data3=explode(",",$split_data2[count($split_data2)-2]);
		$split_day1=explode("-",$split_data3[1]);
		echo $day_split[2]." ".$month[intval($day_split[1])-1]." ".(intval($day_split[0])+543);
	?>
    </u></td>
    <td colspan="4">ระยะเวลา: <u>
	<?php echo $split_data1[0]." งวด";
		
	?></u></td>
	<td colspan="4"> <u>
	<?php $total_date=cal_real_date($split_data1[5],$split_data1[8]);
	$y_date=floor($total_date/365);
	$total_date=$total_date-$y_date*365;
	$m_date=floor($total_date/30);
	$total_date=$total_date-$m_date*30;
	if($y_date>0)
	{
		echo $y_date." ปี ";
	}
	if($m_date>0)
	{
		echo $m_date." เดือน ";
	}
	if($total_date>0)
	{
		echo $total_date." วัน ";
	}
	?></u></td>
  </tr>
  
  <tr>
    <td colspan="17"><center><table width="100%" height="100%" border="1" id="show_c_debtor1" name="show_c_debtor1">
    <br />
    <tr>
	<td colspan="1"><center>ลำดับที่</td>
    <td colspan="1"><center>งวดที่</td>
    <td colspan="2"><center>กำหนดชำระ</td>
    <td colspan="1"><center>วัน</td>
    <td colspan="2"><center>เงินต้นยกมา</td>
    <td colspan="1"><center>ดอกเบี้ย</td>
    <td colspan="2"><center>ยอดส่ง</td>
    <td colspan="2"><center>เป็นเงินต้น</td>
    <td colspan="1"><center>เป็นดอกเบี้ย</td>
    <td colspan="2"><center>เงินต้นคงเหลือ</td>
	<td colspan="2"><center>ดอกเบี้ยคงค้าง</td>
    </tr>
    <?php
	$total4=0;
	$total5=0;
	$total6=0;
	$fdate=$split_data1[5];
	$money_amount=$split_data1[6];
	
	for($i=0;$i<count($split_data2)-1;$i++)
	{
		$split_insert=explode(",",$split_data2[$i]);
		
		?>
		<tr>
		<td colspan="1" style="font-sizne: 10px; text-align:center;"><?php echo $i+1;?></td>
        <td colspan="1" style="font-sizne: 10px; text-align:center;"><?php echo $split_insert[0];?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:center;"><?php 
		$spli_day_payout=explode("-",$split_insert[1]);
		echo $spli_day_payout[2]." ".$month1[intval($spli_day_payout[1])-1]." ".(intval($spli_day_payout[0])+543);
		?>
        </td>
    	<td colspan="1" style="font-sizne: 10px; text-align:center;"><?php echo cal_date1($fdate,$split_insert[1]);
		$interest=round((intval($money_amount)*(((intval($split_insert[3])/365)*cal_date1($fdate,$split_insert[1]))))/100+0.004,2);
		?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($money_amount,2);?></td> <?php //จำนวนเงินต้นที่ยกมา ?>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;"><?php echo $split_insert[3]."%";?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php    
		echo number_format($split_insert[4]+$split_insert[5],2); 
		$total4=$total4+$split_insert[4]+$split_insert[5];
		?></td> <?php //ยอดส่ง ?>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php 
		echo number_format($split_insert[4],2);
		$total5=$total5+$split_insert[4];
		?></td><?php //เป็นเงินต้น ?>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;">
		<?php echo number_format($split_insert[5],2);
		$total6=$total6+$split_insert[5];
		?></td><?php //เป็นดอกเบี้ย ?>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php
		
				$money_amount=$money_amount-($split_insert[4]);
		echo number_format($money_amount,2);
        ?></td> <?php //เงินต้นคงเหลือ?>
		<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php
		
				
		echo number_format($split_insert[7],2);
        ?></td> <?php //ดอกเบี้ยคงค้าง?>
        </tr>
        <?php
		$fdate=$split_insert[1];
		
		}
	?>
	<tr>
		<td colspan="1"></td>
        <td colspan="1"></td>
    	<td colspan="2"></td>
    	<td colspan="1"></td>
    	<td colspan="2"></td>
    	<td colspan="1"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total4,2);?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total5,2);?></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total6,2);?></td>
    	<td colspan="2"></td>
		<td colspan="2"></td>
	</tr>
    </table></center></td>
  </tr>
</table>
</center>
<br />
<center><input name="btn_to_excel1" type="button" id="btn_to_excel1" value="ส่งออกข้อมูลประมาณการการ์ดลูกหนี้รายตัว (จ่ายเงินค่างวดจริง)"  onclick="exportToExcel('show_card_debtor1')" /></input></center>

