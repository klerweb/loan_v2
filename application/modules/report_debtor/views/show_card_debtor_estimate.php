
<br />

<script type="text/javascript">
$(document).ready(function(){
	
});
function exportToExcel(tableID){
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6' style='height: 75px; text-align: center; width: 250px'>";
    var textRange; var j=0;
    tab = document.getElementById(tableID); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {

        tab_text=tab_text;

        tab_text=tab_text+tab.rows[j].innerHTML.toUpperCase()+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text= tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); //remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); //remove input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer

    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write( 'sep=,\r\n' + tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"sudhir123.txt");
    }

    else {
       sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
    }
    
    return (sa);
}

</script>
<?php

function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return round($result);
}

function cal_real_date($fdate,$ldate)
{
	$first_date=explode("-",$fdate);
	$last_date=explode("-",$ldate);
	$different=$last_date[0]-$first_date[0];
	$count_date="";
	
	for($i=0;$i<=$different;$i++)
	{
		if($i==0)
		{
			$ldate1=$first_date[0]+$i;
			
				if($ldate1>2036)
				{
					if($ldate1%4==0)
					{
						if($different==0)
						{
							$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2016"."-"."12"."-"."31";
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}					
					}
					else
					{
						if($different==0)
						{
							$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2017"."-"."12"."-"."31";
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$count_date=$count_date+cal_date1($ldate3,$ldate2);
						}					
					}
					
				}
				else
				{
					if($different==0)
					{
						$count_date=$count_date+cal_date1($fdate,$ldate);
					}
					else
					{
						$ldate2=$first_date[0]."-"."12"."-"."31";
						$ldate3=$first_date[0]."-".$first_date[1]."-".$first_date[2];
						$count_date=$count_date+cal_date1($ldate3,$ldate2);
					}
				}	
		}
		else if($i>0 && $i<$different)
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".'12'."-".'31';
					$ldate3="2016"."-".'01'."-".'01';
					
				}
				else
				{
					$ldate2="2017"."-".'12'."-".'31';
					$ldate3="2017"."-".'01'."-".'01';
					
				}
			}
			else
			{
				$ldate2=$ldate1."-".'12'."-".'31';
				$ldate3=$ldate1."-".'01'."-".'01';

			}
			$count_date=$count_date+cal_date1($ldate3,$ldate2);
			
		}
		else if ($i==$different )
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2016"."-".'01'."-".'01';
					$count_date=$count_date+cal_date1($ldate3,$ldate2);	
				}
				else
				{
					$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2017"."-".'01'."-".'01';
					$count_date=$count_date+cal_date1($ldate3,$ldate2);	
				}
			}
			else
			{
				$ldate3=$ldate1."-".'01'."-".'01';
				$count_date=$count_date+cal_date1($ldate3,$ldate);	
			}
		}
	}
	return $count_date+$different;
	
}

$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
$month1 = array("ม.ค.", "ก.พ.", "มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
$split_data=explode("/*/",$load_debtor);
	
	$split_data1=explode(",",$split_data[0]);
	?>
<center><table width="100%" height="100%" border="0" id="show_card_debtor" name="show_card_debtor">
  <tr>
    <td colspan="14"><center>ประมาณการการ์ดลูกหนี้รายตัว</center></td>
  </tr>
  <tr>
    <td colspan="14"> </td>
  </tr>
  <tr>
    <td colspan="4">ชื่อ: <u><?php echo $split_data1[1]; ?></u></td>
    <td colspan="4">รหัสลูกหนี้: <u><?php echo $split_data1[2]; ?></u></td>
    <td colspan="6">ประเภทสินชื่อ: <u><?php echo $split_data1[3]; ?></u></td>
  </tr>
  <tr>
    <td colspan="4">สัญญาเลขที่: <u><?php echo $split_data1[4]; ?></u></td>
    <td colspan="4">วันที่: <u>
	<?php 
	$split_day=explode("-",$split_data1[5]);
	echo $split_day[2]." ".$month[intval($split_day[1])-1]." ".(intval($split_day[0])+543);
	?>
    </u></td>
    <td colspan="3">วงเงินกู้: <u><?php echo number_format($split_data1[6],2); ?></u></td>
    <td colspan="3">อัตราดอกเบี้ย:  <u><?php echo $split_data1[7]; ?></u> ปี</td>
  </tr>
  <tr>
    <td colspan="4">วันที่สิ้นสุดสัญญา: <u>
    <?php
		$split_data2=explode("\\",$split_data[1]);
		$split_data3=explode(",",$split_data2[count($split_data2)-2]);
		$split_day1=explode("-",$split_data3[1]);
		echo $split_day1[2]." ".$month[intval($split_day1[1])-1]." ".(intval($split_day1[0])+543);
	?>
    </u></td>
    <td colspan="4">ระยะเวลา: <u>
	<?php echo $split_data1[0]." งวด";
		
	?></u></td>
	<td colspan="4"> <u>
	<?php $total_date=cal_real_date($split_data1[5],$split_data3[1]);
	$y_date=floor($total_date/365);
	$total_date=$total_date-$y_date*365;
	$m_date=floor($total_date/30);
	$total_date=$total_date-$m_date*30;
	if($y_date>0)
	{
		echo $y_date." ปี ";
	}
	if($m_date>0)
	{
		echo $m_date." เดือน ";
	}
	if($total_date>0)
	{
		echo $total_date." วัน ";
	}
	?></u></td>
  </tr>
  <tr>
    <td colspan="15"><center><table width="100%" height="100%" border="1" id="show_c_debtor" name="show_c_debtor">
    <br />
    <tr>
	<td colspan="1"><center>ลำดับที่</td>
    <td colspan="1"><center>งวดที่</td>
    <td colspan="2"><center>กำหนดชำระ</td>
    <td colspan="1"><center>วัน</td>
    <td colspan="2"><center>เงินต้นยกมา</td>
    <td colspan="1"><center>ดอกเบี้ย</td>
    <td colspan="2"><center>ยอดส่ง</td>
    <td colspan="2"><center>เป็นเงินต้น</td>
    <td colspan="1"><center>เป็นดอกเบี้ย</td>
    <td colspan="2"><center>เงินต้นคงเหลือ</td>
    </tr>
    <?php
	$total=0;
	$total1=0;
	$total2=0;
	$total3=0;
	$fdate=$split_data1[5];
	//echo $fdate;
	$money_amount=$split_data1[6];
	for($i=0;$i<count($split_data2)-1;$i++)
	{
		
		?><tr><?php
		$split_insert=explode(",",$split_data2[$i]);
		?>
		<td colspan="1" style="font-sizne: 10px; text-align:center;"><?php echo number_format($i+1);?></td>
        <td colspan="1"><center><?php echo number_format($split_insert[0]);?></td>
    	<td colspan="2"><center><?php 
		$spli_day_payout=explode("-",$split_insert[1]);
		echo $spli_day_payout[2]." ".$month1[intval($spli_day_payout[1])-1]." ".(intval($spli_day_payout[0])+543);
		?>
        </td>
    	<td colspan="1"><center><?php 
		
		echo cal_real_date($fdate,$split_insert[1]);
		
		
		
		
		$interest=(intval($money_amount)*(((intval($split_insert[3])/365)*cal_real_date($fdate,$split_insert[1]))))/100;
		$ex = explode('.',$interest);
			if(count($ex)==2) {$s = substr($ex[1],0,2);
			$interest=$ex[0].".".$s;}
			
		
		?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($money_amount,2);?></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;"><?php echo $split_insert[3]."%";?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php    echo number_format($interest+$split_insert[2],2);
		$total1=$total1+$interest+$split_insert[2];
		?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($split_insert[2],2);
		$total2=$total2+$split_insert[2];
		?></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;">
		<?php echo number_format($interest,2);
		$total3=$total3+$interest;
		?></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php
		$money_amount=$money_amount-$split_insert[2];
		echo number_format($money_amount,2);
        ?></td>
        </tr>
        <?php
		$fdate=$split_insert[1];
	}
	?>
	<tr>
		<td colspan="1" style="font-sizne: 10px; text-align:right;"></td>
        <td colspan="1" style="font-sizne: 10px; text-align:right;"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;"></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total1,2);?></p></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total2);?></p></td>
    	<td colspan="1" style="font-sizne: 10px; text-align:right;"><?php echo number_format($total3,2);?></p></td>
    	<td colspan="2" style="font-sizne: 10px; text-align:right;"></td>
	</tr>
    </table></center></td>
  </tr>
</table>
</center>
<br />
<center><input name="btn_to_excel1" type="button" id="btn_to_excel1" value="ส่งออกข้อมูลประมาณการการ์ดลูกหนี้รายตัว "  onclick="exportToExcel('show_card_debtor')" /></input></center>
<br>



