<?php
function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return round($result);
}

function cal_real_date($fdate,$ldate)
{
	$first_date=explode("-",$fdate);
	$last_date=explode("-",$ldate);
	$different=$last_date[0]-$first_date[0];
	$count_date="";
	for($i=0;$i<=$different;$i++)
	{
		if($i==0)
		{
			$ldate1=$first_date[0]+$i;
			$ldate2=$ldate1."-".'12'."-".'31';
			$count_date=$count_date+cal_date1($fdate,$ldate2);
		}
		else if($i>0 && $i<$different)
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2037)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".'12'."-".'31';
					$ldate3="2016"."-".'01'."-".'01';
				}
				else
				{
					$ldate2="2017"."-".'12'."-".'31';
					$ldate3="2017"."-".'01'."-".'01';
				}
			}
			else
			{
				$ldate2=$ldate1."-".'12'."-".'31';
				$ldate3=$ldate1."-".'01'."-".'01';
			}
			$count_date=$count_date+cal_date1($ldate3,$ldate2);
		}
		else if ($i==$different)
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2016"."-".'01'."-".'01';
				}
				else
				{
					$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2017"."-".'01'."-".'01';
				}
				$count_date=$count_date+cal_date1($ldate3,$ldate2);
			}
			else
			{
				$ldate3=$ldate1."-".'01'."-".'01';
				$count_date=$count_date+cal_date1($ldate3,$ldate);
			}	
		}
	}
	return $count_date+$different;
	
}
echo cal_real_date('2017-05-20','2050-5-20')."<br>";
echo cal_date1('2017-05-20','2025-5-20')."<br>";
$total_day=cal_real_date('2017-05-20','2070-9-20');
$year_total=floor($total_day/365);
$total_day1=$total_day-floor($total_day/365)*365;
$month_total=floor($total_day1/30);
$total_day1=$total_day1-($month_total*30);
echo $year_total."ปี<br>".$month_total."เดือน<br>".$total_day1."วัน<br>";




/*function factorial($number) {
	if ($number < 2) {
		return 1;
	} 
	else {
		return ($number * factorial($number-1));
	}
}

echo factorial(6);*/

?>