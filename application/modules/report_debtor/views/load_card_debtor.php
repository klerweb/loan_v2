
<script type="text/javascript">

//id_card: $("#id_card_search").val(),name:$("#name_search").val(),contact_id:$("#contact_id").val()
function fncSubmit()
{
	if ($("#id_card_search").val()=="" && $("#name_search").val()=="" && $("#contact_id").val()=="")
	 {
		 alert("กรุณาใส่ข้อมูลที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}
//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
</script>
<?php $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
?>
<style>

</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('report_debtor/search_card_debtor'); ?>" onSubmit="JavaScript:return fncSubmit();">
					<div class="form-group">
						<label class="sr-only" for="id_card_search">บัตรประชาชน</label>
						<input type="text" class="form-control" id="id_card_search" name="id_card_search" autofocus="autofocus"  placeholder="<?php if(isset($txt_id_card)==false)
{
	echo "หมายเลขบัตรประชาชน";
} else { echo $txt_id_card; } ?>" maxlength="13" onkeydown="number();"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="name_search">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="name_search" name="name_search" autofocus="autofocus" placeholder="<?php if(isset($txt_name)==false)
{
	echo "ชื่อนามสกุลผู้กู้";
} else { echo $txt_name; } ?>" maxlength="200"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="contact_id">เลขที่สัญญาเงินกู้</label>
						<input type="text" class="form-control" id="contact_id" name="contact_id" autofocus="autofocus" placeholder="<?php if(isset($txt_contact_id)==false)
{
	echo "เลขที่สัญญาเงินกู้";
} else { echo $txt_contact_id; } ?>" maxlength="50" /> 
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30" id="table_show_loan_withdraw" >
				<thead>
					<tr>
		            	<th><p align="left">#</p></th>
						
            <th><p align="left">เลขที่สัญญาเงินกู้</p></th>
            <th><p align="left">ชื่อผู้กู้</p></th>
           
			<th><p align="right">จำนวนเงินกู้</p></th>
			<th><p align="left">วันที่เริ่มต้นสัญญา</p></th>
			<th><p align="left">วันที่สิ้นสุดสัญญา</p></th>
			 <th><p align="left">ประมาณการ์ดลูกหนี้</p></th>
			 <th><p align="left">การ์ดลูกหนี้</p></th>
			 
			
				</thead>
				<tbody>
<?php	if($report_card_debtor!="no data")
{
	$i_no = 1;
	$i=0;
	$check_data_print=array();
$pieces_loan_payments = explode("\\", $report_card_debtor);
for ($show_array=0;$show_array<count($pieces_loan_payments)-1;$show_array++)
{
	$split2_loan_payments=explode(",",$pieces_loan_payments[$show_array]);
	
	echo "<tr >";
		echo "<td>".$i_no++."</td>";
		echo "<td>".$split2_loan_payments[0]."</td>";
		echo "<td>".$split2_loan_payments[1]."</td>"; ?>
		
		<td><p align="right"><?php echo number_format($split2_loan_payments[2],2);?></p></td><?php
		echo "<td>";
			$split_day1=explode("-", $split2_loan_payments[3]);
$year1=intval($split_day1[0])+543;
echo intval($split_day1[2])." ".$month[intval($split_day1[1])-1]." ".$year1."</td>";
		echo "<td>";
		$split_day=explode("-", $split2_loan_payments[4]);

$year=intval($split_day[0])+543;
echo intval($split_day[2])." ".$month[intval($split_day[1])-1]." ".$year."</td>";
	echo "<td>"; ?>
	<a href="<?php echo site_url()?>report_debtor/estimate_card_debtors/<?php echo $split2_loan_payments[6];?>">
		ประมาณการลูกหนี้
		</a></td> 
	<td>
	<?php if($split2_loan_payments[5]==1) { ?>
	<a href="<?php echo site_url()?>report_debtor/real_card_debtors/<?php echo $split2_loan_payments[6];?>">
		การ์ดลูกหนี้
		</a></td> 
		<?php }?>
		
	<?php echo "</tr>"; 
	$i++;
}
}
?>


	</tbody>
</table>
		</div><!-- table-responsive -->
	</div>
</div>	