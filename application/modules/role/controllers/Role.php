<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller 
{
	private $title = 'สิทธิการใช้งาน';
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('role_model');
		$this->load->model('module_role_model');
		$this->load->model('mapping/mapping_model');
	}
	
	public function index()
	{		
		$data_menu['menu'] = 'user';
		$data_breadcrumb['menu'] = array('ผู้ใช้งาน' =>'#');
		
		$data_content = array(
			'result' => $this->role_model->search()
		);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('role', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'user';
		$data_breadcrumb['menu'] = array('ผู้ใช้งาน' =>'#',$this->title => site_url('user'));
		
		$data_content = array(
				'id' => $id,
				'result' => $id ? $this->role_model->get_table_by_id($id) : null,
				'menu' => $this->mapping_model->search(),
				'role_module' => $id ? $this->module_role_model->search($id) : null
			);
		
		$data = array(
				'title' => $this->title,
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{		
		$module_id = $_POST['module_id'];
		$isread = !empty($_POST['isread']) ? $_POST['isread'] : null;
		$iswrite = !empty($_POST['iswrite']) ? $_POST['iswrite'] : null;
		$isedit = !empty($_POST['isedit']) ? $_POST['isedit'] : null;
		$isdelete = !empty($_POST['isdelete']) ? $_POST['isdelete'] : null;
		$isowner = !empty($_POST['isowner']) ? $_POST['isowner'] : null;
		
		unset($_POST['module_id']);
		unset($_POST['isread']);
		unset($_POST['iswrite']);
		unset($_POST['isedit']);
		unset($_POST['isdelete']);
		unset($_POST['isowner']);
		
		$result = $this->role_model->save($_POST,$_POST['role_id']);
		if(!empty($module_id))
		{
			$role_id = $result['id'];
			$this->module_role_model->delete_by_roleid($role_id);
			foreach ($module_id as $row) {
				if(!empty($isread[$row]) || !empty($iswrite[$row]) || !empty($isedit[$row])  || !empty($isdelete[$row]) || !empty($isowner[$row]) ){
					$data['role_id'] = $role_id;
					$data['module_id'] = $row;
					$data['module_isread'] = !empty($isread[$row]) ?  $isread[$row] : '0';
					$data['module_iswrite'] = !empty($iswrite[$row]) ?  $iswrite[$row] : '0';
					$data['module_isedit'] = !empty($isedit[$row]) ?  $isedit[$row] : '0';
					$data['module_isdelete'] = !empty($isdelete[$row]) ?  $isdelete[$row] : '0';		
					$data['module_isowner'] = !empty($isowner[$row]) ?  $isowner[$row] : '0';			
					$resultx = $this->module_role_model->save($data);
				}
			}
		}
		if($result ['rows'] > 0)
		{
			redirect_url(base_url().'role','บันทึกข้อมูลเรียบร้อยแล้ว');
		}else{
			redirect_url(base_url().'role','ไม่สามารถบันทึกข้อมูลได้');
		}
	}
	
	public function check_dup()
	{
		$result = $this->role_model->check_dup($_POST['name']);
		if(!empty($result)){
			echo false;
		}else{
			echo true;
		}
	}
	
}
