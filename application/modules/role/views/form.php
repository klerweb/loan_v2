<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title"><?php echo !empty($id) ? 'แก้ไข' : 'เพิ่ม';?>ข้อมูล</h4>
	</div>
	<div class="panel-body">
    <form id="frm1" name="frm1" method="post" action="<?php echo base_url().'role/save'?>" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-sm-3 form-group">
	    	<label class="control-label">ชื่อสิทธิการใช้งาน</label>
	    </div>
		<div class="col-sm-3 form-group">
	    	<input type="text" name="role_name" id="role_name" class="form-control" value="<?php echo $result['role_name'];?>" <?php echo $id ? 'disabled':'';?>>
		</div>  
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
	    	<label class="control-label">สถานะ</label>
	    </div>
	    <div class="col-sm-3 form-group">
	        	<div class="row" >
	       			<div class="col-sm-6">
	            		<input type="radio" id="role_status1" name="role_status" value="1" <?php echo $result['role_status']!='0' ? 'checked="checked"' : ''?>>
		            	<label>Active</label>
	                </div>
	                <div class="col-sm-6">
	            		<input type="radio" id="role_status0" name="role_status" value="0" <?php echo $result['role_status']=='0' ? 'checked="checked"' : ''?>>
		            	<label>Cancel</label>
	                </div>
	       		</div>
		 </div>
	 </div>
	  <div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h5>รายการเมนู</h5>
		</div>
	</div>
	<div class="row">
		<?php 
		if (!empty($menu)){
			foreach ($menu as $row) {
				if($row['module_status'] == '1'){
					
				 	$selected = array();
	            	if (!empty($role_module)){
	            		foreach ($role_module as $row2) {
	            			if($row['module_id'] == $row2['module_id']){
	            				$selected['isread'] = !empty($row2['module_isread']) ? 'checked' : ''; 
	            				$selected['iswrite'] = !empty($row2['module_iswrite']) ? 'checked' : '';
	            				$selected['isedit'] = !empty($row2['module_isedit']) ? 'checked' : '';
	            				$selected['isdelete'] = !empty($row2['module_isdelete']) ? 'checked' : '';
	            				$selected['isowner'] = !empty($row2['module_isowner']) ? 'checked' : '';
	            				break;
	            			}
	            		}
	            	}
					
					echo '<div class="col-sm-3 form-group">
						    	<label class="control-label">'.$row['module_name'].'</label>
						    	<input name="module_id['.$row['module_id'].']" type="hidden" value="'.$row['module_id'].'" />	 
						  </div>
						  <div class="col-sm-9 form-group">	
							    	<div class="col-sm-2">
										<input type="checkbox" value="1" name="isread['.$row['module_id'].']" id="isread_'.$row['module_id'].'" '.@$selected['isread'].'>
										<label>Read</label>
									</div>
									<div class="col-sm-2">
										<input type="checkbox" value="1" name="iswrite['.$row['module_id'].']" id="iswrite_'.$row['module_id'].'" '.@$selected['iswrite'].'>
										<label>Write</label>
									</div>
									<div class="col-sm-2">
										<input type="checkbox" value="1" name="isedit['.$row['module_id'].']" id="isedit_'.$row['module_id'].'" '.@$selected['isedit'].'>
										<label>Edit</label>
									</div>
									<div class="col-sm-2">
										<input type="checkbox" value="1" name="isdelete['.$row['module_id'].']" id="isdelete_'.$row['module_id'].'" '.@$selected['isdelete'].'>
										<label>Delete</label>
									</div>
									<div class="col-sm-2">
										<input type="checkbox" value="1" name="isowner['.$row['module_id'].']" id="isowner_'.$row['module_id'].'" '.@$selected['isowner'].'>
										<label>Owner</label>
									</div>
						   </div>';
				}
			}
		}
		?> 
	</div>
	 <input type="hidden" name="role_id" value="<?php echo $id;?>">
	</form>
	</div><!-- panel-body -->
	<div class="panel-footer right">
         <button id="btnSubmit" class="btn btn-primary">บันทึก</button>
	</div><!-- panel-footer -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>
<script>
        jQuery(document).ready(function () {     

        	var response;
            $.validator.addMethod(
                "check_dup", 
                function(value, element) {
                    $.ajax({
                        type: "POST",
                        url: base_url+"role/check_dup",
                        data: "name="+value,
                        dataType:"html",
                        success: function(msg)
                        {
                            response = ( msg == true ) ? true : false;
                        }
                     });
                    return response;
                },
                "Name is Already !"
            );
             
        	$("#frm1").validate({
        		rules: {
        	        "role_name":{
        	        	//check_dup:true,
        	        	required: true
        	        },
        		},
        		highlight: function(element) {
        			$(element).closest(".form-group").removeClass("has-success").addClass("has-error");
        		},
        		unhighlight: function(element) {
        	        $(element).closest('.form-group').removeClass('has-error');
        	    },
        		errorElement: 'span',
        	    errorClass: 'help-block',
        	    errorPlacement: function(error, element) {
        	        if(element.parent('.input-group').length) {
        	            error.insertAfter(element.parent());
        	        } else {
        	            error.insertAfter(element);
        	        }
        	    },
        		success: function(element) {
        			$(element).closest(".form-group").removeClass("has-error");
        		}		
        	});

        	jQuery('#btnSubmit').click(function (e) {
            	$('#frm1').submit();
            });
        });
</script>
