<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model
{
	private $table = 'sys_role';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function search($fullname='')
	{
		$this->db->select("*");
		$this->db->from($this->table);		
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function check_dup($name='')
	{
		$this->db->select("*");
		$this->db->from($this->table);		
		$this->db->where('role_name', $name);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function get_table_by_id($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("role_id", $id);
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->row_array() : null;
		return $result;
	}
		
	public function save($array, $id='')
	{
		unset($array['role_id']);
		if($id=='')
		{			
			$array['role_createdate'] = date($this->config->item('log_date_format'));
			$array['role_createby'] = get_uid_login();
    
			$this->db->set($array);
			$this->db->insert($this->table);
			$id = $this->db->insert_id();
		}
		else
		{			
			unset($array['role_name']);
			$array['role_updatedate'] = date($this->config->item('log_date_format'));
			$array['role_updateby'] = get_uid_login();
			$this->db->set($array);
			$this->db->where("role_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>