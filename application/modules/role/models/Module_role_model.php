<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module_role_model extends CI_Model
{
	private $table = 'sys_role_module';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function search($role_id='')
	{
		$this->db->select("*");
		$this->db->from($this->table);	
		$this->db->where("role_id",$role_id);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}
	
	public function get_table_by_id($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("user_role_id", $id);
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->row_array() : null;
		return $result;
	}
	
	public function save($array, $id='')
	{
		unset($array['role_module_id']);
		if($id=='')
		{			
			$array['role_module_createdate'] = date($this->config->item('log_date_format'));
			$array['role_module_createby'] = get_uid_login();
    
			$this->db->set($array);
			$this->db->insert($this->table);
			$id = $this->db->insert_id();
		}
		else
		{			
			$array['role_module_updatedate'] = date($this->config->item('log_date_format'));
			$array['role_module_updateby'] = get_uid_login();
			$this->db->set($array);
			$this->db->where("role_module_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function delete_by_roleid($role_id)
	{
		$this->db->delete($this->table,array('role_id'=>$role_id));
	}
}
?>