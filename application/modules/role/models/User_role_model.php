<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_role_model extends CI_Model
{
	private $table = 'sys_user_role';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function search($user_id='')
	{
		$this->db->select("*");
		$this->db->from($this->table);	
		$this->db->where("user_id",$user_id);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}
	
	public function get_table_by_id($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("user_role_id", $id);
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->row_array() : null;
		return $result;
	}
	
	public function save($array, $id='')
	{
		unset($array['user_role_id']);
		if($id=='')
		{
			$array['user_role_createdate'] = date($this->config->item('log_date_format'));
			$array['user_role_createby'] = get_uid_login();
    
			$this->db->set($array);
			$this->db->insert($this->table);
			$id = $this->db->insert_id();
		}
		else
		{			
			$array['user_role_updatedate'] = date($this->config->item('log_date_format'));
			$array['user_role_updateby'] = get_uid_login();
			$this->db->set($array);
			$this->db->where("user_role_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function delete_by_userid($user_id)
	{
		$this->db->delete($this->table,array('user_id'=>$user_id));
	}
}
?>