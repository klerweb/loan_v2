
<script type="text/javascript">

//id_card: $("#id_card_search").val(),name:$("#name_search").val(),contact_id:$("#contact_id").val()
function fncSubmit()
{
	if ($("#id_card_search").val()=="" && $("#name_search").val()=="" && $("#contact_id").val()=="")
	 {
		 alert("กรุณาใส่ข้อมูลที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}
//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
</script>
<?php $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
?>

<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url()?>loan_payment/payment'">เพิ่มการรับชำระหนี้เงินกู้</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('loan_payment/search_all_payment'); ?>" >
					<div class="form-group">
						<label class="sr-only" for="id_card_search">บัตรประชาชน</label>
						<input type="text" class="form-control" id="id_card_search" name="id_card_search" autofocus="autofocus"  placeholder="<?php if(isset($txt_id_card)==false)
{
	echo "หมายเลขบัตรประชาชน";
} else { echo $txt_id_card; } ?>" maxlength="13" onkeydown="number();"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="name_search">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="name_search" name="name_search" autofocus="autofocus" placeholder="<?php if(isset($txt_name)==false)
{
	echo "ชื่อนามสกุลผู้กู้";
} else { echo $txt_name; } ?>" maxlength="200"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="contact_id">เลขที่สัญญา</label>
						<input type="text" class="form-control" id="contact_id" name="contact_id" autofocus="autofocus" placeholder="<?php if(isset($txt_contact_id)==false)
{
	echo "เลขที่สัญญา";
} else { echo $txt_contact_id; } ?>" maxlength="50" /> 
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30" id="table_show_loan_withdraw" >
				<thead>
					<tr>
		            	<th>#</th>
						
            <th>เลขที่สัญญาเงินกู้</th>
            <th>ชื่อผู้กู้</th>
			<th style='text-align:right;'>จำนวนเงินกู้</th>
			<th style='text-align:right;'>จำนวนเงินกู้คงเหลือ</th>
			<th style='text-align:right;'>จำนวนเงินที่จ่าย</th>
			 <th >วันที่ครบกำหนดชำระ</th>
			<th>วันที่จ่าย</th>
			<th>ใบเสร็จรับเงิน</th>
			
				</thead>
				<tbody>
<?php	if($loan_payment!="no data")
{
	$i_no = 1;
$pieces_loan_payments = explode("\\", $loan_payment);
for ($show_array=0;$show_array<count($pieces_loan_payments)-1;$show_array++)
{
	$split2_loan_payments=explode(",",$pieces_loan_payments[$show_array]);
	echo "<tr >";
		echo "<td>".$i_no++."</td>";
		
		echo "<td>".$split2_loan_payments[1]."</td>";
		echo "<td>".$split2_loan_payments[2]."</td>";
		
		echo "<td style='text-align:right;'>".number_format($split2_loan_payments[4],2)."</td>";
		echo "<td style='text-align:right;'>".number_format($split2_loan_payments[5],2)."</td>";
		
		echo "<td style='text-align:right;'>".number_format($split2_loan_payments[10],2)."</td>";
		echo "<td>";
			$split_day1=explode("-", $split2_loan_payments[3]);

$year1=intval($split_day1[0])+543;
echo intval($split_day1[2])." ".$month[intval($split_day1[1])-1]." ".$year1."</td>";
	
	
		echo "<td>";
		$split_day=explode("-", $split2_loan_payments[11]);

$year=intval($split_day[0])+543;
echo intval($split_day[2])." ".$month[intval($split_day[1])-1]." ".$year."</td>";
		echo "<td >"; ?>
		<a href="<?php echo site_url()?>loan_payment/receipt_payment/<?php
		$split_data_payment_no=explode("/",$split2_loan_payments[0]);
		echo $split_data_payment_no[0]."_".$split_data_payment_no[1];
		?>" target="_blank">
		<?php echo $split2_loan_payments[0]; ?>
		</a></td> 
		
		
		
	<?php echo "</tr>"; 
	


 }
}
?>


	</tbody>
</table>
		</div><!-- table-responsive -->
	</div>
</div>	