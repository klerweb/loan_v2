<?php echo $show_receipt_payment;?>
<script type="text/javascript" src="<?php echo site_url(); ?>assets/js/modules_finance_disburse/jspdf.debug.js"></script>
<?php $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
$split_data=explode(",",$show_receipt_payment);
?>
<style type="text/css">
html, body{
padding:0px;
margin:0px;
height:100%;
}
td.end{
    border:1px solid black;
}
</style>

<table width="100%" height="0%" align="center" id="show_receipt_payment" border="0">
<tr>
<td rowspan="3" colspan="4"><center><img src="<?php echo site_url(); ?>assets/images/logo_profile.png"></img></center></td>
<td colspan="20"><?php echo "สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)";?></td>
</tr>
<tr >
<td colspan="24"><?php echo "เลขที่ 469 ถนนนครสวรรค์ แขวงสวนจิตรลดา เขตดุสิต กรุงเทพฯ 10300 ";?></td>
</tr >
<tr>
<td colspan="24"><?php echo "โทร. 0-2629-8884    โทรสาร 0-2629-877";?></td>
</tr>
<tr >
<td colspan="24"><p align="right"><?php echo "ใบเสร็จรับเงินเลขที่  : ".$split_data[0];?></p></td>
</tr>
<tr >
<td colspan="24"><p align="right"><?php 
$split_date=explode("-",$split_data[1]);
echo "วันที่ออกใบเสร็จ  : ".$split_date[2]." ".$month[(intval($split_date[1])-1)]." ".(intval($split_date[0])+543);?>
</p></td>
</tr>
<tr >
<td colspan="24"><p align="center"><?php echo "ใบเสร็จรับเงินสินเชื่อ".$split_data[2];?></p></td>
</tr>
<tr>
<table width="100%" height="0%" align="center" id="detail_receipt_payment" border="0">
<tr>
	<td><table width="100%" height="0%" align="center" id="part1" border="1"> <?php // table part_1  ?>
	<tr>
		<td colspan="12">
			<table width="100%" height="0%" id="part1_1" border="0"> <?php // table part_1_1  ?>
				<tr>
					<td>ชื่อผู้กู้ :<?php echo $split_data[3];?> </td>	
				</tr>
				<tr>
					<td >ที่อยู่ :<?php echo $split_data[4];?></td>
				</tr>
				<tr>
					<td><?php echo $split_data[5];?></td>
				</tr>
				<tr>
					<td >เบอร์โทรศัพท์ :<?php echo $split_data[6];?> </td>
				</tr>
				<tr>
					<td >&nbsp</td>
				</tr>
			</table> <?php // end table part_1_1  ?>
		</td>
		<td colspan="12">
			<table width="100%" height="0%" id="part1_2" border="0"> <?php // table part_1_2  ?>
				<tr>
					<td >วันที่ชำระ : <?php 
$split_date=explode("-",$split_data[7]);
echo $split_date[2]." ".$month[(intval($split_date[1])-1)]." ".(intval($split_date[0])+543);?>
</td>
				</tr>
				<tr>
					<td >เลขที่สัญญา :<?php echo $split_data[8];?></td>
				</tr>
				<tr>
					<td >รหัสลูกหนี้ :<?php echo $split_data[9];?></td>
				</tr>
				<tr>
					<td >อัตราดอกเบี้ย :<?php echo $split_data[10]."%";?></td>
				</tr>
				<tr>
					<td >งวดที่ :<?php echo $split_data[11];?></td>
				</tr>
			</table> <?php // table part_1_2  ?>
		</td>
	</tr>
	</table> <?php // end table part_1  ?>
	</td>
</tr> 
<tr>
	<td><table width="100%" height="0%" align="center" id="part2" border="0"> <?php // table part_2  ?>
	<tr>
	<td colspan="12" class="end"><p align="center">รายการ</p></td>
	<td colspan="12" class="end"><p align="center">จำนวนเงิน</p></td>
	</tr>
	<?php//--------------------------------------- ?>
	<tr>
	<td colspan="12" class="end">
	<p align="left">จำนวนเงินที่ชำระ </p>
	<p align="left">ค่าธรรมเนียม</p>
	<p align="left">ดอกเบี้ย</p>
	<p align="left">เงินต้น</p>
	<p align="left">ค่าธรรมเนียมคงเหลือ</p>
	<p align="left">ดอกเบี้ยคงเหลือ</p>
	<p align="left">เงินต้นค้างชำระ</p>
	<p align="left">เงินต้นคงเหลือสุทธิ</p>
	</td>
	<td colspan="12" class="end">
	<p align="right"><?php echo number_format($split_data[12],2);?></p>
	<p align="right"><?php echo number_format(0,2); ?></p>
	<p align="right"><?php echo number_format($split_data[14],2); ?></p>
	<p align="right">
					<?php if($split_data[12]>$split_data[14]){
					if($split_data[12]>=$split_data[14]+$split_data[13])
						{echo number_format(($split_data[12]-$split_data[14]),2);}
					else
					{ echo number_format(($split_data[12]-$split_data[14]),2);;}
					}
					else if($split_data[12]<=$split_data[14])
					{
						echo number_format(0,2);
					}
					?>
					</p>
					<p align="right"><?php echo number_format(0,2); ?></p>
					<p align="right">
					<?php if($split_data[12]<=$split_data[14])
					{echo number_format(($split_data[14]-$split_data[12]),2);}
					else
					{ echo number_format(0,2);}?></p>
					<p align="right">
					<?php if($split_data[12]>$split_data[14]){
					if($split_data[12]>=$split_data[14]+$split_data[13])
						{echo number_format(0,2);}
					else
					{ 
						echo number_format(($split_data[13]-($split_data[12]-$split_data[14])),2);}
					}
					else if($split_data[12]<=$split_data[14])
					{
						echo number_format($split_data[13],2);
					}
					?>
					</p>
					<p align="right"><?php echo number_format($split_data[15],2);?></p>
	</td>
	</tr>
	




















	<?php//-------------------------------?>
	</table> <?php // end table part_2  ?>
	</td>
</tr>
<?php//-----------------------------------?>
</table> <?php // table detail_receipt_payment  ?>
</tr>
</table> <?php // table show_receipt  ?>
