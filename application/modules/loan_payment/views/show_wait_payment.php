<script type="text/javascript">

//id_card: $("#id_card_search").val(),name:$("#name_search").val(),contact_id:$("#contact_id").val()
function fncSubmit()
{
	if ($("#id_card_search").val()=="" && $("#name_search").val()=="" && $("#contact_id").val()=="")
	 {
		 alert("กรุณาใส่ข้อมูลที่ต้องการค้นหา");
		 return false;
	 }
	 else
	 {
		 return true;
	 }
}
//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
</script>
<style>
.divVerdana {font-family: verdana;}
.divTimes {font-family: 'times new roman';}

#div1 {font-size-adjust: 0.58;}
#div2 {font-size-adjust: 0.58;}
</style>
<?php $month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
?>


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('loan_payment/search_wait_payment'); ?>" onSubmit="JavaScript:return fncSubmit();">
					<div class="form-group">
						<label class="sr-only" for="id_card_search">บัตรประชาชน</label>
						<input type="text" class="form-control" id="id_card_search" name="id_card_search" autofocus="autofocus"  placeholder="<?php if(isset($txt_id_card)==false)
{
	echo "หมายเลขบัตรประชาชน";
} else { echo $txt_id_card; } ?>" maxlength="13" onkeydown="number();"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="name_search">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="name_search" name="name_search" autofocus="autofocus" placeholder="<?php if(isset($txt_name)==false)
{
	echo "ชื่อนามสกุลผู้กู้";
} else { echo $txt_name; } ?>" maxlength="200"/> 
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="contact_id">เลขที่ใบแจ้งหนี้</label>
						<input type="text" class="form-control" id="contact_id" name="contact_id" autofocus="autofocus" placeholder="<?php if(isset($txt_contact_id)==false)
{
	echo "เลขที่ใบแจ้งหนี้";
} else { echo $txt_contact_id; } ?>" maxlength="50" /> 
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">ค้นหา</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30" id="table_show_loan_withdraw" width="100%" height="100%">
				<thead>
					<tr>
		            	<th><p align="center">#</p></th>
						<th><p align="left">เลขที่ใบแจ้งหนี้</p></th>
            <th><p align="left">เลขที่สัญญาเงินกู้</p></th>
            <th><p align="center">เชื่อผู้กู้</p></th>
            <th><p align="right">จำนวนเงินกู้</p></th>
			<th><p align="right">จำนวนเงินกู้คงเหลือ</p></th>
			<th><p align="right">จำนวนเงินที่ต้องจ่าย</p></th>
			<th><p align="left">วันที่ครบกำหนดจ่าย</p></th>
			<th><p align="left">จัดการ</p></th>
				</thead>
				<tbody>
<?php	if($loan_wait_payment!="no data")
{
	$i_no = 1;
	$pieces_loan_payments = explode("\\", $loan_wait_payment);
	$check_loop=array();
	for($j=0;$j<count($pieces_loan_payments);$j++)
	{
		$b=0;
		if($j==0)
		{
			$check_loop[]=$pieces_loan_payments[0];
		}
		else
		{
			for($k=0;$k<count($check_loop);$k++)
			{
				if($pieces_loan_payments[$j]==$check_loop[$k])
				{
					$b=1;
				}
			}
			if($b==0)
			{
				$check_loop[]=$pieces_loan_payments[$j];
			}
		}
	}
	//print_r($check_loop);

for ($show_array=0;$show_array<count($check_loop)-1;$show_array++)
{
	$split2_loan_payments=explode(",",$check_loop[$show_array]);
	echo "<tr >";
		echo "<td>".$i_no++."</td>";
		echo "<td >".$split2_loan_payments[0]."</td>"; ?>
		<?php echo "<td>".$split2_loan_payments[1]."</td>";
		echo "<td>".$split2_loan_payments[2]."</td>";
		//echo "<td>".$split2_loan_payments[3]."</td>";
		echo "<td><p align='right'>".number_format($split2_loan_payments[4],2)."</p></td>";
		echo "<td><p align='right'>".number_format($split2_loan_payments[5],2)."</p></td>";
		
		echo "<td><p align='right'>".number_format(($split2_loan_payments[6]+$split2_loan_payments[7]),2)."</p></td>";
		echo "<td>";
		$split_day=explode("-", $split2_loan_payments[8]);

$year=intval($split_day[0])+543;
echo intval($split_day[2])." ".$month[intval($split_day[1])-1]." ".$year."</td>";
	
	?>
		<td><button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ชำระหนี้" data-original-title="ชำระหนี้" onclick="
							if(confirm('คุณต้องการบันทึกการรับชำระหนี้ใช่หรือไม่?'))
	{ location.href='<?php echo site_url()?>loan_payment/save_receive_payment/<?php echo $split2_loan_payments[0];?>';

	}"><span class="fa fa-edit"></span></button></td>
	<?php echo "</tr>"; 
	


 }
}
?>


	</tbody>
</table>
		</div><!-- table-responsive -->
	</div>
</div>	