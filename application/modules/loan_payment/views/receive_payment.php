
<script type="text/javascript">
$(document).ready(function(){
	$('#ddlTitle').on('change', function()
	{
		
		var data_old=$('#ddlObjective').val();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable",
			data: {}})
			.success(function(result) {
				
				var split_data_person=result.split("\\");
				
				var count_data = $("#ddlObjective").children().length;
				for(var i=1;i<=count_data;i++)
				{
					$("#ddlObjective").children(i).remove();
			
				}
				$("#ddlObjective").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 2"));
				//alert(split_data_person.length);
				for(var i=0;i<split_data_person.length-1;i++)
				{
						var split_person=split_data_person[i].split(",");
						if ($('#ddlTitle').val()==0)
						{
							$("#ddlObjective").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
						else if (split_person[0]!=$('#ddlTitle').val())
						{
							$("#ddlObjective").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
				}
				if (data_old==$('#ddlTitle').val())
				{
					$("#ddlObjective").val(0);
				}
				else if (data_old!=$('#ddlTitle').val())
				{
					$("#ddlObjective").val(data_old);
				}
			});

	});
	$('#ddlObjective').on('change', function()
	{
		var data_old=$('#ddlTitle').val();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "finance_disburse/show_person_payable",
			data: {}})
			.success(function(result) {
				//alert(result);
				var split_data_person=result.split("\\");
				
				var count_data = $("#ddlTitle").children().length;
				for(var i=1;i<=count_data;i++)
				{
					$("#ddlTitle").children(i).remove();
			
				}
				$("#ddlTitle").append($("<option></option>").val(0).html("ผู้่อนุมัติเบิกเงิน คนที่ 1"));
				//alert(split_data_person.length);
				for(var i=0;i<split_data_person.length-1;i++)
				{
						var split_person=split_data_person[i].split(",");
						if ($('#ddlObjective').val()==0)
						{
							$("#ddlTitle").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
						else if (split_person[0]!=$('#ddlProvince').val())
						{
							$("#ddlTitle").append($("<option></option>").val(split_person[0]).html(split_person[1]));
						}
				}
				if (data_old==$('#ddlObjective').val())
				{
					$("#ddlTitle").val(0);
				}
				else if (data_old!=$('#ddlObjective').val())
				{
					$("#ddlTitle").val(data_old);
				}
			});

	});
	$("#save_data").click(function(){
		
		var b=true;
		if($('#loan_contract_no').val()=="")
		{
			alert("กรุณาใส่เลขที่สัญญาเงินกู้");
			b=false;
		}
		else if ($("#ddlStatus").val()==0)
		{
			alert("กรุณาเลือกธนาคาร");
			b=false;
		}
		else if ($("#txtDate").val()=="")
		{
			alert("กรุณาเลือกวันที่โอนให้ถูกต้อง");
			b=false;
		}
		
		else if ($("#money_tran").val()==0)
		{
			alert("กรุณาใส่จำนวนเงินโอน");
			b=false;
		}
		if (b)
		{
			if (confirm("กรุณาตรวจสอบข้อมูลอีกครั้งก่อนกดบันทึกเพราะจะไม่สามารถแก้ไขรายการได้ในภายหลัง!") == true) {
			var loan_id=$('#loan_id').val();
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "loan_payment/save_loan_pay",
			data: {loan_contract_no:$("#loan_contract_no").val(),bank:$("#ddlStatus").val(), date_tranfer:$("#txtDate").val(),money_transfer:$("#money_tran").val()}})
			.success(function(result2) {
				
				if(result2=="success")
				{
					
					window.location.href='<?php echo site_url()?>loan_payment/payment';
				}
				else
				{
					alert(result2);
				}
			});
		}
		}
		
	});
		});


//---------------------------check number--------------------
function number(){
	
	
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (event.keyCode === 65 && (event.ctrlKey === true || event.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (event.keyCode >= 35 && event.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }
		
		$('#Amount').keypress(function(e){ 
   if (this.value.length == 0 && e.which == 48 ){
      return false;
   }
});
	}
//-------------------------------end check number
//------------id_card
function autoTab(obj){
    /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย
    หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น  รูปแบบเลขที่บัตรประชาชน
    4-2215-54125-6-12 ก็สามารถกำหนดเป็น  _-____-_____-_-__
    รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____
    หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__
    ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบเลขบัตรประชาชน
    */
        var pattern=new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้
        var pattern_ex=new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้
        var returnText=new String("");
        var obj_l=obj.value.length;
        var obj_l2=obj_l-1;
        for(i=0;i<pattern.length;i++){           
            if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){
                returnText+=obj.value+pattern_ex;
                obj.value=returnText;
            }
        }
        if(obj_l>=pattern.length){
            obj.value=obj.value.substr(0,pattern.length);           
        }
}
//--------------end id card-----------
function check_data()
{
			var loan_contract_no=$('#loan_contract_no').val();
			$.ajax({
			type: "POST",
			url: "<?php echo site_url(); ?>" + "loan_payment/check_contract",
			data: {loan_contract_no:$('#loan_contract_no').val()}})
			.success(function(result2) {
				if(result2=="no data")
				{
					$('#lo_name').val("");
					$('#id_card').val("");
					$('#lo_amount_all').val("");
					$('#lo_amount').val("");
					$('#ddlStatus').val("");
					$('#txtDate').val("");
				}
				else
				{
					var split_data=result2.split(",");
					$('#lo_name').val(split_data[1]);
					$('#id_card').val(split_data[2]);
					$('#lo_amount_all').val(chkNum(split_data[3]));
					$('#lo_amount').val(chkNum(split_data[4]));
					$('#ddlStatus').val("");
					$('#txtDate').val("");
				}
			});
}
function addCommas(nStr)
			{
				nStr += '';
				x = nStr.split('.');
				x1 = x[0];
				x2 = x.length > 1 ? '.' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}
function chkNum(ele)
			{
				var num = parseFloat(ele);
				return addCommas(num.toFixed(2));
			}
</script>


<?php
if($load_payment!="no data")
{
	$split_data=explode('\\',$load_payment);
	$split_data2=explode(",",$split_data[0]);
}
?>
<form>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">รับชำระหนี้เงินกู้</h4>
				</div>
				<div class="panel-body">
				<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เลขที่สัญญาเงินกู้<span class="asterisk"></span></label>
								<input type="text" name="loan_contract_no" id="loan_contract_no"  maxlength="255" class="form-control" value="<?php echo $split_data2[1];?>" onkeyup="check_data();"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ชื่อผู้กู้<span class="asterisk"></span></label>
								<input type="text" name="lo_name" id="lo_name" maxlength="17" value="<?php echo $split_data2[2];?>" disabled="disabled" class="form-control" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมายเลขบัตรประชาชน<span class="asterisk"></span></label>
								<input type="text" name="id_card" id="id_card"  maxlength="13" disabled="disabled" class="form-control" value="<?php echo $split_data2[3];?>"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">จำนวนเงินกู้<span class="asterisk"></span></label>
								<input type="text" name="lo_amount_all" id="lo_amount_all" class="form-control" value="<?php echo number_format($split_data2[8],2);?>" disabled="disabled"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">จำนวนเงินกู้คงเหลือ<span class="asterisk"></span></label>
								<input type="text" name="lo_amount" id="lo_amount" class="form-control" value="<?php echo number_format($split_data2[4],2);?>" disabled="disabled"/>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ชำระเงินผ่าน <span class="asterisk">*</span></label>
								<select name="ddlStatus" id="ddlStatus" data-placeholder="กรุณาเลือกบัญชีธนาคาร" class="width100p" required>
									<option value="">กรุณาเลือกบัญชีธนาคาร</option>
									<?php
									$data_bank=explode("\\",$bank_payment);
									for ($array_bank=0;$array_bank<count($data_bank)-1;$array_bank++)
									{ 
										$split_data_bank=explode(",",$data_bank[$array_bank]);
								?>
								<option value=<?php echo $split_data_bank[0];?>>  <?php echo $split_data_bank[0]." ".$split_data_bank[1]." ".$split_data_bank[2];?> </option>
							<?php }
							?>
							</select> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">จำนวนเงินที่โอน <span class="asterisk">*</span></label>
								<input type="text" name="money_tran" id="money_tran"  maxlength="10" onkeydown="number();" class="form-control" value=""/> 
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">วันที่รับชำระ<span class="asterisk">*</span></label>
								<div class="input-group">
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="" readonly="readonly"/>
									<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						
					</div><!-- row -->

					
					
				<div class="panel-footer" style="text-align:right;">
					<button type="button" class="btn btn-primary" id="save_data" name="save_data">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>