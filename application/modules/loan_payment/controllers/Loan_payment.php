<?php
class loan_payment extends MY_Controller{
	private $title_page = 'ข้อมูลการรับชำระหนี้เงินกู้';
	function __construct()
	{
		parent::__construct();
	}



	public function index()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		$data_content = array(
				
				'loan_payment' => $this->loan_payment_m->load_loan_payments()
		);
		
		$data = array(
				'title' => $this->title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('loan_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_all_loan_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_loan_payment=$this->loan_payment_m->search_payment();
		echo $data_loan_payment;
	}
	public function receipt_payment1()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data['show_receipt_payment']=$this->loan_payment_m->show_receipt_payment($this->uri->segment(3));
		$this->load->view('receipt_payment_print',$data);
	}
	public function Convert($amount_number)
{
    $amount_number = number_format($amount_number, 2, ".","");
    $pt = strpos($amount_number , ".");
    $number = $fraction = "";
    if ($pt === false) 
        $number = $amount_number;
    else
    {
        $number = substr($amount_number, 0, $pt);
        $fraction = substr($amount_number, $pt + 1);
    }
    
    $ret = "";
    $baht = $this->ReadNumber ($number);
    if ($baht != "")
        $ret .= $baht . "บาท";
    
    $satang = $this->ReadNumber($fraction);
    if ($satang != "")
        $ret .=  $satang . "สตางค์";
    else 
        $ret .= "ถ้วน";
    return $ret;
}

public function ReadNumber($number)
{
    $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
    $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
    $number = $number + 0;
    $ret = "";
    if ($number == 0) return $ret;
    if ($number > 1000000)
    {
        $ret .= $this->ReadNumber(intval($number / 1000000)) . "ล้าน";
        $number = intval(fmod($number, 1000000));
    }
    
    $divider = 100000;
    $pos = 0;
    while($number > 0)
    {
        $d = intval($number / $divider);
        $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
            ((($divider == 10) && ($d == 1)) ? "" :
            ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
        $ret .= ($d ? $position_call[$pos] : "");
        $number = $number % $divider;
        $divider = $divider / 10;
        $pos++;
    }
    return $ret;
}
	
	public function receipt_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		//$data['show_receipt_payment']=$this->loan_payment_m->show_receipt_payment($this->uri->segment(3));
		//$this->load->view('receipt_payment_print',$data);
		$data=$this->loan_payment_m->show_receipt_payment($this->uri->segment(3));
		ob_start();
		$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$split_data=explode(",",$data);
		$this->load->library('tcpdf');
		
		//$data = $this->non_loan->getById($id);
				
		$filename = 'receipt_no.'.$this->uri->segment(3);
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
		$pdf->SetTitle($filename);//  กำหนด Title
		$pdf->SetSubject('Export receipt'); // กำหนด Subject
		$pdf->SetKeywords($filename); // กำหนด Keyword
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// add a page
		$pdf->AddPage();
		$split_date=explode("-",$split_data[1]);
		$split_date2=explode("-",$split_data[7]);
		if($split_data[12]>$split_data[14]){
					if($split_data[12]>=$split_data[14]+$split_data[13])
						{$num1=number_format(($split_data[12]-$split_data[14]),2);}
					else
					{ $num1=number_format(($split_data[12]-$split_data[14]),2);;}
					}
					else if($split_data[12]<=$split_data[14])
					{
						$num1=0;
					}
					if($split_data[12]<=$split_data[14])
					{$num2=number_format(($split_data[14]-$split_data[12]),2);}
					else
					{ $num2=0;}
					if($split_data[12]>$split_data[14]){
					if($split_data[12]>=$split_data[14]+$split_data[13])
						{$num3=0;}
					else
					{ 
						$num3=number_format(($split_data[13]-($split_data[12]-$split_data[14])),2);}
					}
					else if($split_data[12]<=$split_data[14])
					{
						$num3=number_format($split_data[13],2);
					}
					$split_payment_no=explode("_",$split_data[0]);
		$htmlcontent ='<table width="100%" border="1" cellpadding="0" cellspacing="0">
			<tr>

				<td width="100%">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets/images/logo_profile.png" style="width:60px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 16px;">เลขที่ 210 สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน เขตพญาไท </td>
						</tr>
					</table>
					
					<table width="100%" border="" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 16px;">กรุงเทพมหานครฯ 10400 โทร.0 2278 1648 โทรสาร 0 2274 1148</td>
						</tr>
					</table>
				</td>
				
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-size: 18px; text-align:center;">ใบเสร็จรับเงิน</td>
							<td width="40%" style="font-size: 18px; text-align:center;">เลขที่ '.$split_payment_no[0]."/".$split_payment_no[1].'</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-size: 18px; text-align:left;">ได้รับเงินจาก '.$split_data[3].					'<br>ที่อยู่ : '.$split_data[4].' '.$split_data[5].'</td>
							<td width="40%" style="font-size: 18px; text-align:left;">วันที่ '.$split_date2[2].' '.$month[(intval($split_date2[1])-1)].' '.(intval($split_date2[0])+543).'<br>เลขที่สัญญา   '.$split_data[8].'<br>วงเงินกู้ (บาท) '.number_format($split_data[19],2).'<br>อัตราดอกเบี้ย  ร้อยละ  '.$split_data[10].'<br>จำนวนงวดชำระ </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-sizne: 18px; text-align:left;">
								<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="30%" style="font-size: 18px; text-align:center;">วัน/เดือน/ปี <br>ที่รับเงิน</td>
										<td width="10%" style="font-size: 18px; text-align:center;">งวดที่</td>
										<td width="30%" style="font-size: 18px; text-align:center;">เงินต้น<br>(บาท)</td>
										<td width="30%" style="font-size: 18px; text-align:center;">ดอกเบี้ย<br>(บาท)</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="font-size: 8px; text-align:left;">
							<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="50%" style="font-size: 18px; text-align:center;">ยอดชำระทั้งสิ้น<br>(บาท)</td>
										<td width="50%" style="font-size: 18px; text-align:center;">เงินต้นคงเหลือ<br>(บาท)</td>
										
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-sizne: 18px; text-align:left;">
								<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="30%" style="font-size: 18px; text-align:center;">'.$split_date2[2].' '.$month[(intval($split_date2[1])-1)].' '.(intval($split_date2[0])+543).'</td>
										<td width="10%" style="font-size: 18px; text-align:center;">'.$split_data[11].'</td>
										<td width="30%" style="font-size: 18px; text-align:right;">'.$num1.'</td>
										<td width="30%" style="font-size: 18px; text-align:right;">'.number_format($split_data[14],2).'</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="font-size: 8px; text-align:left;">
							<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="50%" style="font-size: 18px; text-align:right;">'.number_format($split_data[12],2).'</td>
										<td width="50%" style="font-size: 18px; text-align:right;">'.number_format($split_data[16],2).'</td>
										
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 18px; text-align:left;">            รวม (ตัวอักษร) ('.$this->Convert($split_data[12]).')
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="100%" style="font-size: 18px; text-align:right;"></td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="20%" style="font-size: 18px; text-align:center;">ลงชื่อ</td>
			<td width="40%" style="font-size: 18px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">______________________________</td>
				</tr>
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">เจ้าหน้าที่การเงิน</td>
				</tr>
				</table>
			</td>
			<td width="40%" style="font-size: 18px; text-align:right;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">______________________________</td>
				</tr>
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">ผู้อำนวยการกองการเงินและบัญชี</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br>
		<hr width=80% size=3><p>
		<p align="center"><h1>สำเนา</h1>
		<table width="100%" border="1" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets/images/logo_profile.png" style="width:60px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 16px;">เลขที่ 210 สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน เขตพญาไท </td>
						</tr>
					</table>
					
					<table width="100%" border="" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 16px;">กรุงเทพมหานครฯ 10400 โทร.0 2278 1648 โทรสาร 0 2274 1148</td>
						</tr>
					</table>
				</td>
				
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-size: 18px; text-align:center;">ใบเสร็จรับเงิน</td>
							<td width="40%" style="font-size: 18px; text-align:center;">เลขที่ '.$split_payment_no[0]."/".$split_payment_no[1].'</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-size: 18px; text-align:left;">ได้รับเงินจาก '.$split_data[3].					'<br>ที่อยู่ : '.$split_data[4].' '.$split_data[5].'</td>
							<td width="40%" style="font-size: 18px; text-align:left;">วันที่ '.$split_date2[2].' '.$month[(intval($split_date2[1])-1)].' '.(intval($split_date2[0])+543).'<br>เลขที่สัญญา   '.$split_data[8].'<br>วงเงินกู้ (บาท) '.number_format($split_data[19],2).'<br>อัตราดอกเบี้ย  ร้อยละ  '.$split_data[10].'<br>จำนวนงวดชำระ </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-sizne: 18px; text-align:left;">
								<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="30%" style="font-size: 18px; text-align:center;">วัน/เดือน/ปี <br>ที่รับเงิน</td>
										<td width="10%" style="font-size: 18px; text-align:center;">งวดที่</td>
										<td width="30%" style="font-size: 18px; text-align:center;">เงินต้น<br>(บาท)</td>
										<td width="30%" style="font-size: 18px; text-align:center;">ดอกเบี้ย<br>(บาท)</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="font-size: 8px; text-align:left;">
							<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="50%" style="font-size: 18px; text-align:center;">ยอดชำระทั้งสิ้น<br>(บาท)</td>
										<td width="50%" style="font-size: 18px; text-align:center;">เงินต้นคงเหลือ<br>(บาท)</td>
										
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%">
					<table width="100%" border="1" cellpadding="0" cellspacing="0">
						<tr>
							<td width="60%" style="font-sizne: 18px; text-align:left;">
								<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="30%" style="font-size: 18px; text-align:center;">'.$split_date2[2].' '.$month[(intval($split_date2[1])-1)].' '.(intval($split_date2[0])+543).'</td>
										<td width="10%" style="font-size: 18px; text-align:center;">'.$split_data[11].'</td>
										<td width="30%" style="font-size: 18px; text-align:right;">'.$num1.'</td>
										<td width="30%" style="font-size: 18px; text-align:right;">'.number_format($split_data[14],2).'</td>
									</tr>
								</table>
							</td>
							<td width="40%" style="font-size: 8px; text-align:left;">
							<table width="100%" border="1" cellpadding="0" cellspacing="0">
									<tr>
										<td width="50%" style="font-size: 18px; text-align:right;">'.number_format($split_data[12],2).'</td>
										<td width="50%" style="font-size: 18px; text-align:right;">'.number_format($split_data[16],2).'</td>
										
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 18px; text-align:left;">            รวม (ตัวอักษร) ('.$this->Convert($split_data[12]).')
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="100%" style="font-size: 18px; text-align:right;"></td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="20%" style="font-size: 18px; text-align:center;">ลงชื่อ</td>
			<td width="40%" style="font-size: 18px;">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">______________________________</td>
				</tr>
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">เจ้าหน้าที่การเงิน</td>
				</tr>
				</table>
			</td>
			<td width="40%" style="font-size: 18px; text-align:right;">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">______________________________</td>
				</tr>
				<tr>
					<td width="100%" style="font-size: 18px; text-align:center;">ผู้อำนวยการกองการเงินและบัญชี</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		';
$pdf->SetFont('thsarabun', '', 16);
		$pdf->SetMargins(10, 5, 10, true);
		$pdf->writeHTML($htmlcontent, true, 0, true, true);
		
		$pdf->Output($filename.'.pdf', 'I');
	}
	
	public function payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		
		$data_content = array(
				'bank_payment' => $this->loan_payment_m->load_bank_payment_active(),
				'loan_wait_payment'=>$this->loan_payment_m->show_w_payment()
		);
		$title_page="ใบแจ้งหนี้ที่ยังไม่มีการชำะระเงิน";
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('show_wait_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function show_one_person_loan_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		$data_content = array(
				'loan_one_payment' => $this->loan_payment_m->loan_one_payment($this->uri->segment(3))
		);
		
		$data = array(
				'title' => $this->title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('show_one_person_loan_payments', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	} 
	
	public function edit_receive_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		$data_content = array(
				'bank_payment' => $this->loan_payment_m->load_bank_payment_active(),
				'load_payment'=>$this->loan_payment_m->loan_one_payment($this->uri->segment(3))
				
		);
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		$title_page = 'ข้อมูลการรับชำระหนี้เงินกู้';
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('form_edit_receive_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function save_receive_payment() //ติดค้างอยู่
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$this->title_page="ใบแจ้งหนี้ที่ยังไม่ได้รับชำระเงินกู้";
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment/payment'));
		$data_content = array(
				'bank_payment' => $this->loan_payment_m->load_bank_payment_active(),
				'load_payment'=>$this->loan_payment_m->load_data_invoice($this->uri->segment(3))
		);
		
		$title_page = 'การรับชำระหนี้เงินกู้';
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('receive_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function show_card_debtors()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		$data_content = array(
				'load_debtor' => $this->loan_payment_m->show_c_debtor($this->uri->segment(3))
		);
		$title_page = 'ข้อมูลการรับชำระเงินกู้';
		$data = array(
				'title' => $title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				'js_other' => array('modules_finance_disburse/jquery.table2excel.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('show_card_debtor', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function check_bank_payment_active()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_bank=$this->loan_payment_m->load_bank_payment_active();
		echo $data_bank;

	}
	public function check_bank_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_bank=$this->loan_payment_m->all_bank_payment();
		echo $data_bank;
		
	}
	public function delete_loan_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_delete_disburse=$this->loan_payment_m->delete_l_payment();
		echo $status_delete_disburse;
		
	}
	public function save_loan_pay()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_save_disburse=$this->loan_payment_m->save_l_pay();
		echo $status_save_disburse;
	}
	public function confirm_loan_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_confirm_disburse=$this->loan_payment_m->confirm_l_pay();
		echo $status_confirm_disburse;
	}
	public function search_all_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
	
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#');
		$data_content = array(
				'loan_payment' => $this->loan_payment_m->search_l_payment()
		);
		
		$data = array(
				'title' => $this->title_page,
				'css_other'=>array('modules_finance_disburse/input_box.css'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('loan_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function search_wait_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_menu['menu'] = 'account'; //ให้ menu นี่ใช้งาน
		$data_breadcrumb['menu'] = array('การเงิน' =>'#',$this->title_page => site_url('loan_payment'));
		$data_content = array(
				'bank_payment' => $this->loan_payment_m->load_bank_payment_active(),
				'loan_wait_payment'=>$this->loan_payment_m->search_w_payment()
		);
		$title_page="ใบแจ้งหนี้ที่ยังไม่มีการชำระเงินกู้";
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_finance_disburse/form.js'),
				
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				//'content' => $this->parser->parse('loan_withdraw', array(), TRUE)
				'content' => $this->parser->parse('show_wait_payment', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	public function edit_loan_payment()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$status_edit_payment=$this->loan_payment_m->edit_l_payment();
		echo $status_edit_payment;
	}
	public function check_contract()
	{
		$this->load->model('loan_payment_model', 'loan_payment_m');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
		$data_payment=$this->loan_payment_m->check_contract_no();
		echo $data_payment;
	}
	//-----------------------------------------------pdf
	
}
?>