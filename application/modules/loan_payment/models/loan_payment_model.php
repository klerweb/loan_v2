<?php
class loan_payment_model extends CI_Model{
function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}

	function load_bank_payment()
	{
		$query_bank_payment=$this->db->get('bank_loan_payment');
		return $query_bank_payment->result();
	}
	function search_l_payment()
	{
		$data="";
		if($_POST['contact_id']!="" || $_POST['name_search']!="" || $_POST['id_card_search']!="")
		{
		
		if($_POST['id_card_search']!="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->check_id_card_l($_POST['id_card_search']);
		}
		if($_POST['name_search']!="")
		{
			$split_name=explode(" ",$_POST['name_search']);
			if (count($split_name)>2)
			{
				$split_name[1]=$split_name[1]." ".$split_name[2];
				$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->search_name_in_person($split_name[0],$split_name[1]);
			}
			else if (count($split_name)==1)
			{
				$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->search_name1_in_person($_POST['name_search']);
			}
			else
			{
				$this->load->model('loan_payment_model', '$loan_payment_m');
			$data=$data.$this->loan_payment_m->search_name_in_person($split_name[0],$split_name[1]);
			}
		}
		
		if($_POST['contact_id']!="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->search_contract_no1($_POST['contact_id']);
		}
			
		}
		else if($_POST['contact_id']=="" and $_POST['name_search']=="" and $_POST['id_card_search']=="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->load_loan_payments();
		}
		return $data;
		
	}
	function search_contract_no($invoice_no)
	{	
		$data_loan_pay="";
		$sqlit_data=explode("\\",$invoice_no);
		for($i=0;$i< count($sqlit_data);$i++)
		{	
				$split_data2=explode(",",$sqlit_data[$i]);
				$this->db->where('invoice_no',$split_data2[0]);
				$query_data_loan_payment=$this->db->get('loan_payment');
				if($query_data_loan_payment->num_rows()>0)
				{
					foreach($query_data_loan_payment->result() as $data_loan_payment)
					{
						$this->db->where('loan_id',$split_data2[8]);
						$query_data_loan=$this->db->get('loan');
						$data_loan=$query_data_loan->result();
						$this->db->where('person_id',$split_data2[10]);
						$query_data_person=$this->db->get('person');
						$data_person=$query_data_person->result();
						$this->db->where('title_id',$data_person[0]->title_id);
						$query_data_title=$this->db->get('config_title');
						$data_title=$query_data_title->result();
						$this->db->where('loan_contract_id',$split_data2[9]);
						$query_data_loan_balance=$this->db->get('loan_balance');
						$data_loan_balance=$query_data_loan_balance->result();
						//-----------------------
						$data_loan_pay=$data_loan_pay.$data_loan_payment->payment_no.",".$split_data2[1].",".$split_data2[7].",".$split_data2[6].",".$split_data2[2].",".$split_data2[3].",".$data_loan_payment->com_code.",".$split_data2[4].",".$split_data2[5].",".$split_data2[6].",".$data_loan_payment->money_transfer.",".$data_loan_payment->date_transfer."\\";
					}
		}
		}
		
		if ($data_loan_pay!="")
		{
			return $data_loan_pay;
		}
		else
		{
			return "no data";
		}
}
function search_contract_no1($invoice_no)
	{	
		$data_loan_pay="";
		$this->db->where('loan_contract_no',$invoice_no);
		$query_data_loan_contract=$this->db->get('loan_contract');
		if($query_data_loan_contract->num_rows()>0)
		{
			$data_loan_contract=$query_data_loan_contract->result();
			$contract_no=$data_loan_contract[0]->loan_contract_id;
			$query_data_loan_invoice=$this->db->query("SELECT * FROM loan_invoice WHERE (loan_contract_id=$contract_no) and (loan_invoice.invoice_no IN (SELECT invoice_no FROM loan_payment))");
			if($query_data_loan_invoice->num_rows()>0)
			{
				$data_loan_payment="";
				foreach($query_data_loan_invoice->result() as $data_invoice1)
				{
					$this->db->where('invoice_no',$data_invoice1->invoice_no);
					$query_data_loan_payment=$this->db->get('loan_payment');
					if($query_data_loan_payment->num_rows()>0)
					{
						$data_loan_payment=$query_data_loan_payment->result();
						$this->db->where('invoice_no',$data_invoice1->invoice_no);
						$query_data_loan_invoice=$this->db->get('loan_invoice');
						$data_loan_invoice=$query_data_loan_invoice->result();
						$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
						$query_data_loan_contract=$this->db->get('loan_contract');
						$data_loan_contract=$query_data_loan_contract->result();
						$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
						$query_data_loan=$this->db->get('loan');
						$data_loan=$query_data_loan->result();
						$this->db->where('person_id',$data_loan[0]->person_id);
						$query_data_person=$this->db->get('person');
						$data_person=$query_data_person->result();
						$this->db->where('title_id',$data_person[0]->title_id);
						$query_data_title=$this->db->get('config_title');
						$data_title=$query_data_title->result();
						$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
						$query_data_loan_balance=$this->db->get('loan_balance');
						$data_loan_balance=$query_data_loan_balance->result();
						$data_loan_pay=$data_loan_pay.$data_loan_payment[0]->payment_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_loan_invoice[0]->date_expire_payment.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_payment[0]->com_code.",".$data_loan_invoice[0]->principle.",".$data_loan_invoice[0]->interest.",".$data_loan_invoice[0]->date_expire_payment.",".$data_loan_payment[0]->money_transfer.",".$data_loan_payment[0]->date_transfer."\\"; 
					}
				}
			}

		}
		if ($data_loan_pay!="")
		{
			return $data_loan_pay;
		}
		else
		{
			return "no data";
		}
		/**/
}
function search_name_in_person($name,$lname)
{
	$data_person_id="";
	$check_state=array('person_fname'=>$name,'person_lname'=>$lname);
	
	$this->db->where($check_state);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person_id);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		
		$this->load->model('loan_payment_model', '$loan_payment_m');
		return $this->loan_payment_m->check_id_card_l($data_person1);
	}
	else
	{
		return "no data";
	}
}
function search_name1_in_person($name)
{
	$data_person_id="";
	$this->db->where('person_fname',$name);
	$this->db->or_where('person_lname',$name);
	$query_data_person=$this->db->get('person');
	
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person_id);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		
		$this->load->model('loan_payment_model', '$loan_payment_m');
		return $this->loan_payment_m->check_id_card_l($data_person1);
		
	}
	else
	{
		return "no data";
	}
}
function check_id_card_l($id_card)
{
	$data_invoice="";
	$data_loan_connect="";
	$split_data=explode(",",$id_card);
	for($i=0;$i<count($split_data);$i++)
	{
		$this->db->where('person_thaiid',$split_data[$i]);
		$query_data_person=$this->db->get('person');
		if($query_data_person->num_rows()>0)
		{
			foreach($query_data_person->result() as $data_person)
			{
				$this->db->where('person_id',$data_person->person_id);
				$query_data_loan=$this->db->get('loan');
				if($query_data_loan->num_rows()>0)
				{
					foreach($query_data_loan->result() as $data_loan)
					{
						$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no!='=>null);
						$this->db->where($check_data);
						$query_data_loan_contract=$this->db->get('loan_contract');
						if($query_data_loan_contract->num_rows()>0)
						{
							foreach($query_data_loan_contract->result() as $data_loan_contract)
							{
								$this->db->where('loan_contract_no',$data_loan_contract->loan_contract_no);
								$query_data_loan_contract=$this->db->get('loan_contract');
								if($query_data_loan_contract->num_rows()>0)
								{
									$data_loan_contract=$query_data_loan_contract->result();
									$this->db->order_by('invoice_no','asc');
									$this->db->where('loan_contract_id',$data_loan_contract[0]->loan_contract_id);
									$query_data_loan_invoice=$this->db->get('loan_invoice');
									if($query_data_loan_invoice->num_rows()>0)
									{
										foreach($query_data_loan_invoice->result() as $data_loan_invoice)
										{
											$this->db->where('loan_contract_id',$data_loan_contract[0]->loan_contract_id);
											$query_data_loan_balance=$this->db->get('loan_balance');
											$data_loan_balance=$query_data_loan_balance->result();
											$this->db->where('title_id',$data_person->title_id);
											$query_data_title=$this->db->get('config_title');
											$data_title=$query_data_title->result();
											$this->db->where('loan_contract_id',$data_loan_contract[0]->loan_contract_id);
											$query_data_loan_balance=$this->db->get('loan_balance');
											$data_loan_balance=$query_data_loan_balance->result();
											$this->db->where('invoice_no',$data_loan_invoice->invoice_no);
											$query_data_loan_payment=$this->db->get('loan_payment');
											if($query_data_loan_payment->num_rows()>0)
											{
												$data_loan_payment=$query_data_loan_payment->result();
												$data_invoice=$data_invoice.$data_loan_payment[0]->payment_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_invoice->date_expire_payment.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_payment[0]->com_code.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_invoice->date_expire_payment.",".$data_loan_payment[0]->money_transfer .",".$data_loan_payment[0]->date_transfer."\\";
											}
										}
										
									}
								}

							}
						}

					}
					
				}
			}
			
		}
	}
	return $data_invoice;	
}
function show_c_debtor($payment_no)
{
	$data_l_payment_schedule="";
	$this->db->where('payment_no',$payment_no);
	$query_db_get_invoice_no=$this->db->get('loan_payment');
	if($query_db_get_invoice_no->num_rows()>0)
	{
		$data_get_invoice_no=$query_db_get_invoice_no->result();
		$this->db->where('invoice_no',$data_get_invoice_no[0]->invoice_no);
		$query_db_get_loan_contract_id=$this->db->get('loan_invoice');
		if($query_db_get_loan_contract_id->num_rows()>0)
		{
			$data_get_loan_contract_id=$query_db_get_loan_contract_id->result();
	$this->db->where('loan_contract_id',$data_get_loan_contract_id[0]->loan_contract_id);
	$query_db_get_loan_contract_no=$this->db->get('loan_contract');
	$data_db_get_loan_contract_no=$query_db_get_loan_contract_no->result();
	$this->db->where('loan_contract_no',$data_db_get_loan_contract_no[0]->loan_contract_no);
	$query_data_loan_contract=$this->db->get('loan_contract');
	$data_loan_contract=$query_data_loan_contract->result();
	$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
	$query_data_loan_type_objective=$this->db->get('loan_type');
	$data_loan_type_id=$query_data_loan_type_objective->result();
	$data_loan_type_name="";
		if($data_loan_type_id[0]->loan_objective_id==1)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="เพื่อไถ่ถอนการจำนอง";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="เพื่อไถ่ถอนที่ดินจากการขายฝาก";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==2)
		{
			$data_loan_type_name="เพื่อชำระหนี้ตามสัญญาเงินกู้";
		}
		else if($data_loan_type_id[0]->loan_objective_id==3)
		{
			$data_loan_type_name="เพื่อชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน";
		}
		else if($data_loan_type_id[0]->loan_objective_id==4)
		{
			if($data_loan_type_id[0]->loan_type_redeem_type==1)
			{
				$data_loan_type_name="เพื่อซื้อที่ดินที่หลุดขายฝากไปแล้ว ไม่เกิน 5 ปี";
			}
			if($data_loan_type_id[0]->loan_type_redeem_type==2)
			{
				$data_loan_type_name="เพื่อซื้อที่ดินที่ถูกขายทอดตลาด";
			}
		}
		else if($data_loan_type_id[0]->loan_objective_id==5)
		{
			$data_loan_type_name="เพื่อการประกอบอาชีพเกษตรกรรม";
		}
	$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
	$query_data_loan=$this->db->get('loan');
	$data_loan=$query_data_loan->result();
	$this->db->where('person_id',$data_loan[0]->person_id);
	$query_data_person=$this->db->get('person');
	$data_person=$query_data_person->result();
	$this->db->where('title_id',$data_person[0]->title_id);
	$query_data_title=$this->db->get('config_title');
	$data_title=$query_data_title->result();
	$string=$data_loan_contract[0]->loan_contract_id;
	$query_max_loan_pay_number=$this->db->query("SELECT DISTINCT loan_pay_number FROM payment_schedule where loan_contract_id='$string'");
	$max_loan_pay_number=$query_max_loan_pay_number->result();
	if($query_max_loan_pay_number->num_rows()>0)
	{
		for($i=1;$i<=$query_max_loan_pay_number->num_rows();$i++)
		{
			$check_pay=array('status'=>'P','loan_contract_id'=>$string,'loan_pay_number'=>$i);
			$this->db->where($check_pay);
			$query_data_payment_schedule=$this->db->get('payment_schedule');
			if($query_data_payment_schedule->num_rows()>0)
			{
				$data_loan_payment_sc=$query_data_payment_schedule->result();
				$m_pay=0;
				$check_loan_invoice=array('loan_contract_id'=>$string,'loan_pay_number'=>$i);
				$this->db->where($check_loan_invoice);
				$query_data_loan_invoice=$this->db->get('loan_invoice');
				if($query_data_loan_invoice->num_rows()>0)
				{
					$data_loan_invoice=$query_data_loan_invoice->result();
					$invoice_number=$data_loan_invoice[0]->invoice_no;
					$this->db->where('invoice_no',$invoice_number);
							$query_data_loan_payment= $this->db->get('loan_payment');
							if($query_data_loan_payment->num_rows()>0)
							{
								$query_sum = $this->db->select_sum('money_transfer', 'Amount');
								$query_sum = $this->db->where('invoice_no',$invoice_number);
								$query_sum = $this->db->get('loan_payment');
								$result_sum = $query_sum->result();
								$m_pay=$result_sum[0]->Amount;
							}
				}
				$data_l_payment_schedule=$data_l_payment_schedule.$i.",".$data_loan_payment_sc[0]->date_payout.",".$data_loan_payment_sc[0]->money_pay.",".$data_loan_payment_sc[0]->interest_rate.",".$m_pay."\\";
			}
			else
			{
				$check_pay1=array('status'=>'N','loan_contract_id'=>$string,'loan_pay_number'=>$i);
				$this->db->where($check_pay1);
				$query_data_payment_schedule1=$this->db->get('payment_schedule');
				if($query_data_payment_schedule1->num_rows()>0)
				{
					$data_loan_payment_sc=$query_data_payment_schedule1->result();
					$m_pay=0;
					$check_loan_invoice=array('loan_contract_id'=>$string,'loan_pay_number'=>$i);
					$this->db->where($check_loan_invoice);
					$query_data_loan_invoice=$this->db->get('loan_invoice');
					if($query_data_loan_invoice->num_rows()>0)
					{
						$data_loan_invoice=$query_data_loan_invoice->result();
						$invoice_number=$data_loan_invoice[0]->invoice_no;
						$this->db->where('invoice_no',$invoice_number);
							$query_data_loan_payment= $this->db->get('loan_payment');
							if($query_data_loan_payment->num_rows()>0)
							{
								$query_sum = $this->db->select_sum('money_transfer', 'Amount');
								$query_sum = $this->db->where('invoice_no',$invoice_number);
								$query_sum = $this->db->get('loan_payment');
								$result_sum = $query_sum->result();
								$m_pay=$result_sum[0]->Amount;
							}
					}
					$data_l_payment_schedule=$data_l_payment_schedule.$i.",".$data_loan_payment_sc[0]->date_payout.",".$data_loan_payment_sc[0]->money_pay.",".$data_loan_payment_sc[0]->interest_rate.",".$m_pay."\\";
				}
				else
				{
					$check_pay2=array('status'=>'C','loan_contract_id'=>$string,'loan_pay_number'=>$i);
					$this->db->where($check_pay2);
					$query_data_payment_schedule2=$this->db->get('payment_schedule');
					if($query_data_payment_schedule2->num_rows()>0)
					{
						$data_loan_payment_sc=$query_data_payment_schedule2->result();
						$m_pay=0;
						$check_loan_invoice=array('loan_contract_id'=>$string,'loan_pay_number'=>$i);
						$this->db->where($check_loan_invoice);
						$query_data_loan_invoice=$this->db->get('loan_invoice');
						
						if($query_data_loan_invoice->num_rows()>0)
						{
							$data_loan_invoice=$query_data_loan_invoice->result();
							$invoice_number=$data_loan_invoice[0]->invoice_no;
							$this->db->where('invoice_no',$invoice_number);
							$query_data_loan_payment= $this->db->get('loan_payment');
							if($query_data_loan_payment->num_rows()>0)
							{
								$query_sum = $this->db->select_sum('money_transfer', 'Amount');
								$query_sum = $this->db->where('invoice_no',$invoice_number);
								$query_sum = $this->db->get('loan_payment');
								$result_sum = $query_sum->result();
								$m_pay=$result_sum[0]->Amount;
							}
						}
						$data_l_payment_schedule=$data_l_payment_schedule.$i.",".$data_loan_payment_sc[0]->date_payout.",".$data_loan_payment_sc[0]->money_pay.",".$data_loan_payment_sc[0]->interest_rate.",".$m_pay."\\";
					}
				}
			}
		}
		$data_l_payment_schedule=$query_max_loan_pay_number->num_rows().",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_loan_contract[0]->loan_id.",".$data_loan_type_name.",".$data_db_get_loan_contract_no[0]->loan_contract_no.",".$data_loan_contract[0]->updatedate.",".$data_loan_contract[0]->loan_contract_amount.","."3.00%"."/*/".$data_l_payment_schedule;
		return $data_l_payment_schedule;	
	}
	}
	}	
}
	function count_page_loan_payment()
	{
		$query = $this->db->get('loan_payment');
		if ($query->num_rows()>0)
		{
			
			$page=floor($query->num_rows()/10);
			
			if ($query->num_rows()%9>0)
			{
				$page=$page+1;
			}
			return $page;
		}
		else
		{
			return "no data";
		}
	}
	function loan_one_payment($data_id_payment)
	{
		$data_payment="";
		$data_array_loan_payment="";
		$convert_payment_no=explode("_",$data_id_payment);
		$this->db->where('payment_no',$convert_payment_no[0]."/".$convert_payment_no[1]);
		$query_loan_payment = $this->db->get('loan_payment');
		if ($query_loan_payment->num_rows()>0)
		{
			foreach($query_loan_payment->result() as $data_loan_payment)
			{
				$this->db->where('invoice_no',$data_loan_payment->invoice_no);
				$query_loan_invoice=$this->db->get('loan_invoice');
				foreach($query_loan_invoice->result() as $data_loan_invoice)
				{
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_loan_contact=$this->db->get('loan_contract');
					foreach ($query_loan_contact->result() as $data_loan_contact)
					{
						$this->db->where('loan_contract_id',$data_loan_contact->loan_contract_id);
						$query_loan_balance=$this->db->get('loan_balance');
						
						foreach($query_loan_balance->result() as $data_loan_balance)
						{
						$this->db->where('loan_id',$data_loan_contact->loan_id);
						$query_loan=$this->db->get('loan');
						
						foreach ($query_loan->result() as $data_loan)
						{
							$this->db->where('person_id',$data_loan->person_id);
							$query_person=$this->db->get('person');
							
							foreach($query_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_title=$this->db->get('config_title');
								
								foreach($query_title->result() as $data_title)
								{
									$data_title_name=$data_title->title_name;
								}
								$data_payment=$data_payment.$data_loan_payment->payment_no.",".$data_loan_payment->invoice_no.",".$data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_balance->loan_amount.",".$data_loan_invoice->loan_pay_number.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_payment->com_code.",".$data_loan_payment->money_transfer.",".$data_loan_payment->date_transfer.",".$data_loan_contact->loan_contract_amount;

							}
						}
						}
					}
			}
			
			
		}
		return $data_payment;
		}
		else
		{
			return "no data";
		}
	}
	function load_loan_payments()
	{
		$data_array_loan_payment="";
		$data_title_name="";
		$this->db->order_by('payment_id','desc');
		$query_loan_payment = $this->db->get('loan_payment');
		if ($query_loan_payment->num_rows()>0)
		{
			
			foreach($query_loan_payment->result() as $data_loan_payment)
			{
				
				$this->db->where('invoice_no',$data_loan_payment->invoice_no);
				$query_loan_invoice=$this->db->get('loan_invoice');
				
				foreach ($query_loan_invoice->result() as $data_loan_invoice)
				{
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_loan_contact=$this->db->get('loan_contract');
					foreach ($query_loan_contact->result() as $data_loan_contact)
					{
						$this->db->where('loan_contract_id',$data_loan_contact->loan_contract_id);
						$query_loan_balance=$this->db->get('loan_balance');
						
						foreach($query_loan_balance->result() as $data_loan_balance)
						{
						$this->db->where('loan_id',$data_loan_contact->loan_id);
						$query_loan=$this->db->get('loan');
						
						foreach ($query_loan->result() as $data_loan)
						{
							$this->db->where('person_id',$data_loan->person_id);
							$query_person=$this->db->get('person');
							
							foreach($query_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_title=$this->db->get('config_title');
								
								foreach($query_title->result() as $data_title)
								{
									$data_title_name=$data_title->title_name;
								}
								$data_array_loan_payment=$data_array_loan_payment.$data_loan_payment->payment_no.",".$data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_invoice->date_expire_payment.",".$data_loan_contact->loan_contract_amount.",".$data_loan_balance->loan_amount.",".$data_loan_payment->com_code.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_invoice->date_expire_payment.",".$data_loan_payment->money_transfer.",".$data_loan_payment->date_transfer."\\";

							}
							
						}
						
						}
						
					}
				}
				}
				return $data_array_loan_payment;
			}
		else
		{
			return "no data";
		}
	}
	function check_loan_payment()
	{
		
		$data_title_name="";
	  	$this->db->where("invoice_no", $_POST['id_invoice']);
		$query=$this->db->get('loan_invoice');
		if ($query->num_rows() > 0 )
        { 
			
			foreach($query->result() as $data_invoice)
			{
				$this->db->where('loan_contract_id',$data_invoice->loan_contract_id);
				$query_loan_contact=$this->db->get('loan_contract');
				
				foreach($query_loan_contact->result() as $data_loan_contact)
				{
				$this->db->where("loan_id",$data_loan_contact->loan_id );
				$query1=$this->db->get('loan');
				
				foreach($query1->result() as $data_loan)
				{
					$this->db->where("person_id",$data_loan->person_id);
					$query2=$this->db->get('person');
					
					foreach($query2->result() as $data_person)
					{
						$this->db->where('title_id',$data_person->title_id);
						$query_get_title_name=$this->db->get('config_title');
						foreach($query_get_title_name->result() as $data_get_title_name)
						{
							$data_title_name=$data_get_title_name->title_name;
						}
						$this->db->where("loan_contract_id",$data_invoice->loan_contract_id);
						$query3=$this->db->get('loan_balance');

						foreach($query3->result() as $data_loan_banlance)
						{
							$this->db->where("invoice_no",$_POST['id_invoice']);
							$query4=$this->db->get('loan_payment');
							if($query4->num_rows()>0)
							{
								foreach($query4->result() as $data_payment)
								{
									$My_Multiple_Statements_Array = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'loan_pay_number' => $data_invoice->loan_pay_number);
									$this->db->where($My_Multiple_Statements_Array);
									$query_data_payment_schedule=$this->db->get('payment_schedule');
									
									foreach($query_data_payment_schedule->result() as $data_payment_schedule)
									{
										$this->db->select_sum('money_transfer', 'Amount_money_tran');
										$this->db->where("invoice_no",$_POST['id_invoice']);
										$query_sum_money=$this->db->get('loan_payment'); //รวมเงินที่จ่ายมาแต่ละบิล
										$result_sum_money=$query_sum_money->result();
										$My_Multiple_Statements_Array1 = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'status' => "C");
									$this->db->where($My_Multiple_Statements_Array1);
									$query_data_payment_status=$this->db->get('payment_schedule');
									if ($query_data_payment_status->num_rows()>0)
									{
									echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.",".$data_payment->com_code.",".$data_payment->money_transfer.",".$data_payment->date_transfer.","."ยกเลิก".",".$result_sum_money[0]->Amount_money_tran.",".$data_loan_banlance->status_loan."\\"; //แจ้งการเรียกเก็บเงินกรณีบิลยกเลิกแล้ว จะไม่สามารถจ่ายเงินได้อีก
									}
									else if ($query_data_payment_status->num_rows()==0)
									{
											echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.",".$data_payment->com_code.",".$data_payment->money_transfer.",".$data_payment->date_transfer.","."ปกติ".",".$result_sum_money[0]->Amount_money_tran.",".$data_loan_banlance->status_loan."\\"; // กรณียังสถานะของบิลอื่นยังเป็นปกติ
									}
									}
								}
							}
							else
							{
								$My_Multiple_Statements_Array = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'loan_pay_number' => $data_invoice->loan_pay_number);
									$this->db->where($My_Multiple_Statements_Array);
									$query_data_payment_schedule=$this->db->get('payment_schedule');
									
									foreach($query_data_payment_schedule->result() as $data_payment_schedule)
									{
										$My_Multiple_Statements_Array1 = array('loan_contract_id' =>$data_invoice->loan_contract_id, 'status' => "C");
									$this->db->where($My_Multiple_Statements_Array1);
									$query_data_payment_status=$this->db->get('payment_schedule');
									if ($query_data_payment_status->num_rows()>0)
									{
										echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.","." ".","." ".","." ".","."ยกเลิก".","."0".",".$data_loan_banlance->status_loan."\\";  //แจ้งการเรียกเก็บเงินกรณีบิลยกเลิกแล้ว จะไม่สามารถจ่ายเงินได้อีก
									}
									else if($query_data_payment_status->num_rows()==0)
									{
										echo $data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_loan_banlance->loan_amount.",".$data_invoice->loan_pay_number.",".$data_invoice->principle.",".$data_invoice->interest.",".$data_invoice->date_expire_payment.","." ".","." ".","." ".","."ปกติ".","."0".",".$data_loan_banlance->status_loan."\\"; // กรณียังสถานะของบิลอื่นยังเป็นปกติ
									}
								
									}
							}
						}
					}
				}
				}
				
			}
		}
		else
		{
			echo "ไม่มีเลขใบแจ้งหนี้ที่ต้องการค้นหา";
		}
	}
	function load_bank_payment_active()
	{
		$data_bank_show="";
		$this->db->where("status_com_code","1");
		$query=$this->db->get('bank_loan_payment');
		if($query->num_rows()>0)
		{
			foreach($query->result() as $bank_active)
			{
				$this->db->where('bank_id',$bank_active->bank_id);
				$query_bank_name=$this->db->get('config_bank');
				foreach($query_bank_name->result() as $data_bank_name)
				{
					$data_bank_show=$data_bank_show.$bank_active->com_code.",".$bank_active->account_name.",".$data_bank_name->bank_name."\\";
				}
			}
			return $data_bank_show;
		}
		else
		{
			return "ไม่มี";
		}
	
		
	}
	function all_bank_payment()
	{
		$data_bank_show="";
		$query=$this->db->get('bank_loan_payment');
		if($query->num_rows()>0)
		{
			foreach($query->result() as $bank_active)
			{
				$this->db->where('bank_id',$bank_active->bank_id);
				$query_bank_name=$this->db->get('config_bank');
				foreach($query_bank_name->result() as $data_bank_name)
				{
					$data_bank_show=$data_bank_show.$bank_active->com_code.",".$bank_active->account_name.",".$data_bank_name->bank_name."\\";
				}
			}
			return $data_bank_show;
		}
		else
		{
			return "ไม่มี";
		}
	}
	function delete_l_payment()
	{
		if($this->db->delete("loan_payment","invoice_id=".$_POST['id_invoice']))
		{
				//return "success";
				$data = array('status_pay'=>0);
				$this->db->where("invoice_id",$_POST['id_invoice']);
				if ($this->db->update("loan_invoice",$data))
				{
					return "success";
				}
				else
				{	
					return "fail";
				}
		}
		else
		{	
			return "fail";
		}
	}
	//-------------------------------calculate day
function cal_date1($fdate,$ldate)
{
	$firstday=$fdate;
	$lastday =$ldate;
	list($byear, $bmonth, $bday)= explode("-",$firstday);       //จุดต้องเปลี่ยน
	list($tyear, $tmonth, $tday)= explode("-",$lastday);        //จุดต้องเปลี่ยน
	$mbirthday = mktime(0, 0, 0, $bmonth, $bday, $byear); 
	$mnow = mktime(0, 0, 0, $tmonth, $tday, $tyear );
	$mage = ($mnow - $mbirthday);
	$result = $mage / (60 * 60 * 24); //แปลงค่าเวลารูปแบบ Unix timestamp ให้เป็นจำนวนวัน
	return round($result);
}

function cal_real_date($fdate,$ldate)
{
	
	$first_date=explode("-",$fdate);
	$last_date=explode("-",$ldate);
	$different=$last_date[0]-$first_date[0];
	$count_date="";
	if($different==0)
	{
		if($first_date[0]>2036)
		{
			if($first_date[0]%4==0)
			{
				$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
				$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
			}
			else
			{
				$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
				$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
			}
				$this->load->model('loan_payment_model', '$loan_payment_m');
				$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
		}
		else
		{
			
			
			$this->load->model('loan_payment_model', '$loan_payment_m');
			$count_date=$count_date+$this->loan_payment_m->cal_date1($fdate,$ldate);
		}
		return $count_date+$different;
	}
	else if($different<0)
	{
		return -1;
	}
	else
	{
		
		for($i=0;$i<=$different;$i++)
		{
			if($i==0)
			{
				
				$ldate1=$first_date[0]+$i;
				if($ldate1>2036)
				{
					if($ldate1%4==0)
					{
						if($different==0)
						{
							$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
							
						}
						else
						{
							$ldate2="2016"."-"."12"."-"."31";
							$ldate3="2016"."-".$first_date[1]."-".$first_date[2];
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
						}					
					}
					else
					{
						if($different==0)
						{
							$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
						}
						else
						{
							$ldate2="2017"."-"."12"."-"."31";
							$ldate3="2017"."-".$first_date[1]."-".$first_date[2];
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
						}					
					}
					
				}
				else
				{
					if($different==0)
					{
						$this->load->model('loan_payment_model', '$loan_payment_m');
						$count_date=$count_date+$this->loan_payment_m->cal_date1($fdate,$ldate);
					}
					else
					{
						$ldate2=$first_date[0]."-"."12"."-"."31";
						$ldate3=$first_date[0]."-".$first_date[1]."-".$first_date[2];
						$this->load->model('loan_payment_model', '$loan_payment_m');
						$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
					}
				}
		}
		else if($i>0 && $i<$different)
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".'12'."-".'31';
					$ldate3="2016"."-".'01'."-".'01';
					
				}
				else
				{
					$ldate2="2017"."-".'12'."-".'31';
					$ldate3="2017"."-".'01'."-".'01';
					
				}
			}
			else
			{
				$ldate2=$ldate1."-".'12'."-".'31';
				$ldate3=$ldate1."-".'01'."-".'01';
				

			}
			$this->load->model('loan_payment_model', '$loan_payment_m');
			$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
		}
		else if ($i==$different )
		{
			$ldate1=$first_date[0]+$i;
			if($ldate1>2036)
			{
				if($ldate1%4==0)
				{
					$ldate2="2016"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2016"."-".'01'."-".'01';
					$this->load->model('loan_payment_model', '$loan_payment_m');
					$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
				}
				else
				{
					$ldate2="2017"."-".$last_date[1]."-".$last_date[2];
					$ldate3="2017"."-".'01'."-".'01';
					$this->load->model('loan_payment_model', '$loan_payment_m');
					$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate2);
				}
			}
			else
			{
				$ldate3=$ldate1."-".'01'."-".'01';
				
				$this->load->model('loan_payment_model', '$loan_payment_m');
				$count_date=$count_date+$this->loan_payment_m->cal_date1($ldate3,$ldate);
			}
		}
	}
	return $count_date+$different;
	}
	
}
//-------------------------------calculate day
	function save_l_pay()
	{
		$date_tran_check=explode("/",$_POST['date_tranfer']);
		$date_tran_check1=(intval($date_tran_check[2])-543)."-".$date_tran_check[1]."-".$date_tran_check[0];
		if($date_tran_check1<=date("Y-m-d"))
		{
		$increase_amount=0;
		$this->db->order_by('payment_id','asc');
		$this->db->limit(0);
		$query = $this->db->get('loan_payment');
		$id_new;
		if ($query->num_rows()>0)
		{
			foreach($query->result() as $id_pay)
			{
					$id_new=($id_pay->payment_id)+1;
			}
		}
		else
		{
			$id_new=1;
		}
		$date_tran=explode("/",$_POST['date_tranfer']);
				$data_tran_u=(intval($date_tran[2])-543)."-".$date_tran[1]."-".$date_tran[0];
				if($date_tran[1]>=10)
				{
					$date_pay_check="/".($date_tran[2]+1);
				}
				else
				{
					$date_pay_check="/".($date_tran[2]);
				}

				
					$query_data_count_pay_no=$this->db->query("select payment_no from loan_payment where cast(payment_no as varchar(20)) like '%$date_pay_check%' order by payment_no desc limit 1");
					if($query_data_count_pay_no->num_rows()>0)
					{
						$data_count_pay_no=$query_data_count_pay_no->result();
						$new_pay_no=(($data_count_pay_no[0]->payment_no)+1).$date_pay_check;
					}
					else
					{
						$new_pay_no="1".$date_pay_check;
					}
					
		$this->db->where('loan_contract_no',$_POST['loan_contract_no']);
		$query_data_loan_contract=$this->db->get('loan_contract');
		$data_loan_contract=$query_data_loan_contract->result();
		$loan_contract_id=$data_loan_contract[0]->loan_contract_id;
		$query_data_payment_schedule=$this->db->query("select * from payment_schedule where (loan_contract_id=$loan_contract_id) and (status='P' or status='D') order by loan_pay_number desc limit 1");
		
		if($query_data_payment_schedule->num_rows()>0)
		{
			$data_payment_schedule=$query_data_payment_schedule->result();
		
			$check_array1=array('loan_contract_id'=>$data_payment_schedule[0]->loan_contract_id,'loan_pay_number'=>$data_payment_schedule[0]->loan_pay_number);
		
			$this->db->where($check_array1);
			$query_data_loan_invoice=$this->db->get('loan_invoice');
			$data_loan_invoice=$query_data_loan_invoice->result();
			$this->db->order_by('date_transfer','DESC');
			$this->db->limit('1');
			$this->db->where('invoice_no',$data_loan_invoice[0]->invoice_no);
			$query_data_date_payment=$this->db->get('loan_payment');
			$data_date_payment=$query_data_date_payment->result();
			$this->db->where('loan_contract_id',$loan_contract_id);
			$query_data_loan_balance=$this->db->get('loan_balance');
			$data_loan_balance=$query_data_loan_balance->result();
			$date_last_transfer=$data_date_payment[0]->date_transfer;
		}
		else
		{
			$this->db->where('loan_contract_id',$data_loan_contract[0]->loan_contract_id);
			$query_data_loan_balance=$this->db->get('loan_balance');
			$data_loan_balance=$query_data_loan_balance->result();
			$date_last_transfer=$data_loan_contract[0]->loan_contract_date;
		}
		$this->load->model('loan_payment_model', '$loan_payment_m');
		$total_day=$this->loan_payment_m->cal_real_date($date_last_transfer,$data_tran_u);
		$query_data_payment1_schedule=$this->db->query("select * from payment_schedule where (loan_contract_id=$loan_contract_id) and (status='N' or status='D') order by loan_pay_number asc limit 1");
		$data_payment1_schedule=$query_data_payment1_schedule->result();
		$check_array2=array('loan_contract_id'=>$data_payment1_schedule[0]->loan_contract_id,'loan_pay_number'=>$data_payment1_schedule[0]->loan_pay_number);
		$this->db->where($check_array2);
		$query_data_loan_invoice=$this->db->get('loan_invoice');
		$data_loan_invoice=$query_data_loan_invoice->result();
		if($total_day<0)
		{
			return "กรุณาเลือกวันที่โอนให้ถูกต้อง";
		}
		else
		{
			$count_date=$total_day;
			$interest=(($data_loan_balance[0]->loan_amount)*($count_date*3/36500));
			$check_sum_interest_accrued=array('loan_contract_id'=>$loan_contract_id,'status'=>'D');
			$this->db->order_by('loan_pay_number','DESC');
			$this->db->where($check_sum_interest_accrued);
			$this->db->limit(1);
			$query_sum_interest_accrued=$this->db->get('payment_schedule');
			if($query_sum_interest_accrued->num_rows()>0)
			{
				$sum_interest_accrued=$query_sum_interest_accrued->result();
				if($sum_interest_accrued[0]->interest_accrued!=null)
				{
					$interest_accrued=$sum_interest_accrued[0]->interest_accrued;
				}
				else
				{
					$interest_accrued=0;
				}
			}
			else
			{
				$interest_accrued=0;
			}
			$ex = explode('.',$interest);
			if(count($ex)==2) {$s = substr($ex[1],0,2);
			$interest=$ex[0].".".$s;}
			$split_principle=$_POST['money_transfer']-$interest-$interest_accrued;
			if($split_principle<0)
			{
				$increase_interest=0-$split_principle;
				$split_principle=0;
				$data_insert_payment=array(
				'payment_id'=>$id_new,
				'invoice_no'=>$data_loan_invoice[0]->invoice_no,
				'com_code'=>$_POST['bank'],
				'money_transfer'=>$_POST['money_transfer'],
				'date_transfer'=>$data_tran_u,
				'createddate'=>date("Y-m-d"),
				'updatedate'=>date("Y-m-d"),
				'payment_no'=>$new_pay_no,
				'principle'=>0,
				'interest'=>$_POST['money_transfer'],
				'loan_amount_balance'=>$data_loan_balance[0]->loan_amount,
				'interest_accrued'=>$increase_interest
				);
			}
			else
			{
				$increase_interest=0;

				$data_insert_payment=array(
				'payment_id'=>$id_new,
				'invoice_no'=>$data_loan_invoice[0]->invoice_no,
				'com_code'=>$_POST['bank'],
				'money_transfer'=>$_POST['money_transfer'],
				'date_transfer'=>$data_tran_u,
				'createddate'=>date("Y-m-d"),
				'updatedate'=>date("Y-m-d"),
				'payment_no'=>$new_pay_no,
				'principle'=>$split_principle,
				'interest'=>$interest+$interest_accrued,
				'loan_amount_balance'=>$data_loan_balance[0]->loan_amount-$split_principle,
				'interest_accrued'=>$increase_interest
				);
			}
			$this->db->where('invoice_no',$data_loan_invoice[0]->invoice_no);
			$this->db->select_sum('principle','amount');
			$query_sum_data_payment=$this->db->get('loan_payment');
			$sum_data_payment=$query_sum_data_payment->result();
			$total=$data_loan_balance[0]->loan_amount-$split_principle;
			
			if($total<=0)
			{
				$status_bill='E';
				$this->db->where('loan_contract_id',$loan_contract_id);
				$data_update_loan_balance=array('loan_amount'=>0,'updatedate'=>date("Y-m-d"),'status_loan'=>'E');
				if($this->db->update('loan_balance',$data_update_loan_balance))
				{
					$where_update_payment_schedule_n=array('status'=>'N','loan_contract_id'=>$loan_contract_id);
					$update_payment_schedule_n=array('status'=>'P','updatedate'=>date("Y-m-d"));
					$this->db->where($where_update_payment_schedule_n);
					if($this->db->update('payment_schedule',$update_payment_schedule_n))
					{
						$where_update_payment_schedule_d=array('status'=>'D','loan_contract_id'=>$loan_contract_id);
						$update_payment_schedule_d=array('status'=>'P','updatedate'=>date("Y-m-d"));
						$this->db->where($where_update_payment_schedule_d);
						if($this->db->update('payment_schedule',$update_payment_schedule_d))
						{
							if($this->db->insert('loan_payment',$data_insert_payment))
							{
								return "success";
							}
							else
							{
								return "ไม่สามารถบันทึกได้";
							}
						}
						else
						{
							return "ไม่สามารถบันทึกได้";
						}
					}
					else
					{
						return "ไม่สามารถบันทึกได้";
					}
				}
				else
				{
					return "ไม่สามารถบันทึกได้";
				}
				
				
			}
			else
			{
				
				if($sum_data_payment[0]->amount!=null)
				{
					$sum_amount=$sum_data_payment[0]->amount;
				}
				else
				{
					$sum_amount=0;
				}
				if($data_loan_invoice[0]->principle-($sum_amount+$increase_interest+$split_principle)>0)
				{
						$where_update_payment_schedule_d=array('loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number,'loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id);
						$update_payment_schedule_d=array('status'=>'D','updatedate'=>date("Y-m-d"),'interest_accrued'=>$increase_interest);
						$this->db->where($where_update_payment_schedule_d);
						if($this->db->update('payment_schedule',$update_payment_schedule_d))
						{
							
								if($this->db->insert('loan_payment',$data_insert_payment))
								{
									$this->db->where('loan_contract_id',$loan_contract_id);
									$query_data_amount_loan=$this->db->get('loan_balance');
									$data_amount_loan=$query_data_amount_loan->result();
									$total_amount=$total;
									$query_update_loan_balance=array('loan_amount'=>$total_amount,'updatedate'=>date("Y-m-d"));
									$this->db->where('loan_contract_id',$loan_contract_id);
									if($this->db->update('loan_balance',$query_update_loan_balance))
									{
										
										return "success";
									}
									else{
										return "ไม่สามารถบันทึกได้";
									}
								}
								else
								{
									return "ไม่สามารถบันทึกได้";
								}
							
							//----------end เพิ่ม
						}
						else
						{
							return "ไม่สามารถบันทึกได้";
						}
				}
				else if($data_loan_invoice[0]->principle-($sum_amount+$split_principle+$increase_interest)<0)
				{
					$data_insert_payment=array(
								'payment_id'=>$id_new,
								'invoice_no'=>$data_loan_invoice[0]->invoice_no,
								'com_code'=>$_POST['bank'],
								'money_transfer'=>$_POST['money_transfer'],
								'date_transfer'=>$data_tran_u,
								'createddate'=>date("Y-m-d"),
								'updatedate'=>date("Y-m-d"),
								'payment_no'=>$new_pay_no,
								'principle'=>$split_principle,
								'interest'=>$interest,
								'loan_amount_balance'=>$data_loan_balance[0]->loan_amount-$split_principle
							);
					
					$total=$data_loan_balance[0]->loan_amount-$split_principle;
					$query_update_loan_balance=array('loan_amount'=>$total,'updatedate'=>date("Y-m-d"));
					$this->db->where('loan_contract_id',$loan_contract_id);
					if($this->db->update('loan_balance',$query_update_loan_balance))
					{
						if($this->db->insert('loan_payment',$data_insert_payment))
						{
							$total_check=$split_principle-($data_loan_invoice[0]->principle-$sum_amount);
							$where_update_payment_schedule_d=array('loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number,'loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id);
							$update_payment_schedule_d=array('status'=>'P','updatedate'=>date("Y-m-d"));
							$this->db->where($where_update_payment_schedule_d);
							if($this->db->update('payment_schedule',$update_payment_schedule_d))
							{
								
								$query_data_payment_schedule=$this->db->query("select * from payment_schedule where (loan_contract_id=$loan_contract_id) and (status='N' or status='D') order by loan_pay_number desc");
								
								$check_data_card=array();
			
								$i=0;
								foreach($query_data_payment_schedule->result() as $data_pay1)
								{
									if($total_check>0 && $i<$query_data_payment_schedule->num_rows())
									{
										if($total_check>$data_pay1->money_pay)
										{
											$check_data_card[$i]=$data_pay1->loan_pay_number.",".$data_pay1->money_pay.",CL";
											$total_check=$total_check-$data_pay1->money_pay;
											$i++;
										}
										else
										{
											$check_data_card[$i]=$data_pay1->loan_pay_number.",".($data_pay1->money_pay-$total_check).",N";
											$total_check=$total_check-$data_pay1->money_pay;
										
											$i++;
										}
									}
									
								}
								$b="";
							for($i1=0;$i1<count($check_data_card);$i1++)
								{
									$split_data=explode(",",$check_data_card[$i1]);
									$where_update_payment_schedule_d=array('loan_pay_number'=>$split_data[0],'loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id);
									if($split_data[2]=="N")
									{
										$update_payment_schedule_d=array('status'=>$split_data[2],'money_pay_real'=>$split_data[1],'updatedate'=>date("Y-m-d"));
									}
									else
									{
										$update_payment_schedule_d=array('status'=>$split_data[2],'money_pay'=>$split_data[1],'updatedate'=>date("Y-m-d"));
									}
									$this->db->where($where_update_payment_schedule_d);
									if($this->db->update('payment_schedule',$update_payment_schedule_d))
									{
										$b=true;
									}
									else
									{
										$b=false;
									}
								}
								if($b)
								{
									return "success";
								}
								else
								{
									return "ไม่สามารถบันทึกได้";
								}
							
							}
							}
						else
						{
							return "ไม่สามารถบันทึกได้";
						}	
					}
					else
					{
						return "ไม่สามารถบันทึกได้";
					}
				}
			}
		}
		}
		else
		{
			return "กรุณาเลือกวันที่โอนไม่เกินวันปัจจุบัน";
		}
		
		
	}
	function confirm_l_pay()
	{
		$location="assets/pic_check/";
		$pieces_file = explode("\\", $_POST['file']);
		$type_file=explode(".",$pieces_file[count($pieces_file)-1]);
		$this->db->order_by('id_disbursements','asc');
		$this->db->limit(0);
		$query = $this->db->get('loan_disbursements');
		$id_new;
		if ($query->num_rows()>0)
		{
			foreach($query->result() as $id_dis)
			{
					$id_new=($id_dis->id_disbursements)+1;
			}
		}
		else
		{
			$id_new=1;
		}

		$data_insert=array(
				'id_disbursements'=>$id_new,
				'loan_id'=>$_POST['id'],
				'id_transaction'=>$_POST['id_tran'],
				'id_check'=>$_POST['id_check'],
				'bank_number'=>$_POST['bank'],
				'bank_branch_name'=>$_POST['branch_bank'],
				'date_transfer'=>$_POST['date_tran'],
				'pic_check'=>"check_loan_".$_POST['id'].".".$type_file[1],
				'status_save'=>0
				
			);

		
		if ($this->db->insert('loan_disbursements',$data_insert))
		{
				return "success";
		}
		else
		{
			return "fail";
		}
	}
	function convert_all_pay_number($payment_no,$date,$loan_contract_id,$loan_pay_no,$total_balance)
	{
		$this->db->where('payment_no',$payment_no);
		$query_get_data_payment_no=$this->db->get('loan_payment');
		print_r($query_get_data_payment_no->result());
	}
	function edit_l_payment()
	{
		$this->db->limit(1);
		$this->db->where('invoice_no',$_POST['invoice_no']);
		$query_db_get_loan_contract_id=$this->db->get('loan_invoice');
		$data_db_get_loan_contract_id=$query_db_get_loan_contract_id->result();
		
		$this->db->where('invoice_no',$_POST['invoice_no']);
		$this->db->select_sum('principle','amount');
		$query_sum_data_payment=$this->db->get('loan_payment');
		$sum_data_payment=$query_sum_data_payment->result();
		$old_sum_payment_by_invoice_no=$sum_data_payment[0]->amount;
		$month = array("มกราคม", "กุมภาพันธ์", "มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$split_date_transfer=explode("/",$_POST['date_transfer']);
		$date_tran_check1=(intval($split_date_transfer[2])-543)."-".$split_date_transfer[1]."-".$split_date_transfer[0];
		$split_old_date_transfer=explode("/",$_POST['old_date_transfer']);
		$date_old_tran_check=(intval($split_old_date_transfer[2])-543)."-".$split_old_date_transfer[1]."-".$split_old_date_transfer[0];
		if($date_tran_check1<=date("Y-m-d"))
		{
			$this->db->select('date_transfer');
			if($_POST['date_transfer']==$_POST['old_date_transfer']) //คำนวณยอดใหม่จากของเดิม
			{
				return 1;
			}
			else
			{
				$check_array=array('invoice_no'=>$_POST['invoice_no'],'date_transfer<'=>$date_tran_check1,'payment_no!='=>$_POST['payment_no']);
				$this->db->where($check_array);
				$query_loan_invoice=$this->db->get('loan_payment');
				if($query_loan_invoice->num_rows()>0)
				{
					$this->db->limit(1);
					$this->db->order_by('date_transfer','DESC');
					$check_array1=array('invoice_no'=>$_POST['invoice_no'],'date_transfer<'=>$date_old_tran_check,'payment_no!='=>$_POST['payment_no']);
					$this->db->where($check_array1);
					$query_loan_invoice1=$this->db->get('loan_payment');
					$data_loan_invoice1=$query_loan_invoice1->result();
					
					if($date_tran_check1<$data_loan_invoice1[0]->date_transfer)
					{
						$split_data_loan_invoice=explode("-",$data_loan_invoice1[0]->date_transfer);
						return "กรุณาเลือกวันที่โอนหลังวันที่ ".(intval($split_data_loan_invoice[2])-1)." เดือน".$month[intval($split_data_loan_invoice[1])-1]." พ.ศ.".(intval($split_data_loan_invoice[0])+543);
					}
					else
					{
						$this->db->order_by('date_transfer','ASC');
						$this->db->limit(1);
						$check_array2=array('invoice_no'=>$_POST['invoice_no'],'date_transfer>='=>$date_tran_check1,'payment_no!='=>$_POST['payment_no']);
						$this->db->where($check_array2);
						$query_loan_invoice2=$this->db->get('loan_payment');
						$data_loan_invoice2=$query_loan_invoice2->result();
						if($date_tran_check1>=$data_loan_invoice2[0]->date_transfer)
						{
							$split_data_loan_invoice=explode("-",$data_loan_invoice2[0]->date_transfer);
							return "กรุณาเลือกวันที่โอนก่อนวันที่ ".$split_data_loan_invoice[2]." เดือน".$month[intval($split_data_loan_invoice[1])-1]." พ.ศ.".(intval($split_data_loan_invoice[0])+543);
						}
						else //ต้องคำนวณจากวันที่จ่ายครั้งก่อน แต่ไม่เกินครั้งหลัง
						{
							
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$total_date=$this->loan_payment_m->cal_real_date($data_loan_invoice1[0]->date_transfer,$date_tran_check1);
							$interest=($data_loan_invoice1[0]->loan_amount_balance*$total_date*3/36500);
							$ex = explode('.',$interest);
							if(count($ex)==2) {$s = substr($ex[1],0,2);
							$interest=$ex[0].".".$s;}
							$this->db->limit(1);
							$this->db->where('invoice_no',$_POST['invoice_no']);
							$query_data_loan_contract=$this->db->get('loan_invoice');
							 return 3;
							//$total_date1=$this->loan_payment_m->cal_real_date($data_loan_invoice1[0]->date_transfer,$date_tran_check1);
							/*$split_principle=$_POST['money_transfer']-$interest-$interest_accrued;
							return $split_principle;*/
							/*$check_sum_interest_accrued=array('loan_contract_id'=>$loan_contract_id,'status'=>'D');
							$this->db->order_by('loan_pay_number','DESC');
							$this->db->where($check_sum_interest_accrued);
							$this->db->limit(1);
							$query_sum_interest_accrued=$this->db->get('payment_schedule');*/

						}
						
					}
				}
				else // แก้ไขข้อมูล โดยเอา loan_contract_date มาคิดใหม่
				{
					$this->db->limit(1);
					$this->db->order_by('date_transfer','DESC');
					$check_array1=array('invoice_no'=>$_POST['invoice_no'],'date_transfer<'=>$date_old_tran_check,'payment_no!='=>$_POST['payment_no']);
					$this->db->where($check_array1);
					$query_loan_invoice1=$this->db->get('loan_payment');
					if($query_loan_invoice1->num_rows()>0) //กรณีมีการจ่ายครั้งก่อน
					{
						$data_loan_invoice1=$query_loan_invoice1->result();
						if($data_loan_invoice1[0]->date_transfer>$date_tran_check1)
						{
							$split_data_loan_invoice=explode("-",$data_loan_invoice1[0]->date_transfer);
							return "กรุณาเลือกวันที่โอนหลังวันที่ ".$split_data_loan_invoice[2]." เดือน".$month[intval($split_data_loan_invoice[1])-1]." พ.ศ.".(intval($split_data_loan_invoice[0])+543);
						}
						else //คิดต่อเลย ว่ามีเคยจ่ายไหม ถ้ามีต้องคิดจากครั้งหลัง ถ้าไม่มีให้คิดจาก loan_contract_date
						{
							$this->db->limit(1);
							$this->db->where('loan_contract_id',$data_db_get_loan_contract_id[0]->loan_contract_id);
							$query_get_loan_contract_date=$this->db->get('loan_contract');
							$data_get_loan_contract_date=$query_get_loan_contract_date->result();
							$data_get_loan_contract_date[0]->loan_contract_date;
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$total_date=$this->loan_payment_m->cal_real_date($data_get_loan_contract_date[0]->loan_contract_date,$date_tran_check1);
							return $data_loan_invoice1[0]->loan_amount_balance;
							/*$interest=($data_loan_invoice1[0]->loan_amount_balance*$total_date*3/36500);
							$ex = explode('.',$interest);
							if(count($ex)==2) {$s = substr($ex[1],0,2);
							$interest=$ex[0].".".$s;}
							return $interest;*/
						}
					}
					else //ไม่มีการจ่ายเลยจากครั้งก่อนจากบิลเดียวกัน
					{
						$this->db->limit(1);
						$this->db->where('invoice_no',$_POST['invoice_no']);
						$query_get_loan_pay_number=$this->db->get('loan_invoice');
						$get_loan_pay_number=$query_get_loan_pay_number->result();
						if($get_loan_pay_number[0]->loan_pay_number==1)// เป็นการจ่ายงวดแรก
						{
							$this->db->limit(1);
							$this->db->where('loan_contract_id',$data_db_get_loan_contract_id[0]->loan_contract_id);
							$query_get_loan_contract_date=$this->db->get('loan_contract');
							$data_get_loan_contract_date=$query_get_loan_contract_date->result();
							
							$this->load->model('loan_payment_model', '$loan_payment_m');
							$total_date=$this->loan_payment_m->cal_real_date($data_get_loan_contract_date[0]->loan_contract_date,$date_tran_check1);
							$interest=($data_get_loan_contract_date[0]->loan_contract_amount*$total_date*3/36500);
							$ex = explode('.',$interest);
							if(count($ex)==2) {$s = substr($ex[1],0,2);
							$interest=$ex[0].".".$s;}			
							$this->db->limit(1);
							$this->db->order_by('date_transfer','DESC');
							$check_array1=array('invoice_no'=>$_POST['invoice_no'],'date_transfer<'=>$date_old_tran_check,'payment_no!='=>$_POST['payment_no']);
							$this->db->where($check_array1);
							$query_loan_invoice1=$this->db->get('loan_payment');
							if($query_loan_invoice1->num_rows()>0)
							{
								if($db_get_money_transfer[0]->interest_accrued!=null)
								{
									$db_get_money_transfer=$query_loan_invoice1->result();
									$inter_acc=$db_get_money_transfer[0]->interest_accrued;
								}
								else
								{
									$inter_acc=0;
								}
							}
							else
							{
								$inter_acc=0;
							}
							
							$split_principle=$_POST['money_transfer']-$interest-$inter_acc;
							if($split_principle<0) //จ่ายเงินน้อยกว่าดอก
							{
								$increase_interest=0-$split_principle;
								$split_principle=0;
								$data_update_payment=array(
								'com_code'=>$_POST['bank'],
								'money_transfer'=>$_POST['money_transfer'],
								'date_transfer'=>$date_tran_check1,
								'updatedate'=>date("Y-m-d"),
								'principle'=>0,
								'interest'=>$_POST['money_transfer'],
								'loan_amount_balance'=>$data_get_loan_contract_date[0]->loan_contract_amount,
								'interest_accrued'=>$increase_interest
								);
								$data_update_loan_balance=array(
								'loan_amount'=>$data_get_loan_contract_date[0]->loan_contract_amount,
								'updatedate'=>date("Y-m-d")
								);
								$total_transfer=0;
								$total_amount_balance=$data_get_loan_contract_date[0]->loan_contract_amount;

							}
							else //จ่ายเงินมากกว่าดอก
							{
								$increase_interest=0;
								$data_update_payment=array(
								'com_code'=>$_POST['bank'],
								'money_transfer'=>$_POST['money_transfer'],
								'date_transfer'=>$date_tran_check1,
								'updatedate'=>date("Y-m-d"),
								'principle'=>$split_principle,
								'interest'=>$interest,
								'loan_amount_balance'=>$data_get_loan_contract_date[0]->loan_contract_amount-$split_principle,
								'interest_accrued'=>$increase_interest
								);
								$data_update_loan_balance=array(
								'loan_amount'=>$data_get_loan_contract_date[0]->loan_contract_amount-$split_principle,
								'updatedate'=>date("Y-m-d")
								);
								$total_transfer=$split_principle;
								$total_amount_balance=$data_get_loan_contract_date[0]->loan_contract_amount-$split_principle;
							}
							
							$date_continue=$date_tran_check1;
							$money_tran_check=0;
							$this->db->where('payment_no',$_POST['payment_no']);
							if($this->db->update('loan_payment',$data_update_payment))
							{
								$this->db->where('loan_contract_id',$data_get_loan_contract_date[0]->loan_contract_id);
								if($this->db->update('loan_balance',$data_update_loan_balance))
								{
									$this->db->where('invoice_no',$_POST['invoice_no']);
									$this->db->select_sum('principle','amount');
									$query_sum_data_payment=$this->db->get('loan_payment');
									$sum_data_payment=$query_sum_data_payment->result();
									$new_sum_payment_by_invoice_no=$sum_data_payment[0]->amount;
									$this->db->limit(1);
									$array_check=array('loan_contract_id'=>$data_get_loan_contract_date[0]->loan_contract_id,'loan_pay_number'=>1,'status!='=>'C');
									$this->db->where($array_check);
									$query_db_get_money_transfer=$this->db->get('payment_schedule');
									$db_get_money_transfer=$query_db_get_money_transfer->result();
									if($db_get_money_transfer[0]->money_pay_real!=null)
									{
										$money_tran_check=$db_get_money_transfer[0]->money_pay_real;
									}
									else
									{
										$money_tran_check=$db_get_money_transfer[0]->money_pay;
		
									}
									
									if($new_sum_payment_by_invoice_no==$money_tran_check)
									{
										$data_update_payment_schedule=array(
										'status'=>'P',
										'interest_accrued'=>$increase_interest
										);
									}
									else if ($new_sum_payment_by_invoice_no<$money_tran_check)
									{
										$data_update_payment_schedule=array(
										'status'=>'D',
										'interest_accrued'=>$increase_interest
										);
									}
									else if ($new_sum_payment_by_invoice_no>$money_tran_check)
									{
										$data_update_payment_schedule=array(
										'status'=>'P',
										'interest_accrued'=>$increase_interest
										);
										if($new_sum_payment_by_invoice_no>$old_sum_payment_by_invoice_no[0]->amount)
										{
											$excess_money_pay=$new_sum_payment_by_invoice_no[0]->amount-$old_sum_payment_by_invoice_no[0]->amount; // จ่ายเงินเกิน
											$com_pair_old_new="more";
										}
										else
										{
											$excess_money_pay=$old_sum_payment_by_invoice_no[0]->amount-$new_sum_payment_by_invoice_no[0]->amount; // จ่ายน้อยกว่า
											$com_pair_old_new="less";
										}
									}
									
									$array_check=array('loan_contract_id'=>$data_get_loan_contract_date[0]->loan_contract_id,'loan_pay_number'=>1,'status!='=>'C');
									$this->db->where($array_check);
									if($this->db->update('payment_schedule',$data_update_payment_schedule))
									{
										$this->db->limit(1);
										$array_check=array('loan_contract_id'=>$data_get_loan_contract_date[0]->loan_contract_id,'loan_pay_number'=>1,'status!='=>'C');
										$this->db->where($array_check);
										$query_get_status_payment_schedule=$this->db->get('payment_schedule');
										$get_status_payment_schedule=$query_get_status_payment_schedule->result();
										if($get_status_payment_schedule[0]->status='D')
										{
											$this->db->order_by('date_transfer','asc');
											$check_array1=array('invoice_no'=>$_POST['invoice_no'],'date_transfer>='=>$date_continue,'payment_no!='=>$_POST['payment_no']);
											$this->db->where($check_array1);
											$query_loan_invoice1=$this->db->get('loan_payment');
											if($query_loan_invoice1->num_rows()>0)
											{
												$check_l=true;
												$i=1;
												$data="";
												foreach($query_loan_invoice1->result() as $data_l_payment) //จะวนลูป
												{
													while($check_l and $i<=$query_loan_invoice1->num_rows())
													{
														$this->load->model('loan_payment_model', 'loop_convert_pay_number');//ใช้ model ของตัวเอง ตัวหลังชื่ออ้างอิง
														$data=$data.$this->loop_convert_pay_number->convert_all_pay_number($data_l_payment->payment_no,$date_continue,$data_get_loan_contract_date[0]->loan_contract_id,1,$total_amount_balance).",";
														$i=$i+1;
														$date_continue=$data_l_payment->date_transfer;
													}
												}
											}
											$this->db->limit(1);
											$array_check=array('loan_contract_id'=>$data_get_loan_contract_date[0]->loan_contract_id,'loan_pay_number'=>1,'status!='=>'C');
											$this->db->where($array_check);
											$query_get_status_payment_schedule=$this->db->get('payment_schedule');
											$get_status_payment_schedule=$query_get_status_payment_schedule->result();
											//if(//-------------ทำต่อ
										}
										else if($get_status_payment_schedule[0]->status='P')
										{
											
										}
										
	// ให้เช็ค status p ว่าใช่หรือไม่ถ้าใช่แล้วมีของเก่าเหลือไหม ถ้ามี ให้รวบไปไว้ในงวดสุดท้ายหักกันไป โดยเช็ค ที่ไม่ใช่ CL แต่ถ้าไม่มี CL ไปที่ money_pay_real ก่อน กรณีที่ไม่ใช่ P ให้รวบหาไปเรื่อยๆ ถ้ามันไม่ครบ P ให้ไปจาก CL ถ้าไม่มี CL ให้ไปหา money_pay_real  ถ้าไม่มีอีก ให้ไปรวบงวดถัดไป
										/*$this->db->order_by('date_transfer','asc');
										$check_array1=array('invoice_no'=>$_POST['invoice_no'],'date_transfer>='=>$date_continue,'payment_no!='=>$_POST['payment_no']);
										$this->db->where($check_array1);
										$query_loan_invoice1=$this->db->get('loan_payment');
										
										foreach($query_loan_invoice1->result() as $data_l_payment)
										{
											$total_date=$this->loan_payment_m->cal_real_date($date_continue,$data_l_payment->date_transfer);
											$interest=($total_amount_balance*$total_date*3/36500);
											$ex = explode('.',$interest);
											if(count($ex)==2) {$s = substr($ex[1],0,2);
											$interest=$ex[0].".".$s;}
											
											$this->db->limit(1);
											$this->db->order_by('date_transfer','DESC');
											$check_array1=array('invoice_no'=>$_POST['invoice_no'],'date_transfer<'=>$data_l_payment->date_transfer);
											$this->db->where($check_array1);
											$query_loan_invoice1=$this->db->get('loan_payment');
											if($query_loan_invoice1->num_rows()>0)
											{
												if($db_get_money_transfer[0]->interest_accrued!=null)
												{
													$db_get_money_transfer=$query_loan_invoice1->result();
													$inter_acc=$db_get_money_transfer[0]->interest_accrued;
												}
												else
												{
													$inter_acc=0;
												}
											}
											else
											{
												$inter_acc=0;
											}
											
											$split_principle=$data_l_payment->money_transfer-$interest-$inter_acc;
											//---------------------------------check_insert
											if($split_principle<0) //จ่ายเงินน้อยกว่าดอก
											{
												$increase_interest=0-$split_principle;
												$split_principle=0;
												$data_update_payment=array(
												'updatedate'=>date("Y-m-d"),
												'principle'=>0,
												'interest'=>$data_l_payment->money_transfer,
												'loan_amount_balance'=>$total_amount_balance,
												'interest_accrued'=>$increase_interest
												);
												$data_update_loan_balance=array(
												'loan_amount'=>$total_amount_balance,
												'updatedate'=>date("Y-m-d")
												);
												$total_transfer=0;
												$total_amount_balance=$total_amount_balance;
											}
											else //จ่ายเงินมากกว่าดอก
											{
												$increase_interest=0;
												$data_update_payment=array(
												'updatedate'=>date("Y-m-d"),
												'principle'=>$split_principle,
												'interest'=>$interest,
												'loan_amount_balance'=>$total_amount_balance-$split_principle,
												'interest_accrued'=>$increase_interest
												);
												$data_update_loan_balance=array(
												'loan_amount'=>$total_amount_balance-$split_principle,
												'updatedate'=>date("Y-m-d")
												);
												$total_transfer=$split_principle;
												$total_amount_balance=$total_amount_balance-$split_principle;
											}
											// รอทำต่อ-----------------------------------
											

											//-------------------end check_insert
											
										}*/
									}
									else
									{
										return "ไม่สามารถบันทึกได้";
									}
								}
								else
								{
									return "ไม่สามารถบันทึกได้";
								}
							}
							else
							{
								return "ไม่สามารถบันทึกได้";
							}

						}
						else //ไม่ใช่การจ่ายงวดแรก เงื่อนไขคืองวดถัดไป
						{
						}
					}
				}
			}
		}
		else
		{
			return "กรุณาเลือกวันที่โอนไม่เกินวันปัจจุบัน";
		}
	}
	function edit_l_payment1()
	{
		
		$this->db->where('invoice_no',$_POST['invoice_no']);
		$query_loan_invoice=$this->db->get('loan_invoice');
		$data_loan_invoice=$query_loan_invoice->result();
		$str=str_replace(",","",$_POST['money_transfer']);
		preg_match('/[[:digit:]]+\.?[[:digit:]]*/', $str, $regs);  
		$new_m=doubleval($regs[0]);  
		$old_m=doubleval($_POST['old_money_transfer']);
		$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
		$query_loan_balance=$this->db->get('loan_balance');
		$data_loan_balance=$query_loan_balance->result();
		$old_loan_amount=$data_loan_balance[0]->loan_amount;
		
		if($old_m>$new_m)
		{
			$total=$old_m-$new_m;
			$old_loan_amount=$old_loan_amount+$total;
		}
		else if($old_m==$new_m)
		{
			$old_loan_amount=$old_loan_amount;
		}
		else if($old_m<$new_m)
		{
			
			$total=$new_m-$old_m;
			$old_loan_amount=$old_loan_amount+$total;
		}
			$data_update_balance=array('loan_amount'=>$old_loan_amount,'updatedate'=>date("Y-m-d"));
			$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
			if($this->db->update('loan_balance',$data_update_balance))
			{
				$date_tran=explode("/",$_POST['date_transfer']);
				$data_tran_u=(intval($date_tran[2])-543)."-".$date_tran[1]."-".$date_tran[0];
				$date_pay_check=$date_tran[2].$date_tran[1];
				$old_d_t=explode("/",$_POST['date_transfer']);
				$new_d_t=explode("/",$_POST['old_date_transfer']);
				$old_d_t1=$old_d_t[1]."/".$old_d_t[2];
				$new_d_t1=$new_d_t[1]."/".$new_d_t[2];
				
				if($old_d_t1!=$new_d_t1)
				{
					$query_data_count_pay_no=$this->db->query("select payment_no from loan_payment where cast(payment_no as varchar(20)) like '%$date_pay_check%' order by payment_no desc limit 1");
					if($query_data_count_pay_no->num_rows()>0)
					{
						$data_count_pay_no=$query_data_count_pay_no->result();
						$new_pay_no=($data_count_pay_no[0]->payment_no)+1;
					}
					else
					{
						$new_pay_no=$date_pay_check."001";
					}
				}
				if($old_d_t1==$new_d_t1)
				{
					$this->db->where('payment_no',$_POST['payment_no']);
					$update_loan_payment=array('com_code'=>$_POST['bank'],'updatedate'=>date("Y-m-d"),'money_transfer'=>$_POST['money_transfer'],'date_transfer'=>$data_tran_u);
					if($this->db->update('loan_payment',$update_loan_payment))
					{
						return "success";
					}
					else
					{
						return "ไม่สามารถบันทึกได้";
					}
				}
				else
				{
					$this->db->where('payment_no',$_POST['payment_no']);
					
					$update_loan_payment=array('com_code'=>$_POST['bank'],'updatedate'=>date("Y-m-d"),'money_transfer'=>$_POST['money_transfer'],'date_transfer'=>$data_tran_u,'payment_no'=>$new_pay_no);
					if($this->db->update('loan_payment',$update_loan_payment))
					{
						return "success";
					}
					else
					{
						return "ไม่สามารถบันทึกได้";
					}
					
				}
			}
			else
			{
				return "ไม่สามารถบันทึกได้";
			}
	}
	function show_w_payment()
	{
		$data_wait_payment="";
		$date_year="";
		$month_search=date("m");
		$year_search=date("Y");
		if(intval($month_search)>=12)
		{
			$date_year=((intval($year_search)+1)."-"."01"."-"."01");
		}
		else
		{
			if(intval($month_search)<9)
			{
				$date_year=intval($year_search)."-0".(intval($month_search)+1)."-"."01";
			}
			else
			{
				$date_year=intval($year_search)."-".(intval($month_search)+1)."-"."01";
			}
		}
		$query_loan_invoice=$this->db->query("Select * From loan_invoice Join payment_schedule On (payment_schedule.loan_contract_id = loan_invoice.loan_contract_id and payment_schedule.loan_pay_number = loan_invoice.loan_pay_number) where (payment_schedule.status='N' or payment_schedule.status='D') and loan_invoice.date_expire_payment<='$date_year' order by loan_invoice.date_expire_payment desc");
			
		if($query_loan_invoice->num_rows()>0)
		{
		foreach($query_loan_invoice->result() as $data_loan_invoice)
		{
			
			$check_array=array('loan_contract_id'=>$data_loan_invoice->loan_contract_id,'loan_pay_number'=>$data_loan_invoice->loan_pay_number,'duedelaydate!='=>null);
			$this->db->where($check_array);
			$query_data_payment_schedule=$this->db->get('payment_schedule');
			if($query_data_payment_schedule->num_rows()>0)
			{
				$data_payment_schedule=$query_data_payment_schedule->result();
				if($data_payment_schedule[0]->duedelaydate<=$date_year)
				{
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_data_loan_contract=$this->db->get('loan_contract');
					$data_loan_contract=$query_data_loan_contract->result();
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_data_loan_balance=$this->db->get('loan_balance');
					$data_loan_balance=$query_data_loan_balance->result();
					$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
					$query_data_loan_id=$this->db->get('loan');
					$data_loan_id=$query_data_loan_id->result();
					$this->db->where('person_id',$data_loan_id[0]->person_id);
					$query_data_person=$this->db->get('person');
					$data_person=$query_data_person->result();
					$this->db->where('title_id',$data_person[0]->title_id);
					$query_data_title=$this->db->get('config_title');
					$data_title=$query_data_title->result();
					$data_wait_payment=$data_wait_payment.$data_loan_invoice->invoice_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_payment_schedule[0]->duedelaydate."\\";
				}
			}
			else
			{
				$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
				$query_data_loan_contract=$this->db->get('loan_contract');
				$data_loan_contract=$query_data_loan_contract->result();
				$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
				$query_data_loan_balance=$this->db->get('loan_balance');
				$data_loan_balance=$query_data_loan_balance->result();
				$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
				$query_data_loan_id=$this->db->get('loan');
				$data_loan_id=$query_data_loan_id->result();
				$this->db->where('person_id',$data_loan_id[0]->person_id);
				$query_data_person=$this->db->get('person');
				$data_person=$query_data_person->result();
				$this->db->where('title_id',$data_person[0]->title_id);
				$query_data_title=$this->db->get('config_title');
				$data_title=$query_data_title->result();
				$data_wait_payment=$data_wait_payment.$data_loan_invoice->invoice_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_invoice->date_expire_payment."\\";
			}
		}
		return $data_wait_payment;
		}
		else
		{
			return "no data";
		}

	}
	function search_w_payment()
	{
		$data="";
		if($_POST['id_card_search']!="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->check_w_id_card_l1($_POST['id_card_search']);
		}
		if($_POST['name_search']!="")
		{
			$split_name=explode(" ",$_POST['name_search']);
			if (count($split_name)>2)
			{
				$split_name[1]=$split_name[1]." ".$split_name[2];
				$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->search_w_name_in_person($split_name[0],$split_name[1]);
			}
			else if (count($split_name)==1)
			{
				$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->search_w_name1_in_person($_POST['name_search']);
			}
			else
			{
				$this->load->model('loan_payment_model', '$loan_payment_m');
			$data=$data.$this->loan_payment_m->search_w_name_in_person($split_name[0],$split_name[1]);
			}
		}
		
		if($_POST['contact_id']!="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			 $data=$data.$this->loan_payment_m->search_w_contract_no($_POST['contact_id']."\\");
		}
		return $data;
		
	}
	function search_w_contract_no($invoice_no)
	{	
		if($invoice_no!="no data")
		{
		$data_wait_payment="";
		$sqlit_data=explode("\\",$invoice_no);
		for($i=0;$i<count($sqlit_data)-1;$i++)
		{
			$this->db->where('invoice_no',$sqlit_data[$i]);
			$query_data_loan_invoice=$this->db->get('loan_invoice');
			if($query_data_loan_invoice->num_rows()>0)
			{
			$data_loan_invoice=$query_data_loan_invoice->result();
			$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
			$query_data_loan_contract=$this->db->get('loan_contract');
			$data_loan_contract=$query_data_loan_contract->result();
			$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
			$query_data_loan_balance=$this->db->get('loan_balance');
			$data_loan_balance=$query_data_loan_balance->result();
			$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
			$query_data_loan_id=$this->db->get('loan');
			$data_loan_id=$query_data_loan_id->result();
			$this->db->where('person_id',$data_loan_id[0]->person_id);
			$query_data_person=$this->db->get('person');
			$data_person=$query_data_person->result();
			$this->db->where('title_id',$data_person[0]->title_id);
			$query_data_title=$this->db->get('config_title');
			$data_title=$query_data_title->result();
			$data_wait_payment=$data_wait_payment.$data_loan_invoice[0]->invoice_no.",".$data_loan_contract[0]->loan_contract_no.",".$data_title[0]->title_name." ".$data_person[0]->person_fname." ".$data_person[0]->person_lname.",".$data_person[0]->person_thaiid.",".$data_loan_contract[0]->loan_contract_amount.",".$data_loan_balance[0]->loan_amount.",".$data_loan_invoice[0]->principle.",".$data_loan_invoice[0]->interest.",".$data_loan_invoice[0]->date_expire_payment."\\";
			}
		}
			if($data_wait_payment!="")
			{
				return $data_wait_payment;
			}
			else
			{
				return "no data";
			}
		}
		else
		{
			return "no data";
		}
		
}
function search_w_name_in_person($name,$lname)
{
	$data_person_id="";
	$check_state=array('person_fname'=>$name,'person_lname'=>$lname);
	
	$this->db->where($check_state);
	$query_data_person=$this->db->get('person');
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			 //$data_person_id=$data_person_idใ$this->loan_payment_m->check_id_card_l($data_person->person_thaiid);
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		$this->load->model('loan_payment_model', '$loan_payment_m');
		return $this->loan_payment_m->check_w_id_card_l($data_person_id);
	}
	else
	{
		return "no data";
	}
}
function search_w_name1_in_person($name)
{
	$data_person_id="";
	$this->db->where('person_fname',$name);
	$this->db->or_where('person_lname',$name);
	$query_data_person=$this->db->get('person');
	
	if($query_data_person->num_rows()>0)
	{
		foreach($query_data_person->result() as $data_person)
		{
			
			$data_person_id=$data_person_id.$data_person->person_thaiid.",";
		}
		$this->load->model('loan_payment_model', '$loan_payment_m');
		return $this->loan_payment_m->check_w_id_card_l($data_person_id);
	}
	else
	{
		return "no data";
	}
}
function check_w_id_card_l1($id_card)
{
	$data_invoice="";
	$data="";
	$data_person_array_id="";
	$this->db->where('person_thaiid',$id_card);
	$query_data_person=$this->db->get('person');
	foreach($query_data_person->result() as $data_person)
	{
		$data_person_array_id=$data_person_array_id.$data_person->person_id.",";
	}
	$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$data_person_array_id);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$data_person1=$data_person1.$check_data_card[$l].",";
		}
		$split_data_card=explode(",",$data_person1);
		for( $i=0;$i<count($split_data_card)-1;$i++)
		{
			$this->db->where('person_id',$split_data_card[$i]);
			$query_data_loan=$this->db->get('loan');
			if($query_data_loan->num_rows()>0)
			{
				foreach($query_data_loan->result() as $data_loan)
				{
					$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no!='=>null);
					$this->db->where($check_data);
					$query_data_loan_contract=$this->db->get('loan_contract');
					if($query_data_loan_contract->num_rows()>0)
					{
						foreach($query_data_loan_contract->result() as $data_loan_contract)
						{
							//----------------------------------------------------------
					
							//----------------------------------------------------------
							$this->db->where('loan_contract_no',$data_loan_contract->loan_contract_no);
							$query_data_loan_contract=$this->db->get('loan_contract');
							if($query_data_loan_contract->num_rows()>0)
							{
								$data_loan_contract=$query_data_loan_contract->result();
								$data_loan_contract_id=$data_loan_contract[0]->loan_contract_id;
								$query_data_loan_invoice=$this->db->query("SELECT * FROM loan_invoice WHERE loan_contract_id=$data_loan_contract_id AND NOT loan_invoice.invoice_no IN (SELECT invoice_no FROM loan_payment)");
								if($query_data_loan_invoice->num_rows()>0)
								{
									foreach($query_data_loan_invoice->result() as $data_loan_invoice)
									{
										$data_invoice=$data_invoice.$data_loan_invoice->invoice_no."\\";
									}
								}
								
							}
						}
			}
		}
	}
		}
		if($data_invoice!="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			return $this->loan_payment_m->search_w_contract_no($data_invoice);
		}
		else
		{
			return "no data";
		}
		
}
function check_w_id_card_l($id_card)
{
		$check_data_card=array();
		$j=0;
		$split_data_card=explode(",",$id_card);
		for( $i=0;$i<count($split_data_card);$i++)
		{
			if($i==0)
			{
				$check_data_card[$j]=$split_data_card[$i];
				$j++;
			}
			else
			{
				$b=1;
				for($k=0;$k<count($check_data_card);$k++)
				{
					if($split_data_card[$i]==$check_data_card[$k])
					{
						$b=0;
					}
				}
				if($b==1)
				{
					$check_data_card[$j]=$split_data_card[$i];
					$j++;
				}
			}
		}
		$data_person1="";
		$data_invoice="";
		$data="";
		$data_person_array_id="";
		for($l=0;$l<count($check_data_card)-1;$l++)
		{
			$this->db->where('person_thaiid',$check_data_card[$l]);
			$query_data_person=$this->db->get('person');
			foreach($query_data_person->result() as $data_person)
			{
				$data_person_array_id=$data_person_array_id.$data_person->person_id.",";
			}
		}
		
		$split_data_card=explode(",",$data_person_array_id);
		
		for( $i=0;$i<count($split_data_card)-1;$i++)
		{
			$this->db->where('person_id',$split_data_card[$i]);
			$query_data_loan=$this->db->get('loan');
			if($query_data_loan->num_rows()>0)
			{
				foreach($query_data_loan->result() as $data_loan)
				{
					$check_data=array('loan_id'=>$data_loan->loan_id,'loan_contract_no!='=>null);
					$this->db->where($check_data);
					$query_data_loan_contract=$this->db->get('loan_contract');
					if($query_data_loan_contract->num_rows()>0)
					{
						foreach($query_data_loan_contract->result() as $data_loan_contract)
						{
							//----------------------------------------------------------
					
							//----------------------------------------------------------
							$this->db->where('loan_contract_no',$data_loan_contract->loan_contract_no);
							$query_data_loan_contract=$this->db->get('loan_contract');
							if($query_data_loan_contract->num_rows()>0)
							{
								$data_loan_contract=$query_data_loan_contract->result();
								$data_loan_contract_id=$data_loan_contract[0]->loan_contract_id;
								$query_data_loan_invoice=$this->db->query("SELECT * FROM loan_invoice WHERE loan_contract_id=$data_loan_contract_id AND NOT loan_invoice.invoice_no IN (SELECT invoice_no FROM loan_payment)");
								if($query_data_loan_invoice->num_rows()>0)
								{
									foreach($query_data_loan_invoice->result() as $data_loan_invoice)
									{
										$data_invoice=$data_invoice.$data_loan_invoice->invoice_no."\\";
									}
								}
							}
						}
			}
		}
	}
		}
		if($data_invoice!="")
		{
			$this->load->model('loan_payment_model', '$loan_payment_m');
			return $this->loan_payment_m->search_w_contract_no($data_invoice);
		}
		else
		{
			return "no data";
		}
}
function load_data_invoice($invoice_id)
	{
	$data_payment="";
				$this->db->where('invoice_no',$invoice_id);
				$query_loan_invoice=$this->db->get('loan_invoice');
				foreach($query_loan_invoice->result() as $data_loan_invoice)
				{
					$this->db->where('loan_contract_id',$data_loan_invoice->loan_contract_id);
					$query_loan_contact=$this->db->get('loan_contract');
					foreach ($query_loan_contact->result() as $data_loan_contact)
					{
						$this->db->where('loan_contract_id',$data_loan_contact->loan_contract_id);
						$query_loan_balance=$this->db->get('loan_balance');
						
						foreach($query_loan_balance->result() as $data_loan_balance)
						{
						$this->db->where('loan_id',$data_loan_contact->loan_id);
						$query_loan=$this->db->get('loan');
						
						foreach ($query_loan->result() as $data_loan)
						{
							$this->db->where('person_id',$data_loan->person_id);
							$query_person=$this->db->get('person');
							
							foreach($query_person->result() as $data_person)
							{
								$this->db->where('title_id',$data_person->title_id);
								$query_title=$this->db->get('config_title');
								
								foreach($query_title->result() as $data_title)
								{
									$data_title_name=$data_title->title_name;
								}
								$data_payment=$data_payment.$invoice_id.",".$data_loan_contact->loan_contract_no.",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_balance->loan_amount.",".$data_loan_invoice->loan_pay_number.",".$data_loan_invoice->principle.",".$data_loan_invoice->interest.",".$data_loan_contact->loan_contract_amount."\\";

							}
						}
						}
					}
			}
			return $data_payment;
	}
	function show_receipt_payment($payment_no)
	{
		$convert_split_payment=explode("_",$payment_no);
		$data_receipt_payment="";
		$this->db->where('payment_no',$convert_split_payment[0]."/".$convert_split_payment[1]);
		$query_data_loan_payment=$this->db->get('loan_payment');
		if($query_data_loan_payment->num_rows()>0)
		{
			$data_loan_payment=$query_data_loan_payment->result();
			$this->db->where('invoice_no',$data_loan_payment[0]->invoice_no);
			$query_data_loan_invoice=$this->db->get('loan_invoice');
			if($query_data_loan_invoice->num_rows()>0)
			{
				$data_loan_invoice=$query_data_loan_invoice->result();
				$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
				$query_data_loan_contract=$this->db->get('loan_contract');
				if($query_data_loan_contract->num_rows()>0)
				{
					$data_loan_contract=$query_data_loan_contract->result();
					$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
					$query_data_loan_type=$this->db->get('loan_type');
					$data_loan_type=$query_data_loan_type->result();
					$this->db->where('loan_objective_id',$data_loan_type[0]->loan_objective_id);
					$query_data_loan_objective=$this->db->get('master_loan_objective');
					$data_loan_objective=$query_data_loan_objective->result();
					$this->db->where('loan_contract_id',$data_loan_invoice[0]->loan_contract_id);
					$query_data_loan_balance=$this->db->get('loan_balance');
					$data_loan_balance=$query_data_loan_balance->result();
					$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
					$query_data_person=$this->db->get('loan');
					if($query_data_person->num_rows()>0)
					{
						$data_person=$query_data_person->result();
						
						$this->db->where('person_id',$data_person[0]->person_id);
						$query_data_detail_person=$this->db->get('person');
						$data_detail_person=$query_data_detail_person->result();
						$this->db->where('title_id',$data_detail_person[0]->title_id);
						$query_data_title=$this->db->get('config_title');
						$data_title=$query_data_title->result();
						$this->db->where('district_id',$data_detail_person[0]->person_addr_card_district_id);
						$query_data_district=$this->db->get('master_district');
						$data_district=$query_data_district->result();
						$this->db->where('amphur_id',$data_detail_person[0]->person_addr_card_amphur_id);
						$query_data_amphur=$this->db->get('master_amphur');
						$data_amphur=$query_data_amphur->result();
						$this->db->where('province_id',$data_detail_person[0]->person_addr_card_province_id);
						$query_data_province=$this->db->get('master_province');
						$data_province=$query_data_province->result();
						$address="";
						$tel="";
						if($data_detail_person[0]->person_addr_card_no!=null){
								$address=$address.$data_detail_person[0]->person_addr_card_no." ";
						}
						if($data_detail_person[0]->person_addr_card_moo!=null){
							$address=$address."หมู่ที่ ".$data_detail_person[0]->person_addr_card_moo." ";
						}
						if($data_detail_person[0]->person_addr_card_road!=null){
							$address=$address."หมู่ที่ ".$data_detail_person[0]->person_addr_card_road." ";
						}
						if($data_detail_person[0]->person_phone!=null){
							$tel=$tel."โทร. ".$data_detail_person[0]->person_phone." ";
						}
						if($data_detail_person[0]->person_mobile!=null){
							$tel=$tel."มือถือ. ".$data_detail_person[0]->person_mobile." ";
						}
						$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
						$query_data__loan_object_id=$this->db->get('loan_type');
						$data__loan_object_id=$query_data__loan_object_id->result();
						$this->db->where('loan_objective_id',$data__loan_object_id[0]->loan_objective_id);
						$query_data_objective_name=$this->db->get('master_loan_objective');
						$data_objective_name=$query_data_objective_name->result();
						$check_payment=array('status'=>'P','loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id,'loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number);
						$check_payment1=array('status'=>'D','loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id,'loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number);
						$this->db->where($check_payment);
						$this->db->or_where($check_payment1);
						$query_data_interest_rate=$this->db->get('payment_schedule');
						$data_interest_rate=$query_data_interest_rate->result();
						$check_intrest_accrued=array('loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id,'loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number);
						$this->db->where($check_intrest_accrued);
						$this->db->limit(1);
						$query_data_interest_accrued=$this->db->get('payment_schedule');
						$data_interest_accrued=$query_data_interest_accrued->result();
						if($data_interest_accrued[0]->interest_accrued!=null)
						{
							$inter_acc=$data_interest_accrued[0]->interest_accrued;
						}
						else
						{
							$inter_acc=0;
						}
						$check_sum_principle=array('loan_contract_id'=>$data_loan_invoice[0]->loan_contract_id,'loan_pay_number'=>$data_loan_invoice[0]->loan_pay_number);
						$this->db->where($check_sum_principle);
						$this->db->limit(1);
						$q_get_invoice=$this->db->get('loan_invoice');
						$get_invoice=$q_get_invoice->result();
						$this->db->where('invoice_no',$get_invoice[0]->invoice_no);
						$this->db->select_sum('principle','amount');
						$query_sum_data_payment=$this->db->get('loan_payment');
						$sum_data_payment=$query_sum_data_payment->result();
						if($data_interest_rate[0]->money_pay-$sum_data_payment[0]->amount<=0)
						{
							$sum_payment=0;
						}
						else
						{
							$sum_payment=$data_interest_rate[0]->money_pay-$sum_data_payment[0]->amount;
						}
						$data_receipt_payment=$data_receipt_payment.$payment_no.",".$data_loan_payment[0]->date_transfer.",".$data_objective_name[0]->loan_objective_name.",".$data_title[0]->title_name." ".$data_detail_person[0]->person_fname." ".$data_detail_person[0]->person_lname.",".$address." ".$data_detail_person[0]->person_addr_card_road.","." ตำบล".$data_district[0]->district_name." อำเภอ".$data_amphur[0]->amphur_name." จังหวัด".$data_province[0]->province_name.",".$tel.",".$data_loan_payment[0]->date_transfer.",".$data_loan_contract[0]->loan_contract_no.",".$data_loan_contract[0]->loan_id.",".$data_interest_rate[0]->interest_rate.",".$data_loan_invoice[0]->loan_pay_number.",".$data_loan_payment[0]->money_transfer.",".$data_loan_payment[0]->principle.",".$data_loan_payment[0]->interest.",".$data_loan_balance[0]->loan_amount.",".$data_loan_payment[0]->loan_amount_balance.",".$inter_acc.",".$sum_payment.",".$data_loan_contract[0]->loan_contract_amount;
						return $data_receipt_payment;
					}
					else
					{
						return "no data";
					}
					
				}
				else
				{
					return "no data";
				}
			}
			else
			{
				return "no data";
			}
		}
		else
		{
			return "no data";
		}
	}
	function check_contract_no()
	{
		$data_payment="";
		if($_POST['loan_contract_no']!="")
		{
		$this->db->where('loan_contract_no',$_POST['loan_contract_no']);
		$query_data_loan_contract=$this->db->get('loan_contract');
		if($query_data_loan_contract->num_rows()>0)
		{
			$data_loan_contract=$query_data_loan_contract->result();
			$this->db->where('loan_contract_id',$data_loan_contract[0]->loan_contract_id);
			$query_loan_balance=$this->db->get('loan_balance');
			foreach($query_loan_balance->result() as $data_loan_balance)
			{
				$this->db->where('loan_id',$data_loan_contract[0]->loan_id);
				$query_loan=$this->db->get('loan');
				foreach ($query_loan->result() as $data_loan)
				{
					$this->db->where('person_id',$data_loan->person_id);
					$query_person=$this->db->get('person');
					foreach($query_person->result() as $data_person)
					{
						$this->db->where('title_id',$data_person->title_id);
						$query_title=$this->db->get('config_title');
						foreach($query_title->result() as $data_title)
						{
							$data_title_name=$data_title->title_name;
						}
							$data_payment=$data_payment.$_POST['loan_contract_no'].",".$data_title_name." ".$data_person->person_fname." ".$data_person->person_lname.",".$data_person->person_thaiid.",".$data_loan_balance->loan_amount.",".$data_loan_contract[0]->loan_contract_amount;

							}
						}
			}
			return $data_payment;
		}
		else
		{
			return "no data";
		}
		}
		else
		{
			return "no data";
		}
	}
}
?>