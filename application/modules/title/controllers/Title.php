<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Title extends MY_Controller
{
	private $title_page = 'คำนำหน้า';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('title_model', 'title');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->title->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_title/title.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('title', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('title')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มคำนำหน้า';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('title_model', 'title');
			$data_content['data'] = $this->title->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มคำนำหน้า';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขคำนำหน้า';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_title/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('title_model', 'title');
		
		$array['title_name'] = $this->input->post('txtTitleName');
		$array['title_status'] = '1';
		
		$id = $this->title->save($array, $this->input->post('txtTitleId'));
		
		if($this->input->post('txtTitleId')=='')
		{
			$data = array(
					'alert' => 'บันทึกคำนำหน้าเรียบร้อยแล้ว',
					'link' => site_url('title'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขคำนำหน้าเรียบร้อยแล้ว',
					'link' => site_url('title'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('title_model', 'title');
		$this->title->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกคำนำหน้าเรียบร้อยแล้ว',
				'link' => site_url('title'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['title_name'] = '';
		
		return $data;
	}
}