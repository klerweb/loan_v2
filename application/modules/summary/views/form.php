<form id="frmSummary" name="frmSummary" action="<?php echo base_url().'summary/save'; ?>" method="post">
	<input type="hidden" id="txtId" name="txtId" value="<?php echo $id; ?>" />
	<input type="hidden" id="txtLoan" name="txtLoan" value="<?php echo $data['loan_id']; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">ประเมินราคาทรัพย์สิน</h4>
				</div>
				<div class="panel-body">
					<div class="row" style="padding-bottom:20px;">
						<div class="row" style="padding-bottom:10px;">
					<div class="col-sm-12">
						<h4>ผู้ขอสินเชื่อ</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">ชื่อ - นามสกุล</label>
							<input type="text" name="txtName" id="txtName" class="form-control" value="<?php echo $loan['person']['title']['title_name'].$loan['person']['person_fname'].' '.$loan['person']['person_lname']; ?>" maxlength="100" readonly="readonly" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="control-label">จำนวนเงิน</label>
							<div class="input-group">
								<input type="text" name="txtAmountLoan" id="txtAmountLoan" class="form-control right" value="<?php echo number_format($loan['loan_amount'], 2); ?>" readonly="readonly"/>
								<span class="input-group-addon">บาท</span>
							</div>
							<label class="error" for="txtAmountLoan"></label>					
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<label class="control-label">วัตถุประสงค์</label>
						<textarea name="txtComment" id="txtComment" class="form-control" rows="3" maxlength="255" readonly="readonly"><?php echo $loan['loan_type_objective']; ?></textarea>
					</div>
				</div><!-- row -->
				<hr/>
						<div class="col-sm-12">
							<h4>ที่ดินและสิ่งปลูกสร้าง</h4>
						</div>
					</div>
					<div class="row">
						<div class="table-responsive">
							<table class="table table-striped mb30">
								<thead>
									<tr>
						            	<th>#</th>
						                <th>ที่ดินสิ่งปลูกสร้าง</th>
						                <th>ราคา</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sum = 0;
										if(!empty($data['land']))
										{
											$i = 1;
											foreach ($data['land'] as $land) 
											{
												$land_price = floatval($land['land_record_staff_mortagage_price']);
												$sum += $land_price;
												
												echo '<tr>
										            	<td>'.$i++.'</td>
										                <td>หนังสือแสดงกรรมสิทธิ์ '.$land['land_type_name'].'&nbsp;&nbsp;&nbsp;เลขที่ '.$land['land_no'].'&nbsp;&nbsp;&nbsp;ตำบล '.$land['district_name'].'&nbsp;&nbsp;&nbsp;อำเภอ '.$land['amphur_name'].'&nbsp;&nbsp;&nbsp;จังหวัด '.$land['province_name'].'</td>
										                <td align="right">'.number_format($land_price, 2).'</td>
													</tr>';	

												if(!empty($land['building']))
												{
													foreach ($land['building'] as $building)
													{
														$building_price = floatval($building['building_record_staff_assess_amount']);
														$sum += floatval($building_price);
													
														echo '<tr>
																<td></td>
																<td><span class="fa fa-caret-right"></span> '.$building['building_type_name'].' :: เลขที่ '.$building['building_no'].' ตำบล '.$building['district_name'].' อำเภอ '.$building['amphur_name'].' จังหวัด '.$building['province_name'].'</td>
																<td align="right">'.number_format($building_price, 2).'</td>
															</tr>';
													}
												}
											}
										}
									?>
									<tr>
						            	<td colspan="2"><b>รวม</b></td>
						                <td align="right"><b><?php echo number_format($sum, 2); ?></b></td>
									</tr>
								</tbody>
							</table>
						</div><!-- table-responsive -->
					</div><!-- row -->
					<hr/>
					<div class="row" style="padding-bottom:20px;">
						<div class="col-sm-12">
							<h4>ผลการอนุมัติ</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">วันที่อนุมัติ <span class="asterisk">*</span></label>
								<div class="input-group">
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo $data['loan_summary_date']; ?>" readonly="readonly"/>
									<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-6 -->
						<div class="col-sm-6">
							<label class="control-label">ให้จำนองทรัพย์สินในวงเงิน <span class="asterisk">*</span></label>
							<div class="input-group mb15">
								<input type="text" name="txtAmount" id="txtAmount" class="form-control right" value="<?php echo number_format($data['loan_summary_amount'], 2); ?>" />
								<span class="input-group-addon">บาท</span>
							</div>
							<label class="error" for="txtAmount"></label>
						</div>
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-12">
							<label class="control-label">ความเห็นของผู้อนุมัติ <span class="asterisk">*</span></label>
							<textarea name="txtComment" id="txtComment" class="form-control" rows="3" maxlength="255" required><?php echo $data['loan_summary_comment']; ?></textarea>
						</div>
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>