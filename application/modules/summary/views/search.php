<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('summary/search'); ?>">
					<div class="form-group">
						<label class="sr-only" for="txtLand">รหัสสินเชื่อ</label>
						<input type="text" class="form-control" id="txtLoan" name="txtLoan" placeholder="รหัสสินเชื่อ" value="<?php echo $loan; ?>" />
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
						<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="<?php echo $thaiid; ?>" />
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">Search</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
		            	<th>รหัสสินเชื่อ</th>
		                <th>วันที่ขอสินเชื่อ</th>
		                <th>ชื่อ - นามสกุล</th>
		                <th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					if(!empty($result))
					{
						$i = 1;
						foreach ($result as $row)
						{
							$row['loan_date'] = convert_dmy_th($row['loan_date']);
				?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $row['loan_code']; ?></td>
						<td><?php echo $row['loan_date']; ?></td>
						<td><?php echo $row['title_name'].$row['person_fname'].' '.$row['person_lname']; ?></td>
		           		<td>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="เพิ่ม" data-original-title="เพิ่ม" onclick="location.href='<?php echo site_url('summary/form/add/'.$row['loan_id']); ?>'"><span class="fa fa-plus"></span></button>
		           		</td>
		           	</tr>
				<?php
						}	
					} 
				?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>