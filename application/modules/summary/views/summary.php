<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url('summary/search'); ?>'">เพิ่มแบบประเมิน</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('summary'); ?>">
					<div class="form-group">
						<label class="sr-only" for="txtLand">รหัสสินเชื่อ</label>
						<input type="text" class="form-control" id="txtLoan" name="txtLoan" placeholder="รหัสสินเชื่อ" value="<?php echo $loan; ?>" />
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
						<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="<?php echo $thaiid; ?>" />
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">Search</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
		            	<th>รหัสสินเชื่อ</th>
		                <th>วันที่ขอสินเชื่อ</th>
		                <th>ชื่อ - นามสกุล</th>
		                <th>สถานะ</th>
		                <th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					if(!empty($result))
					{
						$i = 1;
						foreach ($result as $row)
						{
							if($row['loan_summary_status']=='1')
							{
								$row['loan_summary_status'] = 'แบบร่าง';
								$menu = '<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="edit(\''.$row['loan_summary_id'].'\')"><span class="fa fa-edit"></span></button>
									<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ยกเลิก" data-original-title="ยกเลิก" onclick="del(\''.$row['loan_summary_id'].'\')"><span class="fa fa-trash-o"></span></button>';
							}
							else
							{
								$row['loan_summary_status'] = 'บันทึกเรียบร้อย';
								$menu = '';
							}
							
							$row['loan_date'] = convert_dmy_th($row['loan_date']);
				?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $row['loan_code']; ?></td>
						<td><?php echo $row['loan_date']; ?></td>
						<td><?php echo $row['title_name'].$row['person_fname'].' '.$row['person_lname']; ?></td>
						<td><?php echo $row['loan_summary_status']; ?></td>
		           		<td>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="พิมพ์" data-original-title="พิมพ์" onclick="window.open('<?php echo site_url('summary/word/'.$row['loan_summary_id']); ?>', '_blank')"><span class="fa fa-print"></span></button>
		           			<?php echo $menu; ?>
		           		</td>
		           	</tr>
				<?php
						}	
					} 
				?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>