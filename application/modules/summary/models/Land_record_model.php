<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Land_record_model extends CI_Model
{
	private $table = 'land_record';

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function getByLoan($loan)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan_guarantee_land", "loan_guarantee_land.land_id = land_record.land_id", "LEFT");
		$this->db->join("land", "land.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->join("config_land_type", "land.land_type_id = config_land_type.land_type_id", "LEFT");
		$this->db->join("master_district", "master_district.district_id = land.land_addr_district_id", "LEFT");
		$this->db->join("master_amphur", "master_amphur.amphur_id = land.land_addr_amphur_id", "LEFT");
		$this->db->join("master_province", "master_province.province_id = land.land_addr_province_id", "LEFT");
		//$this->db->where("loan_guarantee_land.loan_guarantee_land_status !=", "0");
		//$this->db->where("land.land_status !=", "0");
		//$this->db->where("land_record.land_record_status !=", "0");
		$this->db->where("loan_guarantee_land.loan_id", $loan);

		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}
}
?>
