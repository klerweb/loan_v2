<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_summary_model extends CI_Model
{
	private $table = 'loan_summary';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_summary.loan_summary_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_summary.loan_summary_updateby", get_uid_login());
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_summary_id", $id);
		$this->db->where("loan_summary.loan_summary_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_summary.loan_summary_updateby", get_uid_login());
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
			
			$result['loan_summary_date'] = convert_dmy_th($result['loan_summary_date']);
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_id", $loan_id);
		$this->db->where("loan_summary.loan_summary_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_summary.loan_summary_updateby", get_uid_login());
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function search($loan='', $thaiid='', $fullname='')
	{
		$where = '';
		if($loan!='') $where .= "OR loan.loan_code LIKE '%".$loan."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan", "loan.loan_id = loan_summary.loan_id", "RIGHT");
		$this->db->join("person", "loan.person_id = person.person_id", "RIGHT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan_summary.loan_summary_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_summary.loan_summary_updateby", get_uid_login());
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function searchNonAnalysis($loan='', $thaiid='', $fullname='')
	{
		$where = '';
		if($loan!='') $where .= "OR loan.loan_code LIKE '%".$loan."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
	
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan", "loan.loan_id = loan_summary.loan_id", "RIGHT");
		$this->db->join("person", "loan.person_id = person.person_id", "RIGHT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan_summary.loan_summary_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_summary.loan_summary_updateby", get_uid_login());
		$this->db->where("loan_summary.loan_id NOT IN (SELECT loan_analysis.loan_id FROM loan_analysis WHERE loan_analysis.loan_analysis_status <> '0')", NULL, FALSE);
	
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function save($array, $id='')
	{
		if($id=='')
		{
			$this->db->set($array);
			$this->db->set("loan_summary_createby", get_uid_login());
			$this->db->set("loan_summary_createdate", "NOW()", FALSE);
			$this->db->set("loan_summary_updateby", get_uid_login());
			$this->db->set("loan_summary_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
				
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("loan_summary_updateby", get_uid_login());
			$this->db->set("loan_summary_updatedate", "NOW()", FALSE);
			$this->db->where("loan_summary_id", $id);
			$this->db->update($this->table, $array);
		}
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("loan_summary_status" => "0");
	
		$this->db->set($array);
		$this->db->where("loan_summary_id", $id);
		$this->db->update($this->table, $array);
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function getModel()
	{
		$data['loan_id'] = '';
		$data['loan_summary_comment'] = '';
		$data['loan_summary_date'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_summary_amount'] = '0.00';
		$data['loan_summary_status'] = '1';
	
		return $data;
	}
}
?>