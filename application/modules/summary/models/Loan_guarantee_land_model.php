<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_guarantee_land_model extends CI_Model
{
	private $table = 'loan_guarantee_land';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan)
	{	
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("land_record", "land_record.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->join("land", "land.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->where("loan_guarantee_land.loan_guarantee_land_status !=", "0");
		$this->db->where("land.land_status !=", "0");
		$this->db->where("land_record.land_record_status !=", "0");
		$this->db->where("loan_guarantee_land.loan_id", $loan);
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>