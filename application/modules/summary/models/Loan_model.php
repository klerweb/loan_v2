<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_model extends CI_Model
{
	private $table = 'loan';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function searchNonSummary($loan='', $thaiid='', $fullname='')
	{
		$where = '';
		if($loan!='') $where .= "OR loan.loan_code LIKE '%".$loan."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("person", "loan.person_id = person.person_id", "RIGHT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan.loan_status !=", "0");
		$this->db->where("loan.loan_id NOT IN (SELECT loan_summary.loan_id FROM loan_summary WHERE loan_summary.loan_summary_status <> '0')", NULL, FALSE);
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>