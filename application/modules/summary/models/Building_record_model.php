<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building_record_model extends CI_Model
{
	private $table = 'building_record';

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	public function getByLand($land)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("building", "building.building_id = building_record.building_id", "INNER");
		$this->db->join("config_building_type", "config_building_type.building_type_id = building.building_type_id", "INNER");
		$this->db->join("master_district", "master_district.district_id = building.building_district_id", "INNER");
		$this->db->join("master_amphur", "master_amphur.amphur_id = building.building_amphur_id", "INNER");
		$this->db->join("master_province", "master_province.province_id = building.building_province_id", "INNER");
		//$this->db->where("building_record.building_record_status !=", "0");
		//$this->db->where("building.building_status !=", "0");
		$this->db->where("building.land_id", $land);

		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();

		return $result;
	}
}
?>
