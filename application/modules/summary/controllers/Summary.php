<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Summary extends MY_Controller
{
    private $title_page = 'สรุปการประเมินราคาทรัพย์สิน';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('loan_summary_model', 'loan_summary');

        $loan = '';
        $thaiid = '';
        $fullname = '';
        if (!empty($_POST['txtLoan'])) {
            $loan = $_POST['txtLoan'];
        }
        if (!empty($_POST['txtThaiid'])) {
            $thaiid = $_POST['txtThaiid'];
        }
        if (!empty($_POST['txtFullname'])) {
            $fullname = $_POST['txtFullname'];
        }

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');

        $data_content = array(
                'loan' => $loan,
                'thaiid' => $thaiid,
                'fullname' => $fullname,
                'result' => $this->loan_summary->search($loan, $thaiid, $fullname)
        );

        $data = array(
                'title' => $this->title_page,
                'js_other' => array('modules_summary/summary.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('summary', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function search()
    {
        $this->load->model('summary/loan_model', 'loan');

        $loan = '';
        $thaiid = '';
        $fullname = '';
        if (!empty($_POST['txtLoan'])) {
            $loan = $_POST['txtLoan'];
        }
        if (!empty($_POST['txtThaiid'])) {
            $thaiid = $_POST['txtThaiid'];
        }
        if (!empty($_POST['txtFullname'])) {
            $fullname = $_POST['txtFullname'];
        }

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array(
                'สินเชื่อ' =>'#',
                $this->title_page => site_url('summary')
            );

        $data_content = array(
                'loan' => $loan,
                'thaiid' => $thaiid,
                'fullname' => $fullname,
                'result' => $this->loan->searchNonSummary($loan, $thaiid, $fullname)
        );

        $data = array(
                'title' => 'เพิ่มแบบประเมิน',
                'js_other' => array(),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('search', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function form($type, $id)
    {
        $this->load->model('title/title_model', 'title');
        $this->load->model('loan_summary_model', 'loan_summary');
        $this->load->model('analysis/loan_model', 'loan');
        $this->load->model('analysis/loan_type_model', 'loan_type');
        $this->load->model('summary/land_record_model', 'land_record');
        $this->load->model('summary/building_record_model', 'building_record');

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array(
                'สินเชื่อ' =>'#',
                $this->title_page => site_url('summary')
        );

        if ($type=='add') {
            $title_page = 'เพิ่มแบบประเมิน';

            $data_content['id'] = '';
            $data_content['data'] = $this->loan_summary->getModel();
            $data_content['data']['loan_id'] = $id;
        } else {
            $data_content['data'] = $this->loan_summary->getById($id);

            if (empty($data_content['data'])) {
                $title_page = 'เพิ่มแบบประเมิน';

                $data_content['id'] = '';
                $data_content['data'] = $this->loan_summary->getModel();
                $data_content['data']['loan_id'] = $id;
            } else {
                $title_page = 'แก้ไขแบบประเมิน';

                $data_content['id'] = $id;
            }
        }

        $data_content['loan'] = $this->loan->getById($data_content['data']['loan_id']);
        $data_content['loan']['person']['title'] = $this->title->getById($data_content['loan']['person']['title_id']);
        $data_content['data']['land'] = $this->land_record->getByLoan($data_content['data']['loan_id']);

        if (!empty($data_content['data']['land'])) {
            foreach ($data_content['data']['land'] as $key_land => $land) {
                $data_content['data']['land'][$key_land]['building'] = $this->building_record->getByLand($land['land_id']);
            }
        }

        $loan_type = $this->loan_type->getByLoan($id);

        $loan_type_objective = '';
        if (!empty($loan_type)) {
            foreach ($loan_type as $list_type) {
                $loan_type_objective  .= '- '.$list_type['loan_objective_name'].'<br/>';
            }
        }

        if ($loan_type_objective!='') {
            $loan_type_objective = substr($loan_type_objective, 0, -5);
        }
        $data_content['loan']['loan_type_objective'] = $loan_type_objective;

        $data = array(
                'title' => $title_page,
                'js_other' => array('modules_summary/form.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('form', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function save()
    {
        $this->load->model('loan_summary_model', 'loan_summary');

        $array['loan_id'] = $this->input->post('txtLoan');
        $array['loan_summary_comment'] = $this->input->post('txtComment');
        $array['loan_summary_date'] = convert_ymd_en($this->input->post('txtDate'));
        $array['loan_summary_amount'] = forNumberInt($this->input->post('txtAmount'));
        $array['loan_summary_status'] = '1';

        $result = $this->loan_summary->save($array, $this->input->post('txtId'));

        if ($this->input->post('txtId')=='') {
            $data = array(
                    'alert' => 'บันทึกแบบประเมินเรียบร้อยแล้ว',
                    'link' => site_url('summary'),
            );
        } else {
            $data = array(
                    'alert' => 'แก้ไขแบบประเมินเรียบร้อยแล้ว',
                    'link' => site_url('summary'),
            );
        }

        $this->parser->parse('redirect', $data);
    }

    public function del($id)
    {
        $this->load->model('loan_summary_model', 'loan_summary');
        $this->loan_summary->del($id);

        $data = array(
                'alert' => 'ยกเลิกแบบประเมินเรียบร้อยแล้ว',
                'link' => site_url('summary'),
        );

        $this->parser->parse('redirect', $data);
    }

    public function pdf($id)
    {
        ob_start();
        $this->load->library('tcpdf');

        $this->load->model('loan_summary_model', 'loan_summary');
        $this->load->model('summary/land_record_model', 'land_record');
        $this->load->model('summary/building_record_model', 'building_record');
        $this->load->model('analysis/land_assess_model', 'land_assess');

        $data = $this->loan_summary->getById($id);

        $html_comment = '';
        if ($data['loan_summary_comment']!='') {
            $html_comment = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>ความเห็นของผู้อนุมัติ</td>
					</tr>
					<tr>
						<td>'.num2Thai($data['loan_summary_comment']).'</td>
					</tr>
				</table>';
        }

        $land_list = $this->land_record->getByLoan($data['loan_id']);

        $html = '';
        $html_building = '';
        if (!empty($land_list)) {
            $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10%"></td>
						<td width="90%">๑. ที่ดิน</td>
					</tr>
				</table>';

            $i_land = 1;
            foreach ($land_list as $key_land => $land) {
                $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%"></td>
							<td width="50%">'.num2Thai('1.'.$i_land++).'&nbsp;&nbsp;หนังสือแสดงกรรมสิทธิ์&nbsp;&nbsp;'.num2Thai($land['land_type_name']).'</td>
							<td width="35%">เลขที่&nbsp;&nbsp;'.num2Thai($land['land_no']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="33%">ตำบล&nbsp;&nbsp;'.$land['district_name'].' </td>
							<td width="33%">อำเภอ&nbsp;&nbsp;'.$land['amphur_name'].'</td>
							<td width="34%">จังหวัด&nbsp;&nbsp;'.$land['province_name'].'</td>
						</tr>
					</table>';

                $land_assess = $this->land_assess->getByLand($land['land_id']);

                if (!empty($land_assess)) {
                    $i_assess = 1;
                    foreach ($land_assess as $list_assess) {
                        $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="17%"></td>
									<td width="32%">('.num2Thai($i_assess++).')&nbsp;&nbsp;เนื้อที่ '.num2Thai($list_assess['land_assess_area_rai']).' ไร่ '.num2Thai($list_assess['land_assess_area_ngan']).' งาน '.num2Thai($list_assess['land_assess_area_wah']).' ตารางวา</td>
									<td width="28%">ราคาตารางวาละ '.num2Thai(number_format($list_assess['land_assess_price'] / 400, 2)).' บาท</td>
									<td width="5%" align="right">=</td>
									<td width="18%" align="right">'.num2Thai(number_format($list_assess['land_assess_inspector_sum'], 2)).' บาท</td>
								</tr>
							</table>';
                    }
                }

                $building_list = $this->building_record->getByLand($land['land_id']);

                if (!empty($building_list)) {
                    $i_building = 1;
                    foreach ($building_list as $key_building => $building) {
                        $html_building .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="15%"></td>
									<td width="62%">'.num2Thai('2.'.$i_building++).'&nbsp;&nbsp;'.$building['building_type_name'].' :: เลขที่ '.$building['building_no'].' ตำบล '.$building['district_name'].' อำเภอ '.$building['amphur_name'].' จังหวัด '.$building['province_name'].'</td>
									<td width="5%" align="right">=</td>
									<td width="18%" align="right">'.num2Thai(number_format($building['building_record_staff_assess_amount'], 2)).' บาท</td>
								</tr>
							</table>';
                    }
                }
            }
        }

        if ($html_building!='') {
            $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10%"></td>
						<td width="90%">๒. 	สิ่งปลูกสร้าง</td>
					</tr>
				</table>
				'.$html_building;
        }

        $filename = 'summary_'.date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename);//  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->AddPage();

        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%" style="font-weight: bold;">สรุปการประเมินราคาทรัพย์สิน</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">จากการวิเคราะห์และประเมินราคา วิธีการประเมิน รวมทั้งการพิจารณาข้อมูลและรายละเอียดต่างๆ ข้างต้น</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ควรมีราคาประเมิน ดังนี้</td>
				</tr>
			</table>
			'.$html.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>รวมข้อ ๑ และ ๒</td>
				</tr>
			</table>
			'.$html_comment.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ให้จำนองทรัพย์สินในวงเงิน&nbsp;&nbsp;'.num2Thai(number_format($data['loan_summary_amount'], 2)).' บาท&nbsp;&nbsp;('.num2wordsThai($data['loan_summary_amount']).')</td>
				</tr>
			</table>
			<br/><br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" style="text-align:center;"></td>
					<td width="70%" style="text-align:center;">
						ลงชื่อ ......................................................<br/>
						(...........................................................)<br/>
						ตำแหน่ง ................................................<br/>
						วันที่ .........................................
					</td>
				</tr>
			</table>';

        $pdf->SetFont('thsarabun', '', 16);
        $pdf->writeHTML($htmlcontent, true, 0, true, true);

        $pdf->Output($filename.'.pdf', 'I');
    }

    public function word($id)
    {
        include_once('word/class/tbs_class.php');
        include_once('word/class/tbs_plugin_opentbs.php');

        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $this->load->model('loan_summary_model', 'loan_summary');
        $this->load->model('summary/land_record_model', 'land_record');
        $this->load->model('summary/building_record_model', 'building_record');
        $this->load->model('analysis/land_assess_model', 'land_assess');

        $data = $this->loan_summary->getById($id);
        $land_list = $this->land_record->getByLoan($data['loan_id']);

        $GLOBALS['loan_summary_comment'] =  $data['loan_summary_comment'] ? num2Thai($data['loan_summary_comment']) : '-';
        $GLOBALS['loan_summary_amount'] =  $data['loan_summary_amount'] ? num2Thai(number_format($data['loan_summary_amount'], 2)) : '-';
        $GLOBALS['loan_summary_amount_text'] =  $data['loan_summary_amount'] ? num2wordsThai($data['loan_summary_amount']) : '-';

        $template = 'word/template/4_1_building.docx';
        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

        $data_land = array();
        $data_building = array();
        if (!empty($land_list)) {
            $i_land = 1;
            foreach ($land_list as $key_land => $land) {
                $data_land[] = array('detail'=>'                         '.num2Thai('1.'.$i_land++).' หนังสือแสดงกรรมสิทธิ์  '.num2Thai($land['land_type_name']).' เลขที่  '.num2Thai($land['land_no']).' ตำบล '.$land['district_name'].' อำเภอ  '.$land['amphur_name'].' จังหวัด  '.$land['province_name']);
                $land_assess = $this->land_assess->getByLand($land['land_id']);
                if (!empty($land_assess)) {
                    $i_assess = 1;
                    foreach ($land_assess as $list_assess) {
                        $data_land[$key_land.''.$i_assess] = array('detail'=>'                              ('.num2Thai($i_assess++).') เนื้อที่ '.num2Thai($list_assess['land_assess_area_rai']).' ไร่ '.num2Thai($list_assess['land_assess_area_ngan']).' งาน '.num2Thai($list_assess['land_assess_area_wah']).' ตารางวา ราคาตารางวาละ '.num2Thai(number_format($list_assess['land_assess_price'] / 400, 2)).' บาท  = '.num2Thai(number_format($list_assess['land_assess_inspector_sum'], 2)).' บาท');
                    }
                }

                $building_list = $this->building_record->getByLand($land['land_id']);
                if (!empty($building_list)) {
                    $i_building = 1;
                    foreach ($building_list as $key_building => $building) {
                        $data_building[] = array('detail'=>'                         '.num2Thai('2.'.$i_building++).'  '.$building['building_type_name'].' เลขที่ '.$building['building_no'].' ตำบล '.$building['district_name'].' อำเภอ '.$building['amphur_name'].' จังหวัด '.$building['province_name'].' = '.num2Thai(number_format($building['building_record_staff_assess_amount'], 2)).' บาท');
                    }
                }
            }
        }
        $TBS->MergeBlock('list_land', $data_land);
        $TBS->MergeBlock('list_building', $data_building);

        $TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
        $output_file_name = 'summary_'.date('Y-m-d').'.docx';
        $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
        $TBS->Show(OPENTBS_FILE, $temp_file);
        $this->send_download($temp_file, $output_file_name);
    }

    public function send_download($temp_file,$file) {
          $basename = basename($file);
          $length   = sprintf("%u", filesize($temp_file));

          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="' . $basename . '"');
          header('Content-Transfer-Encoding: binary');
          header('Connection: Keep-Alive');
          header('Expires: 0');
          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
          header('Pragma: public');
          header('Content-Length: ' . $length);
          ob_clean();
          flush();
          set_time_limit(0);
          readfile($temp_file);
          exit();
      }
}
