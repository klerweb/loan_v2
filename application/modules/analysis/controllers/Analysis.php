<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analysis extends MY_Controller
{
	private $title_page = 'พิจารณาการขอสินเชื่อ';

	function __construct()
	{
		parent::__construct();
		//library
	//	$this->load->library('loan_lib');
	}

	public function index()
	{
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_contract_model', 'loan_contract');

		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');

		$result = $this->loan_analysis->search($loan, $thaiid, $fullname);

		if(!empty($result))
		{
			foreach ($result as $key => $value)
			{
				$result[$key]['check_contract'] = $this->loan_contract->getByLoan($value['loan_id']);
			}
		}

		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $result
		);

		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_analysis/analysis.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('analysis', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	public function search()
	{
		$this->load->model('summary/loan_summary_model', 'loan_summary');

		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];

		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title_page => site_url('analysis')
			);

		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->loan_summary->searchNonAnalysis($loan, $thaiid, $fullname)
		);

		$data = array(
				'title' => 'เพิ่มแบบพิจารณา',
				'js_other' => array(),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('search', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	public function form($type, $id, $id_analysis='')
	{
		$this->load->model('title/title_model', 'title');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('career/career_model', 'career');
		$this->load->model('person/person_model', 'person');
		$this->load->model('loan_model', 'loan');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('summary/loan_summary_model', 'loan_summary');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_analysis_cal_model', 'loan_analysis_cal');
		$this->load->model('loan_schedule_model', 'loan_schedule');
		$this->load->model('loan_land_occupy_model', 'loan_land_occupy');
		$this->load->model('land/land_model', 'land_model');
		$this->load->model('building/building_model', 'building_model');

		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title_page => site_url('analysis')
		);

		$data_content = array(
				'title' => $this->title->all(),
				'province' => $this->province->all(),
				'amphur' => array(),
				'district' => array(),
				'married' =>  $this->married->all(),
				'loan' =>  $this->loan->getById($id),
				'loan_asset' =>  $this->loan_asset->getByLoan($id),
				'loan_debt' =>  $this->loan_debt->getByLoan($id),
				'loan_co' =>  $this->loan_co->getByLoan($id),
				'land_record' =>  $this->land_record->getByLoan($id),
				'loan_guarantee_bond' =>  $this->loan_guarantee_bond->getByLoan($id),
				'loan_guarantee_bookbank' =>  $this->loan_guarantee_bookbank->getByLoan($id),
				'loan_guarantee_bondsman' =>  $this->loan_guarantee_bondsman->getByLoan($id),
				'loan_summary' =>  $this->loan_summary->getByLoan($id),
				'loan_land_occupy' =>  $this->loan_land_occupy->getByLoan($id)
		);

		$data_content['loan']['loan_date'] = convert_dmy_th($data_content['loan']['loan_date']);
		$data_content['loan']['person']['title'] = $this->title->getById($data_content['loan']['person']['title_id']);
		$data_content['loan']['person']['person_age'] = empty($data_content['loan']['person']['person_birthdate'])? '' : age($data_content['loan']['person']['person_birthdate']);
		$data_content['loan']['person']['married'] = $this->married->getById($data_content['loan']['person']['status_married_id']);
		$data_content['loan']['person']['district'] = $this->district->getById($data_content['loan']['person']['person_addr_pre_district_id']);
		$data_content['loan']['person']['amphur'] = $this->amphur->getById($data_content['loan']['person']['person_addr_pre_amphur_id']);
		$data_content['loan']['person']['province'] = $this->province->getById($data_content['loan']['person']['person_addr_pre_province_id']);
		$data_content['loan']['person']['career'] = $this->career->getById($data_content['loan']['person']['career_id']);

		$loan_type = $this->loan_type->getByLoan($id);

		$data_content['loan_type_objective'] = '';
		$data_content['loan_creditor'] = '';
		$data_content['loan_real_amount'] = 0;
		$data_content['loan_type_farm_for'] = '';
		$data_content['check_type_farm'] = false;
		$data_content['loan_type_id'] = '';
		$data_content['loan_objective_id'] = '';
		$data_content['loan_creditor_id'] = '';
		$data_content['loan_creditor_title'] = '';
		$data_content['loan_creditor_fname'] = '';
		$data_content['loan_creditor_lname'] = '';
		$case = '';
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$data_content['loan_type_objective'] = '1';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_redeem_amount']);

					$data_content['loan_type_id'] = $list_type['loan_type_id'];
					$data_content['loan_objective_id'] = '1';

					$case .= $list_type['loan_type_redeem_case'].'<br/>';

					if(!empty($list_type['loan_type_redeem_owner']))
					{
						//$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
					//	$creditor_title = $this->title->getById($creditor['title_id']);

						$data_content['loan_creditor_id'] = null; //$list_type['loan_type_redeem_owner'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$data_content['loan_type_objective'] = '2';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_contract_amount']);

					$data_content['loan_type_id'] = $list_type['loan_type_id'];
					$data_content['loan_objective_id'] = '2';

					$case .= $list_type['loan_type_contract_case'].'<br/>';

					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];

						$data_content['loan_creditor_id'] = $list_type['loan_type_contract_creditor'];
						$data_content['loan_creditor_title'] = $creditor['title_id'];
						$data_content['loan_creditor_fname'] = $creditor['person_fname'];
						$data_content['loan_creditor_lname'] = $creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$data_content['loan_type_objective'] = '2';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_case_amount']);

					$data_content['loan_type_id'] = $list_type['loan_type_id'];
					$data_content['loan_objective_id'] = '2';

					$case .= $list_type['loan_type_case_case'].'<br/>';

					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];

						$data_content['loan_creditor_id'] = $list_type['loan_type_case_plaintiff_id'];
						$data_content['loan_creditor_title'] = $creditor['title_id'];
						$data_content['loan_creditor_fname'] = $creditor['person_fname'];
						$data_content['loan_creditor_lname'] = $creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$data_content['loan_type_objective'] = '3';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_sale_amount']);

					$data_content['loan_type_id'] = $list_type['loan_type_id'];
					$data_content['loan_objective_id'] = '4';
				}
				else
				{
					$data_content['check_type_farm'] = true;
					$data_content['loan_type_farm_for'] = $list_type['loan_type_farm_for'];
				}
			}
		}

		if($case!='') $case = substr($case, 0, -5);

		$data_content['loan_income_1'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '1');
		$data_content['loan_income_2'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '2');
		$data_content['loan_income_3'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '3');

		$data_content['income_all']['rai'] = 0;
		$data_content['income_all']['ngan'] = 0;
		$data_content['income_all']['wah'] = 0;
		$data_content['income_self']['rai'] = 0;
		$data_content['income_self']['ngan'] = 0;
		$data_content['income_self']['wah'] = 0;
		$data_content['income_renting']['rai'] = 0;
		$data_content['income_renting']['ngan'] = 0;
		$data_content['income_renting']['wah'] = 0;
		$data_content['income_approve']['rai'] = 0;
		$data_content['income_approve']['ngan'] = 0;
		$data_content['income_approve']['wah'] = 0;

		if(!empty($data_content['loan_income_1']))
		{
			foreach ($data_content['loan_income_1'] as $income1_value)
			{
				$self_rai = empty($income1_value['loan_activity_self_rai'])? 0 : intval($income1_value['loan_activity_self_rai']);
				$self_ngan = empty($income1_value['loan_activity_self_ngan'])? 0 : intval($income1_value['loan_activity_self_ngan']);
				$self_wah = empty($income1_value['loan_activity_self_wah'])? 0 : floatval($income1_value['loan_activity_self_wah']);
				$renting_rai = empty($income1_value['loan_activity_renting_rai'])? 0 : intval($income1_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income1_value['loan_activity_renting_ngan'])? 0 : intval($income1_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income1_value['loan_activity_renting_wah'])? 0 : floatval($income1_value['loan_activity_renting_wah']);
				$approve_rai = empty($income1_value['loan_activity_approve_rai'])? 0 : intval($income1_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income1_value['loan_activity_approve_ngan'])? 0 : intval($income1_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income1_value['loan_activity_approve_wah'])? 0 : floatval($income1_value['loan_activity_approve_wah']);

				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}

		if(!empty($data_content['loan_income_2']))
		{
			foreach ($data_content['loan_income_2'] as $income2_value)
			{
				$self_rai = empty($income2_value['loan_activity_self_rai'])? 0 : intval($income2_value['loan_activity_self_rai']);
				$self_ngan = empty($income2_value['loan_activity_self_ngan'])? 0 : intval($income2_value['loan_activity_self_ngan']);
				$self_wah = empty($income2_value['loan_activity_self_wah'])? 0 : floatval($income2_value['loan_activity_self_wah']);
				$renting_rai = empty($income2_value['loan_activity_renting_rai'])? 0 : intval($income2_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income2_value['loan_activity_renting_ngan'])? 0 : intval($income2_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income2_value['loan_activity_renting_wah'])? 0 : floatval($income2_value['loan_activity_renting_wah']);
				$approve_rai = empty($income2_value['loan_activity_approve_rai'])? 0 : intval($income2_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income2_value['loan_activity_approve_ngan'])? 0 : intval($income2_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income2_value['loan_activity_approve_wah'])? 0 : floatval($income2_value['loan_activity_approve_wah']);

				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}

		if(!empty($data_content['loan_income_3']))
		{
			foreach ($data_content['loan_income_3'] as $income3_value)
			{
				$self_rai = empty($income3_value['loan_activity_self_rai'])? 0 : intval($income3_value['loan_activity_self_rai']);
				$self_ngan = empty($income3_value['loan_activity_self_ngan'])? 0 : intval($income3_value['loan_activity_self_ngan']);
				$self_wah = empty($income3_value['loan_activity_self_wah'])? 0 : floatval($income3_value['loan_activity_self_wah']);
				$renting_rai = empty($income3_value['loan_activity_renting_rai'])? 0 : intval($income3_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income3_value['loan_activity_renting_ngan'])? 0 : intval($income3_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income3_value['loan_activity_renting_wah'])? 0 : floatval($income3_value['loan_activity_renting_wah']);
				$approve_rai = empty($income3_value['loan_activity_approve_rai'])? 0 : intval($income3_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income3_value['loan_activity_approve_ngan'])? 0 : intval($income3_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income3_value['loan_activity_approve_wah'])? 0 : floatval($income3_value['loan_activity_approve_wah']);

				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}

		$income_all = area($data_content['income_all']['rai'], $data_content['income_all']['ngan'], $data_content['income_all']['wah']);
		$data_content['income_all']['rai'] = $income_all['rai'];
		$data_content['income_all']['ngan'] = $income_all['ngan'];
		$data_content['income_all']['wah'] = $income_all['wah'];

		$income_self = area($data_content['income_self']['rai'], $data_content['income_self']['ngan'], $data_content['income_self']['wah']);
		$data_content['income_self']['rai'] = $income_self['rai'];
		$data_content['income_self']['ngan'] = $income_self['ngan'];
		$data_content['income_self']['wah'] = $income_self['wah'];

		$income_renting = area($data_content['income_renting']['rai'], $data_content['income_renting']['ngan'], $data_content['income_renting']['wah']);
		$data_content['income_renting']['rai'] = $income_renting['rai'];
		$data_content['income_renting']['ngan'] = $income_renting['ngan'];
		$data_content['income_renting']['wah'] = $income_renting['wah'];

		$income_approve = area($data_content['income_approve']['rai'], $data_content['income_approve']['ngan'], $data_content['income_approve']['wah']);
		$data_content['income_approve']['rai'] = $income_approve['rai'];
		$data_content['income_approve']['ngan'] = $income_approve['ngan'];
		$data_content['income_approve']['wah'] = $income_approve['wah'];

		$i_assess = 1;
		$data_content['building_count'] = 0;
		$data_content['building_sum'] = 0;
		$data_content['record_sum'] = 0;
		$data_content['land_assess'] = array();
		$data_content['land_main'] = array();
		$data_content['land_other'] = array();
		$data_content['asset_sum'] = 0;
		$data_content['list_land'] = array();
		$i_land = 1;
		if(!empty($data_content['land_record']))
		{
			foreach ($data_content['land_record'] as $key_land => $land_record)
			{
				$data_content['record_sum'] += floatval($land_record['land_record_staff_mortagage_price']);
				$data_content['asset_sum'] += floatval($land_record['land_estimate_price']);

				//echo 'land : land_estimate_price : '.floatval($land_record['land_estimate_price']).'<br/>';

				//$check_main = $land_record['loan_guarantee_land_type']=='1';

				$key_assess = array_search($land_record['land_id'], $data_content['list_land']);

				if(!$key_assess)
				{
					$key_assess = $i_land;
					$data_content['list_land'][$i_land++] = $land_record['land_id'];
				}

				$land_assess = $this->land_assess->getByLand($land_record['land_id']);

				if(!empty($land_assess))
				{
					foreach ($land_assess as $list_assess)
					{
						if(!empty($land_record['loan_type_id']))
						{
							$check_main = array_search($key_assess, $data_content['land_main']);
							if(!$check_main) $data_content['land_main'][$key_assess] = '';
						}
						else
						{
							$check_other = array_search($key_assess, $data_content['land_other']);
							if(!$check_other) $data_content['land_other'][$key_assess] = '';
						}

						$list_assess['key'] = $key_assess;

						$data_content['land_assess'][$i_assess++] = $list_assess;
					}
				}

				$building_record = $this->building_record->getByLand($land_record['land_id']);
				$data_content['land_record'][$key_land]['building_record'] = $building_record;

				if(!empty($building_record))
				{
					foreach ($building_record as $list_building)
					{
						$building_amount = floatval($list_building['building_record_staff_assess_amount']);
						$data_content['asset_sum'] += floatval($list_building['building_estimate_price']);

						//echo 'building : building_estimate_price : '.floatval($list_building['building_estimate_price']).'<br/>';

						$data_content['record_sum'] += $building_amount;
						$data_content['building_sum'] += $building_amount;

						$data_content['building_count']++;
					}
				}
			}
		}

		if(!empty($data_content['loan_land_occupy']))
		{
			foreach ($data_content['loan_land_occupy'] as $key_occupy => $land_occupy)
			{
				$land= $this->land_model->get_land($land_occupy['land_id']);

				$data_content['asset_sum'] += floatval($land['land_estimate_price']);
				//$data_content['asset_sum'] += floatval($land_occupy['estimate_price']);

			}
		}

		if(empty($data_content['loan']['loan_spouse']))
		{
			$data_content['loan']['spouse_name'] = '';
			$data_content['loan']['spouse_age'] = '';
		}
		else
		{
			$data_content['loan']['spouse'] = $this->person->getById($data_content['loan']['loan_spouse']);
			$data_content['loan']['spouse']['title'] = $this->title->getById($data_content['loan']['spouse']['title_id']);
			$data_content['loan']['spouse_name'] = $data_content['loan']['spouse']['title']['title_name'].$data_content['loan']['spouse']['person_fname'].'  '.$data_content['loan']['spouse']['person_lname'];
			$data_content['loan']['spouse_age'] = empty($data_content['loan']['spouse']['person_birthdate'])? '' : age($data_content['loan']['spouse']['person_birthdate']);
		}

		if(!empty($data_content['loan_asset']))
		{
			foreach ($data_content['loan_asset'] as $list_asset)
			{
				$data_content['asset_sum'] += floatval($list_asset['loan_asset_amount']);

				//echo 'loan_asset : loan_asset_amount : '.floatval($list_asset['loan_asset_amount']).'<br/>';
			}
		}

		$data_content['debt_sum'] = 0;
		if(!empty($data_content['loan_debt']))
		{
			foreach ($data_content['loan_debt'] as $list_debt)
			{
				$data_content['debt_sum'] += floatval($list_debt['loan_debt_amount']);
			}
		}

		if($type=='add')
		{
			$title_page = 'เพิ่มแบบพิจารณา';

			$data_content['id'] = '';
			$data_content['data'] = $this->loan_analysis->getModel();
			$data_content['data']['loan_id'] = $id;
			$data_content['data']['loan_analysis_id'] = $id_analysis;
			$data_content['data']['loan_analysis_year'] = $data_content['loan']['loan_period_year'];
			$data_content['data']['loan_analysis_cycle'] = $data_content['loan']['loan_period_year'];

			$_SESSION['loan_analysis_list_cal'] = array();

			// echo '<pre>';
			// print_r($data_content['data']);
			// echo '</pre>';

			$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
		}
		else
		{
			$data_content['data'] = $this->loan_analysis->getById($id_analysis);

			$_SESSION['loan_analysis_list_cal'] = $this->loan_analysis_cal->getByAnalysis($id_analysis);

			if($data_content['data']['loan_analysis_cause']=='') $data_content['data']['loan_analysis_cause'] = $case;

			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มแบบพิจารณา';

				$data_content['id'] = '';
				$data_content['data'] = $this->loan_analysis->getModel();
				$data_content['data']['loan_id'] = $id;
				$data_content['data']['loan_analysis_id'] = $id_analysis;
				$data_content['data']['loan_analysis_year'] = $data_content['loan']['loan_period_year'];
				$data_content['data']['loan_analysis_remain'] = $data_content['loan']['loan_amount'];

				$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
			}
			else
			{
				$title_page = 'แก้ไขแบบพิจารณา';

				$data_content['data']['loan_id'] = $id;
				$data_content['data']['loan_analysis_id'] = $id_analysis;

				$schedule = $this->loan_schedule->getByLoan($id);

				if(!empty($schedule))
				{
					$list_round =  array();
					foreach ($schedule as $list_schedule)
					{
						$list_round[$list_schedule['loan_schedule_times']] = array(
								'principle' => floatval($list_schedule['loan_schedule_amount']),
								'other' => intval($list_schedule['loan_schedule_other']),
								'principle_other' => intval($list_schedule['loan_schedule_principle']),
								'interest_other' => intval($list_schedule['loan_schedule_interest'])
						);
					}

					$_SESSION['loan_analysis_cal'] = $list_round;
				}
				else
				{
					$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
				}
			}
		}

		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_analysis/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);

		$this->parser->parse('main', $data);
	}

	public function expenditure($id, $id_analysis='', $type='')
	{
		$this->load->model('loan_model', 'loan');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_income_model', 'loan_income');

		$loan =  $this->loan->getById($id);
		$person[] = $loan['person_id'];

		$loan_co =  $this->loan_co->getByLoan($id);

		if(!empty($loan_co))
		{
			foreach ($loan_co as $list_co)
			{
				$person[] = $list_co['person_id'];
			}
		}

		$data_content['loan_income_1'] = $this->loan_income->getByPersonGroup($person, '1');
		$data_content['loan_income_2'] = $this->loan_income->getByPersonGroup($person, '2');
		$data_content['loan_income_3'] = $this->loan_income->getByPersonGroup($person, '3');
		$data_content['loan_expenditure_1'] = $this->loan_expenditure->getByPersonGroup($person, '1');
		$data_content['loan_expenditure_2'] = $this->loan_expenditure->getByPersonGroup($person, '2');

		if($type=='html')
		{
			return $this->parser->parse('expenditure_html', $data_content);
		}
		else
		{
			$this->parser->parse('expenditure', $data_content);
		}
	}

	public function cal_ana_add($amount, $type, $year)
	{
		$amount = floatval($amount);
		$type = intval($type);
		$year = intval($year);

		if($type==1)
		{
			$round = $year * 12;
			$income = $income / 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
			$income = $income / 2;
		}
		else
		{
			$round = $year;
		}

		$principle = $amount / $round;

		$list_round = array();
		for ($r=1; $r<=$round; $r++)
		{
			$list_round[$r] = array(
					'principle' => $principle,
					'other' => 0,
					'principle_other' => 0,
					'interest_other' => 0
			);
		}

		$_SESSION['loan_analysis_cal'] = $list_round;
	}

	public function cal_ana()
	{
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));

		if($type==1)
		{
			$round = $year * 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
		}
		else
		{
			$round = $year;
		}

		$principle = $amount / $round;

		$data = array(
			'round' => $round,
			'principle' => $principle
		);

		$this->parser->parse('cal_ana', $data);
	}

	public function save_cal()
	{
		$round = $this->input->post('txtCalRound');
		$amount = $this->input->post('txtCalAmount');
		$other = $this->input->post('txtCalOther');
		$principle_other = $this->input->post('txtCalPrinciple');
		$interest_other = $this->input->post('txtCalInterest');

		$list_round = array();
		foreach ($round as $r_key => $r_value)
		{
			$list_round[$r_value] = array(
						'principle' => $amount[$r_key],
						'other' => $other[$r_key],
						'principle_other' => $principle_other[$r_key],
						'interest_other' => $interest_other[$r_key]
					);
		}

		$_SESSION['loan_analysis_cal'] = $list_round;
	}

	public function save_cal2()
	{
		$round = $this->input->post('txtCalRound');
		$amount = $this->input->post('txtCalAmount');
		$start_list = $this->input->post('txtCalStart');
		$end_list = $this->input->post('txtCalEnd');

		$list_cal = array();
		$list_round = array();
		foreach ($round as $r_key => $r_value)
		{
			$start = intval($start_list[$r_key]);
			$end = intval($end_list[$r_key]);

			$list_cal[] = array(
						'loan_analysis_cal_time' => $r_value,
						'loan_analysis_cal_start' => $start,
						'loan_analysis_cal_end' => $end,
						'loan_analysis_cal_amount' => $amount[$r_key]
					);

			for($r = $start; $r <= $end; $r++)
			{
				$list_round[$r] = $_SESSION['loan_analysis_cal'][$r];
				$list_round[$r]['principle'] = $amount[$r_key];

				if(empty($list_round[$r]['other'])) $list_round[$r]['other'] = 0;
				if(empty($list_round[$r]['principle_other'])) $list_round[$r]['principle_other'] = 0;
				if(empty($list_round[$r]['interest_other'])) $list_round[$r]['interest_other'] = 0;
			}
		}

		$_SESSION['loan_analysis_list_cal'] = $list_cal;
		$_SESSION['loan_analysis_cal'] = $list_round;
	}

	public function cal_ana2()
	{
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$round = floatval(str_replace(",", "", $this->input->post('cycle')));
	//	$type = intval($this->input->post('type'));
	//	$year = intval($this->input->post('year'));
    //
		// if($type==1)
		// {
		// 	$round = $year * 12;
		// }
		// else if($type==6)
		// {
		// 	$round = $year * 2;
		// }
		// else
		// {
		// 	$round = $year;
		// }

		$data = array(
				'round' => $round,
				'amount' => $amount
		);

		$this->parser->parse('cal_ana2', $data);
	}

	public function cal()
	{
		$income = floatval(str_replace(",", "", $this->input->post('income')));
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$interest = intval($this->input->post('interest')) / 100;
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
		$pay = $this->input->post('pay');
		$start = $this->input->post('start');

		$start = convert_ymd_en($start);
		$pay = convert_ymd_en($pay);

		$date_start = $start;
		$date_start = new DateTime($date_start);
		$date_start->sub(new DateInterval('P1D'));
		$date_start = $date_start->format('Y-m-d');
		$date_end = $pay;

		$list_round = array();
		$list_month = array();
		$remain = $amount;
		if(!empty($_SESSION['loan_analysis_cal']))
		{
			$i_key = 1;
			foreach ($_SESSION['loan_analysis_cal'] as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				if($i_key==1) $day_between--;

				$interest_per = number_format(($remain * $interest )  * ($day_between / 365), 2, '.', '');

				if($interest_per < 0)  $interest_per = 0;

				$list_round[$r_key] = array(
						'income' => $income * $type /12,
						'principle' => $r_value['principle'],
						'remain' => $remain,
						'interest' => $interest_per,
						'other' => $r_value['other'],
						'principle_other' => $r_value['principle_other'],
						'interest_other' => $r_value['interest_other'],
						'date' => $date_end
				);
				// echo "<pre>";
				// print_r( $date_start);
				// print_r("---");
				// print_r( $date_end);
				// print_r("---");
				// print_r( $remain);
				// print_r("---");
				// print_r( $day_between);
				// print_r("---");
				// print_r( $r_value['principle']);
				// print_r("---");
				// print_r( $interest_per);
				// echo "</pre>";

				list($y_end, $m_end, $d_end) = explode('-', $date_end);
				$list_month[] = $m_end;

				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$type.'M'));
				$date_end = $date_end->format('Y-m-d');

				$i_key++;
			}
		}

		$list_month = array_unique($list_month);

		$month = '';
		foreach ($list_month as $data_month)
		{
			$month .= thai_month($data_month).', ';
		}

		if(!empty($month)) $month = substr($month, 0, -2);

		$data = array(
			'year' => $year,
			'month' => $month,
			'income' => $income,
			'amount' => $amount,
			'interest' => $interest,
			'type' => $type,
			'pay' => $pay,
			'start' => $start,
			'fiscal_start' => fiscalYear($start),
			'fiscal_end' => fiscalYear($date_start),
			'list_round' => $list_round
		);

		$_SESSION['loan_analysis'] = $data;

		$this->parser->parse('cal', $data);
	}

	public function save_expenditure()
	{
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_debt_model', 'loan_debt');

		$amount = forNumberInt($this->input->post('amount'));

		$array_expenditure['loan_expenditure_amount'.$this->input->post('year')] = $amount;
		$result_expenditure = $this->loan_expenditure->save($array_expenditure, $this->input->post('id'));

		$array_debt['loan_debt_amount'] = $amount;
		$result_debt = $this->loan_debt->saveByLoan($array_debt, $this->input->post('loan'), $this->input->post('name'));
	}

	public function save_other()
	{
		$_SESSION['loan_analysis_cal'][$this->input->post('round')]['other'] = forNumberInt($this->input->post('other'));
	}

	public function save_principle_other()
	{
		$_SESSION['loan_analysis_cal'][$this->input->post('round')]['principle_other'] = forNumberInt($this->input->post('principle_other'));
	}

	public function save_interest_other()
	{
		$_SESSION['loan_analysis_cal'][$this->input->post('round')]['interest_other'] = forNumberInt($this->input->post('interest_other'));
	}

	public function save()
	{
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('person/person_model', 'person');

		if(!empty($this->input->post('txtTypeId')))
		{
			if($this->input->post('txtObjectiveId')=='1')
			{
				$array_type['loan_type_redeem_amount'] = forNumber($this->input->post('txtRealAmount'));

				$array_person['person_fname'] = $this->input->post('txtCreditorFname');
				$array_person['person_lname'] = $this->input->post('txtCreditorLname');

			}
			else if($this->input->post('txtObjectiveId')=='2')
			{
				$array_type['loan_type_contract_amount'] = forNumber($this->input->post('txtRealAmount'));

				$array_person['person_fname'] = $this->input->post('txtCreditorFname');
				$array_person['person_lname'] = $this->input->post('txtCreditorLname');

				$this->person->save($array_person, $this->input->post('txtCreditorId'));
			}
			else if($this->input->post('txtObjectiveId')=='3')
			{
				$array_type['loan_type_case_amount'] = forNumber($this->input->post('txtRealAmount'));

				$array_person['person_fname'] = $this->input->post('txtCreditorFname');
				$array_person['person_lname'] = $this->input->post('txtCreditorLname');

				$this->person->save($array_person, $this->input->post('txtCreditorId'));
			}
			else if($this->input->post('txtObjectiveId')=='4')
			{
				$array_type['loan_type_sale_amount'] = forNumber($this->input->post('txtRealAmount'));
			}

			$this->loan_type->save($array_type, $this->input->post('txtTypeId'));
		}

		$chkEgov = empty($this->input->post('chkEgov'))? '0' : '1';
		$chkCooperative = empty($this->input->post('chkCooperative'))? '0' : '1';
		$chkBank = empty($this->input->post('chkBank'))? '0' : '1';
		$chkBankOther = empty($this->input->post('chkBankOther'))? '0' : '1';

		$array['loan_id'] = $this->input->post('txtLoan');
		$array['loan_analysis_egov'] = $chkEgov;
		$array['loan_analysis_egov_detail'] = $this->input->post('txtEgov');
		$array['loan_analysis_cooperative'] = $chkCooperative;
		$array['loan_analysis_cooperative_detail'] = $this->input->post('txtCooperative');
		$array['loan_analysis_bank'] = $chkBank;
		$array['loan_analysis_bank_detail'] = $this->input->post('txtBank');
		$array['loan_analysis_bankother'] = $chkBankOther;
		$array['loan_analysis_bankother_detail'] = $this->input->post('txtBankOther');
		$array['loan_analysis_behavior'] = $this->input->post('rdBehavior');
		$array['loan_analysis_behavior_detail'] = $this->input->post('txtBehavior');
		$array['loan_analysis_behavior_occupation'] = $this->input->post('rdBehaviorOccupation');
		$array['loan_analysis_behavior_occupation_detail'] = $this->input->post('txtBehaviorOccupation');
		$array['loan_analysis_behavior_farm'] = $this->input->post('rdBehaviorFarm');
		$array['loan_analysis_behavior_farm_detail'] = $this->input->post('txtBehaviorFarm');
		$array['loan_analysis_behavior_health'] = $this->input->post('rdBehaviorHealth');
		$array['loan_analysis_behavior_health_detail'] = $this->input->post('txtBehaviorHealth');
		$array['loan_analysis_behavior_other'] = $this->input->post('txtBehaviorOther');
		$array['loan_analysis_deadline'] = convert_ymd_en($this->input->post('txtDeadline'));
		$array['loan_analysis_remain'] = forNumberInt($this->input->post('txtRemain'));
		$array['loan_analysis_within'] = convert_ymd_en($this->input->post('txtWithin'));
		$array['loan_analysis_cause'] = $this->input->post('txtCause');
		$array['loan_analysis_interest'] = $this->input->post('ddlCalInterest');
		$array['loan_analysis_length'] = $this->input->post('ddlCalType');
		$array['loan_analysis_year'] = $this->input->post('txtShowYear');
		$array['loan_analysis_cycle'] = $this->input->post('txtCycle');
		$array['loan_analysis_start'] = convert_ymd_en($this->input->post('txtCalStart'));
		$array['loan_analysis_pay'] = convert_ymd_en($this->input->post('txtCalPay'));
		$array['loan_analysis_amount'] = forNumberInt($this->input->post('txtCalAmount'));
		$array['loan_analysis_investigate_comment'] = $this->input->post('txtInvestigateComment');
		$array['loan_analysis_investigate_honestly'] = $this->input->post('txtInvestigateHonestly');
		$array['loan_analysis_investigate_cause'] = $this->input->post('txtInvestigateCause');
		$array['loan_analysis_investigate_must'] = $this->input->post('txtInvestigateMust');
		$array['loan_analysis_investigate_matter'] = $this->input->post('txtInvestigateMatter');
		$array['loan_analysis_approve_status'] = $this->input->post('rdApproveStatus');
		$array['loan_analysis_approve_amount'] = forNumberInt($this->input->post('txtApproveAmount'));
		$array['loan_analysis_approve_date'] = convert_ymd_en($this->input->post('txtApproveDate'));
		$array['loan_analysis_approve_conditions'] = $this->input->post('txtApproveConditions');
		$array['loan_analysis_approve_not_type'] = $this->input->post('rdApproveNotType');
		$array['loan_analysis_approve_not_year'] = $this->input->post('txtApproveNotYear');
		$array['loan_analysis_approve_not_case'] = $this->input->post('txtApproveNotCase');
		$array['loan_analysis_approve_sector_status'] = $this->input->post('rdApproveSectorStatus');
		$array['loan_analysis_approve_sector_amount'] = forNumberInt($this->input->post('txtApproveSectorAmount'));
		$array['loan_analysis_approve_sector_date'] = convert_ymd_en($this->input->post('txtApproveSectorDate'));
		$array['loan_analysis_approve_sector_conditions'] = $this->input->post('txtApproveSectorConditions');
		$array['loan_analysis_approve_sector_not_case'] = $this->input->post('txtApproveSectorNotCase');
		$array['loan_analysis_income'] = $this->input->post('txtYear'.$this->input->post('ddlCalIncomeYear').'Sum');
		$array['loan_analysis_income_year'] = $this->input->post('ddlCalIncomeYear');
		$array['loan_analysis_status'] = '1';

		$id = $this->loan_analysis->save($array, $this->input->post('txtId'));

		if($this->input->post('txtId')=='')
		{
			$data = array(
					'alert' => 'บันทึกแบบประเมินเรียบร้อยแล้ว',
					'link' => site_url('analysis'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขแบบประเมินเรียบร้อยแล้ว',
					'link' => site_url('analysis'),
			);
		}

		$this->parser->parse('redirect', $data);
	}

	public function word($id, $id_analysis)
	{
		include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$this->load->model('loan_model', 'loan');
		$this->load->model('title/title_model', 'title');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('career/career_model', 'career');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_owner_model', 'land_owner');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_guarantee_land_model', 'loan_guarantee_land');
		$this->load->model('loan_schedule_model', 'loan_schedule');
		$this->load->model('loan_land_occupy_model', 'loan_land_occupy');
		$this->load->model('building/building_model', 'building_model');


		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$data = $this->loan->getById($id);
		$analysis = $this->loan_analysis->getById($id_analysis);

		$person[] = $data['person_id'];

		$data['person']['title'] = $this->title->getById($data['person']['title_id']);
		$data['person']['person_age'] = empty($data['person']['person_birthdate'])? '' : age($data['person']['person_birthdate']);
		$data['person']['district'] = $this->district->getById($data['person']['person_addr_pre_district_id']);
		$data['person']['amphur'] = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
		$data['person']['province'] = $this->province->getById($data['person']['person_addr_pre_province_id']);
		$data['person']['career'] = $this->career->getById($data['person']['career_id']);

		$loan_type = $this->loan_type->getByLoan($id);

		$loan_type_objective1 = '';
		$loan_type_objective2 = '';
		$loan_type_objective3 = '';
		$loan_creditor = '';
		$loan_real_amount = 0;
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$loan_type_objective1 = '(√)';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_redeem_amount']);

					if(!empty($list_type['loan_type_redeem_owner']))
					{
						$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(√)';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_contract_amount']);

					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(√)';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_case_amount']);

					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '(√)';

					$loan_real_amount += floatval($list_type['loan_type_sale_amount']);
				}
			}
		}

		$married_list = $this->married->all();

		$html_married = '';
		foreach ($married_list as $married)
		{
			$html_married .= $married['status_married_id']==$data['person']['status_married_id']? '(√) ' : '( ) ';
			$html_married .= $married['status_married_name'].' ';
		}

		if(empty($data['loan_spouse']))
		{
			$spouse_name = '-';
			$spouse_age = '-';
		}
		else
		{
			$spouse = $this->person->getById($data['loan_spouse']);
			$spouse_title = $this->title->getById($spouse['title_id']);
			$spouse_name = $spouse_title['title_name'].$spouse['person_fname'].'  '.$spouse['person_lname'];
			$spouse_age = empty($spouse['person_birthdate'])? '' : age($spouse['person_birthdate']);
		}

		$template = 'word/template/13_analysis.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'analysis_'.date('Y-m-d').'.docx';
		$TBS->Show(OPENTBS_DOWNLOAD, $output_file_name);
		exit();
	}

	public function word_old($id, $id_analysis)
	{
		$this->load->library('PHPWord');

		$this->load->model('loan_model', 'loan');
		$this->load->model('title/title_model', 'title');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('career/career_model', 'career');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_owner_model', 'land_owner');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_guarantee_land_model', 'loan_guarantee_land');
		$this->load->model('loan_schedule_model', 'loan_schedule');
		$this->load->model('loan_land_occupy_model', 'loan_land_occupy');
		$this->load->model('loan_doc_model', 'loan_doc');
		$this->load->model('building/building_model', 'building_model');
		$this->load->model('land/land_model', 'land_model');
		$this->load->model('user/user_model', 'user_model');
    $this->load->model('employee/employee_model');

		$PHPWord = new PHPWord();

		$section = $PHPWord->createSection(array('orientation'=>'landscape'));

		$PHPWord->addParagraphStyle('p_right', array('align'=>'right', 'spaceAfter'=>0));
		$PHPWord->addParagraphStyle('p_center', array('align'=>'center', 'spaceAfter'=>0));
		$PHPWord->addParagraphStyle('p_0', array('spaceAfter'=>0));

		$PHPWord->addFontStyle('font_number', array('name' => 'TH SarabunPSK', 'size' => 16, 'color' => 'aaaaaa', 'bold' => true));
		$PHPWord->addFontStyle('font_title', array('name' => 'TH SarabunPSK', 'size' => 22, 'color' => '000000', 'bold' => true));
		$PHPWord->addFontStyle('font_head_top', array('name' => 'TH SarabunPSK', 'size' => 16, 'color' => '000000', 'bold' => true));
		$PHPWord->addFontStyle('font_bold', array('name' => 'TH SarabunPSK', 'size' => 16, 'color' => '000000', 'bold' => true));
		$PHPWord->addFontStyle('font_underline', array('name' => 'TH SarabunPSK', 'size' => 16, 'color' => '000000', 'bold' => false , 'underline'=>'dotted'));
		$PHPWord->addFontStyle('font_head', array('name' => 'TH SarabunPSK', 'size' => 12, 'color' => '000000', 'bold' => false));
		$PHPWord->addFontStyle('font_normal', array('name' => 'TH SarabunPSK', 'size' => 16, 'color' => '000000', 'bold' => false));
		$PHPWord->addFontStyle('font_comment', array('name' => 'TH SarabunPSK', 'size' => 14, 'color' => '000000', 'bold' => false));

		$section->addText('บจธ. สช. - ๑๓', 'font_number', 'p_right');

		$table_head = $section->addTable();

		$table_head->addRow();
		$table_head->addCell(1300)->addImage('assets\images\logo_invoice.png', array('width'=>80, 'height'=>99, 'align'=>'left'));
		$cell_head = $table_head->addCell(13700);
		$cell_head->addText('สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)', 'font_head_top', 'p_0');
		$cell_head->addText('เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐', 'font_head', 'p_0');
		$cell_head->addText('โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘', 'font_head', 'p_0');
		$cell_head->addText('The Land Bank Administration Institute (Public Organization)', 'font_head', 'p_0');
		$cell_head->addText('210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400 ', 'font_head', 'p_0');
		$cell_head->addText('Tel. 0 2278 1648 Fax 0 2278 1148 www.labai.or.th', 'font_head', 'p_0');
		$cell_head->addImage('assets\icon\line2.png', array('width'=>800, 'height'=>1, 'align'=>'left'));

		$section->addText('แบบสรุปผลการพิจารณาการขอสินเชื่อจากสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.)', 'font_head_top', 'p_center');

		$section->addTextBreak(1);

		$data = $this->loan->getById($id);
		$analysis = $this->loan_analysis->getById($id_analysis);

		$person[] = $data['person_id'];

		$data['person']['title'] = $this->title->getById($data['person']['title_id']);
		$data['person']['person_age'] = empty($data['person']['person_birthdate'])? '' : age($data['person']['person_birthdate']);
		$data['person']['district'] = $this->district->getById($data['person']['person_addr_pre_district_id']);
		$data['person']['amphur'] = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
		$data['person']['province'] = $this->province->getById($data['person']['person_addr_pre_province_id']);
		$data['person']['career'] = $this->career->getById($data['person']['career_id']);

		$location = $data['loan_location']=='สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)'? '(√) บจธ. สำนักงานใหญ่ ( ) อื่นๆ' : '( ) บจธ. สำนักงานใหญ่ (√) อื่นๆ '.num2Thai($data['loan_location']);
		$section->addText(' 	 	 ผู้ขอสินเชื่อได้ยื่นความประสงค์ขอสินเชื่อ ณ '.$location.' เมื่อวันที่ '.num2Thai($data['loan_datefull']).' จึงขอเสนอผลการพิจารณาการขอสินเชื่อของผู้ขอสินเชื่อ ดังนี้', 'font_normal', 'p_0');

		// $section->addText($location,'font_underline', 'p_0');
		// $section->addText(' เมื่อวันที่ '.num2Thai($data['loan_datefull']).
		// ' จึงขอเสนอผลการพิจารณาการขอสินเชื่อของผู้ขอสินเชื่อ ดังนี้', 'font_normal', 'p_0');

		$section->addText(' 	 	 ๑. ข้อมูลทั่วไปของผู้ขอสินเชื่อ', 'font_normal', 'p_0');
		$section->addText(' 	 	      ๑.๑ ชื่อ - สกุล '.$data['person']['title']['title_name'].$data['person']['person_fname'].' '.$data['person']['person_lname'].' เลขประจำตัวประชาชน '.num2Thai(formatThaiid($data['person']['person_thaiid'])).' ที่อยู่ บ้านเลขที่ '.num2Thai($data['person']['person_addr_pre_no']).' หมู่ที่ '.(empty($data['person']['person_addr_pre_moo'])? ' - ' : num2Thai($data['person']['person_addr_pre_moo'])).' หมู่บ้าน - ตำบล/แขวง '.$data['person']['district']['district_name'].' อำเภอ/เขต '.$data['person']['amphur']['amphur_name'].' จังหวัด '.$data['person']['province']['province_name'].' ประกอบอาชีพ '.num2Thai($data['person']['career']['career_name'].' '.$data['person']['person_career_other']).' จำนวนสมาชิกในครัวเรือน '.num2Thai($data['loan_member_amount']).' คน จำนวนแรงงานในครัวเรือน '.num2Thai($data['loan_labor_amount']).' คน อายุของผู้ขอสินเชื่อ '.num2Thai($data['person']['person_age']).' ปี กรณีผู้ขอสินเชื่ออายุมากกว่า ๖๕ ปี หรือมีความสามารถในการชำระหนี้ได้ไม่เพียงพอ ผู้ขอสินเชื่อได้เสนอผู้กู้ร่วม คือ', 'font_normal', 'p_0');

		$loan_co =  $this->loan_co->getByLoan($id);

		if(!empty($loan_co))
		{
			$i_co = 1;
			foreach ($loan_co as $list_co)
			{
				$person[] = $list_co['person_id'];
				$age_co = age($list_co['person_birthdate']);

				$section->addText(' 	 	            ('.num2Thai($i_co++).') ชื่อ - สกุล '.$list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname'].' อายุ '.num2Thai($age_co).' ปี มีความเกี่ยวข้องกับผู้ขอสินเชื่อ '.$list_co['relationship_name'].' ประกอบอาชีพ '.$list_co['career_name'], 'font_normal', 'p_0');
			}
		}
		else
		{
			$section->addText(' 	 	            (๑) ชื่อ – สกุลของผู้กู้ร่วม - อายุ - ปี มีความเกี่ยวข้องกับผู้ขอสินเชื่อ - ประกอบอาชีพ - ', 'font_normal', 'p_0');
		}

		$married_list = $this->married->all();

		$html_married = '';
		foreach ($married_list as $married)
		{
			$html_married .= $married['status_married_id']==$data['person']['status_married_id']? '(√) ' : '( ) ';
			$html_married .= num2Thai($married['status_married_name']).'  ';
		}

		$section->addText(' 	 	      ๑.๒ สถานภาพ '.$html_married, 'font_normal', 'p_0');

		if(empty($data['loan_spouse']))
		{
			$spouse_name = ' - ';
			$spouse_age = ' - ';
		}
		else
		{
			$spouse = $this->person->getById($data['loan_spouse']);
			$spouse_title = $this->title->getById($spouse['title_id']);
			$spouse_name = $spouse_title['title_name'].$spouse['person_fname'].'  '.$spouse['person_lname'];
			$spouse_age = empty($spouse['person_birthdate'])? ' - ' : num2Thai(age($spouse['person_birthdate']));
		}

		$section->addText(' 	 	            ชื่อ – สกุลของคู่สมรส (ถ้ามี) '.$spouse_name.' อายุ '.$spouse_age.' ปี', 'font_normal', 'p_0');
		$section->addText(' 	 	      ๑.๓ '.($analysis['loan_analysis_egov']=='1'? '(√)' : '( )').' เป็นลูกค้า ธ.ก.ส. (ระบุ) '.(empty($analysis['loan_analysis_egov_detail'])? ' - ' : num2Thai($analysis['loan_analysis_egov_detail'])).' '.($analysis['loan_analysis_cooperative']=='1'? '(√)' : '( )').' เป็นสมาชิกสหกรณ์ (ระบุ) '.(empty($analysis['loan_analysis_cooperative_detail'])? ' - ' : num2Thai($analysis['loan_analysis_cooperative_detail'])).' '.($analysis['loan_analysis_bank']=='1'? '(√)' : '( )').' เป็นลูกค้าสถาบันการเงิน (ระบุ) '.(empty($analysis['loan_analysis_bank_detail'])? ' - ' : num2Thai($analysis['loan_analysis_bank_detail'])).' '.($analysis['loan_analysis_bankother']=='1'? '(√)' : '( )').' อื่นๆ (ระบุ) '.(empty($analysis['loan_analysis_bankother_detail'])? ' - ' : num2Thai($analysis['loan_analysis_bankother_detail'])), 'font_normal', 'p_0');

		$loan_asset =  $this->loan_asset->getByLoan($id);

		$asset_sum = 0;
		if(!empty($loan_asset))
		{
			foreach ($loan_asset as $list_asset)
			{
				$asset_sum += floatval($list_asset['loan_asset_amount']);
			}
		}

		$loan_land_occupy = $this->loan_land_occupy->getByLoan($id);

		if(!empty($loan_land_occupy))
		{
			foreach ($loan_land_occupy as $key_occupy => $land_occupy)
			{
				$land= $this->land_model->get_land($land_occupy['land_id']);

				$asset_sum += floatval($land['land_estimate_price']);
				//$asset_sum += floatval($land_occupy['estimate_price']);
			}
		}

		$debt =  $this->loan_debt->getByLoan($data['loan_id']);

		$debt_text = '';
		$debt_owner_text = '';
		$debt_sum = 0;
		if(!empty($debt))
		{
			foreach ($debt as $list_debt)
			{
				if($list_debt['loan_debt_owner_type']=='1' || $list_debt['loan_debt_owner_type']=='2')
							$debt_text .= $list_debt['loan_debt_name'].' จำนวน '.number_format($list_debt['loan_debt_amount_remain'], 2).' บาท, ';
				if($list_debt['loan_debt_owner_type']=='1')
							$debt_owner_text .= $list_debt['loan_debt_name'].' จำนวน '.number_format($list_debt['loan_debt_amount_remain'], 2).' บาท, ';

				//$debt_sum += floatval($list_debt['loan_debt_amount']);
				$debt_sum += floatval($list_debt['loan_debt_amount_remain']);
			}
		}

		if($debt_text!='') $debt_text = num2Thai(substr($debt_text, 0, -2)).'  ';
		if($debt_owner_text!='') $debt_owner_text = ' ('.num2Thai(substr($debt_owner_text, 0, -2)).')';

		$check_guarantee = '( )';
		$guarantee = '';
		$check_guarantee_land = '( )';

		$loan_guarantee_land =  $this->loan_guarantee_land->getByLoan($id);
		$loan_type = $this->loan_type->getByLoan($id);

		$guarantee_land = '';
		if(!empty($loan_guarantee_land))
		{
			foreach ($loan_guarantee_land as $list_land)
			{
				//$check_guarantee = '(√)';
				//$check_guarantee_land = '(√)';

				$guarantee .= 'ที่ดินประเภท '.$list_land['land_type_name'].' เลขที่ '.$list_land['land_no'].' เลขที่ดิน '.$list_land['land_addr_no'].' ต.'.$list_land['district_name'].' อ.'.$list_land['amphur_name'].' จ.'.$list_land['province_name'].' เนื้อที่ '.$list_land['land_area_rai'].'-'.$list_land['land_area_ngan'].'-'.$list_land['land_area_wah'].' ไร่, ';
			}
		}
		// echo "<pre>";
		// print_r( $loan_type);
		// echo "</pre>";
		if(empty($loan_type)){
			$check_guarantee_land = '( )';
		}else{
			//$check_guarantee_land = '(√)';
		}

		$list_land_record =  $this->land_record->getByLoan($id);

		$i_assess = 1;
		$building_count = 0;
		$building_sum = 0;
		$record_sum = 0;
		//$land_assess = array();
		$land_owner =  '';
		$land_main = '';
		$land_other = '';
		$list_land = array();
		$i_land = 1;
		if(!empty($list_land_record))
		{
			foreach ($list_land_record as $key_land => $land_record)
			{
				$record_sum += floatval($land_record['land_record_staff_mortagage_price']);
				$asset_sum += floatval($land_record['land_estimate_price']);

				$key_assess = array_search($land_record['land_id'], $list_land);

				if(!$key_assess)
				{
					$key_assess = $i_land;
					$list_land[$i_land++] = $land_record['land_id'];
				}

				if(!empty($land_record['loan_type_id']))
				{
					$land_main .= $key_assess.', ';
					$list_land_owner = $this->land_owner->getByLand($land_record['land_id']);

					if(!empty($list_land_owner))
					{
						foreach ($list_land_owner as $list_owner)
						{
							$land_owner .= $list_owner['title_name'].$list_owner['person_fname'].' '.$list_owner['person_lname'].', ';
						}
					}

					if($land_owner!='') $land_owner = substr($land_owner, 0, -2);
				}
				else
				{
					$land_other .= $key_assess.', ';
				}

				$building_record = $this->building_record->getByLand($land_record['land_id']);

				if(!empty($building_record))
				{
					foreach ($building_record as $list_building)
					{
						$building_amount = floatval($list_building['building_record_staff_assess_amount']);
						$asset_sum += floatval($list_building['building_estimate_price']);

						$record_sum += $building_amount;
						$building_sum += $building_amount;

						$building_count++;
					}
				}
			}
		}

		if($land_main != '') $land_main = substr($land_main, 0, -2);
		if($land_other != '') $land_other = substr($land_other, 0, -2);

		$loan_income_1 = $this->loan_income->getByPerson($data['person_id'], '1');
		$loan_income_2 = $this->loan_income->getByPerson($data['person_id'], '2');
		$loan_income_3 = $this->loan_income->getByPerson($data['person_id'], '3');

		$income_all['rai'] = 0;
		$income_all['ngan'] = 0;
		$income_all['wah'] = 0;
		$income_self['rai'] = 0;
		$income_self['ngan'] = 0;
		$income_self['wah'] = 0;
		$income_renting['rai'] = 0;
		$income_renting['ngan'] = 0;
		$income_renting['wah'] = 0;
		$income_approve['rai'] = 0;
		$income_approve['ngan'] = 0;
		$income_approve['wah'] = 0;

		if(!empty($loan_income_1))
		{
			foreach ($loan_income_1 as $income1_value)
			{
				$self_rai = empty($income1_value['loan_activity_self_rai'])? 0 : intval($income1_value['loan_activity_self_rai']);
				$self_ngan = empty($income1_value['loan_activity_self_ngan'])? 0 : intval($income1_value['loan_activity_self_ngan']);
				$self_wah = empty($income1_value['loan_activity_self_wah'])? 0 : floatval($income1_value['loan_activity_self_wah']);
				$renting_rai = empty($income1_value['loan_activity_renting_rai'])? 0 : intval($income1_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income1_value['loan_activity_renting_ngan'])? 0 : intval($income1_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income1_value['loan_activity_renting_wah'])? 0 : floatval($income1_value['loan_activity_renting_wah']);
				$approve_rai = empty($income1_value['loan_activity_approve_rai'])? 0 : intval($income1_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income1_value['loan_activity_approve_ngan'])? 0 : intval($income1_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income1_value['loan_activity_approve_wah'])? 0 : floatval($income1_value['loan_activity_approve_wah']);

				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}

		if(!empty($loan_income_2))
		{
			foreach ($loan_income_2 as $income2_value)
			{
				$self_rai = empty($income2_value['loan_activity_self_rai'])? 0 : intval($income2_value['loan_activity_self_rai']);
				$self_ngan = empty($income2_value['loan_activity_self_ngan'])? 0 : intval($income2_value['loan_activity_self_ngan']);
				$self_wah = empty($income2_value['loan_activity_self_wah'])? 0 : floatval($income2_value['loan_activity_self_wah']);
				$renting_rai = empty($income2_value['loan_activity_renting_rai'])? 0 : intval($income2_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income2_value['loan_activity_renting_ngan'])? 0 : intval($income2_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income2_value['loan_activity_renting_wah'])? 0 : floatval($income2_value['loan_activity_renting_wah']);
				$approve_rai = empty($income2_value['loan_activity_approve_rai'])? 0 : intval($income2_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income2_value['loan_activity_approve_ngan'])? 0 : intval($income2_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income2_value['loan_activity_approve_wah'])? 0 : floatval($income2_value['loan_activity_approve_wah']);

				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}

		if(!empty($loan_income_3))
		{
			foreach ($loan_income_3 as $income3_value)
			{
				$self_rai = empty($income3_value['loan_activity_self_rai'])? 0 : intval($income3_value['loan_activity_self_rai']);
				$self_ngan = empty($income3_value['loan_activity_self_ngan'])? 0 : intval($income3_value['loan_activity_self_ngan']);
				$self_wah = empty($income3_value['loan_activity_self_wah'])? 0 : floatval($income3_value['loan_activity_self_wah']);
				$renting_rai = empty($income3_value['loan_activity_renting_rai'])? 0 : intval($income3_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income3_value['loan_activity_renting_ngan'])? 0 : intval($income3_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income3_value['loan_activity_renting_wah'])? 0 : floatval($income3_value['loan_activity_renting_wah']);
				$approve_rai = empty($income3_value['loan_activity_approve_rai'])? 0 : intval($income3_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income3_value['loan_activity_approve_ngan'])? 0 : intval($income3_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income3_value['loan_activity_approve_wah'])? 0 : floatval($income3_value['loan_activity_approve_wah']);

				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}

		$income_all = area($income_all['rai'], $income_all['ngan'], $income_all['wah']);
		$income_self = area($income_self['rai'], $income_self['ngan'], $income_self['wah']);
		$income_renting = area($income_renting['rai'], $income_renting['ngan'], $income_renting['wah']);
		$income_approve = area($income_approve['rai'], $income_approve['ngan'], $income_approve['wah']);

		$section->addText(' 	 	      ๑.๔ ผู้ขอสินเชื่อมีทรัพย์สินมูลค่ารวม '.num2Thai(number_format($asset_sum, 2)).' บาท และมีหนี้สินรวมจำนวน '
		.num2Thai(number_format($debt_sum, 2)).' บาท'.$debt_owner_text, 'font_normal', 'p_0');

		$land_all = (empty($income_all['rai']) && empty($income_all['ngan']) && empty($income_all['wah']))? ' - ' : num2Thai($income_all['rai'].' - '.$income_all['ngan'].' - '.$income_all['wah']);
		$land_self = (empty($income_self['rai']) && empty($income_self['ngan']) && empty($income_self['wah']))? ' - ' : num2Thai($income_self['rai'].' - '.$income_self['ngan'].' - '.$income_self['wah']);
		$land_renting = (empty($income_renting['rai']) && empty($income_renting['ngan']) && empty($income_renting['wah']))? ' - ' : num2Thai($income_renting['rai'].' - '.$income_renting['ngan'].' - '.$income_renting['wah']);
		$land_approve = (empty($income_approve['rai']) && empty($income_approve['ngan']) && empty($income_approve['wah']))? ' - ' : num2Thai($income_approve['rai'].' - '.$income_approve['ngan'].' - '.$income_approve['wah']);

		$section->addText(' 	 	      ๑.๕ ในปัจจุบันมีพื้นที่ทำการเกษตร เนื้อที่ '.$land_all.' ไร่ โดยเป็นกรรมสิทธิ์ของตนเอง จำนวน '.$land_self.' ไร่ เช่าที่ดินผู้อื่น จำนวน '.$land_renting.' ไร่ ได้รับอนุญาตให้ทำประโยชน์ในที่ดินผู้อื่น '.$land_approve.' ไร่', 'font_normal', 'p_0');
		$section->addText(' 	 	      ๑.๖ พฤติกรรมของผู้ขอสินเชื่อ', 'font_normal', 'p_0');
		$section->addText(' 	 	             ๑.๖.๑ ความประพฤติ '.($analysis['loan_analysis_behavior']=='1'? '(√)' : '( )').' ดี '.($analysis['loan_analysis_behavior']!='1'? '(√)' : '( )').' ไม่ดี (ระบุ) '.(empty($analysis['loan_analysis_behavior_detail'])? ' - ' : num2Thai($analysis['loan_analysis_behavior_detail'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๑.๖.๒ ความตั้งใจในการประกอบอาชีพ '.($analysis['loan_analysis_behavior_occupation']=='1'? '(√)' : '( )').' ดี '.($analysis['loan_analysis_behavior_occupation']!='1'? '(√)' : '( )').' ไม่ดี (ระบุ) '.(empty($analysis['loan_analysis_behavior_occupation_detail'])? ' - ' : num2Thai($analysis['loan_analysis_behavior_occupation_detail'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๑.๖.๓ ความมุ่งมั่นตั้งใจในการใช้ประโยชน์ที่ดินเพื่อเกษตรกรรม '.($analysis['loan_analysis_behavior_farm']=='1'? '(√)' : '( )').' มี '.($analysis['loan_analysis_behavior_farm']!='1'? '(√)' : '( )').' ไม่มี  (ระบุ) '.(empty($analysis['loan_analysis_behavior_farm_detail'])? ' - ' : num2Thai($analysis['loan_analysis_behavior_farm_detail'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๑.๖.๔ สุขภาพ '.($analysis['loan_analysis_behavior_health']=='1'? '(√)' : '( )').' แข็งแรง '.($analysis['loan_analysis_behavior_health']!='1'? '(√)' : '( )').' ไม่แข็งแรง (ระบุ) '.(empty($analysis['loan_analysis_behavior_health_detail'])? ' - ' : num2Thai($analysis['loan_analysis_behavior_health_detail'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๑.๖.๕ อื่นๆ (ระบุ) '.(empty($analysis['loan_analysis_behavior_other'])? ' - ' : num2Thai($analysis['loan_analysis_behavior_other'])), 'font_normal', 'p_0');



		$loan_type_objective1 = '';
		$loan_type_objective2 = '';
		$loan_type_objective3 = '';
		$loan_creditor = '';
		$loan_real_amount = 0;
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{

				if($list_type['loan_objective_id']=='1')
				{
					$loan_type_objective1 = '(√)';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_redeem_amount']);

					//$creditor = $this->loan_lib->person_fields();
					// if(!empty($list_type['loan_type_redeem_owner']))
					// {
					// 	$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
					// 	$creditor_title = $this->title->getById($creditor['title_id']);
					// 	$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					// }
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(√)';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_contract_amount']);

					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(√)';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_case_amount']);

					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '(√)';

					$loan_real_amount += floatval($list_type['loan_type_sale_amount']);
				}
			}
		}

		$section->addText(' 	 	 ๒. จำนวนเงินที่ขอสินเชื่อ โดยมีวัตถุประสงค์เพื่อ	'.$loan_type_objective1.' ไถ่ถอนที่ดินจากการจำนองหรือขายฝาก '.$loan_type_objective2.' ชำระหนี้ '.$loan_type_objective3.' ซื้อคืนที่ดิน จำนวน '.num2Thai(number_format($data['loan_amount'], 2)).' บาท และเพื่อประกอบอาชีพเกษตรกรรม จำนวน - บาท', 'font_normal', 'p_0');
		$section->addText(' 	 	 ๓. มูลเหตุแห่งหนี้', 'font_normal', 'p_0');

		$loan_doc =  $this->loan_doc->getByLoan($id);

		$html_doc = '';
		$count_doc = 0;
		if(!empty($loan_doc))
		{
			foreach ($loan_doc as $list_doc)
			{
				if($list_doc['loan_doc_type_id']=='4' || $list_doc['loan_doc_type_id']=='5' || $list_doc['loan_doc_type_id']=='6' || $list_doc['loan_doc_type_id']=='7' || $list_doc['loan_doc_type_id']=='8' || $list_doc['loan_doc_type_id']=='9' || $list_doc['loan_doc_type_id']=='10' || $list_doc['loan_doc_type_id']=='12')
				{
					$count_doc++;
					$html_doc .= $list_doc['loan_doc_type_name'].(empty($list_doc['loan_doc_date'])? '' : ' ลงวันที่ '.thai_display_date($list_doc['loan_doc_date'])).', ';
				}
			}

			if($html_doc!='') $html_doc = num2Thai(substr($html_doc, 0, -2));
		}
		else
		{
			$html_doc = ' - ';
		}
		$section->addText(' 	 	      ๓.๑ ชื่อ – สกุลของเจ้าหนี้ หรือเจ้าหนี้ตามคำพิพากษา '.(empty($loan_creditor)? ' - ' : $loan_creditor).' จำนวนเงินที่กู้ยืม หรือขายฝาก และได้รับจริง '.num2Thai(number_format($loan_real_amount, 2)).' บาท *ตามเอกสารการเป็นหนี้ (ระบุ) '.$html_doc.' และจำนวนหนี้ตามเอกสาร '.num2Thai(number_format($loan_real_amount, 2)).' บาท กำหนดเวลาการชำระหนี้ ภายในวันที่ '.(empty($analysis['loan_analysis_deadline_full'])?' - ':num2Thai($analysis['loan_analysis_deadline_full'])).' ผลการไกล่เกลี่ยประนีประนอม มีจำนวนหนี้คงเหลือต้องชำระ '.(empty($analysis['loan_analysis_remain'])? ' - ' : num2Thai(number_format($analysis['loan_analysis_remain'], 2))).' บาท ภายในวันที่ '.(empty($analysis['loan_analysis_within_full'])? ' - ' : num2Thai($analysis['loan_analysis_within_full'])).' สาเหตุที่เป็นหนี้ (ให้อธิบายโดยละเอียดว่า เกิดจากสาเหตุใด เมื่อใด และผลเป็นประการใด) '.(empty($analysis['loan_analysis_cause'])? ' - ' : num2Thai($analysis['loan_analysis_cause'])), 'font_normal', 'p_0');
		//$section->addText(' 	 	      ๓.๑ ชื่อ – สกุลของเจ้าหนี้ หรือเจ้าหนี้ตามคำพิพากษา '.(empty($loan_creditor)? ' - ' : $loan_creditor).' จำนวนเงินที่กู้ยืม หรือขายฝาก และได้รับจริง '.num2Thai(number_format($loan_real_amount, 2)).
		//' บาท *ตามเอกสารการเป็นหนี้ (ระบุ) '.$html_doc.' และจำนวนหนี้ตามเอกสาร xxx บาท กำหนดเวลาการชำระหนี้ ภายในวันที่ '.(empty($analysis['loan_analysis_deadline_full'])? ' - ' : num2Thai($analysis['loan_analysis_deadline_full'])).
		//' ผลการไกล่เกลี่ยประนีประนอม มีจำนวนหนี้คงเหลือต้องชำระ '.(empty($analysis['loan_analysis_remain'])? ' - ' : num2Thai(number_format($analysis['loan_analysis_remain'], 2))).' บาท ภายในวันที่ '.(empty($analysis['loan_analysis_within_full'])? ' - ' : num2Thai($analysis['loan_analysis_within_full'])).' สาเหตุที่เป็นหนี้ (ให้อธิบายโดยละเอียดว่า เกิดจากสาเหตุใด เมื่อใด และผลเป็นประการใด) '.(empty($analysis['loan_analysis_cause'])? ' - ' : num2Thai($analysis['loan_analysis_cause'])), 'font_normal', 'p_0');
		$section->addText(' 	 	      ๓.๒ ผู้ขอสินเชื่อและคู่สมรส (ถ้ามี) มีหนี้สินรายอื่น ได้แก่ (เช่น ธ.ก.ส. สหกรณ์ สถาบันการเงิน หรือบุคคล เป็นต้น) '.$debt_text.' รวมเป็นจำนวน '.num2Thai(number_format($debt_sum, 2)).' บาท', 'font_normal', 'p_0');

		$section->addText(' 	 	 ๔. หลักประกันคำขอสินเชื่อ (รายละเอียดตามแบบบันทึกการตรวจสอบที่ดิน)', 'font_normal', 'p_0');
		$section->addText(' 	 	      ๔.๑ การจำนองที่ดิน', 'font_normal', 'p_0');

		$styleTable = array('borderSize'=>  6, 'borderColor'=> '000000', 'cellMargin'=> 80);
		$PHPWord->addTableStyle('myOwnTableStyle', $styleTable);

		$table_assess = $section->addTable('myOwnTableStyle');

		$table_assess->addRow();
		$table_assess->addCell(750)->addText('แปลงที่', 'font_normal', 'p_center');
		$table_assess->addCell(2250)->addText('เนื้อที่ (ไร่)', 'font_normal', 'p_center');
		$table_assess->addCell(2250)->addText('ราคาจดทะเบียนสิทธิ และนิติกรรม ต่อไร่', 'font_normal', 'p_center');
		$table_assess->addCell(2250)->addText('ราคาซื้อขายเฉลี่ย ในปัจจุบันต่อไร่', 'font_normal', 'p_center');
		$table_assess->addCell(2250)->addText('ราคาประเมินต่อไร่', 'font_normal', 'p_center');
		$table_assess->addCell(2250)->addText('ราคาประเมิน ทั้งแปลง', 'font_normal', 'p_center');
		$table_assess->addCell(3000)->addText('ราคาประเมินเป็นกี่เท่า ของจำนวนเงินกู้', 'font_normal', 'p_center');

		$sum_assess = 0;
		if(!empty($list_land_record))
		{
			foreach ($list_land_record as $key_land => $land_record)
			{
				$land_assess = $this->land_assess->getByLand($land_record['land_id']);

				if(!empty($land_assess))
				{
					foreach ($land_assess as $list_assess)
					{
						$cal_assess = floatval($list_assess['land_assess_inspector_sum']);
						$sum_assess += floatval($cal_assess);

						$key_assess = array_search($list_assess['land_id'], $list_land);

						if(!$key_assess)
						{
							$key_assess = $i_land;
							$list_land[$i_land++] = $list_assess['land_id'];
						}

						$table_assess->addRow();
						$table_assess->addCell(750)->addText(num2Thai($key_assess), 'font_normal', 'p_center');
						$table_assess->addCell(2250)->addText(num2Thai($list_assess['land_assess_area_rai'].' - '.$list_assess['land_assess_area_ngan'].' - '.$list_assess['land_assess_area_wah']), 'font_normal', 'p_center');
						$table_assess->addCell(2250)->addText(num2Thai(number_format($list_assess['land_assess_price'], 2)), 'font_normal', 'p_right');
						$table_assess->addCell(2250)->addText(num2Thai(number_format(($list_assess['land_assess_price_pre_min'] + $list_assess['land_assess_price_pre_max']) / 2, 2)), 'font_normal', 'p_right');
						$table_assess->addCell(2250)->addText(num2Thai(number_format($list_assess['land_assess_price_basic'], 2)), 'font_normal', 'p_right');
						$table_assess->addCell(2250)->addText(num2Thai(number_format($cal_assess, 2)), 'font_normal', 'p_right');
						$table_assess->addCell(3000)->addText('-', 'font_normal', 'p_right');
					}
				}
			}
		}

		$sum_record = $sum_assess + $building_sum;

		$table_assess->addRow();
		$table_assess->addCell(750);
		$table_assess->addCell(2250);
		$table_assess->addCell(2250);
		$table_assess->addCell(2250);
		$table_assess->addCell(2250)->addText('รวมทั้งสิ้น', 'font_normal', 'p_right');
		$table_assess->addCell(2250)->addText(num2Thai(number_format($sum_assess, 2)), 'font_normal', 'p_right');
		$table_assess->addCell(3000)->addText(num2Thai(numberFormatPrecision($sum_assess  / $data['loan_amount'],2)), 'font_normal', 'p_right');

		$section->addText(' 	 	            มีสิ่งปลูกสร้าง จำนวน '.($building_count==0? ' - ' : num2Thai($building_count)).' หลัง ราคาประเมิน '.($building_sum==0? ' - ' : num2Thai(number_format($building_sum,  2))).'  บาท', 'font_normal', 'p_0');
		$section->addText(' 	 	            รวมราคาประเมินที่ดินและสิ่งปลูกสร้าง จำนวน '.num2Thai(number_format($sum_record, 2)).' บาท   ราคาประเมินเป็นกี่เท่าของจำนวนเงินกู้ '.num2Thai(numberFormatPrecision($sum_record / $data['loan_amount'],2)), 'font_normal', 'p_0');
		$section->addText(' 	 	            หมายเหตุ : การจำนองที่ดินซึ่งไถ่ถอนจากเจ้าหนี้ ได้แก่ แปลงที่ '.num2Thai($land_main).' ชื่อ – สกุลของเจ้าของที่ดิน '.$land_owner.' และจำนองที่ดินเพิ่มเติม ได้แก่ แปลงที่ '.(empty($land_other)? ' - ' : num2Thai($land_other)), 'font_normal', 'p_0');
		$section->addImage('assets\icon\line2.png', array('width'=>600, 'height'=>1, 'align'=>'left'));
		$section->addText('* เอกสารการเป็นหนี้ ให้ระบุ เช่น ขายฝาก จำนอง สัญญากู้ยืม สัญญาซื้อคืน หรืออื่นๆ เป็นต้น', 'font_comment', 'p_0');

		$loan_guarantee_bond =  $this->loan_guarantee_bond->getByLoan($id);

		$check_guarantee_bond = '( )';
		$sum_guarantee_bond = 0;
		if(!empty($loan_guarantee_bond))
		{
			$section->addText(' 	 	      ๔.๒ พันธบัตรรัฐบาล (ระบุชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์ของผู้จำนำซึ่งเป็นผู้มีกรรมสิทธิ์ รวมทั้งชนิดพันธบัตร เลขที่ต้น เลขที่ท้าย หน้าทะเบียน ลงวันที่ จำนวนเงินตามพันธบัตรที่จำนำ)', 'font_normal', 'p_0');
			$check_guarantee = '(√)';
			$check_guarantee_bond = '(√)';

			$i_bond = 1;
			foreach ($loan_guarantee_bond as $list_bond)
			{
				$sum_guarantee_bond += floatval($list_bond['loan_bond_amount']);

				$section->addText(' 	 	             ('.num2Thai($i_bond++).') ชื่อ – สกุล '.$list_bond['loan_bond_owner'].' เป็นผู้มีกรรมสิทธ์ ชนิดพันธบัตร '.num2Thai($list_bond['loan_bond_type']).' เลขที่ต้น '.num2Thai($list_bond['loan_bond_startnumber']).' เลขที่ท้าย '.num2Thai($list_bond['loan_bond_endnumber']).' จำนวนเงิน '.num2Thai(number_format($list_bond['loan_bond_amount'], 2)).' บาท วันที่ '.num2Thai(thai_display_date($list_bond['loan_bond_regis_date'])), 'font_normal', 'p_0');
			}

			$guarantee .= 'พันธบัตรรัฐบาล จำนวนเงิน '.number_format($sum_guarantee_bond, 2).' บาท, ';
 		}
 		else
 		{
 			$section->addText(' 	 	      ๔.๒ พันธบัตรรัฐบาล (ระบุชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์ของผู้จำนำซึ่งเป็นผู้มีกรรมสิทธิ์ รวมทั้งชนิดพันธบัตร เลขที่ต้น เลขที่ท้าย หน้าทะเบียน ลงวันที่ จำนวนเงินตามพันธบัตรที่จำนำ) - ', 'font_normal', 'p_0');
 		}

		$loan_guarantee_bookbank =  $this->loan_guarantee_bookbank->getByLoan($id);

		$loan_bookbank_type = array(
				'1' => 'ออมทรัพย์',
				'2' => 'ฝากประจำ',
				'3' => 'เผื่อเรียก'
		);

		$check_guarantee_bookbank = '( )';
		$sum_guarantee_bookbank = 0;
		if(!empty($loan_guarantee_bookbank))
		{
			$section->addText(' 	 	      ๔.๓ เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ (ระบุ ชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์ของเจ้าของบัญชีเงินฝาก รวมทั้งบัญชีเงินฝากของธนาคาร สถาบันการเงิน หรือสหกรณ์ ประเภทของเงินฝาก เลขที่บัญชีเงินฝาก ยอดเงินคงเหลือจำนวนเท่าใด และเมื่อใด)', 'font_normal', 'p_0');

			$check_guarantee = '(√)';
			$check_guarantee_bookbank = '(√)';

			$i_bookbank = 1;
			foreach ($loan_guarantee_bookbank as $list_bookbank)
			{
				$sum_guarantee_bookbank += floatval($list_bookbank['loan_bookbank_amount']);

				$section->addText(' 	 	             ('.num2Thai($i_bookbank++).') ชื่อ – สกุล '.$list_bookbank['loan_bookbank_owner'].' เป็นเจ้าของบัญชีเงินฝาก '.num2Thai($list_bookbank['bank_name']).' สาขา '.num2Thai($list_bookbank['loan_bookbank_branch']).' เลขที่บัญชี '.num2Thai($list_bookbank['loan_bookbank_no']).' ประเภทบัญชี '.num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]).' ยอดเงินฝาก ณ วันที่ '.num2Thai(thai_display_date($list_bookbank['loan_bookbank_date'])).' จำนวนเงิน '.num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)).' บาท', 'font_normal', 'p_0');
			}

			$guarantee .= 'เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์  จำนวนเงิน '.number_format($sum_guarantee_bookbank, 2).' บาท, ';
		}
		else
		{
			$section->addText(' 	 	      ๔.๓ เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ (ระบุ ชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์ของเจ้าของบัญชีเงินฝาก รวมทั้งบัญชีเงินฝากของธนาคาร สถาบันการเงิน หรือสหกรณ์ ประเภทของเงินฝาก เลขที่บัญชีเงินฝาก ยอดเงินคงเหลือจำนวนเท่าใด และเมื่อใด) - ', 'font_normal', 'p_0');
		}

		$loan_guarantee_bondsman =  $this->loan_guarantee_bondsman->getByLoan($id);

		$check_guarantee_bondsman = '( )';
		$html_guarantee_bondsman = '';
		if(!empty($loan_guarantee_bondsman))
		{
			$section->addText(' 	 	      ๔.๔ การค้ำประกันด้วยบุคคล (ระบุชื่อ - สกุล อายุ ที่อยู่ หมายเลขโทรศัพท์ อาชีพ ตำแหน่ง สังกัด อัตราเงินเดือน/ค่าจ้างเดือนละเท่าใด และความน่าเชื่อถือ พร้อมทั้งแนบสำเนาบัตรประชาชน ทะเบียนบ้าน หนังสือรับรองจากต้นสังกัด หรือหนังสือแสดงรายได้ โดยระบุตำแหน่ง ระดับ อัตราเงินเดือน และหากผู้ค้ำประกันมีการทำสัญญาค้ำประกันรายอื่นด้วย ให้แจ้งภาระผูกพันดังกล่าวด้วย)', 'font_normal', 'p_0');

			$check_guarantee = '(√)';
			$check_guarantee_bondsman = '(√)';

			$i_bondsman = 1;
			foreach ($loan_guarantee_bondsman as $list_bondsman)
			{
				$age_bondsman = age($list_bondsman['person_birthdate']);
				$html_guarantee_bondsman .= $list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'].', ';

				$section->addText(' 	 	             ๔.๔.'.num2Thai($i_bondsman++).' ชื่อ – สกุล '.$list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'].' อายุ '.num2Thai($age_bondsman).' ปี หมายเลขโทรศัพท์ '.(empty($list_bondsman['person_mobile'])? ' - ' : num2Thai($list_bondsman['person_mobile'])).' ที่อยู่ บ้านเลขที่ '.num2Thai($list_bondsman['person_addr_pre_no']).' หมู่ที่ '.(empty($list_bondsman['person_addr_pre_moo'])? '-' : num2Thai($list_bondsman['person_addr_pre_moo'])).' ถนน '.(empty($list_bondsman['person_addr_pre_road'])? '-' : num2Thai($list_bondsman['person_addr_pre_road'])).' ตำบล '.$list_bondsman['district_name'].' อำเภอ '.$list_bondsman['amphur_name'].' จังหวัด '.$list_bondsman['province_name'].' อาชีพ '.num2Thai($list_bondsman['career_name']).' ตำแหน่ง '.(empty($list_bondsman['person_position'])? ' - ' : num2Thai($list_bondsman['person_position'])).' สังกัด '.(empty($list_bondsman['person_belong'])? ' - ' : num2Thai($list_bondsman['person_belong'])).' กระทรวง '.(empty($list_bondsman['person_ministry'])? ' - ' : num2Thai($list_bondsman['person_ministry'])).' อัตราเงินเดือน/ค่าจ้างเดือนละ '.num2Thai(number_format($list_bondsman['person_income_per_month'], 2)).' บาท ภาระผูกพันที่ต้องชำระหนี้เงินกู้อื่น '.num2Thai(number_format($list_bondsman['person_loan_other_per_year'], 2)).' บาท ภาระผูกพันในการค้ำประกันหนี้เงินกู้อื่น '.num2Thai(number_format($list_bondsman['person_guarantee_other_amount'], 2)).' บาท', 'font_normal', 'p_0');
			}

			if($html_guarantee_bondsman!='') $html_guarantee_bondsman = substr($html_guarantee_bondsman, 0, -2);

			$guarantee .= 'การค้ำประกันด้วยบุคคล ชื่อ '.$html_guarantee_bondsman.', ';
		}
		else
		{
			$section->addText(' 	 	      ๔.๔ การค้ำประกันด้วยบุคคล (ระบุชื่อ - สกุล อายุ ที่อยู่ หมายเลขโทรศัพท์ อาชีพ ตำแหน่ง สังกัด อัตราเงินเดือน/ค่าจ้างเดือนละเท่าใด และความน่าเชื่อถือ พร้อมทั้งแนบสำเนาบัตรประชาชน ทะเบียนบ้าน หนังสือรับรองจากต้นสังกัด หรือหนังสือแสดงรายได้ โดยระบุตำแหน่ง ระดับ อัตราเงินเดือน และหากผู้ค้ำประกันมีการทำสัญญาค้ำประกันรายอื่นด้วย ให้แจ้งภาระผูกพันดังกล่าวด้วย) - ', 'font_normal', 'p_0');
		}

		if($guarantee!='') $guarantee = substr($guarantee, 0, -2);

		$section->addText(' 	 	 ๕. รายได้ ค่าใช้จ่าย และแผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ', 'font_normal', 'p_0');
		$section->addText(' 	 	      ๕.๑ รายได้ และค่าใช้จ่ายของผู้กู้ (รายละเอียดตามข้อมูลของผู้ขอสินเชื่อรายที่แนบ)', 'font_normal', 'p_0');

		$ex_income_1 = $this->loan_income->getByPersonGroup($person, '1');
		$ex_income_2 = $this->loan_income->getByPersonGroup($person, '2');
		$ex_income_3 = $this->loan_income->getByPersonGroup($person, '3');
		$ex_expenditure_1 = $this->loan_expenditure->getByPersonGroup($person, '1');
		$ex_expenditure_2 = $this->loan_expenditure->getByPersonGroup($person, '2');
		$ex_expenditure_3 = $this->loan_expenditure->getByPersonGroup($person, '3');

		$count_income_1 = count($ex_income_1);
		$count_income_2 = count($ex_income_2);
		$count_income_3 = count($ex_income_3);
		$count_expenditure_1 = count($ex_expenditure_1);
		$count_expenditure_2 = count($ex_expenditure_2);
		$count_expenditure_3 = count($ex_expenditure_3);
		$count_expenditure_all = $count_expenditure_1 > $count_expenditure_2? $count_expenditure_1 : $count_expenditure_2;
		if($count_expenditure_all < $count_expenditure_3) $count_expenditure_all = $count_expenditure_3;

		$cal_all = array();
		$sum_all = array(
				'year1' => 0,
				'year2' => 0,
				'year3' => 0,
		);

		$td_len = $count_income_1>=$count_income_2? $count_income_1 : $count_income_2;
		if($td_len<$count_income_3) $td_len =  $count_income_3;
		if($td_len<$count_expenditure_1) $td_len =  $count_expenditure_1;
		if($td_len<$count_expenditure_2) $td_len =  $count_expenditure_2;
		if($td_len<$count_expenditure_3) $td_len =  $count_expenditure_3;

		$td_len++;

		/* echo '$td_len :: '.$td_len.'<br/>';
		echo '$count_income_1 :: '.$count_income_1.'<br/>';
		echo '$count_income_2 :: '.$count_income_2.'<br/>';
		echo '$count_income_3 :: '.$count_income_3.'<br/>';
		echo '$count_expenditure_1 :: '.$count_expenditure_1.'<br/>';
		echo '$count_expenditure_2 :: '.$count_expenditure_2.'<br/>';
		echo '$count_expenditure_3 :: '.$count_expenditure_3.'<br/>'; */

		$td_width = ceil(12000 / ($td_len * 3)); //3000
		$td_remain = 15000 - ($td_width * ($td_len * 3));

		$td_income_1 = array();
		$td_income_2 = array();
		$td_income_3 = array();
		$td_expenditure = array();
		for($i_len = 1; $i_len <= $td_len; $i_len++)
		{
			if($i_len==$td_len)
			{
				$td_income_1[] = 'รวม';
				$td_income_2[] = 'xx';
				$td_income_3[] = 'xx';
				$td_expenditure[] = 'xx';
			}
			else
			{
				$td_income_1[] = 'xx';
				$td_income_2[] = 'xx';
				$td_income_3[] = 'xx';
				$td_expenditure[] = 'xx';
			}
		}

		$table_expenditure = $section->addTable('myOwnTableStyle');

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('รายการ', 'font_normal', 'p_center');
		$table_expenditure->addCell($td_width)->addText('ปีที่ ๑ (ปีปัจจุบัน)', 'font_normal', 'p_center');

		for ($i_len = 1; $i_len<$td_len; $i_len++)
		{
			$table_expenditure->addCell($td_width);
		}

		$table_expenditure->addCell($td_width)->addText('ปีที่ ๒ (ปีก่อน)', 'font_normal', 'p_center');

		for ($i_len = 1; $i_len<$td_len; $i_len++)
		{
			$table_expenditure->addCell($td_width);
		}

		$table_expenditure->addCell($td_width)->addText('ปีที่ ๓ (ปีหลัง)', 'font_normal', 'p_center');

		for ($i_len = 1; $i_len<$td_len; $i_len++)
		{
			$table_expenditure->addCell($td_width);
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('๕.๑.๑ รายได้และค่าใช้จ่ายการเกษตร', 'font_normal', 'p_0');

		for ($i_income1 = 1; $i_income1 <= 3; $i_income1++)
		{
			foreach ($td_income_1 as $list_income_1)
			{
				$table_expenditure->addCell($td_width)->addText($list_income_1, 'font_normal', 'p_0');
			}
		}

		if(!empty($ex_income_1))
		{
			$sum_year1_income1 = 0;
			$sum_year2_income1 = 0;
			$sum_year3_income1 = 0;
			$sum_year1_expenditure1 = 0;
			$sum_year2_expenditure1 = 0;
			$sum_year3_expenditure1 = 0;
			$sum_year1_sum1 = 0;
			$sum_year2_sum1 = 0;
			$sum_year3_sum1 = 0;
			$td_income1_year1_1 = array();
			$td_income1_year2_1 = array();
			$td_income1_year3_1 = array();
			$td_income1_year1_2 = array();
			$td_income1_year2_2 = array();
			$td_income1_year3_2 = array();
			$td_income1_year1_3 = array();
			$td_income1_year2_3 = array();
			$td_income1_year3_3 = array();
			$td_income1_year1_4 = array();
			$td_income1_year2_4 = array();
			$td_income1_year3_4 = array();
			$td_income1_year1_5 = array();
			$td_income1_year2_5 = array();
			$td_income1_year3_5 = array();
			$td_income1_year1_6 = array();
			$td_income1_year2_6 = array();
			$td_income1_year3_6 = array();

			$i_income_1 = 0;
			foreach ($ex_income_1 as $list_income_1)
			{
				$year1_income = floatval($list_income_1['loan_activity_income1']);
				$year2_income = floatval($list_income_1['loan_activity_income2']);
				$year3_income = floatval($list_income_1['loan_activity_income3']);

				$sum_year1_income1 += $year1_income;
				$sum_year2_income1 += $year2_income;
				$sum_year3_income1 += $year3_income;

				$year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
				$year2_expenditure = floatval($list_income_1['loan_activity_expenditure2']);
				$year3_expenditure = floatval($list_income_1['loan_activity_expenditure3']);

				$sum_year1_expenditure1 += $year1_expenditure;
				$sum_year2_expenditure1 += $year2_expenditure;
				$sum_year3_expenditure1 += $year3_expenditure;

				$year1_sum = $year1_income - $year1_expenditure;
				$year2_sum = $year2_income - $year2_expenditure;
				$year3_sum = $year3_income - $year3_expenditure;

				$sum_year1_sum1 += $year1_sum;
				$sum_year2_sum1 += $year2_sum;
				$sum_year3_sum1 += $year3_sum;

				$rai1 = intval($list_income_1['loan_activity_self_rai']) + intval($list_income_1['loan_activity_renting_rai']) + intval($list_income_1['loan_activity_approve_rai']);
				$ngan1 = intval($list_income_1['loan_activity_self_ngan']) + intval($list_income_1['loan_activity_renting_ngan']) + intval($list_income_1['loan_activity_approve_ngan']);
				$wah1 = floatval($list_income_1['loan_activity_self_wah']) + floatval($list_income_1['loan_activity_renting_wah']) + floatval($list_income_1['loan_activity_approve_wah']);

				$area1 = area($rai1, $ngan1, $wah1);

				$td_income1_year1_1[] = num2Thai($list_income_1['loan_activity_name']);
				$td_income1_year2_1[] = num2Thai($list_income_1['loan_activity_name']);
				$td_income1_year3_1[] = num2Thai($list_income_1['loan_activity_name']);
				$td_income1_year1_2[] = '๑ ปี';
				$td_income1_year2_2[] = '๑ ปี';
				$td_income1_year3_2[] = '๑ ปี';
				$td_income1_year1_3[] = num2Thai($area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah']).' ไร่';
				$td_income1_year2_3[] = num2Thai($area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah']).' ไร่';
				$td_income1_year3_3[] = num2Thai($area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah']).' ไร่';
				$td_income1_year1_4[] = num2Thai(number_format($year1_income, 2));
				$td_income1_year2_4[] = num2Thai(number_format($year2_income, 2));
				$td_income1_year3_4[] = num2Thai(number_format($year3_income, 2));
				$td_income1_year1_5[] = num2Thai(number_format($year1_expenditure, 2));
				$td_income1_year2_5[] = num2Thai(number_format($year2_expenditure, 2));
				$td_income1_year3_5[] = num2Thai(number_format($year3_expenditure, 2));
				$td_income1_year1_6[] = num2Thai(number_format($year1_sum, 2));
				$td_income1_year2_6[] = num2Thai(number_format($year2_sum, 2));
				$td_income1_year3_6[] = num2Thai(number_format($year3_sum, 2));

				$cal_all[$i_income_1++] = array(
						'year1' => $year1_sum,
						'year2' => $year2_sum,
						'year3' => $year3_sum
				);
			}

			$td_null_income1 = array();
			if($count_income_1 < $td_len - 1)
			{
				$count_null = $td_len - 1 - $count_income_1;

				for ($i_null = 1; $i_null<=$count_null; $i_null++)
				{
					$td_null_income1[] = 'xx';
					$cal_all[$i_income_1++] = array(
							'year1' => 0,
							'year2' => 0,
							'year3' => 0
						);
				}
			}

			$sum_all['year1'] += $sum_year1_sum1;
			$sum_all['year2'] += $sum_year2_sum1;
			$sum_all['year3'] += $sum_year3_sum1;

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๑) ประเภทการเกษตร', 'font_normal', 'p_0');

			foreach ($td_income1_year1_1 as $income1_year1_1)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year1_1, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income1_year2_1 as $income1_year2_1)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year2_1, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income1_year3_1 as $income1_year3_1)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year3_1, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๒) ระยะเวลาการผลิต (เดือน/ปี)', 'font_normal', 'p_0');

			foreach ($td_income1_year1_2 as $income1_year1_2)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year1_2, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');


			foreach ($td_income1_year2_2 as $income1_year2_2)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year2_2, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income1_year3_2 as $income1_year3_2)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year3_2, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๓) เนื้อที่ จำนวนการผลิต', 'font_normal', 'p_0');

			foreach ($td_income1_year1_3 as $income1_year1_3)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year1_3, 'font_normal', 'p_center');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income1_year2_3 as $income1_year2_3)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year2_3, 'font_normal', 'p_center');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income1_year3_3 as $income1_year3_3)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year3_3, 'font_normal', 'p_center');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๔) รายได้ก่อนหักค่าใช้จ่าย (บาท)', 'font_normal', 'p_0');

			foreach ($td_income1_year1_4 as $income1_year1_4)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year1_4, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_income1, 2)), 'font_normal', 'p_right');

			foreach ($td_income1_year2_4 as $income1_year2_4)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year2_4, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_income1, 2)), 'font_normal', 'p_right');

			foreach ($td_income1_year3_4 as $income1_yea3_4)
			{
				$table_expenditure->addCell($td_width)->addText($income1_yea3_4, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_income1, 2)), 'font_normal', 'p_right');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๕) ค่าใช้จ่ายการเกษตร (บาท)', 'font_normal', 'p_0');

			foreach ($td_income1_year1_5 as $income1_year1_5)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year1_5, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_expenditure1, 2)), 'font_normal', 'p_right');

			foreach ($td_income1_year2_5 as $income1_year2_5)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year2_5, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_expenditure1, 2)), 'font_normal', 'p_right');

			foreach ($td_income1_year3_5 as $income1_year3_5)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year3_5, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_expenditure1, 2)), 'font_normal', 'p_right');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๖) รายได้จากการเกษตรสุทธิ (๔) - (๕)', 'font_normal', 'p_0');

			foreach ($td_income1_year1_6 as $income1_year1_6)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year1_6, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_sum1, 2)), 'font_normal', 'p_right');

			foreach ($td_income1_year2_6 as $income1_year2_6)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year2_6, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_sum1, 2)), 'font_normal', 'p_right');

			foreach ($td_income1_year3_6 as $income1_year3_6)
			{
				$table_expenditure->addCell($td_width)->addText($income1_year3_6, 'font_normal', 'p_0');
			}

			foreach ($td_null_income1 as $null_income1)
			{
				$table_expenditure->addCell($td_width)->addText($null_income1, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_sum1, 2)), 'font_normal', 'p_right');
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('๕.๑.๒ รายได้และค่าใช้จ่ายนอกการเกษตร', 'font_normal', 'p_0');

		for ($i_income2 = 1; $i_income2 <= 3; $i_income2++)
		{
			foreach ($td_income_2 as $list_income_2)
			{
				$table_expenditure->addCell($td_width)->addText($list_income_2, 'font_normal', 'p_0');
			}
		}

	if(!empty($ex_income_2))
	{
		$sum_year1_income2 = 0;
		$sum_year2_income2 = 0;
		$sum_year3_income2 = 0;
		$sum_year1_expenditure2 = 0;
		$sum_year2_expenditure2 = 0;
		$sum_year3_expenditure2 = 0;
		$sum_year1_sum2 = 0;
		$sum_year2_sum2 = 0;
		$sum_year3_sum2 = 0;
		$td_income2_year1_1 = array();
		$td_income2_year2_1 = array();
		$td_income2_year3_1 = array();
		$td_income2_year1_2 = array();
		$td_income2_year2_2 = array();
		$td_income2_year3_2 = array();
		$td_income2_year1_3 = array();
		$td_income2_year2_3 = array();
		$td_income2_year3_3 = array();
		$td_income2_year1_4 = array();
		$td_income2_year2_4 = array();
		$td_income2_year3_4 = array();
		$td_income2_year1_5 = array();
		$td_income2_year2_5 = array();
		$td_income2_year3_5 = array();
		$td_income2_year1_6 = array();
		$td_income2_year2_6 = array();
		$td_income2_year3_6 = array();

		$i_income_2 = 0;
		foreach ($ex_income_2 as $list_income_2)
		{
			$year1_income = floatval($list_income_2['loan_activity_income1']);
			$year2_income = floatval($list_income_2['loan_activity_income2']);
			$year3_income = floatval($list_income_2['loan_activity_income3']);

			$sum_year1_income2 += $year1_income;
			$sum_year2_income2 += $year2_income;
			$sum_year3_income2 += $year3_income;

			$year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_2['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_2['loan_activity_expenditure3']);

			$sum_year1_expenditure2 += $year1_expenditure;
			$sum_year2_expenditure2 += $year2_expenditure;
			$sum_year3_expenditure2 += $year3_expenditure;

			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;

			$sum_year1_sum2 += $year1_sum;
			$sum_year2_sum2 += $year2_sum;
			$sum_year3_sum2 += $year3_sum;

			$rai2 = intval($list_income_2['loan_activity_self_rai']) + intval($list_income_2['loan_activity_renting_rai']) + intval($list_income_2['loan_activity_approve_rai']);
			$ngan2 = intval($list_income_2['loan_activity_self_ngan']) + intval($list_income_2['loan_activity_renting_ngan']) + intval($list_income_2['loan_activity_approve_ngan']);
			$wah2 = floatval($list_income_2['loan_activity_self_wah']) + floatval($list_income_2['loan_activity_renting_wah']) + floatval($list_income_2['loan_activity_approve_wah']);

			$area2 = area($rai2, $ngan2, $wah2);

			$td_income2_year1_1[] = num2Thai($list_income_2['loan_activity_name']);
			$td_income2_year2_1[] = num2Thai($list_income_2['loan_activity_name']);
			$td_income2_year3_1[] = num2Thai($list_income_2['loan_activity_name']);
			$td_income2_year1_2[] = '๑ ปี';
			$td_income2_year2_2[] = '๑ ปี';
			$td_income2_year3_2[] = '๑ ปี';
			$td_income2_year1_3[] = num2Thai($area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah']).' ไร่';
			$td_income2_year2_3[] = num2Thai($area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah']).' ไร่';
			$td_income2_year3_3[] = num2Thai($area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah']).' ไร่';
			$td_income2_year1_4[] = num2Thai(number_format($year1_income, 2));
			$td_income2_year2_4[] = num2Thai(number_format($year2_income, 2));
			$td_income2_year3_4[] = num2Thai(number_format($year3_income, 2));
			$td_income2_year1_5[] = num2Thai(number_format($year1_expenditure, 2));
			$td_income2_year2_5[] = num2Thai(number_format($year2_expenditure, 2));
			$td_income2_year3_5[] = num2Thai(number_format($year3_expenditure, 2));
			$td_income2_year1_6[] = num2Thai(number_format($year1_sum, 2));
			$td_income2_year2_6[] = num2Thai(number_format($year2_sum, 2));
			$td_income2_year3_6[] = num2Thai(number_format($year3_sum, 2));

			$cal_all[$i_income_2]['year1'] = empty($cal_all[$i_income_2]['year1'])? $year1_sum : $cal_all[$i_income_2]['year1'] + $year1_sum;
			$cal_all[$i_income_2]['year2'] = empty($cal_all[$i_income_2]['year2'])? $year2_sum : $cal_all[$i_income_2]['year2'] + $year2_sum;
			$cal_all[$i_income_2]['year3'] = empty($cal_all[$i_income_2]['year3'])? $year3_sum : $cal_all[$i_income_2]['year3'] + $year3_sum;

			$i_income_2++;
		}

		$td_null_income2 = array();
		if($count_income_2 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_2;

			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				$td_null_income2[] = 'xx';

				if(empty($cal_all[$i_income_2]['year1'])) $cal_all[$i_income_2]['year1'] = 0;
				if(empty($cal_all[$i_income_2]['year2'])) $cal_all[$i_income_2]['year2'] = 0;
				if(empty($cal_all[$i_income_2]['year3'])) $cal_all[$i_income_2]['year3'] = 0;

				$i_income_2++;
			}
		}

		$sum_all['year1'] += $sum_year1_sum2;
		$sum_all['year2'] += $sum_year2_sum2;
		$sum_all['year3'] += $sum_year3_sum2;

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๑) ประเภทการผลิต', 'font_normal', 'p_0');

		foreach ($td_income2_year1_1 as $income2_year1_1)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year1_1, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

		foreach ($td_income2_year2_1 as $income2_year2_1)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year2_1, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

		foreach ($td_income2_year3_1 as $income2_year3_1)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year3_1, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๒) ระยะเวลาการผลิต หรือบริการ (เดือน/ปี) ', 'font_normal', 'p_0');

		foreach ($td_income2_year1_2 as $income2_year1_2)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year1_2, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

		foreach ($td_income2_year2_2 as $income2_year2_2)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year2_2, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

		foreach ($td_income2_year3_2 as $income2_year3_2)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year3_2, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๓) รายได้นอกการเกษตร (บาท) ', 'font_normal', 'p_0');

		foreach ($td_income2_year1_4 as $income2_year1_4)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year1_4, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_income2, 2)), 'font_normal', 'p_right');

		foreach ($td_income2_year2_4 as $income2_year2_4)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year2_4, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_income2, 2)), 'font_normal', 'p_right');

		foreach ($td_income2_year3_4 as $income2_year3_4)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year3_4, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_income2, 2)), 'font_normal', 'p_right');

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๔) ค่าใช้จ่ายนอกการเกษตร (บาท)', 'font_normal', 'p_0');

		foreach ($td_income2_year1_5 as $income2_year1_5)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year1_5, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_expenditure2, 2)), 'font_normal', 'p_right');

		foreach ($td_income2_year2_5 as $income2_year2_5)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year2_5, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_expenditure2, 2)), 'font_normal', 'p_right');

		foreach ($td_income2_year3_5 as $income2_year3_5)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year3_5, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_expenditure2, 2)), 'font_normal', 'p_right');

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๕) รายได้นอกการเกษตรสุทธิ (๓) – (๔) ', 'font_normal', 'p_0');

		foreach ($td_income2_year1_6 as $income2_year1_6)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year1_6, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_sum2, 2)), 'font_normal', 'p_right');

		foreach ($td_income2_year2_6 as $income2_year2_6)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year2_6, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_sum2, 2)), 'font_normal', 'p_right');

		foreach ($td_income2_year3_6 as $income2_year3_6)
		{
			$table_expenditure->addCell($td_width)->addText($income2_year3_6, 'font_normal', 'p_0');
		}

		foreach ($td_null_income2 as $null_income2)
		{
			$table_expenditure->addCell($td_width)->addText($null_income2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_sum2, 2)), 'font_normal', 'p_right');
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('๕.๑.๓ รายได้อื่นๆ', 'font_normal', 'p_0');

		for ($i_income3 = 1; $i_income3 <= 3; $i_income3++)
		{
			foreach ($td_income_3 as $list_income_3)
			{
				$table_expenditure->addCell($td_width)->addText($list_income_3, 'font_normal', 'p_0');
			}
		}

		if(!empty($ex_income_3))
		{
			$sum_year1_income3 = 0;
			$sum_year2_income3 = 0;
			$sum_year3_income3 = 0;
			$sum_year1_expenditure3 = 0;
			$sum_year2_expenditure3 = 0;
			$sum_year3_expenditure3 = 0;
			$sum_year1_sum3 = 0;
			$sum_year2_sum3 = 0;
			$sum_year3_sum3 = 0;
			$td_income3_year1_1 = array();
			$td_income3_year2_1 = array();
			$td_income3_year3_1 = array();
			$td_income3_year1_2 = array();
			$td_income3_year2_2 = array();
			$td_income3_year3_2 = array();
			$td_income3_year1_3 = array();
			$td_income3_year2_3 = array();
			$td_income3_year3_3 = array();
			$td_income3_year1_4 = array();
			$td_income3_year2_4 = array();
			$td_income3_year3_4 = array();
			$td_income3_year1_5 = array();
			$td_income3_year2_5 = array();
			$td_income3_year3_5 = array();
			$td_income3_year1_6 = array();
			$td_income3_year2_6 = array();
			$td_income3_year3_6 = array();

			$i_income_3 = 0;
			foreach ($ex_income_3 as $list_income_3)
			{
				$year1_income = floatval($list_income_3['loan_activity_income1']);
				$year2_income = floatval($list_income_3['loan_activity_income2']);
				$year3_income = floatval($list_income_3['loan_activity_income3']);

				$sum_year1_income3 += $year1_income;
				$sum_year2_income3 += $year2_income;
				$sum_year3_income3 += $year3_income;

				$year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
				$year2_expenditure = floatval($list_income_3['loan_activity_expenditure2']);
				$year3_expenditure = floatval($list_income_3['loan_activity_expenditure3']);

				$sum_year1_expenditure3 += $year1_expenditure;
				$sum_year2_expenditure3 += $year2_expenditure;
				$sum_year3_expenditure3 += $year3_expenditure;

				$year1_sum = $year1_income - $year1_expenditure;
				$year2_sum = $year2_income - $year2_expenditure;
				$year3_sum = $year3_income - $year3_expenditure;

				$sum_year1_sum3 += $year1_sum;
				$sum_year2_sum3 += $year2_sum;
				$sum_year3_sum3 += $year3_sum;

				$rai3 = intval($list_income_3['loan_activity_self_rai']) + intval($list_income_3['loan_activity_renting_rai']) + intval($list_income_3['loan_activity_approve_rai']);
				$ngan3 = intval($list_income_3['loan_activity_self_ngan']) + intval($list_income_3['loan_activity_renting_ngan']) + intval($list_income_3['loan_activity_approve_ngan']);
				$wah3 = floatval($list_income_3['loan_activity_self_wah']) + floatval($list_income_3['loan_activity_renting_wah']) + floatval($list_income_3['loan_activity_approve_wah']);

				$area3 = area($rai3, $ngan3, $wah3);

				$td_income3_year1_1[] = num2Thai($list_income_3['loan_activity_name']);
				$td_income3_year2_1[] = num2Thai($list_income_3['loan_activity_name']);
				$td_income3_year3_1[] = num2Thai($list_income_3['loan_activity_name']);
				$td_income3_year1_2[] = '๑ ปี';
				$td_income3_year2_2[] = '๑ ปี';
				$td_income3_year3_2[] = '๑ ปี';
				$td_income3_year1_3[] = num2Thai($area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah']).' ไร่';
				$td_income3_year2_3[] = num2Thai($area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah']).' ไร่';
				$td_income3_year3_3[] = num2Thai($area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah']).' ไร่';
				$td_income3_year1_4[] = num2Thai(number_format($year1_income, 2));
				$td_income3_year2_4[] = num2Thai(number_format($year2_income, 2));
				$td_income3_year3_4[] = num2Thai(number_format($year3_income, 2));
				$td_income3_year1_5[] = num2Thai(number_format($year1_expenditure, 2));
				$td_income3_year2_5[] = num2Thai(number_format($year2_expenditure, 2));
				$td_income3_year3_5[] = num2Thai(number_format($year3_expenditure, 2));
				$td_income3_year1_6[] = num2Thai(number_format($year1_sum, 2));
				$td_income3_year2_6[] = num2Thai(number_format($year2_sum, 2));
				$td_income3_year3_6[] = num2Thai(number_format($year3_sum, 2));

				$cal_all[$i_income_3]['year1'] = empty($cal_all[$i_income_3]['year1'])? $year1_sum : $cal_all[$i_income_3]['year1'] + $year1_sum;
				$cal_all[$i_income_3]['year2'] = empty($cal_all[$i_income_3]['year2'])? $year2_sum : $cal_all[$i_income_3]['year2'] + $year2_sum;
				$cal_all[$i_income_3]['year3'] = empty($cal_all[$i_income_3]['year3'])? $year3_sum : $cal_all[$i_income_3]['year3'] + $year3_sum;

				$i_income_3++;
			}

			$td_null_income3 = array();
			if($count_income_3 < $td_len - 1)
			{
				$count_null = $td_len - 1 - $count_income_3;

				for ($i_null = 1; $i_null<=$count_null; $i_null++)
				{
					$td_null_income3[] = 'xx';
					if(empty($cal_all[$i_income_3]['year1'])) $cal_all[$i_income_3]['year1'] = 0;
					if(empty($cal_all[$i_income_3]['year2'])) $cal_all[$i_income_3]['year2'] = 0;
					if(empty($cal_all[$i_income_3]['year3'])) $cal_all[$i_income_3]['year3'] = 0;

					$i_income_3++;
				}
			}

			$sum_all['year1'] += $sum_year1_sum3;
			$sum_all['year2'] += $sum_year2_sum3;
			$sum_all['year3'] += $sum_year3_sum3;

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๑) ประเภทรายได้อื่นๆ', 'font_normal', 'p_0');

			foreach ($td_income3_year1_1 as $income3_year1_1)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year1_1, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income3_year2_1 as $income3_year2_1)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year2_1, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income3_year3_1 as $income3_year3_1)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year3_1, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๒) ระยะเวลา (เดือน/ปี)', 'font_normal', 'p_0');

			foreach ($td_income3_year1_2 as $income3_year1_2)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year1_2, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income3_year2_2 as $income3_year2_2)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year2_2, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			foreach ($td_income3_year3_2 as $income3_year3_2)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year3_2, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText('xx', 'font_normal', 'p_center');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๓) รายได้อื่นๆ ก่อนหักค่าใช้จ่าย (บาท) ', 'font_normal', 'p_0');

			foreach ($td_income3_year1_4 as $income3_year1_4)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year1_4, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_income3, 2)), 'font_normal', 'p_right');

			foreach ($td_income3_year2_4 as $income3_year2_4)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year2_4, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_income3, 2)), 'font_normal', 'p_right');

			foreach ($td_income3_year3_4 as $income3_year3_4)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year3_4, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_income3, 2)), 'font_normal', 'p_center');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๔) ค่าใช้จ่ายอื่นๆ (บาท)', 'font_normal', 'p_0');

			foreach ($td_income3_year1_5 as $income3_year1_5)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year1_5, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_expenditure3, 2)), 'font_normal', 'p_right');

			foreach ($td_income3_year2_5 as $income3_year2_5)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year2_5, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_expenditure3, 2)), 'font_normal', 'p_right');

			foreach ($td_income3_year3_5 as $income3_year3_5)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year3_5, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_expenditure3, 2)), 'font_normal', 'p_right');

			$table_expenditure->addRow();
			$table_expenditure->addCell($td_remain)->addText('  (๕) รายได้อื่นๆ สุทธิ (๓) - (๔) ', 'font_normal', 'p_0');

			foreach ($td_income3_year1_6 as $income3_year1_6)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year1_6, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_sum3, 2)), 'font_normal', 'p_right');

			foreach ($td_income3_year2_6 as $income3_year2_6)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year2_6, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_sum3, 2)), 'font_normal', 'p_right');

			foreach ($td_income3_year3_6 as $income3_year3_6)
			{
				$table_expenditure->addCell($td_width)->addText($income3_year3_6, 'font_normal', 'p_0');
			}

			foreach ($td_null_income3 as $null_income3)
			{
				$table_expenditure->addCell($td_width)->addText($null_income3, 'font_normal', 'p_center');
			}

			$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_sum3, 2)), 'font_normal', 'p_right');
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('๕.๑.๔ ค่าใช้จ่ายอื่นๆ (บาท)', 'font_normal', 'p_0');

		for ($i_expenditure = 1; $i_expenditure <= 3; $i_expenditure++)
		{
			foreach ($td_expenditure as $list_expenditure)
			{
				$table_expenditure->addCell($td_width)->addText($list_expenditure, 'font_normal', 'p_0');
			}
		}

		$sum_year1_expenditure_1 = 0;
		$sum_year2_expenditure_1 = 0;
		$sum_year3_expenditure_1 = 0;
		$td_year1_expenditure_1 = array();
		$td_year2_expenditure_1 = array();
		$td_year3_expenditure_1 = array();

		$sum_expenditure = array();
		$i_expenditure = 0;
		if(!empty($ex_expenditure_1))
		{
			foreach ($ex_expenditure_1 as $list_expenditure_1)
			{
				$year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
				$year2_amount = floatval($list_expenditure_1['loan_expenditure_amount2']);
				$year3_amount = floatval($list_expenditure_1['loan_expenditure_amount3']);

				$sum_year1_expenditure_1 += $year1_amount;
				$sum_year2_expenditure_1 += $year2_amount;
				$sum_year3_expenditure_1 += $year3_amount;

				$td_year1_expenditure_1[] = num2Thai(number_format($year1_amount, 2));
				$td_year2_expenditure_1[] = num2Thai(number_format($year2_amount, 2));
				$td_year3_expenditure_1[] = num2Thai(number_format($year3_amount, 2));

				$sum_expenditure[$i_expenditure]= array(
						'year1' => $year1_amount,
						'year2' => $year2_amount,
						'year3' => $year3_amount
				);

				$cal_all[$i_expenditure]['year1'] = empty($cal_all[$i_expenditure]['year1'])? 0 - $year1_amount : $cal_all[$i_expenditure]['year1'] - $year1_amount;
				$cal_all[$i_expenditure]['year2'] = empty($cal_all[$i_expenditure]['year2'])? 0 - $year2_amount : $cal_all[$i_expenditure]['year2'] - $year2_amount;
				$cal_all[$i_expenditure]['year3'] = empty($cal_all[$i_expenditure]['year3'])? 0 - $year3_amount : $cal_all[$i_expenditure]['year3'] - $year3_amount;

				$i_expenditure++;
			}
		}

		$td_null_expenditure1 = array();
		if($count_expenditure_1 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_expenditure_1;

			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				$td_null_expenditure1[] = 'xx';

				$sum_expenditure[$i_expenditure]= array(
						'year1' => 0,
						'year2' => 0,
						'year3' => 0
				);

				if(empty($cal_all[$i_expenditure]['year1'])) $cal_all[$i_expenditure]['year1'] = 0;
				if(empty($cal_all[$i_expenditure]['year2'])) $cal_all[$i_expenditure]['year2'] = 0;
				if(empty($cal_all[$i_expenditure]['year3'])) $cal_all[$i_expenditure]['year3'] = 0;

				$i_expenditure++;
			}
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๑) ค่าใช้จ่ายในครัวเรือน', 'font_normal', 'p_0');

		foreach ($td_year1_expenditure_1 as $year1_expenditure_1)
		{
			$table_expenditure->addCell($td_width)->addText($year1_expenditure_1, 'font_normal', 'p_0');
		}

		foreach ($td_null_expenditure1 as $null_expenditure1)
		{
			$table_expenditure->addCell($td_width)->addText($null_expenditure1, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_expenditure_1, 2)), 'font_normal', 'p_right');

		foreach ($td_year2_expenditure_1 as $year2_expenditure_1)
		{
			$table_expenditure->addCell($td_width)->addText($year2_expenditure_1, 'font_normal', 'p_0');
		}

		foreach ($td_null_expenditure1 as $null_expenditure1)
		{
			$table_expenditure->addCell($td_width)->addText($null_expenditure1, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_expenditure_1, 2)), 'font_normal', 'p_right');

		foreach ($td_year3_expenditure_1 as $year3_expenditure_1)
		{
			$table_expenditure->addCell($td_width)->addText($year3_expenditure_1, 'font_normal', 'p_0');
		}

		foreach ($td_null_expenditure1 as $null_expenditure1)
		{
			$table_expenditure->addCell($td_width)->addText($null_expenditure1, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_expenditure_1, 2)), 'font_normal', 'p_right');

		$sum_year1_expenditure_2 = 0;
		$sum_year2_expenditure_2 = 0;
		$sum_year3_expenditure_2 = 0;
		$td_year1_expenditure_2 = array();
		$td_year2_expenditure_2 = array();
		$td_year3_expenditure_2 = array();

		$i_expenditure = 0;
		if(!empty($ex_expenditure_2))
		{
			foreach ($ex_expenditure_2 as $list_expenditure_2)
			{
				$year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
				$year2_amount = floatval($list_expenditure_2['loan_expenditure_amount2']);
				$year3_amount = floatval($list_expenditure_2['loan_expenditure_amount3']);

				$sum_year1_expenditure_2 += $year1_amount;
				$sum_year2_expenditure_2 += $year2_amount;
				$sum_year3_expenditure_2 += $year3_amount;

				$td_year1_expenditure_2[] = num2Thai(number_format($year1_amount, 2));
				$td_year2_expenditure_2[] = num2Thai(number_format($year2_amount, 2));
				$td_year3_expenditure_2[] = num2Thai(number_format($year3_amount, 2));

				$sum_expenditure[$i_expenditure]['year1'] = empty($sum_expenditure[$i_expenditure]['year1'])? $year1_amount : $sum_expenditure[$i_expenditure]['year1'] + $year1_amount;
				$sum_expenditure[$i_expenditure]['year2'] = empty($sum_expenditure[$i_expenditure]['year2'])? $year2_amount : $sum_expenditure[$i_expenditure]['year2'] + $year2_amount;
				$sum_expenditure[$i_expenditure]['year3'] = empty($sum_expenditure[$i_expenditure]['year3'])? $year3_amount : $sum_expenditure[$i_expenditure]['year3'] + $year3_amount;

				$cal_all[$i_expenditure]['year1'] = empty($cal_all[$i_expenditure]['year1'])? 0 - $year1_amount : $cal_all[$i_expenditure]['year1'] - $year1_amount;
				$cal_all[$i_expenditure]['year2'] = empty($cal_all[$i_expenditure]['year2'])? 0 - $year2_amount : $cal_all[$i_expenditure]['year2'] - $year2_amount;
				$cal_all[$i_expenditure]['year3'] = empty($cal_all[$i_expenditure]['year3'])? 0 - $year3_amount : $cal_all[$i_expenditure]['year3'] - $year3_amount;

				$i_expenditure++;
			}
		}

		$td_null_expenditure2 = array();
		if($count_expenditure_2 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_expenditure_2;

			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				$td_null_expenditure2[] = 'xx';

				$sum_expenditure[$i_expenditure]['year1'] = empty($sum_expenditure[$i_expenditure]['year1'])? 0 : $sum_expenditure[$i_expenditure]['year1'] + 0;
				$sum_expenditure[$i_expenditure]['year2'] = empty($sum_expenditure[$i_expenditure]['year2'])? 0 : $sum_expenditure[$i_expenditure]['year2'] + 0;
				$sum_expenditure[$i_expenditure]['year3'] = empty($sum_expenditure[$i_expenditure]['year3'])? 0 : $sum_expenditure[$i_expenditure]['year3'] + 0;

				if(empty($cal_all[$i_expenditure]['year1'])) $cal_all[$i_expenditure]['year1'] = 0;
				if(empty($cal_all[$i_expenditure]['year2'])) $cal_all[$i_expenditure]['year2'] = 0;
				if(empty($cal_all[$i_expenditure]['year3'])) $cal_all[$i_expenditure]['year3'] = 0;

				$i_expenditure++;
			}
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๒) ค่าใช้จ่ายอื่นๆ', 'font_normal', 'p_0');

		foreach ($td_year1_expenditure_2 as $year1_expenditure_2)
		{
			$table_expenditure->addCell($td_width)->addText($year1_expenditure_2, 'font_normal', 'p_0');
		}

		foreach ($td_null_expenditure2 as $null_expenditure2)
		{
			$table_expenditure->addCell($td_width)->addText($null_expenditure2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_expenditure_2, 2)), 'font_normal', 'p_right');

		foreach ($td_year2_expenditure_2 as $year2_expenditure_2)
		{
			$table_expenditure->addCell($td_width)->addText($year2_expenditure_2, 'font_normal', 'p_0');
		}

		foreach ($td_null_expenditure2 as $null_expenditure2)
		{
			$table_expenditure->addCell($td_width)->addText($null_expenditure2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_expenditure_2, 2)), 'font_normal', 'p_right');

		foreach ($td_year3_expenditure_2 as $year3_expenditure_2)
		{
			$table_expenditure->addCell($td_width)->addText($year3_expenditure_2, 'font_normal', 'p_0');
		}

		foreach ($td_null_expenditure2 as $null_expenditure2)
		{
			$table_expenditure->addCell($td_width)->addText($null_expenditure2, 'font_normal', 'p_center');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_expenditure_2, 2)), 'font_normal', 'p_right');

		$sum_year1_expenditure_all = 0;
		$sum_year2_expenditure_all = 0;
		$sum_year3_expenditure_all = 0;
		$td_year1_expenditure_all = array();
		$td_year2_expenditure_all = array();
		$td_year3_expenditure_all = array();

		foreach ($sum_expenditure as $list_expenditure_all)
		{
			$year1_amount = $list_expenditure_all['year1'];
			$year2_amount = $list_expenditure_all['year2'];
			$year3_amount = $list_expenditure_all['year3'];

			$sum_year1_expenditure_all += $year1_amount;
			$sum_year2_expenditure_all += $year2_amount;
			$sum_year3_expenditure_all += $year3_amount;

			$td_year1_expenditure_all[] = num2Thai(number_format($year1_amount, 2));
			$td_year2_expenditure_all[] = num2Thai(number_format($year2_amount, 2));
			$td_year3_expenditure_all[] = num2Thai(number_format($year3_amount, 2));
		}

		$sum_all['year1'] -= $sum_year1_expenditure_all;
		$sum_all['year2'] -= $sum_year2_expenditure_all;
		$sum_all['year3'] -= $sum_year3_expenditure_all;

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('  (๓) รวมค่าใช้จ่าย (๑) + (๒)', 'font_normal', 'p_0');

		foreach ($td_year1_expenditure_all as $year1_expenditure_all)
		{
			$table_expenditure->addCell($td_width)->addText($year1_expenditure_all, 'font_normal', 'p_0');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year1_expenditure_all, 2)), 'font_normal', 'p_right');

		foreach ($td_year2_expenditure_all as $year2_expenditure_all)
		{
			$table_expenditure->addCell($td_width)->addText($year2_expenditure_all, 'font_normal', 'p_0');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year2_expenditure_all, 2)), 'font_normal', 'p_right');

		foreach ($td_year3_expenditure_all as $year3_expenditure_all)
		{
			$table_expenditure->addCell($td_width)->addText($year3_expenditure_all, 'font_normal', 'p_0');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_year3_expenditure_all, 2)), 'font_normal', 'p_right');

		$td_year1 = array();
		$td_year2 = array();
		$td_year3 = array();

		foreach ($cal_all as $list_cal_all)
		{
			$td_year1[] = num2Thai(number_format($list_cal_all['year1'], 2));
			$td_year2[] = num2Thai(number_format($list_cal_all['year2'], 2));
			$td_year3[] = num2Thai(number_format($list_cal_all['year3'], 2));
		}

		$table_expenditure->addRow();
		$table_expenditure->addCell($td_remain)->addText('๕.๑.๕ รายได้สุทธิ (บาท) ๕.๑.๑ (๖) + ๕.๑.๒ (๕) + ๕.๑.๓ - ๕.๑.๔ (๓)', 'font_normal', 'p_0');

		foreach ($td_year1 as $list_td_year1)
		{
			$table_expenditure->addCell($td_width)->addText($list_td_year1, 'font_normal', 'p_0');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_all['year1'], 2)), 'font_normal', 'p_right');

		foreach ($td_year2 as $list_td_year2)
		{
			$table_expenditure->addCell($td_width)->addText($list_td_year2, 'font_normal', 'p_0');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_all['year2'], 2)), 'font_normal', 'p_right');

		foreach ($td_year3 as $list_td_year3)
		{
			$table_expenditure->addCell($td_width)->addText($list_td_year3, 'font_normal', 'p_0');
		}

		$table_expenditure->addCell($td_width)->addText(num2Thai(number_format($sum_all['year3'], 2)), 'font_normal', 'p_right');

		$section->addText(' 	 	      ๕.๒ แผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ (กรณีขอสินเชื่อเพื่อไปประกอบอาชีพเกษตรกรรมของผู้ขอสินเชื่อว่ามีแผนการผลิต แหล่งเงินทุนที่ใช้ในการผลิต และการตลาด อย่างไร)         -       ', 'font_normal', 'p_0');
		$section->addText(' 	 	 ๖. ประมาณการกระแสเงินสดและแผนการชำระคืนของผู้ขอสินเชื่อ', 'font_normal', 'p_0');

		$schedule = $this->loan_schedule->getByLoan($id);
		$round_start = '';
		$round_end = '';
		$list_round =  array();
		if(!empty($schedule))
		{
			foreach ($schedule as $list_schedule)
			{
				if($round_start=='') $round_start = $list_schedule['loan_schedule_date'];
				$round_end = $list_schedule['loan_schedule_date'];

				$list_round[$list_schedule['loan_schedule_times']] = array(
						'principle' => floatval($list_schedule['loan_schedule_amount']),
						'other' => intval($list_schedule['loan_schedule_other']),
						'principle_other' => intval($list_schedule['loan_schedule_principle']),
						'interest_other' => intval($list_schedule['loan_schedule_interest'])
				);
			}
		}

		$income = floatval($analysis['loan_analysis_income']);
		$amount = floatval($data['loan_amount']);
		$interest = intval($analysis['loan_analysis_interest']) / 100;
		$type = intval($analysis['loan_analysis_length']);
		$year = intval($analysis['loan_analysis_year']);

		$start_round = new DateTime($round_start);
		$end_round = new DateTime($round_end);
		$interval = $start_round->diff($end_round);
		$interval = $interval->format('%y/%m');
		$year_explode = explode("/", $interval);
		$y= $year_explode[0];
		$m= $year_explode[1];
		if ($m == '0') {
			$count_year = $y.' ปี ';
		}
		else {
			$count_year = $y.' ปี '.$m.' เดือน';
		}

		$pay = $analysis['loan_analysis_pay'];
		$start = $analysis['loan_analysis_start'];

		$start = convert_ymd_en($start);
		$pay = convert_ymd_en($pay);

		$date_start = $start;
		$date_start = new DateTime($date_start);
		$date_start->sub(new DateInterval('P1D'));
		$date_start = $date_start->format('Y-m-d');
		$date_end = $pay;

		$list_month = array();
		$remain = $amount;

		if(!empty($list_round))
		{
			$i_key = 1;
			foreach ($list_round as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				if($i_key==1) $day_between--;

				$interest_per = number_format(($remain * $interest )  * ($day_between / 365), 2, '.', '');

				if($interest_per < 0)  $interest_per = 0;

				$list_round[$r_key] = array(
						'income' => $income * $type /12,
						'principle' => $r_value['principle'],
						'remain' => $remain,
						'interest' => $interest_per,
						'other' => $r_value['other'],
						'principle_other' => $r_value['principle_other'],
						'interest_other' => $r_value['interest_other'],
						'date' => $date_end
				);

				$list_month[] = date('m', strtotime($date_end));

				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$type.'M'));
				$date_end = $date_end->format('Y-m-d');

				$i_key++;
			}

			$list_month = array_unique($list_month);

			$month = '';
			foreach ($list_month as $data_month)
			{
				$month .= thai_month($data_month).', ';
			}

			if(!empty($month)) $month = substr($month, 0, -2);

			$fiscal_start = fiscalYear($start);
			$fiscal_end = fiscalYear($date_start);

			//$show_first = 0;
			$sum_estimate_amt = 0;
			if(!empty($list_round))
			{
				$remain = 0;
				$i_round = 1;
				$i_tab = 1;
				$i_start = 1;
				$c_round = 1;
				$count_round = count($list_round);

				$html_round = array();
				$html_1 = array();
				$html_1_1 = array();
				$html_1_2 = array();
				$html_1_3 = array();
				$html_1_4 = array();
				$html_1_5 = array();
				$html_1_6 = array();
				$html_2 = array();
				$html_2_1 = array();
				$html_2_2 = array();
				$html_2_3 = array();
				$html_2_4 = array();
				$html_2_5 = array();
				$html_2_6 = array();
				$html_2_7 = array();
				$html_2_8 = array();
				$html_2_9 = array();
				$html_3 = array();
				$html_4 = array();

				$end_show = '';

				foreach ($list_round as $key_round => $round)
				{
					$data1_4 = $round['other'];
					if($key_round=='1')
					{
						$data1_2 = $amount;
						$data1_6 = $round['income'] + $data1_2 + $data1_4;
						$data2_2 = $amount;

						//$show_first = $round['principle'] + $round['interest'];
					}
					else
					{
						$data1_2 = 0;
						$data1_6 = $round['income'] + $data1_4;
						$data2_2 = 0;
					}
					$sum_estimate_amt += $round['principle'] + $round['interest'];

					$data2_3 = $round['principle'];
					$data2_4 = $round['interest'];
					$data2_7 = $round['principle_other'];
					$data2_8 = $round['interest_other'];

					$data2_9 = $data2_2 + $data2_3 + $data2_4 + $data2_7 + $data2_8;
					$data3 = $data1_6 - $data2_9;

					$remain += $data3;

					$html_round[] = num2Thai($key_round); //center
					$html_1[] = 'xx'; //center
					$html_1_1[] = num2Thai(number_format($round['income'], 2)); //right
					$html_1_2[] = num2Thai(number_format($data1_2, 2)); //right
					$html_1_3[] = '๐.๐๐'; //right
					$html_1_4[] = num2Thai(number_format($data1_4, 2)); //right
					$html_1_5[] = '๐.๐๐'; //right
					$html_1_6[] = num2Thai(number_format($data1_6, 2)); //right
					$html_2[] = '';
					$html_2_1[] = num2Thai(number_format($round['remain'], 2)); //right
					$html_2_2[] = num2Thai(number_format($data2_2, 2)); //right
					$html_2_3[] = num2Thai(number_format($data2_3, 2)); //right
					$html_2_4[] = num2Thai(number_format($data2_4, 2)); //right
					$html_2_5[] = '๐.๐๐'; //right
					$html_2_6[] = '๐.๐๐'; //right
					$html_2_7[] = num2Thai(number_format($data2_7, 2)); //right
					$html_2_8[] = num2Thai(number_format($data2_8, 2)); //right
					$html_2_9[] = num2Thai(number_format($data2_9, 2)); //right
					$html_3[] = num2Thai(number_format($data3, 2)); //right
					$html_4[] = num2Thai(number_format($remain, 2)); //right

					if($i_round%5==0 || $i_round==$count_round)
					{
						$table_ana = $section->addTable('myOwnTableStyle');

						$count_td = count($html_round);
						$td_width = ceil(10000 / $count_td); //3000
						$td_remain = 15000 - ($td_width * $count_td);

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('๖.๑ เงินสดรับ (บาท)', 'font_normal', 'p_0');

						foreach ($html_round as $value_round)
						{
							$table_ana->addCell($td_width)->addText($value_round, 'font_normal', 'p_center');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('งวดที่ (ตั้งแต่งวดที่ '.num2Thai($i_start).' จนถึงงวดที่ '.num2Thai($i_round).')', 'font_normal', 'p_0');

						foreach ($html_1 as $value_html1)
						{
							$table_ana->addCell($td_width)->addText($value_html1, 'font_normal', 'p_center');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๑) รายได้สุทธิ (ตามข้อ ๕.๑.๕)', 'font_normal', 'p_0');

						foreach ($html_1_1 as $value_html1_1)
						{
							$table_ana->addCell($td_width)->addText($value_html1_1, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๒) สินเชื่อของ บจธ.', 'font_normal', 'p_0');

						foreach ($html_1_2 as $value_html1_2)
						{
							$table_ana->addCell($td_width)->addText($value_html1_2, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๓) สินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม (บจธ.)', 'font_normal', 'p_0');

						foreach ($html_1_3 as $value_html1_3)
						{
							$table_ana->addCell($td_width)->addText($value_html1_3, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๔) เงินกู้จากเจ้าหนี้อื่นๆ', 'font_normal', 'p_0');

						foreach ($html_1_4 as $value_html1_4)
						{
							$table_ana->addCell($td_width)->addText($value_html1_4, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๕) เงินทุนสมทบของตนเอง (ถ้ามี)', 'font_normal', 'p_0');

						foreach ($html_1_5 as $value_html1_5)
						{
							$table_ana->addCell($td_width)->addText($value_html1_5, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๖) รวมเงินสดรับ (๑)+(๒)+(๓)+(๔)+(๕)', 'font_normal', 'p_0');

						foreach ($html_1_6 as $value_html1_6)
						{
							$table_ana->addCell($td_width)->addText($value_html1_6, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('๖.๒ เงินสดจ่าย (บาท)', 'font_normal', 'p_0');

						foreach ($html_2 as $value_html2)
						{
							$table_ana->addCell($td_width)->addText($value_html2, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๑) สินเชื่อของ บจธ. คงเหลือ', 'font_normal', 'p_0');

						foreach ($html_2_1 as $value_html2_1)
						{
							$table_ana->addCell($td_width)->addText($value_html2_1, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๒) ชำระหนี้สินเดิมให้เจ้าหนี้', 'font_normal', 'p_0');

						foreach ($html_2_2 as $value_html2_2)
						{
							$table_ana->addCell($td_width)->addText($value_html2_2, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๓) ชำระต้นเงินสินเชื่อของ บจธ.', 'font_normal', 'p_0');

						foreach ($html_2_3 as $value_html2_3)
						{
							$table_ana->addCell($td_width)->addText($value_html2_3, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๔) ชำระดอกเบี้ยของสินเชื่อของ บจธ.', 'font_normal', 'p_0');

						foreach ($html_2_4 as $value_html2_4)
						{
							$table_ana->addCell($td_width)->addText($value_html2_4, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๕) ชำระต้นเงินสินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม', 'font_normal', 'p_0');

						foreach ($html_2_5 as $value_html2_5)
						{
							$table_ana->addCell($td_width)->addText($value_html2_5, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๖) ชำระดอกเบี้ยสินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม', 'font_normal', 'p_0');

						foreach ($html_2_6 as $value_html2_6)
						{
							$table_ana->addCell($td_width)->addText($value_html2_6, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๗) ชำระดอกเบี้ยต้นเงินหนี้อื่นๆ', 'font_normal', 'p_0');

						foreach ($html_2_7 as $value_html2_7)
						{
							$table_ana->addCell($td_width)->addText($value_html2_7, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๘) ชำระดอกเบี้ยหนี้อื่นๆ', 'font_normal', 'p_0');

						foreach ($html_2_8 as $value_html2_8)
						{
							$table_ana->addCell($td_width)->addText($value_html2_8, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('    (๙) รวมเงินสดจ่าย (๒)+(๓)+(๔)+(๕)+(๖)+(๗)+(๘)', 'font_normal', 'p_0');

						foreach ($html_2_9 as $value_html2_9)
						{
							$table_ana->addCell($td_width)->addText($value_html2_9, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('๖.๓ เงินสดคงเหลือ ๑ (๖) - ๒ (๙)', 'font_normal', 'p_0');

						foreach ($html_3 as $value_html3)
						{
							$table_ana->addCell($td_width)->addText($value_html3, 'font_normal', 'p_right');
						}

						$table_ana->addRow();
						$table_ana->addCell($td_remain)->addText('๖.๔ เงินสดคงเหลือสะสม', 'font_normal', 'p_0');

						foreach ($html_4 as $value_html4)
						{
							$table_ana->addCell($td_width)->addText($value_html4, 'font_normal', 'p_right');
						}

						$section->addTextBreak(1);

						$i_tab++;
						$i_start = $i_round + 1;
						$c_round = 1;

						$end_show = $round['date'];

						$html_round = array();
						$html_1 = array();
						$html_1_1 = array();
						$html_1_2 = array();
						$html_1_3 = array();
						$html_1_4 = array();
						$html_1_5 = array();
						$html_1_6 = array();
						$html_2 = array();
						$html_2_1 = array();
						$html_2_2 = array();
						$html_2_3 = array();
						$html_2_4 = array();
						$html_2_5 = array();
						$html_2_6 = array();
						$html_2_7 = array();
						$html_2_8 = array();
						$html_2_9 = array();
						$html_3 = array();
						$html_4 = array();
					}
					else
					{
						$c_round++;
					}

					$i_round++;
				}
				$estimate_plan =  floor($sum_estimate_amt / ($year * 12 / $type)/100) * 100;

			}
		}

		if($type==1)
		{
			$type = 12;
		}
		else if($type==6)
		{
			$type = 2;
		}
		else
		{
			$type = 1;
		}

		$section->addText('๖.๕ จากประมาณการกระแสเงินสดข้างต้น ผู้ขอสินเชื่อชำระคืนเงินต้นเสร็จสิ้นภายใน '.num2Thai($count_year).' ตั้งแต่ปีบัญชี '.num2Thai($fiscal_start).' ถึงปีบัญชี '.num2Thai($fiscal_end).' โดยชำระต้นเงินและดอกเบี้ยได้ เป็นเงินปีละประมาณ '.num2Thai(number_format($estimate_plan, 2)).' บาท และชำระหนี้ได้ปีละ '.num2Thai($type).' ครั้ง ในแต่ละปีชำระได้ในเดือน '.$month, 'font_normal', 'p_0');
		$section->addText(' 	 	 ๗. สรุปความเห็นในการพิจารณาอนุมัติคำขอสินเชื่อของผู้สอบสวน', 'font_normal', 'p_0');
		$section->addText(' 	 	      ๗.๑ พิจารณาตามหลักเกณฑ์ของ บจธ.', 'font_normal', 'p_0');
		$section->addText(' 	 	             ความเห็น '.(empty($analysis['loan_analysis_investigate_comment'])? ' - ' : num2Thai($analysis['loan_analysis_investigate_comment'])), 'font_normal', 'p_0');
		$section->addText(' 	 	      ๗.๒ มูลเหตุแห่งหนี้', 'font_normal', 'p_0');
		$section->addText(' 	 	             ๗.๒.๑ สุจริตหรือไม่สุจริต ดังนี้ '.(empty($analysis['loan_analysis_investigate_honestly'])? ' - ' : num2Thai($analysis['loan_analysis_investigate_honestly'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๗.๒.๒ กรณีระยะเวลาในการก่อหนี้สินเพิ่งจะเกิดขึ้นไม่ถึงหนึ่งปี มีเหตุผลที่น่าเชื่อว่าหนี้นั้นเกิดขึ้นโดยสุจริต ดังนี้ '.(empty($analysis['loan_analysis_investigate_cause'])? ' - ' : num2Thai($analysis['loan_analysis_investigate_cause'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๗.๒.๓ ความจำเป็นหรือไม่มีความจำเป็น ดังนี้ '.(empty($analysis['loan_analysis_investigate_must'])? ' - ' : num2Thai($analysis['loan_analysis_investigate_must'])), 'font_normal', 'p_0');
		$section->addText(' 	 	      ๗.๓ กรณีที่ดินแปลงซึ่งขอสินเชื่อเพื่อไถ่ถอนหรือซื้อคืนมีมูลค่าน้อยกว่าจำนวนเงินที่ขอสินเชื่อ การอนุมัติคำขอสินเชื่อครั้งนี้จะเป็นประโยชน์ต่อผู้ขอสินเชื่ออย่างไร '.(empty($analysis['loan_analysis_investigate_matter'])? ' - ' : num2Thai($analysis['loan_analysis_investigate_matter'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             '.$check_guarantee.' ผู้ขอสินเชื่อได้เสนอหลักประกันเพิ่มเติม', 'font_bold', 'p_0');
		$section->addText(' 	 	             '.$check_guarantee_land.' ที่ดิน', 'font_normal', 'p_0');
		$section->addText(' 	 	             '.$check_guarantee_bond.' พันธบัตรรัฐบาล', 'font_normal', 'p_0');
		$section->addText(' 	 	             '.$check_guarantee_bookbank.' เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์', 'font_normal', 'p_0');
		$section->addText(' 	 	             '.($check_guarantee=='( )'? '( )' : '( )').' ผู้ขอสินเชื่อไม่สามารถหาหลักประกันเพิ่มเติมได้', 'font_normal', 'p_0');
//		$section->addText(' 	 	             '.($check_guarantee=='( )'? '(√)' : '( )').' ผู้ขอสินเชื่อไม่สามารถหาหลักประกันเพิ่มเติมได้', 'font_normal', 'p_0');

		$section->addText(' 	 	      ๗.๔ สรุปความเห็นและผลการพิจารณาเกี่ยวกับการขอสินเชื่อตามคำขอ', 'font_normal', 'p_0');

		$section->addText(' 	 	             ๗.๔.๑ ( ) เห็นสมควรอนุมัติในวงเงินสินเชื่อ '.num2Thai(number_format($data['loan_amount'], 2)).' บาท กำหนดชำระคืนเสร็จเรียบร้อยภายในวันที่ '.(empty($end_show)? ' - ' : num2Thai(thai_display_date($end_show))).' หลักประกัน '.(empty($guarantee)? ' - ' : num2Thai($guarantee)), 'font_normal', 'p_0');
		$section->addText(' 	 	                   เงื่อนไขหรือคำชี้แจงเพิ่มเติม '.(empty($analysis['loan_analysis_approve_conditions'])? ' - ' : num2Thai($analysis['loan_analysis_approve_conditions'])), 'font_normal', 'p_0');
		$section->addText(' 	 	             ๗.๔.๒ ( ) เห็นสมควรไม่อนุมัติ', 'font_normal', 'p_0');
		$section->addText('                                         ( ) อยู่ในหลักเกณฑ์ให้สินเชื่อจาก บจธ. ปี .......................... เนื่องจาก '.(empty($analysis['loan_analysis_approve_not_case'])? ' - ' : num2Thai($analysis['loan_analysis_approve_not_case'])), 'font_normal', 'p_0');
		$section->addText('                                         ( ) ไม่อยู่ในหลักเกณฑ์ให้สินเชื่อจาก บจธ. ปี .......................... เนื่องจาก '.(empty($analysis['loan_analysis_approve_not_case'])? ' - ' : num2Thai($analysis['loan_analysis_approve_not_case'])), 'font_normal', 'p_0');

		$section->addTextBreak(1);

		$table_cent1 = $section->addTable();

		$table_cent1->addRow();
		$table_cent1->addCell(10000);

		$user = $this->user_model->get_table_by_id($analysis['loan_analysis_createby']);

		$cell_cent1 = $table_cent1->addCell(5000);
		$cell_cent1->addText('ลงชื่อ ......................................................', 'font_normal', 'p_center');
		$employee = $this->employee_model->getByUserId(get_uid_login());
		$emp_name = $employee?($employee['employee_fname'].' '.$employee['employee_lname']):$user['user_fname'].' '.$user['user_lname'];
		$cell_cent1->addText('('.$emp_name.')', 'font_normal', 'p_center');
		$cell_cent1->addText('ตำแหน่ง ....'.($employee?$employee['position_name']:' ').'.......................', 'font_normal', 'p_center');
		$cell_cent1->addText('วันที่ .........................................', 'font_normal', 'p_center');

		$section->addText(' 	 	 ๘. สรุปความเห็นในการพิจารณาคำขอสินเชื่อของฝ่าย/กอง ..................................................................................................................................................', 'font_normal', 'p_0');
		$section->addText(' 	 	      ( ) อนุมัติในวงเงินสินเชื่อ ............................................................ บาท กำหนดชำระคืนเสร็จเรียบร้อยภายในวันที่ ………………………………………………… หลักประกัน .........................................................................................................................................................................................................................................................', 'font_normal', 'p_0');
		$section->addText(' 	 	      เงื่อนไขหรือคำชี้แจงเพิ่มเติม ............................................................................................................................................................................................', 'font_normal', 'p_0');
		$section->addText('..............................................................................................................................................................................................................................................................................', 'font_normal', 'p_0');
		$section->addText(' 	 	      ( ) ไม่อนุมัติ เนื่องจาก ......................................................................................................................................................................................................', 'font_normal', 'p_0');

		$section->addTextBreak(1);

		$table_cent2 = $section->addTable();

		$table_cent2->addRow();
		$table_cent2->addCell(10000);

		$cell_cent2 = $table_cent2->addCell(5000);
		$cell_cent2->addText('ลงชื่อ ......................................................', 'font_normal', 'p_center');
		$cell_cent2->addText('(.................................................)', 'font_normal', 'p_center');
		$cell_cent2->addText('ตำแหน่ง ..................................', 'font_normal', 'p_center');
		$cell_cent2->addText('วันที่ .........................................', 'font_normal', 'p_center');

		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
		$temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
		$objWriter->save($temp_file);
		// echo "<pre>";
		// print_r($temp_file);
		// echo "</pre>";

		$filename = 'analysis_'.date('Ymd').'.docx';

		// header('Content-Disposition: attachment; filename="'.$filename.'.docx"');
		// readfile($temp_file);
		// unlink($temp_file);
		$this->send_download($temp_file,$filename);

	}

	public function send_download($temp_file,$file) {
	      $basename = basename($file);
	      $length   = sprintf("%u", filesize($temp_file));

	      header('Content-Description: File Transfer');
	      header('Content-Type: application/octet-stream');
	      header('Content-Disposition: attachment; filename="' . $basename . '"');
	      header('Content-Transfer-Encoding: binary');
	      header('Connection: Keep-Alive');
	      header('Expires: 0');
	      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	      header('Pragma: public');
	      header('Content-Length: ' . $length);
	      ob_clean();
	      flush();
	      set_time_limit(0);
	      readfile($temp_file);
	      exit();
	  }
	public function pdf($id, $id_analysis)
	{
		ob_start();
		$this->load->library('tcpdf');

		$this->load->model('loan_model', 'loan');
		$this->load->model('title/title_model', 'title');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('career/career_model', 'career');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_owner_model', 'land_owner');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_schedule_model', 'loan_schedule');

		$data = $this->loan->getById($id);
		$analysis = $this->loan_analysis->getById($id_analysis);

		$person[] = $data['person_id'];

		$data['person']['title'] = $this->title->getById($data['person']['title_id']);
		$data['person']['person_age'] = empty($data['person']['person_birthdate'])? '' : age($data['person']['person_birthdate']);
		$data['person']['district'] = $this->district->getById($data['person']['person_addr_pre_district_id']);
		$data['person']['amphur'] = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
		$data['person']['province'] = $this->province->getById($data['person']['person_addr_pre_province_id']);
		$data['person']['career'] = $this->career->getById($data['person']['career_id']);

		$loan_type = $this->loan_type->getByLoan($id);

		$loan_type_objective1 = '';
		$loan_type_objective2 = '';
		$loan_type_objective3 = '';
		$loan_creditor = '';
		$loan_real_amount = 0;
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$loan_type_objective1 = '(/)';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_redeem_amount']);

					if(!empty($list_type['loan_type_redeem_owner']))
					{
						$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(/)';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_contract_amount']);

					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(/)';
					$loan_type_objective3 = '( )';

					$loan_real_amount += floatval($list_type['loan_type_case_amount']);

					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '(/)';

					$loan_real_amount += floatval($list_type['loan_type_sale_amount']);
				}
			}
		}

		$married_list = $this->married->all();

		$html_married = '';
		foreach ($married_list as $married)
		{
			$html_married .= $married['status_married_id']==$data['person']['status_married_id']? '(/) ' : '( ) ';
			$html_married .= $married['status_married_name'].'&nbsp;&nbsp;';
		}

		if(empty($data['loan_spouse']))
		{
			$spouse_name = '-';
			$spouse_age = '-';
		}
		else
		{
			$spouse = $this->person->getById($data['loan_spouse']);
			$spouse_title = $this->title->getById($spouse['title_id']);
			$spouse_name = $spouse_title['title_name'].$spouse['person_fname'].'  '.$spouse['person_lname'];
			$spouse_age = empty($spouse['person_birthdate'])? '' : age($spouse['person_birthdate']);
		}

		$debt =  $this->loan_debt->getByLoan($data['loan_id']);

		$debt_text = '';
		$debt_owner_text = '';
		$debt_sum = 0;
		if(!empty($debt))
		{
			foreach ($debt as $list_debt)
			{
				$debt_text .= $list_debt['loan_debt_name'].', ';
				if($list_debt['loan_debt_owner_type']=='1') $debt_owner_text .= $list_debt['loan_debt_name'].' จำนวน '.number_format($list_debt['loan_debt_amount'], 2).' บาท, ';

				$debt_sum += floatval($list_debt['loan_debt_amount']);
			}
		}

		if($debt_text!='') $debt_text = num2Thai(substr($debt_text, 0, -2)).'&nbsp;&nbsp;';
		if($debt_owner_text!='') $debt_owner_text = ' ('.num2Thai(substr($debt_owner_text, 0, -2)).')';

		$loan_co =  $this->loan_co->getByLoan($id);

		$html_loan_co = '';
		if(!empty($loan_co))
		{
			$html_loan_co = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td width="10%"></td>
			<td width="90%">กรณีผู้ขอสินเชื่ออายุมากกว่า ๖๕ ปี หรือมีความสามารถในการชำระหนี้ได้ไม่เพียงพอ ผู้ขอสินเชื่อได้เสนอผู้กู้ร่วม คือ</td>
			</tr>
			</table>';
			$i_co = 1;
			foreach ($loan_co as $list_co)
			{
				$person[] = $list_co['person_id'];
				$age_co = age($list_co['person_birthdate']);

				$html_loan_co .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="15%"></td>
				<td width="30%">('.num2Thai($i_co++).') ชื่อ&nbsp;&nbsp;'.$list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname'].'</td>
				<td width="15%">อายุ '.num2Thai($age_co).' ปี</td>
				<td width="40%">มีความเกี่ยวข้องกับผู้ขอสินเชื่อ&nbsp;&nbsp;'.$list_co['relationship_name'].'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="18%"></td>
				<td width="82%">ประกอบอาชีพ&nbsp;&nbsp;'.$list_co['career_name'].'</td>
				</tr>
				</table>';
			}
		}

		$loan_asset =  $this->loan_asset->getByLoan($id);

		$asset_sum = 0;
		if(!empty($loan_asset))
		{
			foreach ($loan_asset as $list_asset)
			{
				$asset_sum += floatval($list_asset['loan_asset_amount']);
			}
		}

		$loan_debt =  $this->loan_debt->getByLoan($id);

		$debt_sum = 0;
		if(!empty($loan_debt))
		{
			foreach ($loan_debt as $list_debt)
			{
				$debt_sum += floatval($list_debt['loan_debt_amount']);
			}
		}

		$loan_income_1 = $this->loan_income->getByPerson($data['person_id'], '1');
		$loan_income_2 = $this->loan_income->getByPerson($data['person_id'], '2');
		$loan_income_3 = $this->loan_income->getByPerson($data['person_id'], '3');

		$income_all['rai'] = 0;
		$income_all['ngan'] = 0;
		$income_all['wah'] = 0;
		$income_self['rai'] = 0;
		$income_self['ngan'] = 0;
		$income_self['wah'] = 0;
		$income_renting['rai'] = 0;
		$income_renting['ngan'] = 0;
		$income_renting['wah'] = 0;
		$income_approve['rai'] = 0;
		$income_approve['ngan'] = 0;
		$income_approve['wah'] = 0;

		if(!empty($loan_income_1))
		{
			foreach ($loan_income_1 as $income1_value)
			{
				$self_rai = empty($income1_value['loan_activity_self_rai'])? 0 : intval($income1_value['loan_activity_self_rai']);
				$self_ngan = empty($income1_value['loan_activity_self_ngan'])? 0 : intval($income1_value['loan_activity_self_ngan']);
				$self_wah = empty($income1_value['loan_activity_self_wah'])? 0 : floatval($income1_value['loan_activity_self_wah']);
				$renting_rai = empty($income1_value['loan_activity_renting_rai'])? 0 : intval($income1_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income1_value['loan_activity_renting_ngan'])? 0 : intval($income1_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income1_value['loan_activity_renting_wah'])? 0 : floatval($income1_value['loan_activity_renting_wah']);
				$approve_rai = empty($income1_value['loan_activity_approve_rai'])? 0 : intval($income1_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income1_value['loan_activity_approve_ngan'])? 0 : intval($income1_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income1_value['loan_activity_approve_wah'])? 0 : floatval($income1_value['loan_activity_approve_wah']);

				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}

		if(!empty($loan_income_2))
		{
			foreach ($loan_income_2 as $income2_value)
			{
				$self_rai = empty($income2_value['loan_activity_self_rai'])? 0 : intval($income2_value['loan_activity_self_rai']);
				$self_ngan = empty($income2_value['loan_activity_self_ngan'])? 0 : intval($income2_value['loan_activity_self_ngan']);
				$self_wah = empty($income2_value['loan_activity_self_wah'])? 0 : floatval($income2_value['loan_activity_self_wah']);
				$renting_rai = empty($income2_value['loan_activity_renting_rai'])? 0 : intval($income2_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income2_value['loan_activity_renting_ngan'])? 0 : intval($income2_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income2_value['loan_activity_renting_wah'])? 0 : floatval($income2_value['loan_activity_renting_wah']);
				$approve_rai = empty($income2_value['loan_activity_approve_rai'])? 0 : intval($income2_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income2_value['loan_activity_approve_ngan'])? 0 : intval($income2_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income2_value['loan_activity_approve_wah'])? 0 : floatval($income2_value['loan_activity_approve_wah']);

				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}

		if(!empty($loan_income_3))
		{
			foreach ($loan_income_3 as $income3_value)
			{
				$self_rai = empty($income3_value['loan_activity_self_rai'])? 0 : intval($income3_value['loan_activity_self_rai']);
				$self_ngan = empty($income3_value['loan_activity_self_ngan'])? 0 : intval($income3_value['loan_activity_self_ngan']);
				$self_wah = empty($income3_value['loan_activity_self_wah'])? 0 : floatval($income3_value['loan_activity_self_wah']);
				$renting_rai = empty($income3_value['loan_activity_renting_rai'])? 0 : intval($income3_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income3_value['loan_activity_renting_ngan'])? 0 : intval($income3_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income3_value['loan_activity_renting_wah'])? 0 : floatval($income3_value['loan_activity_renting_wah']);
				$approve_rai = empty($income3_value['loan_activity_approve_rai'])? 0 : intval($income3_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income3_value['loan_activity_approve_ngan'])? 0 : intval($income3_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income3_value['loan_activity_approve_wah'])? 0 : floatval($income3_value['loan_activity_approve_wah']);

				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}

		$income_all = area($income_all['rai'], $income_all['ngan'], $income_all['wah']);
		$income_self = area($income_self['rai'], $income_self['ngan'], $income_self['wah']);
		$income_renting = area($income_renting['rai'], $income_renting['ngan'], $income_renting['wah']);
		$income_approve = area($income_approve['rai'], $income_approve['ngan'], $income_approve['wah']);

		$list_land_record =  $this->land_record->getByLoan($id);

		$check_guarantee = '( )';
		$check_guarantee_land = '( )';

		$i_assess = 1;
		$building_count = 0;
		$building_sum = 0;
		$record_sum = 0;
		$land_assess = array();
		$land_owner =  '';
		$land_main = '';
		$land_other = '';
		if(!empty($list_land_record))
		{
			foreach ($list_land_record as $key_land => $land_record)
			{
				$record_sum += floatval($land_record['land_record_staff_mortagage_price']);

				$check_main = $land_record['loan_guarantee_land_type']=='1';

				if($check_main)
				{
					$list_land_owner = $this->land_owner->getByLand($land_record['land_id']);

					if(!empty($list_land_owner))
					{
						foreach ($list_land_owner as $list_owner)
						{
							$land_owner .= $list_owner['title_name'].$list_owner['person_fname'].' '.$list_owner['person_lname'].', ';
						}
					}

					if($land_owner!='')
					{
						$land_owner = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						<td>ชื่อ – สกุลของเจ้าของที่ดิน '.substr($land_owner, 0, -2).'</td>
						</tr>
						</table>';
					}
				}
				else
				{
					$check_guarantee = '(/)';
					$check_guarantee_land = '(/)';
				}

				$list_land_assess = $this->land_assess->getByLand($land_record['land_id']);

				if(!empty($list_land_assess))
				{
					foreach ($list_land_assess as $list_assess)
					{
						if($check_main)
						{
							$land_main .= $i_assess.', ';
						}
						else
						{
							$land_other .= $i_assess.', ';
						}

						$land_assess[$i_assess++] = $list_assess;
					}
				}

				$building_record = $this->building_model->getByLand($land_record['land_id']);
				$list_land_record[$key_land]['building_record'] = $building_record;

				if(!empty($building_record))
				{
					foreach ($building_record as $list_building)
					{
						$building_amount = floatval($list_building['building_record_staff_assess_amount']);

						$record_sum += $building_amount;
						$building_sum += $building_amount;

						$building_count++;
					}
				}
			}
		}

		if($land_main != '') $land_main = substr($land_main, 0, -2);
		if($land_other != '') $land_other = substr($land_other, 0, -2);

		$sum_assess = 0;
		$html_assess = '';
		if(!empty($land_assess))
		{
			foreach ($land_assess as $key_assess => $list_assess)
			{
				$cal_assess = floatval($list_assess['land_assess_inspector_sum']);
				$sum_assess += floatval($cal_assess);

				$html_assess .= '<tr>
				<td align="center">'.num2Thai($key_assess).'</td>
				<td align="center">'.num2Thai($list_assess['land_assess_area_rai'].' - '.$list_assess['land_assess_area_ngan'].' - '.$list_assess['land_assess_area_wah']).'&nbsp;&nbsp;</td>
				<td align="right">'.num2Thai(number_format($list_assess['land_assess_price'], 2)).'&nbsp;&nbsp;</td>
				<td align="right">'.num2Thai(number_format(($list_assess['land_assess_price_pre_min'] + $list_assess['land_assess_price_pre_max']) / 2, 2)).'&nbsp;&nbsp;</td>
				<td align="right">'.num2Thai(number_format($list_assess['land_assess_price_basic'], 2)).'&nbsp;&nbsp;</td>
				<td align="right">'.num2Thai(number_format($cal_assess, 2)).'&nbsp;&nbsp;</td>
				<td align="right">-&nbsp;&nbsp;</td>
				</tr>';
			}
		}

		$sum_record = $sum_assess + $building_sum;

		$loan_guarantee_bond =  $this->loan_guarantee_bond->getByLoan($id);

		$check_guarantee_bond = '( )';
		$html_guarantee_bond = '';
		if(!empty($loan_guarantee_bond))
		{
			$check_guarantee = '(/)';
			$check_guarantee_bond = '(/)';

			$i_bond = 1;
			foreach ($loan_guarantee_bond as $list_bond)
			{
				$html_guarantee_bond .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="15%"></td>
				<td width="45%">('.num2Thai($i_bond++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bond['loan_bond_owner'].' เป็นผู้มีกรรมสิทธ์</td>
				<td width="40%">ชนิดพันธบัตร&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_type']).'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="30%">เลขที่ต้น&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_startnumber']).'</td>
				<td width="30%">เลขที่ท้าย&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_endnumber']).'</td>
				<td width="50%">จำนวนเงิน&nbsp;&nbsp;'.num2Thai(number_format($list_bond['loan_bond_amount'], 2)).'&nbsp;&nbsp;บาท</td>
				</tr>
				</table>';
			}
		}

		$loan_guarantee_bookbank =  $this->loan_guarantee_bookbank->getByLoan($id);

		$loan_bookbank_type = array(
				'1' => 'ออมทรัพย์',
				'2' => 'ฝากประจำ',
				'3' => 'เผื่อเรียก'
		);

		$check_guarantee_bookbank = '( )';
		$html_guarantee_bookbank = '';
		if(!empty($loan_guarantee_bookbank))
		{
			$check_guarantee = '(/)';
			$check_guarantee_bookbank = '(/)';

			$i_bookbank = 1;
			foreach ($loan_guarantee_bookbank as $list_bookbank)
			{
				$html_guarantee_bookbank .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="15%"></td>
				<td width="85%">('.num2Thai($i_bookbank++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bookbank['loan_bookbank_owner'].' เป็นเจ้าของบัญชีเงินฝาก</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="65%">ธนาคาร/สถาบันการเงิน/สหกรณ์&nbsp;&nbsp;'.num2Thai($list_bookbank['bank_name']).'</td>
				<td width="35%">สาขา&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_branch']).'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="30%">เลขที่บัญชี&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_no']).'</td>
				<td width="30%">ประเภทบัญชี&nbsp;&nbsp;'.num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]).'</td>
				<td width="40%">ยอดเงินฝาก ณ วันที่&nbsp;&nbsp;'.num2Thai(convert_dmy_th($list_bookbank['loan_bookbank_date'])).'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td>จำนวนเงิน&nbsp;&nbsp;'.num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)).'&nbsp;&nbsp;บาท</td>
				</tr>
				</table>';
			}
		}

		$loan_guarantee_bondsman =  $this->loan_guarantee_bondsman->getByLoan($id);

		$check_guarantee_bondsman = '( )';
		$html_guarantee_bondsman = '';
		if(!empty($loan_guarantee_bondsman))
		{
			$check_guarantee = '(/)';
			$check_guarantee_bondsman = '(/)';

			$i_bondsman = 1;
			foreach ($loan_guarantee_bondsman as $list_bondsman)
			{
				$age_bondsman = age($list_bondsman['person_birthdate']);

				$html_guarantee_bondsman .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="15%"></td>
				<td width="40%">('.num2Thai($i_bondsman++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'].'</td>
				<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($age_bondsman).'&nbsp;&nbsp;ปี</td>
				<td width="30%">หมายเลขโทรศัพท์&nbsp;&nbsp;'.(empty($list_bondsman['person_mobile'])? '-' : num2Thai($list_bondsman['person_mobile'])).'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td>ที่อยู่&nbsp;&nbsp;บ้านเลขที่ '.num2Thai($list_bondsman['person_addr_pre_no']).' หมู่ที่ '.(empty($list_bondsman['person_addr_pre_moo'])? '-' : num2Thai($list_bondsman['person_addr_pre_moo'])).' ถนน '.(empty($list_bondsman['person_addr_pre_road'])? '-' : num2Thai($list_bondsman['person_addr_pre_road'])).' ตำบล '.$list_bondsman['district_name'].' อำเภอ '.$list_bondsman['amphur_name'].' จังหวัด '.$list_bondsman['province_name'].'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="35%">อาชีพ&nbsp;&nbsp;'.num2Thai($list_bondsman['career_name']).'</td>
				<td width="30%">ตำแหน่ง&nbsp;&nbsp;'.(empty($list_bondsman['person_position'])? '-' : num2Thai($list_bondsman['person_position'])).'</td>
				<td width="35%">สังกัด&nbsp;&nbsp;'.(empty($list_bondsman['person_belong'])? '-' : num2Thai($list_bondsman['person_belong'])).'</td>
				</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td>อัตราเงินเดือน/ค่าจ้างเดือนละ&nbsp;&nbsp;'.num2Thai(number_format($list_bondsman['person_income_per_month'], 2)).'&nbsp;&nbsp;บาท</td>
				</tr>
				</table>';
			}
		}

		$html_expenditure = $this->expenditure($id, $id_analysis, 'html');

		$schedule = $this->loan_schedule->getByLoan($id);

		$html_schedule = '';
		$list_round =  array();
		if(!empty($schedule))
		{
			foreach ($schedule as $list_schedule)
			{
				$list_round[$list_schedule['loan_schedule_times']] = array(
						'principle' => floatval($list_schedule['loan_schedule_amount']),
						'other' => intval($list_schedule['loan_schedule_other']),
						'principle_other' => intval($list_schedule['loan_schedule_principle']),
						'interest_other' => intval($list_schedule['loan_schedule_interest'])
				);
			}
		}

		$income = floatval($analysis['loan_analysis_cooperative']);
		$amount = floatval($data['loan_amount']);
		$interest = intval($analysis['loan_analysis_interest']) / 100;
		$type = intval($analysis['loan_analysis_length']);
		$year = intval($analysis['loan_analysis_year']);
		$pay = $analysis['loan_analysis_pay'];
		$start = $analysis['loan_analysis_start'];

		$start = convert_ymd_en($start);
		$pay = convert_ymd_en($pay);

		$date_start = $start;
		$date_start = new DateTime($date_start);
		$date_start->add(new DateInterval('P1D'));
		$date_start = $date_start->format('Y-m-d');
		$date_end = $pay;

		$list_month = array();
		$remain = $amount;
		if(!empty($list_round))
		{
			foreach ($list_round as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				$interest_per = ($remain * $interest * $day_between) / 365;

				$list_round[$r_key] = array(
						'income' => $income,
						'principle' => $r_value['principle'],
						'remain' => $remain,
						'interest' => $interest_per,
						'other' => $r_value['other'],
						'principle_other' => $r_value['principle_other'],
						'interest_other' => $r_value['interest_other'],
						'date' => $date_end
				);

				$list_month[] = date('m', strtotime($date_end));

				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$type.'M'));
				$date_end = $date_end->format('Y-m-d');
			}

			$list_month = array_unique($list_month);

			$month = '';
			foreach ($list_month as $data_month)
			{
				$month .= thai_month($data_month).', ';
			}

			if(!empty($month)) $month = substr($month, 0, -2);

			$data_schedule = array(
					'year' => $year,
					'month' => $month,
					'income' => $income,
					'amount' => $amount,
					'interest' => $interest,
					'type' => $type,
					'pay' => $pay,
					'start' => $start,
					'fiscal_start' => fiscalYear($start),
					'fiscal_end' => fiscalYear($date_start),
					'list_round' => $list_round
			);

			$html_schedule = $this->parser->parse('cal_html', $data_schedule);
		}

		if(!empty($month)) $month = substr($month, 0, -2);

		$filename = 'analysis_'.date('Ymd');

		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
		$pdf->SetTitle($filename);//  กำหนด Title
		$pdf->SetSubject('Export receipt'); // กำหนด Subject
		$pdf->SetKeywords($filename); // กำหนด Keyword

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->SetMargins(10, 10, 10, true);
		$pdf->AddPage();

		$htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
		<td width="80%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๑๓</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 10px;"></td>
		</tr>
		</table>
		<hr />
		</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-weight: bold; text-align:center;">
		แบบสรุปผลการพิจารณาการขอสินเชื่อจากสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.)
		</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 10px;"></td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="60%">ผู้ขอสินเชื่อได้ยื่นความประสงค์ขอสินเชื่อ ณ&nbsp;&nbsp;'.num2Thai($data['loan_location']).'</td>
		<td width="35%">เมื่อวันที่&nbsp;&nbsp;'.num2Thai($data['loan_datefull']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>จึงขอเสนอผลการพิจารณาการขอสินเชื่อของผู้ขอสินเชื่อ ดังนี้</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="95%">๑.&nbsp;&nbsp;ข้อมูลทั่วไปของผู้ขอสินเชื่อ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="50%">๑.๑&nbsp;&nbsp;ชื่อ - สกุล&nbsp;&nbsp;'.$data['person']['title']['title_name'].$data['person']['person_fname'].' '.$data['person']['person_lname'].'</td>
		<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($data['person']['person_thaiid'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%">ที่อยู่ บ้านเลขที่&nbsp;&nbsp;'.num2Thai($data['person']['person_addr_pre_no']).'</td>
		<td width="10%">หมู่ที่&nbsp;&nbsp;'.(empty($data['person']['person_addr_pre_moo'])? '-' : num2Thai($data['person']['person_addr_pre_moo'])).'</td>
		<td width="25%">ตำบล&nbsp;&nbsp;'.$data['person']['district']['district_name'].'</td>
		<td width="25%">อำเภอ&nbsp;&nbsp;'.$data['person']['amphur']['amphur_name'].'</td>
		<td width="20%">จังหวัด&nbsp;&nbsp;'.$data['person']['province']['province_name'].'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="40%">ประกอบอาชีพ&nbsp;&nbsp;'.num2Thai($data['person']['career']['career_name']).'</td>
		<td width="30%">จำนวนสมาชิกในครัวเรือน&nbsp;&nbsp;'.num2Thai($data['loan_member_amount']).'&nbsp;&nbsp;คน</td>
		<td width="30%">จำนวนแรงงานในครัวเรือน&nbsp;&nbsp;'.num2Thai($data['loan_labor_amount']).'&nbsp;&nbsp;คน</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%">อายุของผู้ขอสินเชื่อ&nbsp;&nbsp;'.num2Thai($data['person']['person_age']).'&nbsp;&nbsp;ปี</td>
		</tr>
		</table>
		'.$html_loan_co.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๑.๒&nbsp;&nbsp;สถานภาพ 	'.$html_married.'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="60%">ชื่อ – สกุลของคู่สมรส (ถ้ามี)&nbsp;&nbsp;'.$spouse_name.'</td>
		<td width="25%">อายุ&nbsp;&nbsp;'.num2Thai($spouse_age).'&nbsp;&nbsp;ปี</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๑.๓&nbsp;&nbsp;'.($analysis['loan_analysis_egov']=='1'? '(/)' : '( )').' เป็นลูกค้า ธ.ก.ส. (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_egov_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">'.($analysis['loan_analysis_cooperative']=='1'? '(/)' : '( )').' เป็นสมาชิกสหกรณ์ (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_cooperative_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">'.($analysis['loan_analysis_bank']=='1'? '(/)' : '( )').' เป็นลูกค้าสถาบันการเงิน (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_bank_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">'.($analysis['loan_analysis_bankother']=='1'? '(/)' : '( )').' อื่นๆ (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_bankother_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="50%">๑.๔&nbsp;&nbsp;ผู้ขอสินเชื่อมีทรัพย์สินมูลค่ารวม&nbsp;&nbsp;'.num2Thai(number_format($asset_sum, 2)).' บาท</td>
		<td width="40%">และมีหนี้สินรวมจำนวน&nbsp;&nbsp;'.num2Thai(number_format($debt_sum, 2)).' บาท</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๑.๕&nbsp;&nbsp;ในปัจจุบันมีพื้นที่ทำการเกษตร เนื้อที่&nbsp;&nbsp;'.num2Thai($income_all['rai'].' - '.$income_all['ngan'].' - '.$income_all['wah']).'&nbsp;&nbsp;ไร่</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">โดยเป็นกรรมสิทธิ์ของตนเอง จำนวน&nbsp;&nbsp;'.num2Thai($income_self['rai'].' - '.$income_self['ngan'].' - '.$income_self['wah']).'&nbsp;&nbsp;ไร่</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">เช่าที่ดินผู้อื่น จำนวน&nbsp;&nbsp;'.num2Thai($income_renting['rai'].' - '.$income_renting['ngan'].' - '.$income_renting['wah']).'&nbsp;&nbsp;ไร่</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">ได้รับอนุญาตให้ทำประโยชน์ในที่ดินผู้อื่น จำนวน&nbsp;&nbsp;'.num2Thai($income_approve['rai'].' - '.$income_approve['ngan'].' - '.$income_approve['wah']).'&nbsp;&nbsp;ไร่</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๑.๖&nbsp;&nbsp;พฤติกรรมของผู้ขอสินเชื่อ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="32%">๑.๖.๑&nbsp;&nbsp;ความประพฤติ</td>
		<td width="5%">'.($analysis['loan_analysis_behavior']=='1'? '(/)' : '( )').' ดี</td>
		<td width="8%">'.($analysis['loan_analysis_behavior']!='1'? '(/)' : '( )').' ไม่ดี</td>
		<td width="40%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="32%">๑.๖.๒&nbsp;&nbsp;ความตั้งใจในการประกอบอาชีพ</td>
		<td width="5%">'.($analysis['loan_analysis_behavior_occupation']=='1'? '(/)' : '( )').' ดี</td>
		<td width="8%">'.($analysis['loan_analysis_behavior_occupation']!='1'? '(/)' : '( )').' ไม่ดี</td>
		<td width="40%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_occupation_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="52%">๑.๖.๓&nbsp;&nbsp;ความมุ่งมั่นตั้งใจในการใช้ประโยชน์ที่ดินเพื่อเกษตรกรรม</td>
		<td width="5%">'.($analysis['loan_analysis_behavior_farm']=='1'? '(/)' : '( )').' มี</td>
		<td width="8%">'.($analysis['loan_analysis_behavior_farm']!='1'? '(/)' : '( )').' ไม่มี</td>
		<td width="20%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_farm_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="15%">๑.๖.๔&nbsp;&nbsp;สุขภาพ</td>
		<td width="10%">'.($analysis['loan_analysis_behavior_health']=='1'? '(/)' : '( )').' แข็งแรง</td>
		<td width="15%">'.($analysis['loan_analysis_behavior_health']!='1'? '(/)' : '( )').' ไม่แข็งแรง</td>
		<td width="45%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_health_detail']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">๑.๖.๕&nbsp;&nbsp;อื่นๆ (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_other']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="95%">๒.&nbsp;&nbsp;จำนวนเงินที่ขอสินเชื่อ โดยมีวัตถุประสงค์เพื่อ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">'.$loan_type_objective1.' ไถ่ถอนที่ดินจากการจำนองหรือขายฝาก '.$loan_type_objective2.' ชำระหนี้ '.$loan_type_objective3.' ซื้อคืนที่ดิน จำนวน '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
		</tr>
		</table>
		<!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>และเพื่อประกอบอาชีพเกษตรกรรม จำนวน 	xxx บาท</td>
		</tr>
		</table> -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="95%">๓.&nbsp;&nbsp;มูลเหตุแห่งหนี้</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๓.๑&nbsp;&nbsp;ชื่อ – สกุลของเจ้าหนี้ หรือเจ้าหนี้ตามคำพิพากษา&nbsp;&nbsp;'.(empty($loan_creditor)? '-' : $loan_creditor).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>จำนวนเงินที่กู้ยืม หรือขายฝาก และได้รับจริง '.num2Thai(number_format($loan_real_amount, 2)).' บาท<!-- *ตามเอกสารการเป็นหนี้ (ระบุ) xxx จำนวนหนี้ตามเอกสาร xxx บาท -->กำหนดเวลาการชำระหนี้ ภายในวันที่ '.num2Thai($analysis['loan_analysis_deadline']).' ผลการไกล่เกลี่ยประนีประนอม มีจำนวนหนี้คงเหลือต้องชำระ '.num2Thai(number_format($analysis['loan_analysis_remain'], 2)).' บาท ภายในวันที่ '.num2Thai($analysis['loan_analysis_within']).' สาเหตุที่เป็นหนี้ (ให้อธิบายโดยละเอียดว่า เกิดจากสาเหตุใด เมื่อใด และผลเป็นประการใด) '.num2Thai($analysis['loan_analysis_cause']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๓.๒&nbsp;&nbsp;ผู้ขอสินเชื่อและคู่สมรส (ถ้ามี) มีหนี้สินรายอื่น ได้แก่ (เช่น ธ.ก.ส. สหกรณ์ สถาบันการเงิน หรือบุคคล เป็นต้น)</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>'.$debt_text.'จำนวน '.num2Thai(number_format($debt_sum, 2)).' บาท</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="95%">๔.&nbsp;&nbsp;หลักประกันคำขอสินเชื่อ (รายละเอียดตามแบบบันทึกการตรวจสอบที่ดิน)</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๔.๑&nbsp;&nbsp;การจำนองที่ดิน</td>
		</tr>
		</table>
		<table width="100%" border="1" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%" align="center">แปลงที่</td>
		<td width="12%" align="center">เนื้อที่ (ไร่)</td>
		<td width="18%" align="center">ราคาจดทะเบียนสิทธิ<br/>และนิติกรรม ต่อไร่</td>
		<td width="15%" align="center">ราคาซื้อขายเฉลี่ย<br/>ในปัจจุบันต่อไร่</td>
		<td width="15%" align="center">ราคาประเมินต่อไร่</td>
		<td width="15%" align="center">ราคาประเมิน<br/>ทั้งแปลง</td>
		<td width="20%" align="center">ราคาประเมินเป็นกี่เท่า<br/>ของจำนวนเงินกู้</td>
		</tr>
		'.$html_assess.'
		<tr>
		<td colspan="5" align="right">รวมทั้งสิ้น&nbsp;&nbsp;</td>
		<td align="right">'.num2Thai(number_format($sum_assess, 2)).'&nbsp;&nbsp;</td>
		<td align="right">'.num2Thai(number_format($sum_assess  / $data['loan_amount'], 2)).'&nbsp;&nbsp;</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="50%">มีสิ่งปลูกสร้าง จำนวน '.num2Thai($building_count).'  หลัง</td>
		<td width="50%">ราคาประเมิน '.num2Thai(number_format($building_sum,  2)).'  บาท</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>รวมราคาประเมินที่ดินและสิ่งปลูกสร้าง จำนวน '.num2Thai(number_format($sum_record, 2)).' บาท</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="50%">ราคาประเมินเป็นกี่เท่าของจำนวนเงินกู้ '.num2Thai(number_format($sum_record / $data['loan_amount'], 2)).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>หมายเหตุ : การจำนองที่ดินซึ่งไถ่ถอนจากเจ้าหนี้ ได้แก่ แปลงที่&nbsp;&nbsp;'.num2Thai($land_main).'</td>
		</tr>
		</table>
		'.$land_owner.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>และจำนองที่ดินเพิ่มเติม ได้แก่ แปลงที่&nbsp;&nbsp;'.num2Thai($land_other).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="100%" style="font-size: 5px;"></td>
		</tr>
		</table>
		<hr width="50%"/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td style="font-size: 14px;">* เอกสารการเป็นหนี้ ให้ระบุ เช่น ขายฝาก จำนอง สัญญากู้ยืม สัญญาซื้อคืน หรืออื่นๆ เป็นต้น</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๔.๒&nbsp;&nbsp;พันธบัตรรัฐบาล (ระบุชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์ของผู้จำนำซึ่งเป็นผู้มีกรรมสิทธิ์ รวมทั้งชนิด</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>พันธบัตร เลขที่ต้น เลขที่ท้าย หน้าทะเบียน ลงวันที่ จำนวนเงินตามพันธบัตรที่จำนำ) </td>
		</tr>
		</table>
		'.$html_guarantee_bond.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๔.๓&nbsp;&nbsp;เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ (ระบุ ชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>ของเจ้าของบัญชีเงินฝาก รวมทั้งบัญชีเงินฝากของธนาคาร สถาบันการเงิน หรือสหกรณ์ ประเภทของเงินฝาก เลขที่บัญชีเงินฝาก ยอดเงินคงเหลือจำนวนเท่าใด และเมื่อใด)</td>
		</tr>
		</table>
		'.$html_guarantee_bookbank.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๔.๔&nbsp;&nbsp;การค้ำประกันด้วยบุคคล (ระบุชื่อ - สกุล อายุ ที่อยู่ หมายเลขโทรศัพท์ อาชีพ ตำแหน่ง สังกัด อัตราเงินเดือน/</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>ค่าจ้างเดือนละเท่าใด และความน่าเชื่อถือ พร้อมทั้งแนบสำเนาบัตรประชาชน ทะเบียนบ้าน หนังสือรับรองจากต้นสังกัด หรือหนังสือแสดงรายได้ โดยระบุตำแหน่ง ระดับ อัตราเงินเดือน และหากผู้ค้ำประกันมีการทำสัญญาค้ำประกันรายอื่นด้วย ให้แจ้งภาระผูกพันดังกล่าวด้วย)</td>
		</tr>
		</table>
		'.$html_guarantee_bondsman.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="95%">๕.&nbsp;&nbsp;รายได้ ค่าใช้จ่าย และแผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๕.๑&nbsp;&nbsp;รายได้ และค่าใช้จ่ายของผู้กู้ (รายละเอียดตามข้อมูลของผู้ขอสินเชื่อรายที่แนบ)</td>
		</tr>
		</table>
		'.$html_expenditure.'
		<!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๕.๒&nbsp;&nbsp;แผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ (กรณีขอสินเชื่อเพื่อไปประกอบอาชีพเกษตรกรรมของผู้ขอ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>สินเชื่อว่ามีแผนการผลิต แหล่งเงินทุนที่ใช้ในการผลิต และการตลาด อย่างไร)</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td></td>
		</tr>
		</table> -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="5%"></td>
		<td width="95%">๖.&nbsp;&nbsp;ประมาณการกระแสเงินสดและแผนการชำระคืนของผู้ขอสินเชื่อ</td>
		</tr>
		</table>
		'.$html_schedule.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๗.&nbsp;&nbsp;สรุปความเห็นในการพิจารณาอนุมัติคำขอสินเชื่อของผู้สอบสวน</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">๗.๑&nbsp;&nbsp;พิจารณาตามหลักเกณฑ์ของ บจธ.</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">ความเห็น '.(empty($analysis['loan_analysis_investigate_comment'])? '-' : num2Thai($analysis['loan_analysis_investigate_comment'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">๗.๒&nbsp;&nbsp;มูลเหตุแห่งหนี้</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">๗.๒.๑&nbsp;&nbsp;สุจริตหรือไม่สุจริต ดังนี้</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">'.(empty($analysis['loan_analysis_investigate_honestly'])? '-' : num2Thai($analysis['loan_analysis_investigate_honestly'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">๗.๒.๒&nbsp;&nbsp;กรณีระยะเวลาในการก่อหนี้สินเพิ่งจะเกิดขึ้นไม่ถึงหนึ่งปี มีเหตุผลที่น่าเชื่อว่าหนี้นั้นเกิดขึ้น</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>โดยสุจริต ดังนี้</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">'.(empty($analysis['loan_analysis_investigate_cause'])? '-' : num2Thai($analysis['loan_analysis_investigate_cause'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">๗.๒.๓&nbsp;&nbsp;ความจำเป็นหรือไม่มีความจำเป็น ดังนี้ </td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">'.(empty($analysis['loan_analysis_investigate_must'])? '-' : num2Thai($analysis['loan_analysis_investigate_must'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">๗.๓&nbsp;&nbsp;กรณีที่ดินแปลงซึ่งขอสินเชื่อเพื่อไถ่ถอนหรือซื้อคืนมีมูลค่าน้อยกว่าจำนวนเงินที่ขอสินเชื่อ การอนุมัติ</td>
		</tr>
		</table>
		<!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>คำขอสินเชื่อครั้งนี้จะเป็นประโยชน์ต่อผู้ขอสินเชื่ออย่างไร</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">'.(empty($analysis['loan_analysis_investigate_matter'])? '-' : num2Thai($analysis['loan_analysis_investigate_matter'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">'.$check_guarantee.' ผู้ขอสินเชื่อได้เสนอหลักประกันเพิ่มเติม</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">'.$check_guarantee_land.' ที่ดิน</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">'.$check_guarantee_bond.' พันธบัตรรัฐบาล</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">'.$check_guarantee_bookbank.' เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">'.($check_guarantee=='( )'? '(/)' : '( )').'	ผู้ขอสินเชื่อไม่สามารถหาหลักประกันเพิ่มเติมได้</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">๗.๔&nbsp;&nbsp;สรุปความเห็นและผลการพิจารณาเกี่ยวกับการขอสินเชื่อตามคำขอ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">๗.๔.๑&nbsp;&nbsp;'.($analysis['loan_analysis_approve_status']=='1'? '(/)' : '( )').' เห็นสมควรอนุมัติในวงเงินสินเชื่อ '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">กำหนดชำระคืนเสร็จเรียบร้อยภายในวันที่ '.num2Thai($analysis['loan_analysis_approve_date']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">เงื่อนไขหรือคำชี้แจงเพิ่มเติม</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="25%"></td>
		<td width="75%">'.(empty($analysis['loan_analysis_approve_conditions'])? '-' : num2Thai($analysis['loan_analysis_approve_conditions'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">๗.๔.๒&nbsp;&nbsp;'.($analysis['loan_analysis_approve_status']!='1'? '(/)' : '( )').' เห็นสมควรไม่อนุมัติ</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="26%"></td>
		<td width="74%">'.($analysis['loan_analysis_approve_not_type']=='1' && $analysis['loan_analysis_approve_status']!='1'? '(/)' : '( )').' อยู่ในหลักเกณฑ์ให้สินเชื่อจาก บจธ. ปี '.(empty($analysis['loan_analysis_approve_not_year'])? '-' : num2Thai($analysis['loan_analysis_approve_not_year'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="26%"></td>
		<td width="74%">เนื่องจาก '.(empty($analysis['loan_analysis_approve_not_case'])? '-' : num2Thai($analysis['loan_analysis_approve_not_case'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="26%"></td>
		<td width="74%">'.($analysis['loan_analysis_approve_not_type']!='1' && $analysis['loan_analysis_approve_status']!='1'? '(/)' : '( )').' ไม่อยู่ในหลักเกณฑ์ให้สินเชื่อจาก บจธ. ปี '.(empty($analysis['loan_analysis_approve_not_year'])? '-' : num2Thai($analysis['loan_analysis_approve_not_year'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="26%"></td>
		<td width="74%">เนื่องจาก '.(empty($analysis['loan_analysis_approve_not_case'])? '-' : num2Thai($analysis['loan_analysis_approve_not_case'])).'</td>
		</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="30%" style="text-align:center;"></td>
		<td width="70%" style="text-align:center;">
		ลงชื่อ ......................................................<br/>
		(...........................................................)<br/>
		ตำแหน่ง ................................................<br/>
		วันที่ .........................................
		</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="10%"></td>
		<td width="90%">๘.&nbsp;&nbsp;สรุปความเห็นในการพิจารณาคำขอสินเชื่อของฝ่าย/กอง</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">'.($analysis['loan_analysis_approve_sector_status']=='1'? '(/)' : '( )').' อนุมัติในวงเงินสินเชื่อ '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">กำหนดชำระคืนเสร็จเรียบร้อยภายในวันที่ '.num2Thai($analysis['loan_analysis_approve_sector_date']).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">เงื่อนไขหรือคำชี้แจงเพิ่มเติม</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="20%"></td>
		<td width="80%">'.(empty($analysis['loan_analysis_approve_sector_conditions'])? '-' : num2Thai($analysis['loan_analysis_approve_sector_conditions'])).'</td>
		</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15%"></td>
		<td width="85%">'.($analysis['loan_analysis_approve_sector_status']!='1'? '(/)' : '( )').'	ไม่อนุมัติ เนื่องจาก '.(empty($analysis['loan_analysis_approve_sector_not_case'])? '-' : num2Thai($analysis['loan_analysis_approve_sector_not_case'])).'</td>
		</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="30%" style="text-align:center;"></td>
		<td width="70%" style="text-align:center;">
		ลงชื่อ ......................................................<br/>
		(...........................................................)<br/>
		ตำแหน่ง ................................................<br/>
		วันที่ .........................................
		</td>
		</tr>
		</table> -->';

		$pdf->SetFont('thsarabun', '', 16);
		$pdf->writeHTML($htmlcontent, true, 0, true, true);

		$pdf->Output($filename.'.pdf', 'I');
	}

	public function check()
	{
		echo '<pre>';
		print_r($_SESSION['loan_analysis']);
		echo '</pre>';
	}
}
?>
