<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analysis extends MY_Controller
{
	private $title_page = 'พิจารณาการขอสินเชื่อ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('loan_analysis_model', 'loan_analysis');
		
		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');
		
		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->loan_analysis->search($loan, $thaiid, $fullname)
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_analysis/analysis.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('analysis', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function search()
	{
		$this->load->model('summary/loan_summary_model', 'loan_summary');
		
		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title_page => site_url('analysis')
			);
		
		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->loan_summary->searchNonAnalysis($loan, $thaiid, $fullname)
		);
		
		$data = array(
				'title' => 'เพิ่มแบบพิจารณา',
				'js_other' => array(),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('search', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($type, $id, $id_analysis='')
	{
		$this->load->model('title/title_model', 'title');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('career/career_model', 'career');
		$this->load->model('person/person_model', 'person');
		$this->load->model('loan_model', 'loan');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('summary/loan_summary_model', 'loan_summary');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_schedule_model', 'loan_schedule');
		
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title_page => site_url('analysis')
		);
		
		$data_content = array(
				'title' => $this->title->all(),
				'province' => $this->province->all(),
				'amphur' => array(),
				'district' => array(),
				'married' =>  $this->married->all(),
				'loan' =>  $this->loan->getById($id),
				'loan_asset' =>  $this->loan_asset->getByLoan($id),
				'loan_debt' =>  $this->loan_debt->getByLoan($id),
				'loan_co' =>  $this->loan_co->getByLoan($id),
				'land_record' =>  $this->land_record->getByLoan($id),
				'loan_guarantee_bond' =>  $this->loan_guarantee_bond->getByLoan($id),
				'loan_guarantee_bookbank' =>  $this->loan_guarantee_bookbank->getByLoan($id),
				'loan_guarantee_bondsman' =>  $this->loan_guarantee_bondsman->getByLoan($id),
				'loan_summary' =>  $this->loan_summary->getByLoan($id)
		);
		
		$data_content['loan']['loan_date'] = convert_dmy_th($data_content['loan']['loan_date']);
		$data_content['loan']['person']['title'] = $this->title->getById($data_content['loan']['person']['title_id']);
		$data_content['loan']['person']['person_age'] = empty($data_content['loan']['person']['person_birthdate'])? '' : age($data_content['loan']['person']['person_birthdate']);
		$data_content['loan']['person']['married'] = $this->married->getById($data_content['loan']['person']['status_married_id']);
		$data_content['loan']['person']['district'] = $this->district->getById($data_content['loan']['person']['person_addr_pre_district_id']);
		$data_content['loan']['person']['amphur'] = $this->amphur->getById($data_content['loan']['person']['person_addr_pre_amphur_id']);
		$data_content['loan']['person']['province'] = $this->province->getById($data_content['loan']['person']['person_addr_pre_province_id']);
		$data_content['loan']['person']['career'] = $this->career->getById($data_content['loan']['person']['career_id']);
		
		$loan_type = $this->loan_type->getByLoan($id);
		
		$data_content['loan_type_objective'] = '';
		$data_content['loan_creditor'] = '';
		$data_content['loan_real_amount'] = 0;
		$data_content['loan_type_farm_for'] = '';
		$data_content['check_type_farm'] = false;
		$case = '';
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$data_content['loan_type_objective'] = '1';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_redeem_amount']);
					
					$case .= $list_type['loan_type_redeem_case'].'<br/>';
					
					if(!empty($list_type['loan_type_redeem_owner']))
					{
						$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$data_content['loan_type_objective'] = '2';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_contract_amount']);
					
					$case .= $list_type['loan_type_contract_case'].'<br/>';
					
					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$data_content['loan_type_objective'] = '2';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_case_amount']);
					
					$case .= $list_type['loan_type_case_case'].'<br/>';
					
					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$data_content['loan_type_objective'] = '3';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_sale_amount']);
				}
				else
				{
					$data_content['check_type_farm'] = true;
					$data_content['loan_type_farm_for'] = $list_type['loan_type_farm_for'];
				}
			}
		}
		
		if($case!='') $case = substr($case, 0, -5);
		
		$data_content['loan_income_1'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '1');
		$data_content['loan_income_2'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '2');
		$data_content['loan_income_3'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '3');
		
		$data_content['income_all']['rai'] = 0;
		$data_content['income_all']['ngan'] = 0;
		$data_content['income_all']['wah'] = 0;
		$data_content['income_self']['rai'] = 0;
		$data_content['income_self']['ngan'] = 0;
		$data_content['income_self']['wah'] = 0;
		$data_content['income_renting']['rai'] = 0;
		$data_content['income_renting']['ngan'] = 0;
		$data_content['income_renting']['wah'] = 0;
		$data_content['income_approve']['rai'] = 0;
		$data_content['income_approve']['ngan'] = 0;
		$data_content['income_approve']['wah'] = 0;
		
		if(!empty($data_content['loan_income_1']))
		{
			foreach ($data_content['loan_income_1'] as $income1_value)
			{
				$self_rai = empty($income1_value['loan_activity_self_rai'])? 0 : intval($income1_value['loan_activity_self_rai']);
				$self_ngan = empty($income1_value['loan_activity_self_ngan'])? 0 : intval($income1_value['loan_activity_self_ngan']);
				$self_wah = empty($income1_value['loan_activity_self_wah'])? 0 : intval($income1_value['loan_activity_self_wah']);
				$renting_rai = empty($income1_value['loan_activity_renting_rai'])? 0 : intval($income1_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income1_value['loan_activity_renting_ngan'])? 0 : intval($income1_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income1_value['loan_activity_renting_wah'])? 0 : intval($income1_value['loan_activity_renting_wah']);
				$approve_rai = empty($income1_value['loan_activity_approve_rai'])? 0 : intval($income1_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income1_value['loan_activity_approve_ngan'])? 0 : intval($income1_value['loan_activity_approve_ngan']); 
				$approve_wah = empty($income1_value['loan_activity_approve_wah'])? 0 : intval($income1_value['loan_activity_approve_wah']);
				
				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}
		
		if(!empty($data_content['loan_income_2']))
		{
			foreach ($data_content['loan_income_2'] as $income2_value)
			{
				$self_rai = empty($income2_value['loan_activity_self_rai'])? 0 : intval($income2_value['loan_activity_self_rai']);
				$self_ngan = empty($income2_value['loan_activity_self_ngan'])? 0 : intval($income2_value['loan_activity_self_ngan']);
				$self_wah = empty($income2_value['loan_activity_self_wah'])? 0 : intval($income2_value['loan_activity_self_wah']);
				$renting_rai = empty($income2_value['loan_activity_renting_rai'])? 0 : intval($income2_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income2_value['loan_activity_renting_ngan'])? 0 : intval($income2_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income2_value['loan_activity_renting_wah'])? 0 : intval($income2_value['loan_activity_renting_wah']);
				$approve_rai = empty($income2_value['loan_activity_approve_rai'])? 0 : intval($income2_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income2_value['loan_activity_approve_ngan'])? 0 : intval($income2_value['loan_activity_approve_ngan']); 
				$approve_wah = empty($income2_value['loan_activity_approve_wah'])? 0 : intval($income2_value['loan_activity_approve_wah']);
		
				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}
		
		if(!empty($data_content['loan_income_3']))
		{
			foreach ($data_content['loan_income_3'] as $income3_value)
			{
				$self_rai = empty($income3_value['loan_activity_self_rai'])? 0 : intval($income3_value['loan_activity_self_rai']);
				$self_ngan = empty($income3_value['loan_activity_self_ngan'])? 0 : intval($income3_value['loan_activity_self_ngan']);
				$self_wah = empty($income3_value['loan_activity_self_wah'])? 0 : intval($income3_value['loan_activity_self_wah']);
				$renting_rai = empty($income3_value['loan_activity_renting_rai'])? 0 : intval($income3_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income3_value['loan_activity_renting_ngan'])? 0 : intval($income3_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income3_value['loan_activity_renting_wah'])? 0 : intval($income3_value['loan_activity_renting_wah']);
				$approve_rai = empty($income3_value['loan_activity_approve_rai'])? 0 : intval($income3_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income3_value['loan_activity_approve_ngan'])? 0 : intval($income3_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income3_value['loan_activity_approve_wah'])? 0 : intval($income3_value['loan_activity_approve_wah']);
		
				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}
		
		$income_all = area($data_content['income_all']['rai'], $data_content['income_all']['ngan'], $data_content['income_all']['wah']);
		$data_content['income_all']['rai'] = $income_all['rai'];
		$data_content['income_all']['ngan'] = $income_all['ngan'];
		$data_content['income_all']['wah'] = $income_all['wah'];
		
		$income_self = area($data_content['income_self']['rai'], $data_content['income_self']['ngan'], $data_content['income_self']['wah']);
		$data_content['income_self']['rai'] = $income_self['rai'];
		$data_content['income_self']['ngan'] = $income_self['ngan'];
		$data_content['income_self']['wah'] = $income_self['wah'];
		
		$income_renting = area($data_content['income_renting']['rai'], $data_content['income_renting']['ngan'], $data_content['income_renting']['wah']);
		$data_content['income_renting']['rai'] = $income_renting['rai'];
		$data_content['income_renting']['ngan'] = $income_renting['ngan'];
		$data_content['income_renting']['wah'] = $income_renting['wah'];
		
		$income_approve = area($data_content['income_approve']['rai'], $data_content['income_approve']['ngan'], $data_content['income_approve']['wah']);
		$data_content['income_approve']['rai'] = $income_approve['rai'];
		$data_content['income_approve']['ngan'] = $income_approve['ngan'];
		$data_content['income_approve']['wah'] = $income_approve['wah'];
		
		$i_assess = 1;
		$data_content['building_count'] = 0;
		$data_content['building_sum'] = 0;
		$data_content['record_sum'] = 0;
		$data_content['land_assess'] = array();
		$data_content['land_main'] = '';
		$data_content['land_other'] = '';
		if(!empty($data_content['land_record']))
		{
			foreach ($data_content['land_record'] as $key_land => $land_record)
			{
				$data_content['record_sum'] += floatval($land_record['land_record_staff_mortagage_price']);
				
				$check_main = $land_record['loan_guarantee_land_type']=='1';
				
				$land_assess = $this->land_assess->getByLand($land_record['land_id']);
				
				if(!empty($land_assess))
				{
					foreach ($land_assess as $list_assess)
					{
						if($check_main)
						{
							$data_content['land_main'] .= $i_assess.', ';
						}
						else
						{
							$data_content['land_other'] .= $i_assess.', ';
						}
						
						$data_content['land_assess'][$i_assess++] = $list_assess;
					}
				}
				
				$building_record = $this->building_record->getByLand($land_record['land_id']);
				$data_content['land_record'][$key_land]['building_record'] = $building_record;
				
				if(!empty($building_record))
				{
					foreach ($building_record as $list_building)
					{
						$building_amount = floatval($list_building['building_record_staff_assess_amount']);
						
						$data_content['record_sum'] += $building_amount;
						$data_content['building_sum'] += $building_amount;
						
						$data_content['building_count']++;
					}
				}
			}
		}
		
		if($data_content['land_main'] != '') $data_content['land_main'] = substr($data_content['land_main'], 0, -2);
		if($data_content['land_other'] != '') $data_content['land_other'] = substr($data_content['land_other'], 0, -2);
		
		if(empty($data_content['loan']['loan_spouse']))
		{
			$data_content['loan']['spouse_name'] = '';
			$data_content['loan']['spouse_age'] = '';
		}
		else
		{
			$data_content['loan']['spouse'] = $this->person->getById($data_content['loan']['loan_spouse']);
			$data_content['loan']['spouse']['title'] = $this->title->getById($data_content['loan']['spouse']['title_id']);
			$data_content['loan']['spouse_name'] = $data_content['loan']['spouse']['title']['title_name'].$data_content['loan']['spouse']['person_fname'].'  '.$data_content['loan']['spouse']['person_lname'];
			$data_content['loan']['spouse_age'] = empty($data_content['loan']['spouse']['person_birthdate'])? '' : age($data_content['loan']['spouse']['person_birthdate']);
		}
		
		$data_content['asset_sum'] = 0;
		if(!empty($data_content['loan_asset']))
		{
			foreach ($data_content['loan_asset'] as $list_asset) 
			{
				$data_content['asset_sum'] += floatval($list_asset['loan_asset_amount']);
			}
		}
		
		$data_content['debt_sum'] = 0;
		if(!empty($data_content['loan_debt']))
		{
			foreach ($data_content['loan_debt'] as $list_debt)
			{
				$data_content['debt_sum'] += floatval($list_debt['loan_debt_amount']);
			}
		}
		
		if($type=='add')
		{
			$title_page = 'เพิ่มแบบพิจารณา';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->loan_analysis->getModel();
			$data_content['data']['loan_id'] = $id;
			$data_content['data']['loan_analysis_id'] = $id_analysis;
			
			$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
		}
		else
		{
			$data_content['data'] = $this->loan_analysis->getById($id_analysis);
			
			if($data_content['data']['loan_analysis_cause']=='') $data_content['data']['loan_analysis_cause'] = $case;
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มแบบพิจารณา';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->loan_analysis->getModel();
				$data_content['data']['loan_id'] = $id;
				$data_content['data']['loan_analysis_id'] = $id_analysis;
				$data_content['data']['loan_analysis_remain'] = $data_content['loan']['loan_amount'];
				
				$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
			}
			else
			{
				$title_page = 'แก้ไขแบบพิจารณา';
				
				$data_content['data']['loan_id'] = $id;
				$data_content['data']['loan_analysis_id'] = $id_analysis;
				
				$schedule = $this->loan_schedule->getByLoan($id);
				
				if(!empty($schedule))
				{
					$list_round =  array();
					foreach ($schedule as $list_schedule)
					{
						$list_round[$list_schedule['loan_schedule_times']] = array(
								'principle' => floatval($list_schedule['loan_schedule_amount']),
								'other' => intval($list_schedule['loan_schedule_other']),
								'principle_other' => intval($list_schedule['loan_schedule_principle']),
								'interest_other' => intval($list_schedule['loan_schedule_interest'])
						);
					}
					
					$_SESSION['loan_analysis_cal'] = $list_round;
				}
				else
				{
					$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
				}
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_analysis/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function expenditure($id, $id_analysis='', $type='')
	{
		$this->load->model('loan_model', 'loan');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_income_model', 'loan_income');
		
		$loan =  $this->loan->getById($id);
		$person[] = $loan['person_id'];
		
		$loan_co =  $this->loan_co->getByLoan($id);
		
		if(!empty($loan_co))
		{
			foreach ($loan_co as $list_co)
			{
				$person[] = $list_co['person_id'];
			}
		}
		
		$data_content['loan_income_1'] = $this->loan_income->getByPersonGroup($person, '1');
		$data_content['loan_income_2'] = $this->loan_income->getByPersonGroup($person, '2');
		$data_content['loan_income_3'] = $this->loan_income->getByPersonGroup($person, '3');
		$data_content['loan_expenditure_1'] = $this->loan_expenditure->getByPersonGroup($person, '1');
		$data_content['loan_expenditure_2'] = $this->loan_expenditure->getByPersonGroup($person, '2');
		
		if($type=='html')
		{
			return $this->parser->parse('expenditure_html', $data_content);
		}
		else
		{
			$this->parser->parse('expenditure', $data_content);
		}
	}
	
	public function cal_ana_add($amount, $type, $year)
	{
		$amount = floatval($amount);
		$type = intval($type);
		$year = intval($year);
		
		if($type==1)
		{
			$round = $year * 12;
			$income = $income / 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
			$income = $income / 2;
		}
		else
		{
			$round = $year;
		}
		
		$principle = $amount / $round;
		
		$list_round = array();
		for ($r=1; $r<=$round; $r++)
		{
			$list_round[$r] = array(
					'principle' => $principle,
					'other' => 0,
					'principle_other' => 0,
					'interest_other' => 0
			);
		}
		
		$_SESSION['loan_analysis_cal'] = $list_round;
	}
	
	public function cal_ana()
	{
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
		
		if($type==1)
		{
			$round = $year * 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
		}
		else
		{
			$round = $year;
		}
		
		$principle = $amount / $round;
		
		$data = array(
			'round' => $round,
			'principle' => $principle
		);
		
		$this->parser->parse('cal_ana', $data);
	}
	
	public function save_cal()
	{
		$round = $this->input->post('txtCalRound');
		$amount = $this->input->post('txtCalAmount');
		$other = $this->input->post('txtCalOther');
		$principle_other = $this->input->post('txtCalPrinciple');
		$interest_other = $this->input->post('txtCalInterest');
		
		$list_round = array();
		foreach ($round as $r_key => $r_value)
		{
			$list_round[$r_value] = array(
						'principle' => $amount[$r_key],
						'other' => $other[$r_key],
						'principle_other' => $principle_other[$r_key],
						'interest_other' => $interest_other[$r_key]
					);
		}
		
		$_SESSION['loan_analysis_cal'] = $list_round;
	}
	
	public function cal_ana2()
	{
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
	
		if($type==1)
		{
			$round = $year * 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
		}
		else
		{
			$round = $year;
		}
	
		$principle = $amount / $round;
	
		$data = array(
				'round' => $round,
				'principle' => $principle
		);
	
		$this->parser->parse('cal_ana', $data);
	}
	
	public function cal()
	{
		$income = floatval(str_replace(",", "", $this->input->post('income')));
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$interest = intval($this->input->post('interest')) / 100;
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
		$pay = $this->input->post('pay');
		$start = $this->input->post('start');
		
		$start = convert_ymd_en($start);
		$pay = convert_ymd_en($pay);
		
		$date_start = $start;
		$date_start = new DateTime($date_start);
		$date_start->sub(new DateInterval('P1D'));
		$date_start = $date_start->format('Y-m-d');
		$date_end = $pay;
		
		$list_round = array();
		$list_month = array();
		$remain = $amount;
		if(!empty($_SESSION['loan_analysis_cal']))
		{
			foreach ($_SESSION['loan_analysis_cal'] as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				$interest_per = ($remain * $interest * $day_between) / 365;
					
				$list_round[$r_key] = array(
						'income' => $income,
						'principle' => $r_value['principle'],
						'remain' => $remain,
						'interest' => $interest_per,
						'other' => $r_value['other'],
						'principle_other' => $r_value['principle_other'],
						'interest_other' => $r_value['interest_other'],
						'date' => $date_end
				);
					
				$list_month[] = date('m', strtotime($date_end));
					
				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$type.'M'));
				$date_end = $date_end->format('Y-m-d');
			}
		}
		
		$list_month = array_unique($list_month);
		
		$month = '';
		foreach ($list_month as $data_month)
		{
			$month .= thai_month($data_month).', ';
		}
		
		if(!empty($month)) $month = substr($month, 0, -2);
		
		$data = array(
			'year' => $year,
			'month' => $month,
			'income' => $income,
			'amount' => $amount,
			'interest' => $interest,
			'type' => $type,
			'pay' => $pay,
			'start' => $start,
			'fiscal_start' => fiscalYear($start),
			'fiscal_end' => fiscalYear($date_start),
			'list_round' => $list_round
		);
		
		$_SESSION['loan_analysis'] = $data;
		
		$this->parser->parse('cal', $data);
	}
	
	public function save_expenditure()
	{
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		
		$array['loan_expenditure_amount'.$this->input->post('year')] = forNumberInt($this->input->post('amount'));
		
		$result = $this->loan_expenditure->save($array, $this->input->post('id'));
	}
	
	public function save()
	{
		$this->load->model('loan_analysis_model', 'loan_analysis');
		
		$chkEgov = empty($this->input->post('chkEgov'))? '0' : '1';
		$chkCooperative = empty($this->input->post('chkCooperative'))? '0' : '1';
		$chkBank = empty($this->input->post('chkBank'))? '0' : '1';
		$chkBankOther = empty($this->input->post('chkBankOther'))? '0' : '1';
		
		$array['loan_id'] = $this->input->post('txtLoan');
		$array['loan_analysis_egov'] = $chkEgov;
		$array['loan_analysis_egov_detail'] = $this->input->post('txtEgov');
		$array['loan_analysis_cooperative'] = $chkCooperative;
		$array['loan_analysis_cooperative_detail'] = $this->input->post('txtCooperative');
		$array['loan_analysis_bank'] = $chkBank;
		$array['loan_analysis_bank_detail'] = $this->input->post('txtBank');
		$array['loan_analysis_bankother'] = $chkBankOther;
		$array['loan_analysis_bankother_detail'] = $this->input->post('txtBankOther');
		$array['loan_analysis_behavior'] = $this->input->post('rdBehavior');
		$array['loan_analysis_behavior_detail'] = $this->input->post('txtBehavior');
		$array['loan_analysis_behavior_occupation'] = $this->input->post('rdBehaviorOccupation');
		$array['loan_analysis_behavior_occupation_detail'] = $this->input->post('txtBehaviorOccupation');
		$array['loan_analysis_behavior_farm'] = $this->input->post('rdBehaviorFarm');
		$array['loan_analysis_behavior_farm_detail'] = $this->input->post('txtBehaviorFarm');
		$array['loan_analysis_behavior_health'] = $this->input->post('rdBehaviorHealth');
		$array['loan_analysis_behavior_health_detail'] = $this->input->post('txtBehaviorHealth');
		$array['loan_analysis_behavior_other'] = $this->input->post('txtBehaviorOther');
		$array['loan_analysis_deadline'] = convert_ymd_en($this->input->post('txtDeadline'));
		$array['loan_analysis_remain'] = forNumberInt($this->input->post('txtRemain'));
		$array['loan_analysis_within'] = convert_ymd_en($this->input->post('txtWithin'));
		$array['loan_analysis_cause'] = $this->input->post('txtCause');
		$array['loan_analysis_interest'] = $this->input->post('ddlCalInterest');
		$array['loan_analysis_length'] = $this->input->post('ddlCalType');
		$array['loan_analysis_year'] = $this->input->post('txtShowYear');
		$array['loan_analysis_start'] = convert_ymd_en($this->input->post('txtCalStart'));
		$array['loan_analysis_pay'] = convert_ymd_en($this->input->post('txtCalPay'));
		$array['loan_analysis_amount'] = forNumberInt($this->input->post('txtCalAmount'));
		$array['loan_analysis_investigate_comment'] = $this->input->post('txtInvestigateComment');
		$array['loan_analysis_investigate_honestly'] = $this->input->post('txtInvestigateHonestly');
		$array['loan_analysis_investigate_cause'] = $this->input->post('txtInvestigateCause');
		$array['loan_analysis_investigate_must'] = $this->input->post('txtInvestigateMust');
		$array['loan_analysis_investigate_matter'] = $this->input->post('txtInvestigateMatter');
		$array['loan_analysis_approve_status'] = $this->input->post('rdApproveStatus');
		$array['loan_analysis_approve_amount'] = forNumberInt($this->input->post('txtApproveAmount'));
		$array['loan_analysis_approve_date'] = convert_ymd_en($this->input->post('txtApproveDate'));
		$array['loan_analysis_approve_conditions'] = $this->input->post('txtApproveConditions');
		$array['loan_analysis_approve_not_type'] = $this->input->post('rdApproveNotType');
		$array['loan_analysis_approve_not_year'] = $this->input->post('txtApproveNotYear');
		$array['loan_analysis_approve_not_case'] = $this->input->post('txtApproveNotCase');
		$array['loan_analysis_approve_sector_status'] = $this->input->post('rdApproveSectorStatus');
		$array['loan_analysis_approve_sector_amount'] = forNumberInt($this->input->post('txtApproveSectorAmount'));
		$array['loan_analysis_approve_sector_date'] = convert_ymd_en($this->input->post('txtApproveSectorDate'));
		$array['loan_analysis_approve_sector_conditions'] = $this->input->post('txtApproveSectorConditions');
		$array['loan_analysis_approve_sector_not_case'] = $this->input->post('txtApproveSectorNotCase');
		$array['loan_analysis_income'] = $this->input->post('txtYear'.$this->input->post('ddlCalIncomeYear').'Sum');
		$array['loan_analysis_income_year'] = $this->input->post('ddlCalIncomeYear');
		$array['loan_analysis_status'] = '1';
		
		$id = $this->loan_analysis->save($array, $this->input->post('txtId'));
		
		if($this->input->post('txtId')=='')
		{
			$data = array(
					'alert' => 'บันทึกแบบประเมินเรียบร้อยแล้ว',
					'link' => site_url('analysis'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขแบบประเมินเรียบร้อยแล้ว',
					'link' => site_url('analysis'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	
	public function pdf($id, $id_analysis)
	{
		ob_start();
		$this->load->library('tcpdf');
		
		$this->load->model('loan_model', 'loan');
		$this->load->model('title/title_model', 'title');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('career/career_model', 'career');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_owner_model', 'land_owner');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_schedule_model', 'loan_schedule');
		
		$data = $this->loan->getById($id);
		$analysis = $this->loan_analysis->getById($id_analysis);
		
		$person[] = $data['person_id'];
		
		$data['person']['title'] = $this->title->getById($data['person']['title_id']);
		$data['person']['person_age'] = empty($data['person']['person_birthdate'])? '' : age($data['person']['person_birthdate']);
		$data['person']['district'] = $this->district->getById($data['person']['person_addr_pre_district_id']);
		$data['person']['amphur'] = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
		$data['person']['province'] = $this->province->getById($data['person']['person_addr_pre_province_id']);
		$data['person']['career'] = $this->career->getById($data['person']['career_id']);
		
		$loan_type = $this->loan_type->getByLoan($id);
		
		$loan_type_objective1 = '';
		$loan_type_objective2 = '';
		$loan_type_objective3 = '';
		$loan_creditor = '';
		$loan_real_amount = 0;
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$loan_type_objective1 = '(/)';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '( )';
					
					$loan_real_amount += floatval($list_type['loan_type_redeem_amount']);
					
					if(!empty($list_type['loan_type_redeem_owner']))
					{
						$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(/)';
					$loan_type_objective3 = '( )';
					
					$loan_real_amount += floatval($list_type['loan_type_contract_amount']);
					
					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '(/)';
					$loan_type_objective3 = '( )';
					
					$loan_real_amount += floatval($list_type['loan_type_case_amount']);
					
					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$loan_type_objective1 = '( )';
					$loan_type_objective2 = '( )';
					$loan_type_objective3 = '(/)';
					
					$loan_real_amount += floatval($list_type['loan_type_sale_amount']);
				}
			}
		}
		
		$married_list = $this->married->all();
		
		$html_married = '';
		foreach ($married_list as $married)
		{
			$html_married .= $married['status_married_id']==$data['person']['status_married_id']? '(/) ' : '( ) ';
			$html_married .= $married['status_married_name'].'&nbsp;&nbsp;';
		}
		
		if(empty($data['loan_spouse']))
		{
			$spouse_name = '-';
			$spouse_age = '-';
		}
		else
		{
			$spouse = $this->person->getById($data['loan_spouse']);
			$spouse_title = $this->title->getById($spouse['title_id']);
			$spouse_name = $spouse_title['title_name'].$spouse['person_fname'].'  '.$spouse['person_lname'];
			$spouse_age = empty($spouse['person_birthdate'])? '' : age($spouse['person_birthdate']);
		}
		
		$debt =  $this->loan_debt->getByLoan($data['loan_id']);
			
		$debt_text = '';
		$debt_sum = 0;
		if(!empty($debt))
		{
			foreach ($debt as $list_debt)
			{
				$debt_text .= $list_debt['loan_debt_name'].' , ';
				$debt_sum += floatval($list_debt['loan_debt_amount']);
			}
		}
			
		if($debt_text!='') $debt_text = num2Thai(substr($debt_text, 0, -3)).'&nbsp;&nbsp;';
		
		$loan_co =  $this->loan_co->getByLoan($id);
		
		$html_loan_co = '';
		if(!empty($loan_co))
		{
			$html_loan_co = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10%"></td>
						<td width="90%">กรณีผู้ขอสินเชื่ออายุมากกว่า ๖๕ ปี หรือมีความสามารถในการชำระหนี้ได้ไม่เพียงพอ ผู้ขอสินเชื่อได้เสนอผู้กู้ร่วม คือ</td>
					</tr>
				</table>';
			$i_co = 1;
			foreach ($loan_co as $list_co)
			{
				$person[] = $list_co['person_id'];
				$age_co = age($list_co['person_birthdate']);
		
				$html_loan_co .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%"></td>
							<td width="30%">('.num2Thai($i_co++).') ชื่อ&nbsp;&nbsp;'.$list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname'].'</td>
							<td width="15%">อายุ '.num2Thai($age_co).' ปี</td>
							<td width="40%">มีความเกี่ยวข้องกับผู้ขอสินเชื่อ&nbsp;&nbsp;'.$list_co['relationship_name'].'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="18%"></td>
							<td width="82%">ประกอบอาชีพ&nbsp;&nbsp;'.$list_co['career_name'].'</td>
						</tr>
					</table>';
			}
		}
		
		$loan_asset =  $this->loan_asset->getByLoan($id);
		
		$asset_sum = 0;
		if(!empty($loan_asset))
		{
			foreach ($loan_asset as $list_asset)
			{
				$asset_sum += floatval($list_asset['loan_asset_amount']);
			}
		}
		
		$loan_debt =  $this->loan_debt->getByLoan($id);
		
		$debt_sum = 0;
		if(!empty($loan_debt))
		{
			foreach ($loan_debt as $list_debt)
			{
				$debt_sum += floatval($list_debt['loan_debt_amount']);
			}
		}
		
		$loan_income_1 = $this->loan_income->getByPerson($data['person_id'], '1');
		$loan_income_2 = $this->loan_income->getByPerson($data['person_id'], '2');
		$loan_income_3 = $this->loan_income->getByPerson($data['person_id'], '3');
		
		$income_all['rai'] = 0;
		$income_all['ngan'] = 0;
		$income_all['wah'] = 0;
		$income_self['rai'] = 0;
		$income_self['ngan'] = 0;
		$income_self['wah'] = 0;
		$income_renting['rai'] = 0;
		$income_renting['ngan'] = 0;
		$income_renting['wah'] = 0;
		$income_approve['rai'] = 0;
		$income_approve['ngan'] = 0;
		$income_approve['wah'] = 0;
		
		if(!empty($loan_income_1))
		{
			foreach ($loan_income_1 as $income1_value)
			{
				$self_rai = empty($income1_value['loan_activity_self_rai'])? 0 : intval($income1_value['loan_activity_self_rai']);
				$self_ngan = empty($income1_value['loan_activity_self_ngan'])? 0 : intval($income1_value['loan_activity_self_ngan']);
				$self_wah = empty($income1_value['loan_activity_self_wah'])? 0 : intval($income1_value['loan_activity_self_wah']);
				$renting_rai = empty($income1_value['loan_activity_renting_rai'])? 0 : intval($income1_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income1_value['loan_activity_renting_ngan'])? 0 : intval($income1_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income1_value['loan_activity_renting_wah'])? 0 : intval($income1_value['loan_activity_renting_wah']);
				$approve_rai = empty($income1_value['loan_activity_approve_rai'])? 0 : intval($income1_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income1_value['loan_activity_approve_ngan'])? 0 : intval($income1_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income1_value['loan_activity_approve_wah'])? 0 : intval($income1_value['loan_activity_approve_wah']);
		
				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}
		
		if(!empty($loan_income_2))
		{
			foreach ($loan_income_2 as $income2_value)
			{
				$self_rai = empty($income2_value['loan_activity_self_rai'])? 0 : intval($income2_value['loan_activity_self_rai']);
				$self_ngan = empty($income2_value['loan_activity_self_ngan'])? 0 : intval($income2_value['loan_activity_self_ngan']);
				$self_wah = empty($income2_value['loan_activity_self_wah'])? 0 : intval($income2_value['loan_activity_self_wah']);
				$renting_rai = empty($income2_value['loan_activity_renting_rai'])? 0 : intval($income2_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income2_value['loan_activity_renting_ngan'])? 0 : intval($income2_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income2_value['loan_activity_renting_wah'])? 0 : intval($income2_value['loan_activity_renting_wah']);
				$approve_rai = empty($income2_value['loan_activity_approve_rai'])? 0 : intval($income2_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income2_value['loan_activity_approve_ngan'])? 0 : intval($income2_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income2_value['loan_activity_approve_wah'])? 0 : intval($income2_value['loan_activity_approve_wah']);
		
				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}
		
		if(!empty($loan_income_3))
		{
			foreach ($loan_income_3 as $income3_value)
			{
				$self_rai = empty($income3_value['loan_activity_self_rai'])? 0 : intval($income3_value['loan_activity_self_rai']);
				$self_ngan = empty($income3_value['loan_activity_self_ngan'])? 0 : intval($income3_value['loan_activity_self_ngan']);
				$self_wah = empty($income3_value['loan_activity_self_wah'])? 0 : intval($income3_value['loan_activity_self_wah']);
				$renting_rai = empty($income3_value['loan_activity_renting_rai'])? 0 : intval($income3_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income3_value['loan_activity_renting_ngan'])? 0 : intval($income3_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income3_value['loan_activity_renting_wah'])? 0 : intval($income3_value['loan_activity_renting_wah']);
				$approve_rai = empty($income3_value['loan_activity_approve_rai'])? 0 : intval($income3_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income3_value['loan_activity_approve_ngan'])? 0 : intval($income3_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income3_value['loan_activity_approve_wah'])? 0 : intval($income3_value['loan_activity_approve_wah']);
		
				$income_all['rai'] += $self_rai + $renting_rai + $approve_rai;
				$income_all['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$income_all['wah'] += $self_wah + $renting_wah + $approve_wah;
				$income_self['rai'] += $self_rai;
				$income_self['ngan'] += $self_ngan;
				$income_self['wah'] += $self_wah;
				$income_renting['rai'] += $renting_rai;
				$income_renting['ngan'] += $renting_ngan;
				$income_renting['wah'] += $renting_wah;
				$income_approve['rai'] += $approve_rai;
				$income_approve['ngan'] += $approve_ngan;
				$income_approve['wah'] += $approve_wah;
			}
		}
		
		$income_all = area($income_all['rai'], $income_all['ngan'], $income_all['wah']);
		$income_self = area($income_self['rai'], $income_self['ngan'], $income_self['wah']);
		$income_renting = area($income_renting['rai'], $income_renting['ngan'], $income_renting['wah']);
		$income_approve = area($income_approve['rai'], $income_approve['ngan'], $income_approve['wah']);
		
		$list_land_record =  $this->land_record->getByLoan($id);
		
		$check_guarantee = '( )';
		$check_guarantee_land = '( )';
		
		$i_assess = 1;
		$building_count = 0;
		$building_sum = 0;
		$record_sum = 0;
		$land_assess = array();
		$land_owner =  '';
		$land_main = '';
		$land_other = '';
		if(!empty($list_land_record))
		{
			foreach ($list_land_record as $key_land => $land_record)
			{
				$record_sum += floatval($land_record['land_record_staff_mortagage_price']);
		
				$check_main = $land_record['loan_guarantee_land_type']=='1';
				
				if($check_main)
				{
					$list_land_owner = $this->land_owner->getByLand($land_record['land_id']);
					
					if(!empty($list_land_owner))
					{
						foreach ($list_land_owner as $list_owner)
						{
							$land_owner .= $list_owner['title_name'].$list_owner['person_fname'].' '.$list_owner['person_lname'].', ';
						}
					}
					
					if($land_owner!='')
					{
						$land_owner = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>ชื่อ – สกุลของเจ้าของที่ดิน '.substr($land_owner, 0, -2).'</td>
								</tr>
							</table>';
					}
				}
				else
				{
					$check_guarantee = '(/)';
					$check_guarantee_land = '(/)';
				}
		
				$list_land_assess = $this->land_assess->getByLand($land_record['land_id']);
		
				if(!empty($list_land_assess))
				{
					foreach ($list_land_assess as $list_assess)
					{
						if($check_main)
						{
							$land_main .= $i_assess.', ';
						}
						else
						{
							$land_other .= $i_assess.', ';
						}
		
						$land_assess[$i_assess++] = $list_assess;
					}
				}
		
				$building_record = $this->building_record->getByLand($land_record['land_id']);
				$list_land_record[$key_land]['building_record'] = $building_record;
		
				if(!empty($building_record))
				{
					foreach ($building_record as $list_building)
					{
						$building_amount = floatval($list_building['building_record_staff_assess_amount']);
		
						$record_sum += $building_amount;
						$building_sum += $building_amount;
		
						$building_count++;
					}
				}
			}
		}
		
		if($land_main != '') $land_main = substr($land_main, 0, -2);
		if($land_other != '') $land_other = substr($land_other, 0, -2);
		
		$sum_assess = 0;
		$html_assess = '';
		if(!empty($land_assess))
		{
			foreach ($land_assess as $key_assess => $list_assess)
			{
				$cal_assess = floatval($list_assess['land_assess_inspector_sum']);
				$sum_assess += floatval($cal_assess);
				
				$html_assess .= '<tr>
						<td align="center">'.num2Thai($key_assess).'</td>
						<td align="center">'.num2Thai($list_assess['land_assess_area_rai'].' - '.$list_assess['land_assess_area_ngan'].' - '.$list_assess['land_assess_area_wah']).'&nbsp;&nbsp;</td>
						<td align="right">'.num2Thai(number_format($list_assess['land_assess_price'], 2)).'&nbsp;&nbsp;</td>
						<td align="right">'.num2Thai(number_format(($list_assess['land_assess_price_pre_min'] + $list_assess['land_assess_price_pre_max']) / 2, 2)).'&nbsp;&nbsp;</td>
						<td align="right">'.num2Thai(number_format($list_assess['land_assess_price_basic'], 2)).'&nbsp;&nbsp;</td>
						<td align="right">'.num2Thai(number_format($cal_assess, 2)).'&nbsp;&nbsp;</td>
						<td align="right">-&nbsp;&nbsp;</td>
					</tr>';
			}
		}
		
		$sum_record = $sum_assess + $building_sum;
		
		$loan_guarantee_bond =  $this->loan_guarantee_bond->getByLoan($id);
		
		$check_guarantee_bond = '( )';
		$html_guarantee_bond = '';
		if(!empty($loan_guarantee_bond)) 
		{
			$check_guarantee = '(/)';
			$check_guarantee_bond = '(/)';
			
			$i_bond = 1;
			foreach ($loan_guarantee_bond as $list_bond)
			{
				$html_guarantee_bond .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="15%"></td>
						<td width="45%">('.num2Thai($i_bond++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bond['loan_bond_owner'].' เป็นผู้มีกรรมสิทธ์</td>
						<td width="40%">ชนิดพันธบัตร&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_type']).'</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="30%">เลขที่ต้น&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_startnumber']).'</td>
						<td width="30%">เลขที่ท้าย&nbsp;&nbsp;'.num2Thai($list_bond['loan_bond_endnumber']).'</td>
						<td width="50%">จำนวนเงิน&nbsp;&nbsp;'.num2Thai(number_format($list_bond['loan_bond_amount'], 2)).'&nbsp;&nbsp;บาท</td>
					</tr>
				</table>';
			}
		}
		
		$loan_guarantee_bookbank =  $this->loan_guarantee_bookbank->getByLoan($id);
		
		$loan_bookbank_type = array(
				'1' => 'ออมทรัพย์',
				'2' => 'ฝากประจำ',
				'3' => 'เผื่อเรียก'
		);
		
		$check_guarantee_bookbank = '( )';
		$html_guarantee_bookbank = '';
		if(!empty($loan_guarantee_bookbank)) 
		{
			$check_guarantee = '(/)';
			$check_guarantee_bookbank = '(/)';
			
			$i_bookbank = 1;
			foreach ($loan_guarantee_bookbank as $list_bookbank)
			{
				$html_guarantee_bookbank .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="15%"></td>
							<td width="85%">('.num2Thai($i_bookbank++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bookbank['loan_bookbank_owner'].' เป็นเจ้าของบัญชีเงินฝาก</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="65%">ธนาคาร/สถาบันการเงิน/สหกรณ์&nbsp;&nbsp;'.num2Thai($list_bookbank['bank_name']).'</td>
							<td width="35%">สาขา&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_branch']).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="30%">เลขที่บัญชี&nbsp;&nbsp;'.num2Thai($list_bookbank['loan_bookbank_no']).'</td>
							<td width="30%">ประเภทบัญชี&nbsp;&nbsp;'.num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]).'</td>
							<td width="40%">ยอดเงินฝาก ณ วันที่&nbsp;&nbsp;'.num2Thai(convert_dmy_th($list_bookbank['loan_bookbank_date'])).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>จำนวนเงิน&nbsp;&nbsp;'.num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)).'&nbsp;&nbsp;บาท</td>
						</tr>
					</table>';
			}
		}
		
		$loan_guarantee_bondsman =  $this->loan_guarantee_bondsman->getByLoan($id);
		
		$check_guarantee_bondsman = '( )';
		$html_guarantee_bondsman = '';
		if(!empty($loan_guarantee_bondsman))
		{
			$check_guarantee = '(/)';
			$check_guarantee_bondsman = '(/)';
			
			$i_bondsman = 1;
			foreach ($loan_guarantee_bondsman as $list_bondsman)
			{
				$age_bondsman = age($list_bondsman['person_birthdate']);
				
				$html_guarantee_bondsman .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="15%"></td>
						<td width="40%">('.num2Thai($i_bondsman++).') ชื่อ – สกุล&nbsp;&nbsp;'.$list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'].'</td>
						<td width="15%">อายุ&nbsp;&nbsp;'.num2Thai($age_bondsman).'&nbsp;&nbsp;ปี</td>
						<td width="30%">หมายเลขโทรศัพท์&nbsp;&nbsp;'.(empty($list_bondsman['person_mobile'])? '-' : num2Thai($list_bondsman['person_mobile'])).'</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>ที่อยู่&nbsp;&nbsp;บ้านเลขที่ '.num2Thai($list_bondsman['person_addr_pre_no']).' หมู่ที่ '.(empty($list_bondsman['person_addr_pre_moo'])? '-' : num2Thai($list_bondsman['person_addr_pre_moo'])).' ถนน '.(empty($list_bondsman['person_addr_pre_road'])? '-' : num2Thai($list_bondsman['person_addr_pre_road'])).' ตำบล '.$list_bondsman['district_name'].' อำเภอ '.$list_bondsman['amphur_name'].' จังหวัด '.$list_bondsman['province_name'].'</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="35%">อาชีพ&nbsp;&nbsp;'.num2Thai($list_bondsman['career_name']).'</td>
						<td width="30%">ตำแหน่ง&nbsp;&nbsp;'.(empty($list_bondsman['person_position'])? '-' : num2Thai($list_bondsman['person_position'])).'</td>
						<td width="35%">สังกัด&nbsp;&nbsp;'.(empty($list_bondsman['person_belong'])? '-' : num2Thai($list_bondsman['person_belong'])).'</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>อัตราเงินเดือน/ค่าจ้างเดือนละ&nbsp;&nbsp;'.num2Thai(number_format($list_bondsman['person_income_per_month'], 2)).'&nbsp;&nbsp;บาท</td>
					</tr>
				</table>';
			}
		}
		
		$html_expenditure = $this->expenditure($id, $id_analysis, 'html');
		
		$schedule = $this->loan_schedule->getByLoan($id);
		
		$html_schedule = '';
		$list_round =  array();
		if(!empty($schedule))
		{
			foreach ($schedule as $list_schedule)
			{
				$list_round[$list_schedule['loan_schedule_times']] = array(
						'principle' => floatval($list_schedule['loan_schedule_amount']),
						'other' => intval($list_schedule['loan_schedule_other']),
						'principle_other' => intval($list_schedule['loan_schedule_principle']),
						'interest_other' => intval($list_schedule['loan_schedule_interest'])
				);
			}
		}
		
		$income = floatval($analysis['loan_analysis_cooperative']);
		$amount = floatval($data['loan_amount']);
		$interest = intval($analysis['loan_analysis_interest']) / 100;
		$type = intval($analysis['loan_analysis_length']);
		$year = intval($analysis['loan_analysis_year']);
		$pay = $analysis['loan_analysis_pay'];
		$start = $analysis['loan_analysis_start'];
		
		$start = convert_ymd_en($start);
		$pay = convert_ymd_en($pay);
		
		$date_start = $start;
		$date_start = new DateTime($date_start);
		$date_start->add(new DateInterval('P1D'));
		$date_start = $date_start->format('Y-m-d');
		$date_end = $pay;
		
		$list_month = array();
		$remain = $amount;
		if(!empty($list_round))
		{
			foreach ($list_round as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				$interest_per = ($remain * $interest * $day_between) / 365;
					
				$list_round[$r_key] = array(
						'income' => $income,
						'principle' => $r_value['principle'],
						'remain' => $remain,
						'interest' => $interest_per,
						'other' => $r_value['other'],
						'principle_other' => $r_value['principle_other'],
						'interest_other' => $r_value['interest_other'],
						'date' => $date_end
				);
					
				$list_month[] = date('m', strtotime($date_end));
					
				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$type.'M'));
				$date_end = $date_end->format('Y-m-d');
			}
			
			$list_month = array_unique($list_month);
			
			$month = '';
			foreach ($list_month as $data_month)
			{
				$month .= thai_month($data_month).', ';
			}
			
			if(!empty($month)) $month = substr($month, 0, -2);
			
			$data_schedule = array(
					'year' => $year,
					'month' => $month,
					'income' => $income,
					'amount' => $amount,
					'interest' => $interest,
					'type' => $type,
					'pay' => $pay,
					'start' => $start,
					'fiscal_start' => fiscalYear($start),
					'fiscal_end' => fiscalYear($date_start),
					'list_round' => $list_round
			);
			
			$html_schedule = $this->parser->parse('cal_html', $data_schedule);
		}
		
		if(!empty($month)) $month = substr($month, 0, -2);
		
		$filename = 'analysis_'.date('Ymd');
		
		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
		$pdf->SetTitle($filename);//  กำหนด Title
		$pdf->SetSubject('Export receipt'); // กำหนด Subject
		$pdf->SetKeywords($filename); // กำหนด Keyword
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// add a page
		$pdf->SetMargins(10, 10, 10, true);
		$pdf->AddPage();
		
		$htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
					<td width="80%">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๑๓</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="100%" style="font-size: 10px;"></td>
							</tr>
						</table>
						<hr />
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-weight: bold; text-align:center;">
						แบบสรุปผลการพิจารณาการขอสินเชื่อจากสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.)
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 10px;"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="60%">ผู้ขอสินเชื่อได้ยื่นความประสงค์ขอสินเชื่อ ณ&nbsp;&nbsp;'.num2Thai($data['loan_location']).'</td>
					<td width="35%">เมื่อวันที่&nbsp;&nbsp;'.num2Thai($data['loan_datefull']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>จึงขอเสนอผลการพิจารณาการขอสินเชื่อของผู้ขอสินเชื่อ ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="95%">๑.&nbsp;&nbsp;ข้อมูลทั่วไปของผู้ขอสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="50%">๑.๑&nbsp;&nbsp;ชื่อ - สกุล&nbsp;&nbsp;'.$data['person']['title']['title_name'].$data['person']['person_fname'].' '.$data['person']['person_lname'].'</td>
					<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($data['person']['person_thaiid'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%">ที่อยู่ บ้านเลขที่&nbsp;&nbsp;'.num2Thai($data['person']['person_addr_pre_no']).'</td>
					<td width="10%">หมู่ที่&nbsp;&nbsp;'.(empty($data['person']['person_addr_pre_moo'])? '-' : num2Thai($data['person']['person_addr_pre_moo'])).'</td>
					<td width="25%">ตำบล&nbsp;&nbsp;'.$data['person']['district']['district_name'].'</td>
					<td width="25%">อำเภอ&nbsp;&nbsp;'.$data['person']['amphur']['amphur_name'].'</td>
					<td width="20%">จังหวัด&nbsp;&nbsp;'.$data['person']['province']['province_name'].'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="40%">ประกอบอาชีพ&nbsp;&nbsp;'.num2Thai($data['person']['career']['career_name']).'</td>
					<td width="30%">จำนวนสมาชิกในครัวเรือน&nbsp;&nbsp;'.num2Thai($data['loan_member_amount']).'&nbsp;&nbsp;คน</td>
					<td width="30%">จำนวนแรงงานในครัวเรือน&nbsp;&nbsp;'.num2Thai($data['loan_labor_amount']).'&nbsp;&nbsp;คน</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%">อายุของผู้ขอสินเชื่อ&nbsp;&nbsp;'.num2Thai($data['person']['person_age']).'&nbsp;&nbsp;ปี</td>
				</tr>
			</table>
			'.$html_loan_co.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๑.๒&nbsp;&nbsp;สถานภาพ 	'.$html_married.'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="60%">ชื่อ – สกุลของคู่สมรส (ถ้ามี)&nbsp;&nbsp;'.$spouse_name.'</td>
					<td width="25%">อายุ&nbsp;&nbsp;'.num2Thai($spouse_age).'&nbsp;&nbsp;ปี</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๑.๓&nbsp;&nbsp;'.($analysis['loan_analysis_egov']=='1'? '(/)' : '( )').' เป็นลูกค้า ธ.ก.ส. (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_egov_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">'.($analysis['loan_analysis_cooperative']=='1'? '(/)' : '( )').' เป็นสมาชิกสหกรณ์ (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_cooperative_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">'.($analysis['loan_analysis_bank']=='1'? '(/)' : '( )').' เป็นลูกค้าสถาบันการเงิน (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_bank_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">'.($analysis['loan_analysis_bankother']=='1'? '(/)' : '( )').' อื่นๆ (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_bankother_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="50%">๑.๔&nbsp;&nbsp;ผู้ขอสินเชื่อมีทรัพย์สินมูลค่ารวม&nbsp;&nbsp;'.num2Thai(number_format($asset_sum, 2)).' บาท</td>
					<td width="40%">และมีหนี้สินรวมจำนวน&nbsp;&nbsp;'.num2Thai(number_format($debt_sum, 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๑.๕&nbsp;&nbsp;ในปัจจุบันมีพื้นที่ทำการเกษตร เนื้อที่&nbsp;&nbsp;'.num2Thai($income_all['rai'].' - '.$income_all['ngan'].' - '.$income_all['wah']).'&nbsp;&nbsp;ไร่</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">โดยเป็นกรรมสิทธิ์ของตนเอง จำนวน&nbsp;&nbsp;'.num2Thai($income_self['rai'].' - '.$income_self['ngan'].' - '.$income_self['wah']).'&nbsp;&nbsp;ไร่</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">เช่าที่ดินผู้อื่น จำนวน&nbsp;&nbsp;'.num2Thai($income_renting['rai'].' - '.$income_renting['ngan'].' - '.$income_renting['wah']).'&nbsp;&nbsp;ไร่</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">ได้รับอนุญาตให้ทำประโยชน์ในที่ดินผู้อื่น จำนวน&nbsp;&nbsp;'.num2Thai($income_approve['rai'].' - '.$income_approve['ngan'].' - '.$income_approve['wah']).'&nbsp;&nbsp;ไร่</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๑.๖&nbsp;&nbsp;พฤติกรรมของผู้ขอสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="32%">๑.๖.๑&nbsp;&nbsp;ความประพฤติ</td>
					<td width="5%">'.($analysis['loan_analysis_behavior']=='1'? '(/)' : '( )').' ดี</td>
					<td width="8%">'.($analysis['loan_analysis_behavior']!='1'? '(/)' : '( )').' ไม่ดี</td>
					<td width="40%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="32%">๑.๖.๒&nbsp;&nbsp;ความตั้งใจในการประกอบอาชีพ</td>
					<td width="5%">'.($analysis['loan_analysis_behavior_occupation']=='1'? '(/)' : '( )').' ดี</td>
					<td width="8%">'.($analysis['loan_analysis_behavior_occupation']!='1'? '(/)' : '( )').' ไม่ดี</td>
					<td width="40%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_occupation_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="52%">๑.๖.๓&nbsp;&nbsp;ความมุ่งมั่นตั้งใจในการใช้ประโยชน์ที่ดินเพื่อเกษตรกรรม</td>
					<td width="5%">'.($analysis['loan_analysis_behavior_farm']=='1'? '(/)' : '( )').' มี</td>
					<td width="8%">'.($analysis['loan_analysis_behavior_farm']!='1'? '(/)' : '( )').' ไม่มี</td>
					<td width="20%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_farm_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="15%">๑.๖.๔&nbsp;&nbsp;สุขภาพ</td>
					<td width="10%">'.($analysis['loan_analysis_behavior_health']=='1'? '(/)' : '( )').' แข็งแรง</td>
					<td width="15%">'.($analysis['loan_analysis_behavior_health']!='1'? '(/)' : '( )').' ไม่แข็งแรง</td>
					<td width="45%">(ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_health_detail']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๑.๖.๕&nbsp;&nbsp;อื่นๆ (ระบุ)&nbsp;&nbsp;'.num2Thai($analysis['loan_analysis_behavior_other']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="95%">๒.&nbsp;&nbsp;จำนวนเงินที่ขอสินเชื่อ โดยมีวัตถุประสงค์เพื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">'.$loan_type_objective1.' ไถ่ถอนที่ดินจากการจำนองหรือขายฝาก '.$loan_type_objective2.' ชำระหนี้ '.$loan_type_objective3.' ซื้อคืนที่ดิน จำนวน '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
				</tr>
			</table>
			<!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>และเพื่อประกอบอาชีพเกษตรกรรม จำนวน 	xxx บาท</td>
				</tr>
			</table> -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="95%">๓.&nbsp;&nbsp;มูลเหตุแห่งหนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๓.๑&nbsp;&nbsp;ชื่อ – สกุลของเจ้าหนี้ หรือเจ้าหนี้ตามคำพิพากษา&nbsp;&nbsp;'.(empty($loan_creditor)? '-' : $loan_creditor).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>จำนวนเงินที่กู้ยืม หรือขายฝาก และได้รับจริง '.num2Thai(number_format($loan_real_amount, 2)).' บาท<!-- *ตามเอกสารการเป็นหนี้ (ระบุ) xxx จำนวนหนี้ตามเอกสาร xxx บาท -->กำหนดเวลาการชำระหนี้ ภายในวันที่ '.num2Thai($analysis['loan_analysis_deadline']).' ผลการไกล่เกลี่ยประนีประนอม มีจำนวนหนี้คงเหลือต้องชำระ '.num2Thai(number_format($analysis['loan_analysis_remain'], 2)).' บาท ภายในวันที่ '.num2Thai($analysis['loan_analysis_within']).' สาเหตุที่เป็นหนี้ (ให้อธิบายโดยละเอียดว่า เกิดจากสาเหตุใด เมื่อใด และผลเป็นประการใด) '.num2Thai($analysis['loan_analysis_cause']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๓.๒&nbsp;&nbsp;ผู้ขอสินเชื่อและคู่สมรส (ถ้ามี) มีหนี้สินรายอื่น ได้แก่ (เช่น ธ.ก.ส. สหกรณ์ สถาบันการเงิน หรือบุคคล เป็นต้น)</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>'.$debt_text.'จำนวน '.num2Thai(number_format($debt_sum, 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="95%">๔.&nbsp;&nbsp;หลักประกันคำขอสินเชื่อ (รายละเอียดตามแบบบันทึกการตรวจสอบที่ดิน)</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๔.๑&nbsp;&nbsp;การจำนองที่ดิน</td>
				</tr>
			</table>
			<table width="100%" border="1" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%" align="center">แปลงที่</td>
					<td width="12%" align="center">เนื้อที่ (ไร่)</td>
					<td width="18%" align="center">ราคาจดทะเบียนสิทธิ<br/>และนิติกรรม ต่อไร่</td>
					<td width="15%" align="center">ราคาซื้อขายเฉลี่ย<br/>ในปัจจุบันต่อไร่</td>
					<td width="15%" align="center">ราคาประเมินต่อไร่</td>
					<td width="15%" align="center">ราคาประเมิน<br/>ทั้งแปลง</td>
					<td width="20%" align="center">ราคาประเมินเป็นกี่เท่า<br/>ของจำนวนเงินกู้</td>
				</tr>
				'.$html_assess.'
				<tr>
					<td colspan="5" align="right">รวมทั้งสิ้น&nbsp;&nbsp;</td>
					<td align="right">'.num2Thai(number_format($sum_assess, 2)).'&nbsp;&nbsp;</td>
					<td align="right">'.num2Thai(number_format($sum_assess  / $data['loan_amount'], 2)).'&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">มีสิ่งปลูกสร้าง จำนวน '.num2Thai($building_count).'  หลัง</td>
					<td width="50%">ราคาประเมิน '.num2Thai(number_format($building_sum,  2)).'  บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>รวมราคาประเมินที่ดินและสิ่งปลูกสร้าง จำนวน '.num2Thai(number_format($sum_record, 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="50%">ราคาประเมินเป็นกี่เท่าของจำนวนเงินกู้ '.num2Thai(number_format($sum_record / $data['loan_amount'], 2)).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>หมายเหตุ : การจำนองที่ดินซึ่งไถ่ถอนจากเจ้าหนี้ ได้แก่ แปลงที่&nbsp;&nbsp;'.num2Thai($land_main).'</td>
				</tr>
			</table>
			'.$land_owner.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>และจำนองที่ดินเพิ่มเติม ได้แก่ แปลงที่&nbsp;&nbsp;'.num2Thai($land_other).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" style="font-size: 5px;"></td>
				</tr>
			</table>
			<hr width="50%"/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="font-size: 14px;">* เอกสารการเป็นหนี้ ให้ระบุ เช่น ขายฝาก จำนอง สัญญากู้ยืม สัญญาซื้อคืน หรืออื่นๆ เป็นต้น</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๔.๒&nbsp;&nbsp;พันธบัตรรัฐบาล (ระบุชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์ของผู้จำนำซึ่งเป็นผู้มีกรรมสิทธิ์ รวมทั้งชนิด</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>พันธบัตร เลขที่ต้น เลขที่ท้าย หน้าทะเบียน ลงวันที่ จำนวนเงินตามพันธบัตรที่จำนำ) </td>
				</tr>
			</table>
			'.$html_guarantee_bond.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๔.๓&nbsp;&nbsp;เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์ (ระบุ ชื่อ – สกุล ที่อยู่ และหมายเลขโทรศัพท์</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ของเจ้าของบัญชีเงินฝาก รวมทั้งบัญชีเงินฝากของธนาคาร สถาบันการเงิน หรือสหกรณ์ ประเภทของเงินฝาก เลขที่บัญชีเงินฝาก ยอดเงินคงเหลือจำนวนเท่าใด และเมื่อใด)</td>
				</tr>
			</table>
			'.$html_guarantee_bookbank.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๔.๔&nbsp;&nbsp;การค้ำประกันด้วยบุคคล (ระบุชื่อ - สกุล อายุ ที่อยู่ หมายเลขโทรศัพท์ อาชีพ ตำแหน่ง สังกัด อัตราเงินเดือน/</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>ค่าจ้างเดือนละเท่าใด และความน่าเชื่อถือ พร้อมทั้งแนบสำเนาบัตรประชาชน ทะเบียนบ้าน หนังสือรับรองจากต้นสังกัด หรือหนังสือแสดงรายได้ โดยระบุตำแหน่ง ระดับ อัตราเงินเดือน และหากผู้ค้ำประกันมีการทำสัญญาค้ำประกันรายอื่นด้วย ให้แจ้งภาระผูกพันดังกล่าวด้วย)</td>
				</tr>
			</table>
			'.$html_guarantee_bondsman.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="95%">๕.&nbsp;&nbsp;รายได้ ค่าใช้จ่าย และแผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๕.๑&nbsp;&nbsp;รายได้ และค่าใช้จ่ายของผู้กู้ (รายละเอียดตามข้อมูลของผู้ขอสินเชื่อรายที่แนบ)</td>
				</tr>
			</table>
			'.$html_expenditure.'
			<!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๕.๒&nbsp;&nbsp;แผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ (กรณีขอสินเชื่อเพื่อไปประกอบอาชีพเกษตรกรรมของผู้ขอ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>สินเชื่อว่ามีแผนการผลิต แหล่งเงินทุนที่ใช้ในการผลิต และการตลาด อย่างไร)</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td></td>
				</tr>
			</table> -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="5%"></td>
					<td width="95%">๖.&nbsp;&nbsp;ประมาณการกระแสเงินสดและแผนการชำระคืนของผู้ขอสินเชื่อ</td>
				</tr>
			</table>
			'.$html_schedule.'
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๗.&nbsp;&nbsp;สรุปความเห็นในการพิจารณาอนุมัติคำขอสินเชื่อของผู้สอบสวน</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๗.๑&nbsp;&nbsp;พิจารณาตามหลักเกณฑ์ของ บจธ.</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">ความเห็น '.(empty($analysis['loan_analysis_investigate_comment'])? '-' : num2Thai($analysis['loan_analysis_investigate_comment'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๗.๒&nbsp;&nbsp;มูลเหตุแห่งหนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๗.๒.๑&nbsp;&nbsp;สุจริตหรือไม่สุจริต ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.(empty($analysis['loan_analysis_investigate_honestly'])? '-' : num2Thai($analysis['loan_analysis_investigate_honestly'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๗.๒.๒&nbsp;&nbsp;กรณีระยะเวลาในการก่อหนี้สินเพิ่งจะเกิดขึ้นไม่ถึงหนึ่งปี มีเหตุผลที่น่าเชื่อว่าหนี้นั้นเกิดขึ้น</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>โดยสุจริต ดังนี้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.(empty($analysis['loan_analysis_investigate_cause'])? '-' : num2Thai($analysis['loan_analysis_investigate_cause'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๗.๒.๓&nbsp;&nbsp;ความจำเป็นหรือไม่มีความจำเป็น ดังนี้ </td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.(empty($analysis['loan_analysis_investigate_must'])? '-' : num2Thai($analysis['loan_analysis_investigate_must'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๗.๓&nbsp;&nbsp;กรณีที่ดินแปลงซึ่งขอสินเชื่อเพื่อไถ่ถอนหรือซื้อคืนมีมูลค่าน้อยกว่าจำนวนเงินที่ขอสินเชื่อ การอนุมัติ</td>
				</tr>
			</table>
			<!-- <table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>คำขอสินเชื่อครั้งนี้จะเป็นประโยชน์ต่อผู้ขอสินเชื่ออย่างไร</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.(empty($analysis['loan_analysis_investigate_matter'])? '-' : num2Thai($analysis['loan_analysis_investigate_matter'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.$check_guarantee.' ผู้ขอสินเชื่อได้เสนอหลักประกันเพิ่มเติม</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">'.$check_guarantee_land.' ที่ดิน</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">'.$check_guarantee_bond.' พันธบัตรรัฐบาล</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">'.$check_guarantee_bookbank.' เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">'.($check_guarantee=='( )'? '(/)' : '( )').'	ผู้ขอสินเชื่อไม่สามารถหาหลักประกันเพิ่มเติมได้</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">๗.๔&nbsp;&nbsp;สรุปความเห็นและผลการพิจารณาเกี่ยวกับการขอสินเชื่อตามคำขอ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๗.๔.๑&nbsp;&nbsp;'.($analysis['loan_analysis_approve_status']=='1'? '(/)' : '( )').' เห็นสมควรอนุมัติในวงเงินสินเชื่อ '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">กำหนดชำระคืนเสร็จเรียบร้อยภายในวันที่ '.num2Thai($analysis['loan_analysis_approve_date']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">เงื่อนไขหรือคำชี้แจงเพิ่มเติม</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%"></td>
					<td width="75%">'.(empty($analysis['loan_analysis_approve_conditions'])? '-' : num2Thai($analysis['loan_analysis_approve_conditions'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">๗.๔.๒&nbsp;&nbsp;'.($analysis['loan_analysis_approve_status']!='1'? '(/)' : '( )').' เห็นสมควรไม่อนุมัติ</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="26%"></td>
					<td width="74%">'.($analysis['loan_analysis_approve_not_type']=='1' && $analysis['loan_analysis_approve_status']!='1'? '(/)' : '( )').' อยู่ในหลักเกณฑ์ให้สินเชื่อจาก บจธ. ปี '.(empty($analysis['loan_analysis_approve_not_year'])? '-' : num2Thai($analysis['loan_analysis_approve_not_year'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="26%"></td>
					<td width="74%">เนื่องจาก '.(empty($analysis['loan_analysis_approve_not_case'])? '-' : num2Thai($analysis['loan_analysis_approve_not_case'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="26%"></td>
					<td width="74%">'.($analysis['loan_analysis_approve_not_type']!='1' && $analysis['loan_analysis_approve_status']!='1'? '(/)' : '( )').' ไม่อยู่ในหลักเกณฑ์ให้สินเชื่อจาก บจธ. ปี '.(empty($analysis['loan_analysis_approve_not_year'])? '-' : num2Thai($analysis['loan_analysis_approve_not_year'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="26%"></td>
					<td width="74%">เนื่องจาก '.(empty($analysis['loan_analysis_approve_not_case'])? '-' : num2Thai($analysis['loan_analysis_approve_not_case'])).'</td>
				</tr>
			</table>
			<br/><br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" style="text-align:center;"></td>
					<td width="70%" style="text-align:center;">
						ลงชื่อ ......................................................<br/>
						(...........................................................)<br/>
						ตำแหน่ง ................................................<br/>
						วันที่ .........................................
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">๘.&nbsp;&nbsp;สรุปความเห็นในการพิจารณาคำขอสินเชื่อของฝ่าย/กอง</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">'.($analysis['loan_analysis_approve_sector_status']=='1'? '(/)' : '( )').' อนุมัติในวงเงินสินเชื่อ '.num2Thai(number_format($data['loan_amount'], 2)).' บาท</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">กำหนดชำระคืนเสร็จเรียบร้อยภายในวันที่ '.num2Thai($analysis['loan_analysis_approve_sector_date']).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">เงื่อนไขหรือคำชี้แจงเพิ่มเติม</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="20%"></td>
					<td width="80%">'.(empty($analysis['loan_analysis_approve_sector_conditions'])? '-' : num2Thai($analysis['loan_analysis_approve_sector_conditions'])).'</td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"></td>
					<td width="85%">'.($analysis['loan_analysis_approve_sector_status']!='1'? '(/)' : '( )').'	ไม่อนุมัติ เนื่องจาก '.(empty($analysis['loan_analysis_approve_sector_not_case'])? '-' : num2Thai($analysis['loan_analysis_approve_sector_not_case'])).'</td>
				</tr>
			</table>
			<br/><br/>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="30%" style="text-align:center;"></td>
					<td width="70%" style="text-align:center;">
						ลงชื่อ ......................................................<br/>
						(...........................................................)<br/>
						ตำแหน่ง ................................................<br/>
						วันที่ .........................................
					</td>
				</tr>
			</table> -->';
		
		$pdf->SetFont('thsarabun', '', 16);
		$pdf->writeHTML($htmlcontent, true, 0, true, true);
		
		$pdf->Output($filename.'.pdf', 'I');
	}
	
	public function check()
	{
		echo '<pre>';
		print_r($_SESSION['loan_analysis']);
		echo '</pre>';
	}
}
?>