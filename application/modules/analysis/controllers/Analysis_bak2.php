<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analysis extends MY_Controller
{
	private $title_page = 'พิจารณาการขอสินเชื่อ';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('loan_analysis_model', 'loan_analysis');
		
		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');
		
		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->loan_analysis->search($loan, $thaiid, $fullname)
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_analysis/analysis.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('analysis', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function search()
	{
		$this->load->model('summary/loan_summary_model', 'loan_summary');
		
		$loan = '';
		$thaiid = '';
		$fullname = '';
		if(!empty($_POST['txtLoan'])) $loan = $_POST['txtLoan'];
		if(!empty($_POST['txtThaiid'])) $thaiid = $_POST['txtThaiid'];
		if(!empty($_POST['txtFullname'])) $fullname = $_POST['txtFullname'];
		
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title_page => site_url('analysis')
			);
		
		$data_content = array(
				'loan' => $loan,
				'thaiid' => $thaiid,
				'fullname' => $fullname,
				'result' => $this->loan_summary->searchNonAnalysis($loan, $thaiid, $fullname)
		);
		
		$data = array(
				'title' => 'เพิ่มแบบพิจารณา',
				'js_other' => array(),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('search', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($type, $id, $id_analysis='')
	{
		$this->load->model('title/title_model', 'title');
		$this->load->model('district/district_model', 'district');
		$this->load->model('amphur/amphur_model', 'amphur');
		$this->load->model('province/province_model', 'province');
		$this->load->model('status_married/status_married_model', 'married');
		$this->load->model('career/career_model', 'career');
		$this->load->model('person/person_model', 'person');
		$this->load->model('loan_model', 'loan');
		$this->load->model('loan_type_model', 'loan_type');
		$this->load->model('loan_asset_model', 'loan_asset');
		$this->load->model('loan_debt_model', 'loan_debt');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_guarantee_bond_model', 'loan_guarantee_bond');
		$this->load->model('loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
		$this->load->model('loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
		$this->load->model('loan_income_model', 'loan_income');
		$this->load->model('summary/land_record_model', 'land_record');
		$this->load->model('summary/building_record_model', 'building_record');
		$this->load->model('land_assess_model', 'land_assess');
		$this->load->model('summary/loan_summary_model', 'loan_summary');
		$this->load->model('loan_analysis_model', 'loan_analysis');
		$this->load->model('loan_analysis_cal_model', 'loan_analysis_cal');
		$this->load->model('loan_schedule_model', 'loan_schedule');
		
		$data_menu['menu'] = 'loan';
		$data_breadcrumb['menu'] = array(
				'สินเชื่อ' =>'#',
				$this->title_page => site_url('analysis')
		);
		
		$data_content = array(
				'title' => $this->title->all(),
				'province' => $this->province->all(),
				'amphur' => array(),
				'district' => array(),
				'married' =>  $this->married->all(),
				'loan' =>  $this->loan->getById($id),
				'loan_asset' =>  $this->loan_asset->getByLoan($id),
				'loan_debt' =>  $this->loan_debt->getByLoan($id),
				'loan_co' =>  $this->loan_co->getByLoan($id),
				'land_record' =>  $this->land_record->getByLoan($id),
				'loan_guarantee_bond' =>  $this->loan_guarantee_bond->getByLoan($id),
				'loan_guarantee_bookbank' =>  $this->loan_guarantee_bookbank->getByLoan($id),
				'loan_guarantee_bondsman' =>  $this->loan_guarantee_bondsman->getByLoan($id),
				'loan_summary' =>  $this->loan_summary->getByLoan($id)
		);
		
		$data_content['loan']['loan_date'] = convert_dmy_th($data_content['loan']['loan_date']);
		$data_content['loan']['person']['title'] = $this->title->getById($data_content['loan']['person']['title_id']);
		$data_content['loan']['person']['person_age'] = empty($data_content['loan']['person']['person_birthdate'])? '' : age($data_content['loan']['person']['person_birthdate']);
		$data_content['loan']['person']['married'] = $this->married->getById($data_content['loan']['person']['status_married_id']);
		$data_content['loan']['person']['district'] = $this->district->getById($data_content['loan']['person']['person_addr_pre_district_id']);
		$data_content['loan']['person']['amphur'] = $this->amphur->getById($data_content['loan']['person']['person_addr_pre_amphur_id']);
		$data_content['loan']['person']['province'] = $this->province->getById($data_content['loan']['person']['person_addr_pre_province_id']);
		$data_content['loan']['person']['career'] = $this->career->getById($data_content['loan']['person']['career_id']);
		
		$loan_type = $this->loan_type->getByLoan($id);
		
		$data_content['loan_type_objective'] = '';
		$data_content['loan_creditor'] = '';
		$data_content['loan_real_amount'] = 0;
		$data_content['loan_type_farm_for'] = '';
		$data_content['check_type_farm'] = false;
		$case = '';
		if(!empty($loan_type))
		{
			foreach ($loan_type as $list_type)
			{
				if($list_type['loan_objective_id']=='1')
				{
					$data_content['loan_type_objective'] = '1';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_redeem_amount']);
					
					$case .= $list_type['loan_type_redeem_case'].'<br/>';
					
					if(!empty($list_type['loan_type_redeem_owner']))
					{
						$creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='2')
				{
					$data_content['loan_type_objective'] = '2';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_contract_amount']);
					
					$case .= $list_type['loan_type_contract_case'].'<br/>';
					
					if(!empty($list_type['loan_type_contract_creditor']))
					{
						$creditor = $this->person->getById($list_type['loan_type_contract_creditor']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='3')
				{
					$data_content['loan_type_objective'] = '2';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_case_amount']);
					
					$case .= $list_type['loan_type_case_case'].'<br/>';
					
					if(!empty($list_type['loan_type_case_plaintiff_id']))
					{
						$creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
						$creditor_title = $this->title->getById($creditor['title_id']);
						$data_content['loan_creditor'] = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
					}
				}
				else if($list_type['loan_objective_id']=='4')
				{
					$data_content['loan_type_objective'] = '3';
					$data_content['loan_real_amount'] += floatval($list_type['loan_type_sale_amount']);
				}
				else
				{
					$data_content['check_type_farm'] = true;
					$data_content['loan_type_farm_for'] = $list_type['loan_type_farm_for'];
				}
			}
		}
		
		if($case!='') $case = substr($case, 0, -5);
		
		$data_content['loan_income_1'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '1');
		$data_content['loan_income_2'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '2');
		$data_content['loan_income_3'] = $this->loan_income->getByPerson($data_content['loan']['person_id'], '3');
		
		$data_content['income_all']['rai'] = 0;
		$data_content['income_all']['ngan'] = 0;
		$data_content['income_all']['wah'] = 0;
		$data_content['income_self']['rai'] = 0;
		$data_content['income_self']['ngan'] = 0;
		$data_content['income_self']['wah'] = 0;
		$data_content['income_renting']['rai'] = 0;
		$data_content['income_renting']['ngan'] = 0;
		$data_content['income_renting']['wah'] = 0;
		$data_content['income_approve']['rai'] = 0;
		$data_content['income_approve']['ngan'] = 0;
		$data_content['income_approve']['wah'] = 0;
		
		if(!empty($data_content['loan_income_1']))
		{
			foreach ($data_content['loan_income_1'] as $income1_value)
			{
				$self_rai = empty($income1_value['loan_activity_self_rai'])? 0 : intval($income1_value['loan_activity_self_rai']);
				$self_ngan = empty($income1_value['loan_activity_self_ngan'])? 0 : intval($income1_value['loan_activity_self_ngan']);
				$self_wah = empty($income1_value['loan_activity_self_wah'])? 0 : intval($income1_value['loan_activity_self_wah']);
				$renting_rai = empty($income1_value['loan_activity_renting_rai'])? 0 : intval($income1_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income1_value['loan_activity_renting_ngan'])? 0 : intval($income1_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income1_value['loan_activity_renting_wah'])? 0 : intval($income1_value['loan_activity_renting_wah']);
				$approve_rai = empty($income1_value['loan_activity_approve_rai'])? 0 : intval($income1_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income1_value['loan_activity_approve_ngan'])? 0 : intval($income1_value['loan_activity_approve_ngan']); 
				$approve_wah = empty($income1_value['loan_activity_approve_wah'])? 0 : intval($income1_value['loan_activity_approve_wah']);
				
				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}
		
		if(!empty($data_content['loan_income_2']))
		{
			foreach ($data_content['loan_income_2'] as $income2_value)
			{
				$self_rai = empty($income2_value['loan_activity_self_rai'])? 0 : intval($income2_value['loan_activity_self_rai']);
				$self_ngan = empty($income2_value['loan_activity_self_ngan'])? 0 : intval($income2_value['loan_activity_self_ngan']);
				$self_wah = empty($income2_value['loan_activity_self_wah'])? 0 : intval($income2_value['loan_activity_self_wah']);
				$renting_rai = empty($income2_value['loan_activity_renting_rai'])? 0 : intval($income2_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income2_value['loan_activity_renting_ngan'])? 0 : intval($income2_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income2_value['loan_activity_renting_wah'])? 0 : intval($income2_value['loan_activity_renting_wah']);
				$approve_rai = empty($income2_value['loan_activity_approve_rai'])? 0 : intval($income2_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income2_value['loan_activity_approve_ngan'])? 0 : intval($income2_value['loan_activity_approve_ngan']); 
				$approve_wah = empty($income2_value['loan_activity_approve_wah'])? 0 : intval($income2_value['loan_activity_approve_wah']);
		
				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}
		
		if(!empty($data_content['loan_income_3']))
		{
			foreach ($data_content['loan_income_3'] as $income3_value)
			{
				$self_rai = empty($income3_value['loan_activity_self_rai'])? 0 : intval($income3_value['loan_activity_self_rai']);
				$self_ngan = empty($income3_value['loan_activity_self_ngan'])? 0 : intval($income3_value['loan_activity_self_ngan']);
				$self_wah = empty($income3_value['loan_activity_self_wah'])? 0 : intval($income3_value['loan_activity_self_wah']);
				$renting_rai = empty($income3_value['loan_activity_renting_rai'])? 0 : intval($income3_value['loan_activity_renting_rai']);
				$renting_ngan = empty($income3_value['loan_activity_renting_ngan'])? 0 : intval($income3_value['loan_activity_renting_ngan']);
				$renting_wah = empty($income3_value['loan_activity_renting_wah'])? 0 : intval($income3_value['loan_activity_renting_wah']);
				$approve_rai = empty($income3_value['loan_activity_approve_rai'])? 0 : intval($income3_value['loan_activity_approve_rai']);
				$approve_ngan = empty($income3_value['loan_activity_approve_ngan'])? 0 : intval($income3_value['loan_activity_approve_ngan']);
				$approve_wah = empty($income3_value['loan_activity_approve_wah'])? 0 : intval($income3_value['loan_activity_approve_wah']);
		
				$data_content['income_all']['rai'] += $self_rai + $renting_rai + $approve_rai;
				$data_content['income_all']['ngan'] += $self_ngan + $renting_ngan + $approve_ngan;
				$data_content['income_all']['wah'] += $self_wah + $renting_wah + $approve_wah;
				$data_content['income_self']['rai'] += $self_rai;
				$data_content['income_self']['ngan'] += $self_ngan;
				$data_content['income_self']['wah'] += $self_wah;
				$data_content['income_renting']['rai'] += $renting_rai;
				$data_content['income_renting']['ngan'] += $renting_ngan;
				$data_content['income_renting']['wah'] += $renting_wah;
				$data_content['income_approve']['rai'] += $approve_rai;
				$data_content['income_approve']['ngan'] += $approve_ngan;
				$data_content['income_approve']['wah'] += $approve_wah;
			}
		}
		
		$income_all = area($data_content['income_all']['rai'], $data_content['income_all']['ngan'], $data_content['income_all']['wah']);
		$data_content['income_all']['rai'] = $income_all['rai'];
		$data_content['income_all']['ngan'] = $income_all['ngan'];
		$data_content['income_all']['wah'] = $income_all['wah'];
		
		$income_self = area($data_content['income_self']['rai'], $data_content['income_self']['ngan'], $data_content['income_self']['wah']);
		$data_content['income_self']['rai'] = $income_self['rai'];
		$data_content['income_self']['ngan'] = $income_self['ngan'];
		$data_content['income_self']['wah'] = $income_self['wah'];
		
		$income_renting = area($data_content['income_renting']['rai'], $data_content['income_renting']['ngan'], $data_content['income_renting']['wah']);
		$data_content['income_renting']['rai'] = $income_renting['rai'];
		$data_content['income_renting']['ngan'] = $income_renting['ngan'];
		$data_content['income_renting']['wah'] = $income_renting['wah'];
		
		$income_approve = area($data_content['income_approve']['rai'], $data_content['income_approve']['ngan'], $data_content['income_approve']['wah']);
		$data_content['income_approve']['rai'] = $income_approve['rai'];
		$data_content['income_approve']['ngan'] = $income_approve['ngan'];
		$data_content['income_approve']['wah'] = $income_approve['wah'];
		
		$i_assess = 1;
		$data_content['building_count'] = 0;
		$data_content['building_sum'] = 0;
		$data_content['record_sum'] = 0;
		$data_content['land_assess'] = array();
		$data_content['land_main'] = '';
		$data_content['land_other'] = '';
		if(!empty($data_content['land_record']))
		{
			foreach ($data_content['land_record'] as $key_land => $land_record)
			{
				$data_content['record_sum'] += floatval($land_record['land_record_staff_mortagage_price']);
				
				$check_main = $land_record['loan_guarantee_land_type']=='1';
				
				$land_assess = $this->land_assess->getByLand($land_record['land_id']);
				
				if(!empty($land_assess))
				{
					foreach ($land_assess as $list_assess)
					{
						if($check_main)
						{
							$data_content['land_main'] .= $i_assess.', ';
						}
						else
						{
							$data_content['land_other'] .= $i_assess.', ';
						}
						
						$data_content['land_assess'][$i_assess++] = $list_assess;
					}
				}
				
				$building_record = $this->building_record->getByLand($land_record['land_id']);
				$data_content['land_record'][$key_land]['building_record'] = $building_record;
				
				if(!empty($building_record))
				{
					foreach ($building_record as $list_building)
					{
						$building_amount = floatval($list_building['building_record_staff_assess_amount']);
						
						$data_content['record_sum'] += $building_amount;
						$data_content['building_sum'] += $building_amount;
						
						$data_content['building_count']++;
					}
				}
			}
		}
		
		if($data_content['land_main'] != '') $data_content['land_main'] = substr($data_content['land_main'], 0, -2);
		if($data_content['land_other'] != '') $data_content['land_other'] = substr($data_content['land_other'], 0, -2);
		
		if(empty($data_content['loan']['loan_spouse']))
		{
			$data_content['loan']['spouse_name'] = '';
			$data_content['loan']['spouse_age'] = '';
		}
		else
		{
			$data_content['loan']['spouse'] = $this->person->getById($data_content['loan']['loan_spouse']);
			$data_content['loan']['spouse']['title'] = $this->title->getById($data_content['loan']['spouse']['title_id']);
			$data_content['loan']['spouse_name'] = $data_content['loan']['spouse']['title']['title_name'].$data_content['loan']['spouse']['person_fname'].'  '.$data_content['loan']['spouse']['person_lname'];
			$data_content['loan']['spouse_age'] = empty($data_content['loan']['spouse']['person_birthdate'])? '' : age($data_content['loan']['spouse']['person_birthdate']);
		}
		
		$data_content['asset_sum'] = 0;
		if(!empty($data_content['loan_asset']))
		{
			foreach ($data_content['loan_asset'] as $list_asset) 
			{
				$data_content['asset_sum'] += floatval($list_asset['loan_asset_amount']);
			}
		}
		
		$data_content['debt_sum'] = 0;
		if(!empty($data_content['loan_debt']))
		{
			foreach ($data_content['loan_debt'] as $list_debt)
			{
				$data_content['debt_sum'] += floatval($list_debt['loan_debt_amount']);
			}
		}
		
		if($type=='add')
		{
			$title_page = 'เพิ่มแบบพิจารณา';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->loan_analysis->getModel();
			$data_content['data']['loan_id'] = $id;
			$data_content['data']['loan_analysis_id'] = $id_analysis;
			$data_content['data']['loan_analysis_year'] = $data_content['loan']['loan_period_year'];
			
			$_SESSION['loan_analysis_list_cal'] = array();
			
			$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
		}
		else
		{
			$data_content['data'] = $this->loan_analysis->getById($id_analysis);
			
			$_SESSION['loan_analysis_list_cal'] = $this->loan_analysis_cal->getByAnalysis($id_analysis);
			
			if($data_content['data']['loan_analysis_cause']=='') $data_content['data']['loan_analysis_cause'] = $case;
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มแบบพิจารณา';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->loan_analysis->getModel();
				$data_content['data']['loan_id'] = $id;
				$data_content['data']['loan_analysis_id'] = $id_analysis;
				$data_content['data']['loan_analysis_year'] = $data_content['loan']['loan_period_year'];
				$data_content['data']['loan_analysis_remain'] = $data_content['loan']['loan_amount'];
				
				$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
			}
			else
			{
				$title_page = 'แก้ไขแบบพิจารณา';
				
				$data_content['data']['loan_id'] = $id;
				$data_content['data']['loan_analysis_id'] = $id_analysis;
				
				$schedule = $this->loan_schedule->getByLoan($id);
				
				if(!empty($schedule))
				{
					$list_round =  array();
					foreach ($schedule as $list_schedule)
					{
						$list_round[$list_schedule['loan_schedule_times']] = array(
								'principle' => floatval($list_schedule['loan_schedule_amount']),
								'other' => intval($list_schedule['loan_schedule_other']),
								'principle_other' => intval($list_schedule['loan_schedule_principle']),
								'interest_other' => intval($list_schedule['loan_schedule_interest'])
						);
					}
					
					$_SESSION['loan_analysis_cal'] = $list_round;
				}
				else
				{
					$this->cal_ana_add($data_content['loan']['loan_amount'], $data_content['data']['loan_analysis_length'], $data_content['loan']['loan_period_year']);
				}
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_analysis/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function expenditure($id, $id_analysis='', $type='')
	{
		$this->load->model('loan_model', 'loan');
		$this->load->model('loan_co_model', 'loan_co');
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		$this->load->model('loan_income_model', 'loan_income');
		
		$loan =  $this->loan->getById($id);
		$person[] = $loan['person_id'];
		
		$loan_co =  $this->loan_co->getByLoan($id);
		
		if(!empty($loan_co))
		{
			foreach ($loan_co as $list_co)
			{
				$person[] = $list_co['person_id'];
			}
		}
		
		$data_content['loan_income_1'] = $this->loan_income->getByPersonGroup($person, '1');
		$data_content['loan_income_2'] = $this->loan_income->getByPersonGroup($person, '2');
		$data_content['loan_income_3'] = $this->loan_income->getByPersonGroup($person, '3');
		$data_content['loan_expenditure_1'] = $this->loan_expenditure->getByPersonGroup($person, '1');
		$data_content['loan_expenditure_2'] = $this->loan_expenditure->getByPersonGroup($person, '2');
		
		if($type=='html')
		{
			return $this->parser->parse('expenditure_html', $data_content);
		}
		else
		{
			$this->parser->parse('expenditure', $data_content);
		}
	}
	
	public function cal_ana_add($amount, $type, $year)
	{
		$amount = floatval($amount);
		$type = intval($type);
		$year = intval($year);
		
		if($type==1)
		{
			$round = $year * 12;
			$income = $income / 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
			$income = $income / 2;
		}
		else
		{
			$round = $year;
		}
		
		$principle = $amount / $round;
		
		$list_round = array();
		for ($r=1; $r<=$round; $r++)
		{
			$list_round[$r] = array(
					'principle' => $principle,
					'other' => 0,
					'principle_other' => 0,
					'interest_other' => 0
			);
		}
		
		$_SESSION['loan_analysis_cal'] = $list_round;
	}
	
	public function cal_ana()
	{
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
		
		if($type==1)
		{
			$round = $year * 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
		}
		else
		{
			$round = $year;
		}
		
		$principle = $amount / $round;
		
		$data = array(
			'round' => $round,
			'principle' => $principle
		);
		
		$this->parser->parse('cal_ana', $data);
	}
	
	public function save_cal()
	{
		$round = $this->input->post('txtCalRound');
		$amount = $this->input->post('txtCalAmount');
		$other = $this->input->post('txtCalOther');
		$principle_other = $this->input->post('txtCalPrinciple');
		$interest_other = $this->input->post('txtCalInterest');
		
		$list_round = array();
		foreach ($round as $r_key => $r_value)
		{
			$list_round[$r_value] = array(
						'principle' => $amount[$r_key],
						'other' => $other[$r_key],
						'principle_other' => $principle_other[$r_key],
						'interest_other' => $interest_other[$r_key]
					);
		}
		
		$_SESSION['loan_analysis_cal'] = $list_round;
	}
	
	public function save_cal2()
	{
		$round = $this->input->post('txtCalRound');
		$amount = $this->input->post('txtCalAmount');
		$start_list = $this->input->post('txtCalStart');
		$end_list = $this->input->post('txtCalEnd');
		
		$list_cal = array();
		$list_round = array();
		foreach ($round as $r_key => $r_value)
		{
			$start = intval($start_list[$r_key]);
			$end = intval($end_list[$r_key]);
			
			$list_cal[] = array(
						'loan_analysis_cal_time' => $r_value,
						'loan_analysis_cal_start' => $start,
						'loan_analysis_cal_end' => $end,
						'loan_analysis_cal_amount' => $amount[$r_key]
					);
			
			for($r = $start; $r <= $end; $r++)
			{
				$list_round[$r] = $_SESSION['loan_analysis_cal'][$r];
				$list_round[$r]['principle'] = $amount[$r_key];
				
				if(empty($list_round[$r]['other'])) $list_round[$r]['other'] = 0;
				if(empty($list_round[$r]['principle_other'])) $list_round[$r]['principle_other'] = 0;
				if(empty($list_round[$r]['interest_other'])) $list_round[$r]['interest_other'] = 0;
			}
		}
		
		$_SESSION['loan_analysis_list_cal'] = $list_cal;
		$_SESSION['loan_analysis_cal'] = $list_round;
	}
	
	public function cal_ana2()
	{
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
		
		if($type==1)
		{
			$round = $year * 12;
		}
		else if($type==6)
		{
			$round = $year * 2;
		}
		else
		{
			$round = $year;
		}
		
		$data = array(
				'round' => $round,
				'amount' => $amount
		);
		
		$this->parser->parse('cal_ana2', $data);
	}
	
	public function cal()
	{
		$income = floatval(str_replace(",", "", $this->input->post('income')));
		$amount = floatval(str_replace(",", "", $this->input->post('amount')));
		$interest = intval($this->input->post('interest')) / 100;
		$type = intval($this->input->post('type'));
		$year = intval($this->input->post('year'));
		$pay = $this->input->post('pay');
		$start = $this->input->post('start');
		
		$start = convert_ymd_en($start);
		$pay = convert_ymd_en($pay);
		
		$date_start = $start;
		$date_start = new DateTime($date_start);
		$date_start->sub(new DateInterval('P1D'));
		$date_start = $date_start->format('Y-m-d');
		$date_end = $pay;
		
		$list_round = array();
		$list_month = array();
		$remain = $amount;
		if(!empty($_SESSION['loan_analysis_cal']))
		{
			foreach ($_SESSION['loan_analysis_cal'] as $r_key => $r_value)
			{
				$day_between = cal_day_between($date_start, $date_end);
				$interest_per = ($remain * $interest * $day_between) / 365;
					
				$list_round[$r_key] = array(
						'income' => $income,
						'principle' => $r_value['principle'],
						'remain' => $remain,
						'interest' => $interest_per,
						'other' => $r_value['other'],
						'principle_other' => $r_value['principle_other'],
						'interest_other' => $r_value['interest_other'],
						'date' => $date_end
				);
					
				list($y_end, $m_end, $d_end) = explode('-', $date_end);
				$list_month[] = $m_end;
					
				$remain -= $r_value['principle'];
				$date_start = $date_end;
				$date_end = new DateTime($date_end);
				$date_end->add(new DateInterval('P'.$type.'M'));
				$date_end = $date_end->format('Y-m-d');
			}
		}
		
		$list_month = array_unique($list_month);
		
		$month = '';
		foreach ($list_month as $data_month)
		{
			$month .= thai_month($data_month).', ';
		}
		
		if(!empty($month)) $month = substr($month, 0, -2);
		
		$data = array(
			'year' => $year,
			'month' => $month,
			'income' => $income,
			'amount' => $amount,
			'interest' => $interest,
			'type' => $type,
			'pay' => $pay,
			'start' => $start,
			'fiscal_start' => fiscalYear($start),
			'fiscal_end' => fiscalYear($date_start),
			'list_round' => $list_round
		);
		
		$_SESSION['loan_analysis'] = $data;
		
		$this->parser->parse('cal', $data);
	}
	
	public function save_expenditure()
	{
		$this->load->model('loan_expenditure_model', 'loan_expenditure');
		
		$array['loan_expenditure_amount'.$this->input->post('year')] = forNumberInt($this->input->post('amount'));
		
		$result = $this->loan_expenditure->save($array, $this->input->post('id'));
	}
	
	public function save_other()
	{
		$_SESSION['loan_analysis_cal'][$this->input->post('round')]['other'] = forNumberInt($this->input->post('other'));
	}
	
	public function save_principle_other()
	{
		$_SESSION['loan_analysis_cal'][$this->input->post('round')]['principle_other'] = forNumberInt($this->input->post('principle_other'));
	}
	
	public function save_interest_other()
	{
		$_SESSION['loan_analysis_cal'][$this->input->post('round')]['interest_other'] = forNumberInt($this->input->post('interest_other'));
	}
	
	public function save()
	{
		$this->load->model('loan_analysis_model', 'loan_analysis');
		
		$chkEgov = empty($this->input->post('chkEgov'))? '0' : '1';
		$chkCooperative = empty($this->input->post('chkCooperative'))? '0' : '1';
		$chkBank = empty($this->input->post('chkBank'))? '0' : '1';
		$chkBankOther = empty($this->input->post('chkBankOther'))? '0' : '1';
		
		$array['loan_id'] = $this->input->post('txtLoan');
		$array['loan_analysis_egov'] = $chkEgov;
		$array['loan_analysis_egov_detail'] = $this->input->post('txtEgov');
		$array['loan_analysis_cooperative'] = $chkCooperative;
		$array['loan_analysis_cooperative_detail'] = $this->input->post('txtCooperative');
		$array['loan_analysis_bank'] = $chkBank;
		$array['loan_analysis_bank_detail'] = $this->input->post('txtBank');
		$array['loan_analysis_bankother'] = $chkBankOther;
		$array['loan_analysis_bankother_detail'] = $this->input->post('txtBankOther');
		$array['loan_analysis_behavior'] = $this->input->post('rdBehavior');
		$array['loan_analysis_behavior_detail'] = $this->input->post('txtBehavior');
		$array['loan_analysis_behavior_occupation'] = $this->input->post('rdBehaviorOccupation');
		$array['loan_analysis_behavior_occupation_detail'] = $this->input->post('txtBehaviorOccupation');
		$array['loan_analysis_behavior_farm'] = $this->input->post('rdBehaviorFarm');
		$array['loan_analysis_behavior_farm_detail'] = $this->input->post('txtBehaviorFarm');
		$array['loan_analysis_behavior_health'] = $this->input->post('rdBehaviorHealth');
		$array['loan_analysis_behavior_health_detail'] = $this->input->post('txtBehaviorHealth');
		$array['loan_analysis_behavior_other'] = $this->input->post('txtBehaviorOther');
		$array['loan_analysis_deadline'] = convert_ymd_en($this->input->post('txtDeadline'));
		$array['loan_analysis_remain'] = forNumberInt($this->input->post('txtRemain'));
		$array['loan_analysis_within'] = convert_ymd_en($this->input->post('txtWithin'));
		$array['loan_analysis_cause'] = $this->input->post('txtCause');
		$array['loan_analysis_interest'] = $this->input->post('ddlCalInterest');
		$array['loan_analysis_length'] = $this->input->post('ddlCalType');
		$array['loan_analysis_year'] = $this->input->post('txtShowYear');
		$array['loan_analysis_start'] = convert_ymd_en($this->input->post('txtCalStart'));
		$array['loan_analysis_pay'] = convert_ymd_en($this->input->post('txtCalPay'));
		$array['loan_analysis_amount'] = forNumberInt($this->input->post('txtCalAmount'));
		$array['loan_analysis_investigate_comment'] = $this->input->post('txtInvestigateComment');
		$array['loan_analysis_investigate_honestly'] = $this->input->post('txtInvestigateHonestly');
		$array['loan_analysis_investigate_cause'] = $this->input->post('txtInvestigateCause');
		$array['loan_analysis_investigate_must'] = $this->input->post('txtInvestigateMust');
		$array['loan_analysis_investigate_matter'] = $this->input->post('txtInvestigateMatter');
		$array['loan_analysis_approve_status'] = $this->input->post('rdApproveStatus');
		$array['loan_analysis_approve_amount'] = forNumberInt($this->input->post('txtApproveAmount'));
		$array['loan_analysis_approve_date'] = convert_ymd_en($this->input->post('txtApproveDate'));
		$array['loan_analysis_approve_conditions'] = $this->input->post('txtApproveConditions');
		$array['loan_analysis_approve_not_type'] = $this->input->post('rdApproveNotType');
		$array['loan_analysis_approve_not_year'] = $this->input->post('txtApproveNotYear');
		$array['loan_analysis_approve_not_case'] = $this->input->post('txtApproveNotCase');
		$array['loan_analysis_approve_sector_status'] = $this->input->post('rdApproveSectorStatus');
		$array['loan_analysis_approve_sector_amount'] = forNumberInt($this->input->post('txtApproveSectorAmount'));
		$array['loan_analysis_approve_sector_date'] = convert_ymd_en($this->input->post('txtApproveSectorDate'));
		$array['loan_analysis_approve_sector_conditions'] = $this->input->post('txtApproveSectorConditions');
		$array['loan_analysis_approve_sector_not_case'] = $this->input->post('txtApproveSectorNotCase');
		$array['loan_analysis_income'] = $this->input->post('txtYear'.$this->input->post('ddlCalIncomeYear').'Sum');
		$array['loan_analysis_income_year'] = $this->input->post('ddlCalIncomeYear');
		$array['loan_analysis_status'] = '1';
		
		$id = $this->loan_analysis->save($array, $this->input->post('txtId'));
		
		if($this->input->post('txtId')=='')
		{
			$data = array(
					'alert' => 'บันทึกแบบประเมินเรียบร้อยแล้ว',
					'link' => site_url('analysis'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขแบบประเมินเรียบร้อยแล้ว',
					'link' => site_url('analysis'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	
	public function check()
	{
		echo '<pre>';
		print_r($_SESSION['loan_analysis']);
		echo '</pre>';
	}
}
?>