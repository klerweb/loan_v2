<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_income_model extends CI_Model
{
	private $table = 'loan_income';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByPerson($person_id, $type='')
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_income.person_id", $person_id);
		$this->db->where("loan_income.loan_activity_status !=", "0");
	
		if($type != '') $this->db->where("loan_income.loan_activity_type", $type);
	
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function getByPersonGroup($person, $type='')
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where_in("loan_income.person_id", $person);
		$this->db->where("loan_income.loan_activity_status !=", "0");
	
		if($type != '') $this->db->where("loan_income.loan_activity_type", $type);
	
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>