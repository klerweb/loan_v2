<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_debt_model extends CI_Model
{
	private $table = 'loan_debt';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_debt.loan_id", $loan_id);
		$this->db->where("loan_debt.loan_debt_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function saveByLoan($array, $loan, $name)
	{
		$this->db->set($array);
		$this->db->set("loan_debt_updatedate", "NOW()", FALSE);
		$this->db->where("loan_id", $loan);
		$this->db->where("loan_debt_name", $name);
		$this->db->update($this->table, $array);
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>