<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_expenditure_model extends CI_Model
{
	private $table = 'loan_expenditure';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByPerson($person_id, $type='')
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_expenditure.person_id", $person_id);
		$this->db->where("loan_expenditure.loan_expenditure_status !=", "0");
		
		if($type != '') $this->db->where("loan_expenditure.loan_expenditure_type", $type);
	
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function getByPersonGroup($person, $type='')
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where_in("loan_expenditure.person_id", $person);
		$this->db->where("loan_expenditure.loan_expenditure_status !=", "0");
	
		if($type != '') $this->db->where("loan_expenditure.loan_expenditure_type", $type);
	
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function save($array, $id='')
	{
		if($id=='')
		{
			$this->db->set($array);
			$this->db->set("loan_expenditure_createdate", "NOW()", FALSE);
			$this->db->set("loan_expenditure_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
	
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("loan_expenditure_updatedate", "NOW()", FALSE);
			$this->db->where("loan_expenditure_id", $id);
			$this->db->update($this->table, $array);
		}
	
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>