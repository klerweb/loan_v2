<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_model extends CI_Model
{
	private $table = 'loan';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_id", $id);
		$this->db->where("loan_status !=", "0");
		$query = $this->db->get();
		
		if($query->num_rows()!=0)
		{	
			$result = $query->row_array();
			
			$result['loan_datefull'] = thai_display_date($result['loan_date']);
			
			$this->load->model('person/person_model', 'person');
			$result['person'] = $this->person->getById($result['person_id']);
		}
		else
		{
			$result = null;
		}

		return $result;
	}
}
?>