<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_guarantee_land_model extends CI_Model
{
	private $table = 'loan_guarantee_land';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("land", "land.land_id = loan_guarantee_land.land_id", "LEFT");
		$this->db->join("config_land_type", "config_land_type.land_type_id = land.land_type_id", "LEFT");
		$this->db->join("master_district", "master_district.district_id = land.land_addr_district_id", "LEFT");
		$this->db->join("master_amphur", "master_amphur.amphur_id = land.land_addr_amphur_id", "LEFT");
		$this->db->join("master_province", "master_province.province_id = land.land_addr_province_id", "LEFT");
		$this->db->where("loan_guarantee_land.loan_id", $loan_id);
		$this->db->where("loan_guarantee_land.loan_guarantee_land_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>