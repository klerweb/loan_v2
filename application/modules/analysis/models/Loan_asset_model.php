<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_asset_model extends CI_Model
{
	private $table = 'loan_asset';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_asset.loan_id", $loan_id);
		$this->db->where("loan_asset.loan_asset_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>