<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_guarantee_bondsman_model extends CI_Model
{
	private $table = 'loan_guarantee_bondsman';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("person", "person.person_id = loan_guarantee_bondsman.person_id", "RIGHT");
		$this->db->join("config_title", "config_title.title_id = person.title_id", "LEFT");
		$this->db->join("config_career", "config_career.career_id = person.career_id", "LEFT");
		$this->db->join("master_district", "master_district.district_id = person.person_addr_pre_district_id", "LEFT");
		$this->db->join("master_amphur", "master_amphur.amphur_id = person.person_addr_pre_amphur_id", "LEFT");
		$this->db->join("master_province", "master_province.province_id = person.person_addr_pre_province_id", "LEFT");
		$this->db->where("loan_guarantee_bondsman.loan_id", $loan_id);
		$this->db->where("loan_guarantee_bondsman.loan_bondsman_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>