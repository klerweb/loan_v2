<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Land_owner_model extends CI_Model
{
	private $table = 'land_owner';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLand($land_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("person", "person.person_id = land_owner.person_id", "RIGHT");
		$this->db->join("config_title", "config_title.title_id = person.title_id", "LEFT");
		$this->db->where("land_owner.land_id", $land_id);
		$this->db->where("land_owner.land_owner_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>