<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_co_model extends CI_Model
{
	private $table = 'loan_co';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("person", "person.person_id = loan_co.person_id", "RIGHT");
		$this->db->join("config_title", "config_title.title_id = person.title_id", "LEFT");
		$this->db->join("config_career", "config_career.career_id = person.career_id", "LEFT");
		$this->db->join("master_district", "master_district.district_id = person.person_addr_pre_district_id", "LEFT");
		$this->db->join("master_amphur", "master_amphur.amphur_id = person.person_addr_pre_amphur_id", "LEFT");
		$this->db->join("master_province", "master_province.province_id = person.person_addr_pre_province_id", "LEFT");
		$this->db->join("config_relationship", "config_relationship.relationship_id = loan_co.relationship_id", "LEFT");
		$this->db->where("loan_co.loan_id", $loan_id);
		$this->db->where("loan_co.loan_co_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>