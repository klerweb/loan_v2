<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Land_assess_model extends CI_Model
{
	private $table = 'land_assess';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLand($land)
	{	
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("land_assess.land_assess_status !=", "0");
		$this->db->where("land_assess.land_id", $land);
		
		$query = $this->db->get();
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>