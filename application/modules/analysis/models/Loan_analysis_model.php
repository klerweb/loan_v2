<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_analysis_model extends CI_Model
{
	private $table = 'loan_analysis';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_analysis_status", "1");
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_analysis_id", $id);
		$this->db->where("loan_analysis_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_analysis_createby", get_uid_login());
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
			
			$result['loan_analysis_deadline_full'] = empty($result['loan_analysis_deadline'])? '' : thai_display_date($result['loan_analysis_deadline']);
			$result['loan_analysis_deadline'] = empty($result['loan_analysis_deadline'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_deadline']);
			$result['loan_analysis_within_full'] = empty($result['loan_analysis_within'])? '' : thai_display_date($result['loan_analysis_within']);
			$result['loan_analysis_within'] = empty($result['loan_analysis_within'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_within']);
			$result['loan_analysis_start'] = empty($result['loan_analysis_start'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_start']);
			$result['loan_analysis_pay'] = empty($result['loan_analysis_pay'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_pay']);
			$result['loan_analysis_approve_date'] = empty($result['loan_analysis_approve_date'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_approve_date']);
			$result['loan_analysis_approve_sector_date'] = empty($result['loan_analysis_approve_sector_date'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_approve_sector_date']);
			$result['loan_analysis_income_year'] = empty($result['loan_analysis_income_year'])? '3' : $result['loan_analysis_income_year'];
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_id", $loan_id);
		$this->db->where("loan_analysis_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_analysis_createby", get_uid_login());
		$query = $this->db->get();
	
		if($query->num_rows()!=0)
		{
			$result = $query->row_array();
				
			$result['loan_analysis_deadline'] = empty($result['loan_analysis_deadline'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_deadline']);
			$result['loan_analysis_within'] = empty($result['loan_analysis_within'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_within']);
			$result['loan_analysis_start'] = empty($result['loan_analysis_start'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_start']);
			$result['loan_analysis_pay'] = empty($result['loan_analysis_pay'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_pay']);
			$result['loan_analysis_approve_date'] = empty($result['loan_analysis_approve_date'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_approve_date']);
			$result['loan_analysis_approve_sector_date'] = empty($result['loan_analysis_approve_sector_date'])? date('d/m/').(intval(date('Y'))+543) : convert_dmy_th($result['loan_analysis_approve_sector_date']);
			$result['loan_analysis_income_year'] = empty($result['loan_analysis_income_year'])? '3' : $result['loan_analysis_income_year'];
		}
		else
		{
			$result = null;
		}
	
		return $result;
	}
	
	public function search($loan='', $thaiid='', $fullname='')
	{
		$where = '';
		if($loan!='') $where .= "OR loan.loan_code LIKE '%".$loan."%' ";
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("loan", "loan.loan_id = loan_analysis.loan_id", "RIGHT");
		$this->db->join("person", "loan.person_id = person.person_id", "RIGHT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->where("loan_analysis.loan_analysis_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("loan_analysis.loan_analysis_createby", get_uid_login());
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
	
	public function save($array, $id='')
	{
		$this->load->model('loan_schedule_model', 'loan_schedule');
		$this->load->model('loan_analysis_cal_model', 'loan_analysis_cal');
		
		if($id=='')
		{
			$this->db->set($array);
			$this->db->set("loan_analysis_createby", get_uid_login());
			$this->db->set("loan_analysis_createdate", "NOW()", FALSE);
			$this->db->set("loan_analysis_updateby", get_uid_login());
			$this->db->set("loan_analysis_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
				
			$id = $this->db->insert_id();
		}
		else
		{
			$this->db->set($array);
			$this->db->set("loan_analysis_updateby", get_uid_login());
			$this->db->set("loan_analysis_updatedate", "NOW()", FALSE);
			$this->db->where("loan_analysis_id", $id);
			$this->db->update($this->table, $array);
			
			$this->loan_schedule->delByLoan($array['loan_id']);
			$this->loan_analysis_cal->delByLoan($array['loan_id']);
		}
		
		if(!empty($_SESSION['loan_analysis_list_cal']))
		{
			foreach ($_SESSION['loan_analysis_list_cal'] as $key => $cal)
			{
				$array_cal['loan_id'] = $this->input->post('txtLoan');
				$array_cal['loan_analysis_id'] = $id;
				$array_cal['loan_analysis_cal_time'] = $cal['loan_analysis_cal_time'];
				$array_cal['loan_analysis_cal_start'] = $cal['loan_analysis_cal_start'];
				$array_cal['loan_analysis_cal_end'] = $cal['loan_analysis_cal_end'];
				$array_cal['loan_analysis_cal_amount'] = $cal['loan_analysis_cal_amount'];
					
				$this->loan_analysis_cal->save($array_cal);
			}
		}
		
		if(!empty($_SESSION['loan_analysis']['list_round']))
		{
			foreach ($_SESSION['loan_analysis']['list_round'] as $key => $schedule)
			{
				$array_schedule['loan_id'] = $this->input->post('txtLoan');
				$array_schedule['loan_analysis_id'] = $id;
				$array_schedule['loan_schedule_times'] = $key;
				$array_schedule['loan_schedule_date'] = $schedule['date'];
				$array_schedule['loan_schedule_amount'] = $schedule['principle'];
				$array_schedule['loan_schedule_other'] = $schedule['other'];
				$array_schedule['loan_schedule_principle'] = $schedule['principle_other'];
				$array_schedule['loan_schedule_interest'] = $schedule['interest_other'];
				$array_schedule['loan_schedule_payment_interest'] = $schedule['interest'];
					
				$this->loan_schedule->save($array_schedule);
			}
		}
		
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function getModel()
	{
		$data['loan_id'] = '';
		$data['loan_summary_id'] = '';
		$data['loan_analysis_egov'] = '0';
		$data['loan_analysis_egov_detail'] = '';
		$data['loan_analysis_cooperative'] = '0';
		$data['loan_analysis_cooperative_detail'] = '';
		$data['loan_analysis_bank'] = '0';
		$data['loan_analysis_bank_detail'] = '';
		$data['loan_analysis_bankother'] = '0';
		$data['loan_analysis_bankother_detail'] = '';
		$data['loan_analysis_behavior'] = '1';
		$data['loan_analysis_behavior_detail'] = '';
		$data['loan_analysis_behavior_occupation'] = '1';
		$data['loan_analysis_behavior_occupation_detail'] = '';
		$data['loan_analysis_behavior_farm'] = '1';
		$data['loan_analysis_behavior_farm_detail'] = '';
		$data['loan_analysis_behavior_health'] = '1';
		$data['loan_analysis_behavior_health_detail'] = '';
		$data['loan_analysis_behavior_other'] = '';
		$data['loan_analysis_deadline'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_analysis_remain'] = '0';
		$data['loan_analysis_within'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_analysis_cause'] = '';
		$data['loan_analysis_year'] = '30';
		$data['loan_analysis_interest'] = '3';
		$data['loan_analysis_length'] = '12';
		$data['loan_analysis_start'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_analysis_pay'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_analysis_amount'] = '0';
		$data['loan_analysis_investigate_comment'] = '';
		$data['loan_analysis_investigate_honestly'] = '';
		$data['loan_analysis_investigate_cause'] = '';
		$data['loan_analysis_investigate_must'] = '';
		$data['loan_analysis_investigate_matter'] = '';
		$data['loan_analysis_approve_status'] = '1';
		$data['loan_analysis_approve_amount'] = '0';
		$data['loan_analysis_approve_date'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_analysis_approve_conditions'] = '';
		$data['loan_analysis_approve_not_type'] = '1';
		$data['loan_analysis_approve_not_year'] = '';
		$data['loan_analysis_approve_not_case'] = '';
		$data['loan_analysis_approve_sector_status'] = '1';
		$data['loan_analysis_approve_sector_amount'] = '0';
		$data['loan_analysis_approve_sector_date'] = date('d/m/').(intval(date('Y'))+543);
		$data['loan_analysis_approve_sector_conditions'] = '';
		$data['loan_analysis_approve_sector_not_case'] = '';
		$data['loan_analysis_status'] = '1';
		$data['loan_analysis_income'] = '0';
		$data['loan_analysis_income_year'] = '3';
	
		return $data;
	}
}
?>