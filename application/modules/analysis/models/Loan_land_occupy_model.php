<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_land_occupy_model extends CI_Model
{
	private $table = 'loan_land_occupy';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_land_occupy.loan_id", $loan_id);
		$this->db->where("loan_land_occupy.loan_land_occupy_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>