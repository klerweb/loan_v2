<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_guarantee_bookbank_model extends CI_Model
{
	private $table = 'loan_guarantee_bookbank';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("config_bank", "config_bank.bank_id = loan_guarantee_bookbank.bank_id", "LEFT");
		$this->db->where("loan_guarantee_bookbank.loan_id", $loan_id);
		$this->db->where("loan_guarantee_bookbank.loan_bookbank_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>