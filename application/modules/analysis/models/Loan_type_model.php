<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_type_model extends CI_Model
{
	private $table = 'loan_type';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("master_loan_objective", "loan_type.loan_objective_id = master_loan_objective.loan_objective_id", "RIGHT");
		$this->db->where("loan_type.loan_id", $loan_id);
		$this->db->where("loan_type.loan_type_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
	
	public function save($array, $id)
	{
		$this->db->set($array);
		$this->db->set("loan_type_createdate", "NOW()", FALSE);
		$this->db->where("loan_type_id", $id);
		$this->db->update($this->table, $array);
		
		$num_row = $this->db->affected_rows();
	
		return array('id' => $id, 'rows' => $num_row);
	}
}
?>