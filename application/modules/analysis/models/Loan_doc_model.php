<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_doc_model extends CI_Model
{
	private $table = 'loan_doc';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("config_loan_doc_type", "config_loan_doc_type.loan_doc_type_id = loan_doc.loan_doc_type_id", "LEFT");
		$this->db->where("loan_doc.loan_id", $loan_id);
		$this->db->where("loan_doc.loan_doc_status !=", "0");
		
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
	
		return $result;
	}
}
?>