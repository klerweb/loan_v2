<?php 
	$html = '';
	$show_first = 0;
	if(!empty($list_round))
	{ 
		$remain = 0;
		$i_round = 1;
		$i_tab = 1;
		$i_start = 1;
		$c_round = 1;
		$count_round = count($list_round);
				
		$html_round = '';
		$html_1 = '';
		$html_1_1 = '';
		$html_1_2 = '';
		$html_1_3 = '';
		$html_1_4 = '';
		$html_1_5 = '';
		$html_1_6 = '';
		$html_2 = '';
		$html_2_1 = '';
		$html_2_2 = '';
		$html_2_3 = '';
		$html_2_4 = '';
		$html_2_5 = '';
		$html_2_6 = '';
		$html_2_7 = '';
		$html_2_8 = '';
		$html_2_9 = '';
		$html_3 = '';
		$html_4 = '';
		
		foreach ($list_round as $key_round => $round)
		{
			$data1_4 = $round['other'];
			if($key_round=='1')
			{
				$data1_2 = $amount;
				$data1_6 = $round['income'] + $data1_2 + $data1_4;
				$data2_2 = $amount;
						
				$show_first = $round['principle'] + $round['interest'];
			}
			else
			{
				$data1_2 = 0;
				$data1_6 = $round['income'] + $data1_4;
				$data2_2 = 0;
			}
					
			$data2_3 = $round['principle'];
			$data2_4 = $round['interest'];
			$data2_7 = $round['principle_other'];
			$data2_8 = $round['interest_other'];
						
			$data2_9 = $data2_2 + $data2_3 + $data2_4 + $data2_7 + $data2_8;
			$data3 = $data1_6 - $data2_9;
						
			$remain += $data3;
						
			$html_round .= '<th style="text-align:center;">'.num2Thai($key_round).'</th>';
			$html_1 .= '<td align="center">xx</td>';
			$html_1_1 .= '<td align="right">'.num2Thai(number_format($round['income'], 2)).'</td>';
			$html_1_2 .= '<td align="right">'.num2Thai(number_format($data1_2, 2)).'</td>';
			$html_1_3 .= '<td align="right">๐.๐๐</td>';
			$html_1_4 .= '<td align="right">'.num2Thai(number_format($data1_4, 2)).'</td>';
			$html_1_5 .= '<td align="right">๐.๐๐</td>';
			$html_1_6 .= '<td align="right">'.num2Thai(number_format($data1_6, 2)).'</td>';
			$html_2 .= '<td></td>';
			$html_2_1 .= '<td align="right">'.num2Thai(number_format($round['remain'], 2)).'</td>';
			$html_2_2 .= '<td align="right">'.num2Thai(number_format($data2_2, 2)).'</td>';
			$html_2_3 .= '<td align="right">'.num2Thai(number_format($data2_3, 2)).'</td>';
			$html_2_4 .= '<td align="right">'.num2Thai(number_format($data2_4, 2)).'</td>';
			$html_2_5 .= '<td align="right">๐.๐๐</td>';
			$html_2_6 .= '<td align="right">๐.๐๐</td>';
			$html_2_7 .= '<td align="right">'.num2Thai(number_format($data2_7, 2)).'</td>';
			$html_2_8 .= '<td align="right">'.num2Thai(number_format($data2_8, 2)).'</td>';
			$html_2_9 .= '<td align="right">'.num2Thai(number_format($data2_9, 2)).'</td>';
			$html_3 .= '<td align="right">'.num2Thai(number_format($data3, 2)).'</td>';
			$html_4 .= '<td align="right">'.num2Thai(number_format($remain, 2)).'</td>';
					
			if($i_round%5==0 || $i_round==$count_round)
			{
				$html .= '<table width="100%" border="1" cellpadding="0" cellspacing="0">
								<tr>
									<th rowspan="2" style="text-align:center; vertical-align: middle;">รายการ</th>
									<th colspan="'.$c_round.'" style="text-align:center;">งวดที่ (ตั้งแต่งวดที่ '.num2Thai($i_start).' จนถึงงวดที่ '.num2Thai($i_round).')</th>
								</tr>
								<tr>
									'.$html_round.'
								</tr>
								<tr><td>๑. เงินสดรับ (บาท)</td>'.$html_1.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๑) รายได้สุทธิ</td>'.$html_1_1.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๒) สินเชื่อของ บจธ.</td>'.$html_1_2.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๓) สินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม (บจธ.)</td>'.$html_1_3.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๔) เงินกู้จากเจ้าหนี้อื่นๆ</td>'.$html_1_4.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๕) เงินทุนสมทบของตนเอง (ถ้ามี)</td>'.$html_1_5.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๖) รวมเงินสดรับ (๑)+(๒)+(๓)+(๔)+(๕)</td>'.$html_1_6.'</tr>
								<tr><td>๒. เงินสดจ่าย (บาท)</td>'.$html_2.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๑) สินเชื่อ บจธ. คงเหลือ</td>'.$html_2_1.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๒) ชำระหนี้สินเดิมให้เจ้าหนี้</td>'.$html_2_2.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๓) ชำระต้นเงินสินเชื่อของ บจธ.</td>'.$html_2_3.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๔) ชำระดอกเบี้ยของสินเชื่อของ บจธ.</td>'.$html_2_4.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๕) ชำระต้นเงินสินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม</td>'.$html_2_5.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๖) ชำระดอกเบี้ยสินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม</td>'.$html_2_6.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๗) ชำระดอกเบี้ยต้นเงินหนี้อื่นๆ</td>'.$html_2_7.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๘) ชำระดอกเบี้ยหนี้อื่นๆ</td>'.$html_2_8.'</tr>
								<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;(๙) รวมเงินสดจ่าย (๒)+(๓)+(๔)+(๕)+(๖)+(๗)+(๘)</td>'.$html_2_9.'</tr>
								<tr><td>๓. เงินสดคงเหลือ ๑ (๖) - ๒ (๙)</td>'.$html_3.'</tr>
								<tr><td>๔. เงินสดคงเหลือสะสม</td>'.$html_4.'</tr>
						</table><br/><br/>';
						
				$i_tab++;
				$i_start = $i_round + 1;
				$c_round = 1;
							
				$html_round = '';
				$html_1 = '';
				$html_1_1 = '';
				$html_1_2 = '';
				$html_1_3 = '';
				$html_1_4 = '';
				$html_1_5 = '';
				$html_1_6 = '';
				$html_2 = '';
				$html_2_1 = '';
				$html_2_2 = '';
				$html_2_3 = '';
				$html_2_4 = '';
				$html_2_5 = '';
				$html_2_6 = '';
				$html_2_7 = '';
				$html_2_8 = '';
				$html_2_9 = '';
				$html_3 = '';
				$html_4 = '';
			}
			else
			{
				$c_round++;
			}
					
			$i_round++;
		}
	} 
?>
<?php echo $html; ?>
<?php 
	switch ($type)
	{
		case 1: $type = 12;
			break;
		case 6: $type = 2;
			break;
		case 12: $type = 1;
			break;
	}
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="10%"></td>
		<td width="90%">๖.๕&nbsp;&nbsp;จากประมาณการกระแสเงินสดข้างต้น ผู้ขอสินเชื่อชำระคืนเงินต้นเสร็จสิ้นภายใน <?php echo num2Thai($year); ?>  ปี</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>ตั้งแต่ปีบัญชี <?php echo num2Thai($fiscal_start); ?> ถึงปีบัญชี <?php echo num2Thai($fiscal_end); ?> ปี โดยชำระต้นเงินและดอกเบี้ยได้ เป็นเงินปีละประมาณ <?php echo num2Thai(number_format($show_first, 2)) ;?> บาท และชำระหนี้ได้ปีละ <?php echo num2Thai($type); ?> ครั้ง ในแต่ละปีชำระได้ในเดือน <?php echo $month; ?></td>
	</tr>
</table>