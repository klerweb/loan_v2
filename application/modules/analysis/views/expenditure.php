<?php 
	$count_income_1 = count($loan_income_1);
	$count_income_2 = count($loan_income_2);
	$count_income_3 = count($loan_income_3);
	$count_expenditure_1 = count($loan_expenditure_1);
	$count_expenditure_2 = count($loan_expenditure_2);
	$count_expenditure_all = $count_expenditure_1 > $count_expenditure_2? $count_expenditure_1 : $count_expenditure_2;
			
	$cal_all = array();
	$sum_all = array(
			'year1' => 0,
			'year2' => 0,
			'year3' => 0,
		);
			
	$td_len = $count_income_1>=$count_income_2? $count_income_1 : $count_income_2;
	if($td_len<$count_income_3) $td_len =  $count_income_3;
	if($td_len<$count_expenditure_1) $td_len =  $count_expenditure_1;
	if($td_len<$count_expenditure_2) $td_len =  $count_expenditure_2;
			
	$td_len++;
			
	$td_income_1 = '';
	$td_income_2 = '';
	$td_income_3 = '';
	$td_expenditure = '';
	for($i_len = 1; $i_len <= $td_len; $i_len++)
	{
		if($i_len==$td_len)
		{
			$td_income_1 .= '<th style="text-align:center;">รวม</th>';
			$td_income_2 .= '<th style="text-align:center;">xx</th>';
			$td_income_3 .= '<th style="text-align:center;">xx</th>';
			$td_expenditure .= '<th style="text-align:center;">xx</th>';
		}
		else
		{
			$td_income_1 .= '<th style="text-align:center;">xx</th>';
			$td_income_2 .= '<th style="text-align:center;">xx</th>';
			$td_income_3 .= '<th style="text-align:center;">xx</th>';
			$td_expenditure .= '<th style="text-align:center;">xx</th>';
		}
	}	

	$html_year1_income1 = '';
	$html_year2_income1 = '';
	$html_year3_income1 = '';
	if(!empty($loan_income_1)) 
	{
		$sum_year1_income1 = 0;
		$sum_year2_income1 = 0;
		$sum_year3_income1 = 0;
		$sum_year1_expenditure1 = 0;
		$sum_year2_expenditure1 = 0;
		$sum_year3_expenditure1 = 0;
		$sum_year1_sum1 = 0;
		$sum_year2_sum1 = 0;
		$sum_year3_sum1 = 0;
		$td_income1_year1_1 = '';
		$td_income1_year2_1 = '';
		$td_income1_year3_1 = '';
		$td_income1_year1_2 = '';
		$td_income1_year2_2 = '';
		$td_income1_year3_2 = '';
		$td_income1_year1_3 = '';
		$td_income1_year2_3 = '';
		$td_income1_year3_3 = '';
		$td_income1_year1_4 = '';
		$td_income1_year2_4 = '';
		$td_income1_year3_4 = '';
		$td_income1_year1_5 = '';
		$td_income1_year2_5 = '';
		$td_income1_year3_5 = '';
		$td_income1_year1_6 = '';
		$td_income1_year2_6 = '';
		$td_income1_year3_6 = '';
			
		$i_income_1 = 0;
			
		foreach ($loan_income_1 as $list_income_1)
		{
			$year1_income = floatval($list_income_1['loan_activity_income1']);
			$year2_income = floatval($list_income_1['loan_activity_income2']);
			$year3_income = floatval($list_income_1['loan_activity_income3']);
		
			$sum_year1_income1 += $year1_income;
			$sum_year2_income1 += $year2_income;
			$sum_year3_income1 += $year3_income;
		
			$year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_1['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_1['loan_activity_expenditure3']);
				
			$sum_year1_expenditure1 += $year1_expenditure;
			$sum_year2_expenditure1 += $year2_expenditure;
			$sum_year3_expenditure1 += $year3_expenditure;
		
			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;
		
			$sum_year1_sum1 += $year1_sum;
			$sum_year2_sum1 += $year2_sum;
			$sum_year3_sum1 += $year3_sum;
				
			$rai1 = intval($list_income_1['loan_activity_self_rai']) + intval($list_income_1['loan_activity_renting_rai']) + intval($list_income_1['loan_activity_approve_rai']);
			$ngan1 = intval($list_income_1['loan_activity_self_ngan']) + intval($list_income_1['loan_activity_renting_ngan']) + intval($list_income_1['loan_activity_approve_ngan']);
			$wah1 = floatval($list_income_1['loan_activity_self_wah']) + floatval($list_income_1['loan_activity_renting_wah']) + floatval($list_income_1['loan_activity_approve_wah']);
				
			$area1 = area($rai1, $ngan1, $wah1);
				
			$td_income1_year1_1 .= '<td align="center">'.$list_income_1['loan_activity_name'].'</td>';
			$td_income1_year2_1 .= '<td align="center">'.$list_income_1['loan_activity_name'].'</td>';
			$td_income1_year3_1 .= '<td align="center">'.$list_income_1['loan_activity_name'].'</td>';
			$td_income1_year1_2 .= '<td align="center">1 ปี</td>';
			$td_income1_year2_2 .= '<td align="center">1 ปี</td>';
			$td_income1_year3_2 .= '<td align="center">1 ปี</td>';
			$td_income1_year1_3 .= '<td align="center">'.$area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah'].' ไร่</td>';
			$td_income1_year2_3 .= '<td align="center">'.$area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah'].' ไร่</td>';
			$td_income1_year3_3 .= '<td align="center">'.$area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah'].' ไร่</td>';
			$td_income1_year1_4 .= '<td align="right">'.number_format($year1_income, 2).'</td>';
			$td_income1_year2_4 .= '<td align="right">'.number_format($year2_income, 2).'</td>';
			$td_income1_year3_4 .= '<td align="right">'.number_format($year3_income, 2).'</td>';
			$td_income1_year1_5 .= '<td align="right">'.number_format($year1_expenditure, 2).'</td>';
			$td_income1_year2_5 .= '<td align="right">'.number_format($year2_expenditure, 2).'</td>';
			$td_income1_year3_5 .= '<td align="right">'.number_format($year3_expenditure, 2).'</td>';
			$td_income1_year1_6 .= '<td align="right">'.number_format($year1_sum, 2).'</td>';
			$td_income1_year2_6 .= '<td align="right">'.number_format($year2_sum, 2).'</td>';
			$td_income1_year3_6 .= '<td align="right">'.number_format($year3_sum, 2).'</td>';
		
			$cal_all[$i_income_1++] = array(
					'year1' => $year1_sum,
					'year2' => $year2_sum,
					'year3' => $year3_sum
			);
		}
		
		$td_null_income1 = '';
		if($count_income_1 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_1;
			$td_null_income1 = str_repeat('<td align="center">xx</td>', $count_null);
			 
			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				$cal_all[$i_income_1++] = array(
						'year1' => 0,
						'year2' => 0,
						'year3' => 0
					);
			}
		}
		
		$sum_all['year1'] += $sum_year1_sum1;
		$sum_all['year2'] += $sum_year2_sum1;
		$sum_all['year3'] += $sum_year3_sum1;
		
		$html_year1_income1 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทการผลิต</td>
				'.$td_income1_year1_1.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลาการผลิต (เดือน/ปี)</td>
				'.$td_income1_year1_2.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income1_year1_3.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) รายได้ก่อนหักค่าใช้จ่าย (บาท)</td>
				'.$td_income1_year1_4.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year1_income1, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) ค่าใช้จ่ายการเกษตร (บาท)</td>
				'.$td_income1_year1_5.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year1_expenditure1, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(6) รายได้จากการเกษตรสุทธิ (4) - (5)</td>
				'.$td_income1_year1_6.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year1_sum1, 2).'</td>
			</tr>';  
		
		$html_year2_income1 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทการเกษตร</td>
				'.$td_income1_year2_1.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลาการผลิต (เดือน/ปี)</td>
				'.$td_income1_year2_2.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income1_year2_3.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) รายได้ก่อนหักค่าใช้จ่าย (บาท)</td>
				'.$td_income1_year2_4.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year2_income1, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) ค่าใช้จ่ายการเกษตร (บาท)</td>
				'.$td_income1_year2_5.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year2_expenditure1, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(6) รายได้จากการเกษตรสุทธิ (4) - (5)</td>
				'.$td_income1_year2_6.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year2_sum1, 2).'</td>
			</tr>';
		
		$html_year3_income1 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทการเกษตร</td>
				'.$td_income1_year3_1.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลาการผลิต (เดือน/ปี)</td>
				'.$td_income1_year3_2.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income1_year3_3.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) รายได้ก่อนหักค่าใช้จ่าย (บาท)</td>
				'.$td_income1_year3_4.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year3_income1, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) ค่าใช้จ่ายการเกษตร (บาท)</td>
				'.$td_income1_year3_5.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year3_expenditure1, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(6) รายได้จากการเกษตรสุทธิ (4) - (5)</td>
				'.$td_income1_year3_6.'
				'.$td_null_income1.'
				<td align="right">'.number_format($sum_year3_sum1, 2).'</td>
			</tr>';
	}
	
	$html_year1_income2 = '';
	$html_year2_income2 = '';
	$html_year3_income2 = '';
	if(!empty($loan_income_2)) 
	{
		$sum_year1_income2 = 0;
		$sum_year2_income2 = 0;
		$sum_year3_income2 = 0;
		$sum_year1_expenditure2 = 0;
		$sum_year2_expenditure2 = 0;
		$sum_year3_expenditure2 = 0;
		$sum_year1_sum2 = 0;
		$sum_year2_sum2 = 0;
		$sum_year3_sum2 = 0;
		$td_income2_year1_1 = '';
		$td_income2_year2_1 = '';
		$td_income2_year3_1 = '';
		$td_income2_year1_2 = '';
		$td_income2_year2_2 = '';
		$td_income2_year3_2 = '';
		$td_income2_year1_3 = '';
		$td_income2_year2_3 = '';
		$td_income2_year3_3 = '';
		$td_income2_year1_4 = '';
		$td_income2_year2_4 = '';
		$td_income2_year3_4 = '';
		$td_income2_year1_5 = '';
		$td_income2_year2_5 = '';
		$td_income2_year3_5 = '';
		$td_income2_year1_6 = '';
		$td_income2_year2_6 = '';
		$td_income2_year3_6 = '';
			
		$i_income_2 = 0;
			
		foreach ($loan_income_2 as $list_income_2)
		{
			$year1_income = floatval($list_income_2['loan_activity_income1']);
			$year2_income = floatval($list_income_2['loan_activity_income2']);
			$year3_income = floatval($list_income_2['loan_activity_income3']);
				
			$sum_year1_income2 += $year1_income;
			$sum_year2_income2 += $year2_income;
			$sum_year3_income2 += $year3_income;
				
			$year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_2['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_2['loan_activity_expenditure3']);
				
			$sum_year1_expenditure2 += $year1_expenditure;
			$sum_year2_expenditure2 += $year2_expenditure;
			$sum_year3_expenditure2 += $year3_expenditure;
				
			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;
				
			$sum_year1_sum2 += $year1_sum;
			$sum_year2_sum2 += $year2_sum;
			$sum_year3_sum2 += $year3_sum;
	
			$rai2 = intval($list_income_2['loan_activity_self_rai']) + intval($list_income_2['loan_activity_renting_rai']) + intval($list_income_2['loan_activity_approve_rai']);
			$ngan2 = intval($list_income_2['loan_activity_self_ngan']) + intval($list_income_2['loan_activity_renting_ngan']) + intval($list_income_2['loan_activity_approve_ngan']);
			$wah2 = floatval($list_income_2['loan_activity_self_wah']) + floatval($list_income_2['loan_activity_renting_wah']) + floatval($list_income_2['loan_activity_approve_wah']);
	
			$area2 = area($rai2, $ngan2, $wah2);
	
			$td_income2_year1_1 .= '<td align="center">'.$list_income_2['loan_activity_name'].'</td>';
			$td_income2_year2_1 .= '<td align="center">'.$list_income_2['loan_activity_name'].'</td>';
			$td_income2_year3_1 .= '<td align="center">'.$list_income_2['loan_activity_name'].'</td>';
			$td_income2_year1_2 .= '<td align="center">1 ปี</td>';
			$td_income2_year2_2 .= '<td align="center">1 ปี</td>';
			$td_income2_year3_2 .= '<td align="center">1 ปี</td>';
			$td_income2_year1_3 .= '<td align="center">'.$area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah'].' ไร่</td>';
			$td_income2_year2_3 .= '<td align="center">'.$area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah'].' ไร่</td>';
			$td_income2_year3_3 .= '<td align="center">'.$area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah'].' ไร่</td>';
			$td_income2_year1_4 .= '<td align="right">'.number_format($year1_income, 2).'</td>';
			$td_income2_year2_4 .= '<td align="right">'.number_format($year2_income, 2).'</td>';
			$td_income2_year3_4 .= '<td align="right">'.number_format($year3_income, 2).'</td>';
			$td_income2_year1_5 .= '<td align="right">'.number_format($year1_expenditure, 2).'</td>';
			$td_income2_year2_5 .= '<td align="right">'.number_format($year2_expenditure, 2).'</td>';
			$td_income2_year3_5 .= '<td align="right">'.number_format($year3_expenditure, 2).'</td>';
			$td_income2_year1_6 .= '<td align="right">'.number_format($year1_sum, 2).'</td>';
			$td_income2_year2_6 .= '<td align="right">'.number_format($year2_sum, 2).'</td>';
			$td_income2_year3_6 .= '<td align="right">'.number_format($year3_sum, 2).'</td>';
	
			$cal_all[$i_income_2]['year1'] = empty($cal_all[$i_income_2]['year1'])? $year1_sum : $cal_all[$i_income_2]['year1'] + $year1_sum;
			$cal_all[$i_income_2]['year2'] = empty($cal_all[$i_income_2]['year2'])? $year2_sum : $cal_all[$i_income_2]['year2'] + $year2_sum;
			$cal_all[$i_income_2]['year3'] = empty($cal_all[$i_income_2]['year3'])? $year3_sum : $cal_all[$i_income_2]['year3'] + $year3_sum;
	
			$i_income_2++;
		}
		
		$td_null_income2 = '';
		if($count_income_2 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_2;
			$td_null_income2 = str_repeat('<td align="center">xx</td>', $count_null);
				
			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				if(empty($cal_all[$i_income_2]['year1'])) $cal_all[$i_income_2]['year1'] = 0;
				if(empty($cal_all[$i_income_2]['year2'])) $cal_all[$i_income_2]['year2'] = 0;
				if(empty($cal_all[$i_income_2]['year3'])) $cal_all[$i_income_2]['year3'] = 0;
					
				$i_income_2++;
			}
		}
		
		$sum_all['year1'] += $sum_year1_sum2;
		$sum_all['year2'] += $sum_year2_sum2;
		$sum_all['year3'] += $sum_year3_sum2;
		
		$html_year1_income2 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทการผลิต</td>
				'.$td_income2_year1_1.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลาการผลิต หรือบริการ (เดือน/ปี) </td>
				'.$td_income2_year1_2.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<!-- <tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income2_year1_3.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr> -->
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) รายได้นอกการเกษตร (บาท) </td>
				'.$td_income2_year1_4.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year1_income2, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) ค่าใช้จ่ายนอกการเกษตร  (บาท)</td>
				'.$td_income2_year1_5.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year1_expenditure2, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) รายได้จากการเกษตรสุทธิ (3) - (4)</td>
				'.$td_income2_year1_6.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year1_sum2, 2).'</td>
			</tr>';
		
		$html_year2_income2 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทการผลิต</td>
				'.$td_income2_year2_1.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลาการผลิต หรือบริการ (เดือน/ปี) </td>
				'.$td_income2_year2_2.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<!-- <tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income2_year2_3.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>-->
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) รายได้นอกการเกษตร (บาท) </td>
				'.$td_income2_year2_4.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year2_income2, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) ค่าใช้จ่ายนอกการเกษตร  (บาท)</td>
				'.$td_income2_year2_5.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year2_expenditure2, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) รายได้นอกการเกษตรสุทธิ (3) – (4) </td>
				'.$td_income2_year2_6.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year2_sum2, 2).'</td>
			</tr>';
		
		$html_year3_income2 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทการผลิต</td>
				'.$td_income2_year3_1.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลาการผลิต หรือบริการ (เดือน/ปี) </td>
				'.$td_income2_year3_2.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<!-- <tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income2_year3_3.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr> -->
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) รายได้นอกการเกษตร (บาท) </td>
				'.$td_income2_year3_4.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year3_income2, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) ค่าใช้จ่ายนอกการเกษตร  (บาท)</td>
				'.$td_income2_year3_5.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year3_expenditure2, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) รายได้จากการเกษตรสุทธิ (3) - (4)</td>
				'.$td_income2_year3_6.'
				'.$td_null_income2.'
				<td align="right">'.number_format($sum_year3_sum2, 2).'</td>
			</tr>';
	}
	
	$html_year1_income3 = '';
	$html_year2_income3 = '';
	$html_year3_income3 = '';
	if(!empty($loan_income_3)) 
	{
		$sum_year1_income3 = 0;
		$sum_year2_income3 = 0;
		$sum_year3_income3 = 0;
		$sum_year1_expenditure3 = 0;
		$sum_year2_expenditure3 = 0;
		$sum_year3_expenditure3 = 0;
		$sum_year1_sum3 = 0;
		$sum_year2_sum3 = 0;
		$sum_year3_sum3 = 0;
		$td_income3_year1_1 = '';
		$td_income3_year2_1 = '';
		$td_income3_year3_1 = '';
		$td_income3_year1_2 = '';
		$td_income3_year2_2 = '';
		$td_income3_year3_2 = '';
		$td_income3_year1_3 = '';
		$td_income3_year2_3 = '';
		$td_income3_year3_3 = '';
		$td_income3_year1_4 = '';
		$td_income3_year2_4 = '';
		$td_income3_year3_4 = '';
		$td_income3_year1_5 = '';
		$td_income3_year2_5 = '';
		$td_income3_year3_5 = '';
		$td_income3_year1_6 = '';
		$td_income3_year2_6 = '';
		$td_income3_year3_6 = '';
			
		$i_income_3 = 0;
			
		foreach ($loan_income_3 as $list_income_3)
		{
			$year1_income = floatval($list_income_3['loan_activity_income1']);
			$year2_income = floatval($list_income_3['loan_activity_income2']);
			$year3_income = floatval($list_income_3['loan_activity_income3']);
				
			$sum_year1_income3 += $year1_income;
			$sum_year2_income3 += $year2_income;
			$sum_year3_income3 += $year3_income;
				
			$year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_3['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_3['loan_activity_expenditure3']);
	
			$sum_year1_expenditure3 += $year1_expenditure;
			$sum_year2_expenditure3 += $year2_expenditure;
			$sum_year3_expenditure3 += $year3_expenditure;
				
			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;
	
			$sum_year1_sum3 += $year1_sum;
			$sum_year2_sum3 += $year2_sum;
			$sum_year3_sum3 += $year3_sum;
	
			$rai3 = intval($list_income_3['loan_activity_self_rai']) + intval($list_income_3['loan_activity_renting_rai']) + intval($list_income_3['loan_activity_approve_rai']);
			$ngan3 = intval($list_income_3['loan_activity_self_ngan']) + intval($list_income_3['loan_activity_renting_ngan']) + intval($list_income_3['loan_activity_approve_ngan']);
			$wah3 = floatval($list_income_3['loan_activity_self_wah']) + floatval($list_income_3['loan_activity_renting_wah']) + floatval($list_income_3['loan_activity_approve_wah']);
	
			$area3 = area($rai3, $ngan3, $wah3);
	
			$td_income3_year1_1 .= '<td align="center">'.$list_income_3['loan_activity_name'].'</td>';
			$td_income3_year2_1 .= '<td align="center">'.$list_income_3['loan_activity_name'].'</td>';
			$td_income3_year3_1 .= '<td align="center">'.$list_income_3['loan_activity_name'].'</td>';
			$td_income3_year1_2 .= '<td align="center">1 ปี</td>';
			$td_income3_year2_2 .= '<td align="center">1 ปี</td>';
			$td_income3_year3_2 .= '<td align="center">1 ปี</td>';
			$td_income3_year1_3 .= '<td align="center">'.$area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah'].' ไร่</td>';
			$td_income3_year2_3 .= '<td align="center">'.$area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah'].' ไร่</td>';
			$td_income3_year3_3 .= '<td align="center">'.$area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah'].' ไร่</td>';
			$td_income3_year1_4 .= '<td align="right">'.number_format($year1_income, 2).'</td>';
			$td_income3_year2_4 .= '<td align="right">'.number_format($year2_income, 2).'</td>';
			$td_income3_year3_4 .= '<td align="right">'.number_format($year3_income, 2).'</td>';
			$td_income3_year1_5 .= '<td align="right">'.number_format($year1_expenditure, 2).'</td>';
			$td_income3_year2_5 .= '<td align="right">'.number_format($year2_expenditure, 2).'</td>';
			$td_income3_year3_5 .= '<td align="right">'.number_format($year3_expenditure, 2).'</td>';
			$td_income3_year1_6 .= '<td align="right">'.number_format($year1_sum, 2).'</td>';
			$td_income3_year2_6 .= '<td align="right">'.number_format($year2_sum, 2).'</td>';
			$td_income3_year3_6 .= '<td align="right">'.number_format($year3_sum, 2).'</td>';
	
			$cal_all[$i_income_3]['year1'] = empty($cal_all[$i_income_3]['year1'])? $year1_sum : $cal_all[$i_income_3]['year1'] + $year1_sum;
			$cal_all[$i_income_3]['year2'] = empty($cal_all[$i_income_3]['year2'])? $year2_sum : $cal_all[$i_income_3]['year2'] + $year2_sum;
			$cal_all[$i_income_3]['year3'] = empty($cal_all[$i_income_3]['year3'])? $year3_sum : $cal_all[$i_income_3]['year3'] + $year3_sum;
	
			$i_income_3++;
		}
		
		$td_null_income3 = '';
		if($count_income_3 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_3;
			$td_null_income3 = str_repeat('<td align="center">xx</td>', $count_null);
			 
			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				if(empty($cal_all[$i_income_3]['year1'])) $cal_all[$i_income_3]['year1'] = 0;
				if(empty($cal_all[$i_income_3]['year2'])) $cal_all[$i_income_3]['year2'] = 0;
				if(empty($cal_all[$i_income_3]['year3'])) $cal_all[$i_income_3]['year3'] = 0;
			
				$i_income_3++;
			}
		}
		
		$sum_all['year1'] += $sum_year1_sum3;
		$sum_all['year2'] += $sum_year2_sum3;
		$sum_all['year3'] += $sum_year3_sum3;
		
		$html_year1_income3 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทรายได้อื่นๆ</td>
				'.$td_income3_year1_1.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลา (เดือน/ปี)</td>
				'.$td_income3_year1_2.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<!-- <tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income3_year1_3.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr> -->
			<tr>
				<td>&nbsp;&nbsp;&nbsp(3) รายได้อื่นๆ ก่อนหักค่าใช้จ่าย (บาท) </td>
				'.$td_income3_year1_4.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year1_income3, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) ค่าใช้จ่ายอื่นๆ (บาท)</td>
				'.$td_income3_year1_5.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year1_expenditure3, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) รายได้อื่นๆ สุทธิ (3) - (4) </td>
				'.$td_income3_year1_6.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year1_sum3, 2).'</td>
			</tr>';
		
		$html_year2_income3 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทรายได้อื่นๆ</td>
				'.$td_income3_year2_1.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลา (เดือน/ปี)</td>
				'.$td_income3_year2_2.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<!-- <tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income3_year2_3.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr> -->
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) รายได้อื่นๆ ก่อนหักค่าใช้จ่าย (บาท) </td>
				'.$td_income3_year2_4.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year2_income3, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) ค่าใช้จ่ายอื่นๆ (บาท)</td>
				'.$td_income3_year2_5.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year2_expenditure3, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) รายได้อื่นๆ สุทธิ (3) - (4) </td>
				'.$td_income3_year2_6.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year2_sum3, 2).'</td>
			</tr>';
		
		$html_year3_income3 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(1) ประเภทรายได้อื่นๆ</td>
				'.$td_income3_year3_1.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(2) ระยะเวลา (เดือน/ปี)</td>
				'.$td_income3_year3_2.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<!-- <tr>
				<td>&nbsp;&nbsp;&nbsp;(3) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income3_year3_3.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr> -->
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(3) รายได้อื่นๆ ก่อนหักค่าใช้จ่าย (บาท) </td>
				'.$td_income3_year3_4.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year3_income3, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(4) ค่าใช้จ่ายอื่นๆ (บาท)</td>
				'.$td_income3_year3_5.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year3_expenditure3, 2).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(5) รายได้อื่นๆ สุทธิ (3) - (4) </td>
				'.$td_income3_year3_6.'
				'.$td_null_income3.'
				<td align="right">'.number_format($sum_year3_sum3, 2).'</td>
			</tr>';
	}
	
	$sum_year1_expenditure_1 = 0;
	$sum_year2_expenditure_1 = 0;
	$sum_year3_expenditure_1 = 0;
	$td_year1_expenditure_1 = '';
	$td_year2_expenditure_1 = '';
	$td_year3_expenditure_1 = '';
	 
	$sum_expenditure = array();
	$i_expenditure = 0;
	
	if(!empty($loan_expenditure_1))
	{
		foreach ($loan_expenditure_1 as $list_expenditure_1)
		{
			$year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
			$year2_amount = floatval($list_expenditure_1['loan_expenditure_amount2']);
			$year3_amount = floatval($list_expenditure_1['loan_expenditure_amount3']);
			 
			$sum_year1_expenditure_1 += $year1_amount;
			$sum_year2_expenditure_1 += $year2_amount;
			$sum_year3_expenditure_1 += $year3_amount;
			 
			$td_year1_expenditure_1 .= '<td align="right">'.number_format($year1_amount, 2).'<!-- <a style="cursor: pointer; cursor: hand;" onclick="edit_expenditure(\''.$list_expenditure_1['loan_expenditure_id'].'\', \''.$list_expenditure_1['loan_expenditure_name'].'\', \'1\', \''.$year1_amount.'\')"><span class="glyphicon glyphicon-edit"></span></a> --></td>';
			$td_year2_expenditure_1 .= '<td align="right">'.number_format($year2_amount, 2).' <!-- <a style="cursor: pointer; cursor: hand;" onclick="edit_expenditure(\''.$list_expenditure_1['loan_expenditure_id'].'\', \''.$list_expenditure_1['loan_expenditure_name'].'\', \'2\', \''.$year2_amount.'\')"><span class="glyphicon glyphicon-edit"></span></a> --></td>';
			$td_year3_expenditure_1 .= '<td align="right">'.number_format($year3_amount, 2).'<!-- <a style="cursor: pointer; cursor: hand;" onclick="edit_expenditure(\''.$list_expenditure_1['loan_expenditure_id'].'\', \''.$list_expenditure_1['loan_expenditure_name'].'\', \'3\', \''.$year3_amount.'\')"><span class="glyphicon glyphicon-edit"></span></a> --></td>';
	
			$sum_expenditure[$i_expenditure]= array(
					'year1' => $year1_amount,
					'year2' => $year2_amount,
					'year3' => $year3_amount
			);
			 
			$cal_all[$i_expenditure]['year1'] = empty($cal_all[$i_expenditure]['year1'])? 0 - $year1_amount : $cal_all[$i_expenditure]['year1'] - $year1_amount;
			$cal_all[$i_expenditure]['year2'] = empty($cal_all[$i_expenditure]['year2'])? 0 - $year2_amount : $cal_all[$i_expenditure]['year2'] - $year2_amount;
			$cal_all[$i_expenditure]['year3'] = empty($cal_all[$i_expenditure]['year3'])? 0 - $year3_amount : $cal_all[$i_expenditure]['year3'] - $year3_amount;
	
			$i_expenditure++;
		}
	}
	
	$td_null_expenditure1 = '';
	if($count_expenditure_1 < $td_len - 1)
	{
		$count_null = $td_len - 1 - $count_expenditure_1;
		$td_null_expenditure1 = str_repeat('<td align="center">xx</td>', $count_null);
	
		for ($i_null = 1; $i_null<=$count_null; $i_null++)
		{
			$sum_expenditure[$i_expenditure]= array(
					'year1' => 0,
					'year2' => 0,
					'year3' => 0
			);
					
			if(empty($cal_all[$i_expenditure]['year1'])) $cal_all[$i_expenditure]['year1'] = 0;
			if(empty($cal_all[$i_expenditure]['year2'])) $cal_all[$i_expenditure]['year2'] = 0;
			if(empty($cal_all[$i_expenditure]['year3'])) $cal_all[$i_expenditure]['year3'] = 0;
					
			$i_expenditure++;
		}
	}
	
	$sum_year1_expenditure_2 = 0;
	$sum_year2_expenditure_2 = 0;
	$sum_year3_expenditure_2 = 0;
	$td_year1_expenditure_2 = '';
	$td_year2_expenditure_2 = '';
	$td_year3_expenditure_2 = '';
	
	$i_expenditure = 0;
	
	if(!empty($loan_expenditure_2))
	{
		foreach ($loan_expenditure_2 as $list_expenditure_2)
		{
			$year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
			$year2_amount = floatval($list_expenditure_2['loan_expenditure_amount2']);
			$year3_amount = floatval($list_expenditure_2['loan_expenditure_amount3']);
	
			$sum_year1_expenditure_2 += $year1_amount;
			$sum_year2_expenditure_2 += $year2_amount;
			$sum_year3_expenditure_2 += $year3_amount;
	
			$td_year1_expenditure_2 .= '<td align="right">'.number_format($year1_amount, 2).' <a style="cursor: pointer; cursor: hand;" onclick="edit_expenditure(\''.$list_expenditure_2['loan_expenditure_id'].'\', \''.$list_expenditure_2['loan_expenditure_name'].'\', \'1\', \''.$year1_amount.'\')"><span class="glyphicon glyphicon-edit"></span></a></td>';
			$td_year2_expenditure_2 .= '<td align="right">'.number_format($year2_amount, 2).' <a style="cursor: pointer; cursor: hand;" onclick="edit_expenditure(\''.$list_expenditure_2['loan_expenditure_id'].'\', \''.$list_expenditure_2['loan_expenditure_name'].'\', \'2\', \''.$year2_amount.'\')"><span class="glyphicon glyphicon-edit"></span></a></td>';
			$td_year3_expenditure_2 .= '<td align="right">'.number_format($year3_amount, 2).' <a style="cursor: pointer; cursor: hand;" onclick="edit_expenditure(\''.$list_expenditure_2['loan_expenditure_id'].'\', \''.$list_expenditure_2['loan_expenditure_name'].'\', \'3\', \''.$year3_amount.'\')"><span class="glyphicon glyphicon-edit"></span></a></td>';
			 
			$sum_expenditure[$i_expenditure]['year1'] = empty($sum_expenditure[$i_expenditure]['year1'])? $year1_amount : $sum_expenditure[$i_expenditure]['year1'] + $year1_amount;
			$sum_expenditure[$i_expenditure]['year2'] = empty($sum_expenditure[$i_expenditure]['year2'])? $year2_amount : $sum_expenditure[$i_expenditure]['year2'] + $year2_amount;
			$sum_expenditure[$i_expenditure]['year3'] = empty($sum_expenditure[$i_expenditure]['year3'])? $year3_amount : $sum_expenditure[$i_expenditure]['year3'] + $year3_amount;
	
			$cal_all[$i_expenditure]['year1'] = empty($cal_all[$i_expenditure]['year1'])? 0 - $year1_amount : $cal_all[$i_expenditure]['year1'] - $year1_amount;
			$cal_all[$i_expenditure]['year2'] = empty($cal_all[$i_expenditure]['year2'])? 0 - $year2_amount : $cal_all[$i_expenditure]['year2'] - $year2_amount;
			$cal_all[$i_expenditure]['year3'] = empty($cal_all[$i_expenditure]['year3'])? 0 - $year3_amount : $cal_all[$i_expenditure]['year3'] - $year3_amount;
	
			$i_expenditure++;
		}
	}
	
	$td_null_expenditure2 = '';
	if($count_expenditure_2 < $td_len - 1)
	{
		$count_null = $td_len - 1 - $count_expenditure_2;
		$td_null_expenditure2 = str_repeat('<td align="center">xx</td>', $count_null);
	
		for ($i_null = 1; $i_null<=$count_null; $i_null++)
		{
			$sum_expenditure[$i_expenditure]['year1'] = empty($sum_expenditure[$i_expenditure]['year1'])? 0 : $sum_expenditure[$i_expenditure]['year1'] + 0;
			$sum_expenditure[$i_expenditure]['year2'] = empty($sum_expenditure[$i_expenditure]['year2'])? 0 : $sum_expenditure[$i_expenditure]['year2'] + 0;
			$sum_expenditure[$i_expenditure]['year3'] = empty($sum_expenditure[$i_expenditure]['year3'])? 0 : $sum_expenditure[$i_expenditure]['year3'] + 0;
		
			if(empty($cal_all[$i_expenditure]['year1'])) $cal_all[$i_expenditure]['year1'] = 0;
			if(empty($cal_all[$i_expenditure]['year2'])) $cal_all[$i_expenditure]['year2'] = 0;
			if(empty($cal_all[$i_expenditure]['year3'])) $cal_all[$i_expenditure]['year3'] = 0;
				
			$i_expenditure++;
		}
	}
	
	$sum_year1_expenditure_all = 0;
	$sum_year2_expenditure_all = 0;
	$sum_year3_expenditure_all = 0;
	$td_year1_expenditure_all = '';
	$td_year2_expenditure_all = '';
	$td_year3_expenditure_all = '';
	
	foreach ($sum_expenditure as $list_expenditure_all)
	{
		$year1_amount = $list_expenditure_all['year1'];
		$year2_amount = $list_expenditure_all['year2'];
		$year3_amount = $list_expenditure_all['year3'];
	
		$sum_year1_expenditure_all += $year1_amount;
		$sum_year2_expenditure_all += $year2_amount;
		$sum_year3_expenditure_all += $year3_amount;
	
		$td_year1_expenditure_all .= '<td align="right">'.number_format($year1_amount, 2).'</td>';
		$td_year2_expenditure_all .= '<td align="right">'.number_format($year2_amount, 2).'</td>';
		$td_year3_expenditure_all .= '<td align="right">'.number_format($year3_amount, 2).'</td>';
	}
	
	$sum_all['year1'] -= $sum_year1_expenditure_all;
	$sum_all['year2'] -= $sum_year2_expenditure_all;
	$sum_all['year3'] -= $sum_year3_expenditure_all;
	
	$td_year1 = '';
	$td_year2 = '';
	$td_year3 = '';
	$td_year1_all = '';
	$td_year2_all = '';
	$td_year3_all = '';
	
	foreach ($cal_all as $list_cal_all)
	{
		$td_year1 .= '<td align="right">'.number_format($list_cal_all['year1'], 2).'</td>';
		$td_year2 .= '<td align="right">'.number_format($list_cal_all['year2'], 2).'</td>';
		$td_year3 .= '<td align="right">'.number_format($list_cal_all['year3'], 2).'</td>';
	}
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel-group" id="accordion_year">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion_year" href="#year1">ปีที่ 1 (ปีปัจจุบัน)</a>
					</h4>
				</div>
				<div id="year1" class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered mb30">
								<thead>
									<tr>
						            	<th style="text-align:center; vertical-align: middle;">รายการ</th>
						            	<th colspan="<?php echo $td_len; ?>" style="text-align:center;">ปีที่ 1 (ปีปัจจุบัน)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
						            	<th>1. รายได้และค่าใช้จ่ายการเกษตร</th>
						                <?php echo $td_income_1; ?>
									</tr>
									<?php echo $html_year1_income1; ?>
									<tr>
						            	<th>2. รายได้และค่าใช้จ่ายนอกการเกษตร</th>
						                <?php echo $td_income_2; ?>
									</tr>
									<?php echo $html_year1_income2; ?>
									<tr>
						            	<th>3. รายได้อื่นๆ</th>
						                <?php echo $td_income_3; ?>
									</tr>
									<?php echo $html_year1_income3; ?>
									<tr>
						            	<th>4. ค่าใช้จ่ายอื่นๆ (บาท)</th>
						                <?php echo $td_expenditure; ?>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(1) ค่าใช้จ่ายในครัวเรือน</td>
						               	<?php echo $td_year1_expenditure_1; ?>
						               	<?php echo $td_null_expenditure1; ?>
						               	<td align="right"><?php echo number_format($sum_year1_expenditure_1, 2); ?></td>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(2) ค่าใช้จ่ายอื่นๆ</td>
						               	<?php echo $td_year1_expenditure_2; ?>
						               	<?php echo $td_null_expenditure2; ?>
						               	<td align="right"><?php echo number_format($sum_year1_expenditure_2, 2); ?></td>
									</tr>
									<tr>
										<td>&nbsp;&nbsp;&nbsp;(3) รวมค่าใช้จ่าย (1) + (2)</td>
						               	<?php echo $td_year1_expenditure_all; ?>
						               	<td align="right"><?php echo number_format($sum_year1_expenditure_all, 2); ?></td>
									</tr>
									<tr>
										<td>5. รายได้สุทธิ (บาท)</td>
										<?php echo $td_year1; ?>
						               	<td align="right">
						               		<?php echo number_format($sum_all['year1'], 2); ?>
						               		<input type="hidden" id="txtYear1Sum" name="txtYear1Sum" value="<?php echo $sum_all['year1']; ?>" />
						               	</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- panel -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" class="collapsed" data-parent="#accordion_year" href="#year2">ปีที่ 2 (ปีก่อน)</a>
					</h4>
				</div>
				<div id="year2" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered mb30">
								<thead>
									<tr>
						            	<th style="text-align:center; vertical-align: middle;">รายการ</th>
						            	<th colspan="<?php echo $td_len; ?>" style="text-align:center;">ปีที่ 2 (ปีก่อน)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
						            	<th>1. รายได้และค่าใช้จ่ายการเกษตร</th>
						                <?php echo $td_income_1; ?>
									</tr>
									<?php echo $html_year2_income1; ?>
									<tr>
						            	<th>2. รายได้และค่าใช้จ่ายนอกการเกษตร</th>
						                <?php echo $td_income_2; ?>
									</tr>
									<?php echo $html_year2_income2; ?>
									<tr>
						            	<th>3. รายได้อื่นๆ</th>
						                <?php echo $td_income_3; ?>
									</tr>
									<?php echo $html_year2_income3; ?>
									<tr>
						            	<th>4. ค่าใช้จ่ายอื่นๆ (บาท)</th>
						                <?php echo $td_expenditure; ?>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(1) ค่าใช้จ่ายในครัวเรือน</td>
						               	<?php echo $td_year2_expenditure_1; ?>
						               	<?php echo $td_null_expenditure1; ?>
						               	<td align="right"><?php echo number_format($sum_year2_expenditure_1, 2); ?></td>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(2) ค่าใช้จ่ายอื่นๆ</td>
						               	<?php echo $td_year2_expenditure_2; ?>
						               	<?php echo $td_null_expenditure2; ?>
						               	<td align="right"><?php echo number_format($sum_year2_expenditure_2, 2); ?></td>
									</tr>
									<tr>
										<td>&nbsp;&nbsp;&nbsp;(3) รวมค่าใช้จ่าย (1) + (2)</td>
						               	<?php echo $td_year2_expenditure_all; ?>
						               	<td align="right"><?php echo number_format($sum_year2_expenditure_all, 2); ?></td>
									</tr>
									<tr>
										<td>5. รายได้สุทธิ (บาท)</td>
										<?php echo $td_year2; ?>
						               	<td align="right">
						               		<?php echo number_format($sum_all['year2'], 2); ?>
						               		<input type="hidden" id="txtYear2Sum" name="txtYear2Sum" value="<?php echo $sum_all['year2']; ?>" />
						               	</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- panel -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" class="collapsed" data-parent="#accordion_year" href="#year3">ปีที่ 3 (ปีหลัง)</a>
					</h4>
				</div>
				<div id="year3" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered mb30">
								<thead>
									<tr>
						            	<th style="text-align:center; vertical-align: middle;">รายการ</th>
						                <th colspan="<?php echo $td_len; ?>" style="text-align:center;">ปีที่ 3 (ปีหลัง)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
						            	<th>1. รายได้และค่าใช้จ่ายการเกษตร</th>
						                <?php echo $td_income_1; ?>
									</tr>
									<?php echo $html_year3_income1; ?>
									<tr>
						            	<th>2. รายได้และค่าใช้จ่ายนอกการเกษตร</th>
						                <?php echo $td_income_2; ?>
									</tr>
									<?php echo $html_year3_income2; ?>
									<tr>
						            	<th>3. รายได้อื่นๆ</th>
						                <?php echo $td_income_3; ?>
									</tr>
									<?php echo $html_year3_income3; ?>
									<tr>
						            	<th>4. ค่าใช้จ่ายอื่นๆ (บาท)</th>
						                <?php echo $td_expenditure; ?>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(1) ค่าใช้จ่ายในครัวเรือน</td>
						               	<?php echo $td_year3_expenditure_1; ?>
						               	<?php echo $td_null_expenditure1; ?>
						               	<td align="right"><?php echo number_format($sum_year3_expenditure_1, 2); ?></td>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(2) ค่าใช้จ่ายอื่นๆ</td>
						               	<?php echo $td_year3_expenditure_2; ?>
						               	<?php echo $td_null_expenditure2; ?>
						               	<td align="right"><?php echo number_format($sum_year3_expenditure_2, 2); ?></td>
									</tr>
									<tr>
						            	<td>&nbsp;&nbsp;&nbsp;(3) รวมค่าใช้จ่าย (1) + (2)</td>
						               	<?php echo $td_year3_expenditure_all; ?>
						               	<td align="right"><?php echo number_format($sum_year3_expenditure_all, 2); ?></td>
									</tr>
									<tr>
										<td>5. รายได้สุทธิ (บาท)</td>
										<?php echo $td_year3; ?>
						               	<td align="right">
						               		<?php echo number_format($sum_all['year3'], 2); ?>
						               		<input type="hidden" id="txtYear3Sum" name="txtYear3Sum" value="<?php echo $sum_all['year3']; ?>" />
						               	</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!-- panel -->
		</div><!-- panel-group -->
	</div><!-- col-md-12 -->
</div><!-- row -->