<form id="frmAnalysis" name="frmAnalysis" action="<?php echo base_url().'analysis/save'; ?>" method="post">
	<input type="hidden" id="txtId" name="txtId" value="<?php echo $data['loan_analysis_id']; ?>" />
	<input type="hidden" id="txtLoan" name="txtLoan" value="<?php echo $data['loan_id']; ?>" />
	<input type="hidden" id="txtLoanSummary" name="txtLoanSummary" value="<?php echo $loan_summary['loan_summary_id']; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">พิจารณาการขอสินเชื่อ</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a data-toggle="tab" href="#tab_1">บุคคล</a></li>
								<li><a data-toggle="tab" href="#tab_2">สินเชื่อ</a></li>
								<li><a data-toggle="tab" href="#tab_3">หลักประกัน</a></li>
								<li><a data-toggle="tab" href="#tab_4">ค่าใช้จ่าย</a></li>
								<li><a data-toggle="tab" href="#tab_5">ประมาณการ</a></li>
								<li><a data-toggle="tab" href="#tab_6">สรุปความเห็น</a></li>
								<li><a data-toggle="tab" href="#tab_7">ผลการพิจารณา</a></li>
							</ul>
							<div class="tab-content">
								<div id="tab_1" class="tab-pane active">
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>ผู้ขอสินเชื่อ</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">เลขที่บัตรประชาชน</label>
												<input type="text" name="txtThaiid" id="txtThaiid" class="form-control" value="<?php echo $loan['person']['person_thaiid']; ?>" maxlength="13" readonly="readonly"/>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">ชื่อ - นามสกุล</label>
												<input type="text" name="txtName" id="txtName" class="form-control" value="<?php echo $loan['person']['title']['title_name'].$loan['person']['person_fname'].' '.$loan['person']['person_lname']; ?>" maxlength="100" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-6 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">อายุ</label>
												<div class="input-group">
													<input type="text" name="txtAge" id="txtAge" class="form-control right" value="<?php echo $loan['person']['person_age']; ?>" readonly="readonly"/>
													<span class="input-group-addon">ปี</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div>
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">บ้านเลขที่</label>
												<input type="text" name="txtAddrCardNo" id="txtAddrCardNo" class="form-control" value="<?php echo $loan['person']['person_addr_pre_no']; ?>" maxlength="10" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">หมู่ที่</label>
												<input type="text" name="txtAddrCardMoo" id="txtAddrCardMoo" class="form-control" value="<?php echo empty($loan['person']['person_addr_pre_moo'])? '-' : $loan['person']['person_addr_pre_moo']; ?>" maxlength="2" readonly="readonly"/>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">ถนน</label>
												<input type="text" name="txtAddrCardRoad" id="txtAddrCardRoad" class="form-control" value="<?php echo empty($loan['person']['person_addr_pre_road'])? '-' : $loan['person']['person_addr_pre_road']; ?>" maxlength="100" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">ตำบล</label>
												<input type="text" name="txtAddrCardDistrict" id="txtAddrCardDistrict" class="form-control" value="<?php echo $loan['person']['district']['district_name']; ?>" maxlength="100" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div>
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">อำเภอ</label>
												<input type="text" name="txtAddrCardAmphur" id="txtAddrCardAmphur" class="form-control" value="<?php echo $loan['person']['amphur']['amphur_name']; ?>" maxlength="100" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">จังหวัด</label>
												<input type="text" name="txtAddrCardProvince" id="txtAddrCardProvince" class="form-control" value="<?php echo $loan['person']['province']['province_name']; ?>" maxlength="100" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">จำนวนสมาชิกในครัวเรือน</label>
												<div class="input-group">
													<input type="text" name="txtMemberAmount" id="txtMemberAmount" class="form-control right" value="<?php echo $loan['loan_member_amount']; ?>" readonly="readonly"/>
													<span class="input-group-addon">คน</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">จำนวนแรงงานในครัวเรือน</label>
												<div class="input-group">
													<input type="text" name="txtLaborAmount" id="txtLaborAmount" class="form-control right" value="<?php echo $loan['loan_labor_amount']; ?>" readonly="readonly"/>
													<span class="input-group-addon">คน</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">อาชีพ</label>
												<input type="text" name="txtCareer" id="txtCareer" class="form-control" value="<?php echo $loan['person']['career']['career_name']; ?>" maxlength="255" readonly="readonly"/>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">สถานะการแต่งงาน</label>
												<input type="text" name="txtMarried" id="txtMarried" class="form-control" value="<?php echo $loan['person']['married']['status_married_name']; ?>" maxlength="25" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div><!-- row -->
									<hr/>
									<div class="row" style="padding-bottom:10px;<?php echo $loan['spouse_name']==''? ' display:none;' : ''; ?>">
										<div class="col-sm-12">
											<h4>คู่สมรส</h4>
										</div>
									</div>
									<div class="row"<?php echo $loan['spouse_name']==''? ' style="display:none;"' : ''; ?>>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">ชื่อ - นามสกุล</label>
												<input type="text" name="txtSpouseName" id="txtSpouseName" class="form-control" value="<?php echo $loan['spouse_name']; ?>" maxlength="100" readonly="readonly" />
											</div><!-- form-group -->
										</div><!-- col-sm-6 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">อายุ</label>
												<div class="input-group">
													<input type="text" name="txtSpouseAge" id="txtSpouseAge" class="form-control right" value="<?php echo $loan['spouse_age']; ?>" readonly="readonly"/>
													<span class="input-group-addon">ปี</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div><!-- row -->
									<?php if(!empty($loan_co)) { ?>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>ผู้กู้ร่วม</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table class="table table-striped mb30">
													<thead>
														<tr>
											            	<th>#</th>
											            	<th>ชื่อ - นามสกุล</th>
											                <th>อายุ</th>
											                <th>ความเกี่ยวข้อง</th>
											                <th>อาชีพ</th>
														</tr>
													</thead>
													<tbody>
													<?php
														$i_co = 1;
														foreach ($loan_co as $list_co)
														{
															$age_co = age($list_co['person_birthdate']);
													?>
													<tr>
											            	<td align="center"><?php echo $i_co++; ?></td>
											            	<td><?php echo $list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname']; ?></td>
											                <td align="right"><?php echo $age_co; ?></td>
											                <td align="center"><?php echo $list_co['relationship_name']; ?></td>
											                <td><?php echo $list_co['career_name']; ?></td>
														</tr>
													<?php } ?>
													</tbody>
												</table>
											</div><!-- table-responsive -->
										</div>
									</div>
									<?php } ?>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>สมาชิกองค์กรต่างๆ</h4>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-sm-3">
												<div class="ckbox ckbox-default">
													<input type="checkbox" id="chkEgov" name="chkEgov"  value="1" <?php echo $data['loan_analysis_egov']=='1'? 'checked' : ''; ?>>
													<label for="chkEgov">เป็นลูกค้า ธ.ก.ส.</label>
												</div>
											</div><!-- col-sm-3 -->
											<div id="divEgov" class="col-sm-9" <?php echo $data['loan_analysis_egov']=='1'? '' : 'style="display:none;"'; ?>>
												<input type="text" id="txtEgov" name="txtEgov" placeholder="ระบุ" value="<?php echo $data['loan_analysis_egov_detail']; ?>" class="form-control" maxlength="255" required>
											</div><!-- col-sm-9 -->
										</div>
									</div><!-- row -->
									<div class="row">
										<div class="form-group">
											<div class="col-sm-3">
												<div class="ckbox ckbox-default">
													<input type="checkbox" id="chkCooperative" name="chkCooperative"  value="1" <?php echo $data['loan_analysis_cooperative']=='1'? 'checked' : ''; ?>>
													<label for="chkCooperative">เป็นสมาชิกสหกรณ์</label>
												</div>
											</div><!-- col-sm-3 -->
											<div id="divCooperative" class="col-sm-9" <?php echo $data['loan_analysis_cooperative']=='1'? '' : 'style="display:none;"'; ?>>
												<input type="text" id="txtCooperative" name="txtCooperative" placeholder="ระบุ" value="<?php echo $data['loan_analysis_cooperative_detail']; ?>" class="form-control" maxlength="255" required>
											</div><!-- col-sm-9 -->
										</div>
									</div><!-- row -->
									<div class="row">
										<div class="form-group">
											<div class="col-sm-3">
												<div class="ckbox ckbox-default">
													<input type="checkbox" id="chkBank" name="chkBank"  value="1" <?php echo $data['loan_analysis_bank']=='1'? 'checked' : ''; ?>>
													<label for="chkBank">เป็นลูกค้าสถาบันการเงิน</label>
												</div>
											</div><!-- col-sm-3 -->
											<div id="divBank" class="col-sm-9" <?php echo $data['loan_analysis_bank']=='1'? '' : 'style="display:none;"'; ?>>
												<input type="text" id="txtBank" name="txtBank" placeholder="ระบุ" value="<?php echo $data['loan_analysis_bank_detail']; ?>" class="form-control" maxlength="255" required>
											</div><!-- col-sm-9 -->
										</div>
									</div><!-- row -->
									<div class="row">
										<div class="form-group">
											<div class="col-sm-3">
												<div class="ckbox ckbox-default">
													<input type="checkbox" id="chkBankOther" name="chkBankOther"  value="1" <?php echo $data['loan_analysis_bankother']=='1'? 'checked' : ''; ?>>
													<label for="chkBankOther">อื่นๆ</label>
												</div>
											</div><!-- col-sm-3 -->
											<div id="divBankOther" class="col-sm-9" <?php echo $data['loan_analysis_bankother']=='1'? '' : 'style="display:none;"'; ?>>
												<input type="text" id="txtBankOther" name="txtBankOther" placeholder="ระบุ" value="<?php echo $data['loan_analysis_bankother_detail']; ?>" class="form-control" maxlength="255" required>
											</div><!-- col-sm-9 -->
										</div>
									</div><!-- row -->
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>ทรัพย์สินและหนี้สิน</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">ทรัพย์สินมูลค่ารวม</label>
												<div class="input-group">
													<input type="text" id="txtAssetSum" name="txtAssetSum" class="form-control right" value="<?php echo number_format($asset_sum, 2); ?>" readonly="readonly"/>
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">หนี้สินรวม</label>
												<div class="input-group">
													<input type="text" id="txtDeptSum" name="txtDeptSum" class="form-control right" value="<?php echo number_format($debt_sum, 2); ?>" readonly="readonly"/>
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">พื้นที่ทำการเกษตร</label>
												<div class="input-group">
													<input type="text" class="form-control" value="<?php echo $income_all['rai']; ?> - <?php echo $income_all['ngan']; ?> - <?php echo  $income_all['wah']; ?>" readonly="readonly" />
													<span class="input-group-addon">ไร่</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">กรรมสิทธิ์ของตนเอง</label>
												<div class="input-group">
													<input type="text" class="form-control" value="<?php echo $income_self['rai']; ?> - <?php echo $income_self['ngan']; ?> - <?php echo $income_self['wah']; ?>" readonly="readonly" />
													<span class="input-group-addon">ไร่</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">เช่าที่ดินผู้อื่น</label>
												<div class="input-group">
													<input type="text" class="form-control" value="<?php echo $income_renting['rai']; ?> - <?php echo $income_renting['ngan']; ?> - <?php echo $income_renting['wah']; ?>" readonly="readonly" />
													<span class="input-group-addon">ไร่</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
										<div class="col-sm-3">
											<div class="form-group">
												<label class="control-label">ได้รับอนุญาตให้ทำประโยชน์ในที่ดินผู้อื่น</label>
												<div class="input-group">
													<input type="text" class="form-control" value="<?php echo $income_approve['rai']; ?> - <?php echo $income_approve['ngan']; ?> - <?php echo $income_approve['wah']; ?>" readonly="readonly"/>
													<span class="input-group-addon">ไร่</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-3 -->
									</div><!-- row -->
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>พฤติกรรมของผู้ขอสินเชื่อ</h4>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-5 control-label">1. ความประพฤติ</label>
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorY" name="rdBehavior" value="1" <?php echo $data['loan_analysis_behavior']=='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorY" style="padding-left:20px;">ดี</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorN" name=rdBehavior value="0" <?php echo $data['loan_analysis_behavior']!='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorN" style="padding-left:20px;">ไม่ดี</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-3">
												<input type="text" id="txtBehavior" name="txtBehavior" placeholder="ระบุ" value="<?php echo $data['loan_analysis_behavior_detail']; ?>" class="form-control" maxlength="255" >
											</div><!-- col-sm-3 -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-5 control-label">2. ความตั้งใจในการประกอบอาชีพ</label>
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorOccupationY" name="rdBehaviorOccupation" value="1" <?php echo $data['loan_analysis_behavior_occupation']=='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorOccupationY" style="padding-left:20px;">ดี</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorOccupationN" name="rdBehaviorOccupation" value="0" <?php echo $data['loan_analysis_behavior_occupation']!='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorOccupationN" style="padding-left:20px;">ไม่ดี</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-3">
												<input type="text" id="txtBehaviorOccupation" name="txtBehaviorOccupation" placeholder="ระบุ" value="<?php echo $data['loan_analysis_behavior_occupation_detail']; ?>" class="form-control" maxlength="255">
											</div><!-- col-sm-3 -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-5 control-label">3. ความมุ่งมั่นตั้งใจในการใช้ประโยชน์ที่ดินเพื่อเกษตรกรรม</label>
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorFarmY" name="rdBehaviorFarm" value="1" <?php echo $data['loan_analysis_behavior_farm']=='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorFarmY" style="padding-left:20px;">ดี</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorFarmN" name="rdBehaviorFarm" value="0" <?php echo $data['loan_analysis_behavior_farm']!='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorFarmN" style="padding-left:20px;">ไม่ดี</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-3">
												<input type="text" id="txtBehaviorFarm" name="txtBehaviorFarm" placeholder="ระบุ" value="<?php echo $data['loan_analysis_behavior_farm_detail']; ?>" class="form-control" maxlength="255">
											</div><!-- col-sm-3 -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-5 control-label">4. สุขภาพ</label>
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorHealthY" name="rdBehaviorHealth" value="1" <?php echo $data['loan_analysis_behavior_health']=='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorHealthY" style="padding-left:20px;">แข็งแรง</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" id="rdBehaviorHealthN" name="rdBehaviorHealth" value="0" <?php echo $data['loan_analysis_behavior_health']!='1'? 'checked' : ''; ?>>
													<label for="rdBehaviorHealthN" style="padding-left:20px;">ไม่แข็งแรง</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-3">
												<input type="text" id="txtBehaviorHealth" name="txtBehaviorHealth" placeholder="ระบุ" value="<?php echo $data['loan_analysis_behavior_health_detail']; ?>" class="form-control" maxlength="255">
											</div><!-- col-sm-3 -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<label class="col-sm-5 control-label">5. อื่นๆ</label>
											<div class="col-sm-7">
												<input type="text" id="txtBehaviorOther" name="txtBehaviorOther" placeholder="ระบุ" value="<?php echo $data['loan_analysis_behavior_other']; ?>" class="form-control" maxlength="255" >
											</div><!-- col-sm-7 -->
										</div>
									</div>
								</div><!-- tab-pane -->
								<div id="tab_2" class="widget-bloglist tab-pane">
									<div class="row" style="margin-top:10px;">
										<div class="form-group col-sm-12">
											<label class="col-sm-2 control-label">เขียนที่</label>
											<div class="col-sm-10">
												<input type="text" name="txtLocation" id="txtLocation" class="form-control" value="<?php echo $loan['loan_location']; ?>" maxlength="255" readonly="readonly"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-6">
											<label class="col-sm-4 control-label">วันที่</label>
											<div class="col-sm-8">
												<div class="input-group">
													<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo $loan['loan_date']; ?>" readonly="readonly"/>
													<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</div>
										</div>
										<div class="form-group col-sm-6">
											<label class="col-sm-4 control-label">จำนวนเงิน</label>
											<div class="col-sm-8">
												<div class="input-group">
													<input type="text" name="txtAmount" id="txtAmount" class="form-control right" value="<?php echo number_format($loan['loan_amount'], 2); ?>" readonly="readonly"/>
													<span class="input-group-addon">บาท</span>
												</div>
												<label class="error" for="txtAmount"></label>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-12">
											<div class="row">
											</div>
											<label class="col-sm-2 control-label">วัตถุประสงค์เพื่อ</label>
											<div class="col-sm-4">
												<div class="rdio rdio-default">
													<input type="radio" name="rdTypeObjective1" id="rdTypeObjective" value="1" readonly="readonly"<?php echo $loan_type_objective=='1'? 'checked="checked"' : ''; ?>>
													<label for="rdTypeObjective1" style="padding-left:20px;">ไถ่ถอนที่ดินจากการจำนองหรือขายฝาก</label>
												</div>
											</div><!-- col-sm-4 -->
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" name="rdTypeObjective2" id="rdTypeObjective" value="2" readonly="readonly"<?php echo $loan_type_objective=='2'? 'checked="checked"' : ''; ?>>
													<label for="rdTypeObjective2" style="padding-left:20px;">ชำระหนี้</label>
												</div>
											</div><!-- col-sm-2 -->
											<div class="col-sm-4">
												<div class="rdio rdio-default">
													<input type="radio" name="rdTypeObjective3" id="rdTypeObjective" value="3" disabled="disabled"<?php echo $loan_type_objective=='3'? 'checked="checked"' : ''; ?>>
													<label for="rdTypeObjective3" style="padding-left:20px;">ซื้อคืนที่ดิน</label>
												</div>
											</div><!-- col-sm-4 -->
										</div>
									</div>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>มูลเหตุแห่งหนี้</h4>
										</div>
									</div>
									<input type="hidden" name="txtTypeId" id="txtTypeId" value="<?php echo $loan_type_id; ?>" />
									<input type="hidden" name="txtObjectiveId" id="txtObjectiveId" value="<?php echo $loan_objective_id; ?>" />
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">ชื่อ – สกุลของเจ้าหนี้ หรือเจ้าหนี้ตามคำพิพากษา</label>
												<?php if($loan_objective_id=='1' || $loan_objective_id=='2' || $loan_objective_id=='3') { ?>
												<input type="hidden" name="txtCreditorId" id="txtCreditorId" value="<?php echo $loan_creditor_id; ?>" />
												<input type="hidden" name="txtCreditorTitle" id="txtCreditorTitle" value="<?php echo $loan_creditor_title; ?>" />
												<input type="text" name="txtCreditorFname" id="txtCreditorFname" class="form-control" value="<?php echo $loan_creditor_fname; ?>" style="width:45%; float:left;" maxlength="50" />
												<input type="text" name="txtCreditorLname" id="txtCreditorLname" class="form-control"  value="<?php echo $loan_creditor_lname; ?>" style="width:50%; float:right;" maxlength="50" />
												<?php } else { ?>
												<input type="text" name="txtCreditor" id="txtCreditor" class="form-control" value="<?php echo $loan_creditor; ?>" />
												<?php } ?>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">จำนวนเงินที่กู้ยืมหรือขายฝาก และได้รับจริง</label>
												<div class="input-group">
													<input type="text" name="txtRealAmount" id="txtRealAmount" class="form-control right" value="<?php echo number_format($loan_real_amount, 2); ?>"/>
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">ตามเอกสารการเป็นหนี้</label>
												<input type="text" class="form-control" value="xxx" />
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">จำนวนหนี้ตามเอกสาร</label>
												<input type="text" class="form-control" value="xxx"  />
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">กำหนดเวลาการชำระหนี้</label>
												<div class="input-group">
													<input type="text" id="txtDeadline" name="txtDeadline" class="form-control" value="<?php echo $data['loan_analysis_deadline']; ?>" readonly="readonly" />
													<span id="iconDeadline" class="input-group-addon hand" ><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">มีจำนวนหนี้คงเหลือต้องชำระ</label>
												<div class="input-group">
													<input type="text" name="txtRemain" id="txtRemain" class="form-control right" value="<?php echo number_format($data['loan_analysis_remain'], 2); ?>" />
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">ภายในวันที่</label>
												<div class="input-group">
													<input type="text" id="txtWithin" name="txtWithin" class="form-control" value="<?php echo $data['loan_analysis_within']; ?>" readonly="readonly" />
													<span id="iconWithin" class="input-group-addon hand" ><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-8">
											<div class="form-group">
												<label class="control-label">สาเหตุที่เป็นหนี้</label>
												<textarea name="txtCause" id="txtCause" class="form-control" rows="5"><?php echo $data['loan_analysis_cause']; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-8 -->
									</div><!-- row -->
								</div><!-- tab-pane -->
								<div id="tab_3" class="widget-bloglist tab-pane">
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>แปลงที่ดิน</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table class="table table-striped mb30">
													<thead>
														<tr>
											            	<th>#</th>
											            	<th>เนื้อที่ (ไร่)</th>
											                <th>ราคาจดทะเบียนสิทธิ<br/>และนิติกรรม ต่อไร่ </th>
											                <th>ราคาซื้อขายเฉลี่ย<br/>ในปัจจุบันต่อไร่</th>
											                <th>ราคาประเมินต่อไร่</th>
											                <th>ราคาประเมินทั้งแปลง</th>
											                <th>ราคาประเมินเป็นกี่เท่า<br/>ของจำนวนเงินกู้</th>
														</tr>
													</thead>
													<tbody>
													<?php
														$sum_assess = 0;
														if(!empty($land_assess))
														{
															foreach ($land_assess as $key_assess => $list_assess)
															{
																//$cal_assess = CalulatePricePerRai($list_assess['land_assess_area_rai'], $list_assess['land_assess_area_ngan'], $list_assess['land_assess_area_wah'], $list_assess['land_assess_price']);
																$cal_assess = floatval($list_assess['land_assess_inspector_sum']);
																$sum_assess += floatval($cal_assess);
													?>
															<tr>
																<td align="center"><?php echo $list_assess['key']; ?></td>
																<td align="center"><?php echo $list_assess['land_assess_area_rai'].' - '.$list_assess['land_assess_area_ngan'].' - '.$list_assess['land_assess_area_wah']; ?></td>
																<td align="right"><?php echo number_format($list_assess['land_assess_price'], 2); ?></td>
																<td align="right"><?php echo number_format(($list_assess['land_assess_price_pre_min'] + $list_assess['land_assess_price_pre_max']) / 2, 2); ?></td>
																<td align="right"><?php echo number_format($list_assess['land_assess_price_basic'], 2); ?></td>
																<td align="right"><?php echo number_format($cal_assess, 2); ?></td>
																<td align="right">-</td>
															</tr>
													<?php
															}
														}
													?>
													</tbody>
													<tfoot>
														<tr>
															<td colspan="5" align="right">รวมทั้งสิ้น</td>
															<td align="right"><?php echo number_format($sum_assess, 2); ?></td>
															<td align="right"><?php echo number_format($sum_assess  / $loan['loan_amount'], 2); ?></td>
														</tr>
													</tfoot>
												</table>
											</div><!-- table-responsive -->
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">มีสิ่งปลูกสร้าง จำนวน</label>
												<div class="input-group">
													<input type="text" id="txtBuildingCount" name="txtBuildingCount" class="form-control right" value="<?php echo $building_count; ?>" readonly="readonly"/>
													<span class="input-group-addon">หลัง</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">ราคาประเมิน</label>
												<div class="input-group">
													<input type="text" class="form-control right" value="<?php echo number_format($building_sum,  2); ?>" readonly="readonly"/>
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">รวมราคาประเมินที่ดินและสิ่งปลูกสร้าง</label>
												<div class="input-group">
													<input type="text" id="txtRecordSum" name="txtRecordSum" class="form-control right" value="<?php echo number_format($sum_assess + $building_sum, 2); ?>" readonly="readonly"/>
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
									</div>
									<?php
										$txt_main = '';
										foreach ($land_main as $key_main => $list_main)
										{
											$txt_main .= $key_main.', ';
										}

										if($txt_main != '') $txt_main = substr($txt_main, 0, -2);
									?>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">การจำนองที่ดินซึ่งไถ่ถอนจากเจ้าหนี้ ได้แก่ แปลงที่</label>
												<input type="text" id="txtLandMain" name="txtLandMain" class="form-control" value="<?php echo $txt_main; ?>" readonly="readonly"/>
											</div><!-- form-group -->
										</div><!-- col-sm-6 -->
										<?php
										$txt_other = '';
										foreach ($land_other as $key_other => $list_other)
										{
											$txt_other .= $key_main.', ';
										}

										if($txt_other != '') $txt_other = substr($txt_other, 0, -2);
									?>
										<div class="col-sm-6">
											<div class="form-group">
												<label class="control-label">และจำนองที่ดินเพิ่มเติม ได้แก่ แปลงที่</label>
												<input type="text" id="txtLandOther" name="txtLandOther" class="form-control" value="<?php echo $txt_other; ?>"  readonly="readonly"/>
											</div><!-- form-group -->
										</div><!-- col-sm-6 -->
									</div><!-- row -->
									<?php if(!empty($loan_guarantee_bond)) { ?>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>พันธบัตรรัฐบาล</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table class="table table-striped mb30">
													<thead>
														<tr>
											            	<th>#</th>
											            	<th>ชื่อ - นามสกุล</th>
											                <th>ชนิดพันธบัตร</th>
											                <th>เลขที่ต้น</th>
											                <th>เลขที่ท้าย</th>
											                <th>ราคาที่ตราไว้</th>
														</tr>
													</thead>
													<tbody>
													<?php
														$i_bond = 1;
														foreach ($loan_guarantee_bond as $list_bond)
														{
														?>
															<tr>
																<td align="center"><?php echo $i_bond++; ?></td>
																<td><?php echo $list_bond['loan_bond_owner']; ?></td>
																<td><?php echo $list_bond['loan_bond_type']; ?></td>
																<td align="center"><?php echo $list_bond['loan_bond_startnumber']; ?></td>
																<td align="center"><?php echo $list_bond['loan_bond_endnumber']; ?></td>
																<td align="right"><?php echo number_format($list_bond['loan_bond_amount'], 2); ?></td>
															</tr>
													<?php
														}
													?>
													</tbody>
												</table>
											</div><!-- table-responsive -->
										</div>
									</div>
									<?php } ?>
									<?php if(!empty($loan_guarantee_bookbank)) { ?>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>เงินฝากในบัญชีของธนาคาร หรือสถาบันการเงิน หรือสหกรณ์</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table class="table table-striped mb30">
													<thead>
														<tr>
											            	<th>#</th>
											            	<th>ชื่อ - นามสกุล</th>
											                <th>ธนาคาร</th>
											                <th>เลขที่บัญชี</th>
											                <th>จำนวนเงิน</th>
														</tr>
													</thead>
													<tbody>
													<?php
														$i_bookbank = 1;
														foreach ($loan_guarantee_bookbank as $list_bookbank)
														{
														?>
															<tr>
																<td align="center"><?php echo $i_bookbank++; ?></td>
																<td><?php echo $list_bookbank['loan_bookbank_owner']; ?></td>
																<td><?php echo $list_bookbank['bank_name']; ?></td>
																<td align="center"><?php echo $list_bookbank['loan_bookbank_no']; ?></td>
																<td align="right"><?php echo number_format($list_bookbank['loan_bookbank_amount'], 2); ?></td>
															</tr>
													<?php } ?>
													</tbody>
												</table>
											</div><!-- table-responsive -->
										</div>
									</div>
									<?php } ?>
									<?php if(!empty($loan_guarantee_bondsman)) { ?>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>บุคคล</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="table-responsive">
												<table class="table table-striped mb30">
													<thead>
														<tr>
											            	<th>#</th>
											            	<th>ชื่อ - นามสกุล</th>
											                <th>อายุ</th>
											                <th>หมายเลขโทรศัพท์</th>
											                <th>อาชีพ</th>
											                <th>ตำแหน่ง</th>
											                <th>อัตราเงินเดือน</th>
														</tr>
													</thead>
													<tbody>
													<?php
														$i_bondsman = 1;
														foreach ($loan_guarantee_bondsman as $list_bondsman)
														{
															$age_bondsman = age($list_bondsman['person_birthdate']);
													?>
													<tr>
											            	<td align="center"><?php echo $i_bondsman++; ?></td>
											            	<td><?php echo $list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname']; ?></td>
											                <td align="right"><?php echo $age_bondsman; ?></td>
											                <td><?php echo $list_bondsman['person_mobile']; ?></td>
											                <td><?php echo $list_bondsman['career_name']; ?></td>
											                 <td><?php echo $list_bondsman['person_position']; ?></td>
											                <td align="right"><?php echo number_format($list_bondsman['person_income_per_month'], 2); ?></td>
														</tr>
													<?php } ?>
													</tbody>
												</table>
											</div><!-- table-responsive -->
										</div>
									</div>
									<?php } ?>
								</div><!-- tab-pane -->
								<div id="tab_4" class="widget-bloglist tab-pane">
									<!-- <div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>ผู้ขอสินเชื่อ</h4>
										</div>
									</div> -->
									<div id="divExpenditure"></div>
									<?php if($check_type_farm) { ?>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">แผนการผลิตเพื่อฟื้นฟูอาชีพของผู้ขอสินเชื่อ</label>
												<textarea id="txtFarmFor" name="txtFarmFor" class="form-control" rows="5" readonly="readonly"><?php echo $loan_type_farm_for; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-12 -->
									</div><!-- row -->
									<?php } ?>
								</div><!-- tab-pane -->
								<div id="tab_5" class="widget-bloglist tab-pane">
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>คำนวณ</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">เงินต้น</label>
												<div class="input-group">
													<input type="text" id="txtCalAmount" name="txtCalAmount" class="form-control right" value="<?php echo number_format($loan['loan_amount'], 2); ?>" readonly="readonly"/>
													<span class="input-group-addon">บาท</span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4-->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">อัตราดอกเบี้ย</label>
												<select name="ddlCalInterest" id="ddlCalInterest" data-placeholder="กรุณาเลือก" class="width100p">
													<option value="3"<?php echo $data['loan_analysis_interest']=='3'? ' selected' : ''; ?>>3</option>
													<option value="4"<?php echo $data['loan_analysis_interest']=='4'? ' selected' : ''; ?>>4</option>
													<option value="5"<?php echo $data['loan_analysis_interest']=='5'? ' selected' : ''; ?>>5</option>
												</select>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">การชำระ</label>
												<select name="ddlCalType" id="ddlCalType" data-placeholder="กรุณาเลือก" class="width100p">
													<option value="12"<?php echo $data['loan_analysis_length']=='12'? ' selected' : ''; ?>>รายปี</option>
													<option value="6"<?php echo $data['loan_analysis_length']=='6'? ' selected' : ''; ?>>ราย 6 เดือน</option>
													<option value="1"<?php echo $data['loan_analysis_length']=='1'? ' selected' : ''; ?>>รายเดือน</option>
												</select>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
									</div>
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">ระยะเวลา</label>
												<div class="input-group">
													<input type="text" id="txtCalYear" name="txtCalYear" class="form-control right" value="<?php echo $data['loan_analysis_year']; ?>" />
													<span class="input-group-addon">ปี</span>
												</div>
												<label class="error" for="txtCalYear"></label>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">จำนวนงวด</label>
												<div class="input-group">
													<input type="text" id="txtCycle" name="txtCycle" class="form-control right" value="<?php echo $data['loan_analysis_cycle']; ?>" />
													<span class="input-group-addon">งวด</span>
												</div>
												<label class="error" for="txtCycle"></label>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
									</div>

									<div class="row">

										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">วันที่เริ่มต้น</label>
												<div class="input-group">
													<input type="text" id="txtCalStart" name="txtCalStart" class="form-control" value="<?php echo $data['loan_analysis_start']; ?>" readonly="readonly" />
													<span id="iconStart" class="input-group-addon hand" ><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">วันที่ชำระ</label>
												<div class="input-group">
													<input type="text" id="txtCalPay" name="txtCalPay" class="form-control" value="<?php echo $data['loan_analysis_pay']; ?>" readonly="readonly" />
													<span id="iconPay" class="input-group-addon hand" ><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label">รายได้</label>
												<select name="ddlCalIncomeYear" id="ddlCalIncomeYear" data-placeholder="กรุณาเลือก" class="width100p">
													<option value="1"<?php echo $data['loan_analysis_income_year']=='1'? ' selected' : ''; ?>>ปีที่ 1 (ปีปัจจุบัน)</option>
													<option value="2"<?php echo $data['loan_analysis_income_year']=='2'? ' selected' : ''; ?>>ปีที่ 2 (ปีก่อน)</option>
													<option value="3"<?php echo $data['loan_analysis_income_year']=='3'? ' selected' : ''; ?>>ปีที่ 3 (ปีหลัง)</option>
												</select>
											</div><!-- form-group -->
										</div><!-- col-sm-4 -->
									</div>

									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12 right">
											<button type="button" id="btnCal" class="btn btn-primary">คำนวน</button>
										</div><!-- col-sm-12 -->
									</div>
									<div id="divCal" class="row" style="width: 100%; overflow:scroll;"></div>
								</div><!-- tab-pane -->
								<div id="tab_6" class="widget-bloglist tab-pane">
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>พิจารณาตามหลักเกณฑ์ของ บจธ.</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">ความเห็น</label>
												<textarea id="txtInvestigateComment" name="txtInvestigateComment" class="form-control" rows="5" ><?php echo $data['loan_analysis_investigate_comment']; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-12 -->
									</div><!-- row -->
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>มูลเหตุแห่งหนี้</h4>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">สุจริตหรือไม่สุจริต ดังนี้</label>
												<textarea id="txtInvestigateHonestly" name="txtInvestigateHonestly" class="form-control" rows="5" maxlength="500"><?php echo $data['loan_analysis_investigate_honestly']; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-12 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">กรณีระยะเวลาในการก่อหนี้สินเพิ่งจะเกิดขึ้นไม่ถึงหนึ่งปี มีเหตุผลที่น่าเชื่อว่าหนี้นั้นเกิดขึ้นโดยสุจริต ดังนี้</label>
												<textarea id="txtInvestigateCause" name="txtInvestigateCause" class="form-control" rows="5" maxlength="500"><?php echo $data['loan_analysis_investigate_cause']; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-12 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">ความจำเป็นหรือไม่มีความจำเป็น ดังนี้</label>
												<textarea id="txtInvestigateMust" name="txtInvestigateMust" class="form-control" rows="5" maxlength="500"><?php echo $data['loan_analysis_investigate_must']; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-12 -->
									</div><!-- row -->
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label">กรณีที่ดินแปลงซึ่งขอสินเชื่อเพื่อไถ่ถอนหรือซื้อคืนมีมูลค่าน้อยกว่าจำนวนเงินที่ขอสินเชื่อ การอนุมัติคำขอสินเชื่อครั้งนี้จะเป็นประโยชน์ต่อผู้ขอสินเชื่ออย่างไร</label>
												<textarea id="txtInvestigateMatter" name="txtInvestigateMatter" class="form-control" rows="5" maxlength="500"><?php echo $data['loan_analysis_investigate_matter']; ?></textarea>
											</div><!-- form-group -->
										</div><!-- col-sm-12 -->
									</div><!-- row -->
								</div><!-- tab-pane -->
								<div id="tab_7" class="widget-bloglist tab-pane">
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>สรุปความเห็นและผลการพิจารณาเกี่ยวกับการขอสินเชื่อตามคำขอ</h4>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" name="rdApproveStatus" id="rdApproveStatus1" value="1" <?php if($data['loan_analysis_approve_status']=='1') echo 'checked="checked"'; ?>>
													<label for="rdApproveStatus1" style="padding-left:20px;">เห็นสมควรอนุมัติ</label>
												</div>
											</div><!-- col-sm-2 -->
											<div id="divApprove" class="col-sm-10" <?php if($data['loan_analysis_approve_status']!='1') echo 'style="display:none;"'; ?>>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="control-label">วงเงินสินเชื่อ</label>
															<div class="input-group">
																<input type="text" id="txtApproveAmount" name="txtApproveAmount" class="form-control right" value="<?php echo number_format($loan['loan_amount'], 2); ?>" readonly="readonly"/>
																<span class="input-group-addon">บาท</span>
															</div>
														</div><!-- form-group -->
													</div><!-- col-sm-6 -->
													<div class="col-sm-6">
														<div class="form-group">
															<label class="control-label">ภายในวันที่</label>
															<div class="input-group">
																<input type="text" id="txtApproveDate" name="txtApproveDate" class="form-control" value="<?php echo $data['loan_analysis_approve_date']; ?>" readonly="readonly" />
																<span id="iconApprove" class="input-group-addon hand" ><i class="glyphicon glyphicon-calendar"></i></span>
															</div>
														</div><!-- form-group -->
													</div><!-- col-sm-6 -->
												</div><!-- row -->
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label class="control-label">เงื่อนไขหรือคำชี้แจงเพิ่มเติม</label>
															<textarea id="txtApproveConditions" name="txtApproveConditions" class="form-control" rows="5" maxlength="500"><?php echo $data['loan_analysis_approve_conditions']; ?></textarea>
														</div><!-- form-group -->
													</div><!-- col-sm-12 -->
												</div><!-- row -->
											</div><!-- col-sm-10 -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" name="rdApproveStatus" id="rdApproveStatus0" value="0" <?php if($data['loan_analysis_approve_status']!='1') echo 'checked="checked"'; ?>>
													<label for="rdApproveStatus0" style="padding-left:20px;">เห็นไม่สมควรอนุมัติ</label>
												</div>
											</div><!-- col-sm-2 -->
											<div id="divDisapprove" class="col-sm-10" <?php if($data['loan_analysis_approve_status']=='1') echo 'style="display:none;"'; ?>>
												<div class="row">
													<div class="col-sm-4">
														<div class="form-group">
															<div class="rdio rdio-default">
																<input type="radio" name="rdApproveNotType" id="rdApproveNotType1" value="1" <?php if($data['loan_analysis_approve_not_type']=='1') echo 'checked="checked"'; ?>>
																<label for="rdApproveNotType1" style="padding-left:20px;">อยู่ในหลักเกณฑ์ให้สินเชื่อจาก</label>
															</div>
														</div>
													</div><!-- col-sm-4 -->
													<div class="col-sm-4">
														<div class="form-group">
															<div class="rdio rdio-default">
																<input type="radio" name="rdApproveNotType" id="rdApproveNotType0" value="0" <?php if($data['loan_analysis_approve_not_type']!='1') echo 'checked="checked"'; ?>>
																<label for="rdApproveNotType0" style="padding-left:20px;">ไม่อยู่ในหลักเกณฑ์ให้สินเชื่อจาก</label>
															</div>
														</div>
													</div><!-- col-sm-4 -->
												</div>
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label class="control-label">บจธ. ปี</label>
															<input type="text" id="txtApproveNotYear" name="txtApproveNotYear" class="form-control" value="<?php echo $data['loan_analysis_approve_not_year']; ?>" maxlength="4" />
														</div><!-- form-group -->
													</div><!-- col-sm-3 -->
												</div>
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label class="control-label">เนื่องจาก</label>
															<textarea id="txtApproveNotCase" name="txtApproveNotCase" class="form-control" rows="3" maxlength="500"><?php echo $data['loan_analysis_approve_not_case']; ?></textarea>
														</div><!-- form-group -->
													</div><!-- col-sm-5 -->
												</div>
											</div><!-- col-sm-10 -->
										</div>
									</div>
									<hr/>
									<div class="row" style="padding-bottom:10px;">
										<div class="col-sm-12">
											<h4>สรุปความเห็นในการพิจารณาคำขอสินเชื่อของฝ่าย/กอง</h4>
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" name="rdApproveSectorStatus" id="rdApproveSectorStatus1" value="1" <?php if($data['loan_analysis_approve_sector_status']=='1') echo 'checked="checked"'; ?>>
													<label for="rdApproveSectorStatus1" style="padding-left:20px;">เห็นสมควรอนุมัติ</label>
												</div>
											</div><!-- col-sm-2 -->
											<div id="divApproveSector" class="col-sm-10" <?php if($data['loan_analysis_approve_sector_status']!='1') echo 'style="display:none;"'; ?>>
												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															<label class="control-label">วงเงินสินเชื่อ</label>
															<div class="input-group">
																<input type="text" id="txtApproveSectorAmount" name="txtApproveSectorAmount" class="form-control right" value="<?php echo number_format($loan['loan_amount'], 2); ?>" readonly="readonly"/>
																<span class="input-group-addon">บาท</span>
															</div>
														</div><!-- form-group -->
													</div><!-- col-sm-6 -->
													<div class="col-sm-6">
														<div class="form-group">
															<label class="control-label">ภายในวันที่</label>
															<div class="input-group">
																<input type="text" id="txtApproveSectorDate" name="txtApproveSectorDate" class="form-control" value="<?php echo $data['loan_analysis_approve_sector_date']; ?>" readonly="readonly" />
																<span id="iconApproveSector" class="input-group-addon hand" ><i class="glyphicon glyphicon-calendar"></i></span>
															</div>
														</div><!-- form-group -->
													</div><!-- col-sm-6 -->
												</div><!-- row -->
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label class="control-label">เงื่อนไขหรือคำชี้แจงเพิ่มเติม</label>
															<textarea id="txtApproveSectorConditions" name="txtApproveSectorConditions" class="form-control" rows="5" maxlength="500"><?php echo $data['loan_analysis_approve_sector_conditions']; ?></textarea>
														</div><!-- form-group -->
													</div><!-- col-sm-12 -->
												</div><!-- row -->
											</div><!-- col-sm-10 -->
										</div>
									</div>
									<div class="row">
										<div class="form-group">
											<div class="col-sm-2">
												<div class="rdio rdio-default">
													<input type="radio" name="rdApproveSectorStatus" id="rdApproveSectorStatus0" value="0" <?php if($data['loan_analysis_approve_sector_status']!='1') echo 'checked="checked"'; ?>>
													<label for="rdApproveSectorStatus0" style="padding-left:20px;">เห็นไม่สมควรอนุมัติ</label>
												</div>
											</div><!-- col-sm-2 -->
											<div id="divDisapproveSector" class="col-sm-10" <?php if($data['loan_analysis_approve_sector_status']=='1') echo 'style="display:none;"'; ?>>
												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															<label class="control-label">เนื่องจาก</label>
															<textarea id="txtApproveSectorNotCase" name="txtApproveSectorNotCase" class="form-control" rows="3" maxlength="500"><?php echo $data['loan_analysis_approve_sector_not_case']; ?></textarea>
														</div><!-- form-group -->
													</div><!-- col-sm-12 -->
												</div><!-- row -->
											</div><!-- col-sm-10 -->
										</div>
									</div>
								</div>
							</div><!-- tab-content -->
						</div>
					</div>
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 -->
	</div>
</form>
<div id="modalExpenditure" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				<h4 class="modal-title">ค่าใช้จ่ายอื่นๆ</h4>
			</div>
			<div class="modal-body">
				<form id="frmExpenditure" action="<?php echo base_url().'analysis/save_expenditure'; ?>" method="post" class="form-horizontal form-bordered">
					<input type="hidden" id="txtLoan" name="txtLoan" value="<?php echo $data['loan_id']; ?>" />
					<input type="hidden" id="txtIdExpenditure" name="txtIdExpenditure"  value="" />
					<input type="hidden" id="txtNameExpenditure" name="txtNameExpenditure"  value="" />
					<input type="hidden" id="txtYearExpenditure" name="txtYearExpenditure"  value="" />
					<div class="form-group" style="border:0px;">
						<div class="input-group mb15">
							<input type="text" id="txtExpenditure" name="txtExpenditure" placeholder="Default Input" class="form-control" value="0.00"  />
							<span class="input-group-addon">บาท</span>
						</div>
						<label class="error" for="txtExpenditure"></label>
					</div><!-- form-group -->
				</form>
			</div>
			<div class="modal-footer">
				<button id="btnExpenditure" type="button" class="btn btn-primary">บันทึก</button>
			</div>
		</div>
	</div>
</div>
<div id="modalAnaCal2" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				<h4 class="modal-title">ประมาณการ</h4>
			</div>
			<div class="modal-body">
				<form id="frmAnaCal2" action="<?php echo base_url().'analysis/save_cal2'; ?>" method="post" class="form-horizontal form-bordered">
					<div id="divCalAna2"></div>
				</form>
			</div>
			<div class="modal-footer">
				<button id="btnAnaCal2" type="button" class="btn btn-primary">บันทึก</button>
			</div>
		</div>
	</div>
</div>
<div id="modalAnaCal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				<h4 class="modal-title">ประมาณการ</h4>
			</div>
			<div class="modal-body">
				<form id="frmAnaCal" action="<?php echo base_url().'analysis/save_cal'; ?>" method="post" class="form-horizontal form-bordered">
					<input type="hidden" id="txtAnaCalYear" name="txtAnaCalYear"  value="" />
					<input type="hidden" id="txtAnaCalType" name="txtAnaCalType"  value="" />
					<input type="hidden" id="txtAnaCalAmount" name="txtAnaCalAmount"  value="" />
					<div id="divCalAna"></div>
				</form>
			</div>
			<div class="modal-footer">
				<button id="btnAnaCal" type="button" class="btn btn-primary">บันทึก</button>
			</div>
		</div>
	</div>
</div>
<div id="modalOther" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				<h4 class="modal-title">เงินกู้จากเจ้าหนี้อื่นๆ</h4>
			</div>
			<div class="modal-body">
				<form id="frmOther" action="<?php echo base_url().'analysis/save_other'; ?>" method="post" class="form-horizontal form-bordered">
					<input type="hidden" id="txtRound" name="txtRound"  value="" />
					<div class="form-group" style="border:0px;">
						<div class="input-group mb15">
							<input type="text" id="txtOther" name="txtOther" placeholder="Default Input" class="form-control" value="0.00"  />
							<span class="input-group-addon">บาท</span>
						</div>
						<label class="error" for="txtOther"></label>
					</div><!-- form-group -->
				</form>
			</div>
			<div class="modal-footer">
				<button id="btnOther" type="button" class="btn btn-primary">บันทึก</button>
			</div>
		</div>
	</div>
</div>
<div id="modalPrincipleOther" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				<h4 class="modal-title">ชำระดอกเบี้ยต้นเงินหนี้อื่นๆ</h4>
			</div>
			<div class="modal-body">
				<form id="frmPrincipleOther" action="<?php echo base_url().'analysis/save_principle_other'; ?>" method="post" class="form-horizontal form-bordered">
					<input type="hidden" id="txtRound" name="txtRound"  value="" />
					<div class="form-group" style="border:0px;">
						<div class="input-group mb15">
							<input type="text" id="txtPrincipleOther" name="txtPrincipleOther" placeholder="Default Input" class="form-control" value="0.00"  />
							<span class="input-group-addon">บาท</span>
						</div>
						<label class="error" for="txtPrincipleOther"></label>
					</div><!-- form-group -->
				</form>
			</div>
			<div class="modal-footer">
				<button id="btnPrincipleOther" type="button" class="btn btn-primary">บันทึก</button>
			</div>
		</div>
	</div>
</div>
<div id="modalInterestOther" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				<h4 class="modal-title">ชำระดอกเบี้ยหนี้อื่นๆ</h4>
			</div>
			<div class="modal-body">
				<form id="frmInterestOther" action="<?php echo base_url().'analysis/save_interest_other'; ?>" method="post" class="form-horizontal form-bordered">
					<input type="hidden" id="txtRound" name="txtRound"  value="" />
					<div class="form-group" style="border:0px;">
						<div class="input-group mb15">
							<input type="text" id="txtInterestOther" name="txtInterestOther" placeholder="Default Input" class="form-control" value="0.00"  />
							<span class="input-group-addon">บาท</span>
						</div>
						<label class="error" for="txtInterestOther"></label>
					</div><!-- form-group -->
				</form>
			</div>
			<div class="modal-footer">
				<button id="btnInterestOther" type="button" class="btn btn-primary">บันทึก</button>
			</div>
		</div>
	</div>
</div>
