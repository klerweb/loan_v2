<div class="table-responsive">
	<table class="table table-hover mb30">
		<thead>
			<tr>
				<th>#</th>
				<th>เงินต้น</th>
				<th>เงินกู้จากเจ้าหนี้อื่นๆ</th>
				<th>ชำระดอกเบี้ยต้นเงินหนี้อื่นๆ</th>
				<th>ชำระดอกเบี้ยหนี้อื่นๆ</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				if(!empty($round))
				{ 
					for($r=1; $r<=$round; $r++)
					{
			?>
			<tr>
				<td>
					<?php echo $r; ?>
					<input type="hidden" id="txtCalRound_<?php echo $r; ?>" name="txtCalRound[]"  class="form-control" value="<?php echo $r; ?>" />
				</td>
				<td><input type="text" id="txtCalAmount_<?php echo $r; ?>" name="txtCalAmount[]"  class="form-control" value="<?php echo $principle; ?>" /></td>
				<td><input type="text" id="txtCalOther_<?php echo $r; ?>" name="txtCalOther[]" class="form-control" value="0" /></td>
				<td><input type="text" id="txtCalPrinciple_<?php echo $r; ?>" name="txtCalPrinciple[]" class="form-control" value="0" /></td>
				<td><input type="text" id="txtCalInterest_<?php echo $r; ?>" name="txtCalInterest[]" class="form-control"  value="0" /></td>
			</tr>
			<?php 
					}
				} 
			?>
		</tbody>
	</table>
</div><!-- table-responsive -->