<div class="contentpanel">
<div class="row">
	<div class="col-sm-12">
		<br/>
		<?php
			$html = '';
			$sum_estimate_amt = 0;
			if(!empty($list_round))
			{
				$remain = 0;
				$i_round = 1;
				$i_tab = 1;
				$i_start = 1;
				$c_round = 1;
				$count_round = count($list_round);

				$html_round = '';
				$html_1 = '';
				$html_1_1 = '';
				$html_1_2 = '';
				$html_1_3 = '';
				$html_1_4 = '';
				$html_1_5 = '';
				$html_1_6 = '';
				$html_2 = '';
				$html_2_1 = '';
				$html_2_2 = '';
				$html_2_3 = '';
				$html_2_4 = '';
				$html_2_5 = '';
				$html_2_6 = '';
				$html_2_7 = '';
				$html_2_8 = '';
				$html_2_9 = '';
				$html_3 = '';
				$html_4 = '';
				foreach ($list_round as $key_round => $round)
				{
					$data1_4 = $round['other'];
					if($key_round=='1')
					{
						$data1_2 = $amount;
						$data1_6 = $round['income'] + $data1_2 + $data1_4;
						$data2_2 = $amount;


					}
					else
					{
						$data1_2 = 0;
						$data1_6 = $round['income'] + $data1_4;
						$data2_2 = 0;
					}
					$sum_estimate_amt += $round['principle'] + $round['interest'];

					$data2_3 = $round['principle'];
					$data2_4 = $round['interest'];
					$data2_7 = $round['principle_other'];
					$data2_8 = $round['interest_other'];

					$data2_9 = $data2_2 + $data2_3 + $data2_4 + $data2_7 + $data2_8;
					$data3 = $data1_6 - $data2_9;

					$remain += $data3;

					$html_round .= '<th style="text-align:center;">'.$key_round.'</th>';
					$html_1 .= '<td align="center">xx</td>';
					$html_1_1 .= '<td align="right">'.number_format($round['income'], 2).'</td>';
					$html_1_2 .= '<td align="right">'.number_format($data1_2, 2).'</td>';
					$html_1_3 .= '<td align="right">0.00</td>';
					$html_1_4 .= '<td align="right">'.number_format($data1_4, 2).' <a style="cursor: pointer; cursor: hand;" onclick="edit_other(\''.$key_round.'\', \''.$data1_4.'\')"><span class="glyphicon glyphicon-edit"></span></a></td></td>';
					$html_1_5 .= '<td align="right">0.00</td>';
					$html_1_6 .= '<td align="right">'.number_format($data1_6, 2).'</td>';
					$html_2 .= '<td></td>';
					$html_2_1 .= '<td align="right">'.number_format($round['remain'], 2).'</td>';
					$html_2_2 .= '<td align="right">'.number_format($data2_2, 2).'</td>';
					$html_2_3 .= '<td align="right">'.number_format($data2_3, 2).'</td>';
					$html_2_4 .= '<td align="right">'.number_format($data2_4, 2).'</td>';
					$html_2_5 .= '<td align="right">0.00</td>';
					$html_2_6 .= '<td align="right">0.00</td>';
					$html_2_7 .= '<td align="right">'.number_format($data2_7, 2).' <a style="cursor: pointer; cursor: hand;" onclick="edit_principle_other(\''.$key_round.'\', \''.$data2_7.'\')"><span class="glyphicon glyphicon-edit"></span></a></td>';
					$html_2_8 .= '<td align="right">'.number_format($data2_8, 2).' <a style="cursor: pointer; cursor: hand;" onclick="edit_interest_other(\''.$key_round.'\', \''.$data2_8.'\')"><span class="glyphicon glyphicon-edit"></span></a></td>';
					$html_2_9 .= '<td align="right">'.number_format($data2_9, 2).'</td>';
					$html_3 .= '<td align="right">'.number_format($data3, 2).'</td>';
					$html_4 .= '<td align="right">'.number_format($remain, 2).'</td>';

					if($i_round%5==0 || $i_round==$count_round)
					{
						$style = $i_tab==1? ' in' : '';
						$html .= '<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse_'.$i_tab.'">งวดที่ '.$i_start.' - '.$i_round.'</a>
								</h4>
							</div>
							<div id="collapse_'.$i_tab.'" class="panel-collapse collapse'.$style.'">
								<div class="panel-body">
									<div class="col-sm-12">
										<div class="table-responsive">
											<table class="table table-bordered mb30" style="width:100%;">
												<thead>
													<tr>
														<th rowspan="2" style="text-align:center; vertical-align: middle; width:350px;">รายการ</th>
														<th colspan="'.$c_round.'" style="text-align:center;">งวดที่ (ตั้งแต่งวดที่ '.$i_start.' จนถึงงวดที่ '.$i_round.')</th>
													</tr>
													<tr>
														'.$html_round.'
													</tr>
												</thead>
												<tbody>
													<tr><td style="width:350px;">1. เงินสดรับ (บาท)</td>'.$html_1.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(1) รายได้สุทธิ</td>'.$html_1_1.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(2) สินเชื่อของ บจธ.</td>'.$html_1_2.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(3) สินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม (บจธ.)</td>'.$html_1_3.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(4) เงินกู้จากเจ้าหนี้อื่นๆ</td>'.$html_1_4.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(5) เงินทุนสมทบของตนเอง (ถ้ามี)</td>'.$html_1_5.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(6) รวมเงินสดรับ (1)+(2)+(3)+(4)+(5)</td>'.$html_1_6.'</tr>
													<tr><td style="width:350px;">2. เงินสดจ่าย (บาท)</td>'.$html_2.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(1) สินเชื่อ บจธ. คงเหลือ</td>'.$html_2_1.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(2) ชำระหนี้สินเดิมให้เจ้าหนี้</td>'.$html_2_2.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(3) ชำระต้นเงินสินเชื่อของ บจธ.</td>'.$html_2_3.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(4) ชำระดอกเบี้ยของสินเชื่อของ บจธ.</td>'.$html_2_4.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(5) ชำระต้นเงินสินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม</td>'.$html_2_5.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(6) ชำระดอกเบี้ยสินเชื่อเพื่อการประกอบอาชีพเกษตรกรรม</td>'.$html_2_6.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(7) ชำระดอกเบี้ยต้นเงินหนี้อื่นๆ</td>'.$html_2_7.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(8) ชำระดอกเบี้ยหนี้อื่นๆ</td>'.$html_2_8.'</tr>
													<tr><td style="width:350px;">&nbsp;&nbsp;&nbsp;&nbsp;(9) รวมเงินสดจ่าย (2)+(3)+(4)+(5)+(6)+(7)+(8)</td>'.$html_2_9.'</tr>
													<tr><td style="width:350px;">3. เงินสดคงเหลือ 1 (6) - 2 (9)</td>'.$html_3.'</tr>
													<tr><td style="width:350px;">4. เงินสดคงเหลือสะสม</td>'.$html_4.'</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div><!-- panel -->';

						$i_tab++;
						$i_start = $i_round + 1;
						$c_round = 1;

						$html_round = '';
						$html_1 = '';
						$html_1_1 = '';
						$html_1_2 = '';
						$html_1_3 = '';
						$html_1_4 = '';
						$html_1_5 = '';
						$html_1_6 = '';
						$html_2 = '';
						$html_2_1 = '';
						$html_2_2 = '';
						$html_2_3 = '';
						$html_2_4 = '';
						$html_2_5 = '';
						$html_2_6 = '';
						$html_2_7 = '';
						$html_2_8 = '';
						$html_2_9 = '';
						$html_3 = '';
						$html_4 = '';
					}
					else
					{
						$c_round++;
					}

					$i_round++;
				}
				$estimate_plan =  floor($sum_estimate_amt / ($year * 12 / $type)/100) * 100;
	 		}
	 	?>
	 	<div class="panel-group" id="accordion">
	 		<?php echo $html; ?>
	 	</div><!-- panel-group -->
	</div>
</div>
<div class="row" style="padding:10px;">
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label">ชำระคืนเงินต้นเสร็จสิ้นภายใน</label>
			<div class="input-group">
				<input type="text" id="txtShowYear"  name="txtShowYear" class="form-control right" value="<?php echo $year; ?>" readonly="readonly" />
				<span class="input-group-addon">ปี</span>
			</div>
		</div><!-- form-group -->
	</div><!-- col-sm-4 -->
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label">ตั้งแต่ปีบัญชี</label>
			<input type="text" id="txtShowFiscalStart"  name="txtShowFiscalStart" class="form-control" value="<?php echo $fiscal_start; ?>" disabled="disabled"  />
		</div><!-- form-group -->
	</div><!-- col-sm-4 -->
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label">ถึงปีบัญชี</label>
			<input type="text" id="txtShowFiscalEnd"  name="txtShowFiscalEnd" class="form-control" value="<?php echo $fiscal_end; ?>" disabled="disabled"  />
		</div><!-- form-group -->
	</div><!-- col-sm-4 -->
</div><!-- row -->
<div class="row" style="padding:10px;">
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label">โดยชำระต้นเงินและดอกเบี้ยได้ เป็นเงินปีละประมาณ </label>
			<div class="input-group">
				<input type="text" id="txtShowFirst"  name="txtShowFirst" class="form-control right" value="<?php echo number_format($estimate_plan, 2) ;?>" disabled="disabled"  />
				<span class="input-group-addon">บาท</span>
			</div>
		</div><!-- form-group -->
	</div><!-- col-sm-4 -->
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label">ชำระหนี้ได้ปีละ</label>
				<div class="input-group">
					<?php
						switch ($type)
						{
							case 1: $type = 12;
								break;
							case 6: $type = 2;
								break;
							case 12: $type = 1;
								break;
						}
					?>
					<input type="text" id="txtShowTime"  name="txtShowTime" class="form-control right" value="<?php echo $type; ?>" disabled="disabled" />
					<span class="input-group-addon">ครั้ง</span>
				</div>
			</div><!-- form-group -->
		</div><!-- col-sm-4 -->
		<div class="col-sm-4">
			<div class="form-group">
				<label class="control-label">ในแต่ละปีชำระได้ในเดือน</label>
				<input type="text" id="txtShowMonth"  name="txtShowMonth" class="form-control" value="<?php echo $month; ?>" disabled="disabled" />
			</div><!-- form-group -->
		</div><!-- col-sm-4 -->
	</div><!-- row -->
</div>
