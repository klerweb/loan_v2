<?php 
	$count_income_1 = count($loan_income_1);
	$count_income_2 = count($loan_income_2);
	$count_income_3 = count($loan_income_3);
	$count_expenditure_1 = count($loan_expenditure_1);
	$count_expenditure_2 = count($loan_expenditure_2);
	$count_expenditure_all = $count_expenditure_1 > $count_expenditure_2? $count_expenditure_1 : $count_expenditure_2;
			
	$cal_all = array();
	$sum_all = array(
			'year1' => 0,
			'year2' => 0,
			'year3' => 0,
		);
			
	$td_len = $count_income_1>=$count_income_2? $count_income_1 : $count_income_2;
	if($td_len<$count_income_3) $td_len =  $count_income_3;
	if($td_len<$count_expenditure_1) $td_len =  $count_expenditure_1;
	if($td_len<$count_expenditure_2) $td_len =  $count_expenditure_2;
			
	$td_len++;
			
	$td_income_1 = '';
	$td_income_2 = '';
	$td_income_3 = '';
	$td_expenditure = '';
	for($i_len = 1; $i_len <= $td_len; $i_len++)
	{
		if($i_len==$td_len)
		{
			$td_income_1 .= '<th style="text-align:center;">รวม</th>';
			$td_income_2 .= '<th style="text-align:center;">xx</th>';
			$td_income_3 .= '<th style="text-align:center;">xx</th>';
			$td_expenditure .= '<th style="text-align:center;">xx</th>';
		}
		else
		{
			$td_income_1 .= '<th style="text-align:center;">xx</th>';
			$td_income_2 .= '<th style="text-align:center;">xx</th>';
			$td_income_3 .= '<th style="text-align:center;">xx</th>';
			$td_expenditure .= '<th style="text-align:center;">xx</th>';
		}
	}	

	$html_income1 = '';
	if(!empty($loan_income_1)) 
	{
		$sum_year1_income1 = 0;
		$sum_year2_income1 = 0;
		$sum_year3_income1 = 0;
		$sum_year1_expenditure1 = 0;
		$sum_year2_expenditure1 = 0;
		$sum_year3_expenditure1 = 0;
		$sum_year1_sum1 = 0;
		$sum_year2_sum1 = 0;
		$sum_year3_sum1 = 0;
		$td_income1_year1_1 = '';
		$td_income1_year2_1 = '';
		$td_income1_year3_1 = '';
		$td_income1_year1_2 = '';
		$td_income1_year2_2 = '';
		$td_income1_year3_2 = '';
		$td_income1_year1_3 = '';
		$td_income1_year2_3 = '';
		$td_income1_year3_3 = '';
		$td_income1_year1_4 = '';
		$td_income1_year2_4 = '';
		$td_income1_year3_4 = '';
		$td_income1_year1_5 = '';
		$td_income1_year2_5 = '';
		$td_income1_year3_5 = '';
		$td_income1_year1_6 = '';
		$td_income1_year2_6 = '';
		$td_income1_year3_6 = '';
			
		$i_income_1 = 0;
			
		foreach ($loan_income_1 as $list_income_1)
		{
			$year1_income = floatval($list_income_1['loan_activity_income1']);
			$year2_income = floatval($list_income_1['loan_activity_income2']);
			$year3_income = floatval($list_income_1['loan_activity_income3']);
		
			$sum_year1_income1 += $year1_income;
			$sum_year2_income1 += $year2_income;
			$sum_year3_income1 += $year3_income;
		
			$year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_1['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_1['loan_activity_expenditure3']);
				
			$sum_year1_expenditure1 += $year1_expenditure;
			$sum_year2_expenditure1 += $year2_expenditure;
			$sum_year3_expenditure1 += $year3_expenditure;
		
			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;
		
			$sum_year1_sum1 += $year1_sum;
			$sum_year2_sum1 += $year2_sum;
			$sum_year3_sum1 += $year3_sum;
				
			$rai1 = intval($list_income_1['loan_activity_self_rai']) + intval($list_income_1['loan_activity_renting_rai']) + intval($list_income_1['loan_activity_approve_rai']);
			$ngan1 = intval($list_income_1['loan_activity_self_ngan']) + intval($list_income_1['loan_activity_renting_ngan']) + intval($list_income_1['loan_activity_approve_ngan']);
			$wah1 = floatval($list_income_1['loan_activity_self_wah']) + floatval($list_income_1['loan_activity_renting_wah']) + floatval($list_income_1['loan_activity_approve_wah']);
				
			$area1 = area($rai1, $ngan1, $wah1);
				
			$td_income1_year1_1 .= '<td align="center">'.num2Thai($list_income_1['loan_activity_name']).'</td>';
			$td_income1_year2_1 .= '<td align="center">'.num2Thai($list_income_1['loan_activity_name']).'</td>';
			$td_income1_year3_1 .= '<td align="center">'.num2Thai($list_income_1['loan_activity_name']).'</td>';
			$td_income1_year1_2 .= '<td align="center">๑ ปี</td>';
			$td_income1_year2_2 .= '<td align="center">๑ ปี</td>';
			$td_income1_year3_2 .= '<td align="center">๑ ปี</td>';
			$td_income1_year1_3 .= '<td align="center">'.num2Thai($area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah']).' ไร่</td>';
			$td_income1_year2_3 .= '<td align="center">'.num2Thai($area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah']).' ไร่</td>';
			$td_income1_year3_3 .= '<td align="center">'.num2Thai($area1['rai'].' - '.$area1['ngan'].' - '.$area1['wah']).' ไร่</td>';
			$td_income1_year1_4 .= '<td align="right">'.num2Thai(number_format($year1_income, 2)).'</td>';
			$td_income1_year2_4 .= '<td align="right">'.num2Thai(number_format($year2_income, 2)).'</td>';
			$td_income1_year3_4 .= '<td align="right">'.num2Thai(number_format($year3_income, 2)).'</td>';
			$td_income1_year1_5 .= '<td align="right">'.num2Thai(number_format($year1_expenditure, 2)).'</td>';
			$td_income1_year2_5 .= '<td align="right">'.num2Thai(number_format($year2_expenditure, 2)).'</td>';
			$td_income1_year3_5 .= '<td align="right">'.num2Thai(number_format($year3_expenditure, 2)).'</td>';
			$td_income1_year1_6 .= '<td align="right">'.num2Thai(number_format($year1_sum, 2)).'</td>';
			$td_income1_year2_6 .= '<td align="right">'.num2Thai(number_format($year2_sum, 2)).'</td>';
			$td_income1_year3_6 .= '<td align="right">'.num2Thai(number_format($year3_sum, 2)).'</td>';
		
			$cal_all[$i_income_1++] = array(
					'year1' => $year1_sum,
					'year2' => $year2_sum,
					'year3' => $year3_sum
			);
		}
		
		$td_null_income1 = '';
		if($count_income_1 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_1;
			$td_null_income1 = str_repeat('<td align="center">xx</td>', $count_null);
			 
			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				$cal_all[$i_income_1++] = array(
						'year1' => 0,
						'year2' => 0,
						'year3' => 0
					);
			}
		}
		
		$sum_all['year1'] += $sum_year1_sum1;
		$sum_all['year2'] += $sum_year2_sum1;
		$sum_all['year3'] += $sum_year3_sum1;
		
		$html_income1 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(๑) ประเภทการเกษตร</td>
				'.$td_income1_year1_1.'
				'.$td_null_income1.'
				<td align="center">xx</td>
				'.$td_income1_year2_1.'
				'.$td_null_income1.'
				<td align="center">xx</td>
				'.$td_income1_year3_1.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๒) ระยะเวลาการผลิต (เดือน/ปี)</td>
				'.$td_income1_year1_2.'
				'.$td_null_income1.'
				<td align="center">xx</td>
				'.$td_income1_year2_2.'
				'.$td_null_income1.'
				<td align="center">xx</td>
				'.$td_income1_year3_2.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๓) เนื้อที่ จำนวนการผลิต</td>
				'.$td_income1_year1_3.'
				'.$td_null_income1.'
				<td align="center">xx</td>
				'.$td_income1_year2_3.'
				'.$td_null_income1.'
				<td align="center">xx</td>
				'.$td_income1_year3_3.'
				'.$td_null_income1.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๔) รายได้ก่อนหักค่าใช้จ่าย (บาท)</td>
				'.$td_income1_year1_4.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year1_income1, 2)).'</td>
				'.$td_income1_year2_4.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year2_income1, 2)).'</td>
				'.$td_income1_year3_4.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year3_income1, 2)).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๕) ค่าใช้จ่ายการเกษตร (บาท)</td>
				'.$td_income1_year1_5.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year1_expenditure1, 2)).'</td>
				'.$td_income1_year2_5.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year2_expenditure1, 2)).'</td>
				'.$td_income1_year3_5.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year3_expenditure1, 2)).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๖) รายได้จากการเกษตรสุทธิ (๔) - (๕)</td>
				'.$td_income1_year1_6.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year1_sum1, 2)).'</td>
				'.$td_income1_year2_6.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year2_sum1, 2)).'</td>
				'.$td_income1_year3_6.'
				'.$td_null_income1.'
				<td align="right">'.num2Thai(number_format($sum_year3_sum1, 2)).'</td>
			</tr>';
	}
	
	$html_income2 = '';
	if(!empty($loan_income_2)) 
	{
		$sum_year1_income2 = 0;
		$sum_year2_income2 = 0;
		$sum_year3_income2 = 0;
		$sum_year1_expenditure2 = 0;
		$sum_year2_expenditure2 = 0;
		$sum_year3_expenditure2 = 0;
		$sum_year1_sum2 = 0;
		$sum_year2_sum2 = 0;
		$sum_year3_sum2 = 0;
		$td_income2_year1_1 = '';
		$td_income2_year2_1 = '';
		$td_income2_year3_1 = '';
		$td_income2_year1_2 = '';
		$td_income2_year2_2 = '';
		$td_income2_year3_2 = '';
		$td_income2_year1_3 = '';
		$td_income2_year2_3 = '';
		$td_income2_year3_3 = '';
		$td_income2_year1_4 = '';
		$td_income2_year2_4 = '';
		$td_income2_year3_4 = '';
		$td_income2_year1_5 = '';
		$td_income2_year2_5 = '';
		$td_income2_year3_5 = '';
		$td_income2_year1_6 = '';
		$td_income2_year2_6 = '';
		$td_income2_year3_6 = '';
			
		$i_income_2 = 0;
			
		foreach ($loan_income_2 as $list_income_2)
		{
			$year1_income = floatval($list_income_2['loan_activity_income1']);
			$year2_income = floatval($list_income_2['loan_activity_income2']);
			$year3_income = floatval($list_income_2['loan_activity_income3']);
				
			$sum_year1_income2 += $year1_income;
			$sum_year2_income2 += $year2_income;
			$sum_year3_income2 += $year3_income;
				
			$year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_2['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_2['loan_activity_expenditure3']);
				
			$sum_year1_expenditure2 += $year1_expenditure;
			$sum_year2_expenditure2 += $year2_expenditure;
			$sum_year3_expenditure2 += $year3_expenditure;
				
			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;
				
			$sum_year1_sum2 += $year1_sum;
			$sum_year2_sum2 += $year2_sum;
			$sum_year3_sum2 += $year3_sum;
	
			$rai2 = intval($list_income_2['loan_activity_self_rai']) + intval($list_income_2['loan_activity_renting_rai']) + intval($list_income_2['loan_activity_approve_rai']);
			$ngan2 = intval($list_income_2['loan_activity_self_ngan']) + intval($list_income_2['loan_activity_renting_ngan']) + intval($list_income_2['loan_activity_approve_ngan']);
			$wah2 = floatval($list_income_2['loan_activity_self_wah']) + floatval($list_income_2['loan_activity_renting_wah']) + floatval($list_income_2['loan_activity_approve_wah']);
	
			$area2 = area($rai2, $ngan2, $wah2);
	
			$td_income2_year1_1 .= '<td align="center">'.num2Thai($list_income_2['loan_activity_name']).'</td>';
			$td_income2_year2_1 .= '<td align="center">'.num2Thai($list_income_2['loan_activity_name']).'</td>';
			$td_income2_year3_1 .= '<td align="center">'.num2Thai($list_income_2['loan_activity_name']).'</td>';
			$td_income2_year1_2 .= '<td align="center">๑ ปี</td>';
			$td_income2_year2_2 .= '<td align="center">๑ ปี</td>';
			$td_income2_year3_2 .= '<td align="center">๑ ปี</td>';
			$td_income2_year1_3 .= '<td align="center">'.num2Thai($area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah']).' ไร่</td>';
			$td_income2_year2_3 .= '<td align="center">'.num2Thai($area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah']).' ไร่</td>';
			$td_income2_year3_3 .= '<td align="center">'.num2Thai($area2['rai'].' - '.$area2['ngan'].' - '.$area2['wah']).' ไร่</td>';
			$td_income2_year1_4 .= '<td align="right">'.num2Thai(number_format($year1_income, 2)).'</td>';
			$td_income2_year2_4 .= '<td align="right">'.num2Thai(number_format($year2_income, 2)).'</td>';
			$td_income2_year3_4 .= '<td align="right">'.num2Thai(number_format($year3_income, 2)).'</td>';
			$td_income2_year1_5 .= '<td align="right">'.num2Thai(number_format($year1_expenditure, 2)).'</td>';
			$td_income2_year2_5 .= '<td align="right">'.num2Thai(number_format($year2_expenditure, 2)).'</td>';
			$td_income2_year3_5 .= '<td align="right">'.num2Thai(number_format($year3_expenditure, 2)).'</td>';
			$td_income2_year1_6 .= '<td align="right">'.num2Thai(number_format($year1_sum, 2)).'</td>';
			$td_income2_year2_6 .= '<td align="right">'.num2Thai(number_format($year2_sum, 2)).'</td>';
			$td_income2_year3_6 .= '<td align="right">'.num2Thai(number_format($year3_sum, 2)).'</td>';
	
			$cal_all[$i_income_2]['year1'] = empty($cal_all[$i_income_2]['year1'])? $year1_sum : $cal_all[$i_income_2]['year1'] + $year1_sum;
			$cal_all[$i_income_2]['year2'] = empty($cal_all[$i_income_2]['year2'])? $year2_sum : $cal_all[$i_income_2]['year2'] + $year2_sum;
			$cal_all[$i_income_2]['year3'] = empty($cal_all[$i_income_2]['year3'])? $year3_sum : $cal_all[$i_income_2]['year3'] + $year3_sum;
	
			$i_income_2++;
		}
		
		$td_null_income2 = '';
		if($count_income_2 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_2;
			$td_null_income2 = str_repeat('<td align="center">xx</td>', $count_null);
				
			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				if(empty($cal_all[$i_income_2]['year1'])) $cal_all[$i_income_2]['year1'] = 0;
				if(empty($cal_all[$i_income_2]['year2'])) $cal_all[$i_income_2]['year2'] = 0;
				if(empty($cal_all[$i_income_2]['year3'])) $cal_all[$i_income_2]['year3'] = 0;
					
				$i_income_2++;
			}
		}
		
		$sum_all['year1'] += $sum_year1_sum2;
		$sum_all['year2'] += $sum_year2_sum2;
		$sum_all['year3'] += $sum_year3_sum2;
		
		$html_income2 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(๑) ประเภทการผลิต</td>
				'.$td_income2_year1_1.'
				'.$td_null_income2.'
				<td align="center">xx</td>
				'.$td_income2_year2_1.'
				'.$td_null_income2.'
				<td align="center">xx</td>
				'.$td_income2_year3_1.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๒) ระยะเวลาการผลิต หรือบริการ (เดือน/ปี) </td>
				'.$td_income2_year1_2.'
				'.$td_null_income2.'
				<td align="center">xx</td>
				'.$td_income2_year2_2.'
				'.$td_null_income2.'
				<td align="center">xx</td>
				'.$td_income2_year3_2.'
				'.$td_null_income2.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๓) รายได้นอกการเกษตร (บาท) </td>
				'.$td_income2_year1_4.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year1_income2, 2)).'</td>
				'.$td_income2_year2_4.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year2_income2, 2)).'</td>
				'.$td_income2_year3_4.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year3_income2, 2)).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๔) ค่าใช้จ่ายนอกการเกษตร (บาท)</td>
				'.$td_income2_year1_5.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year1_expenditure2, 2)).'</td>
				'.$td_income2_year2_5.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year2_expenditure2, 2)).'</td>
				'.$td_income2_year3_5.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year3_expenditure2, 2)).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๕) รายได้นอกการเกษตรสุทธิ (๓) – (๔) </td>
				'.$td_income2_year1_6.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year1_sum2, 2)).'</td>
				'.$td_income2_year2_6.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year2_sum2, 2)).'</td>
				'.$td_income2_year3_6.'
				'.$td_null_income2.'
				<td align="right">'.num2Thai(number_format($sum_year3_sum2, 2)).'</td>
			</tr>';
	}
	
	$html_income3 = '';
	if(!empty($loan_income_3)) 
	{
		$sum_year1_income3 = 0;
		$sum_year2_income3 = 0;
		$sum_year3_income3 = 0;
		$sum_year1_expenditure3 = 0;
		$sum_year2_expenditure3 = 0;
		$sum_year3_expenditure3 = 0;
		$sum_year1_sum3 = 0;
		$sum_year2_sum3 = 0;
		$sum_year3_sum3 = 0;
		$td_income3_year1_1 = '';
		$td_income3_year2_1 = '';
		$td_income3_year3_1 = '';
		$td_income3_year1_2 = '';
		$td_income3_year2_2 = '';
		$td_income3_year3_2 = '';
		$td_income3_year1_3 = '';
		$td_income3_year2_3 = '';
		$td_income3_year3_3 = '';
		$td_income3_year1_4 = '';
		$td_income3_year2_4 = '';
		$td_income3_year3_4 = '';
		$td_income3_year1_5 = '';
		$td_income3_year2_5 = '';
		$td_income3_year3_5 = '';
		$td_income3_year1_6 = '';
		$td_income3_year2_6 = '';
		$td_income3_year3_6 = '';
			
		$i_income_3 = 0;
			
		foreach ($loan_income_3 as $list_income_3)
		{
			$year1_income = floatval($list_income_3['loan_activity_income1']);
			$year2_income = floatval($list_income_3['loan_activity_income2']);
			$year3_income = floatval($list_income_3['loan_activity_income3']);
				
			$sum_year1_income3 += $year1_income;
			$sum_year2_income3 += $year2_income;
			$sum_year3_income3 += $year3_income;
				
			$year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
			$year2_expenditure = floatval($list_income_3['loan_activity_expenditure2']);
			$year3_expenditure = floatval($list_income_3['loan_activity_expenditure3']);
	
			$sum_year1_expenditure3 += $year1_expenditure;
			$sum_year2_expenditure3 += $year2_expenditure;
			$sum_year3_expenditure3 += $year3_expenditure;
				
			$year1_sum = $year1_income - $year1_expenditure;
			$year2_sum = $year2_income - $year2_expenditure;
			$year3_sum = $year3_income - $year3_expenditure;
	
			$sum_year1_sum3 += $year1_sum;
			$sum_year2_sum3 += $year2_sum;
			$sum_year3_sum3 += $year3_sum;
	
			$rai3 = intval($list_income_3['loan_activity_self_rai']) + intval($list_income_3['loan_activity_renting_rai']) + intval($list_income_3['loan_activity_approve_rai']);
			$ngan3 = intval($list_income_3['loan_activity_self_ngan']) + intval($list_income_3['loan_activity_renting_ngan']) + intval($list_income_3['loan_activity_approve_ngan']);
			$wah3 = floatval($list_income_3['loan_activity_self_wah']) + floatval($list_income_3['loan_activity_renting_wah']) + floatval($list_income_3['loan_activity_approve_wah']);
	
			$area3 = area($rai3, $ngan3, $wah3);
	
			$td_income3_year1_1 .= '<td align="center">'.num2Thai($list_income_3['loan_activity_name']).'</td>';
			$td_income3_year2_1 .= '<td align="center">'.num2Thai($list_income_3['loan_activity_name']).'</td>';
			$td_income3_year3_1 .= '<td align="center">'.num2Thai($list_income_3['loan_activity_name']).'</td>';
			$td_income3_year1_2 .= '<td align="center">๑ ปี</td>';
			$td_income3_year2_2 .= '<td align="center">๑ ปี</td>';
			$td_income3_year3_2 .= '<td align="center">๑ ปี</td>';
			$td_income3_year1_3 .= '<td align="center">'.num2Thai($area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah']).' ไร่</td>';
			$td_income3_year2_3 .= '<td align="center">'.num2Thai($area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah']).' ไร่</td>';
			$td_income3_year3_3 .= '<td align="center">'.num2Thai($area3['rai'].' - '.$area3['ngan'].' - '.$area3['wah']).' ไร่</td>';
			$td_income3_year1_4 .= '<td align="right">'.num2Thai(number_format($year1_income, 2)).'</td>';
			$td_income3_year2_4 .= '<td align="right">'.num2Thai(number_format($year2_income, 2)).'</td>';
			$td_income3_year3_4 .= '<td align="right">'.num2Thai(number_format($year3_income, 2)).'</td>';
			$td_income3_year1_5 .= '<td align="right">'.num2Thai(number_format($year1_expenditure, 2)).'</td>';
			$td_income3_year2_5 .= '<td align="right">'.num2Thai(number_format($year2_expenditure, 2)).'</td>';
			$td_income3_year3_5 .= '<td align="right">'.num2Thai(number_format($year3_expenditure, 2)).'</td>';
			$td_income3_year1_6 .= '<td align="right">'.num2Thai(number_format($year1_sum, 2)).'</td>';
			$td_income3_year2_6 .= '<td align="right">'.num2Thai(number_format($year2_sum, 2)).'</td>';
			$td_income3_year3_6 .= '<td align="right">'.num2Thai(number_format($year3_sum, 2)).'</td>';
	
			$cal_all[$i_income_3]['year1'] = empty($cal_all[$i_income_3]['year1'])? $year1_sum : $cal_all[$i_income_3]['year1'] + $year1_sum;
			$cal_all[$i_income_3]['year2'] = empty($cal_all[$i_income_3]['year2'])? $year2_sum : $cal_all[$i_income_3]['year2'] + $year2_sum;
			$cal_all[$i_income_3]['year3'] = empty($cal_all[$i_income_3]['year3'])? $year3_sum : $cal_all[$i_income_3]['year3'] + $year3_sum;
	
			$i_income_3++;
		}
		
		$td_null_income3 = '';
		if($count_income_3 < $td_len - 1)
		{
			$count_null = $td_len - 1 - $count_income_3;
			$td_null_income3 = str_repeat('<td align="center">xx</td>', $count_null);
			 
			for ($i_null = 1; $i_null<=$count_null; $i_null++)
			{
				if(empty($cal_all[$i_income_3]['year1'])) $cal_all[$i_income_3]['year1'] = 0;
				if(empty($cal_all[$i_income_3]['year2'])) $cal_all[$i_income_3]['year2'] = 0;
				if(empty($cal_all[$i_income_3]['year3'])) $cal_all[$i_income_3]['year3'] = 0;
			
				$i_income_3++;
			}
		}
		
		$sum_all['year1'] += $sum_year1_sum3;
		$sum_all['year2'] += $sum_year2_sum3;
		$sum_all['year3'] += $sum_year3_sum3;
		
		$html_income3 = '<tr>
				<td>&nbsp;&nbsp;&nbsp;(๑) ประเภทรายได้อื่นๆ</td>
				'.$td_income3_year1_1.'
				'.$td_null_income3.'
				<td align="center">xx</td>
				'.$td_income3_year2_1.'
				'.$td_null_income3.'
				<td align="center">xx</td>
				'.$td_income3_year3_1.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๒) ระยะเวลา (เดือน/ปี)</td>
				'.$td_income3_year1_2.'
				'.$td_null_income3.'
				<td align="center">xx</td>
				'.$td_income3_year2_2.'
				'.$td_null_income3.'
				<td align="center">xx</td>
				'.$td_income3_year3_2.'
				'.$td_null_income3.'
				<td align="center">xx</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๓) รายได้อื่นๆ ก่อนหักค่าใช้จ่าย (บาท) </td>
				'.$td_income3_year1_4.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year1_income3, 2)).'</td>
				'.$td_income3_year2_4.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year2_income3, 2)).'</td>
				'.$td_income3_year3_4.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year3_income3, 2)).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๔) ค่าใช้จ่ายอื่นๆ (บาท)</td>
				'.$td_income3_year1_5.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year1_expenditure3, 2)).'</td>
				'.$td_income3_year2_5.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year2_expenditure3, 2)).'</td>
				'.$td_income3_year3_5.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year3_expenditure3, 2)).'</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;&nbsp;(๕) รายได้อื่นๆ สุทธิ (๓) - (๔) </td>
				'.$td_income3_year1_6.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year1_sum3, 2)).'</td>
				'.$td_income3_year2_6.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year2_sum3, 2)).'</td>
				'.$td_income3_year3_6.'
				'.$td_null_income3.'
				<td align="right">'.num2Thai(number_format($sum_year3_sum3, 2)).'</td>
			</tr>';
	}
	
	$sum_year1_expenditure_1 = 0;
	$sum_year2_expenditure_1 = 0;
	$sum_year3_expenditure_1 = 0;
	$td_year1_expenditure_1 = '';
	$td_year2_expenditure_1 = '';
	$td_year3_expenditure_1 = '';
	 
	$sum_expenditure = array();
	$i_expenditure = 0;
	
	if(!empty($loan_expenditure_1))
	{
		foreach ($loan_expenditure_1 as $list_expenditure_1)
		{
			$year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
			$year2_amount = floatval($list_expenditure_1['loan_expenditure_amount2']);
			$year3_amount = floatval($list_expenditure_1['loan_expenditure_amount3']);
			 
			$sum_year1_expenditure_1 += $year1_amount;
			$sum_year2_expenditure_1 += $year2_amount;
			$sum_year3_expenditure_1 += $year3_amount;
			 
			$td_year1_expenditure_1 .= '<td align="right">'.num2Thai(number_format($year1_amount, 2)).'</td>';
			$td_year2_expenditure_1 .= '<td align="right">'.num2Thai(number_format($year2_amount, 2)).'</td>';
			$td_year3_expenditure_1 .= '<td align="right">'.num2Thai(number_format($year3_amount, 2)).'</td>';
	
			$sum_expenditure[$i_expenditure]= array(
					'year1' => $year1_amount,
					'year2' => $year2_amount,
					'year3' => $year3_amount
			);
			 
			$cal_all[$i_expenditure]['year1'] = empty($cal_all[$i_expenditure]['year1'])? 0 - $year1_amount : $cal_all[$i_expenditure]['year1'] - $year1_amount;
			$cal_all[$i_expenditure]['year2'] = empty($cal_all[$i_expenditure]['year2'])? 0 - $year2_amount : $cal_all[$i_expenditure]['year2'] - $year2_amount;
			$cal_all[$i_expenditure]['year3'] = empty($cal_all[$i_expenditure]['year3'])? 0 - $year3_amount : $cal_all[$i_expenditure]['year3'] - $year3_amount;
	
			$i_expenditure++;
		}
	}
	
	$td_null_expenditure1 = '';
	if($count_expenditure_1 < $td_len - 1)
	{
		$count_null = $td_len - 1 - $count_expenditure_1;
		$td_null_expenditure1 = str_repeat('<td align="center">xx</td>', $count_null);
	
		for ($i_null = 1; $i_null<=$count_null; $i_null++)
		{
			$sum_expenditure[$i_expenditure]= array(
					'year1' => 0,
					'year2' => 0,
					'year3' => 0
			);
					
			if(empty($cal_all[$i_expenditure]['year1'])) $cal_all[$i_expenditure]['year1'] = 0;
			if(empty($cal_all[$i_expenditure]['year2'])) $cal_all[$i_expenditure]['year2'] = 0;
			if(empty($cal_all[$i_expenditure]['year3'])) $cal_all[$i_expenditure]['year3'] = 0;
					
			$i_expenditure++;
		}
	}
	
	$sum_year1_expenditure_2 = 0;
	$sum_year2_expenditure_2 = 0;
	$sum_year3_expenditure_2 = 0;
	$td_year1_expenditure_2 = '';
	$td_year2_expenditure_2 = '';
	$td_year3_expenditure_2 = '';
	
	$i_expenditure = 0;
	
	if(!empty($loan_expenditure_2))
	{
		foreach ($loan_expenditure_2 as $list_expenditure_2)
		{
			$year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
			$year2_amount = floatval($list_expenditure_2['loan_expenditure_amount2']);
			$year3_amount = floatval($list_expenditure_2['loan_expenditure_amount3']);
	
			$sum_year1_expenditure_2 += $year1_amount;
			$sum_year2_expenditure_2 += $year2_amount;
			$sum_year3_expenditure_2 += $year3_amount;
	
			$td_year1_expenditure_2 .= '<td align="right">'.num2Thai(number_format($year1_amount, 2)).'</td>';
			$td_year2_expenditure_2 .= '<td align="right">'.num2Thai(number_format($year2_amount, 2)).'</td>';
			$td_year3_expenditure_2 .= '<td align="right">'.num2Thai(number_format($year3_amount, 2)).'</td>';
			 
			$sum_expenditure[$i_expenditure]['year1'] = empty($sum_expenditure[$i_expenditure]['year1'])? $year1_amount : $sum_expenditure[$i_expenditure]['year1'] + $year1_amount;
			$sum_expenditure[$i_expenditure]['year2'] = empty($sum_expenditure[$i_expenditure]['year2'])? $year2_amount : $sum_expenditure[$i_expenditure]['year2'] + $year2_amount;
			$sum_expenditure[$i_expenditure]['year3'] = empty($sum_expenditure[$i_expenditure]['year3'])? $year3_amount : $sum_expenditure[$i_expenditure]['year3'] + $year3_amount;
	
			$cal_all[$i_expenditure]['year1'] = empty($cal_all[$i_expenditure]['year1'])? 0 - $year1_amount : $cal_all[$i_expenditure]['year1'] - $year1_amount;
			$cal_all[$i_expenditure]['year2'] = empty($cal_all[$i_expenditure]['year2'])? 0 - $year2_amount : $cal_all[$i_expenditure]['year2'] - $year2_amount;
			$cal_all[$i_expenditure]['year3'] = empty($cal_all[$i_expenditure]['year3'])? 0 - $year3_amount : $cal_all[$i_expenditure]['year3'] - $year3_amount;
	
			$i_expenditure++;
		}
	}
	
	$td_null_expenditure2 = '';
	if($count_expenditure_2 < $td_len - 1)
	{
		$count_null = $td_len - 1 - $count_expenditure_2;
		$td_null_expenditure2 = str_repeat('<td align="center">xx</td>', $count_null);
	
		for ($i_null = 1; $i_null<=$count_null; $i_null++)
		{
			$sum_expenditure[$i_expenditure]['year1'] = empty($sum_expenditure[$i_expenditure]['year1'])? 0 : $sum_expenditure[$i_expenditure]['year1'] + 0;
			$sum_expenditure[$i_expenditure]['year2'] = empty($sum_expenditure[$i_expenditure]['year2'])? 0 : $sum_expenditure[$i_expenditure]['year2'] + 0;
			$sum_expenditure[$i_expenditure]['year3'] = empty($sum_expenditure[$i_expenditure]['year3'])? 0 : $sum_expenditure[$i_expenditure]['year3'] + 0;
		
			if(empty($cal_all[$i_expenditure]['year1'])) $cal_all[$i_expenditure]['year1'] = 0;
			if(empty($cal_all[$i_expenditure]['year2'])) $cal_all[$i_expenditure]['year2'] = 0;
			if(empty($cal_all[$i_expenditure]['year3'])) $cal_all[$i_expenditure]['year3'] = 0;
				
			$i_expenditure++;
		}
	}
	
	$sum_year1_expenditure_all = 0;
	$sum_year2_expenditure_all = 0;
	$sum_year3_expenditure_all = 0;
	$td_year1_expenditure_all = '';
	$td_year2_expenditure_all = '';
	$td_year3_expenditure_all = '';
	
	foreach ($sum_expenditure as $list_expenditure_all)
	{
		$year1_amount = $list_expenditure_all['year1'];
		$year2_amount = $list_expenditure_all['year2'];
		$year3_amount = $list_expenditure_all['year3'];
	
		$sum_year1_expenditure_all += $year1_amount;
		$sum_year2_expenditure_all += $year2_amount;
		$sum_year3_expenditure_all += $year3_amount;
	
		$td_year1_expenditure_all .= '<td align="right">'.num2Thai(number_format($year1_amount, 2)).'</td>';
		$td_year2_expenditure_all .= '<td align="right">'.num2Thai(number_format($year2_amount, 2)).'</td>';
		$td_year3_expenditure_all .= '<td align="right">'.num2Thai(number_format($year3_amount, 2)).'</td>';
	}
	
	$sum_all['year1'] -= $sum_year1_expenditure_all;
	$sum_all['year2'] -= $sum_year2_expenditure_all;
	$sum_all['year3'] -= $sum_year3_expenditure_all;
	
	$td_year1 = '';
	$td_year2 = '';
	$td_year3 = '';
	$td_year1_all = '';
	$td_year2_all = '';
	$td_year3_all = '';
	
	foreach ($cal_all as $list_cal_all)
	{
		$td_year1 .= '<td align="right">'.num2Thai(number_format($list_cal_all['year1'], 2)).'</td>';
		$td_year2 .= '<td align="right">'.num2Thai(number_format($list_cal_all['year2'], 2)).'</td>';
		$td_year3 .= '<td align="right">'.num2Thai(number_format($list_cal_all['year3'], 2)).'</td>';
	}
?>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
	<tr>
		<th style="text-align:center; vertical-align: middle;">รายการ</th>
		<th colspan="<?php echo $td_len; ?>" style="text-align:center;">ปีที่ ๑ (ปีปัจจุบัน)</th>
		<th colspan="<?php echo $td_len; ?>" style="text-align:center;">ปีที่ ๒ (ปีก่อน)</th>
		<th colspan="<?php echo $td_len; ?>" style="text-align:center;">ปีที่ ๓ (ปีหลัง)</th>
	</tr>
	<tr>
		<th>๑. รายได้และค่าใช้จ่ายการเกษตร</th>
		<?php echo $td_income_1; ?>
		<?php echo $td_income_1; ?>
		<?php echo $td_income_1; ?>
	</tr>
	<?php echo $html_income1; ?>
	<tr>
		<th>๒. รายได้และค่าใช้จ่ายนอกการเกษตร</th>
		<?php echo $td_income_2; ?>
		<?php echo $td_income_2; ?>
		<?php echo $td_income_2; ?>
	</tr>
	<?php echo $html_income2; ?>
	<tr>
		<th>๓. รายได้อื่นๆ</th>
		<?php echo $td_income_3; ?>
		<?php echo $td_income_3; ?>
		<?php echo $td_income_3; ?>
	</tr>
	<?php echo $html_income3; ?>
	<tr>
		<th>๔. ค่าใช้จ่ายอื่นๆ (บาท)</th>
		<?php echo $td_expenditure; ?>
		<?php echo $td_expenditure; ?>
		<?php echo $td_expenditure; ?>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;(๑) ค่าใช้จ่ายในครัวเรือน</td>
		<?php echo $td_year1_expenditure_1; ?>
		<?php echo $td_null_expenditure1; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year1_expenditure_1, 2)); ?></td>
		<?php echo $td_year2_expenditure_1; ?>
		<?php echo $td_null_expenditure1; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year2_expenditure_1, 2)); ?></td>
		<?php echo $td_year3_expenditure_1; ?>
		<?php echo $td_null_expenditure1; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year3_expenditure_1, 2)); ?></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;(๒) ค่าใช้จ่ายอื่นๆ</td>
		<?php echo $td_year1_expenditure_2; ?>
		<?php echo $td_null_expenditure2; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year1_expenditure_2, 2)); ?></td>
		<?php echo $td_year2_expenditure_2; ?>
		<?php echo $td_null_expenditure2; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year2_expenditure_2, 2)); ?></td>
		<?php echo $td_year3_expenditure_2; ?>
		<?php echo $td_null_expenditure2; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year3_expenditure_2, 2)); ?></td>
	</tr>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;(๓) รวมค่าใช้จ่าย (๑) + (๒)</td>
		<?php echo $td_year1_expenditure_all; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year1_expenditure_all, 2)); ?></td>
		<?php echo $td_year2_expenditure_all; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year2_expenditure_all, 2)); ?></td>
		<?php echo $td_year3_expenditure_all; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_year3_expenditure_all, 2)); ?></td>
	</tr>
	<tr>
		<td>๕. รายได้สุทธิ (บาท)</td>
		<?php echo $td_year1; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_all['year1'], 2)); ?></td>
		<?php echo $td_year2; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_all['year2'], 2)); ?></td>
		<?php echo $td_year3; ?>
		<td align="right"><?php echo num2Thai(number_format($sum_all['year3'], 2)); ?></td>
	</tr>
</table>