<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sex_model extends CI_Model
{
	private $table = 'master_sex';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("sex_status", "1");
		$this->db->order_by("sex_id", "asc");
		$query = $this->db->get();
		
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>