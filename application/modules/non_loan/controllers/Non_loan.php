<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Non_loan extends MY_Controller
{
    private $title_page = 'แบบบันทึกขอสินเชื่อ (กรณีไม่รับคำขอ)';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model('non_loan_model', 'non_loan');

        $thaiid = '';
        $fullname = '';
        if (!empty($_POST['txtThaiid'])) {
            $thaiid = $_POST['txtThaiid'];
        }
        if (!empty($_POST['txtFullname'])) {
            $fullname = $_POST['txtFullname'];
        }

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array('สินเชื่อ' =>'#');

        $data_content = array(
                'thaiid' => $thaiid,
                'fullname' => $fullname,
                'result' => $this->non_loan->search($thaiid, $fullname)
        );

        $data = array(
                'title' => $this->title_page,
                'js_other' => array('modules_non_loan/non_loan.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('non_loan', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function form($id='')
    {
        $this->load->model('title/title_model', 'title');
        $this->load->model('province/province_model', 'province');
        $this->load->model('non_loan_objective/non_loan_objective_model', 'non_loan_objective');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->model('non_loan_case/non_loan_case_model', 'non_loan_case');
        $this->load->model('non_loan_model', 'non_loan');

        $data_menu['menu'] = 'loan';
        $data_breadcrumb['menu'] = array(
                'สินเชื่อ' =>'#',
                $this->title_page => site_url('non_loan')
            );

        $data_content = array(
                'title' => $this->title->all(),
                'province' => $this->province->all(),
                'amphur' => array(),
                'district' => array(),
                'amphur_creditor' => array(),
                'district_creditor' => array(),
                'objective' => $this->non_loan_objective->all(),
                'land_type' => $this->land_type->all(),
                'case' => $this->non_loan_case->all()
            );

        if ($id=='') {
            $title_page = 'เพิ่มแบบบันทึก';

            $data_content['id'] = '';
            $data_content['data'] = $this->non_loan->getModel();
        } else {
            $this->load->model('non_loan_model', 'non_loan');
            $data_content['data'] = $this->non_loan->getById($id);

            if (empty($data_content['data'])) {
                $title_page = 'เพิ่มแบบบันทึก';

                $data_content['id'] = '';
                $data_content['data'] = $this->non_loan->getModel();
            } else {
                $this->load->model('amphur/amphur_model', 'amphur');
                $this->load->model('district/district_model', 'district');

                $title_page = 'แก้ไขแบบบันทึก';

                $data_content['id'] = $id;

                $data_content['amphur'] = $this->amphur->getByProvince($data_content['data']['person']['person_addr_card_province_id']);
                $data_content['district'] = $this->district->getByAmphur($data_content['data']['person']['person_addr_card_amphur_id']);

                if (!empty($data_content['data']['creditor']['person_addr_card_province_id'])) {
                    $data_content['amphur_creditor'] = $this->amphur->getByProvince($data_content['data']['creditor']['person_addr_card_province_id']);
                }

                if (!empty($data_content['data']['creditor']['person_addr_card_amphur_id'])) {
                    $data_content['district_creditor'] = $this->district->getByAmphur($data_content['data']['creditor']['person_addr_card_amphur_id']);
                }
            }
        }

        $data = array(
                'title' => $title_page,
                'js_other' => array('modules_non_loan/form.js'),
                'menu' => $this->parser->parse('page/menu', $data_menu, true),
                'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, true),
                'content' => $this->parser->parse('form', $data_content, true)
        );

        $this->parser->parse('main', $data);
    }

    public function person_search()
    {
        $this->load->model('person/person_model', 'person');

        $q = $this->input->get('q');
        $data = $this->person->search($q);

        echo json_encode($data);
    }

    public function amphur($province='')
    {
        if ($province=='') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');

            $data['amphur'] = $this->amphur->getByProvince($province);
        }

        $this->parser->parse('amphur', $data);
    }

    public function district($amphur='')
    {
        if ($amphur=='') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');

            $data['district'] = $this->district->getByAmphur($amphur);
        }

        $this->parser->parse('district', $data);
    }

    public function amphur_creditor($province='')
    {
        if ($province=='') {
            $data['amphur'] = array();
        } else {
            $this->load->model('amphur/amphur_model', 'amphur');

            $data['amphur'] = $this->amphur->getByProvince($province);
        }

        $this->parser->parse('amphur_creditor', $data);
    }

    public function district_creditor($amphur='')
    {
        if ($amphur=='') {
            $data['district'] = array();
        } else {
            $this->load->model('district/district_model', 'district');

            $data['district'] = $this->district->getByAmphur($amphur);
        }

        $this->parser->parse('district_creditor', $data);
    }

    public function save()
    {
        $this->load->model('non_loan_model', 'non_loan');

        $array['non_loan_date'] = convert_ymd_en($this->input->post('txtDate'));
        $array['non_loan_location'] = $this->input->post('txtLocation');
        $array['person']['person_id'] = $this->input->post('txtPersonId');
        $array['person']['title_id'] = $this->input->post('ddlTitle');
        $array['person']['person_fname'] = $this->input->post('txtFname');
        $array['person']['person_lname'] = $this->input->post('txtLname');
        $array['person']['person_thaiid'] = str_replace('-', '', $this->input->post('txtThaiid'));
        $array['person']['person_addr_card_no'] = $this->input->post('txtAddrCardNo');
        $array['person']['person_addr_card_moo'] = $this->input->post('txtAddrCardMoo');
        $array['person']['person_addr_card_road'] = $this->input->post('txtAddrCardRoad');
        $array['person']['person_addr_card_district_id'] = $this->input->post('ddlDistrict');
        $array['person']['person_addr_card_amphur_id'] = $this->input->post('ddlAmphur');
        $array['person']['person_addr_card_province_id'] = $this->input->post('ddlProvince');
        $array['person']['person_phone'] = $this->input->post('txtPhone');
        $array['creditor']['person_id'] = $this->input->post('txtCreditorId');
        $array['creditor']['title_id'] = $this->input->post('ddlCreditorTitle');
        $array['creditor']['person_fname'] = $this->input->post('txtCreditorFname');
        $array['creditor']['person_lname'] = $this->input->post('txtCreditorLname');
        $array['creditor']['person_thaiid'] = str_replace('-', '', $this->input->post('txtCreditorThaiid'));
        $array['creditor']['person_addr_card_no'] = $this->input->post('txtCreditorAddrCardNo');
        $array['creditor']['person_addr_card_moo'] = $this->input->post('txtCreditorAddrCardMoo');
        $array['creditor']['person_addr_card_road'] = $this->input->post('txtCreditorAddrCardRoad');
        $array['creditor']['person_addr_card_district_id'] = $this->input->post('ddlCreditorDistrict');
        $array['creditor']['person_addr_card_amphur_id'] = $this->input->post('ddlCreditorAmphur');
        $array['creditor']['person_addr_card_province_id'] = $this->input->post('ddlCreditorProvince');
        $array['creditor']['person_phone'] = $this->input->post('txtCreditorAddrCardRoad');
        $array['non_loan_objective_id'] = $this->input->post('ddlObjective');
        $array['land_type_id'] = $this->input->post('ddlLandType');
        $array['non_loan_area_rai'] = $this->input->post('txtRai');
        $array['non_loan_area_ngan'] = $this->input->post('txtNgan');
        $array['non_loan_area_wah'] = $this->input->post('txtWah');
        $array['non_loan_case_id'] = $this->input->post('ddlCase');
        $array['non_loan_cause_other'] = $this->input->post('txtCaseOther');
        $array['non_loan_status'] = $this->input->post('ddlStatus');

        $id = $this->non_loan->save($array, $this->input->post('txtId'));

        if ($this->input->post('txtId')=='') {
            $data = array(
                    'alert' => 'บันทึกแบบบันทึกเรียบร้อยแล้ว',
                    'link' => site_url('non_loan'),
            );
        } else {
            $data = array(
                    'alert' => 'แก้ไขแบบบันทึกเรียบร้อยแล้ว',
                    'link' => site_url('non_loan'),
            );
        }

        $this->parser->parse('redirect', $data);
    }

    public function del($id)
    {
        $this->load->model('non_loan_model', 'non_loan');
        $this->non_loan->del($id);

        $data = array(
                'alert' => 'ยกเลิกแบบบันทึกเรียบร้อยแล้ว',
                'link' => site_url('non_loan'),
        );

        $this->parser->parse('redirect', $data);
    }

    public function word($id)
    {
        include_once('word/class/tbs_class.php');
        include_once('word/class/tbs_plugin_opentbs.php');

        $this->load->model('analysis/loan_analysis_model', 'loan_analysis');
        $this->load->model('analysis/loan_model', 'loan');
        $this->load->model('person/person_model', 'person');
        $this->load->model('title/title_model', 'title');
        $this->load->model('district/district_model', 'district');
        $this->load->model('amphur/amphur_model', 'amphur');
        $this->load->model('province/province_model', 'province');
        $this->load->model('career/career_model', 'career');
        $this->load->model('analysis/loan_type_model', 'loan_type');
        $this->load->model('analysis/loan_co_model', 'loan_co');
        $this->load->model('summary/land_record_model', 'land_record');
        $this->load->model('summary/building_record_model', 'building_record');
        $this->load->model('analysis/loan_guarantee_bond_model', 'loan_guarantee_bond');
        $this->load->model('analysis/loan_guarantee_bookbank_model', 'loan_guarantee_bookbank');
        $this->load->model('analysis/loan_guarantee_bondsman_model', 'loan_guarantee_bondsman');
        $this->load->model('analysis/loan_income_model', 'loan_income');
        $this->load->model('analysis/loan_expenditure_model', 'loan_expenditure');
        $this->load->model('analysis/loan_schedule_model', 'loan_schedule');
        $this->load->model('analysis/loan_analysis_model', 'loan_analysis');

        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        $GLOBALS['company'] = '';
        $GLOBALS['location'] = '';
        $GLOBALS['date'] = '';

        $analysis = $this->loan_analysis->getById($id);
        $data = $this->loan->getById($analysis['loan_id']);

        $title = $this->title->getById($data['person']['title_id']);
        $district = $this->district->getById($data['person']['person_addr_pre_district_id']);
        $amphur = $this->amphur->getById($data['person']['person_addr_pre_amphur_id']);
        $province = $this->province->getById($data['person']['person_addr_pre_province_id']);
        $career = $this->career->getById($data['person']['career_id']);

        $type_name = '';
        if ($analysis['loan_analysis_per_year']=='1') {
            $type_name = 'รายปี';
        } elseif ($analysis['loan_analysis_per_year']=='6') {
            $type_name = 'ราย ๖ เดือน';
        } elseif ($analysis['loan_analysis_per_year']=='12') {
            $type_name = 'รายเดือน';
        }

        $GLOBALS['person_fullname'] = $title['title_name'].$data['person']['person_fname'].' '.$data['person']['person_lname'];
        $GLOBALS['person_age'] = num2Thai(empty($data['person']['person_birthdate'])? ' - ' : age($data['person']['person_birthdate']));
        $GLOBALS['person_thaiid'] = num2Thai(formatThaiid($data['person']['person_thaiid']));
        $GLOBALS['person_home_no'] = num2Thai($data['person']['person_addr_pre_no']);
        $GLOBALS['person_home_moo'] = empty($data['person']['person_addr_pre_moo'])? ' - ' : num2Thai($data['person']['person_addr_pre_moo']);
        $GLOBALS['person_home_road'] = empty($data['person']['person_addr_pre_road'])? ' - ' : num2Thai($data['person']['person_addr_pre_road']);
        $GLOBALS['person_home_district'] = $district['district_name'];
        $GLOBALS['person_home_amphur'] = $amphur['amphur_name'];
        $GLOBALS['person_home_province'] = $province['province_name'];
        $GLOBALS['person_career'] = $career['career_name'];
        $GLOBALS['person_mobile'] = empty($data['person']['person_mobile'])? ' - ' : num2Thai($data['person']['person_mobile']);
        $GLOBALS['amount'] = num2Thai(number_format($data['loan_amount'], 2));
        $GLOBALS['amount_text'] = num2wordsThai(number_format($data['loan_amount'], 2));
        $GLOBALS['type_name'] = $type_name;

        $loan_type = $this->loan_type->getByLoan($analysis['loan_id']);

        $loan_type_objective = '';
        $html_type = '';
        $loan_creditor = ' - ';
        $loan_creditor_age = ' - ';
        $loan_creditor_thaiid = ' - ';
        $loan_creditor_no = ' - ';
        $loan_creditor_moo = ' - ';
        $loan_creditor_road = ' - ';
        $loan_creditor_district = ' - ';
        $loan_creditor_amphur = ' - ';
        $loan_creditor_province = ' - ';
        $loan_creditor_career = ' - ';
        $loan_creditor_mobile = ' - ';
        if (!empty($loan_type)) {
            foreach ($loan_type as $list_type) {
                if ($list_type['loan_objective_id']=='1') {
                    $loan_type_objective = 'ไถ่ถอนที่ดินจากการจำนองหรือขายฝาก';
                    $html_type .= 'ไถ่ถอนที่ดินจำนอง หรือขายฝาก จากผู้รับจำนองหรือผู้รับซื้อฝากซึ่งอยู่ในอายุสัญญา ';

                    if (!empty($list_type['loan_type_redeem_owner'])) {
                        $creditor = $this->person->getById($list_type['loan_type_redeem_owner']);
                        $creditor_title = $this->title->getById($creditor['title_id']);
                        $creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
                        $creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
                        $creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
                        $creditor_career = $this->career->getById($creditor['career_id']);

                        $loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
                        $loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
                        $loan_creditor_thaiid = $creditor['person_thaiid'];
                        $loan_creditor_no = $creditor['person_addr_pre_no'];
                        $loan_creditor_moo = $creditor['person_addr_pre_moo'];
                        $loan_creditor_road = $creditor['person_addr_pre_road'];
                        $loan_creditor_district = $creditor_district['district_name'];
                        $loan_creditor_amphur = $creditor_amphur['amphur_name'];
                        $loan_creditor_province = $creditor_province['province_name'];
                        $loan_creditor_career = $creditor_career['career_name'];
                        $loan_creditor_mobile = $creditor['person_mobile'];
                    }
                } elseif ($list_type['loan_objective_id']=='2') {
                    $loan_type_objective = 'ชำระหนี้';
                    $html_type .= 'ชำระหนี้ตามสัญญากู้ยืมเงิน ซึ่งได้นำโฉนดที่ดิน';

                    if (!empty($list_type['loan_type_analysis_creditor'])) {
                        $creditor = $this->person->getById($list_type['loan_type_analysis_creditor']);
                        $creditor_title = $this->title->getById($creditor['title_id']);
                        $creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
                        $creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
                        $creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
                        $creditor_career = $this->career->getById($creditor['career_id']);

                        $loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
                        $loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
                        $loan_creditor_thaiid = $creditor['person_thaiid'];
                        $loan_creditor_district = $creditor['person_addr_pre_no'];
                        $loan_creditor_moo = $creditor['person_addr_pre_moo'];
                        $loan_creditor_road = $creditor['person_addr_pre_road'];
                        $loan_creditor_district = $creditor_district['district_name'];
                        $loan_creditor_amphur = $creditor_amphur['amphur_name'];
                        $loan_creditor_province = $creditor_province['province_name'];
                        $loan_creditor_career = $creditor_career['career_name'];
                        $loan_creditor_mobile = $creditor['person_mobile'];
                    }
                } elseif ($list_type['loan_objective_id']=='3') {
                    $loan_type_objective = 'ชำระหนี้';
                    $html_type .= 'เอกสารสิทธิในที่ดินให้เจ้าหนี้ยึดถือไว้หรือชำระหนี้ตามคำพิพากษาอันเกี่ยวกับที่ดิน';

                    if (!empty($list_type['loan_type_case_plaintiff_id'])) {
                        $creditor = $this->person->getById($list_type['loan_type_case_plaintiff_id']);
                        $creditor_title = $this->title->getById($creditor['title_id']);
                        $creditor_district = $this->district->getById($creditor['person_addr_pre_district_id']);
                        $creditor_amphur = $this->amphur->getById($creditor['person_addr_pre_amphur_id']);
                        $creditor_province = $this->province->getById($creditor['person_addr_pre_province_id']);
                        $creditor_career = $this->career->getById($creditor['career_id']);

                        $loan_creditor = $creditor_title['title_name'].$creditor['person_fname'].'  '.$creditor['person_lname'];
                        $loan_creditor_age = empty($creditor['person_birthdate'])? '' : age($creditor['person_birthdate']);
                        $loan_creditor_thaiid = $creditor['person_thaiid'];
                        $loan_creditor_no = $creditor['person_addr_pre_no'];
                        $loan_creditor_moo = $creditor['person_addr_pre_moo'];
                        $loan_creditor_road = $creditor['person_addr_pre_road'];
                        $loan_creditor_district = $creditor_district['district_name'];
                        $loan_creditor_amphur = $creditor_amphur['amphur_name'];
                        $loan_creditor_province = $creditor_province['province_name'];
                        $loan_creditor_career = $creditor_career['career_name'];
                        $loan_creditor_mobile = $creditor['person_mobile'];
                    }
                } elseif ($list_type['loan_objective_id']=='4') {
                    $loan_type_objective = 'ซื้อคืนที่ดิน';
                    $html_type .= 'ซื้อที่ดินที่ถูกขายทอดตลาด หรือหลุดขายฝากไปแล้วไม่เกิน ๕ ปี โดยเจ้าของที่ดินเดิมที่ถูกบังคับจำนองด้วยการขายทอดตลาด';
                } elseif ($list_type['loan_objective_id']=='5') {
                    $loan_type_objective = 'ซื้อคืนที่ดิน';
                    $html_type .= 'ผู้ขายฝากหรือทายาทที่ประสงค์จะนำที่ดินไปประกอบเกษตรกรรม หรือเพื่อการประกอบอาชีพเกษตรกรรม';
                }
            }
        }

        $GLOBALS['type_object'] = $loan_type_objective;
        $GLOBALS['type'] = $html_type;
        $GLOBALS['creditor_fullname'] = $loan_creditor;
        $GLOBALS['creditor_age'] = num2Thai($loan_creditor_age);
        $GLOBALS['creditor_thaiid'] = num2Thai($loan_creditor_thaiid);
        $GLOBALS['creditor_home_no'] = num2Thai($loan_creditor_no);
        $GLOBALS['creditor_home_moo'] = num2Thai($loan_creditor_moo);
        $GLOBALS['creditor_home_road'] = num2Thai($loan_creditor_road);
        $GLOBALS['creditor_home_district'] = $loan_creditor_district;
        $GLOBALS['creditor_home_amphur'] = $loan_creditor_amphur;
        $GLOBALS['creditor_home_province'] = $loan_creditor_province;
        $GLOBALS['creditor_career'] = $loan_creditor_career;
        $GLOBALS['creditor_mobile'] = num2Thai($loan_creditor_mobile);

        $loan_co =  $this->loan_co->getByLoan($analysis['loan_id']);

        $data_co = array();
        if (!empty($loan_co)) {
            $relation = '';
            foreach ($loan_co as $list_co) {
                $co_name = $list_co['title_name'].$list_co['person_fname'].' '.$list_co['person_lname'];
                $co_age = empty($list_co['person_birthdate'])? ' - ' : age($list_co['person_birthdate']);

                $data_co[] = array(
                            'co_fullname' => $co_name,
                            'co_age' =>  $co_age,
                            'co_thaiid' => num2Thai(formatThaiid($list_co['person_thaiid'])),
                            'co_home_no' => num2Thai($list_co['person_addr_pre_no']),
                            'co_home_moo' => (empty($list_co['person_addr_pre_moo'])? ' - ' : num2Thai($list_co['person_addr_pre_moo'])),
                            'co_home_road' => (empty($list_co['person_addr_pre_road'])? ' - ' : num2Thai($list_co['person_addr_pre_road'])),
                            'co_home_district' => $list_co['district_name'],
                            'co_home_amphur' => $list_co['amphur_name'],
                            'co_home_province' => $list_co['province_name'],
                            'co_career' => $list_co['career_name'],
                            'co_mobile' => (empty($list_co['person_mobile'])? ' - ' : num2Thai($list_co['person_mobile']))
                        );

                $relation .= $list_co['relationship_name'].', ';
            }

            $relation =  substr($relation, 0, -2);
        } else {
            $relation = ' - ';
        }

        $GLOBALS['relation'] = $relation;
        $TBS->MergeBlock('co', $data_co);

        $land_record =  $this->land_record->getByLoan($analysis['loan_id']);

        $data_land = array();
        if (!empty($land_record)) {
            foreach ($land_record as $key_land => $land) {
                $building_record = $this->building_record->getByLand($land['land_id']);

                $data_building = array();
                if (!empty($building_record)) {
                    foreach ($building_record as $key_building => $building) {
                        $data_building[] = array(
                                    'building_type_name' => $building['building_type_name'],
                                    'building_no' => num2Thai($building['building_no']),
                                    'building_moo' => (empty($building['building_moo'])? ' - ' : num2Thai($building['building_moo'])),
                                    'building_road' => (empty($building['building_road'])? ' - ' : num2Thai($building['building_road'])),
                                    'building_district'  => $building['district_name'],
                                    'building_amphur'  => $building['amphur_name'],
                                    'building_provine'  => $building['province_name'],
                                );
                    }
                }

                $data_land[] = array(
                        'land_type_name' => num2Thai($land['land_type_name']),
                        'land_no' => num2Thai($land['land_type_name']),
                        'land_addr_no' => num2Thai($land['land_addr_no']),
                        'land_district' => $land['district_name'],
                        'land_amphur' => $land['amphur_name'],
                        'land_province' => $land['province_name'],
                        'land_area' => num2Thai($land['land_area_rai']).' - '.num2Thai($land['land_area_ngan']).' - '.num2Thai($land['land_area_wah']),
                        'land_rai' => num2Thai($land['land_area_rai']),
                        'land_ngan' => num2Thai($land['land_area_ngan']),
                        'land_wah' => num2Thai($land['land_area_wah']),
                        'building' => $data_building
                    );
            }
        }

        $TBS->MergeBlock('land', $data_land);

        $loan_guarantee_bond =  $this->loan_guarantee_bond->getByLoan($analysis['loan_id']);

        $data_bond = array();
        if (!empty($loan_guarantee_bond)) {
            $sum_bond = 0;
            foreach ($loan_guarantee_bond as $list_bond) {
                $sum_bond += intval($list_bond['loan_bond_amount']);

                $data_bond[] = array(
                            'bond_owner' => $list_bond['loan_bond_owner'],
                            'bond_type' => num2Thai($list_bond['loan_bond_type']),
                            'bond_startnumber' => num2Thai($list_bond['loan_bond_startnumber']),
                            'bond_endnumber' => num2Thai($list_bond['loan_bond_endnumber']),
                            'bond_amount' => num2Thai(number_format($list_bond['loan_bond_amount'], 2)),
                            'bond_amount_text' => num2wordsThai(number_format($list_bond['loan_bond_amount'], 2))
                        );
            }

            $sum_bond = num2Thai(number_format($sum_bond, 2));
            $per_bond = num2Thai(number_format(($sum_bond / $data['loan_amount']) * 100, 2));
        } else {
            $sum_bond = ' - ';
            $per_bond = ' - ';
        }

        $GLOBALS['sum_bond'] = $sum_bond;
        $GLOBALS['per_bond'] = $per_bond;

        $TBS->MergeBlock('bond', $data_bond);

        $loan_guarantee_bookbank =  $this->loan_guarantee_bookbank->getByLoan($analysis['loan_id']);

        $loan_bookbank_type = array(
                '1' => 'ออมทรัพย์',
                '2' => 'ฝากประจำ',
                '3' => 'เผื่อเรียก'
        );

        $data_bookbank = array();
        if (!empty($loan_guarantee_bookbank)) {
            $sum_bookbank = 0;
            foreach ($loan_guarantee_bookbank as $list_bookbank) {
                $sum_bookbank += intval($list_bookbank['loan_bookbank_amount']);

                $data_bookbank[] = array(
                        'bookbank_owner' => $list_bookbank['loan_bookbank_owner'],
                        'bookbank_bank' => num2Thai($list_bookbank['bank_name']),
                        'bookbank_branch' => num2Thai($list_bookbank['loan_bookbank_branch']),
                        'bookbank_no' => num2Thai($list_bookbank['loan_bookbank_no']),
                        'bookbank_type' => num2Thai($loan_bookbank_type[$list_bookbank['loan_bookbank_type']]),
                        'bookbank_date' => num2Thai(convert_dmy_th($list_bookbank['loan_bookbank_date'])),
                        'bookbank_amount' => num2Thai(number_format($list_bookbank['loan_bookbank_amount'], 2)),
                        'bookbank_amount_text' => num2wordsThai(number_format($list_bookbank['loan_bookbank_amount'], 2)),
                    );
            }

            $sum_bookbank = num2Thai(number_format($sum_bookbank, 2));
            $per_bookbank = num2Thai(number_format(($sum_bookbank / $data['loan_amount']) * 100, 2));
        } else {
            $sum_bookbank = ' - ';
            $per_bookbank = ' - ';
        }

        $GLOBALS['sum_bookbank'] = $sum_bookbank;
        $GLOBALS['per_bookbank'] = $per_bookbank;

        $TBS->MergeBlock('bookbank', $data_bookbank);

        $loan_guarantee_bondsman =  $this->loan_guarantee_bondsman->getByLoan($analysis['loan_id']);

        $data_bondsman = array();
        if (!empty($loan_guarantee_bondsman)) {
            $sum_bondsman = 0;
            foreach ($loan_guarantee_bondsman as $list_bondsman) {
                $age_bondsman = empty($list_bondsman['person_birthdate'])? ' - ' : num2Thai(age($list_bondsman['person_birthdate']));
                $sum_bondsman += intval(0);

                $data_bondsman[] = array(
                            'bondsman_fullname' => $list_bondsman['title_name'].$list_bondsman['person_fname'].' '.$list_bondsman['person_lname'],
                            'bondsman_age' => $age_bondsman,
                            'bondsman_thaiid' => num2Thai(formatThaiid($list_bondsman['person_thaiid'])),
                            'bondsman_career' => num2Thai($list_bondsman['career_name']),
                            'bondsman_position' => (empty($list_bondsman['person_position'])? ' - ' : num2Thai($list_bondsman['person_position'])),
                            'bondsman_belong' => (empty($list_bondsman['person_belong'])? ' - ' : num2Thai($list_bondsman['person_belong'])),
                            'bondsman_income' => num2Thai(number_format($list_bondsman['person_income_per_month'], 2)),
                            'bondsman_home_no' => num2Thai($list_bondsman['person_addr_pre_no']),
                            'bondsman_home_moo' => (empty($list_bondsman['person_addr_pre_moo'])? ' - ' : num2Thai($list_bondsman['person_addr_pre_moo'])),
                            'bondsman_home_road' => (empty($list_bondsman['person_addr_pre_road'])? ' - ' : num2Thai($list_bondsman['person_addr_pre_road'])),
                            'bondsman_home_district' => $list_bondsman['district_name'],
                            'bondsman_home_amphur' => $list_bondsman['amphur_name'],
                            'bondsman_home_province' => $list_bondsman['province_name'],
                            'bondsman_mobile' => (empty($list_bondsman['person_mobile'])? '-' : num2Thai($list_bondsman['person_mobile']))
                        );
            }

            $sum_bondsman = num2Thai(number_format($sum_bondsman, 2));
        } else {
            $sum_bondsman = ' - ';
        }

        $GLOBALS['$sum_bondsman'] = $sum_bondsman;

        $TBS->MergeBlock('bondsman', $data_bondsman);

        $loan_income_1 = $this->loan_income->getByPerson($data['person_id'], '1');
        $loan_income_2 = $this->loan_income->getByPerson($data['person_id'], '2');
        $loan_income_3 = $this->loan_income->getByPerson($data['person_id'], '3');

        $income = 0;
        $expenditure_farm = 0;
        if (!empty($loan_income_1)) {
            foreach ($loan_income_1 as $list_income_1) {
                $year1_income = floatval($list_income_1['loan_activity_income1']);
                $income += $year1_income;

                $year1_expenditure = floatval($list_income_1['loan_activity_expenditure1']);
                $expenditure_farm += $year1_expenditure;
            }
        }

        $expenditure_other = 0;
        if (!empty($loan_income_2)) {
            foreach ($loan_income_2 as $list_income_2) {
                $year1_income = floatval($list_income_2['loan_activity_income1']);
                $income += $year1_income;

                $year1_expenditure = floatval($list_income_2['loan_activity_expenditure1']);
                $expenditure_other += $year1_expenditure;
            }
        }

        $expenditure_other_text = '';
        if (!empty($loan_income_3)) {
            $html_income_other = '';
            $income_other = 0;

            foreach ($loan_income_3 as $list_income_3) {
                $expenditure_other_text .= $list_income_2['loan_activity_name'].', ';

                $html_income_other .= $list_income_3['loan_activity_name'].', ';
                $year1_income = floatval($list_income_3['loan_activity_income1']);
                $income_other += $year1_income;

                $year1_expenditure = floatval($list_income_3['loan_activity_expenditure1']);
                $expenditure_other += $year1_expenditure;
            }

            $expenditure_other_text =  substr($expenditure_other_text, 0, -2);

            $html_income_other =  substr($html_income_other, 0, -2);
            $income_other = num2Thai(number_format($income_other, 2));
        } else {
            $html_income_other = ' - ';
            $income_other = ' - ';
            $expenditure_other_text = ' - ';
        }

        $GLOBALS['income'] = num2Thai(number_format($income, 2));
        $GLOBALS['html_income_other'] = $html_income_other;
        $GLOBALS['income_other'] = $income_other;
        $GLOBALS['expenditure_other_text'] = $expenditure_other_text;

        $loan_expenditure_1 = $this->loan_expenditure->getByPerson($data['person_id'], '1');
        $loan_expenditure_2 = $this->loan_expenditure->getByPerson($data['person_id'], '2');

        $expenditure_family = 0;
        if (!empty($loan_expenditure_1)) {
            foreach ($loan_expenditure_1 as $list_expenditure_1) {
                $year1_amount = floatval($list_expenditure_1['loan_expenditure_amount1']);
                $expenditure_family += $year1_amount;
            }
        }

        if (!empty($loan_expenditure_2)) {
            foreach ($loan_expenditure_2 as $list_expenditure_2) {
                $year1_amount = floatval($list_expenditure_2['loan_expenditure_amount1']);
                $expenditure_other += $year1_amount;
            }
        }

        $GLOBALS['sum_expenditure'] = num2Thai(number_format($expenditure_farm + $expenditure_family + $expenditure_other, 2));
        $GLOBALS['sum_income'] = num2Thai(number_format($income + $income_other - $expenditure_farm - $expenditure_family - $expenditure_other, 2));

        if ($expenditure_farm==0) {
            $check_farm = '( )';
            $expenditure_farm = ' - ';
        } else {
            $check_farm = '(√)';
            $expenditure_farm = num2Thai(number_format($expenditure_farm, 2));
        }

        $GLOBALS['check_farm'] = $check_farm;
        $GLOBALS['expenditure_farm'] = $expenditure_farm;

        if ($expenditure_family==0) {
            $check_family = '( )';
            $expenditure_family = ' - ';
        } else {
            $check_family = '(√)';
            $expenditure_family = num2Thai(number_format($expenditure_family, 2));
        }

        $GLOBALS['check_family'] = $check_family;
        $GLOBALS['expenditure_family'] = $expenditure_family;

        if ($expenditure_other==0) {
            $check_other = '( )';
            $expenditure_other = ' - ';
        } else {
            $check_other = '(√)';
            $expenditure_other = num2Thai(number_format($expenditure_other, 2));
        }

        $GLOBALS['check_other'] = $check_other;
        $GLOBALS['expenditure_other'] = $expenditure_other;

        $schedule = $this->loan_schedule->getByLoan($analysis['loan_id']);

        $count_round = count($schedule);
        $round_start = '';
        $round_end = '';
        if (!empty($schedule)) {
            foreach ($schedule as $list_schedule) {
                if ($round_start=='') {
                    $round_start = num2Thai(convert_dmy_th($list_schedule['loan_schedule_date']));
                }
                $round_end = num2Thai(convert_dmy_th($list_schedule['loan_schedule_date']));
            }
        }

        $per_amount = $data['loan_amount'] / $count_round;

        $GLOBALS['count_round'] = num2Thai($count_round);
        $GLOBALS['round_start'] = $round_start;
        $GLOBALS['round_end'] = $round_end;
        $GLOBALS['interest'] = num2Thai($analysis['loan_analysis_interest']);
        $GLOBALS['per_amount'] = num2Thai(number_format($per_amount, 2));
        $GLOBALS['per_sum'] = num2Thai(number_format($per_amount * ((100 + intval($analysis['loan_analysis_interest'])) / 100), 2));
        $GLOBALS['year'] = num2Thai($analysis['loan_analysis_year']);

        $template = 'word/template/14_approve.docx';
        $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

        $TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
        $output_file_name = 'approve_'.date('Y-m-d').'.docx';
        $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
        $TBS->Show(OPENTBS_FILE, $temp_file);
        $this->send_download($temp_file, $output_file_name);
    }

    public function send_download($temp_file, $file)
    {
        $basename = basename($file);
        $length   = sprintf("%u", filesize($temp_file));

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $basename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . $length);
        ob_clean();
        flush();
        set_time_limit(0);
        readfile($temp_file);
        exit();
    }
    public function pdf($id)
    {
        ob_start();
        $this->load->library('tcpdf');

        $this->load->model('non_loan_objective/non_loan_objective_model', 'non_loan_objective');
        $this->load->model('land_type/land_type_model', 'land_type');
        $this->load->model('non_loan_case/non_loan_case_model', 'non_loan_case');
        $this->load->model('non_loan_model', 'non_loan');

        $data = $this->non_loan->getById($id);

        if (!empty($data['creditor']['person_fname'])) {
            $data['creditor']['person_fname'] = $data['creditor']['title']['title_name'].$data['creditor']['person_fname'];
        }

        $objective = $this->non_loan_objective->all();

        $html_objective = '';
        $i_objective = 1;
        foreach ($objective as $objective_data) {
            if ($data['non_loan_objective_id']==$objective_data['non_loan_objective_id']) {
                $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
            } else {
                $html_checkbox = '<img src="assets\icon\checkbox_uncheck.png">';
            }

            if ($i_objective%3==0) {
                $html_objective .= '<td width="30%">'.$html_checkbox.' '.num2Thai($objective_data['non_loan_objective_name']).'</td>
						</tr>
					</table>';
            } elseif ($i_objective%3==2) {
                $html_objective .= '<td width="30%">'.$html_checkbox.' '.num2Thai($objective_data['non_loan_objective_name']).'</td>';
            } else {
                $html_objective .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="40%">'.$html_checkbox.' '.num2Thai($objective_data['non_loan_objective_name']).'</td>';
            }

            $i_objective++;
        }

        if ($html_objective!='') {
            if ($i_objective%3==2) {
                $html_objective .= '<td width="60%"></td>
						</tr>
					</table>';
            } elseif ($i_objective%3==0) {
                $html_objective .= '<td width="30%"></td>
						</tr>
					</table>';
            }

            $html_objective = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10%"></td>
						<td width="90%">'.$html_objective.'</td>
					</tr>
				</table>';
        }

        $land_type = $this->land_type->all();

        $html_land_type = '';
        $i_land_type = 1;
        foreach ($land_type as $land_type_data) {
            if ($data['land_type_id']==$land_type_data['land_type_id']) {
                $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
            } else {
                $html_checkbox = '<img src="assets\icon\checkbox_uncheck.png">';
            }

            if ($i_land_type%3==0) {
                $html_land_type .= '<td width="30%">'.$html_checkbox.' '.num2Thai($land_type_data['land_type_name']).'</td>
						</tr>
					</table>';
            } elseif ($i_land_type%3==2) {
                $html_land_type .= '<td width="30%">'.$html_checkbox.' '.num2Thai($land_type_data['land_type_name']).'</td>';
            } else {
                $html_land_type .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="40%">'.$html_checkbox.' '.num2Thai($land_type_data['land_type_name']).'</td>';
            }

            $i_land_type++;
        }

        if ($html_land_type!='') {
            if ($i_land_type%3==2) {
                $html_land_type .= '<td width="60%"></td>
						</tr>
					</table>';
            } elseif ($i_land_type%3==0) {
                $html_land_type .= '<td width="30%"></td>
						</tr>
					</table>';
            }

            $html_land_type = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10%"></td>
						<td width="90%">'.$html_land_type.'</td>
					</tr>
				</table>';
        }

        $case = $this->non_loan_case->all();

        $html_case = '';
        foreach ($case as $case_data) {
            if ($data['non_loan_case_id']==$case_data['non_loan_case_id']) {
                $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
            } else {
                $html_checkbox = '<img src="assets\icon\checkbox_uncheck.png">';
            }

            $html_case .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="10%"></td>
						<td width="90%">'.$html_checkbox.' '.num2Thai($case_data['non_loan_case_name']).'</td>
					</tr>
				</table>';
        }

        if ($data['non_loan_case_id']=='0') {
            $html_checkbox = '<img src="assets\icon\checkbox_check.png">';
            $html_other = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">ระบุ&nbsp;&nbsp;'.num2Thai($data['non_loan_cause_other']).'</td>
				</tr>
			</table>';
        } else {
            $html_checkbox = '<img src="assets\icon\checkbox_uncheck.png">';
            $html_other = '';
        }

        $html_case .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="10%"></td>
					<td width="90%">'.$html_checkbox.' อื่นๆ</td>
				</tr>
			</table>
			'.$html_other;

        $filename = 'non_loan_'.date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename);//  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->AddPage();

        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๒</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					แบบบันทึกการขอสินเชื่อ ตามข้อบังคับสถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.)<br/>
					ว่าด้วย การให้สินเชื่อเพื่อป้องกันและแก้ไขปัญหาการสูญเสียสิทธิในที่ดินเกษตรกรรม พ.ศ. ๒๕๕๙<br/>
					(กรณีไม่รับคำขอ)
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="45%"></td>
				<td width="55%">เขียนที่&nbsp;&nbsp;'.num2Thai($data['non_loan_location']).'</td>
			</tr>
			<tr>
				<td width="45%"></td>
				<td width="55%">วันที่&nbsp;&nbsp;'.num2Thai($data['non_loan_datefull']).'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="30%">ข้อ ๑. ชื่อ&nbsp;&nbsp;'.$data['person']['title']['title_name'].$data['person']['person_fname'].'</td>
				<td width="25%">นามสกุล&nbsp;&nbsp;'.$data['person']['person_lname'].'</td>
				<td width="40%">เลขประจำตัวประชาชน&nbsp;&nbsp;'.num2Thai(formatThaiid($data['person']['person_thaiid'])).'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%">ที่อยู่ เลขที่&nbsp;&nbsp;'.num2Thai($data['person']['person_addr_card_no']).'</td>
				<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($data['person']['person_addr_card_moo'])? '-' : num2Thai($data['person']['person_addr_card_moo'])).'</td>
				<td width="30%">ถนน&nbsp;&nbsp;'.(empty($data['person']['person_addr_card_road'])? '-' : num2Thai($data['person']['person_addr_card_road'])).'</td>
				<td width="35%">ตำบล/แขวง&nbsp;&nbsp;'.$data['person']['district']['district_name'].'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="35%">อำเภอ/เขต&nbsp;&nbsp;'.$data['person']['amphur']['amphur_name'].'</td>
				<td width="30%">จังหวัด&nbsp;&nbsp;'.$data['person']['province']['province_name'].'</td>
				<td width="35%">โทรศัพท์&nbsp;&nbsp;'.(empty($data['person']['person_phone'])? '-' : num2Thai($data['person']['person_phone'])).'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๒.	วัตถุประสงค์การขอสินเชื่อ</td>
			</tr>
		</table>
		'.$html_objective.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๓.	ประเภทที่ดิน</td>
			</tr>
		</table>
		'.$html_land_type.'
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="10%"></td>
				<td width="15%" align="right">จำนวนเนื้อที่</td>
				<td width="15%" align="right">'.num2Thai($data['non_loan_area_rai']).'&nbsp;&nbsp;ไร่</td>
				<td width="15%" align="right">'.num2Thai($data['non_loan_area_ngan']).'&nbsp;&nbsp;งาน</td>
				<td width="20%" align="right">'.num2Thai($data['non_loan_area_wah']).'&nbsp;&nbsp;ตารางวา</td>
				<td width="20%"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="50%">ข้อ ๔.	ชื่อเจ้าหนี้/ผู้รับซื้อฝาก&nbsp;&nbsp;'.(empty($data['creditor']['person_fname'])? '-' : $data['creditor']['person_fname']).'</td>
				<td width="45%">นามสกุล&nbsp;&nbsp;'.(empty($data['creditor']['person_lname'])? '-' : $data['creditor']['person_lname']).'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%">ที่อยู่ เลขที่&nbsp;&nbsp;'.(empty($data['creditor']['person_addr_card_no'])? '-' : num2Thai($data['creditor']['person_addr_card_no'])).'</td>
				<td width="15%">หมู่ที่&nbsp;&nbsp;'.(empty($data['creditor']['person_addr_card_moo'])? '-' : num2Thai($data['creditor']['person_addr_card_moo'])).'</td>
				<td width="30%">ถนน&nbsp;&nbsp;'.(empty($data['creditor']['person_addr_card_road'])? '-' : $data['creditor']['person_addr_card_road']).'</td>
				<td width="35%">ตำบล/แขวง&nbsp;&nbsp;'.(empty($data['creditor']['district']['district_name'])? '-' : $data['creditor']['district']['district_name']).'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="35%">อำเภอ/เขต&nbsp;&nbsp;'.(empty($data['creditor']['amphur']['amphur_name'])? '-' : $data['creditor']['amphur']['amphur_name']).'</td>
				<td width="30%">จังหวัด&nbsp;&nbsp;'.(empty($data['creditor']['province']['province_name'])? '-' : $data['creditor']['province']['province_name']).'</td>
				<td width="35%">โทรศัพท์&nbsp;&nbsp;'.(empty($data['creditor']['person_phone'])? '-' : num2Thai($data['creditor']['person_phone'])).'</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๕.	ไม่รับคำขอ เนื่องจาก</td>
			</tr>
		</table>
		'.$html_case;

        $html_sing = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้าพเจ้าขอรับรองว่า เจ้าหน้าที่ได้อ่านให้ฟัง และข้าพเจ้ารับทราบและเข้าใจแล้ว จึงลงลายชื่อไว้เป็นหลักฐาน</td>
			</tr>
		</table>
		<br/><br/>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="50%" style="text-align:center;">ลงชื่อ .................................................... ผู้ให้ถ้อยคำ</td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... ผู้บันทึก</td>
			</tr>
			<tr>
				<td width="50%" style="text-align:center;">(.................................................)</td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ตำแหน่ง ..................................</td>
			</tr>
		</table>';

        if ($html_other=='') {
            $htmlcontent .= $html_sing;

            $pdf->SetFont('thsarabun', '', 16);
            $pdf->writeHTML($htmlcontent, true, 0, true, true);
        } else {
            $pdf->SetFont('thsarabun', '', 16);
            $pdf->writeHTML($htmlcontent, true, 0, true, true);

            $pdf->SetMargins(10, 20, 10, true);
            $pdf->AddPage();

            $pdf->SetFont('thsarabun', '', 16);

            $pdf->writeHTML($html_sing, true, 0, true, true);
        }

        $pdf->Output($filename.'.pdf', 'I');
    }
}
