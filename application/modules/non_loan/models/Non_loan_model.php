<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Non_loan_model extends CI_Model
{
	private $table = 'non_loan';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function all()
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("non_loan_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("non_loan_createby", get_uid_login());
		$this->db->order_by("non_loan_id", "desc");
		$query = $this->db->get();
		
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function search($thaiid='', $fullname='')
	{
		$where = '';
		if($thaiid!='') $where .= "OR person.person_thaiid LIKE '%".$thaiid."%' ";
		if($fullname!='') $where .= "OR person.person_fname LIKE '%".$fullname."%' OR person.person_lname LIKE '%".$fullname."%'";
		if($where!='') $where = substr($where, 3);
		
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join("person", "non_loan.person_id = person.person_id", "RIGHT");
		$this->db->join("config_title", "person.title_id = config_title.title_id", "LEFT");
		$this->db->join("config_non_loan_objective", "non_loan.non_loan_objective_id = config_non_loan_objective.non_loan_objective_id", "LEFT");
		$this->db->where("non_loan.non_loan_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("non_loan.non_loan_createby", get_uid_login());
		
		if($where!='') $this->db->where("(".$where.")", NULL, FALSE);
		
		$this->db->order_by("non_loan.non_loan_id", "desc");
		
		$query = $this->db->get();
	
		return $query->num_rows()!=0? $query->result_array() : array();
	}
	
	public function getById($id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("non_loan_id", $id);
		$this->db->where("non_loan_status !=", "0");
		if(check_permission_isowner('loan')) $this->db->where("non_loan_createby", get_uid_login());
		$query = $this->db->get();
		
		if($query->num_rows()!=0)
		{
			$this->load->model('person/person_model', 'person');
			$this->load->model('title/title_model', 'title');
			$this->load->model('district/district_model', 'district');
			$this->load->model('amphur/amphur_model', 'amphur');
			$this->load->model('province/province_model', 'province');
			
			$result = $query->row_array();
			
			$result['non_loan_datefull'] = thai_display_date($result['non_loan_date']);
			$result['non_loan_date'] = convert_dmy_th($result['non_loan_date']);
			$result['person'] = $this->person->getById($result['person_id']);
			$result['person']['title'] = $this->title->getById($result['person']['title_id']);
			$result['person']['district'] = $this->district->getById($result['person']['person_addr_card_district_id']);
			$result['person']['amphur'] = $this->amphur->getById($result['person']['person_addr_card_amphur_id']);
			$result['person']['province'] = $this->province->getById($result['person']['person_addr_card_province_id']);
			
			if(!empty($result['non_loan_creditor']))
			{
				$result['creditor'] = $this->person->getById($result['non_loan_creditor']);
				$result['creditor']['title'] = $this->title->getById($result['creditor']['title_id']);
				$result['creditor']['district'] = $this->district->getById($result['creditor']['person_addr_card_district_id']);
				$result['creditor']['amphur'] = $this->amphur->getById($result['creditor']['person_addr_card_amphur_id']);
				$result['creditor']['province'] = $this->province->getById($result['creditor']['person_addr_card_province_id']);
			}
			else
			{
				$result['creditor']['person_id'] = '';
				$result['creditor']['title_id'] = '';
				$result['creditor']['person_fname'] = '';
				$result['creditor']['person_lname'] = '';
				$result['creditor']['person_thaiid'] = '';
				$result['creditor']['person_addr_card_no'] = '';
				$result['creditor']['person_addr_card_moo'] = '';
				$result['creditor']['person_addr_card_road'] = '';
				$result['creditor']['person_addr_card_district_id'] = '';
				$result['creditor']['person_addr_card_amphur_id'] = '';
				$result['creditor']['person_addr_card_province_id'] = '';
				$result['creditor']['person_phone'] = '';
			}
		}
		else
		{
			$result = null;
		}

		return $result;
	}
	
	public function save($array, $id='')
	{
		$this->load->model('person/person_model', 'person');
		
		if($id=='')
		{
			$person['title_id'] = $array['person']['title_id'];
			$person['person_fname'] = $array['person']['person_fname'];
			$person['person_lname'] =  $array['person']['person_lname'];
			$person['person_thaiid'] =  $array['person']['person_thaiid'];
			$person['person_addr_card_no'] =  $array['person']['person_addr_card_no'];
			$person['person_addr_card_moo'] =  $array['person']['person_addr_card_moo'];
			$person['person_addr_card_road'] =  $array['person']['person_addr_card_road'];
			$person['person_addr_card_district_id'] =  $array['person']['person_addr_card_district_id'];
			$person['person_addr_card_amphur_id'] =  $array['person']['person_addr_card_amphur_id'];
			$person['person_addr_card_province_id'] =  $array['person']['person_addr_card_province_id'];
			$person['person_phone'] =  $array['person']['person_phone'];
			
			if($array['person']['person_id']=='')
			{
				$person['person_status'] =  '1';
				
				$data_person = $this->person->save($person);
				$array['person_id'] = $data_person['id'];
			}
			else
			{
				$data_person = $this->person->save($person, $array['person']['person_id']);
				$array['person_id'] = $array['person']['person_id'];
			}
			
			$creditor['title_id'] = $array['creditor']['title_id'];
			$creditor['person_fname'] = $array['creditor']['person_fname'];
			$creditor['person_lname'] =  $array['creditor']['person_lname'];
			$creditor['person_thaiid'] =  $array['creditor']['person_thaiid'];
			$creditor['person_addr_card_no'] =  $array['creditor']['person_addr_card_no'];
			$creditor['person_addr_card_moo'] =  $array['creditor']['person_addr_card_moo'];
			$creditor['person_addr_card_road'] =  $array['creditor']['person_addr_card_road'];
			$creditor['person_addr_card_district_id'] =  $array['creditor']['person_addr_card_district_id'];
			$creditor['person_addr_card_amphur_id'] =  $array['creditor']['person_addr_card_amphur_id'];
			$creditor['person_addr_card_province_id'] =  $array['creditor']['person_addr_card_province_id'];
			$creditor['person_phone'] =  $array['creditor']['person_phone'];
			
			if($array['creditor']['person_id']=='')
			{
				$creditor['person_status'] =  '1';
				
				$data_creditor = $this->person->save($creditor);
				$array['non_loan_creditor'] = $data_creditor['id'];
			}
			else
			{
				$data_creditor = $this->person->save($creditor, $array['creditor']['person_id']);
				$array['non_loan_creditor'] = $array['creditor']['person_id'];	
			}
			
			unset($array['creditor']);
			unset($array['person']);
			
			$this->db->set($array);
			$this->db->set("non_loan_createby", get_uid_login());
			$this->db->set("non_loan_createdate", "NOW()", FALSE);
			$this->db->set("non_loan_updateby", get_uid_login());
			$this->db->set("non_loan_updatedate", "NOW()", FALSE);
			$this->db->insert($this->table);
			
			$id = $this->db->insert_id();
		}
		else
		{
			$person['title_id'] = $array['person']['title_id'];
			$person['person_fname'] = $array['person']['person_fname'];
			$person['person_lname'] =  $array['person']['person_lname'];
			$person['person_thaiid'] =  $array['person']['person_thaiid'];
			$person['person_addr_card_no'] =  $array['person']['person_addr_card_no'];
			$person['person_addr_card_moo'] =  $array['person']['person_addr_card_moo'];
			$person['person_addr_card_road'] =  $array['person']['person_addr_card_road'];
			$person['person_addr_card_district_id'] =  $array['person']['person_addr_card_district_id'];
			$person['person_addr_card_amphur_id'] =  $array['person']['person_addr_card_amphur_id'];
			$person['person_addr_card_province_id'] =  $array['person']['person_addr_card_province_id'];
			$person['person_phone'] =  $array['person']['person_phone'];
			
			$data_person = $this->person->save($person, $array['person']['person_id']);
			$array['person_id'] = $array['person']['person_id'];
			
			$creditor['title_id'] = $array['creditor']['title_id'];
			$creditor['person_fname'] = $array['creditor']['person_fname'];
			$creditor['person_lname'] =  $array['creditor']['person_lname'];
			$creditor['person_thaiid'] =  $array['creditor']['person_thaiid'];
			$creditor['person_addr_card_no'] =  $array['creditor']['person_addr_card_no'];
			$creditor['person_addr_card_moo'] =  $array['creditor']['person_addr_card_moo'];
			$creditor['person_addr_card_road'] =  $array['creditor']['person_addr_card_road'];
			$creditor['person_addr_card_district_id'] =  $array['creditor']['person_addr_card_district_id'];
			$creditor['person_addr_card_amphur_id'] =  $array['creditor']['person_addr_card_amphur_id'];
			$creditor['person_addr_card_province_id'] =  $array['creditor']['person_addr_card_province_id'];
			$creditor['person_phone'] =  $array['creditor']['person_phone'];
			
			$data_creditor = $this->person->save($creditor, $array['creditor']['person_id']);
			$array['non_loan_creditor'] = $array['creditor']['person_id'];
			
			unset($array['creditor']);
			unset($array['person']);
			
			$this->db->set($array);
			$this->db->set("non_loan_updateby", get_uid_login());
			$this->db->set("non_loan_updatedate", "NOW()", FALSE);
			$this->db->where("non_loan_id", $id);
			$this->db->update($this->table, $array);
		}
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function del($id)
	{
		$array =  array("non_loan_status" => "0");
		
		$this->db->set($array);
		$this->db->where("non_loan_id", $id);
		$this->db->update($this->table, $array);
		
		$num_row = $this->db->affected_rows();
		
		return array('id' => $id, 'rows' => $num_row);
	}
	
	public function getModel()
	{
		$data['non_loan_date'] = date('d/m/').(intval(date('Y'))+543);
		$data['non_loan_location'] = 'สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)';
		$data['person_id'] = '';
		$data['person']['person_id'] = '';
		$data['person']['title_id'] = '';
		$data['person']['person_fname'] = '';
		$data['person']['person_lname'] = '';
		$data['person']['person_thaiid'] = '';
		$data['person']['person_addr_card_no'] = '';
		$data['person']['person_addr_card_moo'] = '';
		$data['person']['person_addr_card_road'] = '';
		$data['person']['person_addr_card_district_id'] = '';
		$data['person']['person_addr_card_amphur_id'] = '';
		$data['person']['person_addr_card_province_id'] = '';
		$data['person']['person_phone'] = '';
		$data['non_loan_creditor'] = '';
		$data['creditor']['person_id'] = '';
		$data['creditor']['title_id'] = '';
		$data['creditor']['person_fname'] = '';
		$data['creditor']['person_lname'] = '';
		$data['creditor']['person_thaiid'] = '';
		$data['creditor']['person_addr_card_no'] = '';
		$data['creditor']['person_addr_card_moo'] = '';
		$data['creditor']['person_addr_card_road'] = '';
		$data['creditor']['person_addr_card_district_id'] = '';
		$data['creditor']['person_addr_card_amphur_id'] = '';
		$data['creditor']['person_addr_card_province_id'] = '';
		$data['creditor']['person_phone'] = '';
		$data['non_loan_objective_id'] = '';
		$data['land_type_id'] = '';
		$data['non_loan_area_rai'] = '0';
		$data['non_loan_area_ngan'] = '0';
		$data['non_loan_area_wah'] = '0';
		$data['non_loan_case_id'] = '';
		$data['non_loan_cause_other'] = '';
		$data['non_loan_status'] = '1';
	
		return $data;
	}
}
?>