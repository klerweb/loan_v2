<form id="frmNonLoan" name="frmNonLoan" action="<?php echo base_url().'non_loan/save'; ?>" method="post">
	<input type="hidden" id="txtId" name="txtId" value="<?php echo $id; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">แบบบันทึกขอสินเชื่อ (กรณีไม่รับคำขอ)</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">เขียนที่</label>
								<input type="text" name="txtLocation" id="txtLocation" class="form-control" value="<?php echo $data['non_loan_location']; ?>" maxlength="255" />
							</div><!-- form-group -->
						</div><!-- col-sm-6 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">วันที่ <span class="asterisk">*</span></label>
								<div class="input-group">
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo $data['non_loan_date']; ?>" readonly="readonly"/>
									<span class="input-group-addon hand" id="iconDate"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">สถานะ <span class="asterisk">*</span></label>
								<select name="ddlStatus" id="ddlStatus" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<option value="1"<?php if($data['non_loan_status']=='1') echo ' SELECTED'; ?>>แบบร่าง</option>
									<option value="2"<?php if($data['non_loan_status']=='2') echo ' SELECTED'; ?>>บันทึกเรียบร้อย</option>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<hr/>
					<div class="row" style="padding-bottom:10px;">
						<div class="col-sm-12">
							<h4>ผู้ขอสินเชื่อ</h4>
							<input type="hidden" name="txtPersonId" id="txtPersonId" value="<?php echo $data['person']['person_id']; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เลขที่บัตรประชาชน <span class="asterisk">*</span></label>
								<input type="text" name="txtThaiid" id="txtThaiid" class="form-control" value="<?php echo $data['person']['person_thaiid']; ?>" maxlength="17" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">คำนำหน้า <span class="asterisk">*</span></label>
								<select name="ddlTitle" id="ddlTitle" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php foreach ($title as $title_data) { ?>
										<option value="<?php echo $title_data['title_id']; ?>"<?php if($data['person']['title_id']==$title_data['title_id']) echo ' SELECTED'; ?>><?php echo $title_data['title_name']; ?></option>
									<?php } ?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ชื่อ <span class="asterisk">*</span></label>
								<input type="text" name="txtFname" id="txtFname" class="form-control" value="<?php echo $data['person']['person_fname']; ?>" maxlength="50" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">นามสกุล <span class="asterisk">*</span></label>
								<input type="text" name="txtLname" id="txtLname" class="form-control" value="<?php echo $data['person']['person_lname']; ?>" maxlength="50" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เบอร์โทรศัพท์ <span class="asterisk">*</span></label>
								<input type="text" name="txtPhone" id="txtPhone" class="form-control" value="<?php echo $data['person']['person_phone']; ?>" maxlength="10" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">บ้านเลขที่ <span class="asterisk">*</span></label>
								<input type="text" name="txtAddrCardNo" id="txtAddrCardNo" class="form-control" value="<?php echo $data['person']['person_addr_card_no']; ?>" maxlength="10" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมู่ที่</label>
								<input type="text" name="txtAddrCardMoo" id="txtAddrCardMoo" class="form-control" value="<?php echo $data['person']['person_addr_card_moo']; ?>" maxlength="2" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ถนน</label>
								<input type="text" name="txtAddrCardRoad" id="txtAddrCardRoad" class="form-control" value="<?php echo $data['person']['person_addr_card_road']; ?>" maxlength="100" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">จังหวัด <span class="asterisk">*</span></label>
								<select name="ddlProvince" id="ddlProvince" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php foreach ($province as $province_data) { ?>
										<option value="<?php echo $province_data['province_id']; ?>"<?php if($data['person']['person_addr_card_province_id']==$province_data['province_id']) echo ' SELECTED'; ?>><?php echo $province_data['province_name']; ?></option>
									<?php } ?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">อำเภอ <span class="asterisk">*</span></label>
								<div id="divAmphur">
									<select name="ddlAmphur" id="ddlAmphur" data-placeholder="กรุณาเลือก" class="width100p" required>
										<option value="">กรุณาเลือก</option>
										<?php foreach ($amphur as $amphur_data) { ?>
											<option value="<?php echo $amphur_data['amphur_id']; ?>"<?php if($data['person']['person_addr_card_amphur_id']==$amphur_data['amphur_id']) echo ' SELECTED'; ?>><?php echo $amphur_data['amphur_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ตำบล <span class="asterisk">*</span></label>
								<div id="divDistrict">
									<select name="ddlDistrict" id="ddlDistrict" data-placeholder="กรุณาเลือก" class="width100p" required>
										<option value="">กรุณาเลือก</option>
										<?php foreach ($district as $district_data) { ?>
											<option value="<?php echo $district_data['district_id']; ?>"<?php if($data['person']['person_addr_card_district_id']==$district_data['district_id']) echo ' SELECTED'; ?>><?php echo $district_data['district_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<hr/>
					<div class="row" style="padding-bottom:10px;">
						<div class="col-sm-12">
							<h4>เจ้าหนี้ / ผู้รับซื้อฝาก</h4>
							<input type="hidden" name="txtCreditorId" id="txtCreditorId" value="<?php echo $data['creditor']['person_id']; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เลขที่บัตรประชาชน <span class="asterisk">*</span></label>
								<input type="text" name="txtCreditorThaiid" id="txtCreditorThaiid" class="form-control" value="<?php echo $data['creditor']['person_thaiid']; ?>" maxlength="17" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">คำนำหน้า <span class="asterisk">*</span></label>
								<select name="ddlCreditorTitle" id="ddlCreditorTitle" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php foreach ($title as $title_data) { ?>
										<option value="<?php echo $title_data['title_id']; ?>"<?php if($data['creditor']['title_id']==$title_data['title_id']) echo ' SELECTED'; ?>><?php echo $title_data['title_name']; ?></option>
									<?php } ?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ชื่อ <span class="asterisk">*</span></label>
								<input type="text" name="txtCreditorFname" id="txtCreditorFname" class="form-control" value="<?php echo $data['creditor']['person_fname']; ?>" maxlength="50" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">นามสกุล <span class="asterisk">*</span></label>
								<input type="text" name="txtCreditorLname" id="txtCreditorLname" class="form-control" value="<?php echo $data['creditor']['person_lname']; ?>" maxlength="50" required />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">เบอร์โทรศัพท์</label>
								<input type="text" name="txtCreditorPhone" id="txtCreditorPhone" class="form-control" value="<?php echo $data['creditor']['person_phone']; ?>" maxlength="10" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">บ้านเลขที่</label>
								<input type="text" name="txtCreditorAddrCardNo" id="txtCreditorAddrCardNo" class="form-control" value="<?php echo $data['creditor']['person_addr_card_no']; ?>" maxlength="10" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">หมู่ที่</label>
								<input type="text" name="txtCreditorAddrCardMoo" id="txtCreditorAddrCardMoo" class="form-control" value="<?php echo $data['creditor']['person_addr_card_moo']; ?>" maxlength="2" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ถนน</label>
								<input type="text" name="txtCreditorAddrCardRoad" id="txtCreditorAddrCardRoad" class="form-control" value="<?php echo $data['creditor']['person_addr_card_road']; ?>" maxlength="100" />
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">จังหวัด</label>
								<select name="ddlCreditorProvince" id="ddlCreditorProvince" data-placeholder="กรุณาเลือก" class="width100p">
									<option value="">กรุณาเลือก</option>
									<?php foreach ($province as $province_data) { ?>
										<option value="<?php echo $province_data['province_id']; ?>"<?php if($data['creditor']['person_addr_card_province_id']==$province_data['province_id']) echo ' SELECTED'; ?>><?php echo $province_data['province_name']; ?></option>
									<?php } ?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">อำเภอ</label>
								<div id="divCreditorAmphur">
									<select name="ddlCreditorAmphur" id="ddlCreditorAmphur" data-placeholder="กรุณาเลือก" class="width100p">
										<option value="">กรุณาเลือก</option>
										<?php foreach ($amphur_creditor as $amphur_data) { ?>
											<option value="<?php echo $amphur_data['amphur_id']; ?>"<?php if($data['creditor']['person_addr_card_amphur_id']==$amphur_data['amphur_id']) echo ' SELECTED'; ?>><?php echo $amphur_data['amphur_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ตำบล</label>
								<div id="divCreditorDistrict">
									<select name="ddlCreditorDistrict" id="ddlCreditorDistrict" data-placeholder="กรุณาเลือก" class="width100p">
										<option value="">กรุณาเลือก</option>
										<?php foreach ($district_creditor as $district_data) { ?>
											<option value="<?php echo $district_data['district_id']; ?>"<?php if($data['creditor']['person_addr_card_district_id']==$district_data['district_id']) echo ' SELECTED'; ?>><?php echo $district_data['district_name']; ?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
					</div><!-- row -->
					<hr/>
					<div class="row" style="padding-bottom:10px;">
						<div class="col-sm-12">
							<h4>การขอสินเชื่อ</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">วัตถุประสงค์การขอสินเชื่อ <span class="asterisk">*</span></label>
								<select name="ddlObjective" id="ddlObjective" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php foreach ($objective as $objective_data) { ?>
										<option value="<?php echo $objective_data['non_loan_objective_id']; ?>"<?php if($data['non_loan_objective_id']==$objective_data['non_loan_objective_id']) echo ' SELECTED'; ?>><?php echo $objective_data['non_loan_objective_name']; ?></option>
									<?php } ?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">ประเภทที่ดิน <span class="asterisk">*</span></label>
								<select name="ddlLandType" id="ddlLandType" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php foreach ($land_type as $land_type_data) { ?>
										<option value="<?php echo $land_type_data['land_type_id']; ?>"<?php if($data['land_type_id']==$land_type_data['land_type_id']) echo ' SELECTED'; ?>><?php echo $land_type_data['land_type_name']; ?></option>
									<?php } ?>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-3 -->
						<div class="col-sm-9">
							<div class="form-group">
								<label class="control-label">เนื้อที่ <span class="asterisk">*</span></label>
								<div class="row">
									<div class="col-sm-4">
										<div class="input-group mb15">
											<input type="text" name="txtRai" id="txtRai" class="form-control right" value="<?php echo $data['non_loan_area_rai']; ?>" />
											<span class="input-group-addon">ไร่</span>
										</div>
										<label class="error" for="txtRai"></label>
									</div>
									<div class="col-sm-4">
										<div class="input-group mb15">
											<input type="text" name="txtNgan" id="txtNgan" class="form-control right" value="<?php echo $data['non_loan_area_ngan']; ?>" />
											<span class="input-group-addon">งาน</span>
										</div>
										<label class="error" for="txtNgan"></label>
									</div>
									<div class="col-sm-4">
										<div class="input-group mb15">
											<input type="text" name="txtWah" id="txtWah" class="form-control right" value="<?php echo $data['non_loan_area_wah']; ?>" />
											<span class="input-group-addon">ตารางวา</span>
										</div>
										<label class="error" for="txtWah"></label>
									</div>
								</div>
							</div><!-- form-group -->
						</div><!-- col-sm-9 -->
					</div><!-- row -->
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">ไม่รับคำขอเนื่องจาก <span class="asterisk">*</span></label>
								<select name="ddlCase" id="ddlCase" data-placeholder="กรุณาเลือก" class="width100p" required>
									<option value="">กรุณาเลือก</option>
									<?php foreach ($case as $case_data) { ?>
										<option value="<?php echo $case_data['non_loan_case_id']; ?>"<?php if($data['non_loan_case_id']==$case_data['non_loan_case_id']) echo ' SELECTED'; ?>><?php echo $case_data['non_loan_case_name']; ?></option>
									<?php } ?>
									<option value="0"<?php if($data['non_loan_case_id']=='0') echo ' SELECTED'; ?>>อื่นๆ</option>
								</select>
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div>
					<div id="divOther" class="row"<?php if($data['non_loan_case_id']!='0') echo ' style="display:none;"'; ?>>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">ระบุ <span class="asterisk">*</span></label>
								<input type="text" name="txtCaseOther" id="txtCaseOther" class="form-control" value="<?php echo $data['non_loan_cause_other']; ?>" maxlength="255" required />
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div>
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>