<select name="ddlAmphur" id="ddlAmphur" data-placeholder="กรุณาเลือก" class="width100p" required>
	<option value="">กรุณาเลือก</option>
	<?php foreach ($amphur as $amphur_data) { ?>
		<option value="<?php echo $amphur_data['amphur_id']; ?>"><?php echo $amphur_data['amphur_name']; ?></option>
	<?php } ?>
</select>
<script type="text/javascript">
	$("#ddlAmphur").select2({
		minimumResultsForSearch: -1
	});

	$("#ddlAmphur").change(function(){
		$("#divDistrict").load(base_url+"non_loan/district/"+$("#ddlAmphur").val());
	});
</script>
