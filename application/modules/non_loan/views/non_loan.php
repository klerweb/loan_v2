<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url('non_loan/form'); ?>'">เพิ่มแบบบันทึก</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-btns">
					<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
				</div><!-- panel-btns -->
				<h4 class="panel-title">ค้นหา</h4>
			</div>
			<div class="panel-body">
				<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo site_url('non_loan'); ?>">
					<div class="form-group">
						<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
						<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="<?php echo $thaiid; ?>" />
					</div><!-- form-group -->
					<div class="form-group">
						<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
						<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
					</div><!-- form-group -->
					<button type="submit" class="btn btn-primary mr5">Search</button>
				</form>
			</div><!-- panel-body -->
		</div><!-- panel -->
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
		            	<th>เลขที่ขอสินเชื่อ</th>
		                <th>วันที่ขอสินเชื่อ</th>
		                <th>ชื่อ - นามสกุล</th>
		                <th>วัตถุประสงคค์การขอสินเชื่อ</th>
		                <th>สถานะ</th>
		                <th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					if(!empty($result))
					{
						$i = 1;
						foreach ($result as $row)
						{
							if($row['non_loan_objective_id']=='0') $row['non_loan_objective_name'] = 'อื่นๆ';
							
							if($row['non_loan_status']=='1')
							{
								$row['non_loan_status'] = 'แบบร่าง';
								$menu = '<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="edit(\''.$row['non_loan_id'].'\')"><span class="fa fa-edit"></span></button>
											<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ยกเลิก" data-original-title="ยกเลิก" onclick="del(\''.$row['non_loan_id'].'\')"><span class="fa fa-trash-o"></span></button>';
							}
							else
							{
								$row['non_loan_status'] = 'บันทึกเรียบร้อย';
								$menu = '';
							}
							
							list($y, $m, $d) = explode('-', $row['non_loan_date']);
							$row['non_loan_date'] = $d.'/'.$m.'/'.(intval($y)+543);
				?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td>NLOAN-<?php echo $row['non_loan_id']; ?></td>
						<td><?php echo $row['non_loan_date']; ?></td>
						<td><?php echo $row['title_name'].$row['person_fname'].' '.$row['person_lname']; ?></td>
		           		<td><?php echo $row['non_loan_objective_name']; ?></td>
		           		<td><?php echo $row['non_loan_status']; ?></td>
		           		<td align="center">
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="พิมพ์" data-original-title="พิมพ์" onclick="window.open('<?php echo site_url('non_loan/word/'.$row['non_loan_id']); ?>', '_blank')"><span class="fa fa-print"></span></button>
		           			<?php echo $menu; ?>
		           		</td>
		           	</tr>
				<?php
						}	
					} 
				?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>