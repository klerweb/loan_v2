<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Non_loan_case extends MY_Controller
{
	private $title_page = 'สาเหตุ (กรณีไม่รับคำขอ)';
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$this->load->model('non_loan_case_model', 'non_loan_case');
	
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array('ตั้งค่า' =>'#');
		
		$data_content = array(
				'result' => $this->non_loan_case->all()
		);
		
		$data = array(
				'title' => $this->title_page,
				'js_other' => array('modules_non_loan_case/non_loan_case.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('non_loan_case', $data_content, TRUE)
		);
		
		$this->parser->parse('main', $data);
	}
	
	public function form($id='')
	{
		$data_menu['menu'] = 'setting';
		$data_breadcrumb['menu'] = array(
				'ตั้งค่า' =>'#',
				$this->title_page => site_url('non_loan_case')
			);
		
		if($id=='')
		{
			$title_page = 'เพิ่มสาเหตุ (กรณีไม่รับคำขอ)';
			
			$data_content['id'] = '';
			$data_content['data'] = $this->getModel();
		}
		else
		{
			$this->load->model('non_loan_case_model', 'non_loan_case');
			$data_content['data'] = $this->non_loan_case->getById($id);
			
			if(empty($data_content['data']))
			{
				$title_page = 'เพิ่มสาเหตุ (กรณีไม่รับคำขอ)';
				
				$data_content['id'] = '';
				$data_content['data'] = $this->getModel();
			}
			else
			{
				$title_page = 'แก้ไขสาเหตุ (กรณีไม่รับคำขอ)';
				
				$data_content['id'] = $id;
			}
		}
		
		$data = array(
				'title' => $title_page,
				'js_other' => array('modules_non_loan_case/form.js'),
				'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
				'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
				'content' => $this->parser->parse('form', $data_content, TRUE)
		);
	
		$this->parser->parse('main', $data);
	}
	
	public function save()
	{
		$this->load->model('non_loan_case_model', 'non_loan_case');
		
		$array['non_loan_case_name'] = $this->input->post('txtNonLoanCaseName');
		$array['non_loan_case_status'] = '1';
		
		$id = $this->non_loan_case->save($array, $this->input->post('txtNonLoanCaseId'));
		
		if($this->input->post('txtNonLoanCaseId')=='')
		{
			$data = array(
					'alert' => 'บันทึกสาเหตุ (กรณีไม่รับคำขอ) เรียบร้อยแล้ว',
					'link' => site_url('non_loan_case'),
			);
		}
		else
		{
			$data = array(
					'alert' => 'แก้ไขสาเหตุ (กรณีไม่รับคำขอ) เรียบร้อยแล้ว',
					'link' => site_url('non_loan_case'),
			);
		}
		
		$this->parser->parse('redirect', $data);
	}
	 
	public function del($id)
	{
		$this->load->model('non_loan_case_model', 'non_loan_case');
		$this->non_loan_case->del($id);
		
		$data = array(
				'alert' => 'ยกเลิกสาเหตุ (กรณีไม่รับคำขอ) เรียบร้อยแล้ว',
				'link' => site_url('non_loan_case'),
		);
		
		$this->parser->parse('redirect', $data);
	}
	
	public function getModel()
	{
		$data['non_loan_case_name'] = '';
		
		return $data;
	}
}