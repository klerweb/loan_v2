<form id="frmNonLoanCase" name="frmNonLoanCase" action="<?php echo base_url().'non_loan_case/save'; ?>" method="post">
	<input type="hidden" id="txtNonLoanCaseId" name="txtNonLoanCaseId" value="<?php echo $id; ?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">สาเหตุ (กรณีไม่รับคำขอ)</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">สาเหตุ (กรณีไม่รับคำขอ) <span class="asterisk">*</span></label>
								<input type="text" name="txtNonLoanCaseName" id="txtNonLoanCaseName" class="form-control" value="<?php echo $data['non_loan_case_name']; ?>" maxlength="255" required />
							</div><!-- form-group -->
						</div><!-- col-sm-12 -->
					</div><!-- row -->
				</div><!-- panel-body -->
				<div class="panel-footer" style="text-align:right;">
					<button class="btn btn-primary">บันทึก</button>
				</div><!-- panel-footer -->
			</div><!-- panel -->
		</div><!-- col-md-12 --> 
	</div>
</form>