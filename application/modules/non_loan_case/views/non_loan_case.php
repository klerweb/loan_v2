<div class="row">
	<div class="col-md-12">
		<p class="nomargin" style="margin-bottom:20px; text-align:right;">
			<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo site_url('non_loan_case/form'); ?>'">เพิ่มสาเหตุ (กรณีไม่รับคำขอ)</button>
		</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped mb30">
				<thead>
					<tr>
		            	<th>#</th>
		            	<th>สาเหตุ (กรณีไม่รับคำขอ)</th>
		                <th>จัดการ</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					if(!empty($result))
					{
						$i = 1;
						foreach ($result as $row)
						{
					?>
					<tr>
						<td><?php echo $i++; ?></td>
		           		<td><?php echo $row['non_loan_case_name']; ?></td>
		           		<td>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แก้ไข" data-original-title="แก้ไข" onclick="location.href='<?php echo site_url('non_loan_case/form/'.$row['non_loan_case_id']); ?>'"><span class="fa fa-edit"></span></button>
		           			<button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="ยกเลิก" data-original-title="ยกเลิก" onclick="del('<?php echo $row['non_loan_case_id']; ?>')"><span class="fa fa-trash-o"></span></button>
		           		</td>
		           	</tr>
				<?php
						}	
					} 
				?>
				</tbody>
			</table>
		</div><!-- table-responsive -->
	</div>
</div>