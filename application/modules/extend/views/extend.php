<p class="nomargin" style="margin-bottom:20px; text-align:right;">
	<button type="button" class="btn btn-default btn-xs" onclick="location.href='<?php echo base_url()?>extend/form'">เพิ่มแบบบันทึก</button>
</p>
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-btns">
			<a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
		</div><!-- panel-btns -->
		<h4 class="panel-title">ค้นหาแบบแสดงความจำนงขอขยายระยะเวลาการชำระสินเชื่อ</h4>
	</div>
	<div class="panel-body">
		<form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo base_url()?>extend">
			<div class="form-group">
				<label class="sr-only" for="txtLand">เลขที่ขอสินเชื่อ</label>
				<input type="text" class="form-control" id="txtLoan" name="txtLoan" placeholder="เลขที่ขอสินเชื่อ" value="<?php echo $loan_code; ?>" />
			</div><!-- form-group -->
			<div class="form-group">
				<label class="sr-only" for="txtContact">เลขที่สัญญาเงินกู้</label>
				<input type="text" class="form-control" id="loan_contract_no" name="loan_contract_no" placeholder="เลขที่สัญญาเงินกู้" value="<?php echo $loan_contract_no; ?>" />
			</div><!-- form-group -->
			<div class="form-group">
				<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
				<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="<?php echo $thaiid; ?>" />
			</div><!-- form-group -->
			<div class="form-group">
				<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
				<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="<?php echo $fullname; ?>" />
			</div><!-- form-group -->
			<button type="submit" class="btn btn-default btn-sm">ค้นหา</button>
		</form>
	</div><!-- panel-body -->
</div><!-- panel -->
<div class="table-responsive">
	<table class="table table-striped mb30">
		<thead>
			<tr>
            	<th>#</th>
            	<th>เลขที่ขอสินเชื่อ</th>
                <th>ชื่อ-สกุลผู้ขอสินเชื่อ</th>
                <th>เลขที่สัญญาเงินกู้</th>
                <th>วันที่ขอขยายเวลาชำระหนี้</th>
                <th>จำนวนงวดที่ขอขยาย</th>
                <th>จำนวนเงินต่องวด</th>
                <th>จัดการ</th>
			</tr>
		</thead>
		<tbody>
		<?php 
			if(!empty($result))
			{
				$i = 1;
				foreach ($result as $row)
				{
					list($y, $m, $d) = explode('-', $row['extrequestdate']);
					$extrequestdate = $d.'/'.$m.'/'.(intval($y)+543);	
		?>
			<tr>
				<td><?php echo $i++; ?></td>
				<td><?php echo $row['loan_code']; ?></td>
				<td><?php echo $row['title_name'].$row['person_fname'].' '.$row['person_lname']; ?></td>
				<td><?php echo $row['loan_contract_no']; ?></td>
				<td><?php echo $extrequestdate; ?></td>
				<td><?php echo $row['period_new']; ?></td>
				<td><?php echo number_format($row['money_pay_new'],2); ?></td>
				<td>
		          <button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="พิมพ์" data-original-title="พิมพ์" onclick="window.open('<?php echo site_url('extend/word/'.$row['loan_id'].'/'.$row['loan_contract_id'].'/'.$row['extrequestdate']); ?>', '_blank')"><span class="fa fa-print"></span></button>
		          <button class="btn btn-default btn-xs tooltips" data-toggle="tooltip" data-placement="top" title="แสดง" data-original-title="แสดง" onclick="window.location = '<?php echo base_url().'extend/form/'.$row['loan_id'].'/'.$row['loan_contract_id'].'/'.$row['extrequestdate']?>'"><span class="fa fa-file-o"></span></button>	
		        </td>
           	</tr>
		<?php
				}	
			} 
		?>
		</tbody>
	</table>
</div><!-- table-responsive -->