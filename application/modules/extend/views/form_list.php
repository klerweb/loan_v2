<!-- form search -->
	<div class="row">
		<div class="col-sm-12">
			<h4>รายการสินเชื่อ</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
             <div class="table-responsive">
                 <table class="table mb30">
                 <thead>
                    <tr>
                    	<th></th>
                    	<th>#</th>
                    	<th>ชื่อ-สกุลผู้ขอสินเชื่อ</th>
		                <th>เลขที่สัญญาเงินกู้</th>
		                <th>วงเงินกู้</th>
		                <th></th>                   	
                    </tr>
                  </thead>
                  <tbody>
                  	<?php 
							$i = 1;
							foreach ($result as $row)
							{
					?>
						<tr>
							<td class="table-action">
								<input type="radio" value="<?php echo $row['loan_id'].'|'.$row['loan_contract_id']?>" name="rdRecord">
							</td>
							<td><?php echo $i++; ?></td>
							<td><?php echo $row['title_name'].$row['person_fname'].' '.$row['person_lname']; ?></td>
							<td><?php echo $row['loan_contract_no']; ?></td>
							<td><?php echo number_format($row['loan_contract_amount'],2); ?></td>
			           	</tr>
					<?php
							}	
					?>
                   </tbody>
                   </table>
            </div><!-- table-responsive -->
    	</div>
    </div>
