<div class="row">
<div class="col-md-12"> 
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">เพิ่มแบบแสดงความจำนงขอขยายระยะเวลาการชำระสินเชื่อ</h4>
	</div>
	<div class="panel-body">
	<?php if ($status == '200'){ ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $message; ?>
            </div>
    <?php } ?>
	
	<!-- form search -->
	<div class="row">
		<div class="col-sm-12">
			<h4>ค้นหารายการสินเชื่อ</h4>
		</div>
	</div>
	<div class="row">
			<div class="col-sm-2 form-group">
				<label class="sr-only" for="txtLand">เลขที่ขอสินเชื่อ</label>
				<input type="text" class="form-control" id="txtLoan" name="txtLoan" placeholder="เลขที่ขอสินเชื่อ" value="" />
			</div><!-- form-group -->
			<div class="col-sm-2 form-group">
				<label class="sr-only" for="txtContact">เลขที่สัญญาเงินกู้</label>
				<input type="text" class="form-control" id="loan_contract_no" name="loan_contract_no" placeholder="เลขที่สัญญาเงินกู้" value="" />
			</div><!-- form-group -->
			<div class="col-sm-3 form-group">
				<label class="sr-only" for="txtThaiid">บัตรประชาชน</label>
				<input type="text" class="form-control" id="txtThaiid" name="txtThaiid" placeholder="บัตรประชาชน" value="" />
			</div><!-- form-group -->
			<div class="col-sm-3 form-group">
				<label class="sr-only" for="txtFullname">ชื่อ - นามสกุล</label>
				<input type="text" class="form-control" id="txtFullname" name="txtFullname" placeholder="ชื่อ - นามสกุล" value="" />
			</div><!-- form-group -->
			<button type="button" id="btnSearch" class="btn btn-default btn-sm">ค้นหา</button>
	</div>
	<div class="row">
		<div class="col-sm-12">
	    	<hr/>
		</div>
	</div>
    
    <div id="form_extend">
	</div>  
	
	</div><!-- panel-body -->
	<div class="panel-footer right">
         <button id="btnSubmit" style="display:none" class="btn btn-primary">บันทึก</button>
	</div><!-- panel-footer -->
</div><!-- panel -->
</div><!-- col-md-6 --> 
</div>

<script>
jQuery(document).ready(function () {
	jQuery('#btnSearch').click(function (e) {
    	$('.alert').hide();
    	$('#btnSubmit').hide();
    	//reset form
    	$.ajax({type: "POST",
            url: "<?php echo base_url()?>extend/search",
            data: 'txtLoan='+$('#txtLoan').val()+'&loan_contract_no='+$('#loan_contract_no').val()+'&txtThaiid='+$('#txtThaiid').val()+'&txtFullname='+$('#txtFullname').val(),            
            success:function(result){
		       $("#form_extend").html(result);
		     }
   	 	});
    	
    });
	
	jQuery('input[name="rdRecord"]').live('change', function() {
	   	 var radioValue = $('input[name="rdRecord"]:checked').val();  
	   	 var r = radioValue.split('|');
	    	 var loan_id  = r[0];
	    	 var loan_contract_id  = r[1];   	    	
	     	 $.ajax({type: "POST",
	           url: "<?php echo base_url()?>extend/form_add/"+loan_id+"/"+loan_contract_id,
	           success:function(result){
			       $("#form_extend").html(result);
			       $('#btnSubmit').show();
			     }
	  	 	});
	     	
	   });
	   
	var loan_id = '<?php echo $loan_id?>';
	var loan_contract_id = '<?php echo $loan_contract_id?>';
	var extrequestdate = '<?php echo $extrequestdate?>';
	if(loan_id && loan_contract_id && extrequestdate){
		$.ajax({type: "POST",
	           url: "<?php echo base_url()?>extend/form_add/<?php echo $loan_id?>/<?php echo $loan_contract_id?>/<?php echo $extrequestdate?>",
	           success:function(result){
			       $("#form_extend").html(result);
			       $('#btnSubmit').hide();
			     }
	  	 	});
	}
});
</script>
