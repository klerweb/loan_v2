<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class forms extends MY_Controller {

    private $title = 'รายการแบบกู้ยืม';
    private $status = array('0'=>'','1'=>'แบบร่าง','2'=>'บันทึกเรียบร้อย');

    function __construct() {
        parent::__construct();
        $this->load->helper('date_helper');
        $this->load->helper('land_helper');
        $this->load->helper('loan_helper');
        $this->load->helper('currency_helper');
        $this->load->helper('common_helper');

         $this->load->library('pagination');
    }

    public function index($add='') {

        $this->load->model('forms_contract_model');
        $this->load->model("province/province_model");
        $this->load->model("amphur/amphur_model");
        $this->load->model("district/district_model");

        $page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
        $get_per_page = isset($_per_page) ? get_per_page($_per_page) : get_per_page();

        $post = $this->input->post();
        $status = '0';
        if($add != ""){
            $status = '1';
            $arr_loan = $this->forms_contract_model->get($this->input->post('person_card_id'), $this->input->post('person_name'),$status);

        }
        else{
            $status = '2';
            $arr_loan = $this->forms_contract_model->get($this->input->post('person_card_id'), $this->input->post('person_name'),$status);
        }

        if (count($arr_loan) > 0) {
            foreach ($arr_loan as $key => $val) {
                $arr_province = $this->province_model->get_by_id($val['person_addr_card_province_id']);
                $arr_amphur = $this->amphur_model->get_by_id($val['person_addr_card_amphur_id']);
                $arr_district = $this->district_model->get_by_id($val['person_addr_card_district_id']);

                if (count($arr_province) > 0)
                    $arr_loan[$key]['province_name'] = $arr_province['province_name'];
                if (count($arr_amphur) > 0)
                    $arr_loan[$key]['amphur_name'] = $arr_amphur['amphur_name'];
                if (count($arr_district) > 0)
                    $arr_loan[$key]['district_name'] = $arr_district['district_name'];
                $arr_loan[$key]['loan_status_txt'] = '';
                /*
                if($arr_loan[$key]['loan_status']=='2' && $add != ''){
                     $arr_loan[$key]['loan_status_txt'] = 'style="display:none;"';
                }
                 */
                $arr_loan[$key]['status'] = $this->status[$arr_loan[$key]['loan_status']];
            }
        }

        //split page
        $arr_page_data = array_chunk($arr_loan, $get_per_page, false);

//config pagging
        $total_rows = count($arr_loan);
        $config_pagination = config_pagination();
        $config_pagination['base_url'] = current_url();
        $config_pagination['total_rows'] = $total_rows;
        $config_pagination['per_page'] = $get_per_page;

        $this->pagination->initialize($config_pagination);
        $pagination = $this->pagination->create_links();
        $title_pagination = title_pagination($page, $get_per_page, $total_rows);

        $data_on_page = array();
        if (count($arr_page_data)) {
            $data_on_page = $arr_page_data[$page - 1];
        }
        $arr_data = array(
            'data' => $data_on_page,
            'pagging' => $pagination,
            'pagging_row' => $get_per_page,
            'visibled_add' => $status == '1' ? false : true
        );
        /*
          print('<pre>');
          print_r($arr_data);
          print('</pre>');
         */
        $data_menu['menu'] = 'forms';
        $data_breadcrumb['menu'] =  $status == '1'  ? array('นิติกรรม' => site_url('forms')) : array('นิติกรรม' => '#');

        $data = array(
            'title' => $status == '1'  ? 'เพิ่มแบบกู้ยืม' :  $this->title ,
            'css_other' => array('modules_loan.css'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'content' => $this->parser->parse('forms', $arr_data, TRUE)
        );
        $this->parser->parse('main', $data);
    }

    public function load_contract($loan_id) {
        $this->session->set_userdata("crt_loan_id", $loan_id);
        //unset loan_contract_id
        if ($this->session->has_userdata('crt_loan_contract_id'))
            $this->session->unset_userdata('crt_loan_contract_id');
        //unset loan_schedule_id
        if ($this->session->has_userdata('crt_loan_schedule_id'))
            $this->session->unset_userdata('crt_loan_schedule_id');

        if ($this->session->has_userdata('crt_schedule'))
            $this->session->unset_userdata('crt_schedule');

        if ($this->session->has_userdata('action'))
            $this->session->unset_userdata('action');

        redirect('forms/form_contract');
    }

    public function form_contract() {
        $this->load->model("forms_contract_model");
        $this->load->model("schedule_model");
        $this->load->model("province/province_model");
        $this->load->model("amphur/amphur_model");
        $this->load->model("district/district_model");
        $this->load->model('loan/loan_guarantee_land_model');
        $this->load->model('loan/loan_guarantee_bond_model');
        $this->load->model('loan/loan_guarantee_bookbank_model');
        $this->load->model('loan/loan_type_model');



        //$this->session->set_userdata("crt_loan_id",5);
        $loan_id = $this->session->userdata("crt_loan_id");
        //print($loan_id);
        $arr_person = $this->forms_contract_model->get_loan_guarantee($loan_id);
        if (count($arr_person) > 0) {
            $arr_province = $this->province_model->get_by_id($arr_person['person_addr_card_province_id']);
            $arr_amphur = $this->amphur_model->get_by_id($arr_person['person_addr_card_amphur_id']);
            $arr_district = $this->district_model->get_by_id($arr_person['person_addr_card_district_id']);

            if (count($arr_province) > 0)
                $arr_person['province_name'] = $arr_province['province_name'];
            if (count($arr_amphur) > 0)
                $arr_person['amphur_name'] = $arr_amphur['amphur_name'];
            if (count($arr_district) > 0)
                $arr_person['district_name'] = $arr_district['district_name'];

            if (!empty($arr_person['loan_analysis_length']))
                $arr_person['loan_analysis_length'] = $this->forms_contract_model->get_length($arr_person['loan_analysis_length']);
        }

        $arr_person['loan_analysis_amount'] = empty($arr_person['loan_analysis_amount']) ? 0.00 : $arr_person['loan_analysis_amount'];
        $arr_person['loan_analysis_amount'] = empty($arr_person['loan_analysis_interest']) ? 0.00 : $arr_person['loan_analysis_interest'];
        $arr_person['loan_analysis_approve_sector_amount'] = empty($arr_person['loan_analysis_approve_sector_amount']) ? 0.00 : $arr_person['loan_analysis_approve_sector_amount'];

        $arr_person['schedule'] = $this->schedule_model->get($loan_id);
        foreach ($arr_person['schedule'] as $key => $val) {
            $arr_person['schedule'][$key]['loan_analysis_interest'] = $arr_person['loan_analysis_amount'];
        }

        $arr_person['land_guarantee'] = $this->loan_guarantee_land_model->get_table_data(array('loan_id' => $loan_id));
        $arr_person['guarantee_bound'] = $this->loan_guarantee_bond_model->get_by_opt(array('loan_id' => $loan_id));
        $arr_person['guarantee_bank'] = $this->loan_guarantee_bookbank_model->report_list(array('loan_id' => $loan_id));
        $arr_person['loan_type'] = $this->loan_type_model->get_by_opt(array('loan_id' => $loan_id));


        if (!empty($arr_person['land_guarantee'])) {
            foreach ($arr_person['land_guarantee'] as $key => $val) {

                $arr_person['land_guarantee'][$key]['province_name'] = $this->province_model->get_by_id($val['land_addr_province_id'])['province_name'];
                $arr_person['land_guarantee'][$key]['amphur_name'] = $this->amphur_model->get_by_id($val['land_addr_amphur_id'])['amphur_name'];
                $arr_person['land_guarantee'][$key]['district_name'] = $this->district_model->get_by_id($val['land_addr_district_id'])['district_name'];
            }
        }

        //$this->session->set_userdata('action',array('action'=>'saved'));
        /*
        print('<pre>');
        print_r($arr_person);
        print('</pre>');
        */

        $this->session->set_userdata('crt_schedule', $arr_person['schedule']);
        $arr_contract = array(
            'data' => $arr_person,
            'action' => $this->session->userdata('action')
        );

        $data_menu['menu'] = 'forms';
        $data_breadcrumb['menu'] = array('นิติกรรม' => site_url('forms'));

        $data = array(
            'title' => 'แบบบันทึกกู้ยืมเงิน',
            'js_other' => array('modules_forms/forms_contract.js'),
            'menu' => $this->parser->parse('page/menu', $data_menu, TRUE),
            'breadcrumb' => $this->parser->parse('page/breadcrumb', $data_breadcrumb, TRUE),
            'content' => $this->parser->parse('form_loan_contract', $arr_contract, TRUE)
        );
        $this->parser->parse('main', $data);
    }

    public function save() {
        $this->load->model('contract_model');
        $this->load->model('payment_model');
        $this->load->model('loan_analysis_model');
        $this->load->helper('date_helper');

        $loan_id = $this->session->userdata('crt_loan_id');
        $contract_id = '';
        $schedule_id = '';

        if ($this->session->has_userdata('crt_loan_contract_id'))
            $contract_id = $this->session->userdata('crt_loan_contract_id');

        if ($this->session->has_userdata('crt_loan_schedule_id'))
            $schedule_id = $this->session->userdata('crt_loan_schedule_id');

        if ($loan_id != '') {
            //save loan amount
            $amount  = intval( str_replace(',','', $this->input->post('txt_loan_amount')));
            $this->loan_analysis_model->save($this->input->post('analysis_id'), array('loan_analysis_amount' => $amount));


            $arr_data = array(
                'loan_id' => $loan_id,
                'loan_contract_amount' =>$amount,
                //'loan_contract_no' => $this->input->post('txt_contract_no'),
                'loan_contract_location' => $this->input->post('txt_contract_location'),
                    //'loan_contract_date' => convert_ymd_en($this->input->post('txt_contract_date'))
            );
            $row = $this->contract_model->save($contract_id, $arr_data);
            if ($row['row'] > 0) {
                $contract_id = $row['id'];
                $ss_schedule = $this->session->userdata('crt_schedule');
                $inserted = 1;
                if (!empty($ss_schedule)) {
                    for ($i = 0; $i < count($ss_schedule); $i++) {
                        $arr_pay = array(
                            'loan_id' => $loan_id,
                            'interest_rate' => $ss_schedule[$i]['loan_analysis_interest'],
                            'date_payout' => $ss_schedule[$i]['loan_schedule_date'],
                            'loan_pay_number' => $i + 1,
                            'money_pay' => $ss_schedule[$i]['loan_schedule_amount'],
                            'loan_contract_id' => $contract_id,
                            'payment_interest' => !empty($ss_schedule[$i]['loan_schedule_payment_interest']) ? $ss_schedule[$i]['loan_schedule_payment_interest'] : '0.00'
                        );
                        $row = $this->payment_model->save($this->session->flashdata('ss_save_id'), $arr_pay);
                        if ($row == 0) {

                            $inserted = 0;
                        }
                    }
                }

                if ($inserted > 0) {
                    $this->session->set_flashdata('ss_save_id','1');
                    //update status = 2
                    $updateby = get_uid_login();
                    $this->contract_model->update_status($loan_id, $updateby);

                    $this->session->set_userdata('action', array(
                        'action' => 'saved',
                        'msg' => 'บันทึกข้อมูลเรียบร้อยx'
                            )
                    );
                } else {
                    //delete all row
                    if (!empty($contract_id)) {
                        $this->payment_model->del($contract_id);
                        $this->contract_model->del($contract_id);
                    }

                    $this->session->set_userdata('action', array(
                        'action' => 'error',
                        'msg' => 'ไม่สามารถบันทึกข้อมูลได้'
                            )
                    );
                }
            } else {
                $this->session->set_userdata('action', array(
                    'action' => 'error',
                    'msg' => 'ไม่สามารถบันทึกข้อมูลได้'
                        )
                );
            }
        }

        redirect('forms/form_contract');
    }

    function pdft() {
        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๑๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					ข้อตกลงต่อท้ายหนังสือสัญญาจำนอง
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="32%"></td>
				<td width="68%">ระหว่าง.................................................................................................. ระหว่าง </td>
			</tr>
			<tr>
				<td width="32%"></td>
				<td width="10%">กับ </td>
				<td width="45%" text-align="center">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
				<td width="10%">ผู้รับจำนอง </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="10%"></td>
				<td width="90%">(หนังสือสัญญาจำนองลงวันที่................................................................. ทำ ณ ................................................)</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑.&nbsp;ผู้จำนองเป็นเจ้าของกรรมสิทธิ์ที่ดิน ตามเลขเครื่องหมายที่ดินในสัญญาจำนองได้ตกลงจำนองที่ดินกับบรรดาตึก</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">โรงเรือน และสิ่งปลูกสร้างต่างๆ&nbsp;ซึ่งมีอยู่แล้วในที่ดินรายนี้รวมทั้งซึ่งจะได้ปลูกสร้างขึ้นใหม่ในภายหน้าในที่ดินรายนี้ทั้งสิ้นไว้แก่ </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="35%">ผู้รับจำนองเป็นประกันเงินซึ่งผู้จำนอง &nbsp;หรือ</td>
				<td width="30%">...........................................................</td>
				<td width="35%">เป็นหนี้ผู้รับจำนองอยู่แล้ว  และในเวลานี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">หรือในเวลาใดเวลาหนึ่งในภายหน้า&nbsp;ในเรื่องการกู้เงินและหนี้ในลักษณะอื่นๆ ที่เป็นหนี้แก่ผู้รับจำนองทั้งหมด&nbsp;ซึ่งรวมวงเงิน
</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="30%">....................................</td>
				<td  width="10%"> บาท (</td>
				<td width="40%">..........................................</td>
				<td  width="5%">)</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๒. ผู้จำนองยอมเสียดอกเบี้ยให้แก่ผู้รับจำนอง ในอัตราร้อยละ.................................................ต่อปี ในจำนวนเงินทั้งสิ้น</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ซึ่งผู้จำนองหรือ&nbsp;.........................................................&nbsp;เป็นหนี้ผู้รับจำนองนั้นเงินดอกเบี้ยนี้&nbsp;จะได้คิดในยอดเงินคงเหลือประจำวัน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ซึ่งปรากฏในบัญชีของผู้รับจำนอง&nbsp;และผู้จำนองยอมส่งเงินดอกเบี้ยให้แก่ผู้รับจำนองทุกๆ ..................... เสมอไป</td>
			</tr>
			<tr>
			<td width="100%">ถ้าผู้จำนองผิดนัดชำระดอกเบี้ยไม่น้อยกว่า&nbsp;๑&nbsp;ปีเป็นต้นไปผู้จำนองยินยอมให้ผู้รับจำนองคำนวณดอกเบี้ยที่ค้างชำระทบต้นในบัญชีของผู้จำนองต่อไป</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๓.&nbsp;ผู้จำนองสัญญาว่า&nbsp;ในระหว่างสัญญาจำนองนี้&nbsp;ถ้าผู้รับจำนองเห็นว่า ทรัพย์ที่จำนองนี้&nbsp;มีราคาตกต่ำลงไปกว่าในราคา</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ที่ทำจำนองนี้ และเรียกผู้จำนองให้นำทรัพย์อื่นมาจำนองเพิ่มอีกให้คุ้มพอ
กับจำนวนเงินที่ผู้จำนองเป็นหนี้ผู้รับจำนอง หรือให้คุ้มพอจำนวนเงินที่บุคคลอื่นเป็นหนี้ผู้รับจำนองซึ่งผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ดังกล่าว ผู้จำนองจะต้องจัดหาทรัพย์อื่นมาทำจำนองเพิ่มอีก
ให้คุ้มพอกับจำนวนหนี้ที่ผู้จำนองเป็นหนี้อยู่&nbsp;หรือคุ้มพอกับจำนวนหนี้ที่ผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ซึ่งบุคคลอื่นจะต้องชำระกับผู้รับจำนอง ถ้าผู้จำนองบิดพลิ้วไม่ยอมปฏิบัติ หรือไม่สามารถปฏิบัติตามความที่กล่าวมานี้ ผู้รับจำนองมีสิทธิจะเรียกให้ผู้จำนองชำระหนี้ และบังคับจำนองได้ทันที</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๔.	ถ้าทรัพย์ที่จำนองนี้บุบสลาย หรือต้องภัยอันตราย หรือสูญหายไป ซึ่งเป็นเหตุให้ทรัพย์นั้นเสื่อมราคาไม่เพียงพอ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">แก่การประกันหนี้ของผู้จำนอง หรือที่ผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ซึ่งบุคคลอื่นต้องชำระ&nbsp;ผู้จำนอง<br/>จะต้องเอาทรัพย์อื่นที่มีราคาเพียงพอมาจำนองเพิ่มให้คุ้มพอกับจำนวนหนี้ที่ผู้จำนองเป็นหนี้อยู่&nbsp;หรือคุ้มพอกับจำนวนหนี้ที่ผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ซึ่งบุคคลอื่นจะต้องชำระกับผู้รับจำนอง&nbsp;โดยไม่ชักช้า&nbsp;ถ้าผู้จำนองบิดพลิ้วไม่ยอม<br/>ปฏิบัติ หรือไม่สามารถปฏิบัติตามความที่กล่าวมานี้ ผู้รับจำนองมีสิทธิจะเรียกให้ผู้จำนองชำระหนี้ และบังคับจำนองได้ทันที</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๕.	เมื่อเวลาบังคับจำนองเอาทรัพย์สินซึ่งผู้จำนองได้จำนองไว้เป็นประกันหนี้แห่งตน โดยฟ้องคดีต่อศาลเพื่อให้พิพากษา</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">สั่งให้ยึดทรัพย์สินซึ่งจำนองและออกขายทอดตลาด หรือกรณีเรียกเอาทรัพย์หลุดจำนองแล้ว หรือกรณีภายหลังจากหนี้ถึงกำหนด<br/>ชำระถ้าไม่มีการจำนองรายอื่นหรือบุริมสิทธิอื่นอันได้จดทะเบียนจำนองไว้เหนือทรัพย์สินนี้แล้วผู้จำนองได้มีหนังสือไปยังผู้รับจำนองเพื่อให้ดำเนินการให้มีการขายทอดตลาดทรัพย์สินที่จำนองโดยไม่ต้องฟ้องเป็นคดีต่อศาล&nbsp;และผู้รับจำนองได้ดำเนินการขายทอด<br/>ตลาดทรัพย์นั้นภายในเวลา ๑ ปี นับแต่วันที่ได้รับหนังสือแจ้งแล้วได้เงินจำนวนสุทธิน้อยกว่า&nbsp;หรือราคาทรัพย์สินที่จำนองนี้ต่ำกว่า<br/>จำนวนเงินที่ค้างชำระกับค่าอุปกรณ์ต่างๆ ดังได้กล่าวมาแล้วนั้น&nbsp;เงินยังขาดจำนวนอยู่เท่าใด&nbsp;ผู้จำนองย่อมต้องรับผิดชอบรับใช้เงินที่ขาดจำนวนนั้นให้แก่ผู้รับจำนองจนครบถ้วน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%"></td>
				<td width="80%">ทั้งนี้ ในกรณีการบังคับจำนองเอาทรัพย์ซึ่งผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">อันบุคคลอื่นจะต้องชำระโดยวิธีการตามวรรคหนึ่งแล้วได้เงินจำนวนสุทธิน้อยกว่า&nbsp;หรือราคาทรัพย์ที่จำนองนี้ต่ำกว่าจำนวนเงิน<br/>ที่ค้างชำระกับค่าอุปกรณ์ต่างๆ&nbsp;ผู้จำนองซึ่งได้จำนองทรัพย์สินเพื่อเป็นประกันหนี้อันบุคคลอื่นจะต้องชำระไม่ต้องรับผิดชอบ<br/>ใช้เงินในจำนวนเงินส่วนที่ขาดแต่ให้ลูกหนี้รับใช้เงินในส่วนที่ขาดดังกล่าวให้แก่ผู้รับจำนองจนครบถ้วน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๖. ภายหลังที่ผู้รับจำนองเอาทรัพย์สินที่จำนองนี้หลุดเป็นสิทธิโดยประการใดๆ ก็ดี ปรากฏว่า มีหนี้เงินค่าภาษีอากรเกี่ยว</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">กับทรัพย์สินที่จำนองนี้ค้างอยู่สำหรับเวลาก่อนวันโอนมากน้อยเท่าใดและผู้รับจำนองต้องรับผิดชอบรับใช้เงินภาษีอากรนั้นผู้จำนองต้องรับผิดชอบรับใช้เงินค่าภาษีอากรนั้นให้แก่ผู้รับจำนอง&nbsp;พร้อมทั้งเงินอื่นๆ&nbsp;ที่ผู้รับจำนองต้องเสียไป&nbsp;เนื่องจากการที่ผู้รับจำนอง<br/>ต้องรับผิดชอบรับใช้เงินค่าภาษีอากรนั้น ผู้จำนองต้องรับผิดชอบรับใช้เงินค่าภาษีอากรนั้นให้แก่ผู้รับจำนองพร้อมทั้งเงินอื่นๆ ที่ผู้รับจำนองต้องเสียไปจากการที่ผู้รับจำนองต้องรับผิดชอบรับใช้เงินค่าภาษีอากรเช่นนั้นด้วยโดยครบถ้วน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๗.	ผู้จำนองสัญญาว่าจะรักษาซ่อมแซมตึก บ้านเรือน โรงเรือน และสิ่งปลูกสร้างต่างๆ ซึ่งมีอยู่ในที่ดินรายนี้แล้ว</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">หรือซึ่งจะได้ปลูกสร้างขึ้นต่อไปในภายหน้าให้มั่นคง เรียบร้อย และปกติดีอยู่เสมอตลอดระยะเวลาที่จำนองไว้แก่ผู้รับจำนอง โดยผู้จำนองจะต้องเสียค่ารักษาและซ่อมแซมเอง</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๘. ในกรณีผู้จำนองได้ทำประกันอัคคีภัยโรงเรือนหรือสิ่งปลูกสร้างซึ่งอยู่บนที่ดินที่จำนอง เป็นต้น ให้ผู้จำนองดำเนินการ</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">แจ้งผู้รับประกันภัย&nbsp;เพื่อแก้ไขการสลักหลังกรมธรรม์ประกันภัยโอนสิทธิที่จะรับค่าเสียหายจากบริษัทประกันภัยให้ผู้รับ<br/>จำนองต่อไป</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๙.	ในระหว่างที่ทรัพย์สินรายนี้ยังจำนองตามสัญญานี้อยู่ ผู้จำนองจะให้สิทธิประการใดๆ แก่ผู้อื่นในทรัพย์ที่จำนอง</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">เพื่อให้เกิดภาระผูกพัน เช่น เช่าถือ อาศัย ปลูกสร้าง ทางเดิน ยืม เป็นต้น อันเป็นการเสื่อมสิทธิ ลดสิทธิ รอนสิทธิ ทอนสิทธิ <br/>และเสียสิทธิของผู้จำนองเองในทรัพย์สินที่จำนองต่อผู้รับจำนอง&nbsp;ต้องได้รับความยินยอมอนุญาตจากผู้รับจำนองก่อนเป็นลาย<br/>ลักษณ์อักษร แต่ถ้าผู้จำนองได้ให้สิทธิประการใดๆ&nbsp;ข้างต้นแก่ผู้อื่นไว้แล้วในเวลาที่จำนองนี้&nbsp;ผู้จำนองต้องแจ้งให้ผู้รับจำนองทราบ<br/>เป็นลายลักษณ์อักษรทันที&nbsp;และถ้าผู้จำนองจะต่ออายุสิทธิที่ได้ให้ไว้แล้วออกไปอีก&nbsp;ผู้จำนองจะต้องได้รับความยินยอมเป็นลายลักษณ์อักษรจากผู้รับจำนองก่อน&nbsp;การกระทำใดๆ&nbsp;ที่ผู้จำนองได้กระทำฝ่าฝืน&nbsp;ขัดขืนต่อสัญญาข้อนี้&nbsp;จะไม่ผูกพันผู้รับจำนองด้วย<br/>ประการใดๆ&nbsp;ผู้รับจำนองมีสิทธิที่จะปฏิเสธการกระทำของผู้จำนองนั้นได้ทันที&nbsp;อีกทั้งผู้จำนองมีสิทธิจะบังคับจำนอง&nbsp;และเสียค่าเสียหายได้ด้วย</td>
			</tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๐. ถ้ามีปัญหาเกิดขึ้นเกี่ยวกับกรรมสิทธิ์ของผู้จำนองในที่ดินและทรัพย์สินที่จำนองเมื่อใด ผู้รับจำนองมีสิทธิจะเรียกให้</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ผู้จำนอง และเรียกค่าเสียหายได้ด้วย</td>
			</tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๑. ถ้าผู้จำนองประพฤติผิดหรือไม่ประพฤติตามสัญญาที่บันทึกไว้ข้างบนนี้แต่ข้อหนึ่งข้อใด หรือทั้งหมดผู้รับจำนอง</td>
			</tr>
		</table>

                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๒. ค่าธรรมเนียมและค่าใช้จ่ายทั้งสิ้นในการจำนองและไถ่ถอนจำนอง ผู้จำนองเป็นผู้เสียเอง</td>
			</tr>
		</table>
		';

       $htmlcontent_p2 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">เพื่อเป็นหลักฐานแห่งสัญญา คู่สัญญาได้ตรวจหนังสือต่อหน้าเจ้าพนักงานแล้ว รับรองว่าถูกต้องตามความประสงค์ทุกข้อ จึงได้พร้อมกันลงลายมือชื่อไว้เป็นสำคัญ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ....................................................  ผู้จำนอง</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... ผู้จำนอง</td>
			</tr>

			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... พยาน</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... พยาน</td>
			</tr>

			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>

		</table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="100%">หนังสือสัญญาฉบับนี้ได้ทำต่อหน้า</td>
                                </tr>
                        </table>
                         <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="50%"></td>
                                        <td width="50%" style="text-align:center;">ลงชื่อ ...................................................... เจ้าพนักงานที่ดิน</td>
                                </tr>

                                <tr>
                                        <td width="50%"></td>
                                        <td width="50%" style="text-align:center;">ประทับตราประจำตำแหน่งเป็นสำคัญ</td>
                                </tr>
                        </table>
		';


        ob_start();
        $this->load->library('tcpdf');


        $filename = 'หนังสือกู้ยืมเงิน_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->AddPage();

        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');

        $pdf->SetMargins(10, 10, 10, true);
        $pdf->AddPage();

        $pdf->writeHTML($htmlcontent_p2, true, 0, true, true, '');

        $pdf->Output($filename . '.pdf', 'I');
    }

    public function pdf($id) {
        //37 row per page use footer 12 row

        $this->load->model("forms_contract_model");
        $this->load->model("schedule_model");
        $this->load->model("province/province_model");
        $this->load->model("amphur/amphur_model");
        $this->load->model("district/district_model");
        $this->load->model('loan/loan_guarantee_land_model');
        $this->load->model('loan/loan_guarantee_bond_model');
        $this->load->model('loan/loan_guarantee_bookbank_model');
        $this->load->model('loan/loan_type_model');
        $this->load->model('loan_analysis_cal_model', 'loan_analysis_cal');

        $arr_person = $this->forms_contract_model->get_loan_guarantee($id);
        if (count($arr_person) > 0) {
            $arr_province = $this->province_model->get_by_id($arr_person['person_addr_card_province_id']);
            $arr_amphur = $this->amphur_model->get_by_id($arr_person['person_addr_card_amphur_id']);
            $arr_district = $this->district_model->get_by_id($arr_person['person_addr_card_district_id']);

            if (count($arr_province) > 0)
                $arr_person['province_name'] = $arr_province['province_name'];
            if (count($arr_amphur) > 0)
                $arr_person['amphur_name'] = $arr_amphur['amphur_name'];
            if (count($arr_district) > 0)
                $arr_person['district_name'] = $arr_district['district_name'];

            if (!empty($arr_person['loan_analysis_length']))
                $arr_person['loan_analysis_length'] = $this->forms_contract_model->get_length($arr_person['loan_analysis_length']);
        }

        $arr_person['loan_analysis_amount'] = empty($arr_person['loan_analysis_amount']) ? 0.00 : $arr_person['loan_analysis_amount'];
        $arr_person['loan_analysis_amount'] = empty($arr_person['loan_analysis_interest']) ? 0.00 : $arr_person['loan_analysis_interest'];

        $arr_person['schedule'] = $this->schedule_model->get($id);
        foreach ($arr_person['schedule'] as $key => $val) {
            $arr_person['schedule'][$key]['loan_analysis_interest'] = $arr_person['loan_analysis_amount'];
        }

        $arr_person['land_guarantee'] = $this->loan_guarantee_land_model->get_table_data(array('loan_id' => $id));
        $arr_person['guarantee_bound'] = $this->loan_guarantee_bond_model->get_by_opt(array('loan_id' => $id));
        $arr_person['guarantee_bank'] = $this->loan_guarantee_bookbank_model->report_list(array('loan_id' => $id));
        $arr_person['loan_type'] = $this->loan_type_model->get_by_opt(array('loan_id' => $id));


        if (!empty($arr_person['land_guarantee'])) {
            foreach ($arr_person['land_guarantee'] as $key => $val) {

                $arr_person['land_guarantee'][$key]['province_name'] = $this->province_model->get_by_id($val['land_addr_province_id'])['province_name'];
                $arr_person['land_guarantee'][$key]['amphur_name'] = $this->amphur_model->get_by_id($val['land_addr_amphur_id'])['amphur_name'];
                $arr_person['land_guarantee'][$key]['district_name'] = $this->district_model->get_by_id($val['land_addr_district_id'])['district_name'];
            }
        }


        //display one row if average loan
        $tmp = 0;
        $set_row = FALSE;
        for ($i = 0; $i < count($arr_person['schedule']); $i++) {
            if($tmp==0){
                $tmp = $arr_person['schedule'][$i]['loan_schedule_amount'];
            }
            else{
                if($tmp != $arr_person['schedule'][$i]['loan_schedule_amount']){
                    $set_row = true;
                }
            }
        }
         $pay1 = '';
        $pay = '';

            for ($i = 0; $i < 6; $i++) {
                $pay = $pay . '<tr>';
                $pay = $pay . '<td align="center">' . num2Thai($arr_person['schedule'][$i]['loan_schedule_times']) . '</td>';
                $pay = $pay . '<td>' . num2Thai(thai_display_date($arr_person['schedule'][$i]['loan_schedule_date'])) . '</td>';
                $pay = $pay . '<td align="right">' . num2Thai(number_format($arr_person['schedule'][$i]['loan_schedule_amount'], 2)) . '</td>';
                $pay = $pay . '<td>' . num2Thai(thai_display_date($arr_person['schedule'][$i]['loan_schedule_date'])) . '</td>';
                $pay = $pay . '</tr>';
            }

            $pay1 = '';
            for ($i = 6; $i < count($arr_person['schedule']); $i++) {
                $pay1 = $pay1 . '<tr>';
                $pay1 = $pay1 . '<td align="center">' . num2Thai($arr_person['schedule'][$i]['loan_schedule_times']) . '</td>';
                $pay1 = $pay1 . '<td>' . num2Thai(thai_display_date($arr_person['schedule'][$i]['loan_schedule_date'])) . '</td>';
                $pay1 = $pay1 . '<td align="right">' . num2Thai(number_format($arr_person['schedule'][$i]['loan_schedule_amount'], 2)) . '</td>';
                $pay1 = $pay1 . '<td>' . num2Thai(thai_display_date($arr_person['schedule'][$i]['loan_schedule_date'])) . '</td>';
                $pay1 = $pay1 . '</tr>';
            }

            $pay2 = '';
            $loan_analysis_list_cal = $this->loan_analysis_cal->getByLoan($id);

            if(!empty($loan_analysis_list_cal))
            {
            	foreach ($loan_analysis_list_cal as $list_cal)
            	{
            		$pay2 .= '<tr>
            					<td align="center">' .num2Thai($list_cal['loan_analysis_cal_start']).'</td>
            					<td align="center">'.num2Thai($list_cal['loan_analysis_cal_end']).'</td>
            					<td align="right">' . num2Thai(number_format($list_cal['loan_analysis_cal_amount'], 2)) . '</td>
            				</tr>';
            	}
            }

            $amount = 0.00;
            for ($i = 0; $i < count($arr_person['schedule']); $i++) {
                /*$pay2 = $pay2 . '<tr>';
                $pay2 = $pay2 . '<td align="center">' . num2Thai($i + 1) . '</td>';
                $pay2 = $pay2 . '<td align="center">' . num2Thai($arr_person['schedule'][$i]['loan_schedule_times']) . '</td>';
                $pay2 = $pay2 . '<td align="right">' . num2Thai(number_format($arr_person['schedule'][$i]['loan_schedule_amount'], 2)) . '</td>';
                $pay2 = $pay2 . '</tr>';*/

                $amount += $arr_person['schedule'][$i]['loan_schedule_amount'];
            }

        $avg = number_format($amount / count($arr_person['schedule']));

        $land = '';
        foreach ($arr_person['land_guarantee'] as $val) {

            $addr = 'จังหวัด' . $val['province_name'] . ' อำเภอ' . $val['amphur_name'] . ' ตำบล' . $val['district_name'];

            $land = $land . '<tr>';
            $land = $land . '<td>' . $val['land_type_name'] . '</td>';
            $land = $land . '<td>' . num2Thai($val['land_no']) . '</td>';
            $land = $land . '<td>' . num2Thai($val['land_addr_no']) . '</td>';
            $land = $land . '<td>' . $addr . '</td>';
            $land = $land . '<td>' . num2Thai($val['land_area_rai']) . ' ไร่ ' . num2Thai($val['land_area_ngan']) . ' งาน ' . num2Thai($val['land_area_wah']) . ' ตารางวา</td>';
            $land = $land . '<td>' . $val['title_name'] . $val['person_fname'] . ' ' . $val['person_lname'] . '</td>';
            $land = $land . '</tr>';
        }


        $bond = '';
        foreach ($arr_person['guarantee_bound'] as $val) {
            $bond = $bond . '<tr>';
            $bond = $bond . '<td>' . $val['loan_bond_owner'] . '</td>';
            $bond = $bond . '<td>' . num2Thai($val['loan_bond_thaiid']) . '</td>';
            $bond = $bond . '<td>' . $val['loan_bond_type'] . '</td>';
            $bond = $bond . '<td>' . num2Thai($val['loan_bond_startnumber']) . '</td>';
            $bond = $bond . '<td>' . num2Thai($val['loan_bond_endnumber']) . '</td>';
            $bond = $bond . '<td>' . num2Thai($val['loan_bond_amount']) . '</td>';
            $bond = $bond . '</tr>';
        }


        $bank = '';
        foreach ($arr_person['guarantee_bank'] as $val) {
            $bank = $bank . '<tr>';
            $bank = $bank . '<td>' . $val['loan_bookbank_owner'] . '</td>';
            $bank = $bank . '<td>' . $val['bank_name'] . '</td>';
            $bank = $bank . '<td>' . $val['loan_bookbank_no'] . '</td>';
            $bank = $bank . '<td>' . $val['loan_bookbank_type_name'] . '</td>';
            $bank = $bank . '<td>' . num2Thai(number_format($val['loan_bookbank_amount'], 2)) . '</td>	';
            $bank = $bank . '</tr>';
        }


        /*
          print('<pre>');
          print_r($arr_person);
          print('</pre>');
          exit;
         */

        ob_start();
        $this->load->library('tcpdf');


        $filename = 'หนังสือกู้ยืมเงิน_' . date('Ymd');

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Kler-web'); // ชื่อผู้สร้างไฟล์ PDF
        $pdf->SetTitle($filename); //  กำหนด Title
        $pdf->SetSubject('Export receipt'); // กำหนด Subject
        $pdf->SetKeywords($filename); // กำหนด Keyword

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // add a page
        $pdf->SetFont('thsarabun', '', 16);
        $pdf->SetMargins(10, 10, 10, true);
        $pdf->AddPage();

        $htmlcontent = '
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๕</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					หนังสือกู้ยืมเงิน
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">หนังสือกู้ยืมเงินเพื่อขอใช้สินเชื่อประเภท&nbsp;&nbsp;' . $arr_person['loan_type'][0]['loan_objective_name'] . '</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="4">
			<tr>
				<td width="5%">ทำที่ </td>
				<td width="95%">' .$this->replace_space($arr_person['loan_location']) . '&nbsp;&nbsp;&nbsp;สัญญาเลขที่.............................&nbsp;&nbsp;&nbsp;วันที่...........................................</td>
			</tr>
			<tr>
				<td colspan="2"></td>

			</tr>
		</table>

		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="40%">ข้าพเจ้า' . $arr_person['title_name'] . ' ' . $arr_person['person_fname'] . ' ' . $arr_person['person_lname'] . '</td>
				<td width="15%">อายุ  &nbsp;&nbsp;' . num2Thai(age($arr_person['person_birthdate'])) . '&nbsp;&nbsp;ปี </td>
				<td width="40%">เลขประจำตัวประชาชน &nbsp;&nbsp;' . num2Thai(formatThaiid($arr_person['person_thaiid'])) . '</td>
			</tr>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="30%">ที่อยู่ปัจจุบัน เลขที่ &nbsp;&nbsp;' . num2Thai($arr_person['person_addr_pre_no']) . ' </td>
				<td width="15%">หมู่ที่ &nbsp;&nbsp;' . num2Thai($this->ReplaceEmpty($arr_person['person_addr_pre_moo'])) . '</td>
				<td width="25%">ถนน  &nbsp;&nbsp; ' . $this->ReplaceEmpty($arr_person['person_addr_pre_road']) . '</td>
				<td width="30%">ตำบล/แขวง &nbsp;&nbsp;' . $arr_person['district_name'] . ' </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%">อำเภอ/เขต  &nbsp;' . $arr_person['amphur_name'] . '</td>
				<td width="20%">จังหวัด&nbsp;' . $arr_person['province_name'] . '</td>
				<td width="30%">โทรศัพท์บ้าน&nbsp;' . num2Thai($this->ReplaceEmpty($arr_person['person_phone'])) . '</td>
				<td width="30%">โทรศัพท์มือถือ&nbsp;' . num2Thai($this->ReplaceEmpty($arr_person['person_mobile'])) . '</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
			</tr>
			<tr>
				<td>ซึ่งต่อไปนี้ในหนังสือกู้ยืมเงินจะเรียกว่า "ผู้กู้" ขอทำหนังสือกู้ยืมเงินให้ไว้แก่สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน) (บจธ.)  ซึ่งต่อไปนี้จะเรียกว่า "บจธ." และ/หรือ "ผู้ให้กู้" เพื่อเป็นหลักฐาน ดังต่อไปนี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
			</tr>
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑.&nbsp;ข้าพเจ้าได้กู้ยืมเงินจาก บจธ. เป็นจำนวนเงิน &nbsp;&nbsp;' . num2Thai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2)) . ' &nbsp;&nbsp;(' . num2Thai(num2wordsThai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2))) . ')</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">โดยมีวัตถุประสงค์เพื่อ&nbsp;&nbsp;' . $this->replace_space(num2Thai($arr_person['loan_desc'])). '&nbsp;และผู้กู้ได้รับเงินจำนวนดังกล่าวจาก&nbsp;บจธ.&nbsp;โดยถูกต้องครบถ้วนตามสัญญานี้แล้ว</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๒. ข้าพเจ้าขอสัญญาว่าจะนำเงินกู้ยืมนี้ไปใช้ตามวัตถุประสงค์ที่ระบุไว้ในข้อ ๑ </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๓. การรับชำระหนี้ของข้าพเจ้าซึ่งเป็นผู้กู้ตกลงชำระคืนต้นเงินกู้และดอกเบี้ยตามหนังสือกู้ยืมเงินฉบับนี้โดย</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ครบถ้วนเป็นงวดๆ ตามข้อ ๓.๑ และ/หรือ ๓.๓ โดย บจธ. ซึ่งเป็นผู้ให้กู้ตกลงออกใบชำระหนี้ให้แก่ผู้กู้ เพื่อหลักฐานการชำระ</td>
			</tr>
			<tr>
				<td width="100%">คืนต้นเงินและดอกเบี้ยทุกงวด ดังนี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="12%"></td>
				<td width="98%">๓.๑ ข้าพเจ้าตกลงชำระหนี้ในแต่ละงวด ดังนี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
			</tr>
		</table>
		<table width="100%" border="1" cellpadding="2" cellspacing="0">
			<tr>
				<td width="10%" align="center">งวดที่</td>
				<td width="30%" align="center">กำหนดชำระภายในวันที่</td>
				<td width="30%" align="center">กำหนดชำระเงินต้น (บาท)</td>
				<td width="30%" align="center">ชำระดอกเบี้ยถึงวันที่</td>
			</tr>
			' . $pay . '
		</table>
		';


        $pdf->writeHTML($htmlcontent, true, 0, true, true, '');

        $pdf->AddPage();

        $htmlcontent = '
		<table width="100%" border="1" cellpadding="2" cellspacing="0">
			<tr>
				<td width="10%" align="center">งวดที่</td>
				<td width="30%" align="center">กำหนดชำระภายในวันที่</td>
				<td width="30%" align="center">กำหนดชำระเงินต้น (บาท)</td>
				<td width="30%" align="center">ชำระดอกเบี้ยถึงวันที่</td>
			</tr>
			' . $pay1 . '
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td width="5%"></td>
				<td width="95%">๓.๒ ข้าพเจ้าตกลงชำระหนี้ตามหนังสือกู้ยืมเงินฉบับนี้ ตามข้อ ๑ เป็นงวดราย   &nbsp;&nbsp;' . $arr_person['loan_analysis_length'] . '  &nbsp;&nbsp;  รวม &nbsp;&nbsp; ' . num2Thai(count($arr_person['schedule'])) . '&nbsp;&nbsp;งวด ดังนี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%"></td>
				<td width="60%">
				<table width="100%" border="1" cellpadding="2" cellspacing="0">
					<tr>
						<td width="20%" align="center">งวดที่</td>
						<td width="20%" align="center">ถึงงวดที่</td>
						<td width="60%" align="center">กำหนดชำระเงินต้น (บาท)</td>
					</tr>
					' . $pay2 . '
				</table>
				</td>
				<td width="20%"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>โดยกำหนดชำระต้นเงินและดอกเบี้ยงวดแรกภายในวันที่ ' . num2Thai(thai_display_date($arr_person['schedule'][0]['loan_schedule_date'])) . '</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="12%"></td>
				<td width="88%">๓.๓ ข้าพเจ้าตกลงชำระคืนต้นเงินกู้พร้อมดอกเบี้ยเป็นงวดราย' . $arr_person['loan_analysis_length'] . '&nbsp;โดยเฉลี่ย ไม่ต่ำกว่า ' . num2Thai($avg) . '  บาท&nbsp;รวม ' . num2Thai(count($arr_person['schedule'])) . ' งวด</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">นับแต่วันถัดจากวันรับเงินกู้ โดยเริ่มชำระงวดแรกในวันที่&nbsp;&nbsp;' . num2Thai($this->replace_space(thai_display_date($arr_person['schedule'][0]['loan_schedule_date']))) . '&nbsp;และงวดสุดท้ายชำระต้นเงินคงเหลือ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>ทั้งหมดพร้อมดอกเบี้ย</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๔. ข้าพเจ้ายอมชำระค่าธรรมเนียมและค่าใช้จ่ายในการอำนวยสินเชื่อของ บจธ. ตามอัตราที่ บจธ. กำหนด </td>
			</tr>
			<tr>
				<td width="100%">และยินยอมชำระดอกเบี้ยตามหนังสือหนังสือกู้ฉบับนี้ ในอัตราร้อยละ ' . num2Thai(number_format($arr_person['loan_analysis_interest'], 2)) . ' &nbsp; ต่อปี</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">นับแต่วันถัดจากวันรับเงินกู้ และ/หรือวันที่ใช้หนังสือกู้ยืมเงินฉบับนี้จนกว่าจะชำระคืนเสร็จ  ต่อไปภายหน้า หาก บจธ. </td>
			</tr>
			<tr>
				<td colspan="2">กำหนดอัตราดอกเบี้ยเพิ่มขึ้นหรือลดลงจากที่กำหนดไว้เดิมแต่ไม่เกินอัตราที่กฎหมายกำหนดข้าพเจ้ายอมชำระดอกเบี้ยใน</td>
			</tr>
			<tr>
				<td colspan="2">อัตราที่กำหนดขึ้นใหม่</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๕. ถ้าข้าพเจ้าตกเป็นผู้ผิดสัญญาในข้อหนึ่งข้อใด ข้าพเจ้ายอมรับผิดชอบชดใช้ค่าเสียหาย เนื่องจากการผิดสัญญา</td>
			</tr>
			<tr>
				<td colspan="2">ให้แก่ บจธ. รวมทั้งค่าใช้จ่ายในการเตือน เรียกร้องทวงถาม ดำเนินคดี และบังคับชำระหนี้เงินกู้ตามสัญญานี้จนเต็มจำนวนทุก</td>
			</tr>
			<tr>
				<td colspan="2">อย่างทุกประการ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๖. ข้าพเจ้ายินยอมถือว่ากรณีใดๆ ดังกล่าว ในข้อบังคับของ บจธ. ที่กำหนดไว้ก็ดี หรือในกรณีที่ข้าพเจ้าปฏิบัติ</td>
			</tr>
			<tr>
				<td colspan="2">ผิดสัญญาข้อหนึ่งข้อใดก็ดี เงินกู้รายนี้เป็นอันถึงกำหนดส่งคืนโดยสิ้นเชิง พร้อมทั้งดอกเบี้ยในทันที โดยมิพักคำนึงถึงกำหนด</td>
			</tr>
			<tr>
				<td colspan="2">เวลาที่ตกลงไว้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๗.	ในการกู้ยืมเงินครั้งนี้ ข้าพเจ้าได้นำหลักทรัพย์ ได้แก่ </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
			</tr>
		</table>
		';
        if (!empty($land)) {
            $htmlcontent = $htmlcontent . '
		<table width="100%" border="1" cellpadding="2" cellspacing="0">
			<tr>
				<th  align="center">ประเภทเอกสารสิทธิ</th>
				<th  align="center">เลขที่</th>
				<th  align="center">เลขที่ดิน</th>
				<th  align="center">ที่ตั้ง</th>
				<th  align="center">เนื้อที่</th>
				<th  align="center">ผู้ถือกรรมสิทธิ</th>
			</tr>
				' . $land . '
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
			</tr>
		</table>
		';
        }

        if (!empty($bond)) {
            $htmlcontent = $htmlcontent . '
		<table width="100%" border="1" cellpadding="2" cellspacing="0">
			<tr>
				<th  align="center">เจ้าของกรรมสิทธิ์</th>
				<th  align="center">เลขประจำตัวประชาชน</th>
				<th  align="center">ชนิดพันธบัตร</th>
				<th  align="center">เลขที่ต้น</th>
				<th  align="center">เลขที่ท้าย</th>
				<th  align="center">ราคา (บาท)</th>
			</tr>
			' . $bond . '
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
			</tr>
		</table>
		';
        }

        if (!empty($bank)) {
            $htmlcontent = $htmlcontent . '
		<table width="100%" border="1" cellpadding="2" cellspacing="0">
			<tr>
				<th  align="center">เจ้าของบัญชี</th>
				<th  align="center">ธนาคาร/สถาบันการเงิน/สหกรณ์</th>
				<th  align="center">เลขที่บัญชี</th>
				<th  align="center">ประเภทบัญชี</th>
				<th  align="center">จำนวนเงิน(บาท)</th>
			</tr>
			' . $bank . '
		</table>
		';
        }

        $htmlcontent = $htmlcontent . '
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr><td width="100%">มาจำนอง/จำนำไว้เพื่อเป็นหลักค้ำประกันเงินกู้ของข้าพเจ้า</td></tr>
                                    <tr><td width="100%"></td></tr>
                                    </table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๘.	ข้าพเจ้าสัญญาว่า จะบำรุงรักษา และซ่อมแซมบรรดาสิ่งปลูกสร้างในที่ดินที่จำนองรายนี้ ให้อยู่ในสภาพเดิม</td>
			</tr>
			<tr>
				<td colspan="2">และมั่นคง เรียบร้อย เป็นปกติอยู่เสมอ ถ้าข้าพเจ้าจะทำการเปลี่ยนแปลง ต่อเติม หรือรื้อถอนบรรดาสิ่งปลูกสร้างอย่างใด</td>
			</tr>
			<tr>
				<td colspan="2">ข้าพเจ้าจะต้องได้รับความยินยอมเป็นหนังสือจาก บจธ.</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๙.	บรรดาข้อสัญญาต่างๆ ที่ระบุอยู่ในสัญญาจำนองและข้อตกลงต่อท้ายสัญญาจำนองที่ข้าพเจ้าทำไว้
กับ บจธ. </td>
			</tr>
			<tr>
				<td colspan="2">ข้าพเจ้ารับรู้และถือว่าเป็นเสมือนหนึ่งแห่งข้อสัญญานี้ มีผลผูกพันที่ข้าพเจ้าจะต้องปฏิบัติตาม</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๐.	ถ้าข้าพเจ้าจะขอกู้ยืมเงินจากผู้อื่นในระหว่างที่ยังมีหนี้เงินกู้อยู่กับ บจธ. จักต้องได้รับอนุญาตเป็นหนังสือจาก บจธ.</td>
			</tr>
			<tr>
				<td colspan="2">ก่อน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๑. ข้าพเจ้ารับผูกพันและปฏิบัติตามข้อบังคับและระเบียบของ บจธ. เกี่ยวกับการให้กู้เงินทั้งที่ใช้อยู่ในปัจจุบันและ</td>
			</tr>
			<tr>
				<td colspan="2">ที่แก้ไขเพิ่มเติมในภายหน้าทุกประการ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้าพเจ้าเข้าใจข้อความหนังสือกู้ยืมเงินฉบับนี้โดยตลอดแล้ว จึงลงลายมือชื่อไว้เป็นสำคัญต่อหน้าพยาน</td>
			</tr>

		</table>
		';

        $pdf->writeHTML($htmlcontent, true, 0, true, 0, '');


        if ($pdf->GetY() > 200) {

            $pdf->AddPage();
        }


        $htmlcontent = '
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ   ผู้กู้</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... สามี/ภรรยาผู้ให้ความยินยอม</td>
			</tr>

			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... พยาน</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... พยาน</td>
			</tr>

			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>

		</table>
		';


        $pdf->writeHTML($htmlcontent, true, 0, true, 0, '');

        $htmlcontent = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" style="text-align:center;"><br/><br/><img src="assets\images\logo_invoice.png" style="width:80px;"></td>
				<td width="80%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="text-align:right; color:#aaa;">บจธ. สช. - ๑๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-weight: bold;">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">เลขที่ ๒๑๐ สถานีวิทยุโทรทัศน์กองทัพบก อาคารเบญจสิริ ถนนพหลโยธิน แขวงสามเสนใน เขตพญาไท กรุงเทพฯ ๑๐๔๐๐</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">โทร ๐ ๒๒๗๘ ๑๖๔๘ โทรสาร ๐ ๒๒๗๘ ๑๑๔๘</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">The Land Bank Administration Institute (Public Organization)</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">210 Royal Thai Army Radio And Television Station, Benchasiri Building, Phahonyothin Rd., Samsen Nai,Phaya Thai, Bangkok 10400</td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 12px;">Tel. 0 2278 1648 Fax 0 2278 1148 <u>www.labai.or.th</u></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="100%" style="font-size: 10px;"></td>
						</tr>
					</table>
					<hr />
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%" style="font-weight: bold; text-align:center;">
					ข้อตกลงต่อท้ายหนังสือสัญญาจำนอง
				</td>
			</tr>
			<tr>
				<td width="100%" style="font-size: 10px;"></td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="32%"></td>
				<td width="68%">ระหว่าง ' . $arr_person['title_name'] . ' ' . $arr_person['person_fname'] . ' ' . $arr_person['person_lname'] . ' ผู้จำนอง </td>
			</tr>
			<tr>
				<td width="32%"></td>
				<td width="10%">กับ </td>
				<td width="45%" text-align="center">สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</td>
				<td width="10%">ผู้รับจำนอง </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="10%"></td>
				<td width="90%">(หนังสือสัญญาจำนองลงวันที่................................................................. ทำ ณ ................................................)</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑.&nbsp;ผู้จำนองเป็นเจ้าของกรรมสิทธิ์ที่ดิน ตามเลขเครื่องหมายที่ดินในสัญญาจำนองได้ตกลงจำนองที่ดินกับบรรดาตึก</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">โรงเรือน และสิ่งปลูกสร้างต่างๆ&nbsp;ซึ่งมีอยู่แล้วในที่ดินรายนี้รวมทั้งซึ่งจะได้ปลูกสร้างขึ้นใหม่ในภายหน้าในที่ดินรายนี้ทั้งสิ้นไว้แก่ </td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="35%">ผู้รับจำนองเป็นประกันเงินซึ่งผู้จำนอง &nbsp;หรือ</td>
				<td width="30%">' . $arr_person['title_name'] . ' ' . $arr_person['person_fname'] . ' ' . $arr_person['person_lname'] . '</td>
				<td width="35%">เป็นหนี้ผู้รับจำนองอยู่แล้ว  และในเวลานี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">หรือในเวลาใดเวลาหนึ่งในภายหน้า&nbsp;ในเรื่องการกู้เงินและหนี้ในลักษณะอื่นๆ ที่เป็นหนี้แก่ผู้รับจำนองทั้งหมด&nbsp;ซึ่งรวมวงเงิน
</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%"> '.num2Thai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2)).'&nbsp;บาท ('.num2Thai(num2wordsThai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2))).')</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๒. ผู้จำนองยอมเสียดอกเบี้ยให้แก่ผู้รับจำนอง ในอัตราร้อยละ &nbsp;&nbsp; '.num2Thai(number_format($arr_person['loan_analysis_interest'], 2)) .'&nbsp;&nbsp;ต่อปี ในจำนวนเงินทั้งสิ้น</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ซึ่งผู้จำนองหรือ&nbsp;' . $arr_person['title_name'] . ' ' . $arr_person['person_fname'] . ' ' . $arr_person['person_lname'] . '&nbsp;เป็นหนี้ผู้รับจำนองนั้นเงินดอกเบี้ยนี้&nbsp;จะได้คิดในยอดเงินคงเหลือประจำวัน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ซึ่งปรากฏในบัญชีของผู้รับจำนอง&nbsp;และผู้จำนองยอมส่งเงินดอกเบี้ยให้แก่ผู้รับจำนองทุกๆ ' . $arr_person['loan_analysis_length'] . ' เสมอไป</td>
			</tr>
			<tr>
			<td width="100%">ถ้าผู้จำนองผิดนัดชำระดอกเบี้ยไม่น้อยกว่า&nbsp;๑&nbsp;ปีเป็นต้นไปผู้จำนองยินยอมให้ผู้รับจำนองคำนวณดอกเบี้ยที่ค้างชำระทบต้นในบัญชีของผู้จำนองต่อไป</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๓.&nbsp;ผู้จำนองสัญญาว่า&nbsp;ในระหว่างสัญญาจำนองนี้&nbsp;ถ้าผู้รับจำนองเห็นว่า ทรัพย์ที่จำนองนี้&nbsp;มีราคาตกต่ำลงไปกว่าในราคา</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ที่ทำจำนองนี้ และเรียกผู้จำนองให้นำทรัพย์อื่นมาจำนองเพิ่มอีกให้คุ้มพอ
กับจำนวนเงินที่ผู้จำนองเป็นหนี้ผู้รับจำนอง หรือให้คุ้มพอจำนวนเงินที่บุคคลอื่นเป็นหนี้ผู้รับจำนองซึ่งผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ดังกล่าว ผู้จำนองจะต้องจัดหาทรัพย์อื่นมาทำจำนองเพิ่มอีก
ให้คุ้มพอกับจำนวนหนี้ที่ผู้จำนองเป็นหนี้อยู่&nbsp;หรือคุ้มพอกับจำนวนหนี้ที่ผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ซึ่งบุคคลอื่นจะต้องชำระกับผู้รับจำนอง ถ้าผู้จำนองบิดพลิ้วไม่ยอมปฏิบัติ หรือไม่สามารถปฏิบัติตามความที่กล่าวมานี้ ผู้รับจำนองมีสิทธิจะเรียกให้ผู้จำนองชำระหนี้ และบังคับจำนองได้ทันที</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๔.	ถ้าทรัพย์ที่จำนองนี้บุบสลาย หรือต้องภัยอันตราย หรือสูญหายไป ซึ่งเป็นเหตุให้ทรัพย์นั้นเสื่อมราคาไม่เพียงพอ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">แก่การประกันหนี้ของผู้จำนอง หรือที่ผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ซึ่งบุคคลอื่นต้องชำระ&nbsp;ผู้จำนอง<br/>จะต้องเอาทรัพย์อื่นที่มีราคาเพียงพอมาจำนองเพิ่มให้คุ้มพอกับจำนวนหนี้ที่ผู้จำนองเป็นหนี้อยู่&nbsp;หรือคุ้มพอกับจำนวนหนี้ที่ผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้ซึ่งบุคคลอื่นจะต้องชำระกับผู้รับจำนอง&nbsp;โดยไม่ชักช้า&nbsp;ถ้าผู้จำนองบิดพลิ้วไม่ยอม<br/>ปฏิบัติ หรือไม่สามารถปฏิบัติตามความที่กล่าวมานี้ ผู้รับจำนองมีสิทธิจะเรียกให้ผู้จำนองชำระหนี้ และบังคับจำนองได้ทันที</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๕.	เมื่อเวลาบังคับจำนองเอาทรัพย์สินซึ่งผู้จำนองได้จำนองไว้เป็นประกันหนี้แห่งตน โดยฟ้องคดีต่อศาลเพื่อให้พิพากษา</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">สั่งให้ยึดทรัพย์สินซึ่งจำนองและออกขายทอดตลาด หรือกรณีเรียกเอาทรัพย์หลุดจำนองแล้ว หรือกรณีภายหลังจากหนี้ถึงกำหนด<br/>ชำระถ้าไม่มีการจำนองรายอื่นหรือบุริมสิทธิอื่นอันได้จดทะเบียนจำนองไว้เหนือทรัพย์สินนี้แล้วผู้จำนองได้มีหนังสือไปยังผู้รับจำนองเพื่อให้ดำเนินการให้มีการขายทอดตลาดทรัพย์สินที่จำนองโดยไม่ต้องฟ้องเป็นคดีต่อศาล&nbsp;และผู้รับจำนองได้ดำเนินการขายทอด<br/>ตลาดทรัพย์นั้นภายในเวลา ๑ ปี นับแต่วันที่ได้รับหนังสือแจ้งแล้วได้เงินจำนวนสุทธิน้อยกว่า&nbsp;หรือราคาทรัพย์สินที่จำนองนี้ต่ำกว่า<br/>จำนวนเงินที่ค้างชำระกับค่าอุปกรณ์ต่างๆ ดังได้กล่าวมาแล้วนั้น&nbsp;เงินยังขาดจำนวนอยู่เท่าใด&nbsp;ผู้จำนองย่อมต้องรับผิดชอบรับใช้เงินที่ขาดจำนวนนั้นให้แก่ผู้รับจำนองจนครบถ้วน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%"></td>
				<td width="80%">ทั้งนี้ ในกรณีการบังคับจำนองเอาทรัพย์ซึ่งผู้จำนองได้จำนองทรัพย์สินของตนไว้เพื่อเป็นประกันหนี้</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">อันบุคคลอื่นจะต้องชำระโดยวิธีการตามวรรคหนึ่งแล้วได้เงินจำนวนสุทธิน้อยกว่า&nbsp;หรือราคาทรัพย์ที่จำนองนี้ต่ำกว่าจำนวนเงิน<br/>ที่ค้างชำระกับค่าอุปกรณ์ต่างๆ&nbsp;ผู้จำนองซึ่งได้จำนองทรัพย์สินเพื่อเป็นประกันหนี้อันบุคคลอื่นจะต้องชำระไม่ต้องรับผิดชอบ<br/>ใช้เงินในจำนวนเงินส่วนที่ขาดแต่ให้ลูกหนี้รับใช้เงินในส่วนที่ขาดดังกล่าวให้แก่ผู้รับจำนองจนครบถ้วน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๖. ภายหลังที่ผู้รับจำนองเอาทรัพย์สินที่จำนองนี้หลุดเป็นสิทธิโดยประการใดๆ ก็ดี ปรากฏว่า มีหนี้เงินค่าภาษีอากรเกี่ยว</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">กับทรัพย์สินที่จำนองนี้ค้างอยู่สำหรับเวลาก่อนวันโอนมากน้อยเท่าใดและผู้รับจำนองต้องรับผิดชอบรับใช้เงินภาษีอากรนั้นผู้จำนองต้องรับผิดชอบรับใช้เงินค่าภาษีอากรนั้นให้แก่ผู้รับจำนอง&nbsp;พร้อมทั้งเงินอื่นๆ&nbsp;ที่ผู้รับจำนองต้องเสียไป&nbsp;เนื่องจากการที่ผู้รับจำนอง<br/>ต้องรับผิดชอบรับใช้เงินค่าภาษีอากรนั้น ผู้จำนองต้องรับผิดชอบรับใช้เงินค่าภาษีอากรนั้นให้แก่ผู้รับจำนองพร้อมทั้งเงินอื่นๆ ที่ผู้รับจำนองต้องเสียไปจากการที่ผู้รับจำนองต้องรับผิดชอบรับใช้เงินค่าภาษีอากรเช่นนั้นด้วยโดยครบถ้วน</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๗.	ผู้จำนองสัญญาว่าจะรักษาซ่อมแซมตึก บ้านเรือน โรงเรือน และสิ่งปลูกสร้างต่างๆ ซึ่งมีอยู่ในที่ดินรายนี้แล้ว</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">หรือซึ่งจะได้ปลูกสร้างขึ้นต่อไปในภายหน้าให้มั่นคง เรียบร้อย และปกติดีอยู่เสมอตลอดระยะเวลาที่จำนองไว้แก่ผู้รับจำนอง โดยผู้จำนองจะต้องเสียค่ารักษาและซ่อมแซมเอง</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๘. ในกรณีผู้จำนองได้ทำประกันอัคคีภัยโรงเรือนหรือสิ่งปลูกสร้างซึ่งอยู่บนที่ดินที่จำนอง เป็นต้น ให้ผู้จำนองดำเนินการ</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">แจ้งผู้รับประกันภัย&nbsp;เพื่อแก้ไขการสลักหลังกรมธรรม์ประกันภัยโอนสิทธิที่จะรับค่าเสียหายจากบริษัทประกันภัยให้ผู้รับ<br/>จำนองต่อไป</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๙.	ในระหว่างที่ทรัพย์สินรายนี้ยังจำนองตามสัญญานี้อยู่ ผู้จำนองจะให้สิทธิประการใดๆ แก่ผู้อื่นในทรัพย์ที่จำนอง</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">เพื่อให้เกิดภาระผูกพัน เช่น เช่าถือ อาศัย ปลูกสร้าง ทางเดิน ยืม เป็นต้น อันเป็นการเสื่อมสิทธิ ลดสิทธิ รอนสิทธิ ทอนสิทธิ <br/>และเสียสิทธิของผู้จำนองเองในทรัพย์สินที่จำนองต่อผู้รับจำนอง&nbsp;ต้องได้รับความยินยอมอนุญาตจากผู้รับจำนองก่อนเป็นลาย<br/>ลักษณ์อักษร แต่ถ้าผู้จำนองได้ให้สิทธิประการใดๆ&nbsp;ข้างต้นแก่ผู้อื่นไว้แล้วในเวลาที่จำนองนี้&nbsp;ผู้จำนองต้องแจ้งให้ผู้รับจำนองทราบ<br/>เป็นลายลักษณ์อักษรทันที&nbsp;และถ้าผู้จำนองจะต่ออายุสิทธิที่ได้ให้ไว้แล้วออกไปอีก&nbsp;ผู้จำนองจะต้องได้รับความยินยอมเป็นลายลักษณ์อักษรจากผู้รับจำนองก่อน&nbsp;การกระทำใดๆ&nbsp;ที่ผู้จำนองได้กระทำฝ่าฝืน&nbsp;ขัดขืนต่อสัญญาข้อนี้&nbsp;จะไม่ผูกพันผู้รับจำนองด้วย<br/>ประการใดๆ&nbsp;ผู้รับจำนองมีสิทธิที่จะปฏิเสธการกระทำของผู้จำนองนั้นได้ทันที&nbsp;อีกทั้งผู้จำนองมีสิทธิจะบังคับจำนอง&nbsp;และเสียค่าเสียหายได้ด้วย</td>
			</tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๐. ถ้ามีปัญหาเกิดขึ้นเกี่ยวกับกรรมสิทธิ์ของผู้จำนองในที่ดินและทรัพย์สินที่จำนองเมื่อใด ผู้รับจำนองมีสิทธิจะเรียกให้</td>
			</tr>
		</table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="100%">ผู้จำนอง และเรียกค่าเสียหายได้ด้วย</td>
			</tr>
                </table>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๑. ถ้าผู้จำนองประพฤติผิดหรือไม่ประพฤติตามสัญญาที่บันทึกไว้ข้างบนนี้แต่ข้อหนึ่งข้อใด หรือทั้งหมดผู้รับจำนอง</td>
			</tr>
		</table>

                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">ข้อ ๑๒. ค่าธรรมเนียมและค่าใช้จ่ายทั้งสิ้นในการจำนองและไถ่ถอนจำนอง ผู้จำนองเป็นผู้เสียเอง</td>
			</tr>
		</table>
		';

       $htmlcontent_p2 = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="5%"></td>
				<td width="95%">เพื่อเป็นหลักฐานแห่งสัญญา คู่สัญญาได้ตรวจหนังสือต่อหน้าเจ้าพนักงานแล้ว รับรองว่าถูกต้องตามความประสงค์ทุกข้อ จึงได้พร้อมกันลงลายมือชื่อไว้เป็นสำคัญ</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ....................................................  ผู้จำนอง</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... ผู้จำนอง</td>
			</tr>

			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... พยาน</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">ลงชื่อ ...................................................... พยาน</td>
			</tr>

			<tr>
				<td width="50%"></td>
				<td width="50%" style="text-align:center;">(.................................................)</td>
			</tr>

		</table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="100%">หนังสือสัญญาฉบับนี้ได้ทำต่อหน้า</td>
                                </tr>
                        </table>
                         <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                        <td width="50%"></td>
                                        <td width="50%" style="text-align:center;">ลงชื่อ ...................................................... เจ้าพนักงานที่ดิน</td>
                                </tr>

                                <tr>
                                        <td width="50%"></td>
                                        <td width="50%" style="text-align:center;">ประทับตราประจำตำแหน่งเป็นสำคัญ</td>
                                </tr>
                        </table>
		';

        $pdf->AddPage();
        //เอกสารแบบท้าย

        $pdf->writeHTML($htmlcontent, true, 0, true, 0, '');

         $pdf->AddPage();
        //เอกสารแบบท้าย

        $pdf->writeHTML($htmlcontent_p2, true, 0, true, 0, '');

        $pdf->Output($filename . '.pdf', 'I');
    }

    public function word_data($id){

    	$this->load->model("forms_contract_model");
        $this->load->model("schedule_model");
        $this->load->model("province/province_model");
        $this->load->model("amphur/amphur_model");
        $this->load->model("district/district_model");
        $this->load->model('loan/loan_guarantee_land_model');
        $this->load->model('loan/loan_guarantee_bond_model');
        $this->load->model('loan/loan_guarantee_bookbank_model');
        $this->load->model('loan/loan_type_model');
        $this->load->model('loan_analysis_cal_model', 'loan_analysis_cal');
        $this->load->model('person/person_model');
        $this->load->model('title/title_model');

        $arr_person = $this->forms_contract_model->get_loan_guarantee($id);
        // echo "<pre>";
        // print_r( $arr_person);
        // echo "</pre>";

        if (count($arr_person) > 0) {
            $arr_province = $this->province_model->get_by_id($arr_person['person_addr_card_province_id']);
            $arr_amphur = $this->amphur_model->get_by_id($arr_person['person_addr_card_amphur_id']);
            $arr_district = $this->district_model->get_by_id($arr_person['person_addr_card_district_id']);

            if (count($arr_province) > 0)
                $arr_person['province_name'] = $arr_province['province_name'];
            if (count($arr_amphur) > 0)
                $arr_person['amphur_name'] = $arr_amphur['amphur_name'];
            if (count($arr_district) > 0)
                $arr_person['district_name'] = $arr_district['district_name'];

            if (!empty($arr_person['loan_analysis_length']))
                $arr_person['loan_analysis_length'] = $this->forms_contract_model->get_length($arr_person['loan_analysis_length']);
        }

        $arr_person['loan_analysis_amount'] = empty($arr_person['loan_analysis_amount']) ? 0.00 : $arr_person['loan_analysis_amount'];
        $arr_person['loan_analysis_amount'] = empty($arr_person['loan_analysis_interest']) ? 0.00 : $arr_person['loan_analysis_interest'];

        $arr_person['schedule'] = $this->schedule_model->get($id);
        foreach ($arr_person['schedule'] as $key => $val) {
            $arr_person['schedule'][$key]['loan_analysis_interest'] = $arr_person['loan_analysis_amount'];
        }

        $arr_person['land_guarantee'] = $this->loan_guarantee_land_model->get_table_data(array('loan_id' => $id));
        $arr_person['guarantee_bound'] = $this->loan_guarantee_bond_model->get_by_opt(array('loan_id' => $id));
        $arr_person['guarantee_bank'] = $this->loan_guarantee_bookbank_model->report_list(array('loan_id' => $id));
        $arr_person['loan_type'] = $this->loan_type_model->get_by_opt(array('loan_id' => $id));
        $arr_person['loan_type'][0]['loan_objective_name'] .= 'จาก'.$this->loan_type_model->redeem_type[$arr_person['loan_type'][0]['loan_type_redeem_type']];


        if (!empty($arr_person['land_guarantee'])) {
            foreach ($arr_person['land_guarantee'] as $key => $val) {

                $arr_person['land_guarantee'][$key]['province_name'] = $this->province_model->get_by_id($val['land_addr_province_id'])['province_name'];
                $arr_person['land_guarantee'][$key]['amphur_name'] = $this->amphur_model->get_by_id($val['land_addr_amphur_id'])['amphur_name'];
                $arr_person['land_guarantee'][$key]['district_name'] = $this->district_model->get_by_id($val['land_addr_district_id'])['district_name'];
            }
        }

        $arr_person['loan_analysis_list_cal'] = $this->loan_analysis_cal->getByLoan($id);

        $amount = 0.00;
        for($i = 0; $i < count($arr_person['schedule']); $i++) {
           $amount += $arr_person['schedule'][$i]['loan_schedule_amount'];
           $amount += $arr_person['schedule'][$i]['loan_schedule_interest'];

        }
        $avg_floor= floor($amount /  count($arr_person['schedule']) /100) * 100;
        $arr_person['avg'] = number_format($avg_floor);
		    $arr_person['count_schedule'] = count($arr_person['schedule']);
        if (!empty($arr_person['loan_spouse'])) {
          $spouse_model = $this->person_model->getById($arr_person['loan_spouse']);
          $spouse_title = $this->title_model->getById($spouse_model['title_id']);
          $arr_person['spouse_name'] = $spouse_title['title_name'] . $spouse_model['person_fname'] . ' ' . $spouse_model['person_lname'];
        } else {
          $arr_person['spouse_name'] = '';
        }

		    return $arr_person;
    }

    function word_form5($id){
    	include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$arr_person = $this->word_data($id);

        $GLOBALS = null;
        $GLOBALS['loan_objective_name'] = $arr_person['loan_type'][0]['loan_objective_name'];
        $GLOBALS['loan_location'] = $arr_person['loan_contract_location'];
        $GLOBALS['person_name'] = $arr_person['title_name'] . ' ' . $arr_person['person_fname'] . ' ' . $arr_person['person_lname'];
        $GLOBALS['person_age'] = num2Thai(age($arr_person['person_birthdate']));
        $GLOBALS['person_thaiid'] = num2Thai(formatThaiid($arr_person['person_thaiid']));
        $GLOBALS['person_addr_pre_no'] = num2Thai($arr_person['person_addr_pre_no']);
        $GLOBALS['person_addr_pre_moo'] = num2Thai($this->ReplaceEmpty($arr_person['person_addr_pre_moo']));
        $GLOBALS['person_addr_pre_road'] = $this->ReplaceEmpty($arr_person['person_addr_pre_road']);
        $GLOBALS['district_name'] = $arr_person['district_name'];
        $GLOBALS['amphur_name'] = $arr_person['amphur_name'];
        $GLOBALS['province_name'] = $arr_person['province_name'];
        $GLOBALS['person_phone'] = num2Thai(format_phone($this->ReplaceEmpty($arr_person['person_phone'])));
        $GLOBALS['person_mobile'] = num2Thai(format_phone($this->ReplaceEmpty($arr_person['person_mobile'])));
        $GLOBALS['loan_analysis_approve_sector_amount'] = num2Thai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2));
        $GLOBALS['loan_analysis_approve_sector_amount_text'] = num2Thai(num2wordsThai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2)));
        $GLOBALS['loan_analysis_interest'] = num2wordsThai((string)$arr_person['loan_analysis_interest'], false);
        $GLOBALS['loan_analysis_length'] = $this->ReplaceEmpty($arr_person['loan_analysis_length']);
        $GLOBALS['count_schedule'] = num2Thai($arr_person['count_schedule']);
        $GLOBALS['avg'] = num2Thai($arr_person['avg']);
        $GLOBALS['loan_desc'] = $arr_person['loan_type'][0]['loan_objective_name'];
        $GLOBALS['loan_schedule_date'] = num2Thai(thai_display_date($arr_person['schedule'][0]['loan_schedule_date']));

        $template = 'word/template/5_forms.docx';
        $GLOBALS['spouse_name'] = $arr_person['spouse_name'];
		    $TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).


        $datax = array();
        foreach ($arr_person['schedule'] as $key => $val) {
            $datax[] = array('loan_schedule_times'=> num2Thai($val['loan_schedule_times']),
			            'loan_schedule_date'=> num2Thai(thai_display_date($val['loan_schedule_date'])),
			            'loan_schedule_amount'=> num2Thai(number_format($val['loan_schedule_amount'], 2))
            );
		}
		$TBS->MergeBlock('schedule', $datax);

        $datax = array();
        foreach ($arr_person['loan_analysis_list_cal'] as $val) {
            $datax[] = array('start'=> num2Thai($val['loan_analysis_cal_start']),
				            'end'=> num2Thai($val['loan_analysis_cal_end']),
				            'amount'=> num2Thai(number_format($val['loan_analysis_cal_amount'], 2))
            );
		}
		$TBS->MergeBlock('ana', $datax);

        $datax = array();
        foreach ($arr_person['land_guarantee'] as $val) {
            $addr = 'จังหวัด' . $val['province_name'] . ' อำเภอ' . $val['amphur_name'] . ' ตำบล' . $val['district_name'];
            $datax[] = array('type'=> $val['land_type_name'],
				            'no'=> num2Thai($val['land_no']),
				            'land_no'=> num2Thai($val['land_addr_no']),
				            'addr'=> $addr,
           					'area'=> num2Thai($val['land_area_rai']) . ' ไร่ ' . num2Thai($val['land_area_ngan']) . ' งาน ' . num2Thai($val['land_area_wah']) . ' ตารางวา',
				            'owner'=> $val['title_name'] . $val['person_fname'] . ' ' . $val['person_lname']
            );
		}
		$TBS->MergeBlock('g1', $datax);

        $datax = array();
        foreach ($arr_person['guarantee_bound'] as $val) {
            $datax[] = array('owner'=> $val['loan_bond_owner'],
				            'thaiid'=> num2Thai(formatThaiid($val['loan_bond_thaiid'])),
				            'type'=> $val['loan_bond_type'],
				            'start'=> num2Thai($val['loan_bond_startnumber']),
           					'end'=> num2Thai($val['loan_bond_endnumber']),
				            'amount'=> num2Thai(number_format($val['loan_bond_amount'], 2))
            );
		}
		$TBS->MergeBlock('g2', $datax);

        $datax = array();
        foreach ($arr_person['guarantee_bank'] as $val) {
            $datax[] = array('owner'=> $val['loan_bookbank_owner'],
				            'bank'=> $val['bank_name'],
                    'branch' => $val['loan_bookbank_branch'],
				            'no'=> num2Thai($val['loan_bookbank_no']),
                    'date' => num2Thai(thai_display_date($val['loan_bookbank_date'])),
				            'type'=> $val['loan_bookbank_type_name'],
				            'amount'=> num2Thai(number_format($val['loan_bookbank_amount'], 2))
            );
		}
		$TBS->MergeBlock('g3', $datax);

    $debtors = $this->forms_contract_model->get_co_debtor($id);
    foreach ($debtors as &$debtor) {
      $debtor['person_thaiid'] = num2Thai(formatThaiid($debtor['person_thaiid']));
      $debtor['person_addr_pre_no'] = num2Thai($debtor['person_addr_pre_no']);
      $debtor['person_addr_pre_moo'] = num2Thai($debtor['person_addr_pre_moo']);
      $debtor['person_phone'] = num2Thai(format_phone($debtor['person_phone']));
      $debtor['person_mobile'] = num2Thai(format_phone($debtor['person_mobile']));
      $debtor['person_age'] = num2Thai(age($debtor['person_birthdate']));
      $debtor['person_age'] = num2Thai(age($debtor['person_birthdate']));
      $debtor['person_name'] = $debtor['title_name'] . ' ' . $debtor['person_fname'] . ' ' . $debtor['person_lname'];
    }
    unset($debtor);
    $TBS->MergeBlock('debtors', $debtors);
    $TBS->MergeBlock('debtorSigns', $debtors);
		// Delete comments
		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'forms_5_'.date('Y-m-d').'.docx';
    $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
     $TBS->Show(OPENTBS_FILE, $temp_file);
     $this->send_download($temp_file,$output_file_name);

    }
    public function send_download($temp_file,$file) {
          $basename = basename($file);
          $length   = sprintf("%u", filesize($temp_file));

          header('Content-Description: File Transfer');
          header('Content-Type: application/octet-stream');
          header('Content-Disposition: attachment; filename="' . $basename . '"');
          header('Content-Transfer-Encoding: binary');
          header('Connection: Keep-Alive');
          header('Expires: 0');
          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
          header('Pragma: public');
          header('Content-Length: ' . $length);
          ob_clean();
          flush();
          set_time_limit(0);
          readfile($temp_file);
          exit();
      }
	function word_form10($id){
    	include_once('word/class/tbs_class.php');
		include_once('word/class/tbs_plugin_opentbs.php');

		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

		$arr_person = $this->word_data($id);

        $GLOBALS = null;
        $GLOBALS['person_name'] = $arr_person['title_name'] . ' ' . $arr_person['person_fname'] . ' ' . $arr_person['person_lname'];
        $GLOBALS['loan_analysis_approve_sector_amount'] = num2Thai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2));
        $GLOBALS['loan_analysis_approve_sector_amount_text'] = num2Thai(num2wordsThai(number_format($arr_person['loan_analysis_approve_sector_amount'], 2)));
        $GLOBALS['loan_analysis_interest'] = num2Thai(number_format($arr_person['loan_analysis_interest'], 2));
        $GLOBALS['loan_analysis_length'] = $arr_person['loan_analysis_length'];

		$template = 'word/template/10_5_forms.docx';
		$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

		// Delete comments
		$TBS->PlugIn(OPENTBS_DELETE_COMMENTS);
		$output_file_name = 'forms_10_'.date('Y-m-d').'.docx';


    $temp_file = tempnam(sys_get_temp_dir(), 'Docx');
    $TBS->Show(OPENTBS_FILE, $temp_file);
    $this->send_download($temp_file,$output_file_name);
    }

    private function ReplaceEmpty($str) {
        return empty($str) ? '-' : $str;
    }

    public function replace_space($html) {
        return str_replace(' ','&nbsp;',$html);
        //echo "Total rows: ".$count;
    }

}
