<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class contract_model extends CI_Model {

    private $table = "loan_contract";
    private $contract_id;

    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);

        $this->contract_id = $this->table . '_id';
    }

    public function save($id, $arr) {
        if ($id == '') {
            $arr['createddate'] = date('Y-m-d H:i:s');
            $arr['createdby'] = get_uid_login();
            $this->db->insert($this->table, $arr);
            $id = $this->db->insert_id();
        } else {
            $arr['updatedate'] = date('Y-m-d H:i:s');
            $arr['updateby'] = get_uid_login();
            $this->db->update($this->table, $arr);
            $this->db->where($this->contract_id, $id);
        }
        $num_row = $this->db->affected_rows();
        return array('row' => $num_row, 'id' => $id);
    }

    public function del($contract_id) {
        $this->db->where('loan_contract_id', $contract_id);
        $this->db->delete($this->table);
    }

    public function update_status($loan_id, $update_by = 0) {
        $from_status = '1';
        $status = '2';
        $update_date = date('Y-m-d H:i:s');
        $loan_id = intval($loan_id);

        $this->db->where('loan_id', $loan_id);
        $this->db->update('loan', array('loan_status' => $status, 'loan_updatedate' => $update_date, 'loan_updateby' => $update_by));

        $this->db->where('loan_id', $loan_id);
        //$this->db->where('loan_summary_status', $from_status);
        $this->db->update('loan_summary', array('loan_summary_status' => $status, 'loan_summary_updatedate' => $update_date, 'loan_summary_updateby' => $update_by));

        $this->db->where('loan_id', $loan_id);
        //$this->db->where('loan_analysis_status', $from_status);
        $this->db->update('loan_analysis', array('loan_analysis_status' => $status, 'loan_analysis_updatedate' => $update_date, 'loan_analysis_updateby' => $update_by));

        //land
        $this->db->select('*');
        $this->db->from('loan_guarantee_land');
        $this->db->join('building', 'loan_guarantee_land.land_id=building.land_id', 'left');
        $this->db->where('loan_guarantee_land.loan_id', $loan_id);
        $res_land = $this->db->get();
        //echo $this->db->last_query().'<hr/>';

        foreach ($res_land->result() as $row) {
            $this->db->where('land_id', $row->land_id);
            //$this->db->where('land_status', $from_status);
            $this->db->update('land', array('land_status' => $status, 'land_updatedate' => $update_date, 'land_updateby' => $update_by));
            //echo $this->db->last_query().'<hr/>';

            $this->db->where('land_id', $row->land_id);
            //$this->db->where('land_record_status', $from_status);
            $this->db->update('land_record', array('land_record_status' => $status, 'land_record_updatedate' => $update_date, 'land_record_updateby' => $update_by));
            //echo $this->db->last_query().'<hr/>';

            $this->db->where('land_id', $row->land_id);
            //$this->db->where('land_assess_status', $from_status);
            $this->db->update('land_assess', array('land_assess_status' => $status, 'land_assess_updatedate' => $update_date, 'land_assess_updateby' => $update_by));
            //echo $this->db->last_query().'<hr/>';

            $this->db->where('building_id', $row->building_id);
            $this->db->update('building', array('building_status' => $status, 'building_updatedate' => $update_date, 'building_status' => $update_by));
            //echo $this->db->last_query().'<hr/>';

            $this->db->where('building_id', $row->building_id);
            //$this->db->where('building_record_status', $from_status);
            $this->db->update('building_record', array('building_record_status' => $status, 'building_record_updatedate' => $update_date, 'building_record_updateby' => $update_by));
            // echo $this->db->last_query().'<hr/>';

            $this->db->where('building_id', $row->building_id);
            //$this->db->where('building_assess_status', $from_status);
            $this->db->update('building_assess', array('building_assess_status' => $status, 'building_assess_updatedate' => $update_date, 'building_assess_updateby' => $update_by));
            //echo $this->db->last_query().'<hr/>';

            $this->db->from('loan_guarantee_land');
            $this->db->join('building', 'loan_guarantee_land.land_id=building.land_id', 'left');
            $this->db->where('loan_guarantee_land.loan_id', $loan_id);

            
        }
        
        $this->db->where('loan_id', $loan_id);
        //$this->db->where('loan_guarantee_land_status', $from_status);
        $this->db->update('loan_guarantee_land', array('loan_guarantee_land_status' => $status, 'loan_guarantee_land_updatedate' => $update_date, 'loan_guarantee_land_updateby' => $update_by));
    }

}

?>