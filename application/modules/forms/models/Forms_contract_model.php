<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class forms_contract_model extends CI_Model
{
	private $table = "loan_analysis";
	private $table_loan = "loan";
	private $table_loan_contract = "loan_contract";
	private $table_loan_co = "loan_co";
	private $table_person = "person";
	private $table_district = "master_district";
	private $table_amphur = "master_amphur";
	private $table_province = "master_province";
	private $table_objective = "master_loan_objective";
	private $table_loantype = "loan_type";
	private $table_title = "config_title";
	private $loan_analysis_length = array('1'=>'เดือน','6'=>'6 เดือน','12'=>'ปี');

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}

	//*** default status = 2
	public function get($id_card,$name,$status = "2")
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->join($this->table_loan,"{$this->table}.loan_id={$this->table_loan}.loan_id","inner");
		$this->db->join($this->table_person,"{$this->table_loan}.person_id={$this->table_person}.person_id","inner");
		$this->db->join($this->table_loantype,"{$this->table_loan}.loan_id={$this->table_loantype}.loan_id","inner");
		$this->db->join($this->table_objective,"{$this->table_loantype}.loan_objective_id={$this->table_objective}.loan_objective_id","inner");


		$this->db->where("{$this->table_loan}.loan_status",$status);
		if($id_card != ''){
			$this->db->like("{$this->table_person}.person_thaiid", trim($id_card),'both');
		}
		if($name != ''){
			$this->db->like("{$this->table_person}.person_fname", trim($name),'both');
			$this->db->or_like("{$this->table_person}.person_lname", trim($name),'both');
		}

		if(check_permission_isowner('law')){
	   		$this->db->where("{$this->table_loan}.loan_createby", get_uid_login());
	    }

		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_loan_guarantee($loan_id){
		$this->db->select("*");
		$this->db->from($this->table_loan);
		$this->db->join($this->table_person,"{$this->table_loan}.person_id={$this->table_person}.person_id","inner");
		$this->db->join($this->table,"{$this->table_loan}.loan_id={$this->table}.loan_id",'left');
		$this->db->join($this->table_loan_contract,"{$this->table_loan}.loan_id={$this->table_loan_contract}.loan_id",'left');
		$this->db->join($this->table_title,"{$this->table_person}.title_id={$this->table_title}.title_id","left");
		$this->db->where("{$this->table_loan}.loan_id",$loan_id);

		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_co_debtor($loan_id) {
		$this->db->select("*");
		$this->db->from($this->table_loan);
		$this->db->join($this->table_loan_co, "{$this->table_loan}.loan_id = {$this->table_loan_co}.loan_id", "inner");
		$this->db->join($this->table_person, "{$this->table_loan_co}.person_id = {$this->table_person}.person_id", "inner");
		$this->db->join($this->table_title, "{$this->table_person}.title_id = {$this->table_title}.title_id", "left");
		$this->db->join($this->table_province, "{$this->table_person}.person_addr_pre_province_id = {$this->table_province}.province_id", "left");
		$this->db->join($this->table_amphur, "{$this->table_person}.person_addr_pre_amphur_id = {$this->table_amphur}.amphur_id", "left");
		$this->db->join($this->table_district, "{$this->table_person}.person_addr_pre_district_id = {$this->table_district}.district_id", "left");
		$this->db->where("{$this->table_loan}.loan_id", $loan_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_loan_schedule($loan_id){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_id",$loan_id);

		$query = $this->db->get();
		return $query->row_array();
	}

	public function get_length($lenght){
		return $this->loan_analysis_length[$lenght];
	}





}
?>
