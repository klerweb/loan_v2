<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_analysis_cal_model extends CI_Model
{
	private $table = 'loan_analysis_cal';
	
	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	public function getByLoan($loan_id)
	{
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("loan_id", $loan_id);
		//$this->db->where("loan_analysis_cal_status !=", "0");
		$this->db->order_by("loan_analysis_cal_time", "asc");
		$query = $this->db->get();
	
		$result = $query->num_rows()!=0? $query->result_array() : array();
		
		return $result;
	}
}
?>