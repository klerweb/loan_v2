<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class loan_analysis_model extends CI_Model
{	
	private $table = "loan_analysis";
	

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	
	public function save($id,$arr){
		
		if($id==''){
			$arr['loan_analysis_createdate'] = date('Y-m-d H:i:s');
			$arr['loan_analysis_createby'] = get_uid_login();
			$this->db->insert($this->table, $arr);
		}else{
			$arr['loan_analysis_updatedate'] = date('Y-m-d H:i:s');
			$arr['loan_analysis_updateby'] = get_uid_login();
			$this->db->where('loan_analysis_id',$id);
			$this->db->update($this->table,$arr);
			
		}
		
		$num_row = $this->db->affected_rows();
		return $num_row;
	}
	
	
	
}
?>