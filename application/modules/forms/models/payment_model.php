<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class payment_model extends CI_Model
{	
	private $table = "payment_schedule";
	

	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	
	public function save($id,$arr){
		
		if($id==''){
			$arr['createddate'] = date('Y-m-d H:i:s');
			$arr['createdby'] = get_uid_login();
			$this->db->insert($this->table, $arr);
		}
		$num_row = $this->db->affected_rows();
		return $num_row;
	}
	
	public function del($contract_id){
		$this->db->where('loan_contract_id', $contract_id);
		$this->db->delete($this->table);
	}
	
}
?>