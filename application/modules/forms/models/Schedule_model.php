<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class schedule_model extends CI_Model
{	
	private $table = "loan_schedule";


	function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);	
	}
	
	
	public function get($loan_id){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where("{$this->table}.loan_id",$loan_id);
		$this->db->order_by('loan_schedule_date');
		
		$query = $this->db->get();		
		return $query->result_array();
		
	}
	
	
}
?>