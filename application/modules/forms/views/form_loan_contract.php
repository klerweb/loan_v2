<form action="forms/save" method="post" id="frm_form_contract"> 
<input type="hidden" name="analysis_id" value="<?php echo $data['loan_analysis_id'];?>"/>
<?php
if(isset($action)){
	 if($action['action']=='saved'){
	 ?>
	 <div class="alert alert-success alert-dismissable">
	 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	  <strong>บันทึกข้อมูลเรียบร้อย!</strong>
	 
	</div>
	
	 <?php 
	 }
	 if($action['action']=="error"){
		 ?>
		 <div class="alert alert-danger alert-dismissable">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			  <strong>พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้!</strong>
			</div>
		 <?php
		 
	 }
 }
 ?>
 
 
 <style>
 .css_topic{
	font-size: 14px;
    font-weight: bold;
 }
 caption{
	 font-weight: bold;
 }
 </style>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">แบบบันทึกกู้ยืมเงิน</h4>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<p>หนังสือกู้ยืมเงินเพื่อขอใช้สินเชื่อประเภท &nbsp;&nbsp;&nbsp; <?php echo $data['loan_type'][0]['loan_objective_name'];?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<label>ทำที่</label>
					<input type="text" name="txt_contract_location" id="txt_contract_location" class="form-control" value="สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)"/> 
				</div>
			</div>		
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label>เลขที่สัญญา</label>
					<input type="text" name="txt_contract_no" id="txt_contract_no" class="form-control" readonly/> 
				</div>
			</div>
		
			<div class="col-sm-4">
				<div class="form-group">
					<label>วันที่</label>
					<div class="input-group">
						<input type="text" name="txt_contract_date" id="txt_contract_date" class="form-control" readonly/> 
						<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<hr/>
			<h4>ผู้ขอสินเชื้อ</h4>
			<div class="col-sm-4">
				<div class="form-group">
					<label>คำนำหน้าชื่อ</label>
					<input type="text" name="txt_person_title" class="form-control" value="<?php echo $data['title_name']?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>ชื่อ</label>
					<input type="text" name="txt_person_fname" class="form-control" value="<?php echo $data['person_fname'];?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>นามสกุล</label>
					<input type="text" name="txt_person_lname" class="form-control" value="<?php echo $data['person_lname'];?>"  readonly/>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="col-sm-4">
				<div class="form-group">
					<label>เลขที่บัตรประชาชน</label>
					<input type="text" name="txt_person_card_no" class="form-control"  value="<?php echo formatThaiid($data['person_thaiid']);?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>วัน/เดือน/ปี</label>
					<input type="text" name="txt_person_birthday" class="form-control" value="<?php echo convert_dmy_th($data['person_birthdate']);?>"  readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>อายุ</label>
					<input type="text" name="txt_person_age" class="form-control" value="<?php echo eng_display_age($data['person_birthdate']);?>" readonly/>
				</div>
			</div>
		</div>
		<!-- person -->
		<div class="row">	
			<h4>ที่อยู่</h4>
			<div class="col-sm-4">
				<div class="form-group">
					<label>เลขที่</label>
					<input type="text" name="txt_addr_no" class="form-control" value="<?php echo $data['person_addr_card_no'];?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>หมู่ที่</label>
					<input type="text" name="txt_addr_moo" class="form-control"  value="<?php echo $data['person_addr_card_no'];?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>ถนน</label>
					<input type="text" name="txt_addr_road" class="form-control" value="<?php echo $data['person_addr_card_road'];?>" readonly/>
				</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-4">
				<div class="form-group">
					<label>จังหวัด</label>
					<input type="text" name="txt_addr_province" class="form-control" value="<?php echo $data['province_name'];?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>อำเภอ</label>
					<input type="text" name="txt_addr_amphur" class="form-control" value="<?php echo $data['amphur_name'];?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>ตำบล/แขวง</label>
					<input type="text" name="txt_addr_tumbon" class="form-control" value="<?php echo $data['district_name'];?>" readonly/>
				</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-sm-4">
				<div class="form-group">
					<label>โทรศัพท์บ้าน</label>
					<input type="text" name="txt_addr_tel_home" class="form-control" value="<?php echo $data['person_phone'];?>" readonly/>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label>โทรศัพท์มือถือ</label>
					<input type="text" name="txt_addr_mobile" class="form-control" value="<?php echo $data['person_mobile'];?>" readonly/>
				</div>
			</div>
			
		</div>
		<!-- address -->
		<div class="row">
			<h4>รายละเอียดหนังสือกู้ยืม</h4>
			<div class="col-sm-4">
				<div class="form-group">
					<label class="css_topic">1. กู้ยืมเงินจาก บจธ. เป็นจำนวนเงิน</label>
					<div class="input-group">
						<input type="text" name="txt_loan_amount" class="form-control" value="<?php echo number_format($data["loan_analysis_approve_sector_amount"],2);?>" />
						<span class="input-group-addon">บาท</span>
					</div>
				</div>
			</div>
                        <div class="col-sm-4">
				<div class="form-group">
					<label class="css_topic">โดยมีวัตถุประสงค์เพื่อ</label>
                                                                        <input type="text" name="txt_loan_desc" class="form-control" value="<?php echo $data["loan_desc"];?>" readonly="readonly"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group"> 
					<label class="css_topic">2. แบบบันทึกคำขอสินเชื่อ(บจธ. ๑) ลงวันที่</label>
					<input type="text" name="txt_contract_date" class="form-control" value="<?php echo convert_dmy_th($data["loan_date"]);?>" readonly/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group"> 
					<label class="css_topic">3. การชำระหนี้</label>
					
					<table class="table">
						<caption>ข้อตกลงชำระหนี้ในแต่ละงวด</caption>
						<thead>
							<tr>
								<th>งวดที่</th>
								<th>กำหนดชำระภายในวันที่</th>
								<th>กำหนดชำระเงินต้น (บาท)</th>
								<th>ชำระดอกเบี้ยถึงวันที่</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$first_date = '';
							$total = 0.00;
							$no = 0;
							if(isset($data['schedule'])){
								
								foreach($data['schedule'] as $key => $val){
									
									if($no==1)
										$first_date = convert_dmy_th($val['loan_schedule_date']);
									
									$total += $val['loan_schedule_amount'];
							?>
							<tr>
								<td><?php echo $no+1;?></td>
								<td><?php echo convert_dmy_th($val['loan_schedule_date']);?></td>
								<td><?php echo number_format($val['loan_schedule_amount'],2);?></td>
								<td><?php echo convert_dmy_th($val['loan_schedule_date']);?></td>
								<input type="hidden" name="pay_no[]" value="<?php echo $no;?>">
								<input type="hidden" name="pay_date[]" value="<?php echo $val['loan_schedule_date'];?>">
								<input type="hidden" name="pay_amount[]" value="<?php echo $val['loan_schedule_amount'];?>">
								<input type="hidden" name="pay_interest_rate[]" value="<?php echo $data['loan_analysis_interest'];?>">
								</td>								
							</tr>
							<?php
								$no++;
								}
							}
							if($no > 1)
								$total = $total/($no);		
							
							?>
						</tbody>
					</table>
					<!--
					<table class="table">
						<caption>ตกลงชำระหนี้เป็นงวด</caption>
						<thead>
							<tr>
								<th>งวดที่</th>
								<th>ถึงงวดที่</th>
								<th>กำหนดชำระเงินต้น (บาท)</th>								
							</tr>
						</thead>
						<tbody>
							<?php 
							$first_date = '';
							$total = 0.00;
							$no = 0;
							if(isset($data['schedule'])){
								
								foreach($data['schedule'] as $key => $val){
									
									if($no==1)
										$first_date = convert_dmy_th($val['loan_schedule_date']);
									
									$total += $val['loan_schedule_amount'];
							?>
							<tr>
								<td><?php echo $no+1;?></td>
								<td><?php if($no==count($data['schedule'])-1){ echo $no+1; } else { echo $no+2;}?></td>
								<td><?php echo number_format($val['loan_schedule_amount'],2);?>
								
								</td>								
							</tr>
							<?php
								$no++;
								}
							}
							if($no > 1)
								$total = $total/($no);		
							
							?>
						</tbody>
					</table>
					-->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group"> 
					<label>กำหนดชำระต้นเงินและดอกเบี้ยงวดแรกภายในวันที่</label>
					<input type="text" name="txt_pay_date" class="form-control" value="<?php echo $first_date;?>" readonly />
				</div>
			</div>
		
			<div class="col-sm-4">
				<div class="form-group"> 
					<label>ตกลงชำระคืนต้นเงินกู้</label>
                                        <input type="text" name="txt_pay_min" class="form-control" value="<?php echo  number_format($data['loan_analysis_approve_sector_amount'],2);?>" readonly />
					
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group"> 
					<label>เป็นงวดราย</label>
					<input type="text" name="txt_pay_period" class="form-control" value="<?php echo $data['loan_analysis_length'];?>" readonly />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group"> 
					<label>โดยเฉลี่ย ไม่ต่ำกว่า</label>
					<input type="text" name="txt_pay_avg" class="form-control" value="<?php echo number_format($total,2); ?>" readonly />
				</div>
			</div>
		
			<div class="col-sm-4">
				<div class="form-group"> 
					<label>รวม</label>
					<div class="input-group">
						<input type="text" name="txt_pay_period_type" class="form-control" value="<?php echo $no;?>" readonly />
						<span class="input-group-addon">งวด</span>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group"> 
					<label>นับแต่วันถัดจากวันรับเงินกู้ โดยเริ่มชำระงวดแรก ในวันที่ </label>
					<input type="text" name="txt_pay_first_period" class="form-control" value="<?php echo $first_date;?>" readonly />
					
				</div>
			</div>
			
		</div>
		<div class="row">			
			<div class="col-sm-12">
				<div class="form-group"> 
					<label class="css_topic">4. ชำระค่าธรรมเนียมและค่าใช้จ่ายในการอำนวยสินเชื่อของ บจธ. ตามอัตราที่ บจธ. กำหนด และยินยอมชำระดอกเบี้ยตามหนังสือหนังสือกู้ฉบับนี้ ในอัตราร้อยละ</label>
					<div class="input-group col-sm-4">
						<input type="text" name="txt_pay_period_type" class="form-control" value="<?php echo $data['loan_analysis_interest'];?>" readonly />
						<span class="input-group-addon">ต่อปี</span>
					</div>
				</div>
			</div>			
		</div>
		<div class="row">			
			<div class="col-sm-12">
				<div class="form-group"> 
					<label class="css_topic">5. หลักค้ำประกันเงิน</label>
					<div class="table-responsive"> 
					<table class="table table-striped mb30">
						<caption>อสังหาริมทรัพย์</caption>
						<thead>
							<tr>
								<th>ประเภทเอกสารสิทธิ</th>
								<th>เลขที่</th>
								<th>เลขที่ดิน</th>
								<th>ที่ตั้ง</th>
								<th>เนื้อที่</th>
								<th>ผู้ถือกรรมสิทธิ</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						if(count($data['land_guarantee']) > 0) {
							foreach($data['land_guarantee'] as $val){
						?>
							<tr>
								<td><?php echo $val['land_type_name'];?></td>
								<td><?php echo $val['land_no'];?></td>
								<td><?php echo $val['land_addr_no'];?></td>
								<td><?php echo show_address_master('land_addr',$val);?></td>
								<td><?php echo $val['land_area_rai'].' ไร่ '.$val['land_area_ngan'].' งาน '.$val['land_area_wah'].' ตารางวา';?></td>
								<td><?php echo $val['title_name'].$val['person_fname'].' '.$val['person_lname'];?></td>
							</tr>
						<?php 
							}
						}
						?>
						</tbody>
					</table>
					<table class="table table-striped mb30">
						<caption>พันธบัตรรัฐบาล</caption>
						<thead>
							<tr>
								<th>เจ้าของกรรมสิทธิ์</th>
								<th>เลขประจำตัวประชาชน</th>
								<th>ชนิดพันธบัตร</th>
								<th>เลขที่ต้น</th>
								<th>เลขที่ท้าย</th>
								<th>ราคา (บาท)</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						if(count($data['guarantee_bound']) > 0 ){
							foreach($data['guarantee_bound'] as $val){
						?>
							<tr>
								<td><?php echo $val['loan_bond_owner'];?></td>
								<td><?php echo $val['loan_bond_thaiid'];?></td>
								<td><?php echo $val['loan_bond_type'];?></td>
								<td><?php echo $val['loan_bond_startnumber'];?></td>
								<td><?php echo $val['loan_bond_endnumber'];?></td>
								<td><?php echo $val['loan_bond_amount'];?></td>
							</tr>
						<?php 
							}
						}
						?>
						</tbody>
					</table>
					<table class="table table-striped mb30">
						<caption>เงินฝากในบัญชีของธนาคารหรือสถาบันการเงินหรือสหกรณ์</caption>
						<thead>
							<tr>
								<th>เจ้าของบัญชี</th>
								<th>ธนาคาร/สถาบันการเงิน/สหกรณ์</th>
								<th>เลขที่บัญชี</th>
								<th>ประเภทบัญชี</th>
								<th>จำนวนเงิน(บาท)</th>
								
							</tr>
						</thead>
						<tbody>
						<?php 
						if(count($data['guarantee_bank']) > 0 ){
							foreach($data['guarantee_bank'] as $val)
							{
						?>
							<tr>
								<td><?php echo $val['loan_bookbank_owner'];?></td>
								<td><?php echo $val['bank_name'];?></td>
								<td><?php echo $val['loan_bookbank_no'];?></td>
								<td><?php echo $val['loan_bookbank_type_name'];?></td>
								<td><?php echo $val['loan_bookbank_amount'];?></td>
								
							</tr>
						<?php
							}						
						}
						?>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			
		</div>
		<!-- detail contract loan -->
	</div><!-- panel body -->
	<div class="panel-footer" style="text-align:right;">
	<?php if(!$this->session->flashdata('ss_save_id')) {?>	
            <button class="btn btn-primary">บันทึก</button>
        <?php }else{
            ?>
            <a href="<?php echo site_url('forms');?>" class="btn btn-primary">ปิด</a>
            <?php 
            
        } ?>
		
	</div><!-- panel-footer -->
	</div>
</div>
<!-- Button trigger modal -->
</form>


