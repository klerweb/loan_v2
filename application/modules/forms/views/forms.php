<?php if($visibled_add) { ?>
<div class="row">
    <div class="col-md-12">
        <p class="nomargin" style="margin-bottom:20px; text-align:right;">
            <button type="button" class="btn btn-default " onclick="location.href = '<?php echo site_url('forms/index/add'); ?>'">เพิ่มแบบกู้ยืม</button>
        </p>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-minimize tooltips" data-toggle="tooltip" title="Minimize Panel"><i class="fa fa-minus"></i></a>
                </div><!-- panel-btns -->
                <h4 class="panel-title">ค้นหา</h4>
            </div>
            <div class="panel-body">
                <form id="frmSearch" name="frmSearch" class="form-inline" method="post" action="<?php echo current_url(); ?>">
                    <div class="form-group">
                        <label class="sr-only" for="loan_code">เลขที่บัตรประชาชน</label>
                        <input type="text" class="form-control" id="person_card_id" name="person_card_id" placeholder="เลขที่บัตรประชาชน" value="" />
                    </div><!-- form-group -->
                    <div class="form-group">
                        <label class="sr-only" for="person_name">ชื่อ-นามสกุล</label>
                        <input type="text" class="form-control input-xs" id="person_name" name="person_name" placeholder="ชื่อ-นามสกุล" value="" />
                    </div><!-- form-group -->

                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div><!-- panel-body -->
        </div><!-- panel -->


        <div class="row">
            <div class="col-md-12">
                <!-- BASIC WIZARD -->
                <form method="post" id="valWizard" class="panel-wizard">
                    <div class="tab-pane">
                        <!--<div class="row">
                                <div class="col-sm-12 right">
                                        <button id="btnAddCo" name="btnAddCo" type="button" class="btn btn-primary" onClick="location.href='<?php echo base_url() ?>land/form'">เพิ่มแบบบันทึก</button>
                                </div>
                        </div>
                        -->
                        <div class="col-md-12">

                            <div class="table-responsive">
                                <table class="table mb30">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>เลขที่ขอสินเชื่อ</th>
                                            <th>ชื่อผู้ขอสินเชื่อ</th>
                                            <th>วันที่ขอสินเชื่อ</th>
                                            <th>ประเภทสินเชื่อ</th>
                                            <th>ที่อยู่</th>
                                            <th>สถานะ</th>
                                            <th>จัดการ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (count($data) > 0) {
                                            $i = 1;
                                            foreach ($data as $val) {
                                               
                                                ?>
                                        <tr <?=$val['loan_status_txt'];?>>
                                                    <td><?= $i; ?></td>
                                                    <td class="text-nowrap"><?= $val['loan_code']; ?></td>
                                                    <td class="text-nowrap"><?= $val['person_fname'] . ' ' . $val['person_lname']; ?></td>
                                                    <td class="text-nowrap"><?= thai_display_date($val['loan_date']); ?></td>
                                                    <td><?= $val['loan_objective_name']; ?></td>
                                                    <td><?php echo show_address_master('person_addr_card', $val); ?></td>
                                                    <td><?=$val['status']; ?></td>
                                                    <td class="table-action text-nowrap">										
                                                        <a href="<?php echo site_url("forms/word_form5/" . $val['loan_id']); ?>" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="พิมพ์บจธ. สช. - ๕" target="_bank"><i class="fa fa-print"></i></a>
                                                        <a href="<?php echo site_url("forms/word_form10/" . $val['loan_id']); ?>" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="พิมพ์บจธ. สช. - ๑๐" target="_bank"><i class="fa fa-print"></i></a>
                                                        <?php if($val['loan_status'] != '2'){?><a href="<?php echo site_url("forms/load_contract/" . $val['loan_id']); ?>" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="แก้ไข"><i class="fa fa-pencil-square-o"></i></a><?php }?>
                                                        <!--<a href="#" data-toggle="tooltip" title="" class="delete-row tooltips" data-original-title="ลบ"><i class="fa fa-trash-o"  aria-hidden="true" ></i></a>-->						  
                                                    </td>
                                                </tr>			  
                                                <?php
                                                $i++;
                                            }
                                        } else if ($this->input->post()) {
                                            ?>

                                            <tr>
                                                <td colspan="8"><div class="alert alert-danger text-center" role="alert">ไม่พบข้อมูล</div></td>
                                            </tr>

                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div><!-- table-responsive -->
<?= $pagging; ?>
                        </div>
                    </div><!-- tab-pane -->
                </form><!-- panel-wizard -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div>
</div>
<!-- content -->
