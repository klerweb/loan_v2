<?php
/*user_helper*/

/* get data user login : u_id */
function get_uid_login()
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_id'])){
		return 	$login['u_id'];
	}else{
		return null;
	}	
}

/* get data user login : u_fullname */
function get_ufullname_login()
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_fullname'])){
		return 	$login['u_fullname'];
	}else{
		return null;
	}	
}

/* get data user login : role */
function get_role_login()
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_role'])){
		return 	$login['u_role'];
	}else{
		return null;
	}
}

/* check permission user login : isread by menu */
function check_permission_isread($module)
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_module'][$module]['isread'])){
		return 	true;
	}else{
		return false;
	}
}

/* check permission user login : iswrite by menu */
function check_permission_iswrite($module)
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_module'][$module]['iswrite'])){
		return 	true;
	}else{
		return false;
	}
}

/* check permission user login : isedit by menu */
function check_permission_isedit($module)
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_module'][$module]['isedit'])){
		return 	true;
	}else{
		return false;
	}
}

/* check permission user login : isdelete by menu */
function check_permission_isdelete($module)
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_module'][$module]['isdelete'])){
		return 	true;
	}else{
		return false;
	}
}

/* check permission user login : isowner by menu */
function check_permission_isowner($module)
{
	$CI = & get_instance();
	$login = $CI->session->userdata('session_x');
	if(!empty($login['u_module'][$module]['isowner'])){
		return 	true;
	}else{
		return false;
	}
}
?>