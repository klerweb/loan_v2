<?php
function formatThaiid($thaiid)
{
	if(preg_match('/^[1-9][0-9]{12}$/', $thaiid))
	{
		$thaiid = substr($thaiid, 0, 1).'-'.substr($thaiid, 1, 4).'-'.substr($thaiid, 5, 5).'-'.substr($thaiid, 10, 2).'-'.substr($thaiid, 12, 1);
	}
	
	return $thaiid;
}
?>