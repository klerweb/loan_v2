<?php
	
	/*
	--คำนวณหาพื้นที่เป็นไร่
	-- 1 ไร่ เท่ากับ 4 งาน
	-- 1 งาน เท่ากับ 400 ตารางวา
	*/
	function CalculateLandAreaRai($rai, $ngan,$wa){
			//convert to wa
			//(parseInt(total_rai) * 400) + (parseInt(total_gnan)*100) + (parseInt(total_wha));
			$total_wa = (intval($rai) * 400) + (intval($ngan)*100) + intval($wa);
		
			return doubleVal($total_wa/400.00);
	}
	
	/*
	-- ราคาประเมินขั้นตำ
	--คำนวณจาก 
	-- ราคาซื้อขายในปัจจุบัน สูงสุด ต่ำสุด  และ ราคาประเมินทุนทรัพย์ที่ดินของกรมธนารักษ์
	*/	
	function CalculateBasicPrice($max,$min,$inspect){
		return ((($max+$min)/2.00)+$inspect)/2.00;
	}
	
	function CalulatePricePerRai($rai,$ngan,$wa,$price_per_rai){
		return $price_per_rai * CalculateLandAreaRai($rai,$ngan,$wa);
	}
	
	function CalcutateAge($dob){

        $dob = date("Y-m-d",strtotime($dob));
		
        $dobObject = new DateTime($dob);
        $nowObject = new DateTime();

        $diff = $dobObject->diff($nowObject);

        return $diff->y;

	}
	
	function conver_date($text){
		$arr = explode('/',$text);
		return (intval($arr[2])).'-'.$arr[1].'-'.$arr[0];
	}
	function conver_date_th_en($text){
		$arr = explode('/',$text);
		return (intval($arr[2])-543).'-'.$arr[1].'-'.$arr[0];
	}
	function conver_date_en_th($text){
		if($text != ''){
			$arr = explode('-',$text);
			return $arr[2].'/'.$arr[1].'/'.(intval($arr[0])+543);
		}else{
			return '';
		}
	}
	
	function show_address_master($fix_name,$array){
		
			$str = '';
			if(!empty($array[$fix_name.'_no'])){
				$str .= ' เลขที่ '.$array[$fix_name.'_no'];
			}
			if(!empty($array[$fix_name.'_moo'])){
				$str .= ' หมู่ที่ '.$array[$fix_name.'_moo'];
			}
			if(!empty($array[$fix_name.'_soi'])){
				$str .= ' ซอย'.$array[$fix_name.'_soi'];
			}
			if(!empty($array[$fix_name.'_road'])){
				$str .= ' ถนน'.$array[$fix_name.'_road'];
			}
			if(!empty($array['district_name'])){
				$str .= ' ตำบล'.$array['district_name'];
			}
			if(!empty($array['amphur_name'])){
				$str .= ' อำเภอ'.$array['amphur_name'];
			}
			if(!empty($array['province_name'])){
				$str .= ' จังหวัด'.$array['province_name'];
			}	
			return $str;

	}
	
	

?>