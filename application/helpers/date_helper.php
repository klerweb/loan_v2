﻿<?php
function thai_month($month)
{
	$thai_month_arr = array(
			'',
			'มกราคม',
			'กุมภาพันธ์',
			'มีนาคม',
			'เมษายน',
			'พฤษภาคม',
			'มิถุนายน',
			'กรกฎาคม',
			'สิงหาคม',
			'กันยายน',
			'ตุลาคม',
			'พฤศจิกายน',
			'ธันวาคม'
	);

	return $thai_month_arr[intval($month)];
}

function thai_month_short($month)
{
	$thai_month_arr = array(
			'',
			'ม.ค.',
			'ก.พ.',
			'มี.ค.',
			'เม.ย.',
			'พ.ค.',
			'มิ.ย.',
			'ก.ค.',
			'ส.ค.',
			'ก.ย.',
			'ต.ค.',
			'พ.ย.',
			'ธ.ค.'
	);

	return $thai_month_arr[intval($month)];
}


function thai_display_date($date)
{
        if(!$date){
            return FALSE;
        }

	list($y, $m, $d) = explode('-', $date);
	return  ((int)$d).' '.thai_month($m).' '.(intval($y)+543);
}



function thai_display_date_short($date)
{
        if(!$date){
            return FALSE;
        }

	list($y, $m, $d) = explode('-', $date);
	return ((int)$d).' '.thai_month_short($m).' '.(intval($y)+543);
}

function convert_dmy_th($date)
{
	if(!empty($date)){
	list($y, $m, $d) = explode('-', $date);
	return $d.'/'.$m.'/'.(intval($y)+543);
	}else{
		return '';
	}
}

function convert_ymd_en($date)
{
	list($d, $m, $y) = explode('/', $date);

	return (intval($y)-543).'-'.$m.'-'.$d;
}

function cal_day_between($date1, $date2)
{
	$d1Object = new DateTime($date1);
	$d2Object = new DateTime($date2);

	$diff = $d1Object->diff($d2Object);

	return $diff->days;

	/* $date1 = strtotime($date1);
	$date2 = strtotime($date2);
	$datediff = $date2 - $date1;

	return floor($datediff / (60 * 60 * 24)); */
}

function eng_display_age($engdate){
	list($y, $m, $d) = explode('-', $engdate);
	$date = $y.'-'.$m.'-'.$d;
	return calc_age($date);
}

function thai_display_age($thaidate){
	list($d, $m, $y) = explode('/', $thaidate);
	$date = (intval($y)-543).'-'.$m.'-'.$d;
	return calc_age($date);
}

function calc_age($dob){

	$dob = date("Y-m-d",strtotime($dob));

	$dobObject = new DateTime($dob);
	$nowObject = new DateTime();

	$diff = $dobObject->diff($nowObject);

	return $diff->y;

}

function age($date)
{
	$dob = explode('-', $date);
	$curMonth = date('m');
	$curDay = date('j');
	$curYear = date('Y');
	$age = $curYear - $dob[0];

	if($curMonth<$dob[1] || ($curMonth==$dob[1] && $curDay<$dob[2])) $age--;

	return $age;
}

function fiscalYear($date)
{
   list($year, $month, $day) = explode('-', $date);
   $cday = mktime(0, 0, 0, $month, $day, $year);

   $d1 = mktime(0, 0, 0, 10, 1, $year );
   $d2 = mktime(0, 0, 0, 9, 30, $year + 1);
   if ($cday >= $d1 && $cday < $d2) $year++;

   return $year + 543;
}
?>
