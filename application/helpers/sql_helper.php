<?php
function forNumber($number)
{
	return str_replace(',', '', $number);
}

function forNumberInt($number)
{
	return intval(forNumber($number));
}
?>