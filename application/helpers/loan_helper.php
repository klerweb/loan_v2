<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * get CI obj
 * @return CI_Controller
 */
function getCI() {
    return $CI = & get_instance();
}

/**
 * date now
 * @return string
 */
function date_now() {
    return date('Y-m-d H:i:s');
}

function date_th_now() {
    $y = date('Y') + 543;
    return date('d/m/') . $y;
}

function amount_format($amount) {
    return number_format($amount, 2);
}

function check_page_permission() {
    $CI = & get_instance();
    $page = $CI->uri->segment(2);

    $loan_id = $_SESSION['session_loan']['loan_model']['loan_id'];
    $person_id = $_SESSION['session_loan']['loan_model']['person_id'];

    if (!isset($_SESSION['session_loan'])) {
        redirect("loan");
    } elseif ($page != 'form_person' && !$person_id) {

        //session_form
        $session_form = array(
            'status' => 'fail',
            'message' => 'ไม่พบข้อมูลผู้ขอสินเชื่อ.'
        );
        $CI->session->set_userdata('session_form', $session_form);

        redirect("loan/edit_loan/{$loan_id}");
    }
}

function view_value($value = '', $show = ' - ') {
    return $value ? $value : $show;
}

function dth_to_den($date){
    if (!$date) {
        return '';
    } else {
        list($d, $m, $y) = explode('/', $date);

        return (intval($y) - 543) . '-' . $m . '-' . $d;
    }
}

function den_to_dth($date) {
    if (!$date) {
        return '';
    } else {
        list($y, $m, $d) = explode('-', $date);
        $y = intval($y) + 543;

        return "{$d}/{$m}/{$y}";
    }
}

function money_num_to_str($money = 0) {
    if(!$money){
        $money = 0;
    }
    return number_format($money, 2);
}

function money_str_to_num($money) {
    $money = str_replace(',', '', $money);
    return $money;
}

function get_max_rai() {
    return '';
}

function get_max_ngan() {
    return 3;
}

function get_max_wa() {
    return 99;
}

function get_max_land() {
    return 5;
}

function calculate_age($person_birthdate) {
    $today = date("Y-m-d");
    $diff = date_diff(date_create($person_birthdate), date_create($today));

    return $diff->format('%y');
}

/* pagination */

function config_pagination() {
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['first_link'] = false;
    $config['last_link'] = false;

    //open - close
    $config['full_tag_open'] = '<div class="span12" style="margin:0px;"><div class="dataTables_paginate paging_bootstrap pagination"><ul>';
    $config['full_tag_close'] = '</ul></div></div>';

    //first-page
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
//    $config['first_link'] = '';
    //last-page
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
//    $config['last_link'] = '';
    //next-page
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['next_link'] = '&gt;';

    //prev-page
    $config['prev_tag_open'] = '<li>';
    $config['prevt_tag_close'] = '</li>';
    $config['prev_link'] = '&lt;';

    //cur-page
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';

    //num-page
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    return $config;
}

function get_per_page($per_page = 10) {
    return $per_page;
}

function title_pagination($page, $per_page, $total_rows) {
    $start = $page == 1 ? 1 : ($page * $per_page) - $per_page + 1;
    $end = $page > $total_rows % $per_page ? $total_rows : $page * $per_page;

    if ($total_rows < $per_page) {
        $end = $total_rows;
    }

//    $title_pagination = "Showing {$start} to {$end} of {$total_rows} entries";
    $title_pagination = "{$start} ถึง {$end} จาก {$total_rows}";

    return $title_pagination;
}

function multi_space($num_space = 1) {
    $str = '';
    for ($i = 0; $i < $num_space; $i++) {
        $str .= '&nbsp;';
    }

    return $str;
}

function multi_dot($num_dot = 10) {
    $str = '';
    for ($i = 0; $i < $num_dot; $i++) {
        $str .= '.';
    }

    return $str;
}

/*
  function date_full($date) {
  $date = date_create($date);
  return date_format($date, "j F Y");
  }

  function date_full2($date) {
  $date = date_create($date);
  return date_format($date, "jMY");
  }

  function date_full3($date) {
  $date = date_create($date);
  return date_format($date, "j M Y");
  }

  function time_format($date) {
  $date = date_create($date);
  return date_format($date, "H:i");
  }

  function user_name() {
  //CI
  $CI = getCI();
  //session
  $session_all = $CI->session->all_userdata();

  if (isset($session_all['session_user'])) {
  return $session_all['session_user']['user_name'];
  } else {
  redirect('auth/login');
  }
  }

  function sel_filter_rows() {
  $rank = array(10, 25, 50, 100);
  return $rank;
  }

  function btn_status($status) {
  //cast
  $status = (int) $status;

  if ($status == 1) {
  $btn = '<span class="label label-success">Active</span>';
  } else {
  $btn = '<span class="label label-important">Inactive</span>';
  }

  return $btn;
  }

  function btn_online($status) {
  //cast
  $status = (int) $status;

  if ($status == 1) {
  $btn = '<span class="label label-success">online</span>';
  } else {
  $btn = '<span class="label label-important">offline</span>';
  }

  return $btn;
  }

  function config_pagination() {
  $config['per_page'] = get_per_page();
  $config['use_page_numbers'] = TRUE;
  $config['enable_query_strings'] = TRUE;
  $config['page_query_string'] = TRUE;
  $config['query_string_segment'] = "page";
  $config['full_tag_open'] = '<div class="pagination pagination-centered"><ul>';
  $config['full_tag_close'] = '</ul></div><!--pagination-->';
  $config['first_link'] = '';
  $config['last_link'] = '';
  $config['next_link'] = 'Next page';
  $config['next_tag_open'] = '<li class="next page">';
  $config['next_tag_close'] = '</li>';
  $config['prev_link'] = 'Previous page';
  $config['prev_tag_open'] = '<li class="prev page">';
  $config['prev_tag_close'] = '</li>';
  $config['cur_tag_open'] = '<li class="active"><a href="">';
  $config['cur_tag_close'] = '</a></li>';
  $config['num_tag_open'] = '<li class="page">';
  $config['num_tag_close'] = '</li>';

  return $config;
  }

  function get_per_page($per_page = 10) {
  return $per_page;
  }

  function title_pagination($page, $per_page, $total_rows) {
  $start = $page == 1 ? 1 : ($page * $per_page) - $per_page + 1;
  $end = $page > $total_rows % $per_page ? $total_rows : $page * $per_page;

  if ($total_rows < $per_page) {
  $end = $total_rows;
  }

  $title_pagination = "Showing {$start} to {$end} of {$total_rows} entries";

  return $title_pagination;
  }

  function is_url_exist($url) {
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_NOBODY, true);
  curl_exec($ch);
  $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

  if ($code == 200) {
  $status = true;
  } else {
  $status = false;
  }
  curl_close($ch);
  return $status;
  }

  function register_status($user_model = NULL) {


  //cast
  if (empty($user_model)) {
  return '<span class="label label-important">Unregister</span>';
  }

  if (empty($user_model->mobile)) {
  return '<span class="label label-important">Unregister</span>';
  }


  return '<span class="label label-success">Registered</span>';
  }

  function company_name($company_id) {

  $CI = getCI();
  $CI->load->model('company_model');

  $company = $CI->company_model->fetchByID($company_id);

  if (empty($company)) {
  return "";
  } else {
  return $company->name;
  }
  }

  function get_label_user_status($status) {
  switch ($status) {
  case 0 : $result = '<span class="user-status-deactivate">Deactivate</span>';
  break;
  case 1 : $result = '<span class="user-status-normal">Normal</span>';
  break;
  case 2 : $result = '<span class="user-status-official">Official</span>';
  break;
  case 3 : $result = '<span class="user-status-blocked">Blocked</span>';
  break;
  default : $result = '';
  break;
  }

  return $result;
  }

  function get_param($get_param, $param_key) {
  $get_param = str_replace('?', '', $get_param);
  $get_param = explode('&', $get_param);
  if ($get_param) {
  foreach ($get_param as $key => $param) {
  if (strpos($param, $param_key . "=") > -1) {
  unset($get_param[$key]);
  }
  }

  $get_param = '?' . implode('&', $get_param);
  }

  return $get_param;
  }

  function show_value($value, $default = '-') {
  return $value ? $value : $default;
  }

  function config_upload_photo($resource_path) {
  $config['upload_path'] = $resource_path;
  $config['allowed_types'] = 'gif|jpg|png';
  $config['max_size'] = '1000';
  $config['max_width'] = '10240';
  $config['max_height'] = '7680';

  return $config;
  }
 */
?>
