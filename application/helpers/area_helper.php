<?php
function area($rai, $ngan, $wah)
{
	$ngan_w = floor($wah / 100);
	$array['wah'] = number_format($wah % 100,  1) + ($wah  - floor($wah));
	
	$ngan += $ngan_w;
	$rai_n = floor($ngan / 4);
	$array['ngan'] = $ngan % 4;
	
	$array['rai'] = $rai + $rai_n;
	
	return $array;
}
?>