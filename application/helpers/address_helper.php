<?php
function show_address($fix_name,$array)
{
	$str = '';
	if(!empty($array[$fix_name.'_no'])){
		$str .= ' เลขที่ '.$array[$fix_name.'_no'];
	}
	if(!empty($array[$fix_name.'_moo'])){
		$str .= ' หมู่ที่ '.$array[$fix_name.'_moo'];
	}
	if(!empty($array[$fix_name.'_soi'])){
		$str .= ' ซอย '.$array[$fix_name.'_soi'];
	}
	if(!empty($array[$fix_name.'_road'])){
		$str .= ' ถนน '.$array[$fix_name.'_road'];
	}
	if(!empty($array[$fix_name.'_district_name'])){
		$str .= ' ตำบล '.$array[$fix_name.'_district_name'];
	}
	if(!empty($array[$fix_name.'_amphur_name'])){
		$str .= ' อำเภอ '.$array[$fix_name.'_amphur_name'];
	}
	if(!empty($array[$fix_name.'_province_name'])){
		$str .= ' จังหวัด '.$array[$fix_name.'_province_name'];
	}	
	return $str;
}

function redirect_url($url='',$msg='')
{
	echo '<script type="text/javascript">';
	if(!empty($msg)) { echo 'alert("'.$msg.'");';}
	echo 'window.location.href = "'.$url.'";
		</script>';
}

?>