<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>สถาบันบริหารจัดการธนาคารที่ดิน (องค์การมหาชน)</title>

        <link href="<?php echo base_url(); ?>assets/css/style.default.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/select2.css" rel="stylesheet" />
		<link href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.css" rel="stylesheet" />
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- CSS Other -->
        <?php if (isset($css_other)): ?>
            <?php foreach ($css_other as $css): ?>
                <link href="<?php echo base_url("assets/css/{$css}"); ?>" rel="stylesheet">
            <?php endforeach; ?>
        <?php endif; ?>
        
         <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/bootstrap-wizard.min.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/additional-methods.js"></script>
         <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
        <script type="text/javascript">
        	var base_url = "<?php  echo base_url(); ?>";
        </script>
    </head>
    <body>
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <div class="pull-right">
                        <a href="#" class="menu-collapse"><i class="fa fa-bars"></i></a>
                    </div>
                </div><!-- header-left -->
                <div class="header-right">
                    <div class="pull-right">
                        <div class="btn-group btn-group-list btn-group-notification">
                        <?php //$this->load->view('header-notify'); ?> 
                        </div><!-- btn-group -->
                        <div class="btn-group btn-group-option">
                            <?php $this->load->view('header-user'); ?>
                        </div><!-- btn-group -->
                    </div><!-- pull-right -->
                </div><!-- header-right -->
            </div><!-- headerwrapper -->
        </header>

        <section>
            <div class="mainwrapper">
                <div class="leftpanel">
                    <div class="media profile-left">
                        <?php $this->load->view('panel-profile'); ?>
                    </div><!-- media -->

                    {menu}

                </div><!-- leftpanel -->

                <div class="mainpanel">
                    <div class="pageheader">
                        <div class="media">
                            <div class="pageicon pull-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="media-body">
                                {breadcrumb}
                                <h4>{title}</h4>
                            </div>
                        </div><!-- media -->
                    </div><!-- pageheader -->

                    <div class="contentpanel">

                        {content}

                    </div><!-- contentpanel -->

                </div><!-- mainpanel -->
            </div><!-- mainwrapper -->
        </section>
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.min.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/pace.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/retina.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.cookies.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>        
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>    
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
        
        <!-- JS Other -->
        <?php if (isset($js_other)): ?>
            <?php foreach ($js_other as $js): ?>
                <script src="<?php echo base_url("assets/js/{$js}"); ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>

    </body>
</html>