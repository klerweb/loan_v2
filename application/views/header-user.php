<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-caret-down"></i>
</button>
<ul class="dropdown-menu pull-right" role="menu">
<li><a href="<?php echo base_url().'user/profile'?>"><i class="glyphicon glyphicon-user"></i> My Profile</a></li> 
<li><a href="<?php echo base_url().'user/edit_password'?>"><i class="glyphicon glyphicon-edit"></i> Edit Password</a></li> 
<li class="divider"></li>
<li><a href="<?php echo base_url().'home/logout'?>"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li>
</ul>