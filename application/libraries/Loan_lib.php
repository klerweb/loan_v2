<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_lib {
    ##### person #####

    function person_save($post, $person_id = '') {
        $CI = & get_instance();
        $CI->load->model('person_model');

        //person_data
        $person_data = array(
            'title_id' => $post['title_id'],
            'person_fname' => $post['person_fname'],
            'person_lname' => $post['person_lname'],
            'person_birthdate' => dth_to_den($post['person_birthdate']),
            'sex_id' => isset($post['sex_id']) ? $this->check_val_null($post['sex_id']) : NULL,
            'race_id' => $this->check_val_null($post['race_id']),
            'nationality_id' => $this->check_val_null($post['nationality_id']),
            'religion_id' => $this->check_val_null($post['religion_id']),
            'person_thaiid' => $this->check_val_null($post['person_thaiid']),
            'person_card_startdate' => $this->check_val_null($post['person_card_startdate']) ? dth_to_den($post['person_card_startdate']) : NULL,
            'person_card_expiredate' => $this->check_val_null($post['person_card_expiredate']) ? dth_to_den($post['person_card_expiredate']) : NULL,
            'person_addr_card_no' => $this->check_val_null($post['person_addr_card_no']),
            'person_addr_card_moo' => $this->check_val_null($post['person_addr_card_moo']),
            'person_addr_card_road' => $this->check_val_null($post['person_addr_card_road']),
            'person_addr_card_district_id' => $this->check_val_null($post['person_addr_card_district_id']),
            'person_addr_card_amphur_id' => $this->check_val_null($post['person_addr_card_amphur_id']),
            'person_addr_card_province_id' => $this->check_val_null($post['person_addr_card_province_id']),
            'person_addr_card_zipcode' => isset($post['person_addr_card_zipcode']) ? $this->check_val_null($post['person_addr_card_zipcode']) : '',
            'person_phone' => $this->check_val_null($post['person_phone']),
            'person_email' => $this->check_val_null($post['person_email']),
            'person_mobile' => $this->check_val_null($post['person_mobile']),
            'career_id' => $this->check_val_null($post['career_id']),
            'person_career_other' => $this->check_val_null($post['person_career_other']),
            'status_married_id' => isset($post['status_married_id']) ? $post['status_married_id'] : 0,
            'person_addr_same' => $this->check_val_null($post['person_addr_same']),
        	 'person_card_expire' => $this->check_val_null($post['person_card_expire']),
        );

        //person_income_per_month
        if (isset($post['person_income_per_month'])) {
            $person_data['person_income_per_month'] = $this->check_val_null(money_str_to_num($post['person_income_per_month']));
        }
        //person_expenditure_per_month
        if (isset($post['person_expenditure_per_month'])) {
            $person_data['person_expenditure_per_month'] = $this->check_val_null(money_str_to_num($post['person_expenditure_per_month']));
        }


        //person_position
        if (isset($post['person_position'])) {
            $person_data['person_position'] = $this->check_val_null($post['person_position']);
        }
        //person_job_desc
        if (isset($post['person_job_desc'])) {
            $person_data['person_job_desc'] = $this->check_val_null($post['person_job_desc']);
        }
        //person_type_business
        if (isset($post['person_type_business'])) {
            $person_data['person_type_business'] = $this->check_val_null($post['person_type_business']);
        }
        //person_belong
        if (isset($post['person_belong'])) {
            $person_data['person_belong'] = $this->check_val_null($post['person_belong']);
        }
        //person_ministry
        if (isset($post['person_ministry'])) {
            $person_data['person_ministry'] = $this->check_val_null($post['person_ministry']);
        }
        //person_workplace
        if (isset($post['person_workplace'])) {
            $person_data['person_workplace'] = $this->check_val_null($post['person_workplace']);
        }
        //person_cardid_emp
        if (isset($post['person_cardid_emp'])) {
            $person_data['person_cardid_emp'] = $this->check_val_null($post['person_cardid_emp']);
        }
        //person_cardid_emp_startdate
        if (isset($post['person_cardid_emp_startdate'])) {
            $person_data['person_cardid_emp_startdate'] = $this->check_val_null($post['person_cardid_emp_startdate']) ? dth_to_den($post['person_cardid_emp_startdate']) : NULL;
        }
        //person_cardid_emp_expiredate
        if (isset($post['person_cardid_emp_expiredate'])) {
            $person_data['person_cardid_emp_expiredate'] = $this->check_val_null($post['person_cardid_emp_expiredate']) ? dth_to_den($post['person_cardid_emp_expiredate']) : NULL;
        }

        //person_loan_other_per_month
        if (isset($post['person_loan_other_per_month'])) {
            $person_data['person_loan_other_per_month'] = $post['loan_other_check'] == 1 ? $this->check_val_null($post['person_loan_other_per_month']) : NULL;
        }
        //person_loan_other_per_year
        if (isset($post['person_loan_other_per_year'])) {
            $person_data['person_loan_other_per_year'] = $post['loan_other_check'] == 1 ? $this->check_val_null($post['person_loan_other_per_year']) : NULL;
        }
        //person_guarantee_other_amount
        if (isset($post['person_guarantee_other_amount'])) {
            $person_data['person_guarantee_other_amount'] = $post['guarantee_other_check'] == 1 ? $this->check_val_null($post['person_guarantee_other_amount']) : NULL;
        }

        //person_addr_pre
        if (isset($post['person_addr_same']) && $post['person_addr_same'] == 1) {
            $person_addr_pre = array(
                'person_addr_pre_no' => $this->check_val_null($post['person_addr_card_no']),
                'person_addr_pre_moo' => $this->check_val_null($post['person_addr_card_moo']),
                'person_addr_pre_road' => $this->check_val_null($post['person_addr_card_road']),
                'person_addr_pre_district_id' => $this->check_val_null($post['person_addr_card_district_id']),
                'person_addr_pre_amphur_id' => $this->check_val_null($post['person_addr_card_amphur_id']),
                'person_addr_pre_province_id' => $this->check_val_null($post['person_addr_card_province_id']),
                'person_addr_pre_zipcode' => isset($post['person_addr_card_zipcode']) ? $this->check_val_null($post['person_addr_card_zipcode']) : '',
            );
        } else {
            $person_addr_pre = array(
                'person_addr_pre_no' => $this->check_val_null($post['person_addr_pre_no']),
                'person_addr_pre_moo' => $this->check_val_null($post['person_addr_pre_moo']),
                'person_addr_pre_road' => $this->check_val_null($post['person_addr_pre_road']),
                'person_addr_pre_district_id' => $this->check_val_null($post['person_addr_pre_district_id']),
                'person_addr_pre_amphur_id' => $this->check_val_null($post['person_addr_pre_amphur_id']),
                'person_addr_pre_province_id' => $this->check_val_null($post['person_addr_pre_province_id']),
                'person_addr_pre_zipcode' => isset($post['person_addr_pre_zipcode']) ? $this->check_val_null($post['person_addr_pre_zipcode']) : '',
            );
        }

        //person_address_type
        if (isset($post['person_address_type'])) {
            if ($post['person_address_type'] == 1) {
                $person_data['person_address_type'] = $this->check_val_null($post['person_address_type']);
                $person_data['person_address_type_other'] = NULL;
                $person_data['person_land_type_id'] = $this->check_val_null($post['person_land_type_id']);
                $person_data['person_land_no'] = $this->check_val_null($post['person_land_no']);
                $person_data['person_land_addr_no'] = $this->check_val_null($post['person_land_addr_no']);
                $person_data['person_land_addr_province_id'] = $this->check_val_null($post['person_land_addr_province_id']);
                $person_data['person_land_addr_amphur_id'] = $this->check_val_null($post['person_land_addr_amphur_id']);
                $person_data['person_land_addr_district_id'] = $this->check_val_null($post['person_land_addr_district_id']);
                $person_data['person_land_area_rai'] = $this->check_val_null($post['person_land_area_rai']);
                $person_data['person_land_area_ngan'] = $this->check_val_null($post['person_land_area_ngan']);
                $person_data['person_land_area_wah'] = $this->check_val_null($post['person_land_area_wah']);
                $person_data['person_owner_building_title_id'] = $this->check_val_null($post['person_owner_building_title_id']);
                $person_data['person_owner_building_fname'] = $this->check_val_null($post['person_owner_building_fname']);
                $person_data['person_owner_building_lname'] = $this->check_val_null($post['person_owner_building_lname']);
                $person_data['person_owner_building_note'] = $this->check_val_null($post['person_owner_building_note']);
            } elseif ($post['person_address_type'] == 3) {
                $person_data['person_address_type'] = $this->check_val_null($post['person_address_type']);
                $person_data['person_address_type_other'] = NULL;
                $person_data['person_land_type_id'] = NULL;
                $person_data['person_land_no'] = NULL;
                $person_data['person_land_addr_no'] = NULL;
                $person_data['person_land_addr_province_id'] = NULL;
                $person_data['person_land_addr_amphur_id'] = NULL;
                $person_data['person_land_addr_district_id'] = NULL;
                $person_data['person_land_area_rai'] = NULL;
                $person_data['person_land_area_ngan'] = NULL;
                $person_data['person_land_area_wah'] = NULL;
                $person_data['person_owner_building_title_id'] = $this->check_val_null($post['person_owner_building_title_id']);
                $person_data['person_owner_building_fname'] = $this->check_val_null($post['person_owner_building_fname']);
                $person_data['person_owner_building_lname'] = $this->check_val_null($post['person_owner_building_lname']);
                $person_data['person_owner_building_note'] = $this->check_val_null($post['person_owner_building_note']);
            } else {
                $person_data['person_address_type'] = $this->check_val_null($post['person_address_type']);
                $person_data['person_address_type_other'] = NULL;
                $person_data['person_land_type_id'] = NULL;
                $person_data['person_land_no'] = NULL;
                $person_data['person_land_addr_no'] = NULL;
                $person_data['person_land_addr_province_id'] = NULL;
                $person_data['person_land_addr_amphur_id'] = NULL;
                $person_data['person_land_addr_district_id'] = NULL;
                $person_data['person_land_area_rai'] = NULL;
                $person_data['person_land_area_ngan'] = NULL;
                $person_data['person_land_area_wah'] = NULL;
                $person_data['person_owner_building_note'] = $this->check_val_null($post['person_owner_building_note']);
            }
        }

        //merge $person_data, $person_addr_pre
        $person_data = array_merge($person_data, $person_addr_pre);

        if ($person_data['person_thaiid']) {
            /* insert person */
            $person_model = $CI->person_model->save($person_data, $person_id);
            $person_id = $person_model['id'];
        } else {
            $person_id = 0;
        }

        return $person_id;

        exit;
        ///// new person_id
        if ($person_id) {
            /* update person */
            $CI->person_model->save(array('person_status' => 0), $person_id);
        }

        if ($person_data['person_thaiid']) {
            /* insert person */
            $person_model = $CI->person_model->save($person_data);
            $person_id = $person_model['id'];
        } else {
            $person_id = 0;
        }

        return $person_id;
    }

    function spouse_save($post, $post_person, $person_id = '') {
        $CI = & get_instance();
        $CI->load->model('person_model');

        //person_data
        $person_data = array(
            'title_id' => $post['title_id'],
            'person_fname' => $post['person_fname'],
            'person_lname' => $post['person_lname'],
            'person_birthdate' => dth_to_den($post['person_birthdate']),
            'sex_id' => NULL,
            'race_id' => NULL,
            'nationality_id' => NULL,
            'religion_id' => NULL,
            'person_thaiid' => $this->check_val_null($post['person_thaiid']),
            'person_card_startdate' => $this->check_val_null($post['person_card_startdate']) ? dth_to_den($post['person_card_startdate']) : NULL,
            'person_card_expiredate' => $this->check_val_null($post['person_card_expiredate']) ? dth_to_den($post['person_card_expiredate']) : NULL,
            'career_id' => $this->check_val_null($post['career_id']),
            'person_career_other' => $this->check_val_null($post['person_career_other']),
            'status_married_id' => isset($post_person['status_married_id']) ? $this->check_val_null($post_person['status_married_id']) : NULL,
            'person_phone' => $this->check_val_null($post['person_phone']),
            'person_email' => $this->check_val_null($post['person_email']),
            'person_mobile' => $this->check_val_null($post['person_mobile']),
            'person_card_expire' => $this->check_val_null($post['person_addr_same']),
        	'person_card_expire' => $this->check_val_null($post['person_card_expire']),
        );

        //person_addr_pre
        if (isset($post['person_addr_same']) && $post['person_addr_same'] == 1) {
            if (isset($post_person['person_addr_same']) && $post_person['person_addr_same'] == 1) {
                $person_addr_pre = array(
                    'person_addr_pre_no' => $this->check_val_null($post_person['person_addr_card_no']),
                    'person_addr_pre_moo' => $this->check_val_null($post_person['person_addr_card_moo']),
                    'person_addr_pre_road' => $this->check_val_null($post_person['person_addr_card_road']),
                    'person_addr_pre_district_id' => $this->check_val_null($post_person['person_addr_card_district_id']),
                    'person_addr_pre_amphur_id' => $this->check_val_null($post_person['person_addr_card_amphur_id']),
                    'person_addr_pre_province_id' => $this->check_val_null($post_person['person_addr_card_province_id']),
                    'person_addr_pre_zipcode' => $this->check_val_null($post_person['person_addr_pre_zipcode']),
                );
            } else {
                $person_addr_pre = array(
                    'person_addr_pre_no' => $this->check_val_null($post_person['person_addr_pre_no']),
                    'person_addr_pre_moo' => $this->check_val_null($post_person['person_addr_pre_moo']),
                    'person_addr_pre_road' => $this->check_val_null($post_person['person_addr_pre_road']),
                    'person_addr_pre_district_id' => $this->check_val_null($post_person['person_addr_pre_district_id']),
                    'person_addr_pre_amphur_id' => $this->check_val_null($post_person['person_addr_pre_amphur_id']),
                    'person_addr_pre_province_id' => $this->check_val_null($post_person['person_addr_pre_province_id']),
                    'person_addr_pre_zipcode' => $this->check_val_null($post_person['person_addr_pre_zipcode']),
                );
            }
        } else {
            $person_addr_pre = array(
                'person_addr_pre_no' => $this->check_val_null($post['person_addr_pre_no']),
                'person_addr_pre_moo' => $this->check_val_null($post['person_addr_pre_moo']),
                'person_addr_pre_road' => $this->check_val_null($post['person_addr_pre_road']),
                'person_addr_pre_district_id' => $this->check_val_null($post['person_addr_pre_district_id']),
                'person_addr_pre_amphur_id' => $this->check_val_null($post['person_addr_pre_amphur_id']),
                'person_addr_pre_province_id' => $this->check_val_null($post['person_addr_pre_province_id']),
                'person_addr_pre_zipcode' => $this->check_val_null($post['person_addr_pre_zipcode']),
            );
        }

        //merge $person_data, $person_addr_pre
        $person_data = array_merge($person_data, $person_addr_pre);

        if ($person_id) {
            /* update person */
            $CI->person_model->save(array('person_status' => 0), $person_id);
        }

        if ($post['person_fname'] && ['person_lname']) {
            /* insert person */
            $person_model = $CI->person_model->save($person_data, $person_id);
            $person_id = $person_model['id'];
        } else {
            $person_id = 0;
        }

        return $person_id;
    }

    function person_land_save($post, $person_id = '') {
        $CI = & get_instance();
        $CI->load->model('person_model');

        //person_data
        $person_data = array(
            'title_id' => $post['title_id'],
            'person_fname' => $post['person_fname'],
            'person_lname' => $post['person_lname'],
            'person_phone' => $post['person_phone'],
            'person_mobile' => $this->check_val_null($post['person_mobile']),
            'person_birthdate' => NULL,
            'sex_id' => NULL,
            'race_id' => NULL,
            'nationality_id' => NULL,
            'religion_id' => NULL,
            'person_thaiid' => NULL,
            'person_card_startdate' => NULL,
            'person_card_expiredate' => NULL,
            'person_addr_pre_no' => $this->check_val_null($post['person_addr_pre_no']),
            'person_addr_pre_moo' => $this->check_val_null($post['person_addr_pre_moo']),
            'person_addr_pre_road' => $this->check_val_null($post['person_addr_pre_road']),
            'person_addr_pre_district_id' => $this->check_val_null($post['person_addr_pre_district_id']),
            'person_addr_pre_amphur_id' => $this->check_val_null($post['person_addr_pre_amphur_id']),
            'person_addr_pre_province_id' => $this->check_val_null($post['person_addr_pre_province_id']),
        	'person_card_expire' => $this->check_val_null($post['person_card_expire']),
        );


        if ($person_id) {
            /* update person */
            $CI->person_model->save(array('person_status' => 0), $person_id);
        }

        if ($post['person_fname'] && ['person_lname']) {
            /* insert person */
            $person_model = $CI->person_model->save($person_data);
            $person_id = $person_model['id'];
        } else {
            $person_id = 0;
        }

        return $person_id;
    }

    function person_land2_save($post, $person_id = '') {
        $CI = & get_instance();
        $CI->load->model('person_model');

        //person_data
        $person_data = array(
            'title_id' => $post['title_id'],
            'person_fname' => $post['person_fname'],
            'person_lname' => $post['person_lname'],
            'person_phone' => NULL,
            'person_mobile' => NULL,
            'person_birthdate' => NULL,
            'sex_id' => NULL,
            'race_id' => NULL,
            'nationality_id' => NULL,
            'religion_id' => NULL,
            'person_thaiid' => NULL,
            'person_card_startdate' => NULL,
            'person_card_expiredate' => NULL,
        	'person_card_expire' => NULL
        );


        if ($person_id) {
            /* update person */
            $CI->person_model->save(array('person_status' => 0), $person_id);
        }

        if ($post['person_fname'] && ['person_lname']) {
            /* insert person */
            $person_model = $CI->person_model->save($person_data);
            $person_id = $person_model['id'];
        } else {
            $person_id = 0;
        }

        return $person_id;
    }

    function person_fields() {
        $person_fields = array(
            'person_id' => '',
            'title_id' => '',
            'person_fname' => '',
            'person_lname' => '',
            'person_birthdate' => '',
            'sex_id' => '',
            'sex_name' => '',
            'race_id' => 1,
            'race_name' => '',
            'nationality_id' => 1,
            'nationality_name' => '',
            'religion_id' => 1,
            'religion_name' => '',
            'person_thaiid' => '',
            'person_thaiid_format' => '',
            'person_card_startdate' => '',
            'person_card_expiredate' => '',
            'person_addr_card_no' => '',
            'person_addr_card_moo' => '',
            'person_addr_card_road' => '',
            'person_addr_card_district_id' => '',
            'person_addr_card_amphur_id' => '',
            'person_addr_card_province_id' => '',
            'person_addr_card_district_name' => '',
            'person_addr_card_amphur_name' => '',
            'person_addr_card_province_name' => '',
            'person_addr_card_zipcode' => '',
            'person_addr_pre_no' => '',
            'person_addr_pre_moo' => '',
            'person_addr_pre_road' => '',
            'person_addr_pre_district_id' => '',
            'person_addr_pre_amphur_id' => '',
            'person_addr_pre_province_id' => '',
            'person_addr_pre_district_name' => '',
            'person_addr_pre_amphur_name' => '',
            'person_addr_pre_province_name' => '',
            'person_addr_pre_zipcode' => '',
            'person_phone' => '',
            'person_mobile' => '',
            'person_email' => '',
            'career_id' => '',
            'career_name' => '',
            'person_career_other' => '',
            'status_married_id' => '',
            'status_married_name' => '',
            'relationship_id' => '',
            'relationship_name' => '',
            'person_income_per_month' => money_num_to_str(0),
            'person_expenditure_per_month' => money_num_to_str(0),
            'person_position' => '',
            'person_job_desc' => '',
            'person_type_business' => '',
            'person_belong' => '',
            'person_ministry' => '',
            'person_workplace' => '',
            'person_cardid_emp' => '',
            'person_cardid_emp_startdate' => '',
            'person_cardid_emp_expiredate' => '',
            'person_loan_other_per_month' => '',
            'person_loan_other_per_year' => '',
            'person_guarantee_other_amount' => '',
            'person_land_type_id' => '',
            'person_land_type_name' => '',
            'person_land_no' => '',
            'person_land_addr_no' => '',
            'person_land_addr_province_id' => '',
            'person_land_addr_amphur_id' => '',
            'person_land_addr_district_id' => '',
            'person_land_addr_province_name' => '',
            'person_land_addr_amphur_name' => '',
            'person_land_addr_district_name' => '',
            'person_land_area_rai' => '',
            'person_land_area_ngan' => '',
            'person_land_area_wah' => '',
            'person_address_type' => '',
            'person_address_type_other' => '',
            'person_land_type_id' => '',
            'person_land_no' => '',
            'person_land_addr_no' => '',
            'person_land_addr_province_id' => '',
            'person_land_addr_amphur_id' => '',
            'person_land_addr_district_id' => '',
            'person_land_addr_province_name' => '',
            'person_land_addr_amphur_name' => '',
            'person_land_addr_district_name' => '',
            'person_land_area_rai' => '',
            'person_land_area_ngan' => '',
            'person_land_area_wah' => '',
            'person_owner_building_title_id' => '',
            'person_owner_building_title_name' => '',
            'person_owner_building_fname' => '',
            'person_owner_building_lname' => '',
            'person_owner_building_note' => '',
            'title_name' => '',
        	  'person_card_expire' => '',
            'person_addr_same' => ''
        );

        return $person_fields;
    }

    function person_detail($person_id) {
        $CI = & get_instance();
        $CI->load->model('person_model');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');

        $person_model = $CI->person_model->get_view($person_id);

        if (!$person_model) {
            $person_model = $this->person_fields();
        }

        $person_model['person_birthdate_thai'] = thai_display_date($person_model['person_birthdate']);
        $person_model['person_card_startdate_thai'] = thai_display_date($person_model['person_card_startdate']);
        $person_model['person_card_expiredate_thai'] = thai_display_date($person_model['person_card_expiredate']);
        $person_model['person_age'] = calculate_age($person_model['person_birthdate']);
        $person_model['person_birthdate'] = den_to_dth($person_model['person_birthdate']);
        $person_model['person_card_startdate'] = den_to_dth($person_model['person_card_startdate']);
        $person_model['person_card_expiredate'] = den_to_dth($person_model['person_card_expiredate']);
        $person_model['person_fullname'] = "{$person_model['title_name']}{$person_model['person_fname']} {$person_model['person_lname']}";

        return $person_model;
    }

    ##### land #####

    function land_save($post) {
        $CI = & get_instance();
        $CI->load->model('land_model');
        $CI->load->model('land_owner_model');

        //land_model
        $post_land_model = array(
            'loan_type_id' => $this->check_val_null($post['loan_type_id']),
            'land_no' => $this->check_val_null($post['land_no']),
            'land_area_rai' => $this->check_val_null($post['land_area_rai']),
            'land_area_ngan' => $this->check_val_null($post['land_area_ngan']),
            'land_area_wah' => $this->check_val_null($post['land_area_wah']),
            'land_addr_no' => $this->check_val_null($post['land_addr_no']),
            'land_addr_moo' => $this->check_val_null($post['land_addr_moo']),
            'land_addr_district_id' => $this->check_val_null($post['land_addr_district_id']),
            'land_addr_amphur_id' => $this->check_val_null($post['land_addr_amphur_id']),
            'land_addr_province_id' => $this->check_val_null($post['land_addr_province_id']),
            'land_buiding_detail_id' => isset($post['land_buiding_detail_id'])?$post['land_buiding_detail_id']:null,
            'land_type_id' => $this->check_val_null($post['land_type_id']),
        	  'land_estimate_price' => isset($post['land_estimate_price'])?$post['land_estimate_price']:null
        );

        //ราคาประเมิณ
        if (isset($post['land_record_calculate'])) {
            $post_land_model['land_record_calculate'] = money_str_to_num($post['land_record_calculate']);
        }

        $land_model = $CI->land_model->save($post_land_model, $post['land_id']);
        $land_id = $post['land_id'] ? $post['land_id'] : $land_model['id'];

        //land_owner
        $land_owner = $post['land_owner'];
        if ($land_owner) {
            $land_owner_opt = array('land_id' => $land_id, 'person_id' => $land_owner);
            $land_owner_model = $CI->land_owner_model->get_row_by_opt($land_owner_opt);
            if (!$land_owner_model) {
                $land_owner_model = $CI->land_owner_model->save($land_owner_opt);
                $land_owner_id = $land_owner_model['id'];
            }
        }

        return $land_id;
    }

    function land_save_v2($post, $loan_id = NULL, $owner_id = NULL) {
        $CI = & get_instance();
        $CI->load->model('land_model');
        $CI->load->model('land_owner_model');

        //$loan_id, $owner_id , $land_id
        $land_id = $post['land_id'];

        //land_model
        $post_land_model = array(
            'loan_type_id' => $this->check_val_null($post['loan_type_id']),
            'land_no' => $this->check_val_null($post['land_no']),
            'land_area_rai' => $this->check_val_null($post['land_area_rai']),
            'land_area_ngan' => $this->check_val_null($post['land_area_ngan']),
            'land_area_wah' => $this->check_val_null($post['land_area_wah']),
            'land_addr_no' => $this->check_val_null($post['land_addr_no']),
            'land_addr_moo' => $this->check_val_null($post['land_addr_moo']),
            'land_addr_district_id' => $this->check_val_null($post['land_addr_district_id']),
            'land_addr_amphur_id' => $this->check_val_null($post['land_addr_amphur_id']),
            'land_addr_province_id' => $this->check_val_null($post['land_addr_province_id']),
            'land_buiding_detail_id' => $this->check_val_null($post['land_buiding_detail_id']),
            'land_type_id' => $this->check_val_null($post['land_type_id']),
        	'land_estimate_price' => $this->check_val_null($post['land_estimate_price'])
        );

        $land_model = $CI->land_model->save($post_land_model, $post['land_id']);
        $land_id = $land_id ? $land_id : $land_model['id'];

        //land_owner_model
        if ($post['land_owner_id']) {
            //update land_owner_model
            $post_land_owner_model = array(
                'person_id' => $owner_id,
            );
            $land_owner_model = $CI->land_owner_model->save($post_land_owner_model, $post['land_owner_id']);
            $land_owner_id = $land_owner_model['id'];
        } else {
            //insert land_owner_model
            $post_land_owner_model = array(
                'land_id' => $land_id,
                'person_id' => $owner_id,
            );
            $land_owner_model = $CI->land_owner_model->save($post_land_owner_model);
            $land_owner_id = $land_owner_model['id'];
        }

        //loan_guarantee_land
        $loan_guarantee_land_id = '';
        $post_loan_guarantee_land_model = array(
            'loan_id' => $loan_id,
            'land_id' => $land_id,
        );

        if (isset($post['loan_guarantee_land_bind_type'])) {
            $post_loan_guarantee_land_model['loan_guarantee_land_bind_type'] = $post['loan_guarantee_land_bind_type'];
            $post_loan_guarantee_land_model['loan_guarantee_land_benefit'] = $post['loan_guarantee_land_benefit'];
            $post_loan_guarantee_land_model['loan_guarantee_land_benefit_other'] = $post['loan_guarantee_land_benefit_other'];

            if ($post['loan_guarantee_land_bind_type'] == 1) {
                $post_loan_guarantee_land_model['loan_guarantee_land_title_id'] = $post['loan_guarantee_land_title_id'];
                $post_loan_guarantee_land_model['loan_guarantee_land_fname'] = $post['loan_guarantee_land_fname'];
                $post_loan_guarantee_land_model['loan_guarantee_land_lname'] = $post['loan_guarantee_land_lname'];
                $post_loan_guarantee_land_model['loan_guarantee_land_bind_amount'] = $post['loan_guarantee_land_bind_amount'];
                $post_loan_guarantee_land_model['loan_guarantee_land_relief'] = $post['loan_guarantee_land_relief'];
                $post_loan_guarantee_land_model['loan_guarantee_land_relief_permonth'] = $post['loan_guarantee_land_relief_permonth'];
                $post_loan_guarantee_land_model['loan_guarantee_land_relief_enddate'] = $post['loan_guarantee_land_relief_enddate'];
            } else {
                $post_loan_guarantee_land_model['loan_guarantee_land_title_id'] = NULL;
                $post_loan_guarantee_land_model['loan_guarantee_land_fname'] = NULL;
                $post_loan_guarantee_land_model['loan_guarantee_land_lname'] = NULL;
                $post_loan_guarantee_land_model['loan_guarantee_land_bind_amount'] = NULL;
                $post_loan_guarantee_land_model['loan_guarantee_land_relief'] = NULL;
                $post_loan_guarantee_land_model['loan_guarantee_land_relief_permonth'] = NULL;
                $post_loan_guarantee_land_model['loan_guarantee_land_relief_enddate'] = NULL;
            }
        } else {
            $post_loan_guarantee_land_model['loan_guarantee_land_bind_type'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_benefit'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_benefit_other'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_title_id'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_fname'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_lname'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_bind_amount'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_relief'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_relief_permonth'] = NULL;
            $post_loan_guarantee_land_model['loan_guarantee_land_relief_enddate'] = NULL;
        }

        $loan_guarantee_land_model = $this->loan_guarantee_land_model->save($post_loan_guarantee_land_model, $loan_guarantee_land_id);
        $loan_guarantee_land_id = $loan_guarantee_land_model['id'];

        return $land_id;
    }

    function land_fields() {
        $land_fields = array(
            'loan_id' => '',
            'loan_type_id' => '',
            'land_type_id' => '',
            'loan_objective_id' => '',
            'loan_type_redeem_type' => '',
            'loan_type_redeem_case' => '',
            'loan_type_redeem_owner' => '',
            'loan_type_redeem_relationship' => '',
            'loan_type_contract_case' => '',
            'loan_type_contract_no' => '',
            'loan_type_contract_date' => '',
            'loan_type_contract_creditor' => '',
            'loan_type_contract_borrowers' => '',
            'loan_type_case_black_no' => '',
            'loan_type_case_red_no' => '',
            'loan_type_case_court' => '',
            'loan_type_case_date' => '',
            'loan_type_case_type' => '',
            'loan_type_case_plaintiff' => '',
            'loan_type_case_defendant' => '',
            'loan_type_case_case' => '',
            'loan_type_case_comment' => '',
            'loan_type_sale_date' => '',
            'loan_type_sale_relationship' => '',
            'loan_type_farm_for' => '',
            'loan_type_farm_location' => '',
            'loan_type_farm_project' => '',
            'loan_type_farm_expenditure' => '',
            'loan_type_farm_income_yearstart' => '',
            'loan_type_farm_income_per_year' => money_num_to_str(0),
            'loan_type_farm_payment_yearstart' => '',
            'loan_type_farm_payment_amount' => money_num_to_str(0),
            'loan_type_farm_year_amount' => '',
            'land_id' => '',
            'land_no' => '',
            'land_area_rai' => '',
            'land_area_ngan' => '',
            'land_area_wah' => '',
            'land_addr_no' => '',
            'land_addr_moo' => '',
            'land_addr_district_id' => '',
            'land_addr_amphur_id' => '',
            'land_addr_province_id' => '',
            'land_buiding_detail_id' => '',
        	'land_estimate_price' => ''
        );

        return $land_fields;
    }

    function land_occupy_save($post) {
        $CI = & get_instance();
        $CI->load->model('land_model');
        $CI->load->model('land_owner_model');
//        echo '<pre>';
//        print_r($post);
//        echo '</pre>';
//        exit;
        //land_model
        $post_land_model = array(
//            'loan_type_id' => $this->check_val_null($post['loan_type_id']),
            'land_no' => $this->check_val_null($post['land_no']),
            'land_area_rai' => $this->check_val_null($post['land_area_rai']),
            'land_area_ngan' => $this->check_val_null($post['land_area_ngan']),
            'land_area_wah' => $this->check_val_null($post['land_area_wah']),
            'land_addr_no' => $this->check_val_null($post['land_addr_no']),
            'land_addr_moo' => $this->check_val_null($post['land_addr_moo']),
            'land_addr_district_id' => $this->check_val_null($post['land_addr_district_id']),
            'land_addr_amphur_id' => $this->check_val_null($post['land_addr_amphur_id']),
            'land_addr_province_id' => $this->check_val_null($post['land_addr_province_id']),
//            'land_buiding_detail_id' => $this->check_val_null($post['land_buiding_detail_id']),
            'land_type_id' => $this->check_val_null($post['land_type_id']),
        	'land_estimate_price' => $this->check_val_null($post['estimate_price'])
        );

        //ราคาประเมิณ
//        if(isset($post['land_record_calculate'])){
//            $post_land_model['land_record_calculate'] = money_str_to_num($post['land_record_calculate']);
//        }

        $land_model = $CI->land_model->save($post_land_model, $post['land_id']);
        $land_id = $post['land_id'] ? $post['land_id'] : $land_model['id'];

        //land_owner
//        $land_owner = $post['land_owner'];
//        if ($land_owner) {
//            $land_owner_opt = array('land_id' => $land_id, 'person_id' => $land_owner);
//            $land_owner_model = $CI->land_owner_model->get_row_by_opt($land_owner_opt);
//            if (!$land_owner_model) {
//                $land_owner_model = $CI->land_owner_model->save($land_owner_opt);
//                $land_owner_id = $land_owner_model['id'];
//            }
//        }




        return $land_id;
    }

    ##### building #####

    function building_fields() {
        $land_fields = array(
            'building_id' => '',
            'building_owner' => '',
            'building_type_id' => '',
            'building_no' => '',
            'building_name' => '',
            'building_moo' => '',
            'building_soi' => '',
            'building_road' => '',
            'building_district_id' => '',
            'building_amphur_id' => '',
            'building_province_id' => ''
        );

        return $land_fields;
    }

    function building_full_detail($data) {
        $CI = & get_instance();
        $CI->load->model('building_model');

        //building_type_name
        $building_type = $CI->building_model->get_building_type_by_id($data['building_type_id']);
        $building_type_name = $building_type['building_type_name'];
        $result['building_type_name'] = $building_type_name;

        //address
        $address = '';
        if ($data['building_no']) {
            $address .= "บ้านเลขที่ {$data['building_no']} ";
        }
        if ($data['building_moo']) {
            $address .= "หมู่ {$data['building_moo']} ";
        }
        if ($data['building_name']) {
            $address .= "อาคาร{$data['building_name']} ";
        }
        if ($data['building_soi']) {
            $address .= "ซอย{$data['building_soi']} ";
        }
        if ($data['building_road']) {
            $address .= "ถนน{$data['building_road']} ";
        }
        if ($data['building_district_id']) {
            $district = $CI->building_model->get_district($data['building_district_id']);
            $address .= "ตำบล{$district['district_name']} ";
        }
        if ($data['building_amphur_id']) {
            $amphur = $CI->building_model->get_amphur($data['building_amphur_id']);
            $address .= "อำเภอ{$amphur['amphur_name']} ";
        }
        if ($data['building_province_id']) {
            $province = $CI->building_model->get_province($data['building_province_id']);
            $address .= "จังหวัด{$province['province_name']} ";
        }

        $result['address'] = $address;

        return $result;
    }

    function land_full_detail($data) {
        $CI = & get_instance();
        $CI->load->model('land_model');
        $CI->load->model('building_model');

        //address
        $address = '';
        if ($data['land_addr_no']) {
            $address .= "เลขที่ดิน {$data['land_addr_no']} ";
        }
        if ($data['land_addr_moo']) {
            $address .= "หมู่ {$data['land_addr_moo']} ";
        }
        if ($data['land_addr_district_id']) {
            $district = $CI->building_model->get_district($data['land_addr_district_id']);
            $address .= "ตำบล {$district['district_name']} ";
        }
        if ($data['land_addr_amphur_id']) {
            $amphur = $CI->building_model->get_amphur($data['land_addr_amphur_id']);
            $address .= "อำเภอ {$amphur['amphur_name']} ";
        }
        if ($data['land_addr_province_id']) {
            $province = $CI->building_model->get_province($data['land_addr_province_id']);
            $address .= "จังหวัด {$province['province_name']} ";
        }

        $result['address'] = $address;

        return $result;
    }

    function land_detail($data) {
        $CI = & get_instance();
        $CI->load->model('land_model');
        $CI->load->model('mst_model');
        //$CI->load->model('building_model');

        if ($data['land_addr_district_id']) {
            $district = $CI->building_model->get_district($data['land_addr_district_id']);
            $data['land_addr_district_name'] = $district['district_name'];
        }
        if ($data['land_addr_amphur_id']) {
            $amphur = $CI->building_model->get_amphur($data['land_addr_amphur_id']);
            $data['land_addr_amphur_name'] = $amphur['amphur_name'];
        }
        if ($data['land_addr_province_id']) {
            $province = $CI->building_model->get_province($data['land_addr_province_id']);
            $data['land_addr_province_name'] = $province['province_name'];
        }
        if ($data['land_type_id']) {
            $land_type = $CI->mst_model->get_by_id('land_type', $data['land_type_id']);
            $data['land_type_name'] = $land_type['land_type_name'];
        }

        return $data;
    }

    function building_detail($data) {
        $CI = & get_instance();
        $CI->load->model('building_model');

        //building_type_name
        $building_type = $CI->building_model->get_building_type_by_id($data['building_type_id']);
        $building_type_name = $building_type['building_type_name'];
        $data['building_type_name'] = $building_type_name;

        if ($data['building_district_id']) {
            $district = $CI->building_model->get_district($data['building_district_id']);
            $data['building_district_name'] = $district['district_name'];
        }
        if ($data['building_amphur_id']) {
            $amphur = $CI->building_model->get_amphur($data['building_amphur_id']);
            $data['building_amphur_name'] = $amphur['amphur_name'];
        }
        if ($data['building_province_id']) {
            $province = $CI->building_model->get_province($data['building_province_id']);
            $data['building_province_name'] = $province['province_name'];
        }

        return $data;
    }

    #### loan_type ######

    function loan_type_by_loan_id($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_type_model');
        $CI->load->model('land_model');
        $CI->load->model('building_model');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');

        //$loan_type_model
        $loan_type_model = $CI->loan_type_model->get_by_opt(array('loan_id' => $loan_id));
        if ($loan_type_model) {
            foreach ($loan_type_model as $key => $loan_type) {
                if ($loan_type['loan_objective_id'] == 1) {
                    $loan_type_model[$key]['loan_type_redeem_amount'] = money_num_to_str($loan_type['loan_type_redeem_amount']);
                    $loan_type_model[$key]['loan_type_redeem_amount_thai'] = num2wordsThai($loan_type['loan_type_redeem_amount']);
                    $loan_type_model[$key]['loan_type_redeem_owner_info'] = $this->person_detail($loan_type['loan_type_redeem_owner']);
                    $loan_type_model[$key]['loan_type_redeem_owner_fullname'] = "{$loan_type_model[$key]['loan_type_redeem_owner_info']['title_name']}{$loan_type_model[$key]['loan_type_redeem_owner_info']['person_fname']} {$loan_type_model[$key]['loan_type_redeem_owner_info']['person_lname']}";
                } elseif ($loan_type['loan_objective_id'] == 2) {
                    $loan_type_model[$key]['loan_type_contract_amount'] = money_num_to_str($loan_type['loan_type_contract_amount']);
                    $loan_type_model[$key]['loan_type_contract_amount_thai'] = num2wordsThai($loan_type['loan_type_contract_amount']);
                    $loan_type_model[$key]['loan_type_contract_creditor_info'] = $this->person_detail($loan_type['loan_type_contract_creditor']);
                    $loan_type_model[$key]['loan_type_contract_borrowers_info'] = $this->person_detail($loan_type['loan_type_contract_borrowers']);
                    $loan_type_model[$key]['loan_type_contract_creditor_fullname'] = "{$loan_type_model[$key]['loan_type_contract_creditor_info']['title_name']}{$loan_type_model[$key]['loan_type_contract_creditor_info']['person_fname']} {$loan_type_model[$key]['loan_type_contract_creditor_info']['person_lname']}";
                    $loan_type_model[$key]['loan_type_contract_borrowers_fullname'] = "{$loan_type_model[$key]['loan_type_contract_borrowers_info']['title_name']}{$loan_type_model[$key]['loan_type_contract_borrowers_info']['person_fname']} {$loan_type_model[$key]['loan_type_contract_borrowers_info']['person_lname']}";
                } elseif ($loan_type['loan_objective_id'] == 3) {
                    $loan_type_model[$key]['loan_type_case_amount'] = money_num_to_str($loan_type['loan_type_case_amount']);
                    $loan_type_model[$key]['loan_type_case_amount_thai'] = num2wordsThai($loan_type['loan_type_case_amount']);
                    $loan_type_model[$key]['loan_type_case_type_name'] = $CI->loan_type_model->case_type[$loan_type['loan_type_case_type']];
                    $loan_type_model[$key]['loan_type_case_plaintiff_info'] = $this->person_detail($loan_type['loan_type_case_plaintiff_id']);
                    $loan_type_model[$key]['loan_type_case_defendant_info'] = $this->person_detail($loan_type['loan_type_case_defendant_id']);
                    $loan_type_model[$key]['loan_type_case_plaintiff_fullname'] = "{$loan_type_model[$key]['loan_type_case_plaintiff_info']['title_name']}{$loan_type_model[$key]['loan_type_case_plaintiff_info']['person_fname']} {$loan_type_model[$key]['loan_type_case_plaintiff_info']['person_lname']}";
                    $loan_type_model[$key]['loan_type_case_defendant_fullname'] = "{$loan_type_model[$key]['loan_type_case_defendant_info']['title_name']}{$loan_type_model[$key]['loan_type_case_defendant_info']['person_fname']} {$loan_type_model[$key]['loan_type_case_defendant_info']['person_lname']}";
                } elseif ($loan_type['loan_objective_id'] == 4) {
                    $loan_type_model[$key]['loan_type_sale_amount'] = money_num_to_str($loan_type['loan_type_sale_amount']);
                    $loan_type_model[$key]['loan_type_sale_amount_thai'] = num2wordsThai($loan_type['loan_type_sale_amount']);
                    $loan_type_model[$key]['loan_type_sale_date'] = thai_display_date($loan_type['loan_type_sale_date']);
                } elseif ($loan_type['loan_objective_id'] == 5) {
                    $loan_type_model[$key]['loan_type_farm_income_per_year'] = money_num_to_str($loan_type['loan_type_farm_income_per_year']);
                    $loan_type_model[$key]['loan_type_farm_income_per_year_thai'] = num2wordsThai($loan_type['loan_type_farm_income_per_year']);
                    $loan_type_model[$key]['loan_type_farm_payment_amount'] = money_num_to_str($loan_type['loan_type_farm_payment_amount']);
                    $loan_type_model[$key]['loan_type_farm_payment_amount_thai'] = num2wordsThai($loan_type['loan_type_farm_payment_amount']);
                }

                //land of loan_type
                $land_model = array();
                $land_list = $CI->land_model->get_by_opt(array('loan_type_id' => $loan_type['loan_type_id']));

                if ($land_list) {
                    foreach ($land_list as $key_land => $data_land) {
                        $land_model[$key_land] = $this->land_detail($data_land);

                        //building of land
                        $building_model = array();
                        $building_list = $CI->building_model->get_by_opt(array('land_id' => $data_land['land_id']));

                        if ($building_list) {
                            foreach ($building_list as $key_building => $data_building) {
                                $building_model[$key_building] = $this->building_detail($data_building);
                            }
                        }

                        $land_model[$key_land]['building_list'] = $building_model;
                    }
                }

                $loan_type_model[$key]['land_list'] = $land_model;
            }
        }

        return $loan_type_model;
    }

    #### loan_income ######

    function loan_income_report($person_id) {
        $CI = & get_instance();
        $CI->load->model('loan_income_model');
        $loan_income_data = $CI->loan_income_model->get_by_opt(array('person_id' => $person_id));
        $activity_income = array();

        $y = date('Y');
        $y1 = $y + 543;
        $y2 = $y + 542;
        $y3 = $y + 544;

        if ($loan_income_data) {
            $activity_income[$y1] = array(); //ปีปัจจุบัน
            $activity_income[$y2] = array(); //ปีก่อนหน้า
            $activity_income[$y3] = array(); //ปีถัดไป

            foreach ($loan_income_data as $key => $income) {
                $activity_income[$y1][$income['loan_activity_type']][] = array(
                    'loan_activity_name' => $income['loan_activity_name'],
                    'loan_activity_income' => money_num_to_str($income['loan_activity_income1']),
                    'loan_activity_income_num' => $income['loan_activity_income1'],
                );
                $activity_income[$y2][$income['loan_activity_type']][] = array(
                    'loan_activity_name' => $income['loan_activity_name'],
                    'loan_activity_income' => money_num_to_str($income['loan_activity_income2']),
                    'loan_activity_income_num' => $income['loan_activity_income2'],
                );
                $activity_income[$y3][$income['loan_activity_type']][] = array(
                    'loan_activity_name' => $income['loan_activity_name'],
                    'loan_activity_income' => money_num_to_str($income['loan_activity_income3']),
                    'loan_activity_income_num' => $income['loan_activity_income3'],
                );
            }
        }

        return $activity_income;
    }

    function loan_expenditure_report($person_id) {
        $CI = & get_instance();
        $CI->load->model('loan_income_model');
        $CI->load->model('loan_expenditure_model');
        $loan_income_data = $CI->loan_income_model->get_by_opt(array('person_id' => $person_id));
        $loan_expenditure_data = $CI->loan_expenditure_model->get_by_opt(array('person_id' => $person_id));

        $y = date('Y');
        $y1 = $y + 543;
        $y2 = $y + 542;
        $y3 = $y + 544;

        $activity_expenditure[$y1] = array(
            1 => array(),
            2 => array(),
            3 => array(),
            4 => array()
        ); //ปีปัจจุบัน
        $activity_expenditure[$y2] = array(
            1 => array(),
            2 => array(),
            3 => array(),
            4 => array()
        ); //ปีก่อนหน้า
        $activity_expenditure[$y3] = array(
            1 => array(),
            2 => array(),
            3 => array(),
            4 => array()
        ); //ปีถัดไป

        $activity_type1 = 1; //ลงทุนทำการเกษตร
        $activity_type2 = 2; //ลงนอกการเกษตร
        $activity_type3 = 3; //ค่าใช้จ่ายในครัวเรือน
        $activity_type4 = 4; //อื่นๆ
        //$loan_income_data
        if ($loan_income_data) {
            foreach ($loan_income_data as $key => $income) {
                if ($income['loan_activity_type'] == 3) {
                    $activity_type = $activity_type4; // อื่นๆ
                } else {
                    $activity_type = $income['loan_activity_type'];
                }

                $activity_expenditure[$y1][$activity_type][] = array(
                    'loan_expenditure_name' => $income['loan_activity_name'],
                    'loan_expenditure_amount' => money_num_to_str($income['loan_activity_expenditure1']),
                    'loan_expenditure_amount_num' =>$income['loan_activity_expenditure1']
                );
                $activity_expenditure[$y2][$activity_type][] = array(
                    'loan_expenditure_name' => $income['loan_activity_name'],
                    'loan_expenditure_amount' => money_num_to_str($income['loan_activity_expenditure2']),
                    'loan_expenditure_amount_num' =>$income['loan_activity_expenditure2']
                );
                $activity_expenditure[$y3][$activity_type][] = array(
                    'loan_expenditure_name' => $income['loan_activity_name'],
                    'loan_expenditure_amount' => money_num_to_str($income['loan_activity_expenditure3']),
                    'loan_expenditure_amount_num' =>$income['loan_activity_expenditure3']
                );
            }
        }

        //$loan_expenditure_data
        if ($loan_expenditure_data) {
            foreach ($loan_expenditure_data as $key => $expenditure) {
                if ($expenditure['loan_expenditure_type'] == 2) {
                    $activity_type = $activity_type4; // อื่นๆ
                } else {
                    $activity_type = $activity_type3;
                }

                $activity_expenditure[$y1][$activity_type][] = array(
                    'loan_expenditure_type_name' => $expenditure['loan_expenditure_type_name'],
                    'loan_expenditure_name' => $expenditure['loan_expenditure_name'],
                    'loan_expenditure_amount' => money_num_to_str($expenditure['loan_expenditure_amount1']),
                    'loan_expenditure_amount_num' =>$expenditure['loan_expenditure_amount1']
                );
                $activity_expenditure[$y2][$activity_type][] = array(
                    'loan_expenditure_type_name' => $expenditure['loan_expenditure_type_name'],
                    'loan_expenditure_name' => $expenditure['loan_expenditure_name'],
                    'loan_expenditure_amount' => money_num_to_str($expenditure['loan_expenditure_amount2']),
                    'loan_expenditure_amount_num' =>$expenditure['loan_expenditure_amount2']
                );
                $activity_expenditure[$y3][$activity_type][] = array(
                    'loan_expenditure_type_name' => $expenditure['loan_expenditure_type_name'],
                    'loan_expenditure_name' => $expenditure['loan_expenditure_name'],
                    'loan_expenditure_amount' => money_num_to_str($expenditure['loan_expenditure_amount3']),
                    'loan_expenditure_amount_num' =>$expenditure['loan_expenditure_amount3']
                );
            }
        }

        return $activity_expenditure;
    }

    #### loan_land_occupy ######

    function loan_land_occupy_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_land_occupy_model');
        $CI->load->model('land_model');
        $CI->load->model('loan_model');
        $CI->load->model('land_owner_model');
        $loan_land_occupy_list = $CI->loan_land_occupy_model->get_by_opt(array('loan_id' => $loan_id));


        //loan_model
        $loan_model = $CI->loan_model->get_by_id($loan_id);

        //person_model
        $person_model = $this->person_detail($loan_model['person_id']);
        $spouse_model = $this->person_detail($loan_model['loan_spouse']);
        $person_id = $person_model['person_id'];
        $spouse_id = $spouse_model['person_id'];

        if ($loan_land_occupy_list) {
            foreach ($loan_land_occupy_list as $key => $loan_land_occupy) {
                $owner_fullname = '';
                $land_owner_model = $CI->land_owner_model->get_by_opt($opt = array('land_id' => $loan_land_occupy['land_id'], 'person_id' => $person_id));
                if ($land_owner_model) {
                    //$person_model
                    $owner_fullname = $person_model['person_fullname'];
                } else {
                    $land_owner_model = $CI->land_owner_model->get_by_opt($opt = array('land_id' => $loan_land_occupy['land_id'], 'person_id' => $spouse_id));
                    if ($land_owner_model) {
                        //$spouse_model
                        $owner_fullname = $spouse_model['person_fullname'];
                    }
                }

                $land_model = $CI->land_model->get_by_id($loan_land_occupy['land_id']);
                $land_model['owner_fullname'] = $owner_fullname;
                $land_detail = $this->land_detail($land_model);
                $land_full_detail = $this->land_full_detail($land_model);

                $loan_land_occupy_list[$key]['land_detail'] = $land_detail;
                $loan_land_occupy_list[$key]['land_full_detail'] = $land_full_detail;
            }
        }

        return $loan_land_occupy_list;
    }

    #### loan_asset_model ######

    function loan_asset_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_asset_model');
        $CI->load->model('land_model');
        $CI->load->model('mst_model');
        $loan_asset_list = $CI->loan_asset_model->get_by_opt(array('loan_id' => $loan_id));

        if ($loan_asset_list) {
            foreach ($loan_asset_list as $key => $loan_asset) {
                if ($loan_asset['loan_asset_uom']) {
                    $loan_asset_uom = $CI->mst_model->get_by_id('uom', $loan_asset['loan_asset_uom']);
                    $loan_asset_list[$key]['loan_asset_uom_name'] = $loan_asset_uom['uom_name'];
                }
            }
        }

        return $loan_asset_list;
    }

    #### loan_debt_model ######

    function loan_debt_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_debt_model');
        $CI->load->helper('loan_helper');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');
        $data_list = $CI->loan_debt_model->get_by_opt(array('loan_id' => $loan_id));


        if ($data_list) {
            foreach ($data_list as $key => $data) {
                $data_list[$key]['loan_debt_amount'] = num2Thai(money_num_to_str($data['loan_debt_amount']));
                $data_list[$key]['loan_debt_amount_start'] = (money_num_to_str($data['loan_debt_amount_start']));
                $data_list[$key]['loan_debt_amount_remain'] = (money_num_to_str($data['loan_debt_amount_remain']));
                $data_list[$key]['loan_debt_date_start'] = thai_display_date_short($data['loan_debt_date_start']);
                $data_list[$key]['loan_debt_date_end'] = thai_display_date_short($data['loan_debt_date_end']);
            }
        }

        return $data_list;
    }

    #### loan_co_model ######

    function loan_co_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_co_model');
        $CI->load->model('mst_model');
        $CI->load->helper('loan_helper');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');
        $data_list = $CI->loan_co_model->get_by_opt(array('loan_id' => $loan_id));

        if ($data_list) {
            foreach ($data_list as $key => $data) {
                //relationship_name
                $relationship = $CI->mst_model->get_by_id('relationship', $data['relationship_id']);
                $data_list[$key]['relationship_name'] = $relationship['relationship_name'];

                //person_detail
                $person_data = $this->person_detail($data['person_id']);
                $person_data['person_fullname'] = "{$person_data['title_name']}{$person_data['person_fname']} {$person_data['person_lname']}";
                $person_data['person_income_per_month'] = money_str_to_num($person_data['person_income_per_month']);
                $person_data['person_expenditure_per_month'] = money_str_to_num($person_data['person_expenditure_per_month']);
                $data_list[$key]['person_detail'] = $person_data;
            }
        }

        return $data_list;
    }

    #### loan_guarantee ######

    function loan_guarantee_land_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_guarantee_land_model');
        $CI->load->model('mst_model');
        $CI->load->model('land_model');
        $CI->load->model('building_model');

        $CI->load->helper('loan_helper');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');
        $data_list = $CI->loan_guarantee_land_model->get_by_opt(array('loan_id' => $loan_id));

        if ($data_list) {
            foreach ($data_list as $key => $data) {
                //land of loan_type
                $land_model = array();
                $data_land = $CI->land_model->get_by_id($data['land_id']);
                $land_model = $this->land_detail($data_land);

                //building of land
                $building_model = array();
                $building_list = $CI->building_model->get_by_opt(array('land_id' => $data_land['land_id']));
                if ($building_list) {
                    foreach ($building_list as $key_building => $data_building) {
                        $building_model[$key_building] = $this->building_detail($data_building);
                    }
                }

                $land_model['building_list'] = $building_model;
                $data_list[$key]['land_list'] = $land_model;
                $data_list[$key]['loan_guarantee_land_fullname'] = '';

                if ($data_list[$key]['loan_guarantee_land_bind_type'] == 1) {
                    $data_list[$key]['loan_guarantee_land_bind_amount'] = num2Thai(money_num_to_str($data_list[$key]['loan_guarantee_land_bind_amount']));
                    $data_list[$key]['loan_guarantee_land_relief_enddate'] = thai_display_date($data_list[$key]['loan_guarantee_land_relief_enddate']);
                    $data_list[$key]['loan_guarantee_land_relief_permonth'] = num2Thai(money_num_to_str($data_list[$key]['loan_guarantee_land_relief_permonth']));

                    $title_mst = $CI->mst_model->get_by_id('title', $data_list[$key]['loan_guarantee_land_title_id']);
                    $data_list[$key]['loan_guarantee_land_fullname'] = "{$title_mst['title_name']}{$data_list[$key]['loan_guarantee_land_fname']} {$data_list[$key]['loan_guarantee_land_lname']}";
                }
            }
        }

        return $data_list;
    }

    function loan_guarantee_bond_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_guarantee_bond_model');
        $CI->load->model('mst_model');
        $CI->load->model('land_model');
        $CI->load->model('building_model');

        $CI->load->helper('loan_helper');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');
        $data_list = $CI->loan_guarantee_bond_model->get_by_opt(array('loan_id' => $loan_id));

        if ($data_list) {
            foreach ($data_list as $key => $data) {
                $data_list[$key]['loan_bond_amount_normal'] = $data_list[$key]['loan_bond_amount'];
                $data_list[$key]['loan_bond_amount_thai'] = num2wordsThai($data_list[$key]['loan_bond_amount']);
                $data_list[$key]['loan_bond_amount'] = num2Thai(money_num_to_str($data_list[$key]['loan_bond_amount']));
                $data_list[$key]['loan_bond_thaiid'] = num2Thai($data_list[$key]['loan_bond_thaiid']);
                $data_list[$key]['loan_bond_tax'] = num2Thai($data_list[$key]['loan_bond_tax']);
            }
        }

        return $data_list;
    }

    function loan_guarantee_bookbank_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_guarantee_bookbank_model');
        $CI->load->model('mst_model');
        $CI->load->model('land_model');
        $CI->load->model('building_model');

        $CI->load->helper('loan_helper');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');
        $data_list = $CI->loan_guarantee_bookbank_model->get_by_opt(array('loan_id' => $loan_id));

        if ($data_list) {
            foreach ($data_list as $key => $data) {
                $data_list[$key]['loan_bookbank_amount_normal'] = $data_list[$key]['loan_bookbank_amount'];
                $data_list[$key]['loan_bookbank_amount_thai'] = num2wordsThai($data_list[$key]['loan_bookbank_amount']);
                $data_list[$key]['loan_bookbank_amount'] = num2Thai(money_num_to_str($data_list[$key]['loan_bookbank_amount']));
                $data_list[$key]['loan_bookbank_date'] = thai_display_date($data_list[$key]['loan_bookbank_date']);

                //bank
                $land_type = $CI->mst_model->get_by_id('bank', $data['bank_id']);
                $data_list[$key]['bank_name'] = $land_type['bank_name'];

                //loan_bookbank_type
                $data_list[$key]['loan_bookbank_type_name'] = $CI->loan_guarantee_bookbank_model->loan_bookbank_type[$data_list[$key]['loan_bookbank_type']];
            }
        }

        return $data_list;
    }

    function loan_guarantee_bondsman_report($loan_id) {
        $CI = & get_instance();
        $CI->load->model('loan_guarantee_bondsman_model');
        $CI->load->model('mst_model');
        $CI->load->model('land_model');
        $CI->load->model('building_model');

        $CI->load->helper('loan_helper');
        $CI->load->helper('currency_helper');
        $CI->load->helper('date_helper');
        $data_list = $CI->loan_guarantee_bondsman_model->get_by_opt(array('loan_id' => $loan_id));

        if ($data_list) {
            foreach ($data_list as $key => $data) {
                //person_detail
                $person_data = $this->person_detail($data['person_id']);
                $person_data['person_cardid_emp'] = num2Thai($person_data['person_cardid_emp']);
                $person_data['person_cardid_emp_startdate'] = thai_display_date($person_data['person_cardid_emp_startdate']);
                $person_data['person_cardid_emp_expiredate'] = thai_display_date($person_data['person_cardid_emp_expiredate']);
                $person_data['person_income_per_year'] = num2Thai(money_num_to_str($person_data['person_income_per_month'] * 12));
                $person_data['person_income_per_month'] = num2Thai(money_num_to_str($person_data['person_income_per_month']));
                $person_data['person_loan_other_check'] = $person_data['person_loan_other_per_month'] ? 1 : 0;
                $person_data['person_guarantee_other_amount_check'] = $person_data['person_guarantee_other_amount'] ? 1 : 0;
                $person_data['person_loan_other_per_month'] = num2Thai(money_num_to_str($person_data['person_loan_other_per_month']));
                $person_data['person_loan_other_per_year'] = num2Thai(money_num_to_str($person_data['person_loan_other_per_year']));
                $person_data['person_guarantee_other_amount_per_year'] = num2Thai(money_num_to_str($person_data['person_guarantee_other_amount'] * 12));
                $person_data['person_guarantee_other_amount'] = num2Thai(money_num_to_str($person_data['person_guarantee_other_amount']));
                $person_data['person_fullname'] = "{$person_data['title_name']}{$person_data['person_fname']} {$person_data['person_lname']}";
                $data_list[$key]['person_detail'] = $person_data;
            }
        }

        return $data_list;
    }

    #### function ######

    function check_val_null($val) {
        return $val ? $val : NULL;
    }

    function check_loan_co_age_60($person_id, $loan_id) {
        $CI = & get_instance();
        $CI->load->model('person_model');
        $CI->load->model('loan_co_model');

        $person_data = $CI->person_model->get_by_id($person_id);
        $person_birthdate = $person_data['person_birthdate'];
        $person_age = calculate_age($person_birthdate);

        //check age
        if ($person_age > 60) {
            $loan_co_model = $CI->loan_co_model->get_by_opt(array('loan_id' => $loan_id));

            if (!$loan_co_model) {
                //echo '<script>alert("อายุเกิน 60ปี ต้องมีผู้กู้ร่วม");</script>';
                return FALSE;
            }
        }

        return TRUE;
    }

    function sum_loan_amount($person_id, $objective_id) {
        $CI = & get_instance();
        $CI->load->model('loan_model');
        $CI->load->model('loan_type_model');

        $sum_amount = $CI->loan_model->sum_amount($person_id);
        if ($objective_id == 5) {
            if ($sum_amount['loan_amount'] >= 200000) {
                echo '<script>alert("กู้ได้ไม่เกิน 200,000.00 บาท");</script>';
            }
        } else {
            if ($sum_amount['loan_amount'] >= 3000000) {
                echo '<script>alert("กู้ได้ไม่เกิน 3,000,000.00 บาท");</script>';
            }
        }
    }

    function money_num_to_str($money = 0) {
        if(!$money){
            $money = 0;
        }
        return number_format($money, 2);
    }
}
