<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DB_lib {
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	public function field_data($list_field){
		$arr_fields = null;
		
		//if($list_field)
			foreach($list_field as $field){
				$arr_fields[$field->name] = $this->set_default($field->type);
			}
		return $arr_fields;
	}
	function set_default($field){
		switch($field){
			case 'integer' : return 0;
			case 'date' : return '1900-01-01';
			case 'datetime' : return '1900-01-01 00:00:00';
			case 'double precision' : return (double)0.00;
			case 'double' : return (double)0.00;
			case 'numeric' : return (double)0.00;
			default : return ""; //string
		}
	}
	
   
}
