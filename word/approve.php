<?php
include_once('class/tbs_class.php');
include_once('class/tbs_plugin_opentbs.php');

$TBS = new clsTinyButStrong;
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

$TBS -> MergeField('xxx', '123');

$xxx = '123';
// -----------------
// Load the template
// -----------------
$template = 'test.odt';
$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8);


// Define the name of the output file
$output_file_name = str_replace('.', '_'.date('Y-m-d').'.', $template);

$TBS->Show(OPENTBS_DOWNLOAD, $output_file_name);
exit();
?>